﻿(function (alternateItems, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    // Public properties
    alternateItems.somePublicProperty = {};

    // Public Methods
    alternateItems.somePublicMethod = function () {
    };

    // Private Methods
    function somePrivateMethod(item) {
    }
}(window.alternateItems = window.alternateItems || {}, jQuery));