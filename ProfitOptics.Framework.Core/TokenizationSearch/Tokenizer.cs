﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProfitOptics.Framework.Core.TokenizationSearch
{
    public class Tokenizer
    {
        /// <summary>
        /// Google like logic - "&" is used to create AND connections, otherwise it's OR
        /// </summary>
        /// <param name="text">text to tokenize</param>
        /// <returns></returns>
        public static TokenCollection Tokenize(string text)
        {
            TokenCollection tokenCollection = new TokenCollection();

            if (string.IsNullOrEmpty(text))
            {
                return tokenCollection;
            }

            string[] stringTokens = TokenizeToWords(text);

            if (stringTokens.Length > 0)
            {
                int i = 0;
                while (i < stringTokens.Length)
                {
                    TokenAND tokenAND = new TokenAND();
                    while (i < stringTokens.Length && stringTokens[i] != "&")
                    {
                        tokenAND.mTokensOR.Add(stringTokens[i]);
                        i++;
                    }
                    tokenCollection.mTokensAND.Add(tokenAND);
                    i++;
                }
            }

            return tokenCollection;
        }

        /// <summary>
        /// Strictly AND logic - all tokens (including "&" if there is one) must appear in the searched string
        /// </summary>
        /// <param name="text">text to tokenize</param>
        /// <returns></returns>
        public static TokenCollection TokenizeAND(string text)
        {
            TokenCollection tokenCollection = new TokenCollection();

            if (string.IsNullOrEmpty(text))
            {
                return tokenCollection;
            }

            string[] stringTokens = TokenizeToWords(text);

            if (stringTokens.Length > 0)
            {
                int i = 0;
                while (i < stringTokens.Length)
                {
                    TokenAND tokenAND = new TokenAND();
                    tokenAND.mTokensOR.Add(stringTokens[i]);
                    tokenCollection.mTokensAND.Add(tokenAND);
                    i++;
                }
            }

            return tokenCollection;
        }

        /// <summary>
        /// Strictly OR logic - at least token (including "&" if there is one) must appear in the searched string
        /// </summary>
        /// <param name="text">text to tokenize</param>
        /// <returns></returns>
        public static TokenCollection TokenizeOR(string text)
        {
            TokenCollection tokenCollection = new TokenCollection();

            if (string.IsNullOrEmpty(text))
            {
                return tokenCollection;
            }

            string[] stringTokens = TokenizeToWords(text);

            if (stringTokens.Length > 0)
            {
                int i = 0;
                TokenAND tokenAND = new TokenAND();
                while (i < stringTokens.Length)
                {
                    tokenAND.mTokensOR.Add(stringTokens[i]);
                    i++;
                }
                tokenCollection.mTokensAND.Add(tokenAND);
            }

            return tokenCollection;
        }

        public static string[] TokenizeToWords(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return new string[] { };
            }

            var words = new List<string>();

            var currentWord = string.Empty;

            text = text.ToLower().Trim();

            var insideQuote = false;

            foreach (var ch in text)
            {
                if (ch != ' ' || insideQuote)
                {
                    if (ch != '"')
                    {
                        currentWord += ch;
                    }
                    else
                    {
                        insideQuote = !insideQuote;
                    }
                }
                else
                {
                    words.Add(currentWord);
                    currentWord = string.Empty;
                }
            }

            if (currentWord != string.Empty)
            {
                words.Add(currentWord);
            }

            return words.ToArray();
        }

        public static Dictionary<string, List<string>> TokenizeSpecial(string[] specials, string text)
        {
            var toReturn = new Dictionary<string, List<string>>();

            if (string.IsNullOrEmpty(text))
            {
                return new Dictionary<string, List<string>>();
            }

            text = text.ToLower();

            if (text.Contains(":"))
            {
                foreach (var item in specials)
                {
                    if (!text.Contains($"{item}:"))
                    {
                        continue;
                    }

                    var forThisOne = text.Substring(text.IndexOf($"{item}:")).Replace($"{item}:", "");

                    // Should we remove the last token
                    var remove = false;
                    if (forThisOne.Contains(":"))
                    {
                        remove = true;
                        forThisOne = forThisOne.Substring(0, forThisOne.IndexOf(":"));
                    }

                    var tokens = new List<string>(TokenizeToWords(forThisOne));

                    if (!toReturn.ContainsKey(item))
                    {
                        toReturn[item] = new List<string>();
                    }

                    if (remove)
                    {
                        tokens.RemoveAt(tokens.Count - 1);
                    }

                    toReturn[item].AddRange(tokens);
                }
            }

            string[] plainTokens;

            if (toReturn.Keys.Count == 0 && (plainTokens = TokenizeToWords(text)).Length > 0)
            {
                toReturn["default"] = new List<string>(plainTokens);
            }

            return toReturn;
        }
    }
}