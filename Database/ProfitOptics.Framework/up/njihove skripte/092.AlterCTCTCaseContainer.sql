USE [Sox]

BEGIN

 	ALTER TABLE [Sox].[dbo].[CTCaseContainer] DROP COLUMN CaseTime;
	ALTER TABLE [Sox].[dbo].[CTCaseContainer] ADD CaseTime INT NULL;
	
END