﻿using Intuit.Ipp.OAuth2PlatformClient;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Models.QuickBooks;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Infrastructure.Clients
{
    public interface IQuickBooksClient
    {
        string GetAuthorizeUrl(string scope);
        QBUpdateTokenResponse UpdateToken(string code);
        Task<TokenResponse> RefreshAuthorizeToken(string refreshToken);
        Task<TokenRevocationResponse> RevokeAccessToken(string token);
        List<Customer> GetCustomerList(string token);
        Customer GetCustomer(int id, string token);
        EditCustomerResponse EditCustomer(Customer customer, string token);
        EditCustomerResponse SetCustomerActiveStatus(int customerId, bool isActive, string token, string syncToken);
        QuickBooksOptions GetOptions();
        EditVendorResponse EditVendor(Vendor vendor, string token);
        Vendor GetVendor(int id, string token);
        Item GetItem(string sku, string token);
        EditEstimateResponse EditEstimate(CreateEstimate estimate, string token);
        Estimate CreateEstimate(GCCase gcCase, string token, out string description);
        Invoice GetInvoice(int qbCustomerId, string tnxDate, string token);
        EditInvoiceResponse CreateInvoice(Invoice invoice, string token);
        EditInvoiceResponse EditInvoice(Invoice invoice, string token);
        EditItemResponse EditItem(Item qbItem, string token);
    }
}
