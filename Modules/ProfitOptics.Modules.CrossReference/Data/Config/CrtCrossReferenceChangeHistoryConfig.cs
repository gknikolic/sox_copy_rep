﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.CrossReference.Data.Domain;

namespace ProfitOptics.Modules.CrossReference.Data.Config
{
    public class CrtCrossReferenceChangeHistoryConfig : IEntityTypeConfiguration<CrtCrossReferenceChangeHistory>
    {
        public void Configure(EntityTypeBuilder<CrtCrossReferenceChangeHistory> builder)
        {
            builder.ToTable("CrtCrossReferenceChangeHistory");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.CompetitorItemNumber).HasMaxLength(50).IsRequired();
            builder.Property(entity => entity.CompetitorItemDescription).HasMaxLength(250);
            builder.Property(entity => entity.CompetitorWebsiteUrl).HasMaxLength(250);
            builder.Property(entity => entity.Notes).HasMaxLength(500);

        }
    }
}
