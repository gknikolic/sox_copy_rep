﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.DataLayer.GroundControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCLocationConfig : IEntityTypeConfiguration<GCLocation>
    {
        public void Configure(EntityTypeBuilder<GCLocation> builder)
        {
            builder.ToTable("GCLocations");
            builder.HasKey(entity => entity.Id);
        }
    }
}
