﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Services;

namespace ProfitOptics.Framework.Web.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuickBooksController : Controller
    {
        IQuickBooksClient _quickBooksClient;

        public QuickBooksController(IQuickBooksClient quickBooksClient)
        {
            _quickBooksClient = quickBooksClient;
        }

		[HttpGet]
		public IActionResult Index()
        {
            return View();
        }

		[HttpGet]
		[Route("customers/{id}")]
		public StatusCodeResult GetCustomer(int customerId)
		{
			try
			{
				//var userClaims = _userService.GetUserClaims(User.GetUserId(), "quickbooks_").ToList();
				//var result = _soxService.GetQBCustomer(customerId, (User as ClaimsPrincipal));
				return Ok();
			}
			catch (Exception ex)
			{
				return BadRequest();
			}

		}
	}
}
