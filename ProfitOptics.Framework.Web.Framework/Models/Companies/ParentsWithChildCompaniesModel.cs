﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Models
{
    public class ParentsWithChildCompaniesModel
    {
        public List<ParentCompanyModel> ParentCompanies { get; set; }
        public string QBCusomerExternalUrl { get; set; }
    }
}
