﻿using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.ViewComponents
{
    public class SideBarViewComponent : ViewComponent
    {
        private readonly ILayoutModelFactory _layoutModelFactory;

        public SideBarViewComponent(ILayoutModelFactory layoutModelFactory)
        {
            this._layoutModelFactory = layoutModelFactory;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = await _layoutModelFactory.PrepareSideBarModelAsync();
            return View(model);
        }
    }
}
