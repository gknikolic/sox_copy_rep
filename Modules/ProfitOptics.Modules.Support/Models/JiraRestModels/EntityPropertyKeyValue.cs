﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class EntityPropertyKeyValue
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}