USE [Sox]

ALTER TABLE [Sox].[dbo].[GCProcedureType] ADD [ICD9] NVARCHAR(20) NULL;
ALTER TABLE [Sox].[dbo].[GCProcedureType] ADD [ICD10] NVARCHAR(20) NULL;

GO