﻿namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum GroupByType
    {
        Location,
        Product
    }
}