@echo off
setlocal ENABLEEXTENSIONS

SET database.name="SoxQATest"
SET sql.files.directory="."
SET server.database="."
SET version.file="_db_build.xml"
SET version.xpath="//buildInfo/version"
SET environment="Developer"
SET ConnString="Data Source=%server.database%;Initial Catalog=%database.name%;Integrated Security=True;"

rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.database% /c=%ConnString% /vf=%version.file% /vx=%version.xpath% /env=%environment% /silent

pause