﻿using System.Collections.Generic;
using System.Linq;
using Atlassian.Jira;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.Support.Infrastructure;
using ProfitOptics.Modules.Support.Models.JiraRestModels;

namespace ProfitOptics.Modules.Support.Services
{
    public class SupportMemoryCacheService : ISupportMemoryCacheService
    {
        private readonly IMemoryCacheHelper _memoryCacheHelper;
        private readonly IJiraService _jiraService;
        private readonly Jira _jira;

        public SupportMemoryCacheService(IMemoryCacheHelper memoryCacheHelper, IJiraService jiraService, SupportSettings settings)
        {
            this._memoryCacheHelper = memoryCacheHelper;
            this._jiraService = jiraService;
            if (_jira == null)
            {
                _jira = Jira.CreateRestClient(settings.SupportJiraUrlBasic, settings.SupportJiraUsername, settings.SupportJiraPassword);
            }
        }

        public List<JiraUserFromApi> GetJiraUsersForProject(string projectKey)
        {
            return _memoryCacheHelper.Get(SupportModuleKeys.Cache.JiraUsers, () => _jiraService.FetchUsersForProject(projectKey), SupportMemoryCacheDefaults.CacheTimeInMinutes);
        }

        public List<Project> GetProjects()
        {
            return _memoryCacheHelper.Get(SupportModuleKeys.Cache.Projects, () => _jiraService.FetchProjects(),
                SupportMemoryCacheDefaults.CacheTimeInMinutes);
        }

        public string GetFirstProjectKey()
        {
            return GetProjects().FirstOrDefault()?.Key;
        }

        public BoardConfiguration GetBoardConfiguration(string projectKey)
        {
            return _memoryCacheHelper.Get(SupportModuleKeys.Cache.ProjectBoardConfiguration(projectKey),
                () => _jiraService.FetchBoardConfiguration(projectKey), SupportModuleConstants.Cache.CacheTime);
        }

        public List<ComponentResult> GetComponentsForProject(string selectedProjectKey)
        {
            return _memoryCacheHelper.Get(SupportModuleKeys.Cache.ComponentsPerProject(selectedProjectKey),
                () => _jiraService.FetchComponents(selectedProjectKey), SupportModuleConstants.Cache.CacheTime);
        }

        public List<Atlassian.Jira.IssueType> GetIssueTypesForProject(string projectKey)
        {
            var types = _memoryCacheHelper.Get(SupportModuleKeys.Cache.IssueTypesPerProject(projectKey),
                () => _jiraService.FetchIssueTypes(projectKey), SupportModuleConstants.Cache.CacheTime);

            return types;
        }

        public List<Atlassian.Jira.IssuePriority> GetPrioritiesForProject(string projectKey)
        {
            var priorities = _memoryCacheHelper.Get(SupportModuleKeys.Cache.PrioritiesPerProject,
                () => _jiraService.FetchPriorities(), SupportModuleConstants.Cache.CacheTime);

            return priorities;
        }
    }
}