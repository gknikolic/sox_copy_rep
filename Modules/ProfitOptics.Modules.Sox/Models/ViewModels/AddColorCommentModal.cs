﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class AddColorCommentModal
    {
    
        public int GCTrayId { get; set; }

        [Required]

        [Display(Name = "Tray")]
        public IEnumerable<SelectListItem> GCTrays { get; set; }
        
        public int GCColorId { get; set; }
        
        [Required]

        [Display(Name = "Color")]
        public IEnumerable<SelectListItem> GCColors { get; set; }
        
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }
        
        public string Description { get; set; }
        
        public bool RequiresResolution { get; set; }
    }
}
