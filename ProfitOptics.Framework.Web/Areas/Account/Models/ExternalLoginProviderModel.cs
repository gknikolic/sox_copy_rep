﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class ExternalLoginProviderModel
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
    }
}
