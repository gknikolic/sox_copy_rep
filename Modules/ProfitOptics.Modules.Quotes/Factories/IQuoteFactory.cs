﻿using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Quotes.Models;

namespace ProfitOptics.Modules.Quotes.Factories
{
    public interface IQuoteFactory
    {
        PagedList<CustomerModel> GetCustomerModels(IDataTablesRequest request, int userId);

        PagedList<ItemModel> GetItemModelsFromPriceListData(IDataTablesRequest request, int quoteId, string plcCode);

        PagedList<ItemModel> GetItemsFromStandingOrderData(IDataTablesRequest request, int quoteId, int userId, int orderNum);

        PagedList<ItemModel> GetQuoteItemsData(IDataTablesRequest request, int quoteId, int userId);

        PagedList<ItemModel> GetItemsData(IDataTablesRequest request, int quoteId, string filter);
    }
}
