﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLResponseModel
    {
        public string Message { get; set; }
        public string Description { get; set; }
        public int RowCount { get; set; }
    }
}
