﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Helpers
{
    public static class Constants
    {
        public const string ETL_SUCCESS_MESSAGE_TITLE = "Success.";

        public const string ETL_CT_MODULE_NAME = "censi_track_module";
        public const string ETL_QB_MODULE_NAME = "quick_books_module";
        public const string ETL_WW_MODULE_NAME = "workwave_module";
        public const string ETL_MONNIT_MODULE_NAME = "imonnit_module";
        public const string REPORTING_MODULE = "reporting_module";

        public const string QBO_SALES_ITEM_LINE_DETAIL = "SalesItemLineDetail";
        public const string QBO_USD_SHORT = "USD";
        public const string QBO_USD_LONG = "United States Dollar";
        public const string QBO_RESULT_MESSAGE_SUCCESS = "Success!";
        public const string QBO_PROVIDER_ABBREVIATION = "PRO";
        public const string QBO_SKU_SUFFIX_PROC = "PROC";
        public const string QBO_SKU_SUFFIX_TRAY = "TRAY";

        public const string COLOR_STATUS_NOTE_SYSTEM_GENERATED_TITLE = "Automatic System-Generated Note";
        public const string GC_SYSTEM_USERNAME = "GC-System";
        public const string GC_PARENT_COMPANY_OTHERS = "Others";
        public const int GC_SYSTEM_ALERT_TIME_TO_LOOK_BACK = -6;

        public const string NO_BARCODE_DATA = "No data";
        public const string COUNT_SHEET_COMPLETE = "Complete";
        public const string COUNT_SHEET_NOT_COMPLETE = "Incomplete";

        public const string CT_ABBREVIATION_TWO_CHAR = "CT";
        public const string CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG = "Location Chg";
        public const string CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER = "Check into Sterilizer";
        public const string CT_STERILIZE_LOCATION_BASE_NAME = "BELIMED AUTOCLAVE #"; 
        public const string CT_CASE_CART_COMPLETE = "Completed";
        public const string CT_CASE_CART_COMPLETE_ID = "118";
        public const string CT_CASE_CART_INCOMPLETE = "Incomplete";
        public const string CT_CASE_CART_INCOMPLETE_ID = "119";
        public const string CT_LOANERS_CHECK_IN_LOCATION = "Loaner Check In";
        public const string CT_LOANERS_CHECK_OUT_LOCATION = "Loaner Check Out";
        public const string CT_CONTAINER_TYPE_ACTUAL_ITEM_TYPE = "C";
        public const string CT_MATCHED_CASE_CONTAINER = "MATCHED-CT-CASE-CONTAINER";
        public const string CT_CASECONTAINER_LOCATION_NAME_NONE_AVAILABLE = "None Available";

        public static string CT_CensitrackApiEndpoint = "http://{0}.censis.net/mvc/api/";
        public const string CT_CENSITRACK_LOGIN_URL = "login/login";
    }
}
