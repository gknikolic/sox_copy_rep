﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Helpers.HtmlHelpers;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.BackgroundTasks.Factories;
using ProfitOptics.Modules.BackgroundTasks.Models;

namespace ProfitOptics.Modules.BackgroundTasks.Components
{
    public class TasksViewComponent : ViewComponent, IHeaderWidget
    {
        private readonly IBackgroundTaskFactory _backgroundTaskFactory;
        private readonly IHttpContextAccessor _contextAccessor;

        public TasksViewComponent(IBackgroundTaskFactory backgroundTaskFactory,
            IHttpContextAccessor contextAccessor)
        {
            _backgroundTaskFactory = backgroundTaskFactory;
            _contextAccessor = contextAccessor;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (!(_contextAccessor.HttpContext.User?.Identity?.IsAuthenticated ?? false))
            {
                return Content(string.Empty);
            }

            List<BackgroundTaskViewModel> activeTasks = await _backgroundTaskFactory
                .GetActiveBackgroundTasksViewModelsAsync(_contextAccessor.HttpContext.User.GetUserId());

            HttpContext.Session.SetObject(SessionKeys.BackgroundTasks, activeTasks);

            return View(PartialViewHelper.BuildModulePartialViewPath(Assembly.GetExecutingAssembly(),
                    "Views/Shared/Components/Tasks/Default.cshtml"), activeTasks);
        }

        public int OrderNumber() => 2;
    }
}