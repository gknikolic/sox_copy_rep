﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Models
{
    public class PasswordResetServiceResult : UserManagementServiceResult
    {
        public string Url { get; set; }
        public string EmailTo { get; set; }

        public static PasswordResetServiceResult Success(string url, string emailTo) => new PasswordResetServiceResult
        {
            Status = UserManagementServiceResultStatus.Success,
            Error = null,
            Url = url,
            EmailTo = emailTo
        };
    }
}
