﻿using Microsoft.AspNetCore.Authentication;
using ProfitOptics.Framework.Web.Areas.Account.Models;
using System.Threading.Tasks;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;

namespace ProfitOptics.Framework.Web.Areas.Account.Services
{
    public interface IAccountManagementService
    {
        Task<ApplicationUser> GetCurrentUserAsync();

        /// <summary>
        /// Gets a string resource message for given message identificator
        /// </summary>
        /// <param name="message">Message enum</param>
        /// <returns>Message as a text</returns>
        string ProcessStatusMessage(ManageMessageId? message);

        /// <summary>
        /// Gets a string resource message for given message identificator
        /// </summary>
        /// <param name="message">Message enum</param>
        /// <returns>Message as a text</returns>
        string ProcessSecurityStatusMessage(ManageMessageId? message);

        /// <summary>
        /// Updates user informations
        /// </summary>
        /// <param name="user">User that we want to update</param>
        /// <param name="model">Model we use to update user</param>
        /// <returns>ServiceActionResult</returns>
        Task<AccountManagementServiceResult> UpdateAccountInfoForCurrentUserAsync(AccountManagementModel model);

        /// <summary>
        /// Disables Google Authenticator for specified user
        /// </summary>
        /// <param name="user">User that we want to disable for</param>
        /// <returns></returns>
        Task DisableGoogleAuthenticatorAsync();

        /// <summary>
        /// Verifies the Google Authenticator response
        /// </summary>
        /// <param name="model">Verification model</param>
        /// <returns>Is verification successful or not</returns>
        bool VerifyGoogleAuthenticator(GoogleAuthenticatorModel model);

        /// <summary>
        /// Updates the user to enable Google Authenticator as 2FA provider
        /// </summary>
        /// <param name="secretKey">Key for Google Authenticator</param>
        /// <param name="user">User</param>
        /// <returns></returns>
        Task<AccountManagementServiceResult> SetAccountGoogleAuthenticatorAsync(string secretKey);

        /// <summary>
        /// Generates a phone number verification token and sends it to user as SMS
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="number">Specified phone number that we want to verify</param>
        /// <returns>Is sending an SMS to the user successful</returns>
        Task<AccountManagementServiceResult> SendPhoneVerificationCodeAsync(string number);

        Task<AccountManagementServiceResult> ChangePhoneNumberAsync(string phoneNumber, string code);
        Task<AccountManagementServiceResult> ToggleTwoFactorAuthenticationAsync();
        Task<GoogleAuthenticatorEnabledResult> IsGoogleAuthenticatorEnabledAsync();
        Task<AccountManagementServiceResult> ChangePasswordAsync(string modelCurrentPassword, string modelNewPassword);
        Task<AccountManagementServiceResult> AddExternalLoginAsync(AuthenticateResult response);
        Task<AccountManagementServiceResult> RemoveLoginAsync(string loginProvider, string providerKey);
        Task<AccountManagementServiceResult> RemovePhoneNumberAsync();
        Task<AccountManagementServiceResult> SetPasswordAsync(string password);
    }
}
