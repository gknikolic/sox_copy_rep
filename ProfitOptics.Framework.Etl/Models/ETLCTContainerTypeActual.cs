﻿using Newtonsoft.Json;
using System;

namespace ProfitOptics.Framework.Etl.Models
{
    public class ETLCTContainerTypeActual
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("parentId")]
        public long ParentId { get; set; }

        [JsonProperty("weight")]
        public decimal? Weight { get; set; }

        [JsonProperty("physicianName")]
        public string PhysicianName { get; set; }

        [JsonProperty("sterilizationMethodName")]
        public string SterilizationMethodName { get; set; }

        [JsonProperty("serviceName")]
        public string ServiceName { get; set; }

        [JsonProperty("serviceId")]
        public long ServiceId { get; set; }

        [JsonProperty("procurementReferenceId")]
        public string ProcurementReferenceId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("parentName")]
        public string ParentName { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("updateTimestamp")]
        public DateTime UpdateTimestamp { get; set; }

        [JsonProperty("updateUserId")]
        public string UpdateUserId { get; set; }

        [JsonProperty("locationElapsed")]
        public string LocationElapsed { get; set; }

        [JsonProperty("caseCart")]
        public string CaseCart { get; set; }

        [JsonProperty("processingLocationName")]
        public string ProcessingLocationName { get; set; }

        [JsonProperty("parentProcessingLocationName")]
        public string ParentProcessingLocationName { get; set; }

        [JsonProperty("parentUsageIntervalUnitOfMeasure")]
        public string ParentUsageIntervalUnitOfMeasure { get; set; }

        [JsonProperty("homeLocationName")]
        public string HomeLocationName { get; set; }

        [JsonProperty("parentHomeLocationName")]
        public string ParentHomeLocationName { get; set; }

        [JsonProperty("parentIntervalUsageCount")]
        public int? ParentIntervalUsageCount { get; set; }

        [JsonProperty("intervalUsageCount")]
        public int? IntervalUsageCount { get; set; }

        [JsonProperty("usage")]
        public string Usage { get; set; }

        [JsonProperty("censisSetLastRequestDate")]
        public DateTime? CensisSetLastRequestDate { get; set; }

        [JsonProperty("lastMaintenance")]
        public DateTime? LastMaintenance { get; set; }

        [JsonProperty("assembly")]
        public string Assembly { get; set; }

        [JsonProperty("storage")]
        public string Storage { get; set; }

        [JsonProperty("rtlsLocationName")]
        public string RtlsLocationName { get; set; }

        [JsonProperty("rtlsLocationUpdateTimestamp")]
        public DateTime? RtlsLocationUpdateTimestamp { get; set; }

        [JsonProperty("isDiscarded")]
        public bool IsDiscarded { get; set; }

        [JsonProperty("priority")]
        public string Priority { get; set; }

        [JsonProperty("shelfLife")]
        public string ShelfLife { get; set; }

        [JsonProperty("notificationCount")]
        public long NotificationCount { get; set; }

        [JsonProperty("expedite")]
        public string Expedite { get; set; }

        [JsonProperty("rigidity")]
        public string Rigidity { get; set; }

        [JsonProperty("parentExpedite")]
        public string ParentExpedite { get; set; }

        [JsonProperty("parentRigidity")]
        public string ParentRigidity { get; set; }

        [JsonProperty("censisSetHistoryBuildStartDate")]
        public DateTime? CensisSetHistoryBuildStartDate { get; set; }

        [JsonProperty("firstAlternateMethodId")]
        public long? FirstAlternateMethodId { get; set; }

        [JsonProperty("firstAlternateMethodName")]
        public string FirstAlternateMethodName { get; set; }

        [JsonProperty("secondAlternateMethodId")]
        public long? SecondAlternateMethodId { get; set; }

        [JsonProperty("secondAlternateMethodName")]
        public string SecondAlternateMethodName { get; set; }

        [JsonProperty("thirdAlternateMethodId")]
        public long? ThirdAlternateMethodId { get; set; }

        [JsonProperty("thirdAlternateMethodName")]
        public string ThirdAlternateMethodName { get; set; }

        [JsonProperty("complexityLevelDescription")]
        public string ComplexityLevelDescription { get; set; }

        [JsonProperty("disposableFlag")]
        public string DisposableFlag { get; set; }

        [JsonProperty("serialNumber")]
        public string SerialNumber { get; set; }

        [JsonProperty("vendorName")]
        public string VendorName { get; set; }
    }
}
