﻿using System;
using ProfitOptics.Modules.Reporting.Attributes;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Services.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class DataBridgeItem
    {
        private IMarginBridgeSessionService _marginBridgeSessionService;

        public DataBridgeItem(IMarginBridgeSessionService marginBridgeSessionService)
        {
            _marginBridgeSessionService = marginBridgeSessionService;
            MBType = _marginBridgeSessionService.Current.MarginBridgeType;
        }

        public MarginBridgeType MBType { get; set; }

        public DataBridgeItem FirstRow { get; set; }

        public bool IsFirstRow { get; set; }

        [FieldTitle("Caption")]
        public string Caption { get; set; }

        [FieldTitle("P1 Sales")]
        public decimal? Period1Sales { get; set; }

        [FieldTitle("P1 Cost")]
        public decimal? Period1RebatedCost { get; set; }

        [FieldTitle("P2 Sales")]
        public decimal? Period2Sales { get; set; }

        [FieldTitle("P2 Cost")]
        public decimal? Period2RebatedCost { get; set; }

        [FieldTitle("End Margin")]
        public decimal? EndMargin => (IsFirstRow) ? EndMarginForFirstRow() : EndMarginForOtherRows();

        [FieldTitle("Start Margin")]
        public decimal? StartMargin => (IsFirstRow) ? StartMarginForFirstRow() : StartMarginForOtherRows();

        #region Private Methods

        #region StartMargin

        private decimal? StartMarginForFirstRow()
        {
            switch (MBType)
            {
                case MarginBridgeType.GPPercentage:
                    return Period1Sales != 0
                        ? (Period1Sales - Period1RebatedCost) / Period1Sales
                        : 0;
                case MarginBridgeType.GPDollarAmount:
                    return Period1Sales != 0
                        ? Period1Sales - Period1RebatedCost
                        : 0;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private decimal? StartMarginForOtherRows()
        {
            switch (MBType)
            {
                case MarginBridgeType.GPPercentage:
                    return FirstRow.Period1Sales - Period1Sales != 0
                        ? (FirstRow.Period1Sales - Period1Sales - (FirstRow.Period1RebatedCost - Period1RebatedCost)) /
                          (FirstRow.Period1Sales - Period1Sales) - FirstRow.StartMargin
                        : 0;
                case MarginBridgeType.GPDollarAmount:
                    return FirstRow.Period1Sales - Period1Sales != 0
                        ? Period1Sales - Period1RebatedCost
                        : 0;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        #endregion

        #region EndMargin
        private decimal? EndMarginForFirstRow()
        {
            switch (MBType)
            {
                case MarginBridgeType.GPPercentage:
                    return Period2Sales != 0
                        ? (Period2Sales - Period2RebatedCost) / Period2Sales
                        : 0;
                case MarginBridgeType.GPDollarAmount:
                    return Period2Sales != 0
                        ? Period2Sales - Period2RebatedCost
                        : 0;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private decimal? EndMarginForOtherRows()
        {
            switch (MBType)
            {
                case MarginBridgeType.GPPercentage:
                    return (FirstRow.Period2Sales - Period2Sales) != 0
                        ? (FirstRow.Period2Sales - Period2Sales - (FirstRow.Period2RebatedCost - Period2RebatedCost)) /
                          (FirstRow.Period2Sales - Period2Sales) - FirstRow.EndMargin
                        : 0;
                case MarginBridgeType.GPDollarAmount:
                    return FirstRow.Period1Sales - Period1Sales != 0
                        ? Period2Sales - Period2RebatedCost
                        : 0;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        #endregion

        #endregion
    }
}