﻿namespace ProfitOptics.Framework.Etl.Models
{
    public enum CustomCheckModelReturnType
    {
        NumberOfBadRows,
        ListOfBadValues
    }
}