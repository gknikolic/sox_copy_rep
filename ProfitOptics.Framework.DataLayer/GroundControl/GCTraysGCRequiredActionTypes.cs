﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCTraysGCRequiredActionTypes
    {
        public int Id { get; set; }
        public int GCTrayId { get; set; }
        public int GCRequiredActionTypeId { get; set; }
        public GCTray GCTray { get; set; }
        public GCRequiredActionType GCRequiredActionType { get; set; }
    }
}
