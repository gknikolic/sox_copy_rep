﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Framework.Models
{
    public class LargeJsonResult : JsonResult
    {
        public LargeJsonResult(object value) : base(value)
        {
            MaxJsonLength = int.MaxValue;
            RecursionLimit = 100;
        }

        public int MaxJsonLength { get; set; }
        public int RecursionLimit { get; set; }

        public override Task ExecuteResultAsync(ActionContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponse response = context.HttpContext.Response;

            if (!string.IsNullOrEmpty(ContentType))
            {
                response.ContentType = ContentType;
            }
            else
            {
                response.ContentType = "application/json";
            }

            if (Value != null)
            {
                response.WriteAsync(JsonConvert.SerializeObject(Value));
            }

            return Task.CompletedTask;
        }
    }
}
