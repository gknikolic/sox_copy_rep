﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCColorStatusTypeConfig : IEntityTypeConfiguration<GCColorStatusType>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCColorStatusType> builder)
        {
            builder.ToTable("GCColorStatusType");
            builder.HasKey(entity => entity.Id);
            builder.Property(e => e.Name).IsRequired();
        }
    }
}
