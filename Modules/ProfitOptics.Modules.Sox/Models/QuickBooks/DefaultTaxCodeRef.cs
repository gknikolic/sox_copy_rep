﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class DefaultTaxCodeRef
    {
        public string Value { get; set; }
    }
}
