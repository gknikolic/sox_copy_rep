﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
    [Table("WWTrackingData")]
    public class WWTrackingData
    {
        public int Id { get; set; }
        public int WWDriverId { get; set; }
        public int WWVehicleId { get; set; }
        public int? WWPodContainerId { get; set; }
        public int TImeInSec { get; set; }
        public int? Status { get; set; }
        public int StatusSec { get; set; }

        public WWDriver WWDriver { get; set; }
        public WWVehicle WWVehicle { get; set; }
        public WWPodContainer WWPodContainer { get; set; }
        public virtual ICollection<WWRouteStep> WWRouteSteps { get; set; }
    }
}
