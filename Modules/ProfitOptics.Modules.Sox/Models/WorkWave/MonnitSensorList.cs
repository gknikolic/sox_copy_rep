﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class MonnitSensorList
	{
		[JsonProperty("Method")]
		public string Method { get; set; }

		[JsonProperty("Result")]
		public List<MonnitSensor> Result { get; set; }
	}
}
