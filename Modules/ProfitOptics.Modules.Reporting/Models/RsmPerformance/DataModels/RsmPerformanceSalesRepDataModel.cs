﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels
{
    public class RsmPerformanceSalesRepDataModel
    {
        public List<SalesChannelQuotaPerformanceDataModel> SalesChannelPerformance { get; set; }
        public List<ProductFamilyQuotaPerformanceDataModel> ProductFamilyPerformance { get; set; }
        public List<ItemCategoryQuotaPerformanceDataModel> ItemCategoryPerformance { get; set; }
        public List<RegionQuotaPerformanceDataModel> RegionQuotaPerformance { get; set; }

        public RsmPerformanceSalesRepDataModel()
        {
            SalesChannelPerformance = new List<SalesChannelQuotaPerformanceDataModel>();
            ProductFamilyPerformance = new List<ProductFamilyQuotaPerformanceDataModel>();
            ItemCategoryPerformance = new List<ItemCategoryQuotaPerformanceDataModel>();
            RegionQuotaPerformance = new List<RegionQuotaPerformanceDataModel>();
        }
    }
}