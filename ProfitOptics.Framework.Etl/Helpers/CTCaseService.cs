﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Linq;
using System.Text;
using Serilog;
using ProfitOptics.Modules.Sox.Services;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices;
using ProfitOptics.Framework.Etl.Models;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using AutoMapper;
using Newtonsoft.Json;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public class CTCaseService : ICTCaseService
    {
        private readonly Entities _entities;
        private readonly IWorkWaveService _workWaveService;
        private readonly IGroundControlService _groundControlService;
        private readonly IMapper _mapper;

        public CTCaseService(Entities entities, IWorkWaveService workWaveService, IGroundControlService groundControlService, IMapper mapper)
        {
            _entities = entities;
            _workWaveService = workWaveService;
            _groundControlService = groundControlService;
            _mapper = mapper;
        }

        public void AddCTCase(CTCase ctCase)
        {
            //_entities.CTCases.Add(ctCase);
            //_entities.SaveChanges();
        }

        public void EditCTCase(CTCase ctCase)
        {
            throw new NotImplementedException();
        }

        public void AddOrEditCTCases(List<CTCase> ctCases)
        {
            var newCases = new List<string>();

            // hardcoding it until cases start having this!
            long facilityId = 1001;

            ctCases.ForEach(ctCase =>
            {
                // CTCaseHistorical:
                var ctCaseHistorical = _mapper.Map<Framework.DataLayer.CTCaseHistorical>(ctCase);
                ctCaseHistorical.CreatedAtUtc = DateTime.UtcNow;
                _entities.CTCaseHistoricals.Add(ctCaseHistorical);
                _entities.SaveChanges();


                if (ctCase.DestinationFacilityId.HasValue)
                {
                    facilityId = ctCase.DestinationFacilityId.Value;
                }

                Log.Information($"Processing CaseReference: {ctCase.CaseReference}, DestinationFacilityId: {ctCase.DestinationFacilityId}");

                var gcCustomer = _entities.GCCustomers.Where(x => x.FacilityId == facilityId.ToString()).FirstOrDefault();

                if (gcCustomer == null)
                {
                    Log.Error($"No GCCustomer found for facilityId: { facilityId }");
                }
                else
                {
                    Log.Information($"Fetched GCCustomer: {gcCustomer.CustomerName}, for ctCase.DestinationFacilityId: {ctCase.DestinationFacilityId}");
                }

                var existingCase = _entities.CTCases.Where(x => x.CaseReference == ctCase.CaseReference).Include("GCCase").FirstOrDefault();

                if (existingCase == null)
                {
                    ctCase.CreatedAtUtc = DateTime.UtcNow;
                    _entities.CTCases.Add(ctCase);
                    _entities.GCCases.Add(new GCCase
                    {
                        CTCase = ctCase,
                        Title = ctCase.CaseReference,
                        GCCustomerId = gcCustomer?.Id,
                        DueTimestamp = ctCase.DueTimestamp
                    });

                    newCases.Add(ctCase.CaseReference);
                    //var wwResponse = _soxService.AddOrderToWWAsync(newCTCase.Entity.Id);
                }
                else
                {
                    existingCase.DueTimestamp = ctCase.DueTimestamp;
                    existingCase.DestinationName = ctCase.DestinationName;
                    existingCase.CaseDoctor = ctCase.CaseDoctor;
                    existingCase.CaseCartId = ctCase.CaseCartId;
                    existingCase.CaseTime = ctCase.CaseTime;
                    existingCase.CartStatusId = ctCase.CartStatusId;
                    existingCase.CaseCartName = ctCase.CaseCartName;
                    existingCase.CaseCartLocationName = ctCase.CaseCartLocationName;
                    existingCase.CaseStatus = ctCase.CaseStatus;
                    existingCase.IsCartComplete = ctCase.IsCartComplete;
                    existingCase.CartStatus = ctCase.CartStatus;
                    existingCase.IsCanceled = ctCase.IsCanceled;
                    existingCase.Shipped = ctCase.Shipped;
                    existingCase.PreviousDueTimestamp = ctCase.PreviousDueTimestamp;
                    existingCase.PreviousDestinationName = ctCase.PreviousDestinationName;
                    existingCase.ScheduleId = ctCase.ScheduleId;
                    existingCase.ExpediteTime = ctCase.ExpediteTime;
                    existingCase.DueInMinutes = ctCase.DueInMinutes;
                    existingCase.DueInHoursAndMinutes = ctCase.DueInHoursAndMinutes;
                    existingCase.PreviousDueInMinutes = ctCase.PreviousDueInMinutes;
                    existingCase.PreviousDueInHoursAndMinutes = ctCase.PreviousDueInHoursAndMinutes;
                    existingCase.Changed = ctCase.Changed;
                    existingCase.Priority = ctCase.Priority;
                    existingCase.DateTimeNow = ctCase.DateTimeNow;
                    existingCase.EstimatedTime = ctCase.EstimatedTime;
                    existingCase.ModifiedAtUtc = DateTime.UtcNow;

                    if (gcCustomer != null && existingCase.GCCase != null)
                    {
                        existingCase.GCCase.GCCustomerId = gcCustomer.Id;
                    }
                }
            });

            _entities.SaveChanges();
        }

        public void AddOrEditCTCaseContainers(List<CTCaseContainer> ctCaseContainers, int serverOffsetInMinsFromUTC, bool? isStopLifecycleHistory = false)
        {
            var ctLocations = _entities.CTLocations.Include(x => x.CTLocationType).ToList();
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            CTCase ctCase = null;
            bool isCaseComplete = false;
            int? gcCaseId = null;
            bool caseBarocdeChanged = false;
            int? gcCustomerId = null;

            if (ctCaseContainers != null && ctCaseContainers.Count > 0)
            {
                var firstCTCaseContainer = ctCaseContainers.FirstOrDefault();

                ctCase = _entities.CTCases.Where(x => x.CaseReference == firstCTCaseContainer.CaseReference).Include(x => x.GCCase).ThenInclude(x => x.GCTrays).FirstOrDefault();
                if (ctCase == null)
                {
                    Log.Error($"No CTCase found for CaseReference: {ctCaseContainers.First().CaseReference}.");
                    throw new Exception();
                }

                if (ctCase.DestinationFacilityId.HasValue)
                {
                    var gcCustomer = _entities.GCCustomers.Where(x => x.FacilityId == ctCase.DestinationFacilityId.ToString()).FirstOrDefault();
                    if (gcCustomer != null)
                    {
                        gcCustomerId = gcCustomer.Id;
                    }
                }

                var ctContainerType = _entities.CTContainerTypes.Where(x => x.LegacyId == firstCTCaseContainer.ContainerTypeId).FirstOrDefault();
                GCVendor vendor = null;
                if (ctContainerType != null && ctContainerType.VendorName != null)
                {
                    vendor = _entities.GCVendors.Where(x => x.Title == ctContainerType.VendorName).FirstOrDefault();

                    if (vendor == null)
                    {
                        vendor = _groundControlService.CreateGCVendor(ctContainerType.VendorId, ctContainerType.VendorName, true, gcCustomerId);
                    }
                    else
                    {
                        var gcChildCompany = _entities.GCChildCompanies.Where(x => x.GCVendorId == vendor.Id).FirstOrDefault();
                        if(gcChildCompany == null)
                        {
                            _entities.GCChildCompanies.Add(new GCChildCompany()
                            {
                                GCParentCompanyId = null,
                                GCCustomerId = gcCustomerId,
                                GCVendorId = vendor.Id,
                                Title = vendor.Title
                            });
                            _entities.SaveChanges();
                        }
                    }
                }

                //var gcCase = _entities.GCCases.Include(x => x.GCTrays).Where(x => x.Id == ctCase.GCCase.Id).FirstOrDefault();
                if (ctCase.GCCase != null)
                {
                    gcCaseId = ctCase.GCCase.Id;
                    if (vendor != null)
                    {
                        ctCase.GCCase.GCVendorId = vendor.Id;
                    }
                }
                ctCase.GCCase.DueTimestamp = ctCase.DueTimestamp;
                _entities.GCCases.Update(ctCase.GCCase);


                isCaseComplete = ctCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE ||
                                ctCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID ||
                                ctCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE ||
                                ctCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID;


                //ctCaseContainers.ForEach(ctCaseContainer =>
                foreach (var ctCaseContainer in ctCaseContainers)
                {
                    Log.Information($"Processing CTCaseContainer: {JsonConvert.SerializeObject(ctCaseContainer)}.");

                    // Offsetting Timestamps to make them UTC:
                    ctCaseContainer.DueTimestamp = ctCaseContainer.DueTimestampOriginal.AddMinutes(minsToAdd);
                    ctCaseContainer.EndTimestamp = (ctCaseContainer.EndTimestampOriginal.HasValue && ctCaseContainer.EndTimestampOriginal.Value > DateTime.MinValue) ? ctCaseContainer.EndTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    ctCaseContainer.UpdateTimestamp = (ctCaseContainer.UpdateTimestampOriginal.HasValue && ctCaseContainer.UpdateTimestampOriginal.Value > DateTime.MinValue) ? ctCaseContainer.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    ctCaseContainer.MultipleBasis = (ctCaseContainer.MultipleBasisOriginal.HasValue && ctCaseContainer.MultipleBasisOriginal.Value > DateTime.MinValue) ? ctCaseContainer.MultipleBasisOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;

                    // creating historical record:
                    var ctCaseContainerHistorical = _mapper.Map<Framework.DataLayer.CTCaseContainerHistorical>(ctCaseContainer);
                    ctCaseContainerHistorical.CreatedAtUtc = DateTime.UtcNow;
                    _entities.CTCaseContainerHistoricals.Add(ctCaseContainerHistorical);
                    _entities.SaveChanges();

                    string updateUser = null;
                    bool isCurrentLocationLoaner = false;
                    bool isLoaner = false;
                    string locationNameIfNotYetAvailable = null;
                    bool isActualCurrentlyAvailable = true;
                    int? currentActualLapCount = null;

                    string barcode = ProfitOptics.Modules.Sox.Helpers.Constants.NO_BARCODE_DATA;
                    if(ctCaseContainer.ContainerId.HasValue)
                    {
                        barcode = ProfitOptics.Modules.Sox.Helpers.Helpers.GenerateGCTrayBarcode(ctCaseContainer.ContainerId);
                    }

                    long containerTypeId = ctCaseContainer.ContainerTypeId;
                    var ctCurrentContainerType = _entities.CTContainerTypes.Where(x => x.LegacyId == containerTypeId).FirstOrDefault();
                    if (ctCurrentContainerType == null)
                    {
                        Log.Error($"No CTContainerType found for LegacyId: {ctCaseContainer.ContainerTypeId}, moving on to next item...");
                    }
                    else
                    { 
                        if (ctCaseContainer.CurrentLocationName != null && ctCaseContainer.CurrentLocationName.StartsWith("Available"))
                        {
                            locationNameIfNotYetAvailable = ctCaseContainer.LocationName;
                            ctCaseContainer.LocationName = "";
                            isActualCurrentlyAvailable = false;
                        }
                        // Quarantine candidate:
                        else if(ctCaseContainer.CurrentLocationName != null && ctCaseContainer.CurrentLocationName.Equals(ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASECONTAINER_LOCATION_NAME_NONE_AVAILABLE))
                        {
                            locationNameIfNotYetAvailable = null;
                            ctCaseContainer.LocationName = null;
                            isActualCurrentlyAvailable = false;
                        }

                        long? containerTypeActualId = ctCaseContainer.ContainerId;
                        CTContainerTypeActual ctContainerTypeActual = null;

                        if (ctLocations.Count == 0)
                        {
                            Log.Error($"No CTLocations found DB");
                            throw new Exception();
                        }
                        int ctLocationTypeId = 0;

                        if (containerTypeActualId.HasValue)
                        {
                            ctContainerTypeActual = _entities.CTContainerTypeActuals.Where(x => x.LegacyId == containerTypeActualId.Value).FirstOrDefault();
                            if (ctContainerTypeActual == null)
                            {
                                Log.Information($"No CTContainerTypeActual found for LegacyId: {ctCaseContainer.ContainerId}.");
                            }
                        }
                        else
                        {
                            Log.Information($"No CTContainerTypeActual allocated (ctCaseContainer.ContainerId = NULL).");
                        }

                        if (!string.IsNullOrWhiteSpace(ctCaseContainer.LocationName) &&  // Now even loaners have location
                            (ctCaseContainer.LocationName.Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION) ||
                            ctCaseContainer.LocationName.Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION)))
                        {
                            isCurrentLocationLoaner = true;
                        }
                        else
                        {
                            // Silly Hack because of CT
                            var ctLocation = _groundControlService.GetCTLocationForLocationName(ctCaseContainer.LocationName);
                            if (ctLocation != null)
                            {
                                ctLocationTypeId = ctLocation.CTLocationTypeId;
                            }
                        }

                        //CTLocationTypeGCLifecycleStatusType ctLocationTypeGCLifecycleStatusType = null;
                        GCLifecycleStatusType gcLifeCycleStatusType = null;

                        // Tray must have an 'actual' (be allocated) in order to have Location/LocationType
                        if (ctContainerTypeActual != null)
                        {
                            var currentHistoryItem = _entities.CTContainerTypeActualHistoryItems.Where(x =>
                                x.CTContainerTypeActualId == ctContainerTypeActual.Id &&
                                ctCaseContainer.UpdateTimestamp.HasValue &&
                                x.UpdateTimestamp.Date == ctCaseContainer.UpdateTimestamp.Value.Date &&
                                x.UpdateTimestamp.Hour == ctCaseContainer.UpdateTimestamp.Value.Hour &&
                                x.UpdateTimestamp.Minute == ctCaseContainer.UpdateTimestamp.Value.Minute &&
                                x.UpdateTimestamp.Second == ctCaseContainer.UpdateTimestamp.Value.Second).FirstOrDefault(); // comparing without miliseconds

                            isLoaner = _entities.CTContainerTypeActualHistoryItems.Any(x =>
                                x.CTContainerTypeActualId == ctContainerTypeActual.Id &&
                                x.LocationElapsedCase != null &&
                                    (x.LocationElapsedCase.ToLower().Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION.ToLower()) ||
                                    x.LocationElapsedCase.ToLower().Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION.ToLower())));

                            if (currentHistoryItem != null)
                            {
                                updateUser = currentHistoryItem.UpdateUserId;
                                currentActualLapCount = currentHistoryItem.ActualLapCount;
                            }
                            else
                            {
                                updateUser = ctContainerTypeActual.UpdateUserId;
                            }

                            if (isCurrentLocationLoaner)
                            {
                                int loanerCode = (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerInbound;
                                if(ctCaseContainer.LocationName.Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION))
                                {
                                    loanerCode = (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerOutbound;
                                }
                                gcLifeCycleStatusType = _entities.GCLifecycleStatusTypes.Where(x => x.Code == loanerCode).FirstOrDefault();
                            }
                            else
                            {
                                if (ctLocationTypeId == 0)
                                {
                                    //Log.Error($"No CTLocation found for LocationName: {ctCaseContainer.LocationName}.");
                                    Log.Information($"WAS an ERROR, now it's info: No CTLocation found for LocationName: {ctCaseContainer.LocationName}.");
                                    //throw new Exception();
                                }

                                if (ctLocationTypeId > 0)
                                {
                                    // Silly Hack because of CT:
                                    var updateTimeStamp = ctCaseContainer.UpdateTimestamp.HasValue ? ctCaseContainer.UpdateTimestamp.Value : DateTime.MinValue;
                                    gcLifeCycleStatusType = _groundControlService.GetGCLifeCycleBasedOnLocationType(updateTimeStamp, ctLocationTypeId, ctContainerTypeActual.Id, isCaseComplete, gcCaseId, ctCase.DueTimestamp, minsToAdd, updateUser);

                                    if (gcLifeCycleStatusType == null)
                                    {
                                        Log.Error($"No GCLifecycleStatusType found for CTLocationTypeId: {ctLocationTypeId}.");
                                        //throw new Exception();
                                    }
                                }
                            }
                        }

                        var existingCaseContainers = _entities.CTCaseContainers.Where(x =>
                            x.CaseReference == ctCase.CaseReference &&
                            x.ContainerTypeId == ctCaseContainer.ContainerTypeId &&
                            //ctCaseContainer.ContainerName != null && ctCaseContainer.ContainerName.Contains(x.ContainerName) &&
                            x.ItemName == ctCaseContainer.ItemName &&
                            x.CaseItemNumber == ctCaseContainer.CaseItemNumber &&
                                (x.ContainerId == ctCaseContainer.ContainerId ||
                                x.ContainerId == null ||
                                x.ContainerId != ctCaseContainer.ContainerId))
                            //  ^ Ambiguous, but this exact order, means, matching first by ActualId, then match if none existing, then take whichever exists (cause ActualId has probably changed)
                            .OrderByDescending(x => x.ContainerId)
                            .ToList();

                        var matchedCaseContainer = existingCaseContainers.FirstOrDefault();

                        // creatig historical record:
                        var matchedCaseContainerHistorical = _mapper.Map<Framework.DataLayer.CTCaseContainerHistorical>(matchedCaseContainer);
                        if (matchedCaseContainer != null && matchedCaseContainerHistorical != null)
                        {
                            matchedCaseContainerHistorical.ScheduleStatusCode = ProfitOptics.Modules.Sox.Helpers.Constants.CT_MATCHED_CASE_CONTAINER;
                            matchedCaseContainerHistorical.CreatedAtUtc = DateTime.UtcNow;
                            _entities.CTCaseContainerHistoricals.Add(matchedCaseContainerHistorical);
                            _entities.SaveChanges();
                        }

                        GCTray gcTray = null;

                        if (matchedCaseContainer == null)
                        {
                            ctCaseContainer.CreatedAtUtc = DateTime.UtcNow;
                            ctCaseContainer.ContainerId = containerTypeActualId;
                            ctCaseContainer.CTCaseId = ctCase.Id;
                            ctCaseContainer.ContainerTypeId = ctCurrentContainerType.LegacyId;
                            ctCaseContainer.CTContainerTypeId = ctCurrentContainerType.Id;

                            if (ctCaseContainer.VendorName == null && vendor != null)
                            {
                                ctCaseContainer.VendorName = vendor.Title;
                            }

                            _entities.CTCaseContainers.Add(ctCaseContainer);

                            gcTray = new Framework.DataLayer.GCTray()
                            {
                                CTContainerTypeId = ctCurrentContainerType.Id,
                                GCCaseId = ctCase.GCCase.Id,
                                CTCaseContainer = ctCaseContainer,
                                Barcode = barcode,
                                CTContainerTypeActualId = ctContainerTypeActual != null ? ctContainerTypeActual.Id : (int?)null,
                                IsActiveStatus = false,
                                IsLoaner = isLoaner,
                                IsActualCurrentlyAvailable = isActualCurrentlyAvailable,
                                GCLifecycleStatusTypeId = gcLifeCycleStatusType != null ? gcLifeCycleStatusType.Id : (int?)null,
                                ActualLapCount = currentActualLapCount
                            };

                            _entities.GCTrays.Add(gcTray);

                            // Adding only LCs if now Induction (inductions are added to previous case), or loaner (Loaner Check In & Loaner Check Out)
                            if (gcLifeCycleStatusType != null &&
                                    //(isCurrentLocationLoaner || 
                                    (isLoaner ||
                                    !ProfitOptics.Modules.Sox.Helpers.Helpers.CurrentStatusIsInduction(gcLifeCycleStatusType.Id)))
                            {
                                var gcTraysLifecycleStatusTypeToAdd = new GCTraysLifecycleStatusTypes()
                                {
                                    GCTray = gcTray,
                                    LifecycleStatusType = gcLifeCycleStatusType,
                                    Timestamp = (ctCaseContainer.UpdateTimestamp.HasValue && ctCaseContainer.UpdateTimestamp.Value > DateTime.MinValue) ? ctCaseContainer.UpdateTimestamp.Value : DateTime.UtcNow,
                                    IsActiveStatus = true,
                                    User = updateUser,
                                    TimestampOriginal = (ctCaseContainer.UpdateTimestampOriginal.HasValue && ctCaseContainer.UpdateTimestampOriginal.Value > DateTime.MinValue) ? ctCaseContainer.UpdateTimestampOriginal.Value : DateTime.MinValue.AddYears(1000)
                                };
                                gcTray.GCLifecycleStatusTypeId = gcLifeCycleStatusType.Id;
                                gcTray.IsActualCurrentlyAvailable = isActualCurrentlyAvailable;
                                _entities.GCTraysLifecycleStatusTypes.Add(gcTraysLifecycleStatusTypeToAdd);
                            }
                        }
                        else
                        {
                            matchedCaseContainer.CTCaseId = ctCase.Id;
                            matchedCaseContainer.ContainerId = containerTypeActualId;
                            matchedCaseContainer.ContainerTypeId = ctCurrentContainerType.LegacyId;
                            matchedCaseContainer.CTContainerTypeId = ctCurrentContainerType.Id;
                            matchedCaseContainer.CaseReference = ctCaseContainer.CaseReference;
                            matchedCaseContainer.CaseCartId = ctCaseContainer.CaseCartId;
                            matchedCaseContainer.CaseCartName = ctCaseContainer.CaseCartName;
                            matchedCaseContainer.DueMinutes = ctCaseContainer.DueMinutes;
                            matchedCaseContainer.CaseTime = ctCaseContainer.CaseTime;
                            matchedCaseContainer.DueTimestampOriginal = ctCaseContainer.DueTimestampOriginal;
                            matchedCaseContainer.DueTimestamp = ctCaseContainer.DueTimestampOriginal.AddMinutes(minsToAdd);
                            matchedCaseContainer.DestinationName = ctCaseContainer.DestinationName;
                            matchedCaseContainer.EndTimestampOriginal = ctCaseContainer.EndTimestampOriginal;
                            matchedCaseContainer.EndTimestamp = (ctCaseContainer.EndTimestampOriginal.HasValue && ctCaseContainer.EndTimestampOriginal.Value > DateTime.MinValue) ? matchedCaseContainer.EndTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                            matchedCaseContainer.CaseDoctor = ctCaseContainer.CaseDoctor;
                            matchedCaseContainer.ContainerName = ctCaseContainer.ContainerName;
                            matchedCaseContainer.ItemName = ctCaseContainer.ItemName;
                            matchedCaseContainer.Status = ctCaseContainer.Status;
                            matchedCaseContainer.ScheduleStatusCode = ctCaseContainer.ScheduleStatusCode;
                            matchedCaseContainer.ScheduleStatus = ctCaseContainer.ScheduleStatus;
                            matchedCaseContainer.StandardTime = ctCaseContainer.StandardTime;
                            matchedCaseContainer.ExpediteTime = ctCaseContainer.ExpediteTime;
                            matchedCaseContainer.ItemCode = ctCaseContainer.ItemCode;
                            matchedCaseContainer.ItemType = ctCaseContainer.ItemType;
                            matchedCaseContainer.ItemTypeName = ctCaseContainer.ItemTypeName;
                            matchedCaseContainer.StatTime = ctCaseContainer.StatTime;
                            matchedCaseContainer.MultipleBasisOriginal = ctCaseContainer.MultipleBasisOriginal;
                            matchedCaseContainer.MultipleBasis = (ctCaseContainer.MultipleBasisOriginal.HasValue && ctCaseContainer.MultipleBasisOriginal.Value > DateTime.MinValue) ? matchedCaseContainer.MultipleBasisOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                            matchedCaseContainer.LocationName = ctCaseContainer.LocationName;
                            matchedCaseContainer.UpdateTimestampOriginal = ctCaseContainer.UpdateTimestampOriginal;
                            matchedCaseContainer.UpdateTimestamp = (ctCaseContainer.UpdateTimestampOriginal.HasValue && ctCaseContainer.UpdateTimestampOriginal.Value > DateTime.MinValue) ? matchedCaseContainer.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                            matchedCaseContainer.ScheduleCartChanged = ctCaseContainer.ScheduleCartChanged;
                            matchedCaseContainer.IsModified = ctCaseContainer.IsModified;
                            matchedCaseContainer.HoldQuantity = ctCaseContainer.HoldQuantity;
                            matchedCaseContainer.ItemQuantity = ctCaseContainer.ItemQuantity;
                            matchedCaseContainer.IsCanceled = ctCaseContainer.IsCanceled;
                            matchedCaseContainer.IsCartComplete = ctCaseContainer.IsCartComplete;
                            matchedCaseContainer.Shipped = ctCaseContainer.Shipped;
                            matchedCaseContainer.CaseStatus = ctCaseContainer.CaseStatus;
                            matchedCaseContainer.StandardTimeInHoursAndMinutes = ctCaseContainer.StandardTimeInHoursAndMinutes;
                            matchedCaseContainer.ExpediteTimeInHoursAndMinutes = ctCaseContainer.ExpediteTimeInHoursAndMinutes;
                            matchedCaseContainer.StatTimeInHoursAndMinutes = ctCaseContainer.StatTimeInHoursAndMinutes;
                            matchedCaseContainer.DueInMinutes = ctCaseContainer.DueInMinutes;
                            matchedCaseContainer.DueInHoursAndMinutes = ctCaseContainer.DueInHoursAndMinutes;
                            matchedCaseContainer.ProcessingFacilityId = ctCaseContainer.ProcessingFacilityId;
                            matchedCaseContainer.LocationFacilityId = ctCaseContainer.LocationFacilityId;
                            matchedCaseContainer.CaseItemNumber = ctCaseContainer.CaseItemNumber;
                            matchedCaseContainer.CaseContainerStatus = ctCaseContainer.CaseContainerStatus;
                            matchedCaseContainer.CurrentLocationName = ctCaseContainer.CurrentLocationName;
                            matchedCaseContainer.VendorName = ctCaseContainer.VendorName;
                            matchedCaseContainer.SerialNumber = ctCaseContainer.SerialNumber;
                            matchedCaseContainer.ModifiedAtUtc = DateTime.UtcNow;

                            if (matchedCaseContainer.VendorName == null && vendor != null)
                            {
                                matchedCaseContainer.VendorName = vendor.Title;
                            }

                            gcTray = _entities.GCTrays.Where(x => x.CTCaseContainerId.HasValue && x.CTCaseContainerId == matchedCaseContainer.Id).FirstOrDefault();
                            //if(existingGCTray == null)
                            //{
                            //    Log.Error($"No GCTray found for CTCaseContainerId: {existingCaseContainer.Id}.");
                            //    throw new Exception();
                            //}
                            if (gcTray == null)
                            {
                                gcTray = new Framework.DataLayer.GCTray()
                                {
                                    CTContainerTypeId = ctCurrentContainerType.Id,
                                    GCCaseId = ctCase.GCCase.Id,
                                    CTCaseContainer = ctCaseContainer,
                                    CTContainerTypeActualId = ctContainerTypeActual == null ? (int?)null : ctContainerTypeActual.Id,
                                    IsActualCurrentlyAvailable = isActualCurrentlyAvailable,
                                    IsActiveStatus = false,
                                    ActualLapCount = currentActualLapCount
                                };
                            }
                            else
                            {
                                // Actual has changed, need to update lifecycles as well!!
                                if(gcTray.CTContainerTypeActualId.HasValue && 
                                    ctContainerTypeActual != null &&
                                        gcTray.CTContainerTypeActualId.Value != ctContainerTypeActual.Id)
                                {
                                    Log.Warning($"Actual has changed for GCTrayID: {gcTray.Id}, was: {gcTray.CTContainerTypeActualId}, now: {ctContainerTypeActual.Id}");
                                    var gcTrayActualHistory = new GCTrayActualHistory()
                                    {
                                        GCTrayId = gcTray.Id,
                                        CTContainerTypeActualId = gcTray.CTContainerTypeActualId.Value,
                                        CreatedAtUtc = DateTime.UtcNow
                                    };
                                    _groundControlService.AddGCTrayActualHistory(gcTrayActualHistory);
                                }

                                if (ctContainerTypeActual != null)
                                {
                                    gcTray.CTContainerTypeActualId = ctContainerTypeActual.Id;
                                }
                                else if (gcTray.CTContainerTypeActualId != null && ctContainerTypeActual == null) // lost the Actual
                                {
                                    gcTray.CTContainerTypeActualId = null;
                                    isActualCurrentlyAvailable = false;
                                }
                            }

                            if (gcTray.Barcode == null || gcTray.Barcode == ProfitOptics.Modules.Sox.Helpers.Constants.NO_BARCODE_DATA || gcTray.Barcode != barcode)
                            {
                                if (gcTray.Barcode != barcode && barcode != ProfitOptics.Modules.Sox.Helpers.Constants.NO_BARCODE_DATA)
                                {
                                    // update barcode in WW
                                    caseBarocdeChanged = true;
                                }
                                gcTray.Barcode = barcode;
                            }

                            if (gcLifeCycleStatusType != null)
                            {
                                var previousActiveLifecycleStatuses = _entities.GCTraysLifecycleStatusTypes.Include(x => x.LifecycleStatusType).Where(x => x.IsActiveStatus == true && x.GCTrayId == gcTray.Id).OrderByDescending(x => x.Timestamp).ToList();

                                GCTraysLifecycleStatusTypes gcTraysLifecycleStatusType = new GCTraysLifecycleStatusTypes()
                                {
                                    GCTray = gcTray,
                                    LifecycleStatusType = gcLifeCycleStatusType,
                                    //Timestamp = DateTime.UtcNow,
                                    Timestamp = (ctCaseContainer.UpdateTimestamp.HasValue && ctCaseContainer.UpdateTimestamp.Value > DateTime.MinValue) ? ctCaseContainer.UpdateTimestamp.Value : DateTime.UtcNow,
                                    IsActiveStatus = true,
                                    User = updateUser,
                                    TimestampOriginal = (ctCaseContainer.UpdateTimestampOriginal.HasValue && ctCaseContainer.UpdateTimestampOriginal.Value > DateTime.MinValue) ? ctCaseContainer.UpdateTimestampOriginal.Value : DateTime.UtcNow.AddMinutes(minsToAdd),
                                };

                                if (previousActiveLifecycleStatuses != null)
                                {
                                    if (previousActiveLifecycleStatuses.Count > 1)
                                    {
                                        Log.Information($"Multiple Active statuses for GCTrayId: {gcTray.Id}, count: {previousActiveLifecycleStatuses.Count}.");
                                        //throw new Exception();
                                    }

                                    previousActiveLifecycleStatuses.ForEach(x =>
                                    {
                                        if (x.LifecycleStatusType.Origin != null && x.LifecycleStatusType.Origin.Equals(ProfitOptics.Modules.Sox.Helpers.Constants.CT_ABBREVIATION_TWO_CHAR))
                                        {
                                            x.IsActiveStatus = false;
                                        }
                                    });

                                    if (gcTray != null && gcTray.Id > 0)
                                    {
                                        isCurrentLocationLoaner = _entities.GCTraysLifecycleStatusTypes.Include(x => x.LifecycleStatusType).Where(x => x.GCTrayId == gcTray.Id).Any(x => x.LifecycleStatusType.Code == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerInbound);
                                    }

                                    if (isCurrentLocationLoaner || !ProfitOptics.Modules.Sox.Helpers.Helpers.CurrentStatusIsInduction(gcLifeCycleStatusType.Id))
                                    {
                                        // Getting latest status:
                                        GCTraysLifecycleStatusTypes previousActiveLifecycleStatus = null;
                                        if (previousActiveLifecycleStatuses.Count > 0)
                                        {
                                            previousActiveLifecycleStatus = previousActiveLifecycleStatuses[previousActiveLifecycleStatuses.Count - 1];

                                            if (previousActiveLifecycleStatus != null)
                                            {
                                                if (previousActiveLifecycleStatus.LifecycleStatusTypeId != gcLifeCycleStatusType.Id)
                                                {
                                                    if (CurrentStatusIsInCTDomain(previousActiveLifecycleStatus.LifecycleStatusType))
                                                    {
                                                        _entities.GCTraysLifecycleStatusTypes.Add(gcTraysLifecycleStatusType);
                                                    }
                                                }
                                                else
                                                {
                                                    previousActiveLifecycleStatus.IsActiveStatus = true;
                                                    previousActiveLifecycleStatus.User = updateUser;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            _entities.GCTraysLifecycleStatusTypes.Add(gcTraysLifecycleStatusType);
                                        }
                                    }
                                }
                                else
                                {
                                    _entities.GCTraysLifecycleStatusTypes.Add(gcTraysLifecycleStatusType);
                                }

                                gcTray.GCLifecycleStatusTypeId = gcLifeCycleStatusType.Id;
                                gcTray.IsActualCurrentlyAvailable = isActualCurrentlyAvailable;
                            }
                            else if (!string.IsNullOrWhiteSpace(locationNameIfNotYetAvailable))
                            {
                                var updateTimeStamp = ctCaseContainer.UpdateTimestamp.HasValue ? ctCaseContainer.UpdateTimestamp.Value : DateTime.MinValue;

                                //gcTray.GCLifecycleStatusTypeId = _groundControlService.SetGCTraysLifeCycle(updateTimeStamp, locationNameIfNotYetAvailable, ctContainerTypeActual.Id, isCaseComplete, gcCaseId, ctCase.DueTimestamp, minsToAdd, updateUser, isActualCurrentlyAvailable);
                                // Setting this to NULL to prevent ORANGE trays from getting LC Value (not sure why was set in the first place???)
                                gcTray.GCLifecycleStatusTypeId = null;
                                gcTray.IsActualCurrentlyAvailable = isActualCurrentlyAvailable;
                            }

                            //if (isCurrentLocationLoaner)
                            //{
                            //    gcTray.IsLoaner = isCurrentLocationLoaner;
                            //}
                            gcTray.IsLoaner = isLoaner;
                        }

                        if (gcTray != null && gcLifeCycleStatusType != null)
                        {
                            _groundControlService.RaiseSkippedRequiredCTStep(gcTray.Id, gcLifeCycleStatusType.Id);
                        }
                    }
                }

                _entities.SaveChanges();

                if (caseBarocdeChanged)
                {
                    try
                    {
                        Log.Information($"Update Barcodes for GCCase: {ctCase.GCCase.Title} has changed barcodes, updating WW now...");
                        _groundControlService.UpdateCaseBarcodesAsync(ctCase.GCCase.Id).GetAwaiter().GetResult();
                        Log.Information($"Update Case Barcodes COMPLETED.");
                    }
                    catch (Exception e)
                    {
                    }
                }

                if (ctCase.GCCase != null)
                {
                    if (!isStopLifecycleHistory.HasValue || !isStopLifecycleHistory.Value)
                    {
                        Log.Information($"STARTED Fetching History Items for Case Reference: {ctCase.GCCase.Title}");
                        // Matching history items:
                        MatchHistoryItems(ctCase.GCCase.Id);
                        Log.Information($"FINISHED Fetching History Items for Case Reference: {ctCase.GCCase.Title}");
                    }

                    _groundControlService.UpdateGCCaseLifecycleStatus(ctCase.GCCase.Id);
                }
            }
        }

        private void MatchHistoryItems(int gcCaseId)
        {
            var gcTrays = _entities.GCTrays.Where(x => x.GCCaseId == gcCaseId).ToList();
            if (gcTrays != null && gcTrays.Count > 0)
            {
                var ctLocationTypeGCLifecycleStatusTypes = _entities.CTLocationTypeGCLifecycleStatusTypes.ToList();
                var deconStageMappedLocationType = ctLocationTypeGCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.DeconStaged).FirstOrDefault();

                if (deconStageMappedLocationType != null)
                {
                    gcTrays.ForEach(gcTray =>
                    {
                        if (gcTray.CTContainerTypeActualId.HasValue && gcTray.CTContainerTypeActualId.Value > 0)
                        {
                            int newLCsFound = 0;

                            var historyItems = _entities.CTContainerTypeActualHistoryItems.Include(x => x.CTLocation).Where(x =>
                                x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value &&
                                (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG || x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER))
                                    .OrderByDescending(x => x.UpdateTimestamp).ToList();

                            var trayLCs = _entities.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == gcTray.Id).ToList();
                            var isLoaner = _entities.GCTraysLifecycleStatusTypes.Include(x => x.LifecycleStatusType).Any(x => x.LifecycleStatusType.Code == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerInbound);
                            gcTray.IsLoaner = isLoaner;

                            if (historyItems != null && historyItems.Count > 0)
                            {
                                Log.Information($"Found {historyItems.Count} history items for GCTrayId: {gcTray.Id}, CTContainerTypeActualId: {gcTray.CTContainerTypeActualId.Value}");
                                var statusesToCheck = new List<CTContainerTypeActualHistoryItem>();
                                bool hasRachedDeconStaged = false;
                                foreach (var hi in historyItems)
                                {
                                    if (hi.CTLocation != null)
                                    {
                                        if (!hasRachedDeconStaged)
                                            statusesToCheck.Add(hi);
                                        if (hi.CTLocation.CTLocationTypeId == deconStageMappedLocationType.CTLocationTypeId)
                                        {
                                            hasRachedDeconStaged = true;
                                        }
                                    }
                                }

                                if (statusesToCheck.Count > 0)
                                {
                                    statusesToCheck.Reverse(); // reversing to start from earliest Timestamp

                                    for (int i = 0; i < statusesToCheck.Count; i++)
                                    {
                                        var statusToCheck = statusesToCheck[i];

                                        if (!trayLCs.Any(x => x.Timestamp == statusToCheck.UpdateTimestamp))
                                        {
                                            var lcStatuType = ctLocationTypeGCLifecycleStatusTypes.Where(x => x.CTLocationTypeId == statusToCheck.CTLocation.CTLocationTypeId).FirstOrDefault();
                                            int lcStatuTypeId = lcStatuType.GCLifecycleStatusTypeId;

                                            if (lcStatuType != null)
                                            {
                                                if(lcStatuType.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly ||
                                                lcStatuType.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Verification)
                                                {
                                                    if(i > 0)
                                                    {
                                                        var previousStatusToCheck = statusesToCheck[i-1];
                                                        var previousLC = ctLocationTypeGCLifecycleStatusTypes.Where(x => x.CTLocationTypeId == previousStatusToCheck.CTLocation.CTLocationTypeId).FirstOrDefault();
                                                        if(previousLC != null)
                                                        {
                                                            if (previousLC.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.AssemblyStaged ||
                                                                previousLC.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Washer)
                                                            {
                                                                lcStatuTypeId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly;
                                                            }
                                                            else if(previousLC.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.CleanStaged)
                                                            {
                                                                lcStatuTypeId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Verification;
                                                            }
                                                        }
                                                    }
                                                }

                                                _entities.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                                {
                                                    LifecycleStatusTypeId = lcStatuTypeId,
                                                    GCTrayId = gcTray.Id,
                                                    IsActiveStatus = false,
                                                    Timestamp = statusToCheck.UpdateTimestamp,
                                                    User = statusToCheck.UpdateUserId,
                                                    TimestampOriginal = statusToCheck.UpdateTimestampOriginal
                                                });
                                                newLCsFound++;
                                            }
                                        }
                                    }

                                    Log.Information($"Found {newLCsFound} unrecorded (old) LC Statuses");
                                    _entities.SaveChanges();
                                }
                            }
                        }
                    });
                }
            }
        }

        public void SettingGCTrayActiveStatus(List<long?> allLegacyIds)
        {
            var idsToIterate = allLegacyIds.Where(x => x.HasValue).Distinct().ToList();

            idsToIterate.ForEach(ctActualsLegacyId =>
            {
                var ctContainerTypeActual = _entities.CTContainerTypeActuals.Where(x => x.LegacyId == ctActualsLegacyId).FirstOrDefault();

                if (ctContainerTypeActual != null)
                {
                    var gcTrays = _entities.GCTrays.Include(x => x.GCCase).ThenInclude(x => x.CTCase).Where(x => x.CTContainerTypeActualId == ctContainerTypeActual.Id).ToList();
                    if (gcTrays != null)
                    {
                        gcTrays.ForEach(gcTray =>
                        {
                            if(gcTray.GCCaseId.HasValue)
                            {
                                gcTray.IsActiveStatus = false;
                            }
                        });
                    }

                    var nextGCTray = gcTrays.Where(x => 
                        x.CTContainerTypeActualId == ctContainerTypeActual.Id && 
                        x.GCCase != null && x.GCCase.CTCase != null && 
                        x.GCCase.CTCase.DueTimestamp > DateTime.UtcNow
                    ).OrderBy(x => x.GCCase.CTCase.DueTimestamp).FirstOrDefault();
                    if(nextGCTray != null)
                    {
                        nextGCTray.IsActiveStatus = true;
                    }
                }
            });

            _entities.SaveChanges();
        }

        private bool CurrentStatusIsInCTDomain(GCLifecycleStatusType gCLifecycleStatusType)
        {
            var result = false;
            var ctLifeCycles = _entities.GCLifecycleStatusTypes.Where(x => x.Origin == ProfitOptics.Modules.Sox.Helpers.Constants.CT_ABBREVIATION_TWO_CHAR).ToList();
            if (ctLifeCycles != null && ctLifeCycles.Count > 0)
            {
                result = ctLifeCycles.Any(x => x.Id == gCLifecycleStatusType.Id);

                if(!result) // make an exception for Received
                {
                    result = gCLifecycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Received;
                }
            }
            return result;
        }

        public void CreateWorkWaveOrdersForCTCases()
        {
            try
			{
                var ctCases = _entities.GCCases.Where(x => x.OutgoingOrderId == null && x.IncomingOrderId == null).ToList();

                if (ctCases != null)
                {
                    ctCases.ForEach(x =>
                    {
                        var wwResponse = _workWaveService.AddOrderToWWAsync(x.Id).Result;
                        Log.Information($"Created WW Order for CaseReference: {x.CTCase.CaseReference}, RequestId: { wwResponse.RequestId }");
                    });
                }
            }
            catch(Exception ex)
			{
                throw ex;
			}
            
        }

        public List<CTCaseContainer> CaseTesting()
        {
            return _entities.CTCaseContainers.Where(x => x.CTCaseId == 815).ToList();
        }

        public CTContainerType GetCTContainerByContainerTypeId(long containerTypeId)
        {
            return _entities.CTContainerTypes.Where(x => x.LegacyId == containerTypeId).FirstOrDefault();
        }

        public ProfitOptics.Framework.DataLayer.CTContainerService GetCTContainerServiceByLegacyId(int serviceId)
        {
            return _entities.CTContainerServices.Where(x => x.LegacyId == serviceId).FirstOrDefault();
        }

        public CTContainerTypeActual GetCTContainerTypeActualByLegacyId(long containerId)
        {
            return _entities.CTContainerTypeActuals.Where(x => x.LegacyId == containerId).FirstOrDefault();
        }
    }
}
