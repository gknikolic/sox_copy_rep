﻿var DateFormat = "mm/dd/yy";

var Common = {
    hasCCEModulePermission: null,
    hasPromotionsModulePermission: null,
    hasCostModulePermission: null,
    hasProjectModulePermission: null,
    hasToolsModulePermission: null,
    hasSalesModulePermission: null,
    hasAdminModulePermission: null
};

var ResponseStatus = {
    error: "error",
    success: "success"
};

var AlertState = {
    error: "error",
    success: "success",
    info: "info"
};

$(document).ready(function () {
    $("#alertBoxWrapper").show();

    $container = $("#AlertContainer").notify();

    $.fn.datepicker.defaults.autoclose = true;
    $.fn.datepicker.defaults.todayHighlight = true;
    $.fn.datepicker.defaults.forceParse = true;

    ////$(".datepicker").datepicker();

    ////$(".datepicker").datepicker().on("show.bs.modal", function (e) {
    ////    e.stopPropagation();
    //});

    //$(".datepicker").datepicker().on("hide.bs.modal", function (e) {
    //    e.stopPropagation();
    //});

    var dates = $(".date")
    $.each(dates, function (i, v) {
        var date = $(this);
        var span = $(this).find("span");
        var input = $(this).find("input");

        span.click(function (e) {
            if (input.is(":disabled"))
                date.datepicker("hide");
        });
    });

    $.validator.addMethod("isElementStatusValid", function (value, element) {
        return $(element).data("status") != "invalid"
    }, "Element value is not valid.");

    $.validator.addMethod("isValidDate", function (value, element) {
        var comp = value.split('/');
        var m = parseInt(comp[0], 10);
        var d = parseInt(comp[1], 10);
        var y = parseInt(comp[2], 10);
        if (y.toString().length < 4) return false;
        var date = new Date(y, m - 1, d);
        if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {
            return true;
        } else {
            return false;
        }
    }, "Enter valid date value (e.g. 01/20/2015)");

    $.validator.addMethod("validRange", function (value, element) {
        var min = Number($(element).attr("min"));
        var max = Number($(element).attr("max"));

        value = Number(value);
        return value >= min && value <= max;
    });

    $.validator.addMethod('positiveIntegerNumber',
    function (value) {
        return Number(value) >= 0 && Math.floor(value) == value;
    }, 'Enter a positive integer number.');

    $.fn.dataTable.ext.errMode = function (settings, helpPage, message) {
        bootbox.alert("Error occurred while processing data. Please reaload page. Details: " + message);
    };

    $.validator.addMethod("validNumberOfDaysInYear", function (value, element) {
        if (value == null)
            return true;

        return !isNaN(parseInt(value)) && isFinite(value) && Number(value) >= 0 && Number(value) <= 365;
    });

    $.validator.addMethod("validPercentValue", function (value, element) {
        if (value == null)
            return true;

        return !isNaN(parseFloat(value)) && isFinite(value) && Number(value) >= -100 && Number(value) <= 100;
    });
});

Common.blockUIDefaultSettings = {
    message: "Loading...",
    overlayCSS: {
        backgroundColor: '#666',
        opacity: 0.5
    },
    allowBodyStretch: true
}

Common.getConfirmOptions = function (message, callback) {
    return {
        title: ViewResJS.Confirm,
        buttons: {
            confirm: { label: ViewResJS.Yes },
            cancel: { label: ViewResJS.No }
        },
        message: "<p style='font-size:14px;'>" + message + "</p>",
        callback: function (result) {
            if (result) {
                callback();
            }
        }
    }
}

Common.adjustTableColumnSizing = function () {
    var tables = $.fn.dataTable.fnTables(true);
    for (var i = 0; i < tables.length; i++) {
        $(tables[i]).dataTable().fnAdjustColumnSizing(false);
    }
}

Common.emptyIfNull = function (obj) {
    if (obj == null)
        return "";

    return obj;
}

Common.preserveTableSelection = function (tableId) {
    $('#' + tableId + ' tbody tr').each(function (index, value) {
        if ($(this).hasClass('row_selected'))
            $('#' + tableId).data("selectedRowIndex", index);
    });
}

Common.selectTableRowIfNeeded = function (tableId) {

    var rowIndex = $('#' + tableId).data("selectedRowIndex");

    if (rowIndex != null && rowIndex != undefined) {

        $('#' + tableId + ' tbody tr').each(function (index, value) {
            $(this).removeClass("row_selected");
        });

        $('#' + tableId + ' tbody tr').each(function (index, value) {
            if (index == rowIndex) {
                $(this).addClass("row_selected");
            }
        });

        $('#' + tableId).data("selectedRowIndex", null)
    }
}

Common.formValidationErrorHandler = function (form, errorMap, errorList) {
    // Clean up any tooltips for valid elements
    $.each(form.validElements(), function (index, element) {
        var $element = $(element);

        if ($element.hasClass("select2")) {
            var value = "select2-" + $element.attr("id") + '-container';
            $element = $("[aria-labelledby='" + value + "']");
        }

        $element.data("title", "") // Clear the title - there is no error associated anymore
            .removeClass("error")
            .tooltip("destroy");
    });

    // Create new tooltips for invalid elements
    $.each(errorList, function (index, error) {
        var $element = $(error.element);

        if ($element.hasClass("select2")) {
            var value = "select2-" + $element.attr("id") + '-container';
            $element = $("[aria-labelledby='" + value + "']");
        }

        $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
            .data("title", error.message)
            .addClass("error")
            .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
    });
}

Common.processResponseStatus = function (data) {
    if (data == '')
        location = Common.rootURL + '/Account/Login';

    if (data.Status == ResponseStatus.error) {
        showalert(AlertState.error, data.Message);
        return ResponseStatus.error;
    }
    if (data.Message)
        showalert(AlertState.info, data.Message);

    return ResponseStatus.success;
}

Common.formatPrice = function (basis, operator, adjustment) {
    var formatedPrice = '';
    if (basis != null && operator != null && adjustment != null) {
        formatedPrice = basis + operator;
        if (basis == '$')
            formatedPrice = basis + (Number(adjustment)).formatMoney(2, '.', ',', '');
        else
            formatedPrice += (adjustment * 100).formatMoney(2, '.', ',', '');
    }
    return formatedPrice;
}

Common.isPriceChanged = function (r) {

    if (r.ProjectType.indexOf("VCM") > -1) {
        //var prevBasis = r.PreviousNewPriceBasis;
        //var prevOperator = r.PreviousNewPriceOperator;
        var prevAdj = r.PreviousNewPriceAdjustment;
        //var newBasis = r.NewPriceBasis;
        //var newOperator = r.NewPriceOperator;
        var newAdj = r.NewPriceAdjustment;
        //if (prevBasis != null && prevBasis != newBasis)
        //    return true;
        //else if (prevOperator != null && prevOperator != newOperator)
        //    return true;
        //else if (prevAdj != null && prevAdj != newAdj)
        //    return true;
        //else
        //    return false;
        //return r.PriceChangePct < 0; //RAPMT-1370 VCM red flag

        //RAPMT-1422
        var result = false;
        if (prevAdj != null)
            result = prevAdj > newAdj;
        return result;
    }
    else
        return false;
}

Common.formatPriceChanged = function (r) {
    var formatedPrice = '';
    var basis = r.PreviousNewPriceBasis != null ? r.PreviousNewPriceBasis : r.NewPriceBasis;
    var operator = r.PreviousNewPriceOperator != null ? r.PreviousNewPriceOperator : r.NewPriceOperator;
    var adjustment = r.PreviousNewPriceAdjustment != null ? r.PreviousNewPriceAdjustment : r.NewPriceAdjustment;
    if (basis != null && operator != null && adjustment != null) {
        formatedPrice = basis + operator;
        if (basis == '$')
            formatedPrice = basis + (Number(adjustment)).formatMoney(2, '.', ',', '');
        else
            formatedPrice += (adjustment * 100).formatMoney(2, '.', ',', '');
    }
    return "Proposed price: " + formatedPrice;
}

Common.getSelectAllOptionsHTML = function () {
    var html = '<div>' +
                    '<div class="form-inline">' +
                        '<input id="selectPageRequests" type="radio" name="selectOption" checked/>' +
                        '<label class="control-label" for="selectPageRequests">' + ViewResJS.SelectPageRequests + '</label>' +
                    '</div>' +
                    '<div class="form-inline">' +
                        '<input id="selectAllRequests" type="radio" name="selectOption"/>' +
                        '<label class="control-label" for="selectAllRequests">' + ViewResJS.SelectAllRequests + ' (' + ViewResJS.SelectAllOptionMessage + ')' + '</label>' +
                    '</div>' +
                '</div>';

    return html;
}

Common.getGeneralSelectAllOptionsHTML = function () {
    var html = '<div>' +
                    '<div class="form-inline">' +
                        '<input id="selectAllItems" type="radio" name="selectOption" checked/>' +
                        '<label class="control-label" for="selectAllItems">' + ViewResJS.SelectAll + '</label>' +
                    '</div>' +
                    '<div class="form-inline">' +
                        '<input id="selectPageItems" type="radio" name="selectOption" />' +
                        '<label class="control-label" for="selectPageItems">' + ViewResJS.SelectPageRequests + '</label>' +
                    '</div>' +
                '</div>';

    return html;
}

Common.formatMoney = function (value) {
    return Common.formatMoney(value, 2);
}

Common.formatMoney = function (value, decimalNumbers) {
    var nvalue = Math.abs(value);

    var toret = "$" + nvalue.formatMoney(decimalNumbers, '.', ',', '');
    if (value < 0)
        toret = "(" + toret + ")";
    return toret;
}

Common.formatNumber = function (value, decimalNumbers) {
    var nvalue = Math.abs(value);

    var toret = nvalue.formatMoney(decimalNumbers, '.', ',', '');
    if (value < 0)
        toret = "(" + toret + ")";
    return toret;
}

Common.formatPercent = function (value, decimal) {
    if (decimal || decimal == 0)
        return Common.formatNumber(value, decimal) + '%';

    return Common.formatNumber(value, 1) + '%';
}

Common.showFilter = function (filterId, evt) {
    //stop table sorting
    evt.stopPropagation();

    Common.hideFilters();

    var left = evt.pageX;
    var top = evt.pageY;
    var width = $('#' + filterId).width();

    left -= width / 2;
    top += 20;

    $('#' + filterId).show();

    $('#' + filterId).offset({ left: left, top: top });

    var inputId = filterId.replace("Container", "InputId");

    $("#" + inputId).focus();

    $("#" + inputId).off("keydown").on("keydown", function (e) {
        var keycode = (event.keyCode ? event.keyCode : event.which);

        if (keycode == 13) {
            e.preventDefault();
            $("#" + filterId).find("button:contains('Filter')").click();
        }
    });
}

Common.hideFilters = function () {
    $('.filter-popover').hide();
}

Common.clearFilter = function (filterInput, tableName) {
    $('#' + filterInput).val('');

    var filterSpan = filterInput.replace("InputId", "Span");
    $('#' + filterSpan).removeClass("active");

    //clear additional filters
    if (filterInput == "ProductFilterInputId") {
        $("#ProductIncludePCSCFilterInputId").prop("checked", "checked");
        $("#ProductIncludeProductFilterInputId").prop("checked", "checked");
    }

    if (filterInput == "CurrentPriceFilterInputId") {
        $("#CurrentPriceIncludeNetFilterInputId").prop("checked", "checked");
        $("#CurrentPriceIncludeDiscountFilterInputId").prop("checked", "checked");
    }

    if (filterInput == "NewPriceFilterInputId") {
        $("#NewPriceIncludeNetFilterInputId").prop("checked", "checked");
        $("#NewPriceIncludeDiscountFilterInputId").prop("checked", "checked");
    }

    if (filterInput == "AdditionalFilterInputId") {
        $("#NoPCSCProductSalesInPastFilterInputId").val("");
        $("#NoTotalSalesForCustomerInPastFilterInputId").val("");
        $("#ContractOrExceptionHasExistedForFilterInputId").val("");
        $("#ContractOrExceptionHasNotBeenModifiedFilterInputId").val("");
    }

    Common.hideFilters();

    $('#' + tableName).DataTable().draw();
}

Common.searchByFilter = function (filterInput, tableName) {

    var filterValue = $('#' + filterInput).val();
    var filterSpan = filterInput.replace("InputId", "Span");

    //check additional filters
    if (!$("#ProductIncludePCSCFilterInputId").is(":checked")
        || !$("#ProductIncludeProductFilterInputId").is(":checked")
        || !$("#CurrentPriceIncludeNetFilterInputId").is(":checked")
        || !$("#CurrentPriceIncludeDiscountFilterInputId").is(":checked")
        || !$("#NewPriceIncludeNetFilterInputId").is(":checked")
        || !$("#NewPriceIncludeDiscountFilterInputId").is(":checked")
        || $("#NoPCSCProductSalesInPastFilterInputId").val() != null
        || $("#NoTotalSalesForCustomerInPastFilterInputId").val() != null
        || $("#ContractOrExceptionHasExistedForFilterInputId").val() != null
        || $("#ContractOrExceptionHasNotBeenModifiedFilterInputId").val() != null)
        var hasInlcudeFilters = true;

    if (hasInlcudeFilters || ($('#' + filterInput).length > 0 && filterValue.length > 0)) {
        $('#' + filterSpan).addClass("active");
    }
    else {
        $('#' + filterSpan).removeClass("active");
    }

    Common.hideFilters();
    $('#' + tableName).DataTable().draw();
}

Common.getTableColumnFilters = function () {
    var inputs = $(".filter-popover :input[type=text]");
    var filters = new Array();

    $.each(inputs, function (i, v) {
        var val = $(this).val();
        var inputId = $(this).attr("id");
        var property = inputId.replace("FilterInputId", "");

        filters.push({
            propertyName: property,
            value: val
        });
    });

    //add additional filters
    filters.push({
        propertyName: "ProductIncludePCSC",
        value: $("#ProductIncludePCSCFilterInputId").is(":checked") //if text is entered then send checkbox value
    });

    filters.push({
        propertyName: "ProductIncludeProduct",
        value: $("#ProductIncludeProductFilterInputId").is(":checked") //if text is entered then send checkbox value
    });

    filters.push({
        propertyName: "CurrentPriceIncludeNet",
        value: $("#CurrentPriceIncludeNetFilterInputId").is(":checked") //if text is entered then send checkbox value
    });

    filters.push({
        propertyName: "CurrentPriceIncludeDiscount",
        value: $("#CurrentPriceIncludeDiscountFilterInputId").is(":checked") //if text is entered then send checkbox value
    });

    filters.push({
        propertyName: "NewPriceIncludeNet",
        value: $("#NewPriceIncludeNetFilterInputId").is(":checked") //if text is entered then send checkbox value
    });

    filters.push({
        propertyName: "NewPriceIncludeDiscount",
        value: $("#NewPriceIncludeDiscountFilterInputId").is(":checked") //if text is entered then send checkbox value
    });

    filters.push({
        propertyName: "NoPCSCProductSalesInPast",
        value: $("#NoPCSCProductSalesInPastFilterInputId").val()
    });

    filters.push({
        propertyName: "NoTotalSalesForCustomerInPast",
        value: $("#NoTotalSalesForCustomerInPastFilterInputId").val()
    });

    filters.push({
        propertyName: "ContractOrExceptionHasExistedFor",
        value: $("#ContractOrExceptionHasExistedForFilterInputId").val()
    });

    filters.push({
        propertyName: "ContractOrExceptionHasNotBeenModified",
        value: $("#ContractOrExceptionHasNotBeenModifiedFilterInputId").val()
    });

    return filters;
}

Common.clearAllFilters = function (tableName) {
    var inputs = $(".filter-popover :input[type=text]");

    $.each(inputs, function (i, v) {
        var inputFieldId = $(this).attr('id');
        var filterSpan = inputFieldId.replace("InputId", "Span");

        $(this).val('');
    });

    $("#" + tableName + " th span").removeClass("active");

    //clear additionaln filters
    $("#ProductIncludePCSCFilterInputId").prop("checked", "checked");
    $("#ProductIncludeProductFilterInputId").prop("checked", "checked");

    $("#CurrentPriceIncludeNetFilterInputId").prop("checked", "checked");
    $("#CurrentPriceIncludeDiscountFilterInputId").prop("checked", "checked");

    $("#NewPriceIncludeNetFilterInputId").prop("checked", "checked");
    $("#NewPriceIncludeDiscountFilterInputId").prop("checked", "checked");

    $("#NoPCSCProductSalesInPastFilterInputId").val("");
    $("#NoTotalSalesForCustomerInPastFilterInputId").val("");
    $("#ContractOrExceptionHasExistedForFilterInputId").val("");
    $("#ContractOrExceptionHasNotBeenModifiedFilterInputId").val("");

    Common.hideFilters();
    $('#' + tableName).DataTable().draw();
    return false;
}

Common.refreshStatus = function () {
    if (Common.hasCCEModulePermission) {
        $.ajax({
            url: Common.rootURL + "/Common/Manage/GetCCEStatus",
            type: "json",
            method: "POST",
            data: {
            },
            success: function (res) {
                if (Common.processResponseStatus(res) == ResponseStatus.error) {
                    return;
                }

                $("#sidebarMenuNewStatus").text(res.Data.NewRequests.formatNumber(0));
                $("#sidebarMenuSAPendingStatus").text(res.Data.SAPendingRequests.formatNumber(0));
                $("#sidebarMenuFPCPendingTStatus").text(res.Data.FPCPendingRequests.formatNumber(0));
                $("#sidebarMenuCPCPendingStatus").text(res.Data.CPCPendingRequests.formatNumber(0));
                $("#sidebarMenuApprovedStatus").text(res.Data.ApprovedRequests.formatNumber(0));
                $("#sidebarMenuExportedStatus").text(res.Data.ExportedRequests.formatNumber(0));
                $("#sidebarMenuClosedStatus").text(res.Data.ClosedRequests.formatNumber(0));
                $("#sidebarMenuRecalledStatus").text(res.Data.RecalledRequests.formatNumber(0));
            }
        });
    }

    if (Common.hasPromotionsModulePermission) {
        $.ajax({
            url: Common.rootURL + "/Common/Manage/GetPromotionsStatus",
            type: "json",
            method: "POST",
            data: {
            },
            success: function (res) {
                if (Common.processResponseStatus(res) == ResponseStatus.error) {
                    return;
                }

                $("#sidebarMenuPromotionsNewStatus").text(res.Data.PromotionsNewRequests.formatNumber(0));
                $("#sidebarMenuPromotionsCreatorPendingStatus").text(res.Data.PromotionsCreatorPendingRequests.formatNumber(0));
                $("#sidebarMenuPromotionsOwnerPendingStatus").text(res.Data.PromotionsOwnerPendingRequests.formatNumber(0));
                $("#sidebarMenuPromotionsFPCPendingStatus").text(res.Data.PromotionsFPCPendingRequests.formatNumber(0));
                $("#sidebarMenuPromotionsCPCPendingStatus").text(res.Data.PromotionsCPCPendingRequests.formatNumber(0));
                $("#sidebarMenuPromotionsApprovedStatus").text(res.Data.PromotionsApprovedRequests.formatNumber(0));
                $("#sidebarMenuPromotionsExportedStatus").text(res.Data.PromotionsExportedRequests.formatNumber(0));
                $("#sidebarMenuPromotionsClosedStatus").text(res.Data.PromotionsClosedRequests.formatNumber(0));
                $("#sidebarMenuPromotionsRecalledStatus").text(res.Data.PromotionsRecalledRequests.formatNumber(0));
            }
        });
    }

    if (Common.hasCostModulePermission) {
        $.ajax({
            url: Common.rootURL + "/Common/Manage/GetCostStatus",
            type: "json",
            method: "POST",
            data: {
            },
            success: function (res) {
                if (Common.processResponseStatus(res) == ResponseStatus.error) {
                    return;
                }

                $("#sidebarMenuCostNewStatus").text(res.Data.CostNewRequests.formatNumber(0));
                $("#sidebarMenuCostPendingCAStatus").text(res.Data.CostPendingCARequests.formatNumber(0));
                $("#sidebarMenuCostPendingCMStatus").text(res.Data.CostPendingCMRequests.formatNumber(0));
                $("#sidebarMenuCostApprovedStatus").text(res.Data.CostApprovedRequests.formatNumber(0));
                $("#sidebarMenuCostClosedStatus").text(res.Data.CostClosedRequests.formatNumber(0));
                $("#sidebarMenuCostExportedStatus").text(res.Data.CostExportedRequests.formatNumber(0));
            }
        });
    }

    if (Common.hasProjectModulePermission) {
        $.ajax({
            url: Common.rootURL + "/Common/Manage/GetProjectStatus",
            type: "json",
            method: "POST",
            data: {
            },
            success: function (res) {
                if (Common.processResponseStatus(res) == ResponseStatus.error) {
                    return;
                }

                $("#sidebarMenuSavedTemplateStatus").text(res.Data.CCESavedTemplates.formatNumber(0));
                $("#sidebarMenuHistoryStatus").text(res.Data.CCEHistoryTemplates.formatNumber(0));

                $("#sidebarMenuPromoSavedTemplateStatus").text(res.Data.PromoSavedTemplates.formatNumber(0));
                $("#sidebarMenuPromoHistoryStatus").text(res.Data.PromoHistoryTemplates.formatNumber(0));
            }
        });
    }

    if (Common.hasSalesModulePermission) {
        $.ajax({
            url: Common.rootURL + "/Common/Manage/GetSalesStatus",
            type: "json",
            method: "POST",
            data: {
            },
            success: function (res) {
                if (Common.processResponseStatus(res) == ResponseStatus.error) {
                    return;
                }

                $("#sidebarMenuBudgetingNotesStatus").text(res.Data.BudgetingUnreadNotes.formatNumber(0));
            }
        });
    }
}

Common.openinnewTab = function (urlToOpen) {
    var win = window.open(Common.rootURL + urlToOpen, '_blank');
    win.focus();
}

function createnotifier(template, vars, opts) {
    return $container.notify("create", template, vars, opts);
}

function showalert(state, text) {
    if (state == AlertState.error) {
        //createnotifier("stickyerror", { title: '<color style="color:Red;">Error</color>', text: text });
        bootbox.alert('Error occurred, please contact administrators. Details: ' + text);
    }
    else if (state == AlertState.success) {
        createnotifier("stickysuccess", { title: '<color style="color:Green;">Success</color>', text: text });
    }
    else if (state == AlertState.info) {
        createnotifier("stickyinfo", { title: 'Information', text: text });
    }
}

function blockUI(msg) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        overlayCSS: {
            backgroundColor: '#666',
            opacity: 0.5
        },
        baseZ: 50000,
        message: msg
    });
}

function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}

var getAoDataVal = function (aoData, v) {
    for (var i = 0; i < aoData.length; i++) {
        if (aoData[i].name == v)
            return aoData[i].value;
    }
};


Number.prototype.formatNumber = function (decimalNumbers) {
    var nvalue = Math.abs(this);

    var toret = nvalue.formatMoney(decimalNumbers, '.', ',', '');
    if (value < 0)
        toret = "(" + toret + ")";
    return toret;
}

Number.getCurrencyValue = function (currency) {
    if (currency.length == 0)
        return currency;
    var ccySign = currency[0];
    if (ccySign == "$") {
        var ccyValue = currency.substr(1, currency.length - 1);
        return Number(ccyValue);
    } else {
        return Number(currency);
    }
}

Number.prototype.round = function (count) {
    var factor = 1;
    for (var i = 0; i < count; i++) {
        factor *= 10;
    }
    return Math.round(this.valueOf() * factor) / factor;
}

Number.prototype.formatMoney = function (c, d, t, w) {
    var sign = this < 0 ? "-" : "";

    var n = Math.abs(this), c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    var k = s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");

    return sign + w + k;
};

Number.prototype.formatPercent = function (c) {
    return c + "%";
};

Number.prototype.formatNumber = function () {
    return this.formatMoney(0, '.', ',', '')
};

String.prototype.formatId = function () {
    return "#" + this;
}

String.prototype.formatJsonDate = function (showtime, format) {

    if (!this || this.substr(0, 4) == "0001")
    { return ""; }

    var date = new Date(parseInt(this.substr(6)));
    var toReturn = date.getMonth() + 1 + "-" + date.getDate() + "-" + date.getFullYear();

    if (!format) {
        toReturn = pad(date.getMonth() + 1, 2) + "-" + pad(date.getDate(), 2) + "-" + date.getFullYear();
    } else if (format == "yyyy/MM/dd") {
        toReturn = date.getFullYear() + "-" + pad(date.getMonth(), 2) + "-" + pad(date.getDate() + 1, 2);
    }

    if (showtime) {
        toReturn = toReturn + " " + pad(date.getHours(), 2) + ":" + pad(date.getMinutes(), 2);
    }
    return toReturn;
};

String.prototype.formatJsonDateNotNormalize = function (showtime) {

    if (!this || this.substr(0, 4) == "0001")
    { return ""; }

    var date = new Date(parseInt(this.substr(6)));
    var nDate = new Date();

    var offsetTime = new Date(date.getTime() + (date.getTimezoneOffset() - nDate.getTimezoneOffset()) * 60 * 1000);

    var toReturn = offsetTime.getUTCMonth() + 1 + "/" + offsetTime.getUTCDate() + "/" + offsetTime.getUTCFullYear();
    if (DateFormat == "mm/dd/yy") {
        toReturn = pad(offsetTime.getUTCMonth() + 1, 2) + "/" + pad(offsetTime.getUTCDate(), 2) + "/" + offsetTime.getUTCFullYear();
    }
    if (showtime) {
        toReturn = toReturn + " " + pad(offsetTime.getUTCHours(), 2) + ":" + pad(offsetTime.getUTCMinutes(), 2);
    }

    return toReturn;
};

Array.prototype.contains = function (obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}

Array.prototype.convertToFormDataFormat = function (arrayName, propertiesToInclude) {
    var array = new Array();
    var $self = this;

    function _createObject(property) {
        var obj = {
            name: property ? arrayName + "[" + i + "]." + property : arrayName + "[" + i + "]",
            required: false,
            type: "text",
            value: property ? $self[i][property] : $self[i]
        }
        array.push(obj);
    }

    for (var i = 0; i < this.length; i++) {
        if (typeof $self[0] == 'object') {
            for (property in this[i]) {
                if (propertiesToInclude) {
                    if (propertiesToInclude.contains(property)) {
                        _createObject(property);
                    }
                } else {
                    _createObject(property);
                }
            }
        } else {
            _createObject();
        }
    }

    return array;
}

var getSortInfoFromDataTable = function (table) {
    var sortedColInfo = { sColumnName: "", sSortColDir: "" }
    if (table != null) {
        var columns = table.fnSettings().aoColumns;
        var sortColIndex = table.dataTable().fnSettings().aaSorting[0][0];
        sortedColInfo.sSortColDir = table.fnSettings().aaSorting[0][1];
        sortedColInfo.sColumnName = columns[sortColIndex].mData;
    }
    return sortedColInfo;
}

jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function (oSettings, iDelay) {
    var _that = this;

    if (iDelay === undefined) {
        iDelay = 250;
    }

    this.each(function (i) {
        $.fn.dataTableExt.iApiIndex = i;
        var
            $this = this,
            oTimerId = null,
            sPreviousSearch = null,
            anControl = $('input', _that.fnSettings().aanFeatures.f);

        anControl.unbind('keyup').bind('keyup', function () {
            var $$this = $this;

            if (sPreviousSearch === null || sPreviousSearch != anControl.val()) {
                window.clearTimeout(oTimerId);
                sPreviousSearch = anControl.val();
                oTimerId = window.setTimeout(function () {
                    $.fn.dataTableExt.iApiIndex = i;
                    _that.fnFilter(anControl.val());
                }, iDelay);
            }
        });

        return this;
    });
    return this;
};

$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
}

jQuery.fn.extend({
    clearValidation: function () {

        function _clearValidation($element) {

            if ($element.hasClass("select2")) {
                var value = "select2-" + $element.attr("id") + '-container';
                $element = $("[aria-labelledby='" + value + "']");
            }

            $element.data("title", "") // Clear the title - there is no error associated anymore
                .removeClass("error")
                .tooltip("destroy");
        }

        var $element = this;

        var inputs = $element.find("input");
        var selects = $element.find("select");
        var selects2 = $element.find(".select2");

        $.each(inputs, function (index, val) {
            _clearValidation($(this));
        });

        $.each(selects, function (index, val) {
            _clearValidation($(this));
        });

        $.each(selects2, function (index, val) {
            _clearValidation($(this));
        });
    }
});
