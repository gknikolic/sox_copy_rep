USE [Sox]

BEGIN

CREATE TABLE [dbo].[GCParentCompanyUsers]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GCParentCompanyId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	FOREIGN KEY ([GCParentCompanyId]) REFERENCES [dbo].[GCParentCompany] ([Id]),
	FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
)

CREATE TABLE [dbo].[GCChildCompanyUsers]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GCChildCompanyId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	FOREIGN KEY ([GCChildCompanyId]) REFERENCES [dbo].[GCChildCompany] ([Id]),
	FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
)

END