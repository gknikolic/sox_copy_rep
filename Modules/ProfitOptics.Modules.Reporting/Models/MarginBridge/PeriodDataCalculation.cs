﻿namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class PeriodDataCalculation
    {
        public decimal Value1 { get; set; }

        public decimal Value2 { get; set; }

        public string LocationBusinessUnitNew { get; set; }

        public string LocationSuperDivision { get; set; }

        public string LocationSubRegionNameNew { get; set; }

        public string LocationAreaName { get; set; }

        public string LocationName { get; set; }

        public string ProductFocusCore { get; set; }

        public string ProductSuperBusinessUnit { get; set; }

        public string ProductSuperGroup { get; set; }

        public string ProductGroup { get; set; }

        public string ProductSubgroup { get; set; }

        public string ProductNumber { get; set; }
    }
}