﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportVendorQuotaConfig : IEntityTypeConfiguration<ReportVendorQuota>
    {
        public void Configure(EntityTypeBuilder<ReportVendorQuota> builder)
        {
            builder.ToTable("ReportProductFamilyQuota");
            builder.HasKey(q => q.Id);

            builder.Property(q => q.VendorId).HasColumnName("ProductFamilyId");
            builder.Property(q => q.Month).IsRequired();
            builder.Property(q => q.Year).IsRequired();
            builder.Property(q => q.Revenue).HasColumnType("decimal(19,4)").IsRequired();

            builder.HasOne(q => q.ReportVendor)
                .WithMany()
                .HasForeignKey(q => q.VendorId)
                .IsRequired();

            builder.HasOne(q => q.ReportHierarchySalesRep)
                .WithMany()
                .HasForeignKey(q => q.SalesRepId)
                .IsRequired();
        }
    }
}