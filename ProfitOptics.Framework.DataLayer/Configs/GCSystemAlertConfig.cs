﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCSystemAlertConfig : IEntityTypeConfiguration<GCSystemAlert>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCSystemAlert> builder)
        {
            builder.ToTable("GCSystemAlert");
            builder.HasKey(entity => entity.Id);
        }
    }
}
