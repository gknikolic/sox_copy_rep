﻿namespace ProfitOptics.Modules.Sox.Models
{
	public enum GeocodeStatusType
	{
		OK,
		LOW_ACCURACY,
		POSTCODE_ISSUE,
		POSTCODE_LEVEL_ACCURACY,
		OUT_OF_TERRITORY_RANGE,
		NOT_FOUND,
		GEOCODING_ERROR,
		GEOCODING_TIMEOUT
	}
}
