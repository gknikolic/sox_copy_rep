using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTInventory")]
    public partial class QTInventory
    {
        public string Company { get; set; }

        public string ItemNum { get; set; }

        public string Plant { get; set; }

        public decimal? OnHand { get; set; }

        public decimal MinQty { get; set; }

        public decimal MaxQty { get; set; }

        public decimal? StdCost { get; set; }

        public decimal? UD19Cost { get; set; }

        public decimal? POOnOrder { get; set; }

        public decimal? MfgOnOrder { get; set; }

        public string ProdCode { get; set; }
    }
}
