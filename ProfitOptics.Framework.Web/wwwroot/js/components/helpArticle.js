﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

let HelpArticle = (function () {

    let baseUrl = window.location.origin;

    let controllerEndpoints = {
        getArticlesUrl: baseUrl + "/HelpArticle/GetHelpArticles",
        getArticleNamesUrl: baseUrl + "/HelpArticle/GetHelpArticleNames",
        getArticleTagsUrl: baseUrl + "/HelpArticle/GetHelpArticleTags",
        addArticleUrl: baseUrl + "/HelpArticle/AddHelpArticle",
        editArticleUrl: baseUrl + "/HelpArticle/EditHelpArticle",
        deleteArticleUrl: baseUrl + "/HelpArticle/DeleteHelpArticle",
        viewAllHelpArticles: "/HelpArticle"
    };

    let settings = {
        shouldReloadPage: false,
        isAdmin: false,
        defaultTakeValue: 5,
        relatedArticleContainer: "#related-article-container",
        filteredArticleContainer: "#filtered-article-container",
        addNewArticleFlag: true,
        noMoreUrlsFlag: false,
        noMoreTagsFlag: false,
        enableResetUrlBtn: false,
        isPreview: false,
        showPreview: true,
        isEdit: false
    };

    let initialArticleState = {
        name: "",
        content: ""
    };

    let dataService = (function() {
        let loadRelatedHelpArticles = function (data, appendTo) {
            $.ajax({
                url: controllerEndpoints.getArticlesUrl,
                type: "POST",
                dataType: "json",
                data: data,
                success: function (response) {
                    userInterface.LoopThroughArticles(response.Data, appendTo);

                    buttonEventBinding.SetShowMoreRelatedArticlesButtonState(response.TotalArticlesCount);
                }
            });
        };
        
        let filterHelpArticles = function (data, appendTo) {
            $.ajax({
                url: controllerEndpoints.getArticlesUrl,
                type: "POST",
                dataType: "json",
                data: data,
                success: function (response) {
                    userInterface.LoopThroughArticles(response.Data, appendTo);

                    buttonEventBinding.SetShowMoreFilteredArticlesButtonState(response.TotalArticlesCount);
                }
            });
        };
        
        let getArticleNames = function () {
            $.ajax({
                url: controllerEndpoints.getArticleNamesUrl,
                type: "GET",
                success: function (articleNames) {
                    autoCompleteNamesInit(articleNames);
                }
            });
        };
        
        let getArticleTags = function () {
            $.ajax({
                url: controllerEndpoints.getArticleTagsUrl,
                type: "GET",
                success: function (articleTags) {
                    autoCompleteTagsInit(articleTags);
                }
            });
        };

        let editArticleAjax = function (data) {
            $.ajax({
                url: controllerEndpoints.editArticleUrl,
                type: "POST",
                dataType: "json",
                data: data,
                success: function (msg) {
                    if (settings.shouldReloadPage || window.location.pathname === controllerEndpoints.viewAllHelpArticles) {
                        window.location.reload();
                        return;
                    }
                    if (msg === "Ok") {
                        initalRequest();
                    }
                }
            });
        };

        let addArticleAjax = function (data) {
            $.ajax({
                url: controllerEndpoints.addArticleUrl,
                type: "POST",
                dataType: "json",
                data: data,
                success: function (msg) {
                    if (window.location.pathname === controllerEndpoints.viewAllHelpArticles) {
                        window.location.reload();
                        return;
                    }
                    if (msg === "Ok") {
                        initalRequest();
                    }
                }
            });
        };

        let deleteArticleAjax = function () {
            let data = {
                id: $("#article-id").val()
            };

            $.ajax({
                url: controllerEndpoints.deleteArticleUrl,
                type: "POST",
                dataType: "json",
                data: data,
                success: function (msg) {
                    if ( window.location.pathname === controllerEndpoints.viewAllHelpArticles) {
                        window.location.reload();
                        return;
                    }
                    if (msg === "Ok") {
                        initalRequest();
                    }
                }
            });
        };

        let loadInitialHelpArticles = function () {
            userInterface.ClearRelatedArticles();

            let data = {
                currentUrl: window.location.pathname,
                skip: 0,
                take: settings.defaultTakeValue
            };

            loadRelatedHelpArticles(data, settings.relatedArticleContainer);
        };

        let loadMoreRelatedHelpArticles = function () {
            let data = {
                currentUrl: window.location.pathname,
                skip: $('#related-article-container  li').length,
                take: settings.defaultTakeValue
            };

            loadRelatedHelpArticles(data, settings.relatedArticleContainer);
        };

        let loadFilteredHelpArticles = function () {
            userInterface.ClearFilteredArticles();

            let data = {
                searchValue: $("#help-article-search").val(),
                skip: $('#filtered-article-container  li').not('li:first').length,
                take: settings.defaultTakeValue
            };

            filterHelpArticles(data, settings.filteredArticleContainer);
        };

        let loadMoreFilteredHelpArticles = function () {
            let data = {
                searchValue: $("#help-article-search").val(),
                skip: $('#filtered-article-container  li').not('li:first').length,
                take: settings.defaultTakeValue
            };

            filterHelpArticles(data, settings.filteredArticleContainer);
        };

        
        let editHelpArticle = function () {
            let relatedUrl = "";

            $("#related-urls div").each(function(index, url) {
                if (relatedUrl === "") {
                    relatedUrl += url.getAttribute("data-url");
                }
                else {
                    relatedUrl += ',' + url.getAttribute("data-url");
                }
            });

            let data = {
                Id: $("#article-id").val(),
                Name: $("#article-name").val(),
                Content: CKEDITOR.instances["textarea-editor"].getData(),
                RelatedUrls: relatedUrl,
                Tags: getTagString()
            };

            editArticleAjax(data);
        };

        let addNewArticle = function () {
            let relatedUrl = "";

            if ($("#related-to-this-url").prop("checked")) {
                relatedUrl = window.location.pathname;
            }
            
            let articleName = $("#article-name").val();

            let data = {
                Id: 0,
                Name: articleName,
                Content: CKEDITOR.instances["textarea-editor"].getData(),
                RelatedUrls: relatedUrl,
                Tags: getTagString()
            };

            addArticleAjax(data);
        };
        
        let getTagString = function () {
            let articleTagDivs = $("#article-tags > div").not("a");

            let tagStr = "";

            $.each(articleTagDivs, function(i, tag) {
                tagStr += tag.innerText.substring(0, tag.innerText.length - 1) + ',';
            });

            tagStr = tagStr.substring(0, tagStr.length - 1);

            return tagStr;
        };

        let autoCompleteNamesInit = function (articleNames) {
            $("#help-article-search").autoComplete({
                minChars: 1,
                source: function (term, suggest) {
                    term = term.toLowerCase();
                    var choices = articleNames;
                    var matches = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) {
                            matches.push(choices[i]);
                        }
                    suggest(matches);
                },
                onSelect: function (e, term, item) {
                    console.log("selected");
                }
            });
        };

        let autoCompleteTagsInit = function (articleTags) {
            $("#tag-name-input").autoComplete({
                minChars: 1,
                source: function (term, suggest) {
                    term = term.toLowerCase();
                    var choices = articleTags;
                    var matches = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) {
                            matches.push(choices[i]);
                        }
                    suggest(matches);
                },
                onSelect: function (e, term, item) {
                    console.log("selected");
                }
            });
        };

        let initalRequest = function () {
            loadInitialHelpArticles();
            loadFilteredHelpArticles();
            getArticleNames();
        };

        return {
            Init: initalRequest,
            LoadFilteredArticles: loadFilteredHelpArticles,
            LoadMoreRelatedArticles: loadMoreRelatedHelpArticles,
            LoadMoreFilteredArticles: loadMoreFilteredHelpArticles,
            EditArticle: editHelpArticle,
            AddArticle: addNewArticle,
            DeleteArticle: deleteArticleAjax,
            GetArticleTags: getArticleTags,
            GetTagString: getTagString
        };
    })();  

    let userInterface = (function() {
        let addNewArticleSetup = function () {
            dataService.GetArticleTags();

            settings.isEdit = false;
            settings.isPreview = false;

            $("#preview-article-btn").removeClass("fa-edit").addClass("fa-search");
            $("#preview-article-btn").attr("title", "Preview");
            $("#delete-article-btn").hide();
            $("#delete-article-confirm").hide();
            $("#save-article-changes").prop("disabled", "disabled");
            $("#edit-article-btn").hide();
            $("#article-name").show();
            $("#article-name").val("");

            $("#preview-article-btn").show();
            $("#tag-name-input").val("");
            $("#related-url-content").show();
            $("#article-tag-container").show();
            $("#article-name-lbl").text("");
            $("#article-form").show();
            $("#article-modal-footer").show();
            $("#article-id").val("");
            $("#related-urls").empty();
            $("#reset-related-urls").hide();
            $("#reset-related-urls").prop("disabled","disabled");
            $("#current-article-content").empty();
            $("#article-tags").empty();
            if (settings.isAdmin) {
                CKEDITOR.instances["textarea-editor"].setData("");
            }
            $("#related-to-this-url").prop("checked", false);
            checkIfNoMoreRelatedUrls();
            buttonEventBinding.SetSaveFunctionState(settings.addNewArticleFlag);
            addCheckboxOnChange();
        };
        
        let addArticleForEditSetup = function (article, shouldReloadPage) {
            dataService.GetArticleTags();

            saveInitalArticleState(article);

            settings.shouldReloadPage = shouldReloadPage;

            settings.isEdit = true;
            settings.isPreview = false;

            $("#preview-article-btn").removeClass("fa-edit").addClass("fa-search");
            $("#preview-article-btn").attr("title", "Preview");
            $("#save-article-changes").prop("disabled", "");

            $("#article-id").val(article.Id);
            //editor buttons
            $("#preview-article-btn").hide();
            $("#edit-article-btn").show();
            $("#tag-name-input").val("");

            //form controls
            $("#delete-article-confirm").hide();
            buttonEventBinding.DeleteArticleButtonInit();
            $("#delete-article-btn").show();
            $("#article-tag-container").hide();
            $("#reset-related-urls").show();
            $("#reset-related-urls").prop("disabled", "disabled");
            $("#article-modal-footer").hide();
            $("#article-name").val(article.Name);
            $("#article-name").hide();
            $("#article-name-lbl").show();
            $("#article-name-lbl").text(article.Name);
            if (checkIfArticleAppliesToThisUrl(article.RelatedUrls)) {
                $("#related-to-this-url").prop("checked", true);
            }
            else {
                $("#related-to-this-url").prop("checked", false);
            }
            $("#current-article-content").empty();
            $("#current-article-content").append(article.Content);
            $("#current-article-content").show();
            if (settings.isAdmin) {
                CKEDITOR.instances["textarea-editor"].setData(article.Content);
            }
            $("#article-form").hide();
            buttonEventBinding.SetSaveFunctionState(!settings.addNewArticleFlag);
            addUrlsAndTagsToModal(article);
            $("#related-url-content").hide();
        };

        let loopThroughArticles = function (articles, appendTo) {
            $(appendTo).empty();
            if (articles.length === 0) {
                $(appendTo).append("<li style=\"margin-left:15px\">There are no related articles.");
                return;
            }

            $.each(articles, function (i, elem) {
                addHelpArticleToView(elem, appendTo);
            });
        };

        let addHelpArticleToView = function (article, appendTo) {
            // the dates are in "/Date(1245398693390)/" format, and have to be parsed
            var jsonDate = article.ModifiedDate === null || article.ModifiedDate === "" ? article.StartDate : article.ModifiedDate;

            var regex = /-?\d+/;
            var m = regex.exec(jsonDate);

            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

            var date = new Date(parseInt(m[0]));

            var dateStr = monthNames[date.getMonth()] + "/" + date.getDate() + "/" + date.getFullYear();

            var articleHtml = "<li id=\"help-article-element" + article.Id + "\">" +
                                    "<a href=\"#\" data-toggle=\"modal\" data-target=\".bs-example-modal-lg\">" +
                                        "<i class=\"menu-icon fa  fa-flag bg-red\"></i>" +
                                        "<div class=\"menu-info\">" +
                                            "<h4 class=\"control-sidebar-subheading\">" + article.Name + "</h4>" +
                                            "<p>Last updated on " + dateStr  + "</p>" +
                                        "</div>" +
                                    "</a>" +
                                "</li>";

            $(appendTo).append(articleHtml);

            addOnClickEventToArticle(article, appendTo);
        };

        let addOnClickEventToArticle = function (article, appendTo) {
            settings.enableResetUrlBtn = false;

            $(appendTo + " #help-article-element" + article.Id).unbind();

            $(appendTo + " #help-article-element" + article.Id).on("click", function() {
                addArticleForEditSetup(article, false);
            });
        };

        let saveInitalArticleState = function (article) {
            initialArticleState.name = article.Name;
            initialArticleState.content = article.Content;
        };

        let loadInitialArticleState = function () {
            //label with name
            $("#article-name-lbl").text(initialArticleState.name);

            //input with name
            $("#article-name").empty();
            $("#article-name").val(initialArticleState.name);

            //content container
            $("#current-article-content").empty();
            $("#current-article-content").append(initialArticleState.content);
        };   

        let addUrlsAndTagsToModal = function (article) {
            let urls = article.RelatedUrls.split(',');

            let tags = article.Tags === null ? null : article.Tags.split(',');

            loopThroughUrls(urls, article.Id);
            loopThroughTags(tags, article.Id);

            addCheckboxOnChange(urls, article.Id);

            buttonEventBinding.ResetUrlsButtonInit(urls, article.Id);
        };

        let loopThroughUrls = function (urls, articleId) {
            $("#related-urls").empty();

            if (urls.length === 0) {
                $("#related-urls").height("25px");
                $("#related-urls").append("<p>This article won't be related to any URL.</p>");
            }
            else {
                $.each(urls, function(index, url) {
                    let id = articleId + "_" + index;
                    appendUrlToModal(id, url);
                });
            }
        };

        let appendUrlToModal = function (id, url) {
            let relatedUrl = "<div id=\"" + id + "\" data-url=\"" + url + "\" class=\"related-url-container\">" + url + "<a class=\"tag-container related-url-close\">&times;</a></div>";

            if (settings.noMoreUrlsFlag) {
                $("#related-urls").empty();
                $("#related-urls").height(50);
                settings.noMoreUrlsFlag = false;
            }

            $("#related-urls").append(relatedUrl);

            if (url === window.location.pathname) {
                $("#related-to-this-url").prop("checked", true);
            }

            addClickEventToRelatedUrl(id);
        };

        let addClickEventToRelatedUrl = function (id) {
            let selector = "#" + id + " a.related-url-close";

            $(selector).unbind("click");
            $(selector).on("click", function() {
                if ($("#" + id).attr("data-url") === window.location.pathname) {
                    $("#related-to-this-url").prop("checked", false);
                }

                $("#" + id).remove();

                if (!settings.enableResetUrlBtn) {
                    $("#reset-related-urls").prop("disabled", "");
                    settings.enableResetUrlBtn = true;
                }

                checkIfNoMoreRelatedUrls();
            });
        };

        let loopThroughTags = function (tags, articleId) {
            $("#article-tags").empty();

            if (tags === null || tags.length === 0) {
                $("#article-tags").height("25px");
                $("#article-tags").append("<p>This article won't have any tags associated.</p>");
            }
            else {
                $.each(tags, function(index, tag) {
                    let id = articleId + "tag_" + index;
                    appendTagToModal(id, tag);
                });
            }
        };

        let appendTagToModal = function (id, tag) {
            if (id === undefined || id === "") {
                id = Date.now();
            }

            let tagHtml = "<div id=\"" + id + "\" class=\"related-url-container\">" + tag + "<a class=\"tag-container tag-close\">&times;</a></div>";

            if (settings.noMoreTagsFlag) {
                $("#article-tags").empty();
                $("#article-tags").height(50);
                settings.noMoreTagsFlag = false;
            }

            $("#article-tags").append(tagHtml);

            addClickEventToTag(id);
        };

        let addClickEventToTag = function (id) {
            let selector = "#" + id + " a.tag-close";
            $(selector).unbind();
            $(selector).on("click", function() {
                $("#" + id).remove();

                checkIfNoMoreTags();
            });
        };
        
        let addCheckboxOnChange = function () {
            let checkbox = $("#related-to-this-url");

            checkbox.unbind();

            checkbox.change(function() {
                checkBoxOnChange();
            });
        };

        let checkBoxOnChange = function () {
            let currentUrl = window.location.pathname;

            let checkbox = $("#related-to-this-url");

            let foundFlag = false;

            let idForRemoval = "";

            $("#related-urls div").each(function(index, elem) {
                if (elem.getAttribute("data-url") === currentUrl) {
                    foundFlag = true;
                    idForRemoval = elem.id;
                }
            });

            if (checkbox.prop("checked") && !foundFlag) {
                appendUrlToModal(Date.now(), currentUrl);

                if (settings.relatedUrlsCount === $("#related-urls > div").length) {
                    $("#reset-related-urls").prop("disabled", "disabled");
                    settings.enableResetUrlBtn = false;
                }
            }
            else if (!checkbox.prop("checked") && foundFlag) {
                $("#" + idForRemoval).remove();

                if (!settings.enableResetUrlBtn) {
                    $("#reset-related-urls").prop("disabled", "");
                    settings.enableResetUrlBtn = true;
                }

                checkIfNoMoreRelatedUrls();
            }
        };

        let checkIfNoMoreRelatedUrls = function () {
            if ($("#related-urls div").length === 0) {
                $("#related-urls").height("25px");
                $("#related-urls").append("<p>This article won't be related to any URL.</p>");
                settings.noMoreUrlsFlag = true;
            }
        };

        let checkIfNoMoreTags = function () {
            if ($("#article-tags > div").length === 0) {
                $("#article-tags").height("25px");
                $("#article-tags").append("<p>This article won't have any tags associated.</p>");
                settings.noMoreTagsFlag = true;
            }
        };

        let clearRelatedArticles = function () {
            $('#related-article-container  li').remove();
        };

        let clearFilteredArticles = function () {
            $('#filtered-article-container  li').not('li:first').remove();
        };

        let checkIfArticleAppliesToThisUrl = function (relatedUrls) {
            let urls = relatedUrls.split(',');

            if (urls.indexOf(window.location.pathname) === -1) {
                return false;
            }

            return true;
        };

        let htmlEditorInBootstrapModalInit = function () {
            $.fn.modal.Constructor.prototype.enforceFocus = function () {
                var $modalElement = this.$element;

                $(document).on('focusin.modal', function (e) {
                    var $parent = $(e.target.parentNode);
                    if ($modalElement[0] !== e.target && !$modalElement.has(e.target).length
                        // add whatever conditions you need here:
                        &&
                        !$parent.hasClass('cke_dialog_ui_input_select') && !$parent.hasClass('cke_dialog_ui_input_text')) {
                        $modalElement.focus();
                    }
                });
            };
        };

        let articleNameBlurInit = function () {
            $("#article-name").unbind();
            $("#article-name").blur(articleNameBlurFunc);
        };

        let articleNameBlurFunc = function () {
            if ($("#article-name").val() === "") {
                $("#save-article-changes").prop("disabled", "disabled");
            }
            else {
                $("#save-article-changes").prop("disabled", "");
            }
        };

        let editorBlurInit = function () {
            var editor = CKEDITOR.instances["textarea-editor"];
            if (editor != null) {
                editor.on("blur", editBlurFunc);
            }
        };

        let editBlurFunc = function () {
            if (CKEDITOR.instances["textarea-editor"].getData() === "" || CKEDITOR.instances["textarea-editor"].getData() === null) {
                $("#save-article-changes").prop("disabled", "disabled");
            }
            else {
                $("#save-article-changes").prop("disabled", "");
            }
        };

        let tagInputInit = function () {
            $("#tag-name-input").unbind();

            $("#tag-name-input").keydown(function(e) {
                let tagName = $("#tag-name-input").val();

                if (e.keyCode !== 13) {
                    return;
                }

                if (!checkIfTagExists(tagName)) {
                    appendTagToModal($("#article-id").val(), tagName);
                }

                $("#tag-name-input").val("");
            });
        };

        let checkIfTagExists = function (tagName) {
            let tags = dataService.GetTagString();

            return tags.indexOf(tagName) !== -1 ? true : false;
        };

        let toggleDeleteConfirmation = function () {
            $("#delete-article-confirm").toggle({
                duration: 500
            });
        };

        let init = function () {
            htmlEditorInBootstrapModalInit();
            if (settings.isAdmin) {
                articleNameBlurInit();
                tagInputInit();
                editorBlurInit();
            };
        };

        return {
            ToggleDeleteConfirmation: toggleDeleteConfirmation,
            AddNewArticleInit: addNewArticleSetup,
            AddArticleToView: addHelpArticleToView,
            ClearRelatedArticles: clearRelatedArticles,
            ClearFilteredArticles: clearFilteredArticles,
            CheckIfArticleAppliesToThisUrl: checkIfArticleAppliesToThisUrl,
            LoopThroughArticles: loopThroughArticles,
            LoopThroughUrls: loopThroughUrls,
            LoadInitalArticleState: loadInitialArticleState,
            Display: addArticleForEditSetup,
            Init: init
        };
    })();

    let buttonEventBinding = (function() {
        let toggleEditControls = function () {
            //article edit controls
            $("#article-form").toggle();
            $("#article-modal-footer").toggle();
            $("#related-url-content").toggle();
            $("#current-article-content").toggle();
            $("#article-tag-container").toggle();
            
            $("#preview-article-btn").removeClass("fa-edit").addClass("fa-search");
            $("#preview-article-btn").attr("title", "Preview");
            $("#preview-article-btn").show();

            if (settings.isEdit) {
                $("#edit-article-btn").hide();
            }

            $("#article-name").toggle();
            $("#article-name-lbl").toggle();

            userInterface.LoadInitalArticleState();
        };

        let previewArticle = function () {
            let clearfix = "<div style=\"clear: both;\"></div>";
            let previewData = CKEDITOR.instances["textarea-editor"].getData();

            //hide edit form
            $("#article-form").toggle();
            $("#article-modal-footer").toggle();
            $("#related-url-content").toggle();
            $("#article-name").toggle();
            $("#article-tag-container").toggle();

            //show preview name
            $("#article-name-lbl").text($("#article-name").val());

            //change button display & icons
            //show preview

            if (!settings.isPreview) {
                $("#preview-article-btn").removeClass("fa-search").addClass("fa-edit");
                $("#preview-article-btn").attr("title", "Edit");

                $("#article-name-lbl").show();
                $("#current-article-content").empty();
                $("#current-article-content").append(previewData);
                $("#current-article-content").append(clearfix);
                $("#current-article-content").show();

                if (settings.isEdit) {
                    $("#edit-article-btn").hide();
                }

                settings.isPreview = true;
            }
            else {
                $("#preview-article-btn").removeClass("fa-edit").addClass("fa-search");
                $("#preview-article-btn").attr("title", "Preview");

                $("#article-name-lbl").hide();
                $("#current-article-content").empty();
                $("#current-article-content").hide();

                settings.isPreview = false;
            }
        };

        let editArticleButtonInit = function () {
            $("#edit-article-btn").unbind();

            $("#edit-article-btn").click(function() {
                toggleEditControls();
            });
        };

        let previewArticleButtonInit = function () {
            $("#preview-article-btn").unbind();

            $("#preview-article-btn").click(function() {
                previewArticle();
            });
        };

        let showMoreButtonInit = function () {
            $("#show-more-articles-btn").unbind();

            $("#show-more-articles-btn").on("click", function(e) {
                e.preventDefault();
                e.stopPropagation();

                dataService.LoadMoreRelatedArticles();
            });
        };

        let showMoreFilteredButtonInit = function () {
            $("#show-more-filtered-articles-btn").hide();

            $("#show-more-filtered-articles-btn").unbind();

            $("#show-more-filtered-articles-btn").on("click", function(e) {
                e.preventDefault();
                e.stopPropagation();

                dataService.LoadMoreFilteredArticles();
            });
        };

        let setShowMoreRelatedArticlesButtonState = function (articleCount) {
            let displayedArticleCount = $('#related-article-container > li').length;

            setButtonState(articleCount, displayedArticleCount, "#show-more-articles-btn");
        };

        let setShowMoreFilteredArticlesButtonState = function (articleCount) {
            let displayedArticleCount = $('#filtered-article-container > li').length;

            setButtonState(articleCount, displayedArticleCount, "#show-more-filtered-articles-btn");
        };

        let setButtonState = function (articleCount, displayedArticleCount, buttonId) {
            if (articleCount === 0) {
                $(buttonId).hide();
                return;
            }

            if (articleCount === displayedArticleCount) {
                setTimeout(function() {
                    $(buttonId).hide();
                }, 100);
            }
            else {
                setTimeout(function() {
                    $(buttonId).show();
                }, 100);
            }
        };

        let setSaveFunctionState = function (functionFlag) {
            $("#save-article-changes").unbind();

            $("#save-article-changes").click(function() {
                if (functionFlag) {
                    dataService.AddArticle();
                }
                else {
                    dataService.EditArticle();
                }
            });
        };

        let addArticleButtonInit = function () {
            $("#add-new-article-btn").unbind();

            $("#add-new-article-btn").on("click", function() {
                userInterface.AddNewArticleInit();
            });
        };

        let searchButtonInit = function () {
            $("#help-article-search-btn").unbind();

            $("#help-article-search-btn").on("click", function(e) {
                e.preventDefault();
                e.stopPropagation();

                dataService.LoadFilteredArticles();
            });
        };

        let saveArticleButtonInit = function (functionFlag) {
            setSaveFunctionState(functionFlag);
        };

        let resetUrlsButtonInit = function (urls, articleId) {
            $("#reset-related-urls").unbind();

            $("#reset-related-urls").click(function() {
                userInterface.LoopThroughUrls(urls, articleId);
                
                $("#reset-related-urls").prop("disabled", "disabled");
                settings.enableResetUrlBtn = false;
            });
        };

        let deleteArticleBtnInit = function () {
            $("#delete-article-btn").unbind("click");

            $("#delete-article-btn").click(function() {
                userInterface.ToggleDeleteConfirmation();
                deleteArticleFalseBtnInit();
            });
        };

        let deleteArticleFalseBtnInit = function () {
            $("#delete-article-false").unbind();

            $("#delete-article-false").click(function() {
                userInterface.ToggleDeleteConfirmation();
            });
        };

        let deleteArticleTrueBtnInit = function () {
            $("#delete-article-true").unbind("click");

            $("#delete-article-true").click(function() {
                dataService.DeleteArticle();
            });
        };

        let buttonInit = function () {
            deleteArticleTrueBtnInit();
            searchButtonInit();
            showMoreButtonInit();
            showMoreFilteredButtonInit();
            editArticleButtonInit();
            previewArticleButtonInit();
            saveArticleButtonInit(!settings.addNewArticleFlag);
            addArticleButtonInit();
        };

        return {
            Init: buttonInit,
            DeleteArticleButtonInit: deleteArticleBtnInit,
            SetShowMoreRelatedArticlesButtonState: setShowMoreRelatedArticlesButtonState,
            SetShowMoreFilteredArticlesButtonState: setShowMoreFilteredArticlesButtonState,
            ResetUrlsButtonInit: resetUrlsButtonInit,
            SetSaveFunctionState: setSaveFunctionState
        };
    })();

    let init = function (isAdmin) {
        dataService.Init();
        initUI(isAdmin);
    };

    let initUI = function (isAdmin) {
        settings.isAdmin = isAdmin === "True";

        buttonEventBinding.Init();
        userInterface.Init();
    }

    return {
        Init: init,
        InitUI: initUI,
        Display: userInterface.Display
    };
})();