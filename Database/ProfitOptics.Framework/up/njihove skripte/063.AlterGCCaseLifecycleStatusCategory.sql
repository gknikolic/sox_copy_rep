  USE [Sox]


ALTER TABLE [Sox].[dbo].[GCLifecycleStatusTypeCategory] ADD  [Order] INT NULL;

ALTER TABLE [Sox].[dbo].[GCLifecycleStatusTypeCategory] ADD  [IsDisabledInTimeline] BIT NULL;

GO
