USE [Sox]

ALTER TABLE [dbo].[GCLifecycleStatusType]
DROP CONSTRAINT [FK_GCLifecycleStatusType_WWPodContainer];

ALTER TABLE [dbo].[GCLifecycleStatusType]
DROP COLUMN [WWPodContainerId];

ALTER TABLE [Sox].[dbo].[GCTraysLifecycleStatusTypes]
ADD [WWPodContainerId] INT NULL;

ALTER TABLE [dbo].[GCTraysLifecycleStatusTypes]  
WITH CHECK ADD CONSTRAINT [FK_GCTraysLifecycleStatusTypes_WWPodContainer] 
FOREIGN KEY([WWPodContainerId])
REFERENCES [dbo].[WWPodContainer] ([Id]);

GO
