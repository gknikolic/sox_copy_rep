﻿using System;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVarianceBillToItemCategoryDrillThroughViewModel : TrendVarianceItemCategoryDrillThroughViewModel
    {
        private readonly IUrlHelper _urlHelper;

        public TrendVarianceBillToItemCategoryDrillThroughViewModel(int customerId,
            string customerName,
            string customerNum,
            int itemCategoryId,
            string itemCategoryName,
            ComparisonType comparisonType,
            DateTime? comparisonStartDate,
            DateTime? comparisonEndDate,
            bool isCorporate, 
            IUrlHelper urlHelper) 
            : base(customerId,
                customerName, 
                customerNum, 
                itemCategoryId, 
                itemCategoryName,
                comparisonType, 
                comparisonStartDate,
                comparisonEndDate,
                isCorporate)
        {
            _urlHelper = urlHelper;
        }

        public override bool IsBillTo => true;

        public override string DataUrl =>
            _urlHelper.Action("GetItemVariancesForItemCategoryAndBillTo", "TrendVariance", new { area = ReportingSettings.AreaName });

        public override string ChartDataUrl =>
            _urlHelper.Action("GetItemVariancesForChartForItemCategoryAndBillTo", "TrendVariance", new { area = ReportingSettings.AreaName });
    }
}