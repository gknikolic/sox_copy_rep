﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class VendorResponse : ResponseBase
    {
        public Vendor Vendor { get; set; }
    }
}
