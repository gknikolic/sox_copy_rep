﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer
{
    public partial class GCProductType
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public virtual ICollection<GCProduct> Products { get; set; }
    }
}
