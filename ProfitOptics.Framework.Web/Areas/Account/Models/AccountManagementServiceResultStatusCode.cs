﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public enum AccountManagementServiceResultStatusCode
    {
        Success,
        Failed,
        UserNotFound
    }
}
