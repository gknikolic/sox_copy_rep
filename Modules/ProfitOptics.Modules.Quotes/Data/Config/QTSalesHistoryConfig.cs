﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTSalesHistoryConfig : IEntityTypeConfiguration<QTSalesHistory>
    {
        public void Configure(EntityTypeBuilder<QTSalesHistory> builder)
        {
            builder.ToTable("QTSalesHistory");
            builder.HasKey(entity => new
            {
                entity.BillToNum,
                entity.ShipToNum,
                entity.ItemNum,
                entity.InvoiceNum,
                entity.InvoiceLineNum,
                entity.InvoiceType,
                entity.QtyShip,
                entity.ExtPrice,
                entity.UnitPrice
            });

            builder.Property(entity => entity.Company).HasMaxLength(20);
            builder.Property(entity => entity.BillToNum).HasMaxLength(50);
            builder.Property(entity => entity.ShipToNum).HasMaxLength(50);
            builder.Property(entity => entity.ItemNum).HasMaxLength(50);
            builder.Property(entity => entity.InvoiceNum).ValueGeneratedNever();
            builder.Property(entity => entity.InvoiceLineNum).ValueGeneratedNever();
            builder.Property(entity => entity.DateInv).HasColumnType("date");
            builder.Property(entity => entity.InvoiceType).HasMaxLength(50);
            builder.Property(entity => entity.OrderUM).HasMaxLength(50);

        }
    }
}
