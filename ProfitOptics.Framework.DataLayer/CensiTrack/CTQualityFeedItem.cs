﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTQualityFeedItem
    {
        public int Id { get; set; }
        public int? GCTraysLifecycleStatusTypeId { get; set; }
        public int? CTContainerTypeActualId { get; set; }
        public string ReportType { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime UpdatedDateUtc { get; set; }
        public string ReportedBy { get; set; }
        public string ResponsibleParty { get; set; }
        public string AssetName { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime EventDateUtc { get; set; }
        public string Comments { get; set; }
        public long LegacyId { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual GCTraysLifecycleStatusTypes GCTraysLifecycleStatusType { get; set; }
        public virtual CTContainerTypeActual CTContainerTypeActual { get; set; }
        public virtual List<CTQualityFeedItemGraphic> CTQualityFeedItemGraphics { get; set; }
    }
}
