﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.DataLayer.Test;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    class TEmployeeConfig : IEntityTypeConfiguration<TEmployee>
    {
        public void Configure(EntityTypeBuilder<TEmployee> builder)
        {
            builder.ToTable("TEmployee");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.FirstName).HasMaxLength(50).IsRequired();
            builder.Property(x => x.LastName).HasMaxLength(50).IsRequired();

            builder.HasMany(x => x.EmployeeSkills)
                .WithOne(x => x.Employee)
                .HasForeignKey(x => x.EmployeeId);
                
        }
    }
}
