using System;

namespace ProfitOptics.Modules.CrossReference.Data.Domain
{
    public partial class CrtCrossReferenceVote
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int Vote { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int CrossReferenceRecordId { get; set; }
    }
}
