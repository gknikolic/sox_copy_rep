﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTLocationConfig : IEntityTypeConfiguration<CTLocation>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTLocation> builder)
        {
            builder.ToTable("CTLocation");
            builder.HasKey(e => e.Id);

            builder.HasOne<CTLocationType>(s => s.CTLocationType)
               .WithMany(x => x.CTLocations)
               .HasConstraintName("FK_CTLocation_CTLocationType");
        }
    }
}
