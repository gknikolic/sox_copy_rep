﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class EditItemResponse : EditBase
    {
        public Item Item { get; set; }
    }
}
