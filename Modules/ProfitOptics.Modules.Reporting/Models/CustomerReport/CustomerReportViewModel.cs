﻿namespace ProfitOptics.Modules.Reporting.Models.CustomerReport
{
    public class CustomerReportViewModel
    {
        public bool IsShipTo { get; set; }
        public bool IsCorporate { get; set; }
        public CustomerReportTableModel TopCustomers { get; set; }
        public CustomerReportTableModel BottomCustomers { get; set; }
    }
}