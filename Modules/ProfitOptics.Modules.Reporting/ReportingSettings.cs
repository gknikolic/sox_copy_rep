﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting
{
    public class ReportingSettings
    {
        public int MaxNumberOfCharactersInNote { get; set; }

        public string BusinessReviewReportTemplatePath { get; set; }

        public string CustomerReportCacheTimeInMinutes { get; set; }

        public string ReportTableColumnNames { get; set; }

        public string TopCustomersCacheKey { get; set; }

        public decimal MinGrowthDeclinePercentage { get; set; }

        public decimal RegionTableGreenPercentage { get; set; }

        public decimal RegionTableYellowPercentage { get; set; }

        public string AllowedItemCategories { get; set; }

        public string AllowedSalesChannels { get; set; }

        public string AllowedProductFamilies { get; set; }

        public string AllowedSalesReps { get; set; }

        public string AllowedRegions { get; set; }

        public const string AreaName = "Reporting";

        public readonly Dictionary<string, int> MonthMappingDict = new Dictionary<string, int>
        {
            { "jan", 1 },
            { "feb", 1 },
            { "mar", 1 },
            { "apr", 2 },
            { "may", 2 },
            { "jun", 2 },
            { "jul", 3 },
            { "aug", 3 },
            { "sep", 3 },
            { "oct", 4 },
            { "nov", 4 },
            { "dec", 4 },
        };
    }
}