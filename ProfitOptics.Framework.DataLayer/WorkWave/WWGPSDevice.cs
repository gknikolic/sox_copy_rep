﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
	[Table("WWGPSDevice")]
	public partial class WWGPSDevice
	{
		public int Id { get; set; }
		public string Label { get; set; }
		public string Category { get; set; }
		public string DeviceId { get; set; }
		public virtual ICollection<WWVehicle> WWVehicles { get; set; }

	}
}
