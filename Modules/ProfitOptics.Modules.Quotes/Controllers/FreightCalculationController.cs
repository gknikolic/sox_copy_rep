﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Infrastructure.Exceptions;
using ProfitOptics.Modules.Quotes.Models;
using ProfitOptics.Modules.Quotes.Services;

namespace ProfitOptics.Modules.Quotes.Controllers
{
    [Area("Quotes")]
    [Authorize(Roles = "quoting,admin,superadmin")]
    public class FreightCalculationController : BaseController
    {
        private readonly IQuoteService _quoteService;

        public FreightCalculationController(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }

        [Authorize]
        public IActionResult CalculateFreight(int quoteId)
        {
            try
            {
                var userId = GetCurrentUserId();
                var quote = (QuoteHeaderModel)_quoteService.GetQuoteHeader(userId, quoteId);

                var result = _quoteService.CalculateFreightCost(quote, userId);

                return Json(new
                {
                    success = true,
                    message = "Success",
                    averageShippingCost = result.AverageShippingCost,
                    totFloorMarginPerc = result.TotFloorMarginPerc,
                    totAdjMarginPerc = result.TotAdjMarginPerc
                });
            }
            catch (Exception ex)
            {
                var e = new JavaScriptErrorException(ex.Message);
                return Json(new { success = false, message = ex.Message });
            }
        }
    }
}