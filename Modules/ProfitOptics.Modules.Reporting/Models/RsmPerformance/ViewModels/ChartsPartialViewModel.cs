﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels
{
    public class ChartsPartialViewModel
    {
        public ChartsPartialViewModel()
        {
            SalesChannelPerformance = new List<SalesChannelChartData>();
            ProductFamilyPerformance = new List<ProductFamilyChartData>();
        }

        public int Year { get; set; }
        public List<SalesChannelChartData> SalesChannelPerformance { get; set; }
        public List<ProductFamilyChartData> ProductFamilyPerformance { get; set; }
    }
}