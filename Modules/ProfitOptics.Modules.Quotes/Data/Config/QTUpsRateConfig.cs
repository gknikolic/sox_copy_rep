﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTUpsRateConfig : IEntityTypeConfiguration<QTUpsRate>
    {
        public void Configure(EntityTypeBuilder<QTUpsRate> builder)
        {
            builder.ToTable("QTUpsRate");
            builder.HasKey(entity => entity.Id);
        }
    }
}
