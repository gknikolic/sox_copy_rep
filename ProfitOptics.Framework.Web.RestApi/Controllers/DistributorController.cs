using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.RestApi.Models;
using ProfitOptics.Framework.Web.RestApi.Models.Distributor;
using ProfitOptics.Framework.Web.RestApi.Resources;
using ProfitOptics.Framework.Web.RestApi.Resources.Distributor;
using ProfitOptics.Framework.Web.RestApi.Services.Distributor;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApiControllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class DistributorController : Controller
    {
        private readonly IDistributorService _distributorService;
        private readonly IMapper _mapper;

        public DistributorController(IDistributorService distributorService, IMapper mapper)
        {
            _distributorService = distributorService;
            _mapper = mapper;
        }

        /// <summary>
        /// Lists all categories.
        /// </summary>
        /// <returns>List os categories.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(QueryResultResource<DistributorResource>), 200)]
        public async Task<QueryResultResource<DistributorResource>> ListAsync([FromQuery] DistributorQueryResource query)
        {
            var distributorsQuery = _mapper.Map<DistributorQueryResource, DistributorQuery>(query);
            var queryResult = await _distributorService.ListAsync(distributorsQuery);

            var resource = _mapper.Map<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor>, QueryResultResource<DistributorResource>>(queryResult);
            return resource;
        }

        /// <summary>
        /// Saves a new distributor.
        /// </summary>
        /// <param name="resource">Distributor data.</param>
        /// <returns>Response for the request.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(DistributorResource), 201)]
        [ProducesResponseType(typeof(ErrorResource), 400)]
        public async Task<IActionResult> PostAsync([FromBody] SaveDistributorResource resource)
        {
            var distributor = _mapper.Map<SaveDistributorResource, ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor>(resource);
            var result = await _distributorService.SaveAsync(distributor);

            if (!result.Success)
            {
                return BadRequest(new ErrorResource(result.Message));
            }

            var distributorResource = _mapper.Map<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor, DistributorResource>(result.Resource);
            return Ok(distributorResource);
        }

        /// <summary>
        /// Updates an existing distributor according to an identifier.
        /// </summary>
        /// <param name="id">Distributor identifier.</param>
        /// <param name="resource">Updated distributor data.</param>
        /// <returns>Response for the request.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(DistributorResource), 200)]
        [ProducesResponseType(typeof(ErrorResource), 400)]
        public async Task<IActionResult> PutAsync(int id, [FromBody] SaveDistributorResource resource)
        {
            var distributor = _mapper.Map<SaveDistributorResource, ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor>(resource);
            var result = await _distributorService.UpdateAsync(id, distributor);

            if (!result.Success)
            {
                return BadRequest(new ErrorResource(result.Message));
            }

            var distributorResource = _mapper.Map<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor, DistributorResource>(result.Resource);
            return Ok(distributorResource);
        }

        /// <summary>
        /// Deletes a given distributor according to an identifier.
        /// </summary>
        /// <param name="id">Distributor identifier.</param>
        /// <returns>Response for the request.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(DistributorResource), 200)]
        [ProducesResponseType(typeof(ErrorResource), 400)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _distributorService.DeleteAsync(id);

            if (!result.Success)
            {
                return BadRequest(new ErrorResource(result.Message));
            }

            var distributorResource = _mapper.Map<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor, DistributorResource>(result.Resource);
            return Ok(distributorResource);
        }
    }
}