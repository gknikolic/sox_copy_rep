﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class DashboardHourPreviewModel
	{
		public DateTime Date { get; set; }
		public List<DashboardHourPreviewItemModel> Items { get; set; }
	}


}
