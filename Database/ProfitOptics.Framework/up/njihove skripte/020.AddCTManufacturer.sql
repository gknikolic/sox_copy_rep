USE [SoxQATest]

BEGIN
 
CREATE TABLE CTManufacturer
(
	[Id] [INT] PRIMARY KEY IDENTITY(1,1),
	[LegacyId] [BIGINT] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[CreatedAtUtc] DateTime NOT NULL,
	[ModifiedAtUtc] DateTime NULL
)

END
