﻿namespace ProfitOptics.Framework.Web.Framework.ServiceResults
{
    public class PasswordResetUrlResult : AuthenticationServiceResult
    {
        public string Url { get; set; }

        private PasswordResetUrlResult()
        {
        }

        public static PasswordResetUrlResult Success(string url) => new PasswordResetUrlResult
        {
            Status = AuthenticationServiceResultStatusCode.Success,
            Url = url
        };

        public static PasswordResetUrlResult EmailNotConfirmed => new PasswordResetUrlResult
        {
            Url = null,
            Status = AuthenticationServiceResultStatusCode.EmailNotConfirmed
        };

        public static PasswordResetUrlResult UserNotFound = new PasswordResetUrlResult
        {
            Url = null,
            Status = AuthenticationServiceResultStatusCode.UserNotFound
        };
    }
}
