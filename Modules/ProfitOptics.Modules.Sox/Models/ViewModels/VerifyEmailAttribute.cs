﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using ProfitOptics.Framework.Web.Framework.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class VerifyEmailAttribute : ValidationAttribute
    {
        //private readonly IUserService _userService;
        //public VerifyEmailAttribute(IUserService userService)
        //{
        //    _userService = userService;
        //}
        //public int Email { get; private set; }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var userService = (IUserService)validationContext
                         .GetService(typeof(IUserService));

            if (value != null)
            {
                string email = value.ToString();

                var user = userService.FindUserByEmailAsync(email);
                if (user.Result != null)
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }
                else
                {
                    return ValidationResult.Success;
                }
            }

            return ValidationResult.Success;
        }
    }
}
