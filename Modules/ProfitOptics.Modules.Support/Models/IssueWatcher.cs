﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Models
{
    public class IssueWatcher
    {
        public List<string> IssueIds { get; set; }

        public string Email { get; set; }
    }
}