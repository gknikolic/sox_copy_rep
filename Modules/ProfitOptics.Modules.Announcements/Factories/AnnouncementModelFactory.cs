﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.TokenizationSearch;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Announcements.Data.Domain;
using ProfitOptics.Modules.Announcements.Models;
using ProfitOptics.Modules.Announcements.Services;

namespace ProfitOptics.Modules.Announcements.Factories
{
    public class AnnouncementModelFactory : IAnnouncementModelFactory
    {
        private readonly IAnnouncementService _announcementService;
        private readonly IUserService _userService;
        private readonly POFrameworkDefaults _frameworkDefaults;

        public AnnouncementModelFactory(IAnnouncementService announcementService,
            IUserService userService,
            POFrameworkDefaults frameworkDefaults)
        {
            _announcementService = announcementService;
            _userService = userService;
            _frameworkDefaults = frameworkDefaults;
        }

        public async Task<AnnouncementModel> GetAnnouncementModelByIdAsync(int id)
        {
            AnnouncementModel model;

            if (id <= 0)
            {
                model = new AnnouncementModel();
            }
            else
            {
                POAnnouncement announcement = _announcementService.GetAnnouncementById(id);

                model = CreateAnnouncementModelPredicate(announcement);
            }

            IList<ApplicationRole> roles = await _userService.GetRolesAsync();

            foreach (ApplicationRole role in roles)
            {
                model.AllRoles.Add(new RoleModel
                {
                    Id = role.Id,
                    Name = role.Name,
                    IsSelected = model.RoleIds.Contains(role.Id)
                });
            }

            return model;
        }

        public async Task<PagedList<AnnouncementModel>> PrepareAnnouncementModelsForUserIdAsync(IDataTablesRequest request, int userId)
        {
            IQueryable<AnnouncementModel> records = (await _announcementService.GetAnnouncementsForUserIdAsync(userId))
                .Select(CreateAnnouncementModelPredicate).AsQueryable();

            PagedList<AnnouncementModel> pagedRecords = ProcessRequest(request, records);

            return pagedRecords;
        }

        public async Task<PagedList<AnnouncementModel>> PrepareAnnouncementListModelAsync(IDataTablesRequest request, int? userId = null, bool validOnly = false, bool activeOnly = false)
        {
            IQueryable<AnnouncementModel> records = (await _announcementService.SearchAnnouncementsAsync(userId, validOnly, activeOnly))
                .Select(CreateAnnouncementModelPredicate).AsQueryable();

            PagedList<AnnouncementModel> pagedRecords = ProcessRequest(request, records);

            return pagedRecords;
        }

        public async Task<List<AnnouncementNotificationModel>> PrepareActiveAnnouncementModelsForUserIdAsync(int userId)
        {
            DateTime utcNow = DateTime.UtcNow;
            
            IEnumerable<POAnnouncementWithProfilePicture> records = await _announcementService
                .GetAnnouncementsWithProfilePicturesForUserIdAsync(userId, true);

            List<AnnouncementNotificationModel> models = records
                .Select(item => new AnnouncementNotificationModel
                {
                    Id = item.Id,
                    CreatedAgo = PrepareCreatedAgo(item.CreateTime, utcNow),
                    UserProfilePictureBase64 = PrepareProfilePicture(item.UserProfilePicture),
                    Title = item.Title,
                    Text = System.Net.WebUtility.HtmlDecode(item.Text),
                }).ToList();

            return models;
        }

        public AnnouncementsPageModel PrepareAnnouncementsPageModel(bool isAdminOrSuperAdmin)
        {
            return new AnnouncementsPageModel
            {
                EnableAdding = isAdminOrSuperAdmin
            };
        }

        private static AnnouncementModel CreateAnnouncementModelPredicate(POAnnouncement announcement)
        {
            return new AnnouncementModel
            {
                Id = announcement.Id,
                Title = announcement.Title,
                CreateTime = announcement.CreateTime,
                ValidUntil = announcement.ValidUntil,
                Text = announcement.Text,
                FirstName = announcement.AspNetUser?.FirstName,
                LastName = announcement.AspNetUser?.LastName,
                RoleIds = announcement.POAnnouncementRoles.Select(q => q.RoleId),
                IsDeleted = announcement.IsDeleted,
                CreatedById = announcement.UserId ?? int.MinValue
            };
        }

        private static string PrepareCreatedAgo(DateTime? announcementCreateTime, DateTime currentTimeUtc)
        {
            if (!announcementCreateTime.HasValue)
            {
                return "forever";
            }

            TimeSpan yearSpan = DateTime.UtcNow.Date.AddYears(1).Subtract(DateTime.UtcNow.Date);

            TimeSpan createdAgoTimeSpan = (currentTimeUtc - announcementCreateTime).Value;
            
            if (createdAgoTimeSpan < TimeSpan.FromMinutes(1))
            {
                return "just now";
            }

            if (createdAgoTimeSpan < TimeSpan.FromHours(1))
            {
                return $"{Math.Floor(createdAgoTimeSpan.TotalMinutes)} min";
            }

            if (createdAgoTimeSpan < TimeSpan.FromDays(1))
            {
                return $"{Math.Floor(createdAgoTimeSpan.TotalHours)} h {createdAgoTimeSpan.Minutes} min";
            }

            if (createdAgoTimeSpan < TimeSpan.FromDays(7))
            {
                return $"{Math.Floor(createdAgoTimeSpan.TotalDays)} d {createdAgoTimeSpan.Hours} h {createdAgoTimeSpan.Minutes} min";
            }

            if (createdAgoTimeSpan < yearSpan)
            {
                return $"{Math.Floor(createdAgoTimeSpan.TotalDays / 7)} w {createdAgoTimeSpan.Days} d {createdAgoTimeSpan.Hours} h {createdAgoTimeSpan.Minutes} min";
            }

            return "over a year";
        }

        private string PrepareProfilePicture(byte[] announcementUserProfilePicture)
        {
            announcementUserProfilePicture = announcementUserProfilePicture ??
                                             File.ReadAllBytes(_frameworkDefaults.ProfilePictureAbsoluteFilePath);

            return Convert.ToBase64String(announcementUserProfilePicture);
        }

        private static PagedList<AnnouncementModel> ProcessRequest(IDataTablesRequest request, IQueryable<AnnouncementModel> records)
        {
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                string[] strings = Tokenizer.TokenizeToWords(request.Search.Value);

                records = strings.Aggregate(records, (current, str) => 
                    current.Where(x => x.Title != null && 
                                       x.Title.Contains(str, StringComparison.InvariantCultureIgnoreCase) || 
                                       x.Text != null && 
                                       x.Text.Contains(str, StringComparison.InvariantCultureIgnoreCase)));
            }

            records = records.ApplySort(request);

            return new PagedList<AnnouncementModel>(records, request.Start, request.Length);
        }
    }
}