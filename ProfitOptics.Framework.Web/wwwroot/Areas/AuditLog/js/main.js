﻿(function (auditLogModule, $, undefined) {
    // Private Properties
    var currentRowId = null;

    // Public properties
    auditLogModule.table = null;

    auditLogModule.friendlyNameTable = null;

    // Public Methods
    auditLogModule.initFilters = function() {
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        //Date range picker with time picker
        $('#from').daterangepicker({
            startDate: yesterday,
            timePicker: true,
            singleDatePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
        });

        var now = new Date();
        $('#to').daterangepicker({
            startDate: now,
            timePicker: true,
            singleDatePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
        });
    };

    auditLogModule.runReport = function(e) {
        e.preventDefault();
        auditLogModule.table.draw();
    };

    auditLogModule.exportToExcell = function exportToExcel() {
        var url = $("#exportToExcelUrl").val()+"?";
        url += "username=" + $("#username").val() + "&";
        url += "from=" + $("#from").val() + "&";
        url += "to=" + $("#to").val() + "&";
        url += "timeOffset=" + new Date().getTimezoneOffset();
        document.location = url;
    };

    auditLogModule.editFriendlyName = function(rowId, source) {
        currentRowId = rowId;
        if (source == "auditLog") {
            $("#modalSource").val(source);
            var data = auditLogModule.table.row(currentRowId).data();

        } else if (source == "friendlyNames") {
            $("#modalSource").val(source);
            var data = auditLogModule.friendlyNameTable.row(currentRowId).data();
        }

        $("#friendlyName").val(data.friendlyName);
        $("#editFriendlyNameModal").modal("show");
    };

    auditLogModule.saveFriendlyName = function() {
        if ($("#modalSource").val() == "auditLog") {
            var data = auditLogModule.table.row(currentRowId).data();
        } else if ($("#modalSource").val() == "friendlyNames") {
            var data = auditLogModule.friendlyNameTable.row(currentRowId).data();
        }
        $.ajax({
            url: $("#saveFriendlyNameUrl").val(),
            type: "json",
            method: "POST",
            data: {
                reflectedType: data.ReflectedType,
                reflectedActionName: data.ReflectedActionName,
                friendlyName: $("#friendlyName").val()
            },
            success: function(res) {
                data.FriendlyName = $("#friendlyName").val();
                if ($("#modalSource").val() == "auditLog") {
                    auditLogModule.table.row(currentRowId).every(function() {
                        this.invalidate();
                    });
                } else if ($("#modalSource").val() == "friendlyNames") {
                    auditLogModule.friendlyNameTable.row(currentRowId).every(function() { this.invalidate(); });
                }
                $("#editFriendlyNameModal").modal("hide");
            }
        });
    };
    // Private Methods

}(window.auditLogModule = window.auditLogModule || {}, jQuery));


app.activeMenus = ["menuAdmin", "menuAdminAuditLog"]; // List all menus that should be activated when this page is opened

$(document).ready(function () {
    auditLogModule.initFilters();
    setTableAuditLog();
    setTableFriendlyNames();
});

function setTableAuditLog() {
    auditLogModule.table = $('#dt-table-auditlog')
        .on('preXhr.dt', function (e, settings, data) {
            // callback before ajax call
            $('.table-overlay').show();
        }).on('xhr.dt', function (e, settings, json, xhr) {
            // callback after ajax call
            $('.table-overlay').hide();
        }).DataTable({
            lengthChange: true,
            autoWidth: false,
            processing: true,
            serverSide: true,
            searching: true,
            ordering: true,
            paging: true,
            //dom: "rtiSp",
            info: true,
            //scrollY: 500,
            //scrollCollapse: true,
            deferRender: true,
            scroller: {
                "loadingIndicator": true,
                "trace": true,
                "displayBuffer": 50,
                "boundaryScale": 0.5,
                "autoHeight": true,
                "serverWait": 0
            },
            order: [[0, "desc"]],
            drawCallback: function (settings) {
            },
            rowCallback: function (row, data, index) {
            },
            ajax: {
                url: $("#getTableUrl").val(),
                data: function (d) {
                    d.username = $("#username").val();
                    d.from = $("#from").val();
                    d.to = $("#to").val();
                    d.timeOffset = new Date().getTimezoneOffset();
                    d.showAll = $("#showAllActions").is(':checked');
                },
                type: "POST"
            },
            language: {
                processing: 'Please wait..'
            },
            columns: [
                {
                    data: "Inserted",
                    render: function (data, type, full, meta) {
                        return data.formatJsonDate(true);
                    },
                    className: "text-left",
                    width: 120
                },
                {
                    data: "Username",
                    render: function (data, type, full, meta) {
                        return data;
                    },
                    className: "text-left",
                    width: 180
                },
                {
                    data: "Area",
                    render: function (data, type, full, meta) {
                        return data;
                    },
                    className: "text-left",
                    width: 100
                },
                {
                    data: "Controller",
                    render: function (data, type, full, meta) {
                        return data;
                    },
                    className: "text-left",
                    width: 100
                },
                {
                    data: "Action",
                    render: function (data, type, full, meta) {
                        return data;
                    },
                    className: "text-left",
                    width: 100
                },
                {
                    data: "IpAddress",
                    render: function (data, type, full, meta) {
                        return data;
                    },
                    className: "text-left"
                },
                {
                    data: "Browser",
                    render: function (data, type, full, meta) {
                        return data;
                    },
                    className: "text-left"
                },
                {
                    data: "FriendlyName",
                    width: "110px",
                    className: "text-center",
                    render: function (data, type, full, meta) {
                        if (data != null) {
                            return data;
                        }
                        else {
                            var res = '<div>';
                            res += '<button title="Edit Friendly Name" class="btn btn-default btn-xs" onclick="auditLogModule.editFriendlyName(' + meta.row + ',' + "'auditLog'" + ')"><span title="Edit Friendly Name" class="glyphicon glyphicon-edit"></span></button>';
                            res += "</div>";

                            return res;
                        }
                    }
                }]
        });

}

$("div.dt-table-auditlog_filter input").keyup(function (e) {
    if (e.keyCode == 13) {
        oTable.fnFilter(this.value);
    }
});

function setTableFriendlyNames() {
    auditLogModule.friendlyNameTable = $('#dt-table-friendlynames')
        .on('preXhr.dt', function (e, settings, data) {
            // callback before ajax call
            $('.table-overlay').show();
        }).on('xhr.dt', function (e, settings, json, xhr) {
            // callback after ajax call
            $('.table-overlay').hide();

            $('div.dataTables_filter input').off('keyup.DT input.DT');
            var searchDelay = null;
            $('div.dataTables_filter input').on('keyup', function () {
                var search = $('div.dataTables_filter input').val();
                clearTimeout(searchDelay);
                searchDelay = setTimeout(function () {
                    if (search != null) {
                        auditLogModule.friendlyNamesTable.search(search).draw();
                    }
                }, 500);
            });
        }).DataTable({
            lengthChange: true,
            autoWidth: true,
            processing: true,
            serverSide: true,
            searching: false,
            ordering: true,
            paging: true,
            dom: "rtiSp",
            info: true,
            scrollY: 500,
            scrollCollapse: true,
            deferRender: true,
            scroller: {
                "loadingIndicator": true,
                "trace": true,
                "displayBuffer": 50,
                "boundaryScale": 0.5,
                "autoHeight": true,
                "serverWait": 0
            },
            order: [[0, "desc"]],
            drawCallback: function (settings) {
            },
            rowCallback: function (row, data, index) {
            },
            ajax: {
                url: $("#getActionFriendlyNameDataUrl").val(),
                data: function (d) {
                },
                type: "POST"
            },
            language: {
                processing: 'Please wait..'
            },
            columns: [
                {
                    data: "ReflectedType",
                    render: function (data, type, full, meta) {
                        return data;
                    },
                    className: "text-left"
                },
                {
                    data: "ReflectedActionName",
                    render: function (data, type, full, meta) {
                        return data;
                    },
                    className: "text-left"
                },
                {
                    data: "FriendlyName",
                    render: function (data, type, full, meta) {
                        return data;
                    },
                    className: "text-left"
                },
                {
                    data: "FriendlyName",
                    width: "110px",
                    className: "text-center",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        var res = '<div>';
                        res += '<button title="Edit Friendly Name" class="btn btn-default btn-xs" onclick="auditLogModule.editFriendlyName(' + meta.row + ',' + "'friendlyNames'" + ')"><span title="Edit Friendly Name" class="glyphicon glyphicon-edit"></span></button>';
                        res += "</div>";

                        return res;
                    }
                }]
        });
}