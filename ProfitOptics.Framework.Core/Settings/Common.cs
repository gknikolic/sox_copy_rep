﻿namespace ProfitOptics.Framework.Core.Settings
{
    public class Common
    {
        public string DefaultConnection { get; set; }
        public string TempDirectory { get; set; }
        public string AttachmentsDirectory { get; set; }
        public string ClientName { get; set; }
        public string GoogleAnalyticsTrackingId { get; set; }
        public string DisabledModules { get; set; }
        public string AllowedEmailDomains { get; set; }
        public bool NotifyAdminsOnUserRegister { get; set; }

        public string PrimaryContactName { get; set; }
        public string PrimaryContactPhone { get; set; }
        public string PrimaryContactEmail { get; set; }
        public string AlternativeContactName { get; set; }
        public string AlternativeContactPhone { get; set; }
        public string AlternativeContactEmail { get; set; }

        public string HierarchyDefaultImageRelativePath { get; set; }
        public string CKEditorRelativePath { get; set; }
        public string EmailParseKey { get; set; }
        public bool EnableSAML { get; set; }
        public string GoogleMapAPIKEY { get; set; }
    }
}
