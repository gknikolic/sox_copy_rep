﻿using ProfitOptics.Framework.Web.RestApi.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.Services.User
{
    public interface IUserService
    {
        Task<AspNetUsers> FindByUsernameAsync(string username);
        Task<List<string>> GetUserRoles(string username);
    }
}
