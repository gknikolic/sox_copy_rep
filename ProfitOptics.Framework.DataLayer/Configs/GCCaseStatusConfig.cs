﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCCaseStatusConfig : IEntityTypeConfiguration<GCCaseStatus>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCCaseStatus> builder)
        {
            builder.ToTable("GCCaseStatus");
            builder.HasKey(entity => entity.Id);
        }
    }
}
