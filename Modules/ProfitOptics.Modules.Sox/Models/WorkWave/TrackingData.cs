﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class TrackingData
	{
		[JsonProperty("driverId")]
		public string DriverId { get; set; }

		[JsonProperty("vehicleId")]
		public string VehicleId { get; set; }
		[JsonProperty("timeInSec")]
		public int TimeInSec { get; set; }
		[JsonProperty("status")]
		public RouteStepStatusType? Status { get; set; }
		[JsonProperty("statusSec")]
		public int StatusSec { get; set; }
		[JsonProperty("pods")]
		public PodContainer Pods { get; set; }

	}
}
