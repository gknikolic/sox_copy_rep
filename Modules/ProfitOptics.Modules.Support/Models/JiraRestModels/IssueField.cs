﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueField
    {
        public string summary { get; set; }

        public List<IssueSubtask> subtasks { get; set; }

        public List<IssueComponent> components { get; set; }

        // will be used to store just a single username, creator, so we can keep track
        public List<string> labels { get; set; }

        public List<IssueFixVersion> fixVersions { get; set; }

        public IssueType issuetype { get; set; }

        public IssueStatus status { get; set; }

        public IssueAssignee assignee { get; set; }

        public int? aggregatetimeestimate { get; set; }

        public IssueReporter reporter { get; set; }

        public int? aggregatetimeoriginalestimate { get; set; }

        public IssueProject project { get; set; }

        public string environment { get; set; }

        public DateTime? updated { get; set; }

        public string description { get; set; }

        public IssuePriority priority { get; set; }

        public int? timeestimate { get; set; }

        public DateTime? duedate { get; set; }

        public int? timeoriginalestimate { get; set; }
    }
}