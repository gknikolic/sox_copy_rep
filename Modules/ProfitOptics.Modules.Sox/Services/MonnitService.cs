﻿using ProfitOptics.Framework.DataLayer.GroundControl;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Models;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Services
{
	public class MonnitService : IMonnitService
	{
		private readonly IMonnitClient _monnitClient;
		private readonly Framework.DataLayer.Entities _context;
		public MonnitService(IMonnitClient monnitClient, Framework.DataLayer.Entities context)
		{
			_monnitClient = monnitClient;
			_context = context;
		}

		public async Task SyncMonnitSensorsAsync()
		{
			var result = await _monnitClient.GetSensors();
			if (result != null && result.Count > 0)
			{
				foreach (var item in result)
				{
					var dbSensor = _context.GCLocations.Where(x => x.MappingTitle == item.SensorId.ToString()).FirstOrDefault();
					if (dbSensor != null)
					{
						dbSensor.Title = item.SensorName;
					}
					else
					{
						var location = new GCLocation() { Title = item.SensorName, MappingTitle = item.SensorId.ToString() };
						_context.GCLocations.Add(location);
					}
					_context.SaveChanges();
				}
			}
		}

		public void HandleMonnitWebhookResponse(MonnitWebhookResponse response)
		{
			if (response != null)
			{
				if (response.SensorMessages != null && response.SensorMessages.Count > 0)
				{
					foreach (var message in response.SensorMessages)
					{
						var location = _context.GCLocations.Where(x => x.MappingTitle == message.SensorId.ToString()).FirstOrDefault();
						if (location != null)
						{
							var locInfo = _context.GCLocationInfos.Where(x => x.GCLocationId == location.Id && x.Timestamp == message.MessageDate).FirstOrDefault();
							if (locInfo == null)
							{
								var messageInfo = message.PlotValues.Split("|");
								float humidity = float.Parse(messageInfo[0]);
								float temp = float.Parse(messageInfo[1]);
								var model = new GCLocationInfo() { GCLocationId = location.Id, Timestamp = message.MessageDate, Humidity = humidity, Temperature = temp };
								_context.GCLocationInfos.Add(model);
								_context.SaveChanges();
							}
						}
					}
				}
			}
		}
	}
}
