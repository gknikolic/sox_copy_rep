﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels
{
    public class GenericChartData
    {
        public int SalesRepId { get; set; }
        public decimal YtdActual { get; set; }
        public decimal YtdQuota { get; set; }
    }
}