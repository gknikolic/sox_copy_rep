
IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'ConcurrencyStamp'
          AND Object_ID = Object_ID(N'ASPNETROLES'))
BEGIN
    Alter Table ASPNETROLES ADD ConcurrencyStamp varchar(255) null
    Alter Table ASPNETROLES ADD NormalizedName varchar(255) null
END

 IF exists(SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AspNetUserTokens' AND TABLE_CATALOG = 'ProfitOptics.Framework')
 BEGIN
 Drop Table AspNetUserTokens
 END

 CREATE TABLE [AspNetUserTokens] (
    [UserId]        NVARCHAR (450) NOT NULL,
    [LoginProvider] NVARCHAR (450) NOT NULL,
    [Name]          NVARCHAR (450) NOT NULL,
    [Value]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AspNetUserTokens]
 PRIMARY KEY CLUSTERED ([UserId] ASC, [LoginProvider] ASC, [Name] ASC)
)


IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'ConcurrencyStamp'
          AND Object_ID = Object_ID(N'AspNetUsers'))
BEGIN
Alter Table AspNetUsers
 Add ConcurrencyStamp varchar(255) null,
	 LockoutEnd datetimeoffset(7) null,
	 NormalizedEmail varchar(255) null,
     NormalizedUserName varchar(255) null
END


 if exists(SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AspNetRoleClaims' AND TABLE_CATALOG = 'ProfitOptics.Framework')
 BEGIN
	Drop Table [AspNetRoleClaims]
 END
CREATE TABLE [AspNetRoleClaims] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [ClaimType]  NVARCHAR (MAX) NULL,
    [ClaimValue] NVARCHAR (MAX) NULL,
    [RoleId]     INT NOT NULL,
    CONSTRAINT [PK_AspNetRoleClaims]
 PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
 FOREIGN KEY ([RoleId])
  REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE
)


GO
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId]
    ON [AspNetRoleClaims]([RoleId] ASC)

	
IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'ProviderDisplayName'
          AND Object_ID = Object_ID(N'AspNetUserLogins'))
BEGIN
Alter Table AspNetUserLogins
   Add  ProviderDisplayName varchar(255) null
END



UPDATE dbo.AspNetUsers
set normalizedUsername = upper(UserName), normalizedEmail = upper(email)