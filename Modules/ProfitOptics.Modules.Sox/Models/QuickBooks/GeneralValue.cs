﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class GeneralValue
    {
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
