﻿using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Models.HelpArticle
{
    public class HelpArticleGroupedByTagModel
    {
        public string Tag { get; set; }
        public IEnumerable<HelpArticleModel> Articles { get; set; }
    }
}