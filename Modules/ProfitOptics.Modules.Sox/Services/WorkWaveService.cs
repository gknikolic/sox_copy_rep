﻿using ProfitOptics.Modules.Sox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProfitOptics.Modules.Sox.Infrastructure;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Models.WorkWave.Enums;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Logging;
using NLog.Web;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using ProfitOptics.Modules.Sox.Models.WorkWave;
using ProfitOptics.Framework.DataLayer.WorkWave;

namespace ProfitOptics.Modules.Sox.Services
{
    public class WorkWaveService : IWorkWaveService
    {
        private readonly int WorkWaveLoadMultiplier = 100;
        private readonly Framework.DataLayer.Entities _context;
        private readonly ISoxLogger soxLogger;
        public IWorkWaveClient WorkWaveClient { get; set; }

        public WorkWaveService(Framework.DataLayer.Entities context,
                               IWorkWaveClient workWaveClient,
                               ISoxLogger soxLogger)
        {
            this._context = context;
            this.WorkWaveClient = workWaveClient;
            this.soxLogger = soxLogger;
        }
        /// <summary>
        /// This method creates both incoming and outgoing order by default if not specified differently 
        /// </summary>
        /// <param name="CaseId"></param>
        /// <param name="createOtugoingOrder"></param>
        /// <param name="createIncomingOrder"></param>
        /// <returns></returns>
        public async Task<WorkWaveResponse> AddOrderToWWAsync(int CaseId, bool createOtugoingOrder = true, bool createIncomingOrder = true)
        {
            WorkWaveResponse result = new WorkWaveResponse();
           
            var gcCase = _context.GCCases.Find(CaseId);
            result.Message = "Processing: " + gcCase.Title;
            var ctCase = _context.CTCases.Find(gcCase.CTCaseId);
            var surgeryTime = ctCase.DueTimestamp;
            if(surgeryTime.TrimHoursMinutesSeconds()<DateTime.Now.TrimHoursMinutesSeconds())
			{
                result.Message = result.Message + "[Error] - Surgery Time elapsed: "+surgeryTime.ToShortDateString();
                return result;
			}
            //_context.Entry(ctCase).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
            var gcCustomer = _context.GCCustomers.Find(gcCase.GCCustomerId);
            if(gcCustomer==null)
			{
                result.Message = result.Message + "[Error] - Missing Customer";
                return result;
			}
            var gcSettings = _context.GCSettings.Where(x => x.Environment == "Development").FirstOrDefault();
            var ctContainers = _context.CTCaseContainers.Where(m => m.CTCaseId == ctCase.Id);
            var numOfTrays = ctContainers.Count() * WorkWaveLoadMultiplier;
            if(numOfTrays==0)
			{
                result.Message = result.Message + "[Error] - Number of trays is 0";
                return result;//If there are no trays, do not create order in WW
			}

            //var nlogger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            Dictionary<string, string> pickupCustomFields = new Dictionary<string, string>();
            Dictionary<string, string> deliveryCustomFields = new Dictionary<string, string>();

            pickupCustomFields.Add(OrderCustomFiledType.PickUpSurgery.GetAttribute<DisplayAttribute>().Name, ctCase.CaseReference);
            //pickupCustomFields.Add(OrderCustomFiledType.PickUpSurgeon.GetAttribute<DisplayAttribute>().Name, ctCase.CaseDoctor);
            deliveryCustomFields.Add(OrderCustomFiledType.DeliverySurgery.GetAttribute<DisplayAttribute>().Name, ctCase.CaseReference ?? "");
            deliveryCustomFields.Add(OrderCustomFiledType.DeliverySurgeon.GetAttribute<DisplayAttribute>().Name, ctCase.CaseDoctor ?? "");
            List<string> barcodesList = new List<string>();
            //var counter = 1;

            foreach (var item in ctContainers)
            {
                if(item.ItemType == null)//if tray is missing one of these two thigs that means that tray is not yet actual and we should not create order in ww
                {
                    result.Message = result.Message + "[Error] - tray " + item.ContainerName + " is missing Item Type";
                    return result;
                }
                if (item.ContainerId == null)
                {
                    result.Message = result.Message + "[Error] - tray " + item.ContainerName + " is missing ContainerId";
                    return result;
                }
                var barcode = item.ItemType + item.ContainerId.ToString();
                barcodesList.Add(barcode);

                // pickupCustomFields.Add(OrderCustomFiledType.PickupTray.GetAttribute<DisplayAttribute>().Name + counter, item.ContainerName);
                //deliveryCustomFields.Add(OrderCustomFiledType.DeliverTray.GetAttribute<DisplayAttribute>().Name + counter++, item.ContainerName ?? "");

            }


            deliveryCustomFields.Add(OrderCustomFiledType.DeliverNotes.GetAttribute<DisplayAttribute>().Name, gcCustomer.DeliverNotes ?? "");
            deliveryCustomFields.Add(OrderCustomFiledType.DeliverAccessCode.GetAttribute<DisplayAttribute>().Name, gcCustomer.DeliverAccessCode ?? "");
            deliveryCustomFields.Add(OrderCustomFiledType.DeliverPhoneNumber.GetAttribute<DisplayAttribute>().Name, gcCustomer.DeliverPhoneNumber ?? "");
            deliveryCustomFields.Add(OrderCustomFiledType.DeliverName.GetAttribute<DisplayAttribute>().Name, gcCustomer.DeliverName ?? "");

            pickupCustomFields.Add(OrderCustomFiledType.OutgoingOrder.GetAttribute<DisplayAttribute>().Name, "True");
            deliveryCustomFields.Add(OrderCustomFiledType.OutgoingOrder.GetAttribute<DisplayAttribute>().Name, "True");
            //calculating timewindows for orders
            DateTime deliveryDate = ctCase.DueTimestamp;
            int deliveryEnd = Int32.Parse(gcCustomer.DeliveryEnd);
            //return pickup order should be in a timewindow that starts 90mins after surgery and ends 5h after surgery no matter customers delivery start/delivery end
            int returnPickupStartTime = Convert.ToInt32(ctCase.DueTimestamp.AddMinutes(90).TimeOfDay.TotalSeconds);
            int returnPickupEndTime = Convert.ToInt32(ctCase.DueTimestamp.AddHours(5).TimeOfDay.TotalSeconds);
            //return delivery order should be in a timewindow that starts 2h after surgery and ends 8h after surgery no matter customers delivery start/delivery end
            int returnDeliveryStartTime = Convert.ToInt32(ctCase.DueTimestamp.AddHours(2).TimeOfDay.TotalSeconds);
            int returnDeliveryEndTime = Convert.ToInt32(ctCase.DueTimestamp.AddHours(8).TimeOfDay.TotalSeconds);
            int deliveryStart = Int32.Parse(gcCustomer.DeliveryStart);
            if(DateTime.Now.TrimHoursMinutesSeconds() != ctCase.DueTimestamp.TrimHoursMinutesSeconds())
			{
                deliveryDate = deliveryDate.AddDays(-1); //delivery date is always day before surgery but only if we are not creating order for today
            }
            
            
            var Pickup = new OrderStep()
            {
                Location = new Location() { Address = gcSettings.VMAddress },
                TimeWindows = new List<TimeWindow>() { new TimeWindow() { StartSec = Int32.Parse(gcSettings.VMPickupStart), EndSec = Int32.Parse(gcSettings.VMPickupEnd) } },
                CustomFields = pickupCustomFields,
                Barcodes = barcodesList
            };
            var Delivery = new OrderStep()
            {
                Location = new Location() { Address = gcCustomer.GetCustomerAddress() },
                TimeWindows = new List<TimeWindow>() { new TimeWindow() { StartSec = deliveryStart, EndSec = deliveryEnd } },
                CustomFields = deliveryCustomFields,
                ServiceTimeSec = gcCustomer.ServiceTime,
                Barcodes = barcodesList
            };
            Order order = new Order()
            {
                Name = gcCustomer.CustomerName + " - " + ctCase.CaseReference,
                Eligibility = new Eligibility() { EligibilitType = EligibilityType.on, onDates = new List<string>() { deliveryDate.ToWorkWaveDate() } },
                ForceVehicleId = null,
                Priority = 0,
                Loads = new Loads() { CleanTrays = numOfTrays },
                Pickup = Pickup,
                Delivery = Delivery

            };
            if (createOtugoingOrder)
            {
                result = await WorkWaveClient.AddOrder(new AddOrdersModel() { Orders = new List<Order>() { order } });
                gcCase.OutgoingRequestId = result.RequestId;
                _context.SaveChanges();

                soxLogger.SoxInformation("OutgoingRequestId: " + gcCase.IncomingRequestId + "CaseId: " + CaseId + "CaseReference: " + ctCase.CaseReference);
               // nlogger.Info("OutgoingRequestId: " + gcCase.IncomingRequestId + "CaseId: " + CaseId + "CaseReference: " + ctCase.CaseReference);
            }

            //create reverse order (just for pickup where was delivery in original order)
            Delivery.CustomFields[OrderCustomFiledType.OutgoingOrder.GetAttribute<DisplayAttribute>().Name] = "False";
            Pickup.CustomFields[OrderCustomFiledType.OutgoingOrder.GetAttribute<DisplayAttribute>().Name] = "False";
            //set time window for return pickup order (pickup and delivery orders are reverted for incoming orders)
            Delivery.TimeWindows.Clear();
            var endOfDay = new DateTime(ctCase.DueTimestamp.Year, ctCase.DueTimestamp.Month, ctCase.DueTimestamp.Day, 23, 59, 59);
            //adjust timewindows so start time is not after end time (if it is, end time becomes end of day)
            if(returnPickupStartTime>=returnPickupEndTime)
			{
                returnPickupEndTime = Convert.ToInt32(endOfDay.TimeOfDay.TotalSeconds);
            }
            if (returnDeliveryStartTime >= returnDeliveryEndTime)
            {
                returnDeliveryEndTime = Convert.ToInt32(endOfDay.TimeOfDay.TotalSeconds);
            }
            Delivery.TimeWindows.Add(new TimeWindow() { StartSec = returnPickupStartTime, EndSec = returnPickupEndTime });
            //set time window for return delivery order
            Pickup.TimeWindows.Clear();
            Pickup.TimeWindows.Add(new TimeWindow() { StartSec = returnDeliveryStartTime, EndSec = returnDeliveryEndTime });
            Order returnOrder = new Order()
            {
                Name = gcCustomer.CustomerName + " - " + ctCase.CaseReference,
                Eligibility = new Eligibility() { EligibilitType = EligibilityType.on, onDates = new List<string>() { ctCase.DueTimestamp.AddHours(2).ToWorkWaveDate() } },
                ForceVehicleId = null,
                Priority = 0,
                Loads = new Loads() { SoiledTrays = numOfTrays },
                Pickup = Delivery,
                Delivery = Pickup

            };
            if (createIncomingOrder)
            {
                var incomingOrderResult = await WorkWaveClient.AddOrder(new AddOrdersModel() { Orders = new List<Order>() { returnOrder } });
                gcCase.IncomingRequestId = incomingOrderResult.RequestId;
                _context.SaveChanges();

                soxLogger.SoxInformation("IncomingRequestId: " + gcCase.IncomingRequestId + "CaseId: " + CaseId + "CaseReference: " + ctCase.CaseReference);
               // nlogger.Info("IncomingRequestId: " + gcCase.IncomingRequestId + "CaseId: " + CaseId + "CaseReference: " + ctCase.CaseReference);
            }
            result.Message = result.Message + "[Success]";
            return result;

        }

        /// <summary>
        /// Compare barcodes in our GC with WW barcodes
        /// </summary>
        /// <param name="gcCaseId"></param>
        /// <returns>List of barcodes from GC that are not present in WW</returns>
        public async Task<List<string>> CompareBarcodesWithWorkWaveAsync(int gcCaseId)
        {
            var result = new List<string>();
            var gccase = _context.GCCases.Find(gcCaseId);
            if (gccase != null)
            {
                var gcBarcodes = _context.GCTrays.Where(x => x.GCCaseId == gcCaseId).Select(x => x.Barcode).ToList();
                var wworders = await WorkWaveClient.GetOrderByIdFromWWAsync(gccase.OutgoingOrderId);
                if (wworders != null)
                {
                    foreach (var barcode in gcBarcodes)
                    {
                        bool wwContains = wworders.Orders.First().Value.Pickup?.Barcodes?.Contains(barcode) ?? false;
                        if (!wwContains)
                        {
                            result.Add(barcode);
                        }
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Syncing orders with WW both ways
        /// </summary>
        /// <returns></returns>
        public async Task<string> SyncOrdersWithWorkWaveAsync()
        {
            //get missing orders from workwave
            string finalResult = "";
            await WorkWaveClient.GetOrdersFromWorWaveAsync();
            //adding orders to workwave for cases that dont have outgoing or incoming order
            var gcCases = _context.GCCases.Where(x => x.OutgoingOrderId == null || x.IncomingOrderId == null).ToList();
           
            if (gcCases != null && gcCases.Count > 0)
            {
                foreach (var gcCase in gcCases)
                {
                    if (gcCase.IsCanceled == false && gcCase.IsWWCanceled==false) //check if case is not canceled or orders in ww are not canceled
					{
                        //var caseLifecycle = _context.GCCasesLifecycleStatusTypes.Where(x => x.GCCaseId == gcCase.Id && x.IsActiveStatus == true).Include(x => x.LifecycleStatusType).FirstOrDefault(); //get current active lifecycle for case
                        //var sterilizeLifecycle = _context.GCLifecycleStatusTypes.Where(x => x.Code == (int)LifecycleStatusType.Sterilize).FirstOrDefault();
                        //if (caseLifecycle != null && sterilizeLifecycle != null && caseLifecycle.LifecycleStatusType.Order >= sterilizeLifecycle.Order) //create orders only when case is past sterilize lifecycle
                        //{
                        //    if (gcCase.OutgoingOrderId == null)
                        //    {
                        //        var result = await AddOrderToWWAsync(gcCase.Id, true, false);//parameters to create only outgoing order
                        //    }
                        //    if (gcCase.IncomingOrderId == null)
                        //    {
                        //        var result = await AddOrderToWWAsync(gcCase.Id, false, true);//parameters to create only outgoing order
                        //    }
                        //}
                        if (gcCase.OutgoingOrderId == null)
                        {
                            var result = await AddOrderToWWAsync(gcCase.Id, true, false);//parameters to create only outgoing order
                            finalResult = finalResult + result?.Message;
                            finalResult = finalResult + "<br>";
                        }
                        if (gcCase.IncomingOrderId == null)
                        {
                            var result = await AddOrderToWWAsync(gcCase.Id, false, true);//parameters to create only outgoing order
                            finalResult = finalResult + result?.Message;
                            finalResult = finalResult + "<br>";
                        }
                    }
                    else if(gcCase.IsCanceled == true)
                    {
                        finalResult = gcCase.Title + "[Error] - Case is Canceled";
                        finalResult = finalResult + "<br>";
                    }
                    else if (gcCase.IsWWCanceled == true)
                    {
                        finalResult = gcCase.Title + "[Error] - WorkWave orders are Canceled";
                        finalResult = finalResult + "<br>";
                    }
                }
            }
            return finalResult;

        }

        public async Task GetGPSDevicesCurrentPos()
		{
            var result = await WorkWaveClient.GetGPSDevicesCurrentPos();
            if(result != null && result.CurrentInfos!= null && result.CurrentInfos.Count>0)
			{
				foreach (var info in result.CurrentInfos)
				{

                    var device = _context.WWGPSDevices.Where(x => x.DeviceId == info.Key).FirstOrDefault();
                    if(device!=null && info.Value != null)
					{
                        var timestamp = DateTimeOffset.FromUnixTimeSeconds(info.Value.Ts);
                        var sample = new WWGPSDeviceSample()
                        {
                            WWGPSDeviceId = device.Id,
                            DeviceId = info.Key,
                            Heading = info.Value.Heading,
                            Latitude = info.Value.LatLng[0],
                            Longitude = info.Value.LatLng[1],
                            SpeedMs = info.Value.SpeedMs,
                            Timestamp = timestamp.UtcDateTime
                        };
                        _context.WWGPSDeviceSamples.Add(sample);
                        _context.SaveChanges();
					}
					
				}
			}
		}

        public async Task SyncGPSDevices()
		{
            var result = await WorkWaveClient.GetGPSDevices();
            if (result != null && result.Devices != null && result.Devices.Count > 0)
            {
                foreach (var device in result.Devices)
                {

                    var dbdevice = _context.WWGPSDevices.Where(x => x.DeviceId == device.Key).FirstOrDefault();
                    if (dbdevice != null) //if device exists in out db check if we need to update it
                    {
                        bool deviceChanged = false;
                        if (dbdevice.Category != device.Value.Category)
						{
                            dbdevice.Category = device.Value.Category;
                            deviceChanged = true;
                        }
                        if(dbdevice.DeviceId != device.Value.Id)
						{
                            dbdevice.DeviceId = device.Value.Id;
                            deviceChanged = true;
                        }
                        if (dbdevice.Label != device.Value.Label)
						{
                            dbdevice.Label = device.Value.Label;
                            deviceChanged = true;
                        }
                        if(deviceChanged)
						{
                            _context.WWGPSDevices.Add(dbdevice);
                            _context.SaveChanges();
                        }
                    }
                    else //if device doesnt exist, create new
					{
                        var wwdevice = new WWGPSDevice()
                        {
                            Category = device.Value.Category,
                            DeviceId = device.Value.Id,
                            Label = device.Value.Label
                        };
                        _context.WWGPSDevices.Add(wwdevice);
                        _context.SaveChanges();
                    }
                }
            }
        }
    }
}
