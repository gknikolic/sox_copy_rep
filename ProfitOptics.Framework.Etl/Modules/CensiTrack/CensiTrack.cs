﻿using AutoMapper;
using CommandLine;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Etl.Helpers;
using ProfitOptics.Framework.Etl.Models;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Services;
//using ProfitOptics.Modules.Sox.Models.CensiTrack.ETL;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Etl.Modules.CensiTrack
{
    [Verb(ProfitOptics.Modules.Sox.Helpers.Constants.ETL_CT_MODULE_NAME, HelpText = "Commands for CensiTrack integration.")]
    public class CensiTrackModuleOptions : IOptions
    {
        [Option('u', "username", Required = false, HelpText = "Login Username.")]
        public string UserName { get; set; }

        [Option('p', "password", Required = false, HelpText = "Login Password.")]
        public string Password { get; set; }

        [Option('c', "client", Required = false, HelpText = "Client Id.")]
        public int ClientId { get; set; }

        [Option('f', "facility", Required = false, HelpText = "Facility Id.")]
        public long FacilityId { get; set; }

        [Option('a', "action", Required = true, HelpText = "Action type Enumerated value of type: CTActionType.")]
        public string Action { get; set; }

        [Option('s', "service", Required = false, HelpText = "Service Id.")]
        public long ServiceId { get; set; }

        [Option('t', "container", Required = false, HelpText = "Container Id.")]
        public long ContainerId { get; set; }

        [Option('o', "order", Required = false, HelpText = "Order (Case) reference.")]
        public string CaseReference { get; set; }

        [Option('k', "kids", Required = false, HelpText = "Fetch child object. 0 = false, 1 = true, 2 = true(async)")]
        public string FetchChildObjects { get; set; }

        [Option('x', "cycleId", Required = false, HelpText = "Cycle Id")]
        public string CycleId { get; set; }

        [Option('b', "bypassContainerServices", Required = false, HelpText = "Bypass Container Services")]
        public string BypassContainerServices { get; set; }

        [Option('h', "stopHistory", Required = false, HelpText = "Stop history from loading")]
        public string StopHistoryFromLoading { get; set; }

        [Option('r', "runAnyway", Required = false, HelpText = "Run desired action anyway")]
        public string RunAnyway { get; set; }
    }

    public class CensiTrackModule : IModule
    {
        private readonly ICensiTrackService _service;
        private readonly IGroundControlService _groundControlService;
        private readonly ICTCaseService _ctCaseService;
        private readonly ICTContainerService _ctContainerService;
        private readonly ICTProductService _ctProductService;
        private readonly ICTBIService _ctBIService;
        private readonly IMapper _mapper;
        private readonly IEtlHelper _etlHelper;
        private readonly Settings _settings;
        private readonly CensiTracOptions _censiTracOptions;

        public CensiTrackModule(ICensiTrackService service, 
            ICTCaseService ctCaseService,
            IGroundControlService groundControlService,
            ICTContainerService ctContainerService,
            ICTProductService ctProductService,
            ICTBIService ctBIService,
            IMapper mapper,
            IEtlHelper etlHelper,
            Settings settings,
            CensiTracOptions censiTracOptions) 
        {
            _service = service;
            _groundControlService = groundControlService;
            _ctCaseService = ctCaseService;
            _ctContainerService = ctContainerService;
            _ctProductService = ctProductService;
            _ctBIService = ctBIService;
            _mapper = mapper;
            _etlHelper = etlHelper;
            _settings = settings;
            _censiTracOptions = censiTracOptions;
        }

        public int Execute(IOptions opts)
        {
            var ctOpts = opts as CensiTrackModuleOptions;
            CTActionType ctActionType = (CTActionType)(int.Parse(ctOpts.Action));

            ETLLogs etlLog = new ETLLogs();
            etlLog.Source = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_CT_MODULE_NAME;
            etlLog.FileName = "no file";
            etlLog.Username = "no username";
            etlLog.Status = ETLStatusType.OK.ToString();
            etlLog.Message = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE;
            etlLog.RowCount = 0;
            etlLog.ActionType = (int)ctActionType;
            etlLog.Options = $@"UserName: {ctOpts.UserName}; Password: ******; ClientId: {ctOpts.ClientId}; FacilityId: {ctOpts.FacilityId}; Action: {ctOpts.Action}; ServiceId: {ctOpts.ServiceId}; ContainerId: {ctOpts.ContainerId}; CaseReference: {ctOpts.CaseReference}; FetchChildObjects: {ctOpts.FetchChildObjects};";
            etlLog.Description = "-";

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            try
            {
                switch (ctActionType)
                {
                    case CTActionType.GetCTCases:
                        var getCTCasesResponse = GetCTCases(ctOpts).GetAwaiter().GetResult();
                        if(!string.IsNullOrWhiteSpace(getCTCasesResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTCasesResponse.Message;
                        }
                        etlLog.RowCount = getCTCasesResponse.RowCount;
                        break;
                    case CTActionType.GetCTFacilities:
                        //var getCTFacilitiesResponse = GetCTFacilities(ctOpts).GetAwaiter().GetResult();
                        var getCTFacilitiesResponse = _groundControlService.CT_GetCTFacilities(_censiTracOptions.ClientId, 0, _censiTracOptions.UserName, _censiTracOptions.Password).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTFacilitiesResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTFacilitiesResponse.Message;
                        }
                        etlLog.RowCount = getCTFacilitiesResponse.RowCount;
                        break;
                    case CTActionType.GetCTContainerServices:
                        var getCTContainerServicesResponse = GetCTContainerServices(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTContainerServicesResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTContainerServicesResponse.Message;
                        }
                        etlLog.RowCount = getCTContainerServicesResponse.RowCount;
                        etlLog.Description = getCTContainerServicesResponse.Description;
                        break;
                    case CTActionType.GetCTContainersByService:
                        var getCTContainersByServiceResponse = GetCTContainerTypesByService(ctOpts);
                        var getCTContainersByServiceResponseResult = getCTContainersByServiceResponse.GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTContainersByServiceResponseResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTContainersByServiceResponseResult.Message;
                        }
                        etlLog.RowCount = getCTContainersByServiceResponseResult.RowCount;
                        etlLog.Description = getCTContainersByServiceResponseResult.Description;
                        break;
                    case CTActionType.GetCTProducts:
                        //var getCTProductsResponse = GetCTProducts(ctOpts).GetAwaiter().GetResult();
                        var getCTProductsResponse = _groundControlService.CT_GetCTProducts(_censiTracOptions.ClientId, 0, _censiTracOptions.UserName, _censiTracOptions.Password).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTProductsResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTProductsResponse.Message;
                        }
                        etlLog.RowCount = getCTProductsResponse.RowCount;
                        break;
                    case CTActionType.GetCTVendors:
                        var getCTVendorsResponse = GetCTVendors(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTVendorsResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTVendorsResponse.Message;
                        }
                        etlLog.RowCount = getCTVendorsResponse.RowCount;
                        break;
                    case CTActionType.GetCTContainerItems:
                        var getCTContainerItemsResponse = GetCTContainerItems(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTContainerItemsResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTContainerItemsResponse.Message;
                        }
                        etlLog.RowCount = getCTContainerItemsResponse.RowCount;
                        break;
                    case CTActionType.GetCTCaseContainers:
                        var getCTCaseContainersResponse = GetCTCaseContainers(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTCaseContainersResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTCaseContainersResponse.Message;
                        }
                        etlLog.RowCount = getCTCaseContainersResponse.RowCount;
                        break;
                    case CTActionType.GetCTCasesContainersAndSubmitToWW:
                        if (ctOpts.BypassContainerServices == null || !ctOpts.BypassContainerServices.Equals("2"))
                        {
                            var getCTCasesContainersAndSubmitToWWResponse = GetCTCasesContainersAndSubmitToWW(ctOpts).GetAwaiter().GetResult();
                            if (!string.IsNullOrWhiteSpace(getCTCasesContainersAndSubmitToWWResponse.Message))
                            {
                                etlLog.Status = ETLStatusType.Error.ToString();
                                etlLog.Message = getCTCasesContainersAndSubmitToWWResponse.Message;
                            }
                            etlLog.RowCount = getCTCasesContainersAndSubmitToWWResponse.RowCount;
                            etlLog.Description = getCTCasesContainersAndSubmitToWWResponse.Description;
                        }
                        break;
                    case CTActionType.GetCTContainerActualsByContainerType:
                        var getCTContainerActualsByContainerTypeResponse = GetCTContainerActualsByContainerType(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTContainerActualsByContainerTypeResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTContainerActualsByContainerTypeResponse.Message;
                        }
                        etlLog.RowCount = getCTContainerActualsByContainerTypeResponse.RowCount;
                        etlLog.Description = getCTContainerActualsByContainerTypeResponse.Description;
                        break;
                    case CTActionType.GetCTLocationTypes:
                        var getCTLocationTypesResponse = _groundControlService.CT_GetCTLocationTypes(_censiTracOptions.ClientId, 0, _censiTracOptions.UserName, _censiTracOptions.Password).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTLocationTypesResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTLocationTypesResponse.Message;
                        }
                        etlLog.RowCount = getCTLocationTypesResponse.RowCount;
                        etlLog.Description = getCTLocationTypesResponse.Description;
                        break;
                    case CTActionType.GetCTLocations:
                        var getCTLocationsResponse = _groundControlService.CT_GetCTLocations(_censiTracOptions.ClientId, 0, _censiTracOptions.UserName, _censiTracOptions.Password).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTLocationsResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTLocationsResponse.Message;
                        }
                        etlLog.RowCount = getCTLocationsResponse.RowCount;
                        etlLog.Description = getCTLocationsResponse.Description;
                        break;
                    case CTActionType.GetContainerTypeActualHistory:
                        bool isStopLifecycleHistory = ctOpts.StopHistoryFromLoading != null && ctOpts.StopHistoryFromLoading == "1";
                        long actualId = ctOpts.ServiceId;
                        string cycleId = string.Empty;
                        var getContainerTypeActualHistoryResponse = _groundControlService.CT_GetContainerTypeActualHistory(_censiTracOptions.ClientId, 0, _censiTracOptions.UserName, _censiTracOptions.Password, _settings.ServerOffsetInMinsFromUTC, isStopLifecycleHistory, cycleId, actualId).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getContainerTypeActualHistoryResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getContainerTypeActualHistoryResponse.Message;
                        }
                        etlLog.RowCount = getContainerTypeActualHistoryResponse.RowCount;
                        etlLog.Description = getContainerTypeActualHistoryResponse.Description;
                        break;
                    case CTActionType.GetCTContainerTypeGraphic:
                        //var getCTContainerTypeGraphicResponse = GetCTContainerTypeGraphic(ctOpts).GetAwaiter().GetResult();
                        //if (!string.IsNullOrWhiteSpace(getCTContainerTypeGraphicResponse.Message))
                        //{
                        //    etlLog.Status = ETLStatusType.Error.ToString();
                        //    etlLog.Message = getCTContainerTypeGraphicResponse.Message;
                        //}
                        //etlLog.RowCount = getCTContainerTypeGraphicResponse.RowCount;
                        //etlLog.Description = getCTContainerTypeGraphicResponse.Description;
                        break;
                    case CTActionType.GetCTContainerTypeGraphicMedia:
                        bool syncAllActuals = ctOpts.RunAnyway != null && ctOpts.RunAnyway == "1";
                        var updateAllTrays = ctOpts.CycleId != null && ctOpts.CycleId == "1";
                        var getCTContainerTypeGraphicMediaResult = _groundControlService.CT_GetCTContainerTypeGraphicMediaAndTrayInductionSyncAndLCUpdate(_censiTracOptions.ClientId, 0, _censiTracOptions.UserName, _censiTracOptions.Password, syncAllActuals, updateAllTrays).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTContainerTypeGraphicMediaResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTContainerTypeGraphicMediaResult.Message;
                        }
                        etlLog.RowCount = getCTContainerTypeGraphicMediaResult.RowCount;
                        etlLog.Description = getCTContainerTypeGraphicMediaResult.Description;
                        break;
                    case CTActionType.GetCTBILoadResults:
                        var getCTBILoadResultsResult = GetCTBILoadResults(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTBILoadResultsResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTBILoadResultsResult.Message;
                        }
                        etlLog.RowCount = getCTBILoadResultsResult.RowCount;
                        etlLog.Description = getCTBILoadResultsResult.Description;
                        break;
                    case CTActionType.SyncTimestamps:
                        var testingResult = SyncTimestampResults(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(testingResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = testingResult.Message;
                        }
                        etlLog.RowCount = testingResult.RowCount;
                        etlLog.Description = testingResult.Description;
                        break;
                    case CTActionType.GetContainerAssemblyMediaForActuals:
                        var getContainerAssemblyMediaForActualsResult = GetContainerAssemblyMediaForActuals(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getContainerAssemblyMediaForActualsResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getContainerAssemblyMediaForActualsResult.Message;
                        }
                        etlLog.RowCount = getContainerAssemblyMediaForActualsResult.RowCount;
                        etlLog.Description = getContainerAssemblyMediaForActualsResult.Description;
                        break;
                    case CTActionType.SyncLCForPastCases:
                        var syncLCForPastCasesResult = SyncLCForPastCases(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(syncLCForPastCasesResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = syncLCForPastCasesResult.Message;
                        }
                        etlLog.RowCount = syncLCForPastCasesResult.RowCount;
                        etlLog.Description = syncLCForPastCasesResult.Description;
                        break;
                    case CTActionType.GetServerTime:
                        Log.Information($"Server time: {DateTime.Now}");
                        break;
                    case CTActionType.CreateGCImageThumbnails:
                        var createGCImageThumbnailsResult = CreateGCImageThumbnails().GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(createGCImageThumbnailsResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = createGCImageThumbnailsResult.Message;
                        }
                        etlLog.RowCount = createGCImageThumbnailsResult.RowCount;
                        etlLog.Description = createGCImageThumbnailsResult.Description;
                        break;
                    case CTActionType.CompleteBrokenTrayHistory:
                        var completeBrokenTrayHistoryResult = CompleteBrokenTrayHistory(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(completeBrokenTrayHistoryResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = completeBrokenTrayHistoryResult.Message;
                        }
                        etlLog.RowCount = completeBrokenTrayHistoryResult.RowCount;
                        etlLog.Description = completeBrokenTrayHistoryResult.Description;
                        break;
                    case CTActionType.GetQualityFeed:
                        int daysBehind = 0;
                        if(ctOpts.RunAnyway != null && int.TryParse(ctOpts.RunAnyway, out daysBehind))
                        {
                            // daysBehind value already set to ctOpts.RunAnyway
                        }
                        else
                        {
                            daysBehind = 1;
                        }
                        var getQualityFeedQAPassResult = _groundControlService.CT_GetQualityFeedResults(_censiTracOptions.ClientId, _censiTracOptions.UserName, _censiTracOptions.Password, daysBehind, _settings.ServerOffsetInMinsFromUTC).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getQualityFeedQAPassResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getQualityFeedQAPassResult.Message;
                        }
                        etlLog.RowCount = getQualityFeedQAPassResult.RowCount;
                        etlLog.Description = getQualityFeedQAPassResult.Description;
                        break;
                    default:
                        break;
                }

                //etlLog.Description = string.Empty;
            }
            catch (Exception e)
            {
                etlLog.Status = ETLStatusType.Error.ToString();
                etlLog.Message = e.Message;
                etlLog.Description = e.StackTrace;
                if (e.InnerException != null)
                {
                    etlLog.Description += $" InnerException: {e.InnerException.Message}";
                }
                Log.Error(e.Message);
            }
            finally
            {
                etlLog.ProcessingTime = DateTime.UtcNow;
                stopwatch.Stop();
                etlLog.Username = stopwatch.Elapsed.ToString();
                if(etlLog.Description == null)
                {
                    etlLog.Description = "-";
                }
                _etlHelper.LogProcessedAction(etlLog.Username, etlLog.Source, etlLog.FileName, etlLog.ProcessingTime.ToString(), etlLog.Status, etlLog.RowCount, etlLog.Message, etlLog.ActionType, etlLog.Options, etlLog.Description);
            }
            

            return 0;
        }

        public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTCases(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTCases - Fetch process started.");

            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTCases - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctCases = await _service.GetCTCasesV2(options.ClientId);
                if (ctCases.Count > 0)
                {
                   _ctCaseService.AddOrEditCTCases(ctCases.Select(x => _mapper.Map<Framework.DataLayer.CTCase>(x)).ToList());
                }
                result.RowCount = ctCases.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        //public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTFacilities(CensiTrackModuleOptions options)
        //{
        //    Log.Information("GetCTFacilities = Fetch process started.");

        //    var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
        //    string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
        //    var description = $"GetCTFacilities - CycleId: {cycleId} | ";

        //    // TODO: Adapt this once CensiTrack changes Authentication:
        //    var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
        //    if (loginResponse.LoginSuccessful)
        //    {
        //        var ctFacilities = await _service.GetCTFacilities(options.ClientId);
        //        if (ctFacilities.Count > 0)
        //        {
        //            _ctFacilityService.AddOrEditCTFacilities(ctFacilities.Select(x => _mapper.Map<Framework.DataLayer.CTFacility>(x)).ToList());
        //        }
        //        result.RowCount = ctFacilities.Count;
        //        result.Description = description;
        //    }
        //    else
        //    {
        //        result.Message = loginResponse.Message;
        //    }

        //    Log.Information("Fetch process ended.");

        //    return result;
        //}

        public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTContainerServices(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTContainerServices - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTContainerServices - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctContainerServices = await _service.GetCTContainerServices(options.ClientId);
                if (ctContainerServices.Count > 0)
                {
                    //_ctContainerService.AddOrEditGCVendors(ctContainerServices.Select(x => _mapper.Map<Framework.DataLayer.GCVendor>(x)).ToList());
                    _ctContainerService.AddOrEditCTContainerServices(ctContainerServices.Select(x => _mapper.Map<Framework.DataLayer.CTContainerService>(x)).ToList());
                }
                result.RowCount = ctContainerServices.Count;
                description += $"Fetched {ctContainerServices.Count} CTContainerServices.";

                // fetching ContainerTypes
                if (!string.IsNullOrWhiteSpace(options.FetchChildObjects) && (options.FetchChildObjects.Equals("1") || options.FetchChildObjects.Equals("2")))
                {
                    ctContainerServices.ForEach(containerService =>
                    {
                        options.Action = ((int)CTActionType.GetCTContainersByService).ToString();
                        options.CycleId = cycleId;
                        options.ServiceId = containerService.Id;
                        Execute(options);
                    });
                }

                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTContainerTypesByService(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTContainerTypesByService - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            var description = $"GetCTContainerTypesByService - CycleId: {options.CycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                // TODO: Handle case when ServiceId is not provided - Get for all existing services
                var etlCTContainerTypes = await _service.GetCTContainerTypesByService(options.ClientId, options.FacilityId, options.ServiceId);
                if (etlCTContainerTypes != null)
                {
                    if (etlCTContainerTypes.Count > 0)
                    {
                        // Getting more info from a ContainerType:
                        var ctCTContainerTypes = etlCTContainerTypes.Select(x => _mapper.Map<Framework.DataLayer.CTContainerType>(x)).ToList();

                        foreach (var ctContainerType in ctCTContainerTypes)
                        {
                            ETLCTContainerType etlCTContainerType = await _service.GetCTContainerTypeItem(options.ClientId, ctContainerType.LegacyId);
                            if (etlCTContainerType != null)
                            {
                                ctContainerType.FacilityId = etlCTContainerType.FacilityId;
                                ctContainerType.SterilizationMethodId = etlCTContainerType.SterilizationMethodId;
                                ctContainerType.FirstAlternateMethodId = etlCTContainerType.FirstAlternateMethodId;
                                ctContainerType.SecondAlternateMethodId = etlCTContainerType.SecondAlternateMethodId;
                                ctContainerType.ThirdAlternateMethodId = etlCTContainerType.ThirdAlternateMethodId;
                                ctContainerType.HomeLocationId = etlCTContainerType.HomeLocationId;
                                ctContainerType.ProcessingLocationId = etlCTContainerType.ProcessingLocationId;
                                ctContainerType.PhysicianName = etlCTContainerType.PhysicianName;
                                ctContainerType.UsesBeforeService = etlCTContainerType.UsesBeforeService;
                                ctContainerType.UsageInterval = etlCTContainerType.UsageInterval;
                                ctContainerType.UsageIntervalUnitOfMeasure = etlCTContainerType.UsageIntervalUnitOfMeasure;
                                ctContainerType.StandardAssemblyTime = etlCTContainerType.StandardAssemblyTime;
                                ctContainerType.Weight = etlCTContainerType.Weight;
                                ctContainerType.ProcessingCost = etlCTContainerType.ProcessingCost;
                                ctContainerType.ProcurementReferenceId = etlCTContainerType.ProcurementReferenceId;
                                ctContainerType.DecontaminationInstructions = etlCTContainerType.DecontaminationInstructions;
                                ctContainerType.AssemblyInstructions = etlCTContainerType.AssemblyInstructions;
                                ctContainerType.SterilizationInstructions = etlCTContainerType.SterilizationInstructions;
                                ctContainerType.CountSheetComments = etlCTContainerType.CountSheetComments;
                                ctContainerType.PriorityId = etlCTContainerType.PriorityId;
                                ctContainerType.BiologicalTestRequired = etlCTContainerType.BiologicalTestRequired;
                                ctContainerType.ClassSixTestRequired = etlCTContainerType.ClassSixTestRequired;
                                ctContainerType.AssemblyByException = etlCTContainerType.AssemblyByException;
                                ctContainerType.SoftWrap = etlCTContainerType.SoftWrap;
                                ctContainerType.ComplexityLevelId = etlCTContainerType.ComplexityLevelId;
                                ctContainerType.OriginalContainerTypeId = etlCTContainerType.OriginalContainerTypeId;
                                ctContainerType.VendorId = etlCTContainerType.VendorId;
                                ctContainerType.VendorName = etlCTContainerType.VendorName;
                            }
                        }

                        _ctContainerService.AddOrEditCTContainerTypes(ctCTContainerTypes, options.ServiceId);
                    }

                    result.RowCount = etlCTContainerTypes.Count;
                    description += $"Feched {etlCTContainerTypes.Count} ContainerTypes";
                }

                // Fetching actuals for inventory:
                if (!string.IsNullOrWhiteSpace(options.FetchChildObjects) && (options.FetchChildObjects.Equals("1") || options.FetchChildObjects.Equals("2")))
                {
                    etlCTContainerTypes.ForEach(ctContainerType =>
                    {
                        options.Action = ((int)CTActionType.GetCTContainerActualsByContainerType).ToString();
                        options.ServiceId = ctContainerType.Id;
                        Execute(options);
                    });

                    //etlCTContainerTypes.ForEach(ctContainerType =>
                    //{
                    //    options.Action = ((int)CTActionType.GetCTContainerItems).ToString();
                    //    options.ServiceId = ctContainerType.Id;
                    //    Execute(options);
                    //});

                    etlCTContainerTypes.ForEach(ctContainerType =>
                    {
                        options.Action = ((int)CTActionType.GetCTContainerTypeGraphic).ToString();
                        options.ServiceId = ctContainerType.Id;
                        Execute(options);
                    });
                }
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        //public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTProducts(CensiTrackModuleOptions options)
        //{
        //    Log.Information("GetCTProducts - Fetch process started.");
        //    var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
        //    var description = $"GetCTProducts - CycleId: {options.CycleId} | ";

        //    // TODO: Adapt this once CensiTrack changes Authentication:
        //    var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
        //    if (loginResponse.LoginSuccessful)
        //    {
        //        var ctProducts = await _service.GetCTProducts(options.ClientId);
        //        if (ctProducts.Count > 0)
        //        {
        //            _ctProductService.AddOrEditCTProducts(ctProducts.Select(x => _mapper.Map<Framework.DataLayer.CTProduct>(x)).ToList());
        //        }
        //        result.RowCount = ctProducts.Count;
        //        result.Description = description;
        //    }
        //    else
        //    {
        //        result.Message = loginResponse.Message;
        //    }

        //    Log.Information("Fetch process ended.");

        //    return result;
        //}

        public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTVendors(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTVendors - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            var description = $"GetCTVendors - CycleId: {options.CycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctVendors = await _service.GetCTVendors(options.ClientId);
                if (ctVendors.Count > 0)
                {
                    _ctProductService.AddOrEditCTVendors(ctVendors.Select(x => _mapper.Map<Framework.DataLayer.CTVendor>(x)).ToList());
                }
                result.RowCount = ctVendors.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTContainerItems(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTContainerItems - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            var description = $"GetCTContainerItems - CycleId: {options.CycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctContainerItems = await _service.GetCTContainerTypeItems(options.ClientId, options.ServiceId);
                if (ctContainerItems.Count > 0)
                {
                    _ctContainerService.AddOrEditCTContainerItems(ctContainerItems.Select(x => _mapper.Map<Framework.DataLayer.CTContainerItem>(x)).ToList(), options.ServiceId);
                }
                result.RowCount = ctContainerItems.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTCaseContainers(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTCaseContainers - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            var description = $"GetCTCaseContainers - CycleId: {options.CycleId} | ";
            bool isStopLifecycleHistory = options.StopHistoryFromLoading != null && options.StopHistoryFromLoading == "1";


            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctCaseContainers = await _service.GetCTCaseContainers(options.ClientId, options.FacilityId, options.CaseReference);
                if (ctCaseContainers.Count > 0)
                {
                    _ctCaseService.AddOrEditCTCaseContainers(ctCaseContainers.Select(x => _mapper.Map<Framework.DataLayer.CTCaseContainer>(x)).ToList(), _settings.ServerOffsetInMinsFromUTC, isStopLifecycleHistory);
                }
                result.RowCount = ctCaseContainers.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTCasesContainersAndSubmitToWW(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTCasesContainersAndSubmitToWW - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            bool isStopLifecycleHistory = options.StopHistoryFromLoading != null && options.StopHistoryFromLoading == "1";

            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTCasesContainersAndSubmitToWW - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                Log.Information("Fetching CTCases...");
                var ctCases = await _service.GetCTCasesV2(options.ClientId);
                var ctContainerTypeActualLegacyIds = new List<long?>();
                if (ctCases.Count > 0)
                {
                    var ctCasesToUse = ctCases.ToList();

                    _ctCaseService.AddOrEditCTCases(ctCasesToUse.Select(x => _mapper.Map<Framework.DataLayer.CTCase>(x)).ToList());

                    ctCasesToUse.ForEach(ctCase =>
                    {
                        Log.Information("Fetching CTCaseContainers...");
                        var ctCaseContainers = _service.GetCTCaseContainers(options.ClientId, options.FacilityId, ctCase.CaseReference).Result;
                        if (ctCaseContainers.Count > 0)
                        {
                            description += $"Feched CaseReference: {ctCase.CaseReference}, containing {ctCaseContainers.Count} Containers | ";
                            try
                            {
                                // HistoryItems for Actuals need to be updated, since skipped in previous step
                                if (true)//options.BypassContainerServices != null && options.BypassContainerServices.Equals("1"))
                                {
                                    ctCaseContainers.ForEach(x =>
                                    {
                                        if (x.ContainerTypeId.HasValue)
                                        {
                                            var ctContainerType = _ctCaseService.GetCTContainerByContainerTypeId(x.ContainerTypeId.Value);
                                            if(ctContainerType == null)
                                            {
                                                Log.Information($"No ContainerType named: {x.ContainerName} found for legacyID: {x.ContainerTypeId.Value}, Fetching now...");
                                                ETLCTContainerType etlCTContainerType = _service.GetCTContainerTypesByService(options.ClientId, x.ContainerTypeId.Value).Result;

                                                if(etlCTContainerType != null)
                                                {
                                                    var ctContainerService = _ctCaseService.GetCTContainerServiceByLegacyId((int)etlCTContainerType.ServiceId);
                                                    if(ctContainerService == null)
                                                    {
                                                        Log.Information($"No ContainerService named: {etlCTContainerType.ServiceName} found for legacyID: {etlCTContainerType.ServiceId}, Fetching now...");
                                                        ETLCTContainerService etlCTContainerService = _service.GetCTContainerServiceByLegacyId(options.ClientId, etlCTContainerType.ServiceId).Result;

                                                        if(etlCTContainerService != null)
                                                        {
                                                            var etlCTContainerServices = new List<ETLCTContainerService>();
                                                            etlCTContainerServices.Add(etlCTContainerService);
                                                            _ctContainerService.AddOrEditCTContainerServices(etlCTContainerServices.Select(x => _mapper.Map<Framework.DataLayer.CTContainerService>(x)).ToList());
                                                        }
                                                    }

                                                    var etlCTContainerTypes = new List<ETLCTContainerType>();
                                                    etlCTContainerTypes.Add(etlCTContainerType);
                                                    _ctContainerService.AddOrEditCTContainerTypes(etlCTContainerTypes.Select(x => _mapper.Map<Framework.DataLayer.CTContainerType>(x)).ToList(), etlCTContainerType.ServiceId);
                                                }
                                            }
                                        }

                                        if (x.ContainerId.HasValue && x.ContainerId.Value > 0)
                                        {
                                            var ctContainerTypeActual = _ctCaseService.GetCTContainerTypeActualByLegacyId(x.ContainerId.Value);
                                            if (ctContainerTypeActual == null)
                                            {
                                                Log.Information($"No ContainerTypeActual named: {x.ContainerName} found for legacyID: {x.ContainerId.Value}, Fetching now...");
                                                ETLCTContainerTypeActual etlCTContainerTypeActual = null;

                                                var actuals = _service.GetCTContainerTypeActualssByContainerTypes(options.ClientId, x.ContainerTypeId.Value).Result;
                                                if(actuals != null)
                                                {
                                                    etlCTContainerTypeActual = actuals.Where(y => y.Id == x.ContainerId.Value).FirstOrDefault();
                                                }

                                                if (etlCTContainerTypeActual != null)
                                                {
                                                    var etlCTContainerTypeActuals = new List<ETLCTContainerTypeActual>();
                                                    etlCTContainerTypeActuals.Add(etlCTContainerTypeActual);
                                                    _ctContainerService.AddOrEditCTContainerTypeActuals(etlCTContainerTypeActuals.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeActual>(x)).ToList(), x.ContainerTypeId.Value, _settings.ServerOffsetInMinsFromUTC);
                                                }
                                            }


                                            var historyItemsResult = _groundControlService.CT_GetCTContainerTypeActualHistoryItems(options.ClientId, x.ContainerId.Value);
                                            if (historyItemsResult != null && historyItemsResult.Result.ETLCTContainerTypeActualHistoryItems != null)
                                            {
                                                var historyItems = historyItemsResult.Result.ETLCTContainerTypeActualHistoryItems;
                                                _groundControlService.CT_AddOrEditCTContainerTypeActualHistoryItemsAndGCTrays(historyItems.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeActualHistoryItem>(x)).ToList(), x.ContainerId.Value, _settings.ServerOffsetInMinsFromUTC, isStopLifecycleHistory, false);
                                            }
                                        }
                                    });
                                }

                                _ctCaseService.AddOrEditCTCaseContainers(ctCaseContainers.Select(x => _mapper.Map<Framework.DataLayer.CTCaseContainer>(x)).ToList(), _settings.ServerOffsetInMinsFromUTC, isStopLifecycleHistory);
                                //For testing only:
                                //var dummy = _ctCaseService.CaseTesting();
                                //_ctCaseService.AddOrEditCTCaseContainers(dummy, isStopLifecycleHistory);
                                ctContainerTypeActualLegacyIds.AddRange(ctCaseContainers.Select(x => x.ContainerId).ToList());
                            }
                            catch (Exception e)
                            {
                                var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                                var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                                Log.Error(error);
                                description += error;
                            }
                        }
                        else
                        {
                            // Check to see if trays are in 'Case Assembly' or 'Transportation Staging'
                            _groundControlService.CheckForCaseAssemblyAndTransportationStaging(options.ClientId, ctCase.CaseReference, _settings.ServerOffsetInMinsFromUTC, isStopLifecycleHistory);
                        }
                    });

                    try
                    {
                        Log.Information("Update LC for Completed Cases STARTED");
                        _ctContainerService.UpdateLCForCompletedCases(_settings.ServerOffsetInMinsFromUTC);
                        Log.Information("Update LC for Completed Cases FINISHED");

                        // First set GCTrayActiveStatus:
                        Log.Information("Setting GCTray Active Status STARTED");
                        _ctCaseService.SettingGCTrayActiveStatus(ctContainerTypeActualLegacyIds);
                        Log.Information("Setting GCTray Active Status FINISHED");
                        // Then do this:
                        // Only pulling cases that are in future and CartStatus is not Completed
                        Log.Information("Update Color Status for GCCases and GCTrays STARTED");
                        var localTime = ProfitOptics.Modules.Sox.Helpers.Helpers.GetLocalDateTime(DateTime.UtcNow);
                        Log.Information($"Local time is: {localTime}");
                        var gcCases = ctCasesToUse.Select(x => _mapper.Map<CTCase>(x)).ToList().Where(x => 
                            x.DueTimestamp > localTime && 
                            x.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE && 
                            x.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID &&
                            x.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE &&
                            x.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID
                        ).ToList();
                        if(gcCases != null && gcCases.Count > 0)
                        {
                            Log.Information($"Found: {gcCases.Count} GCCases for color update");
                            _ctContainerService.UpdateColorStatusForGCCasesAndGCTrays(gcCases);
                        }
                        Log.Information("Update Color Status for GCCases and GCTrays FINISHED");

                        Log.Information("DELETE or CANCEL Cases STARTED");
                        _ctContainerService.CancelOrDeleteCTCanceledCases(ctCasesToUse.Select(x => x.CaseReference).ToList(), _settings.ServerOffsetInMinsFromUTC);
                        Log.Information("DELETE or CANCEL Cases FINISHED");
                    }
                    catch (Exception e)
                    {
                        var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                        var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                        Log.Error(error);
                        description += error;
                    }

                    //Log.Information("Create WorkWave Orders...");
                    //_ctCaseService.CreateWorkWaveOrdersForCTCases();
                }
                result.RowCount = ctCases.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            return result;
        }

        public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTContainerActualsByContainerType(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTContainerActualsByContainerType - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            var description = $"GetCTContainerActualsByContainerType - CycleId: {options.CycleId} | ";

            // Using options.ServiceId as a parameter for ContainerTypeId
            long containerTypeId = options.ServiceId;

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctContainerTypeActuals = await _service.GetCTContainerTypeActualssByContainerTypes(options.ClientId, containerTypeId);
                if (ctContainerTypeActuals.Count > 0)
                {
                    _ctContainerService.AddOrEditCTContainerTypeActuals(ctContainerTypeActuals.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeActual>(x)).ToList(), options.ServiceId, _settings.ServerOffsetInMinsFromUTC);
                }
                result.RowCount = ctContainerTypeActuals.Count;
                result.Description = $"Feched {ctContainerTypeActuals.Count} ContainerTypeActuals for ContainerTypeID (parentId): {containerTypeId}";

                // Fetching actual history:
                if (!string.IsNullOrWhiteSpace(options.FetchChildObjects) && (options.FetchChildObjects.Equals("1") || options.FetchChildObjects.Equals("2")))
                {
                    ctContainerTypeActuals.ForEach(ctContainerTypeActual =>
                    {
                        var isStopLifecycleHistory = options.StopHistoryFromLoading != null && options.StopHistoryFromLoading == "1";
                        var cycleId = options.CycleId;
                        var actualId = ctContainerTypeActual.Id;
                        var getContainerTypeActualHistoryResponse = _groundControlService.CT_GetContainerTypeActualHistory(_censiTracOptions.ClientId, 0, _censiTracOptions.UserName, _censiTracOptions.Password, _settings.ServerOffsetInMinsFromUTC, isStopLifecycleHistory, cycleId, actualId).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getContainerTypeActualHistoryResponse.Message))
                        {
                            var message = getContainerTypeActualHistoryResponse.Message;
                            result.Message += message;
                            Log.Error(message);
                            throw new Exception(message);
                        }
                        result.Description += getContainerTypeActualHistoryResponse.Description;

                    });
                }
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        //public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTLocationTypes(CensiTrackModuleOptions options)
        //{
        //    Log.Information("GetCTLocationTypes - Fetch process started.");
        //    var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
        //    var description = "GetCTLocationTypes - ";

        //    // TODO: Adapt this once CensiTrack changes Authentication:
        //    var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
        //    if (loginResponse.LoginSuccessful)
        //    {
        //        var ctLocationTypes = await _service.GetCTLocationTypes(options.ClientId);
        //        if (ctLocationTypes.Count > 0)
        //        {
        //            _ctFacilityService.AddOrEditCTLocationTypes(ctLocationTypes.Select(x => _mapper.Map<Framework.DataLayer.CTLocationType>(x)).ToList());
        //        }
        //        result.RowCount = ctLocationTypes.Count;
        //        description += $"Feched {ctLocationTypes.Count} LocationTypes";
        //        result.Description = description;
        //    }
        //    else
        //    {
        //        result.Message = loginResponse.Message;
        //    }

        //    Log.Information("Fetch process ended.");

        //    return result;
        //}

        //public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTLocations(CensiTrackModuleOptions options)
        //{
        //    Log.Information("GetCTLocations - Fetch process started.");
        //    var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
        //    var description = "GetCTLocations - ";

        //    // TODO: Adapt this once CensiTrack changes Authentication:
        //    var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
        //    if (loginResponse.LoginSuccessful)
        //    {
        //        var ctLocations = _service.GetCTLocations(options.ClientId).Result.Locations;
        //        if (ctLocations.Count > 0)
        //        {
        //            _ctFacilityService.AddOrEditCTLocations(ctLocations.Select(x => _mapper.Map<Framework.DataLayer.CTLocation>(x)).ToList());
        //        }
        //        result.RowCount = ctLocations.Count;
        //        description += $"Feched {ctLocations.Count} Locations";
        //        result.Description = description;
        //    }
        //    else
        //    {
        //        result.Message = loginResponse.Message;
        //    }

        //    Log.Information("Fetch process ended.");

        //    return result;
        //}

        //public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetContainerTypeActualHistory(CensiTrackModuleOptions options)
        //{
        //    Log.Information("GetContainerTypeActualHistory - Fetch process started.");
        //    var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
        //    var description = $"GetContainerTypeActualHistory - CycleId: {options.CycleId} | ";
        //    bool isStopLifecycleHistory = options.StopHistoryFromLoading != null && options.StopHistoryFromLoading == "1";

        //    long actualId = options.ServiceId;

        //    // TODO: Adapt this once CensiTrack changes Authentication:
        //    var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
        //    if (loginResponse.LoginSuccessful)
        //    {
        //        var response = await _service.GetCTContainerTypeActualHistoryItems(options.ClientId, actualId);
        //        List<ETLCTContainerTypeActualHistoryItem> ctActualHistoryItems = response.ETLCTContainerTypeActualHistoryItems;
        //        if (ctActualHistoryItems != null)
        //        {
        //            if (ctActualHistoryItems.Count > 0)
        //            {
        //                _ctContainerService.AddOrEditCTContainerTypeActualHistoryItemsAndGCTrays(ctActualHistoryItems.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeActualHistoryItem>(x)).ToList(), options.ServiceId, _settings.ServerOffsetInMinsFromUTC, isStopLifecycleHistory, true);
        //            }
        //            result.RowCount = ctActualHistoryItems.Count;
        //            result.Description = $"Feched {ctActualHistoryItems.Count} CTContainerTypeActualHistoryItems for ActualId (parentId): {actualId}";
        //        }

        //        result.Description = description;
        //    }
        //    else
        //    {
        //        result.Message = loginResponse.Message;
        //    }

        //    Log.Information("Fetch process ended.");

        //    return result;
        //}

        //public async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTContainerTypeGraphic(CensiTrackModuleOptions options)
        //{
        //    Log.Information("GetCTContainerTypeGraphic - Fetch process started.");
        //    var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
        //    var description = $"GetCTContainerTypeGraphic - CycleId: {options.CycleId} | ";

        //    // TODO: Adapt this once CensiTrack changes Authentication:
        //    var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
        //    if (loginResponse.LoginSuccessful)
        //    {
        //        var ctContainerTypeGraphics = await _service.GetCTContainerTypeGraphic(options.ClientId, options.ServiceId);
        //        if (ctContainerTypeGraphics.Count > 0)
        //        {
        //            _ctContainerService.AddOrEditCTContainerTypeGraphics(ctContainerTypeGraphics.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeGraphic>(x)).ToList(), options.ServiceId);
        //        }
        //        result.RowCount = ctContainerTypeGraphics.Count;
        //        result.Description = description;
        //    }
        //    else
        //    {
        //        result.Message = loginResponse.Message;
        //    }

        //    Log.Information("Fetch process ended.");

        //    return result;
        //}

        //private async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTContainerTypeGraphicMediaAndTrayInductionSyncAndLCUpdate(CensiTrackModuleOptions options)
        //{
        //    Log.Information("GetCTContainerTypeGraphicMediaAndTrayInductionSyncAndLCUpdate - Fetch process started.");
        //    var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
        //    string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
        //    var description = $"GetCTContainerTypeGraphicMediaAndTrayInductionSyncAndLCUpdate - CycleId: {cycleId} | ";
        //    bool isWithoutImage = false;

        //    // TODO: Adapt this once CensiTrack changes Authentication:
        //    var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
        //    if (loginResponse.LoginSuccessful)
        //    {
        //        try
        //        {
        //            // Part 1 - CTContainerTypeGraphics
        //            isWithoutImage = true;
        //            Log.Information("Preparing to fetch bytes for CTContainerTypeGraphics.");
        //            var graphicsWithoutMedia = _ctContainerService.GetCTContainerTypeGraphic(isWithoutImage);
        //            Log.Information($"{graphicsWithoutMedia.Count} CTContainerTypeGraphics found in GC DB.");
        //            await GetCTContainerTypeGraphicMedia(options, result, graphicsWithoutMedia);
        //        }
        //        catch (Exception e)
        //        {
        //            var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
        //            var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
        //            Log.Error(error);
        //            description += error;
        //        }

        //        try
        //        { 
        //            // Part 2 - CTLoadIndicatorGraphics
        //            bool isLoadMeadiaWithoutImage = true;
        //            Log.Information("Preparing to fetch bytes for CTLoadResultGraphics.");
        //            var loadResultGraphicsWithoutMedia = _ctBIService.GetCTLoadResultGraphics(isLoadMeadiaWithoutImage);
        //            Log.Information($"{loadResultGraphicsWithoutMedia.Count} CTLoadResultGraphics found in GC DB.");
        //            await GetCTLoadResultGraphicMedia(options, result, loadResultGraphicsWithoutMedia);
        //        }
        //        catch (Exception e)
        //        {
        //            var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
        //            var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
        //            Log.Error(error);
        //            description += error;
        //        }

        //        try
        //        {
        //            // Part 3 - AssemblyGraphics
        //            isWithoutImage = true;
        //            Log.Information("Preparing to fetch bytes for CTAssemblyGraphics.");
        //            var assemblyGraphics = _ctContainerService.GetCTAssemblyGraphics(isWithoutImage);
        //            Log.Information($"{assemblyGraphics.Count} CTAssemblyGraphics found in GC DB.");
        //            await GetCTAssemblyGraphicMedia(options, result, assemblyGraphics);
        //        }
        //        catch (Exception e)
        //        {
        //            var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
        //            var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
        //            Log.Error(error);
        //            description += error;
        //        }
        //    }
        //    else
        //    {
        //        result.Message = loginResponse.Message;
        //    }

        //    try
        //    {
        //        Log.Information("Tray Induction synchronization STARTED");
        //        _groundControlService.UpdateTrayInductionSynchronization(options.RunAnyway);
        //        Log.Information("Tray Induction synchronization FINISHED");
        //    }
        //    catch (Exception e)
        //    {
        //        var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
        //        var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
        //        Log.Error(error);
        //        description += error;
        //    }

        //    try
        //    {
        //        Log.Information("Tray LifeCycle update STARTED");
        //        var updateAllTrays = options.CycleId != null && options.CycleId == "1";
        //        _groundControlService.UpdateGCTrayLifecycles(updateAllTrays);
        //        Log.Information("Tray LifeCycle update FINISHED");
        //    }
        //    catch (Exception e)
        //    {
        //        var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
        //        var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
        //        Log.Error(error);
        //        description += error;
        //    }

        //    result.Description = description;
        //    Log.Information("Fetch process ended.");
        //    return result;
        //}

        private async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetCTBILoadResults(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTBILoadResults - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTBILoadResults - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                Log.Information("Fetching CTBILoadResults...");
                var ctBILoadResults = await _service.GetCTBILoadResults(options.ClientId);
                
                if (ctBILoadResults.Count > 0)
                {
                    //_ctBIService.AddOrEditCTBILoadResults(ctBILoadResults.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResult>(x)).ToList());
                    Log.Information($"Fetched {ctBILoadResults.Count} CTBILoadResults");

                    ctBILoadResults.ForEach(ctBILoadResult =>
                    {
                        Log.Information($"-Processing CTBILoadResult, LoadId: {ctBILoadResult.LoadId}, Barcode: {ctBILoadResult.LoadBarcode}...");
                        bool isImportSuccessfull = false;
                        try
                        {
                            // CTBILoadResult Details & CTBILoadResultContent:
                            ETLCTBILoadResultContentResponse ctBILoadResultContentResponse = _service.GetCTBILoadResultContents(options.ClientId, ctBILoadResult.LoadId).Result;
                            Log.Information($"-Fetching CTBILoadResult Contents for loadId: {ctBILoadResult.LoadId}...");
                            if (ctBILoadResultContentResponse != null)
                            {
                                Log.Information($"-Updating CTBILoadResult Items for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}...");
                                isImportSuccessfull = _ctBIService.AddOrEditCTBILoadResult(_mapper.Map<Framework.DataLayer.CTBILoadResult>(ctBILoadResultContentResponse.LoadHeader), _settings.ServerOffsetInMinsFromUTC);

                                if (!isImportSuccessfull)
                                {
                                    Log.Information($"-Import of CTBILoadResult NOT YET Successfull LoadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}...");
                                    if (ctBILoadResultContentResponse.LoadHeader.LoadId > 0)
                                    {
                                        _ctBIService.AddOrEditCTBILoadResultContents(ctBILoadResultContentResponse.LoadItems.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResultContent>(x)).ToList(), ctBILoadResultContentResponse.LoadHeader.LoadId, _settings.ServerOffsetInMinsFromUTC);
                                    }
                                    else
                                    {
                                        Log.Information($"-LoadId is still 0! Skipping AddOrEditCTBILoadResultContents for now...");
                                    }
                                }
                            }
                            if (!isImportSuccessfull)
                            {
                                description += $"Fetched CTBILoadResult Items for loadId: { ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultContentResponse.LoadItems.Count} Contents | ";

                                // CTBILoadResult Indicators:
                                Log.Information($"Fetching CTBILoadResultIndicator Items for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}...");
                                ETLCTBILoadResultIndicatorResponse ctBILoadResultIndicatorResponse = _service.GetCTBILoadResultIndicators(options.ClientId, ctBILoadResultContentResponse.LoadHeader.LoadId).Result;
                                if (ctBILoadResultIndicatorResponse != null)
                                {
                                    Log.Information($"Fetched CTBILoadResult Indicators for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultIndicatorResponse.RequiredIndicators.Count} Indicators");
                                    _ctBIService.AddOrEditCTBILoadResultIndicators(ctBILoadResultIndicatorResponse.RequiredIndicators.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResultIndicator>(x)).ToList(), ctBILoadResultContentResponse.LoadHeader.LoadId, _settings.ServerOffsetInMinsFromUTC);
                                }
                                description += $"Fetched CTBILoadResult Indicators for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultIndicatorResponse.RequiredIndicators.Count} Indicators | ";

                                // CTBILoadResult Graphics:
                                Log.Information($"Fetching CTBILoadResultGraphic Items for loadId: {ctBILoadResult.LoadId}...");
                                List<ETLCTBILoadResultGraphic> ctBILoadResultGraphics = _service.GetCTBILoadResultGraphics(options.ClientId, ctBILoadResultContentResponse.LoadHeader.LoadId).Result;
                                if (ctBILoadResultGraphics != null)
                                {
                                    Log.Information($"Fetched CTBILoadResult Graphics for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultGraphics.Count} Graphics");
                                    _ctBIService.AddOrEditCTBILoadResultGraphics(ctBILoadResultGraphics.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResultGraphic>(x)).ToList(), ctBILoadResultContentResponse.LoadHeader.LoadId, _settings.ServerOffsetInMinsFromUTC);
                                }
                                description += $"Fetched CTBILoadResult Graphics for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultGraphics.Count} Graphics | ";
                                isImportSuccessfull = true;
                                _ctBIService.UpdateCTLoadResultImportStatus(ctBILoadResult.LoadId, isImportSuccessfull);
                            }
                            else
                            {
                                var msg = $"CTBILoadResult with LoadId: {ctBILoadResult.LoadId} already importet into GC!";
                                Log.Information(msg);
                                description += msg;
                            }
                        }
                        catch (Exception e)
                        {
                            var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                            var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                            Log.Error(error);
                            description += error;
                        }
                    });
                }
                result.RowCount = ctBILoadResults.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            return result;
        }

        private async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> SyncTimestampResults(CensiTrackModuleOptions options)
        {
            Log.Information("SyncTimestampResults - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"SyncTimestampResults - CycleId: {cycleId} | ";

            _ctContainerService.SyncTimestamps(_settings.ServerOffsetInMinsFromUTC);

            result.RowCount = 0;
            result.Description = description;

            return result;
        }

        private async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> SyncLCForPastCases(CensiTrackModuleOptions ctOpts)
        {
            Log.Information("SyncLCForPastCases - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"SyncLCForPastCases - CycleId: {cycleId} | ";

            if (ctOpts.BypassContainerServices == null || !ctOpts.BypassContainerServices.Equals("1"))
            {
                _ctContainerService.SyncLCForPastCases();
            }

            if (ctOpts.BypassContainerServices == null || !ctOpts.BypassContainerServices.Equals("2"))
            {
                _ctContainerService.SyncColorForPastCases();
            }

            result.RowCount = 0;
            result.Description = description;

            return result;
        }

        private async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> CreateGCImageThumbnails()
        {
            Log.Information("CreateGCImageThumbnails - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"CreateGCImageThumbnails - CycleId: {cycleId} | ";

            Log.Information($"CreateGCImageThumbnails STARTED...");
            _ctContainerService.CreateGCImageThumbnails();
            Log.Information($"CreateGCImageThumbnails FINISHED");

            result.RowCount = 0;
            result.Description = description;

            return result;
        }

        private async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> CompleteBrokenTrayHistory(CensiTrackModuleOptions ctOpts)
        {
            Log.Information("CompleteBrokenTrayHistory - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"CompleteBrokenTrayHistory - CycleId: {cycleId} | ";

            Log.Information($"UpdateHistoryItemAndLapCount STARTED...");
            bool allItems = false;
            _ctContainerService.UpdateHistoryItemAndLapCount(_settings.ServerOffsetInMinsFromUTC, allItems);
            Log.Information($"UpdateHistoryItemAndLapCount FINISHED...");

            Log.Information($"UpdateGCTrayLapCount STARTED...");
            bool allTrays = false;
            _ctContainerService.UpdateGCTrayLapCount(allTrays);
            Log.Information($"UpdateGCTrayLapCount FINISHED...");


            //Log.Information($"CompleteBrokenTrayHistory STARTED...");
            //List<int> gcTrayIds = null;
            //if(ctOpts.CaseReference != null)
            //{
            //    var trayIdsValue = ctOpts.CaseReference.Split(",").ToList();
            //    if(trayIdsValue != null && trayIdsValue.Count > 0)
            //    {
            //        gcTrayIds = new List<int>();
            //        trayIdsValue.ForEach(x =>
            //        {
            //            int gcTrayId = 0;
            //            if(int.TryParse(x, out gcTrayId) && gcTrayId > 0)
            //            {
            //                gcTrayIds.Add(gcTrayId);
            //            }
            //        });
            //    }
            //}
            //bool scanAllTrays = ctOpts.RunAnyway != null && ctOpts.RunAnyway == "1";
            //_ctContainerService.CompleteBrokenTrayHistory(gcTrayIds, scanAllTrays, _settings.ServerOffsetInMinsFromUTC);
            //Log.Information($"CompleteBrokenTrayHistory FINISHED");

            result.RowCount = 0;
            result.Description = description;

            return result;
        }

        private async Task<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel> GetContainerAssemblyMediaForActuals(CensiTrackModuleOptions options)
        {
            Log.Information("GetContainerAssemblyMediaForActuals - Fetch process started.");
            var result = new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetContainerAssemblyMediaForActuals - CycleId: {cycleId} | ";

            //TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await _groundControlService.CT_Login(new ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                List<long> containerAssemblyActualLegacyIds = _ctContainerService.GetContainerAssemblyActualIds();
                var statuses = _ctContainerService.GetAssemblyStatuses();
                if(statuses != null && statuses.Count>0)
				{
                    statuses.ForEach(assemblyStatus =>
                    {
                        Log.Information($"Fetching Container Found: {containerAssemblyActualLegacyIds.Count} CTContainerTypeActual items for CA media fetching");
                        var legacyId = assemblyStatus?.GCTray?.CTContainerTypeActual?.LegacyId ?? 0;
                        try
                        {
                            //legacyId = 1124; //FOR TESTING
                            //Graphics
                            ETLCTContainerAssembly etlCTContainerAssembly = _service.GetCTContainerAssemblyForActual(options.ClientId, legacyId).Result;

                            if (etlCTContainerAssembly != null && etlCTContainerAssembly.ContainerDetails != null && etlCTContainerAssembly.ContainerDetails.CensisSetHistoryId != null)
                            {
                                var containerAssemblyGraphics = _service.GetCTContainerAssemblyGraphics(options.ClientId, etlCTContainerAssembly.ContainerDetails.Id, (Guid)etlCTContainerAssembly.ContainerDetails.CensisSetHistoryId).Result;
                                if (containerAssemblyGraphics != null && containerAssemblyGraphics.AssemblyGraphics != null && containerAssemblyGraphics.AssemblyGraphics.Count > 0)
                                {
                                    containerAssemblyGraphics.AssemblyGraphics.ForEach(assemblyGraphic => {
                                        _ctContainerService.AddCTAssemblyGraphic(assemblyGraphic, assemblyStatus.Id, legacyId);
                                    });

                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Error($"{e.Message}, InnerExc: {e.InnerException}");
                        }

                        try
                        {
                            //Count Sheets
                            ETLCTContainerAssemblyCountSheet countSheets = _service.GetCTContainerAssemblyCountSheets(options.ClientId, legacyId).Result;
                            if (countSheets.Sets != null && countSheets.Sets.Count() > 0 && countSheets.Items != null && countSheets.Items.Count() > 0)
                            {
                                countSheets.Items.ForEach(countSheet =>
                                {
                                    _ctContainerService.AddCTAssemblyCountSheet(countSheet, countSheets.Sets[0]?.SetName, assemblyStatus.Id);
                                });
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Error($"{e.Message}, InnerExc: {e.InnerException}");
                        }

                    });
                }

                
                //if (containerAssemblyActualLegacyIds != null && containerAssemblyActualLegacyIds.Count > 0)
                //{
                //    Log.Information($"Found: {containerAssemblyActualLegacyIds.Count} CTContainerTypeActual items for CA media fetching");
                //    var test = 1124;
                //    containerAssemblyActualLegacyIds.ForEach(actualLegacyId =>
                //    {
                //         Log.Information($"Fetching Container Found: {containerAssemblyActualLegacyIds.Count} CTContainerTypeActual items for CA media fetching");

                //         ETLCTContainerAssembly etlCTContainerAssembly = _service.GetCTContainerAssemblyForActual(options.ClientId, test/*actualLegacyId*/).Result;
                //         var containerAssemblyGraphics = _service.GetCTContainerAssemblyGraphics(options.ClientId, etlCTContainerAssembly.ContainerDetails.Id, etlCTContainerAssembly.ContainerDetails.CensisSetHistoryId).Result;
                //         if (containerAssemblyGraphics != null && containerAssemblyGraphics.AssemblyGraphics != null && containerAssemblyGraphics.AssemblyGraphics.Count > 0)
                //         {
                //             containerAssemblyGraphics.AssemblyGraphics.ForEach(assemblyGraphic => {
                //                 _ctContainerService.AddCTAssemblyGraphic(assemblyGraphic);
                //             });

                //         }
                //    });
                //}
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            result.Description = description;
            Log.Information("Fetch process ended.");
            return result;
        }

        //private async Task GetCTContainerTypeGraphicMedia(CensiTrackModuleOptions options, ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel result, List<CTContainerTypeGraphic> graphicsWithoutMedia)
        //{
        //    if (graphicsWithoutMedia != null && graphicsWithoutMedia.Count > 0)
        //    {
        //        foreach (var ctContainerTypeGraphic in graphicsWithoutMedia)
        //        {
        //            Log.Information($"- Fetching Bytes for graphic ID: {ctContainerTypeGraphic.GraphicId}.");
        //            byte[] ctContainerTypeImage = null;
        //            try
        //            {
        //                ctContainerTypeImage = await _service.GetCTGraphicImage(options.ClientId, ctContainerTypeGraphic.GraphicId);
        //            }
        //            catch (Exception e)
        //            {

        //            }
                    
        //            if (ctContainerTypeImage != null)
        //            {
        //                Log.Information($"-- Bytes found for graphic ID: {ctContainerTypeGraphic.GraphicId}.");
        //                _ctContainerService.UpdateCTContainerTypeGraphic(ctContainerTypeGraphic.Id, ctContainerTypeImage);
        //                Log.Information($"-- Updated CTContainerTypeGraphic for graphic ID: {ctContainerTypeGraphic.GraphicId}.");
        //            }
        //            else
        //            {
        //                Log.Information($"-- NO Bytes found for graphic ID: {ctContainerTypeGraphic.GraphicId}.");
        //            }
        //            result.RowCount = 1;
                    
        //        }
        //    }
        //}

        //private async Task GetCTLoadResultGraphicMedia(CensiTrackModuleOptions options, ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel result, List<CTBILoadResultGraphic> loadResultGraphicsWithoutMedia)
        //{
        //    if (loadResultGraphicsWithoutMedia != null && loadResultGraphicsWithoutMedia.Count > 0)
        //    {
        //        foreach (var graphic in loadResultGraphicsWithoutMedia)
        //        {
        //            Log.Information($"- Fetching Bytes for graphic ID: {graphic.GraphicId}.");
        //            byte[] ctBILoadResultImage = null;
        //            try
        //            {
        //                ctBILoadResultImage = await _service.GetCTLoadResultGraphicImage(options.ClientId, graphic.GraphicId);
        //            }
        //            catch (Exception e)
        //            {
                        
        //            }

        //            if (ctBILoadResultImage != null)
        //            {
        //                Log.Information($"-- Bytes found for graphic ID: {graphic.GraphicId}.");
        //                _groundControlService.CT_UpdateCTLoadResultGraphic(graphic.Id, ctBILoadResultImage);
        //                Log.Information($"-- Updated CTLoadResultGraphic for graphic ID: {graphic.GraphicId}.");
        //            }
        //            else
        //            {
        //                Log.Information($"-- NO Bytes found for graphic ID: {graphic.GraphicId}.");
        //            }
        //            result.RowCount = 1;

        //        }
        //    }
        //}

        //private async Task GetCTAssemblyGraphicMedia(CensiTrackModuleOptions options, ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLResponseModel result, List<CTAssemblyGraphic> assemblyGraphics)
        //{
        //    if (assemblyGraphics != null && assemblyGraphics.Count > 0)
        //    {
        //        foreach (var graphic in assemblyGraphics)
        //        {
        //            Log.Information($"- Fetching Bytes for graphic ID: {graphic.GraphicId}.");
        //            byte[] graphicBytes = null;
        //            try
        //            {
        //                //graphicBytes = await _service.GetCTGraphicImage(options.ClientId, graphic.GraphicId);
        //                graphicBytes = await _groundControlService.CT_GetCTGraphicImage(options.ClientId, graphic.GraphicId);
        //            }
        //            catch (Exception)
        //            {

        //            }

        //            if (graphicBytes != null)
        //            {
        //                Log.Information($"-- Bytes found for graphic ID: {graphic.GraphicId}.");
        //                _ctContainerService.UpdateCTAssemblyGraphicsGraphic(graphic.Id, graphicBytes);
        //                Log.Information($"-- Updated CTAssemblyGraphicsGraphic for graphic ID: {graphic.GraphicId}.");
        //            }
        //            else
        //            {
        //                Log.Information($"-- NO Bytes found for graphic ID: {graphic.GraphicId}.");
        //            }
        //            result.RowCount = 1;

        //        }
        //    }
        //}
    }
}
