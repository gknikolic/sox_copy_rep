﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Core.Settings
{
    public class Ldap
    {
        public string Url { get; set; }
        public string BindOn { get; set; }
        public string BindCredentials { get; set; }
        public string SearchBase { get; set; }
        public string SearchFilter { get; set; }
        public string AdminCn { get; set; }
    }
}
