﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class HierarchySelectionModel
    {
        public MarginBridgePeriod FirstPeriod { get; set; }

        public MarginBridgePeriod SecondPeriod { get; set; }

        public List<ProductHierarchyFilterModel> ProductsHierarchy { get; set; }

        public List<LocationHierarchyFilterModel> LocationsHierarchy { get; set; }
    }

    public class HierarchyFilter
    {
        public string Code { get; set; }

        public bool Checked { get; set; }

        public int RowId { get; set; }

        public string Description { get; set; }
    }

    public class ProductHierarchyFilterModel : HierarchyFilter
    {
        public ProductLevelType FilterType { get; set; }

        public List<ProductHierarchyFilterModel> Items { get; set; }
    }

    public class LocationHierarchyFilterModel : HierarchyFilter
    {
        public LocationLevelType FilterType { get; set; }

        public List<LocationHierarchyFilterModel> Items { get; set; }
    }

    public class HierarchySelectionLevel
    {
        public string Caption { get; set; }

        public int Value { get; set; }
    }
}