﻿using System;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayAssemblyPhotos
	{
		public DateTime StatusTimestamp { get; set; }
		public int StatusId { get; set; }
		public string CaseReference { get; set; }
		public TrayHistoryPodContainer AssemblyPhotos { get; set; }
		public int NumOfPhotos { get; set; }

	}
}
