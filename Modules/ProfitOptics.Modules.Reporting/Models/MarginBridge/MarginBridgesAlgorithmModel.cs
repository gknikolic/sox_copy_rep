﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Attributes;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class OutputDataPoint
    {
        public string LocationKey { get; set; }
        public string ProductKey { get; set; }
        public double Price { get; set; }
        public double Cost { get; set; }
        public double Volume { get; set; }
        public double ProductMix { get; set; }
        public double RebateRate { get; set; }
        public double RebateVolume { get; set; }
        public double RebateProductMix { get; set; }
    }

    public class OutputData
    {
        public OutputData()
        {
            DataPoints = new List<OutputDataPoint>();
        }

        public List<OutputDataPoint> DataPoints { get; set; }
    }

    public class InputDataPointExt
    {
        public string Item { get; set; }
        public string ItemCategory { get; set; }
        public string ShipTo { get; set; }
        public string BillTo { get; set; }
        public string Territory { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }

        [FieldTitle("LocationKey")]
        public string LocationKey => ShipTo + BillTo + Territory + Region + Zone;
        [FieldTitle("ProductKey")]
        public string ProductKey => Item + ItemCategory;
        [FieldTitle("Price")]
        public decimal Price { get; set; }
        [FieldTitle("Cost")]
        public decimal Cost { get; set; }
        [FieldTitle("CostExtra")]
        public decimal CostExtra { get; set; }
        [FieldTitle("Qty")]
        public decimal Qty { get; set; }
        [FieldTitle("RebateDollars")]
        public decimal RebateDollars { get; set; }

        public decimal InvoiceAmount { get; set; }
        public decimal RebatedCost { get; set; }

        public decimal RebateRate => Cost == decimal.Zero ? decimal.Zero : 1 - (Cost - RebateDollars) / Cost;
    }

    public class InputData
    {
        public InputData()
        {
            DataPoints = new Dictionary<string, InputDataPointExt>();
        }

        public Dictionary<string, InputDataPointExt> DataPoints { get; set; }
    }
}