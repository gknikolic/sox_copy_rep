﻿using System;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTCase
    {
        public int ScheduleId { get; set; }
        public string CaseReference { get; set; }
        public DateTime DueTimestamp { get; set; }
        public string DestinationName { get; set; }
        public string CaseDoctor { get; set; }
        public string CaseCartName { get; set; }
        public string CartStatus { get; set; }
        public long? CaseCartId { get; set; }
        public int CaseStatus { get; set; }
        //public DateTime? CaseTime { get; set; }
        public int? CaseTime { get; set; }
        public bool? Shipped { get; set; }
        public string Changed { get; set; }
        public DateTime? PreviousDueTimestamp { get; set; }
        public string PreviousDestinationName { get; set; }
        public bool? Priority { get; set; }
        public long? destinationFacilityId { get; set; }

        //New props in v2:
        public string Location { get; set; }
        public long? ProcessTypeId { get; set; }
        public string Status { get; set; }
        public DateTime? EndTimestamp { get; set; }
        public bool? MultiCount { get; set; }


        // Not used in v2:
        //public long Facility { get; set; }
        //public bool? IsCartComplete { get; set; }
        //public long? CartStatusId { get; set; }
        //public string CaseCartLocationName { get; set; }
        //public bool IsCanceled { get; set; }
        //public DateTime? ExpediteTime { get; set; }
        //public decimal DueInMinutes { get; set; }
        //public string DueInHoursAndMinutes { get; set; }
        //public decimal PreviousDueInMinutes { get; set; }
        //public string PreviousDueInHoursAndMinutes { get; set; }
        //public DateTime DateTimeNow { get; set; }
        //public string EstimatedTime { get; set; }
    }
}
