﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class PodContainer
	{
		[JsonProperty("signatures")]
		public Dictionary<SignatureType,Pod> Signatures { get; set; }
		[JsonProperty("pictures")]
		public Dictionary<string, Pod> Pictures { get; set; }
		[JsonProperty("note")]
		public Pod Note { get; set; }
		[JsonProperty("barcodes")]
		public List<Barcode> Barcodes { get; set; }
	}
}
