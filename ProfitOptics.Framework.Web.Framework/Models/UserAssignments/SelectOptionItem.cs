﻿namespace ProfitOptics.Framework.Web.Framework.Models.UserAssignments
{
    public class SelectOptionItem
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
