USE [ProfitOptics.Framework]
GO
/****** Object:  Table [dbo].[ReportItemCategoryQuota]    Script Date: 14.8.2019. 16:16:34 ******/

 IF exists(SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ReportItemCategoryQuota' AND TABLE_CATALOG = 'ProfitOptics.Framework')
 BEGIN
 Drop Table ReportItemCategoryQuota
 END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportItemCategoryQuota](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesRepId] [int] NOT NULL,
	[ItemCategoryId] [int] NOT NULL,
	[Month] [nvarchar](20) NOT NULL,
	[Year] [int] NOT NULL,
	[Revenue] [decimal](19, 4) NOT NULL,
 CONSTRAINT [PK_Report_Kintrol_LibertyK2Knee_Quota] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET IDENTITY_INSERT [dbo].[ReportItemCategoryQuota] ON 
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1, 13, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (2, 13, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (3, 13, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (4, 13, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (5, 13, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (6, 13, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (7, 14, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (8, 14, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (9, 14, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (10, 14, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (11, 14, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (12, 14, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (13, 15, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (14, 15, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (15, 15, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (16, 15, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (17, 15, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (18, 15, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (19, 16, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (20, 16, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (21, 16, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (22, 16, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (23, 16, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (24, 16, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (25, 33, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (26, 33, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (27, 33, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (28, 33, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (29, 33, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (30, 33, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (31, 17, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (32, 17, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (33, 17, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (34, 17, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (35, 17, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (36, 17, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (37, 35, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (38, 35, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (39, 35, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (40, 35, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (41, 35, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (42, 35, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (43, 18, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (44, 18, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (45, 18, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (46, 18, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (47, 18, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (48, 18, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (49, 37, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (50, 37, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (51, 37, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (52, 37, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (53, 37, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (54, 37, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (55, 20, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (56, 20, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (57, 20, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (58, 20, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (59, 20, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (60, 20, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (61, 21, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (62, 21, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (63, 21, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (64, 21, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (65, 21, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (66, 21, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (67, 8, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (68, 8, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (69, 8, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (70, 8, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (71, 8, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (72, 8, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (73, 25, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (74, 25, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (75, 25, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (76, 25, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (77, 25, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (78, 25, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (79, 27, 205, N'Oct', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (80, 27, 205, N'Nov', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (81, 27, 205, N'Dec', 2018, CAST(3204.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (82, 27, 211, N'Oct', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (83, 27, 211, N'Nov', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (84, 27, 211, N'Dec', 2018, CAST(14500.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (85, 8, 181, N'Apr', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (86, 8, 181, N'Aug', 2018, CAST(13173.9439 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (87, 8, 181, N'Dec', 2018, CAST(12054.1586 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (88, 8, 181, N'Feb', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (89, 8, 181, N'Jan', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (90, 8, 181, N'Jul', 2018, CAST(13173.9439 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (91, 8, 181, N'Jun', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (92, 8, 181, N'Mar', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (93, 8, 181, N'May', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (94, 8, 181, N'Nov', 2018, CAST(12054.1586 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (95, 8, 181, N'Oct', 2018, CAST(12054.1586 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (96, 8, 181, N'Sep', 2018, CAST(12054.1586 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (97, 8, 188, N'Apr', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (98, 8, 188, N'Aug', 2018, CAST(81167.2665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (99, 8, 188, N'Dec', 2018, CAST(74268.0488 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (100, 8, 188, N'Feb', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (101, 8, 188, N'Jan', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (102, 8, 188, N'Jul', 2018, CAST(81167.2665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (103, 8, 188, N'Jun', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (104, 8, 188, N'Mar', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (105, 8, 188, N'May', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (106, 8, 188, N'Nov', 2018, CAST(74268.0488 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (107, 8, 188, N'Oct', 2018, CAST(74268.0488 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (108, 8, 188, N'Sep', 2018, CAST(74268.0488 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (109, 13, 181, N'Apr', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (110, 13, 181, N'Aug', 2018, CAST(38947.2382 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (111, 13, 181, N'Dec', 2018, CAST(35636.7230 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (112, 13, 181, N'Feb', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (113, 13, 181, N'Jan', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (114, 13, 181, N'Jul', 2018, CAST(38947.2382 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (115, 13, 181, N'Jun', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (116, 13, 181, N'Mar', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (117, 13, 181, N'May', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (118, 13, 181, N'Nov', 2018, CAST(35636.7230 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (119, 13, 181, N'Oct', 2018, CAST(35636.7230 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (120, 13, 181, N'Sep', 2018, CAST(35636.7230 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (121, 13, 188, N'Apr', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (122, 13, 188, N'Aug', 2018, CAST(75672.0538 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (123, 13, 188, N'Dec', 2018, CAST(69239.9292 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (124, 13, 188, N'Feb', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (125, 13, 188, N'Jan', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (126, 13, 188, N'Jul', 2018, CAST(75672.0538 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (127, 13, 188, N'Jun', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (128, 13, 188, N'Mar', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (129, 13, 188, N'May', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (130, 13, 188, N'Nov', 2018, CAST(69239.9292 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (131, 13, 188, N'Oct', 2018, CAST(69239.9292 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (132, 13, 188, N'Sep', 2018, CAST(69239.9292 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (133, 14, 181, N'Apr', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (134, 14, 181, N'Aug', 2018, CAST(43269.7190 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (135, 14, 181, N'Dec', 2018, CAST(39591.7929 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (136, 14, 181, N'Feb', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (137, 14, 181, N'Jan', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (138, 14, 181, N'Jul', 2018, CAST(43269.7190 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (139, 14, 181, N'Jun', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (140, 14, 181, N'Mar', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (141, 14, 181, N'May', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (142, 14, 181, N'Nov', 2018, CAST(39591.7929 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (143, 14, 181, N'Oct', 2018, CAST(39591.7929 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (144, 14, 181, N'Sep', 2018, CAST(39591.7929 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (145, 14, 188, N'Apr', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (146, 14, 188, N'Aug', 2018, CAST(119051.9236 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (147, 14, 188, N'Dec', 2018, CAST(108932.5101 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (148, 14, 188, N'Feb', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (149, 14, 188, N'Jan', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (150, 14, 188, N'Jul', 2018, CAST(119051.9236 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (151, 14, 188, N'Jun', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (152, 14, 188, N'Mar', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (153, 14, 188, N'May', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (154, 14, 188, N'Nov', 2018, CAST(108932.5101 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (155, 14, 188, N'Oct', 2018, CAST(108932.5101 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (156, 14, 188, N'Sep', 2018, CAST(108932.5101 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (157, 15, 181, N'Apr', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (158, 15, 181, N'Aug', 2018, CAST(29709.4318 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (159, 15, 181, N'Dec', 2018, CAST(27184.1301 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (160, 15, 181, N'Feb', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (161, 15, 181, N'Jan', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (162, 15, 181, N'Jul', 2018, CAST(29709.4318 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (163, 15, 181, N'Jun', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (164, 15, 181, N'Mar', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (165, 15, 181, N'May', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (166, 15, 181, N'Nov', 2018, CAST(27184.1301 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (167, 15, 181, N'Oct', 2018, CAST(27184.1301 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (168, 15, 181, N'Sep', 2018, CAST(27184.1301 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (169, 15, 188, N'Apr', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (170, 15, 188, N'Aug', 2018, CAST(62540.5899 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (171, 15, 188, N'Dec', 2018, CAST(57224.6398 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (172, 15, 188, N'Feb', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (173, 15, 188, N'Jan', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (174, 15, 188, N'Jul', 2018, CAST(62540.5899 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (175, 15, 188, N'Jun', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (176, 15, 188, N'Mar', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (177, 15, 188, N'May', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (178, 15, 188, N'Nov', 2018, CAST(57224.6398 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (179, 15, 188, N'Oct', 2018, CAST(57224.6398 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (180, 15, 188, N'Sep', 2018, CAST(57224.6398 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (181, 16, 181, N'Apr', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (182, 16, 181, N'Aug', 2018, CAST(39830.1967 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (183, 16, 181, N'Dec', 2018, CAST(36444.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (184, 16, 181, N'Feb', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (185, 16, 181, N'Jan', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (186, 16, 181, N'Jul', 2018, CAST(39830.1967 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (187, 16, 181, N'Jun', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (188, 16, 181, N'Mar', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (189, 16, 181, N'May', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (190, 16, 181, N'Nov', 2018, CAST(36444.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (191, 16, 181, N'Oct', 2018, CAST(36444.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (192, 16, 181, N'Sep', 2018, CAST(36444.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (193, 16, 188, N'Apr', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (194, 16, 188, N'Aug', 2018, CAST(66884.3408 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (195, 16, 188, N'Dec', 2018, CAST(61199.1719 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (196, 16, 188, N'Feb', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (197, 16, 188, N'Jan', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (198, 16, 188, N'Jul', 2018, CAST(66884.3408 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (199, 16, 188, N'Jun', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (200, 16, 188, N'Mar', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (201, 16, 188, N'May', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (202, 16, 188, N'Nov', 2018, CAST(61199.1719 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (203, 16, 188, N'Oct', 2018, CAST(61199.1719 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (204, 16, 188, N'Sep', 2018, CAST(61199.1719 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (205, 17, 181, N'Apr', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (206, 17, 181, N'Aug', 2018, CAST(42189.5149 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (207, 17, 181, N'Dec', 2018, CAST(38603.4061 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (208, 17, 181, N'Feb', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (209, 17, 181, N'Jan', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (210, 17, 181, N'Jul', 2018, CAST(42189.5149 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (211, 17, 181, N'Jun', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (212, 17, 181, N'Mar', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (213, 17, 181, N'May', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (214, 17, 181, N'Nov', 2018, CAST(38603.4061 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (215, 17, 181, N'Oct', 2018, CAST(38603.4061 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (216, 17, 181, N'Sep', 2018, CAST(38603.4061 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (217, 17, 188, N'Apr', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (218, 17, 188, N'Aug', 2018, CAST(104159.8350 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (219, 17, 188, N'Dec', 2018, CAST(95306.2490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (220, 17, 188, N'Feb', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (221, 17, 188, N'Jan', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (222, 17, 188, N'Jul', 2018, CAST(104159.8350 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (223, 17, 188, N'Jun', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (224, 17, 188, N'Mar', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (225, 17, 188, N'May', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (226, 17, 188, N'Nov', 2018, CAST(95306.2490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (227, 17, 188, N'Oct', 2018, CAST(95306.2490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (228, 17, 188, N'Sep', 2018, CAST(95306.2490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (229, 18, 181, N'Apr', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (230, 18, 181, N'Aug', 2018, CAST(29479.0748 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (231, 18, 181, N'Dec', 2018, CAST(26973.3535 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (232, 18, 181, N'Feb', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (233, 18, 181, N'Jan', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (234, 18, 181, N'Jul', 2018, CAST(29479.0748 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (235, 18, 181, N'Jun', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (236, 18, 181, N'Mar', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (237, 18, 181, N'May', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (238, 18, 181, N'Nov', 2018, CAST(26973.3535 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (239, 18, 181, N'Oct', 2018, CAST(26973.3535 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (240, 18, 181, N'Sep', 2018, CAST(26973.3535 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (241, 18, 188, N'Apr', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (242, 18, 188, N'Aug', 2018, CAST(44391.6794 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (243, 18, 188, N'Dec', 2018, CAST(40618.3866 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (244, 18, 188, N'Feb', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (245, 18, 188, N'Jan', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (246, 18, 188, N'Jul', 2018, CAST(44391.6794 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (247, 18, 188, N'Jun', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (248, 18, 188, N'Mar', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (249, 18, 188, N'May', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (250, 18, 188, N'Nov', 2018, CAST(40618.3866 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (251, 18, 188, N'Oct', 2018, CAST(40618.3866 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (252, 18, 188, N'Sep', 2018, CAST(40618.3866 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (253, 20, 181, N'Apr', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (254, 20, 181, N'Aug', 2018, CAST(29559.9881 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (255, 20, 181, N'Dec', 2018, CAST(27047.3891 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (256, 20, 181, N'Feb', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (257, 20, 181, N'Jan', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (258, 20, 181, N'Jul', 2018, CAST(29559.9881 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (259, 20, 181, N'Jun', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (260, 20, 181, N'Mar', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (261, 20, 181, N'May', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (262, 20, 181, N'Nov', 2018, CAST(27047.3891 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (263, 20, 181, N'Oct', 2018, CAST(27047.3891 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (264, 20, 181, N'Sep', 2018, CAST(27047.3891 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (265, 20, 188, N'Apr', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (266, 20, 188, N'Aug', 2018, CAST(90110.6628 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (267, 20, 188, N'Dec', 2018, CAST(82451.2565 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (268, 20, 188, N'Feb', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (269, 20, 188, N'Jan', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (270, 20, 188, N'Jul', 2018, CAST(90110.6628 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (271, 20, 188, N'Jun', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (272, 20, 188, N'Mar', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (273, 20, 188, N'May', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (274, 20, 188, N'Nov', 2018, CAST(82451.2565 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (275, 20, 188, N'Oct', 2018, CAST(82451.2565 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (276, 20, 188, N'Sep', 2018, CAST(82451.2565 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (277, 21, 181, N'Apr', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (278, 21, 181, N'Aug', 2018, CAST(27123.8849 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (279, 21, 181, N'Dec', 2018, CAST(24818.3547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (280, 21, 181, N'Feb', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (281, 21, 181, N'Jan', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (282, 21, 181, N'Jul', 2018, CAST(27123.8849 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (283, 21, 181, N'Jun', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (284, 21, 181, N'Mar', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (285, 21, 181, N'May', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (286, 21, 181, N'Nov', 2018, CAST(24818.3547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (287, 21, 181, N'Oct', 2018, CAST(24818.3547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (288, 21, 181, N'Sep', 2018, CAST(24818.3547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (289, 21, 188, N'Apr', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (290, 21, 188, N'Aug', 2018, CAST(55547.1531 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (291, 21, 188, N'Dec', 2018, CAST(50825.6451 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (292, 21, 188, N'Feb', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (293, 21, 188, N'Jan', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (294, 21, 188, N'Jul', 2018, CAST(55547.1531 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (295, 21, 188, N'Jun', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (296, 21, 188, N'Mar', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (297, 21, 188, N'May', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (298, 21, 188, N'Nov', 2018, CAST(50825.6451 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (299, 21, 188, N'Oct', 2018, CAST(50825.6451 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (300, 21, 188, N'Sep', 2018, CAST(50825.6451 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (301, 25, 181, N'Apr', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (302, 25, 181, N'Aug', 2018, CAST(28989.4034 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (303, 25, 181, N'Dec', 2018, CAST(26525.3041 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (304, 25, 181, N'Feb', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (305, 25, 181, N'Jan', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (306, 25, 181, N'Jul', 2018, CAST(28989.4034 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (307, 25, 181, N'Jun', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (308, 25, 181, N'Mar', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (309, 25, 181, N'May', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (310, 25, 181, N'Nov', 2018, CAST(26525.3041 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (311, 25, 181, N'Oct', 2018, CAST(26525.3041 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (312, 25, 181, N'Sep', 2018, CAST(26525.3041 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (313, 25, 188, N'Apr', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (314, 25, 188, N'Aug', 2018, CAST(41939.2588 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (315, 25, 188, N'Dec', 2018, CAST(38374.4218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (316, 25, 188, N'Feb', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (317, 25, 188, N'Jan', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (318, 25, 188, N'Jul', 2018, CAST(41939.2588 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (319, 25, 188, N'Jun', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (320, 25, 188, N'Mar', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (321, 25, 188, N'May', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (322, 25, 188, N'Nov', 2018, CAST(38374.4218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (323, 25, 188, N'Oct', 2018, CAST(38374.4218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (324, 25, 188, N'Sep', 2018, CAST(38374.4218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (325, 27, 181, N'Apr', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (326, 27, 181, N'Aug', 2018, CAST(40911.2026 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (327, 27, 181, N'Dec', 2018, CAST(37433.7504 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (328, 27, 181, N'Feb', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (329, 27, 181, N'Jan', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (330, 27, 181, N'Jul', 2018, CAST(40911.2026 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (331, 27, 181, N'Jun', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (332, 27, 181, N'Mar', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (333, 27, 181, N'May', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (334, 27, 181, N'Nov', 2018, CAST(37433.7504 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (335, 27, 181, N'Oct', 2018, CAST(37433.7504 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (336, 27, 181, N'Sep', 2018, CAST(37433.7504 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (337, 27, 188, N'Apr', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (338, 27, 188, N'Aug', 2018, CAST(59227.5492 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (339, 27, 188, N'Dec', 2018, CAST(54193.2076 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (340, 27, 188, N'Feb', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (341, 27, 188, N'Jan', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (342, 27, 188, N'Jul', 2018, CAST(59227.5492 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (343, 27, 188, N'Jun', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (344, 27, 188, N'Mar', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (345, 27, 188, N'May', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (346, 27, 188, N'Nov', 2018, CAST(54193.2076 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (347, 27, 188, N'Oct', 2018, CAST(54193.2076 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (348, 27, 188, N'Sep', 2018, CAST(54193.2076 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (349, 33, 181, N'Apr', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (350, 33, 181, N'Aug', 2018, CAST(72619.2760 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (351, 33, 181, N'Dec', 2018, CAST(66446.6376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (352, 33, 181, N'Feb', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (353, 33, 181, N'Jan', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (354, 33, 181, N'Jul', 2018, CAST(72619.2760 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (355, 33, 181, N'Jun', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (356, 33, 181, N'Mar', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (357, 33, 181, N'May', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (358, 33, 181, N'Nov', 2018, CAST(66446.6376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (359, 33, 181, N'Oct', 2018, CAST(66446.6376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (360, 33, 181, N'Sep', 2018, CAST(66446.6376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (361, 33, 188, N'Apr', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (362, 33, 188, N'Aug', 2018, CAST(144866.2306 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (363, 33, 188, N'Dec', 2018, CAST(132552.6010 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (364, 33, 188, N'Feb', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (365, 33, 188, N'Jan', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (366, 33, 188, N'Jul', 2018, CAST(144866.2306 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (367, 33, 188, N'Jun', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (368, 33, 188, N'Mar', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (369, 33, 188, N'May', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (370, 33, 188, N'Nov', 2018, CAST(132552.6010 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (371, 33, 188, N'Oct', 2018, CAST(132552.6010 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (372, 33, 188, N'Sep', 2018, CAST(132552.6010 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (373, 35, 181, N'Apr', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (374, 35, 181, N'Aug', 2018, CAST(58237.1994 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (375, 35, 181, N'Dec', 2018, CAST(53287.0374 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (376, 35, 181, N'Feb', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (377, 35, 181, N'Jan', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (378, 35, 181, N'Jul', 2018, CAST(58237.1994 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (379, 35, 181, N'Jun', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (380, 35, 181, N'Mar', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (381, 35, 181, N'May', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (382, 35, 181, N'Nov', 2018, CAST(53287.0374 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (383, 35, 181, N'Oct', 2018, CAST(53287.0374 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (384, 35, 181, N'Sep', 2018, CAST(53287.0374 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (385, 35, 188, N'Apr', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (386, 35, 188, N'Aug', 2018, CAST(91135.5789 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (387, 35, 188, N'Dec', 2018, CAST(83389.0547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (388, 35, 188, N'Feb', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (389, 35, 188, N'Jan', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (390, 35, 188, N'Jul', 2018, CAST(91135.5789 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (391, 35, 188, N'Jun', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (392, 35, 188, N'Mar', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (393, 35, 188, N'May', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (394, 35, 188, N'Nov', 2018, CAST(83389.0547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (395, 35, 188, N'Oct', 2018, CAST(83389.0547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (396, 35, 188, N'Sep', 2018, CAST(83389.0547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (397, 37, 181, N'Apr', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (398, 37, 181, N'Aug', 2018, CAST(27179.7085 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (399, 37, 181, N'Dec', 2018, CAST(24869.4333 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (400, 37, 181, N'Feb', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (401, 37, 181, N'Jan', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (402, 37, 181, N'Jul', 2018, CAST(27179.7085 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (403, 37, 181, N'Jun', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (404, 37, 181, N'Mar', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (405, 37, 181, N'May', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (406, 37, 181, N'Nov', 2018, CAST(24869.4333 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (407, 37, 181, N'Oct', 2018, CAST(24869.4333 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (408, 37, 181, N'Sep', 2018, CAST(24869.4333 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (409, 37, 188, N'Apr', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (410, 37, 188, N'Aug', 2018, CAST(83360.6626 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (411, 37, 188, N'Dec', 2018, CAST(76275.0062 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (412, 37, 188, N'Feb', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (413, 37, 188, N'Jan', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (414, 37, 188, N'Jul', 2018, CAST(83360.6626 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (415, 37, 188, N'Jun', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (416, 37, 188, N'Mar', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (417, 37, 188, N'May', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (418, 37, 188, N'Nov', 2018, CAST(76275.0062 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (419, 37, 188, N'Oct', 2018, CAST(76275.0062 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (420, 37, 188, N'Sep', 2018, CAST(76275.0062 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (421, 13, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (422, 13, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (423, 13, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (424, 13, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (425, 13, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (426, 13, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (427, 13, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (428, 13, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (429, 13, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (430, 13, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (431, 13, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (432, 13, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (433, 13, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (434, 13, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (435, 13, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (436, 13, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (437, 13, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (438, 13, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (439, 13, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (440, 13, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (441, 13, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (442, 13, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (443, 13, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (444, 13, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (445, 13, 205, N'Jan', 2019, CAST(59.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (446, 13, 205, N'Feb', 2019, CAST(61.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (447, 13, 205, N'Mar', 2019, CAST(74.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (448, 13, 205, N'Apr', 2019, CAST(65.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (449, 13, 205, N'May', 2019, CAST(72.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (450, 13, 205, N'Jun', 2019, CAST(75.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (451, 13, 205, N'Jul', 2019, CAST(72.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (452, 13, 205, N'Aug', 2019, CAST(84.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (453, 13, 205, N'Sep', 2019, CAST(67.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (454, 13, 205, N'Oct', 2019, CAST(79.3800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (455, 13, 205, N'Nov', 2019, CAST(69.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (456, 13, 205, N'Dec', 2019, CAST(67.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (457, 13, 188, N'Jan', 2019, CAST(70115.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (458, 13, 188, N'Feb', 2019, CAST(73101.3900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (459, 13, 188, N'Mar', 2019, CAST(88461.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (460, 13, 188, N'Apr', 2019, CAST(78058.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (461, 13, 188, N'May', 2019, CAST(85352.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (462, 13, 188, N'Jun', 2019, CAST(89676.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (463, 13, 188, N'Jul', 2019, CAST(85294.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (464, 13, 188, N'Aug', 2019, CAST(100467.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (465, 13, 188, N'Sep', 2019, CAST(80025.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (466, 13, 188, N'Oct', 2019, CAST(93901.0400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (467, 13, 188, N'Nov', 2019, CAST(82392.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (468, 13, 188, N'Dec', 2019, CAST(79822.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (469, 14, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (470, 14, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (471, 14, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (472, 14, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (473, 14, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (474, 14, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (475, 14, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (476, 14, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (477, 14, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (478, 14, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (479, 14, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (480, 14, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (481, 14, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (482, 14, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (483, 14, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (484, 14, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (485, 14, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (486, 14, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (487, 14, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (488, 14, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (489, 14, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (490, 14, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (491, 14, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (492, 14, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (493, 14, 205, N'Jan', 2019, CAST(1994.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (494, 14, 205, N'Feb', 2019, CAST(2079.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (495, 14, 205, N'Mar', 2019, CAST(2516.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (496, 14, 205, N'Apr', 2019, CAST(2220.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (497, 14, 205, N'May', 2019, CAST(2428.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (498, 14, 205, N'Jun', 2019, CAST(2551.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (499, 14, 205, N'Jul', 2019, CAST(2426.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (500, 14, 205, N'Aug', 2019, CAST(2858.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (501, 14, 205, N'Sep', 2019, CAST(2276.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (502, 14, 205, N'Oct', 2019, CAST(2671.3800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (503, 14, 205, N'Nov', 2019, CAST(2343.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (504, 14, 205, N'Dec', 2019, CAST(2270.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (505, 14, 188, N'Jan', 2019, CAST(104507.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (506, 14, 188, N'Feb', 2019, CAST(108956.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (507, 14, 188, N'Mar', 2019, CAST(131851.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (508, 14, 188, N'Apr', 2019, CAST(116344.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (509, 14, 188, N'May', 2019, CAST(127217.3900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (510, 14, 188, N'Jun', 2019, CAST(133661.3800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (511, 14, 188, N'Jul', 2019, CAST(127129.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (512, 14, 188, N'Aug', 2019, CAST(149746.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (513, 14, 188, N'Sep', 2019, CAST(119277.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (514, 14, 188, N'Oct', 2019, CAST(139958.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (515, 14, 188, N'Nov', 2019, CAST(122805.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (516, 14, 188, N'Dec', 2019, CAST(118974.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (517, 15, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (518, 15, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (519, 15, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (520, 15, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (521, 15, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (522, 15, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (523, 15, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (524, 15, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (525, 15, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (526, 15, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (527, 15, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (528, 15, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (529, 15, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (530, 15, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (531, 15, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (532, 15, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (533, 15, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (534, 15, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (535, 15, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (536, 15, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (537, 15, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (538, 15, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (539, 15, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (540, 15, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (541, 15, 205, N'Jan', 2019, CAST(1634.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (542, 15, 205, N'Feb', 2019, CAST(1704.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (543, 15, 205, N'Mar', 2019, CAST(2062.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (544, 15, 205, N'Apr', 2019, CAST(1819.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (545, 15, 205, N'May', 2019, CAST(1989.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (546, 15, 205, N'Jun', 2019, CAST(2090.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (547, 15, 205, N'Jul', 2019, CAST(1988.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (548, 15, 205, N'Aug', 2019, CAST(2342.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (549, 15, 205, N'Sep', 2019, CAST(1865.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (550, 15, 205, N'Oct', 2019, CAST(2189.3000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (551, 15, 205, N'Nov', 2019, CAST(1920.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (552, 15, 205, N'Dec', 2019, CAST(1861.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (553, 15, 188, N'Jan', 2019, CAST(50105.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (554, 15, 188, N'Feb', 2019, CAST(52238.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (555, 15, 188, N'Mar', 2019, CAST(63215.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (556, 15, 188, N'Apr', 2019, CAST(55780.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (557, 15, 188, N'May', 2019, CAST(60993.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (558, 15, 188, N'Jun', 2019, CAST(64083.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (559, 15, 188, N'Jul', 2019, CAST(60951.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (560, 15, 188, N'Aug', 2019, CAST(71795.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (561, 15, 188, N'Sep', 2019, CAST(57186.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (562, 15, 188, N'Oct', 2019, CAST(67102.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (563, 15, 188, N'Nov', 2019, CAST(58878.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (564, 15, 188, N'Dec', 2019, CAST(57041.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (565, 16, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (566, 16, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (567, 16, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (568, 16, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (569, 16, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (570, 16, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (571, 16, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (572, 16, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (573, 16, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (574, 16, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (575, 16, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (576, 16, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (577, 16, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (578, 16, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (579, 16, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (580, 16, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (581, 16, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (582, 16, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (583, 16, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (584, 16, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (585, 16, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (586, 16, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (587, 16, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (588, 16, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (589, 16, 205, N'Jan', 2019, CAST(1762.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (590, 16, 205, N'Feb', 2019, CAST(1837.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (591, 16, 205, N'Mar', 2019, CAST(2223.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (592, 16, 205, N'Apr', 2019, CAST(1961.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (593, 16, 205, N'May', 2019, CAST(2144.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (594, 16, 205, N'Jun', 2019, CAST(2253.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (595, 16, 205, N'Jul', 2019, CAST(2143.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (596, 16, 205, N'Aug', 2019, CAST(2524.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (597, 16, 205, N'Sep', 2019, CAST(2011.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (598, 16, 205, N'Oct', 2019, CAST(2359.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (599, 16, 205, N'Nov', 2019, CAST(2070.5900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (600, 16, 205, N'Dec', 2019, CAST(2006.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (601, 16, 188, N'Jan', 2019, CAST(105218.3000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (602, 16, 188, N'Feb', 2019, CAST(109698.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (603, 16, 188, N'Mar', 2019, CAST(132748.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (604, 16, 188, N'Apr', 2019, CAST(117136.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (605, 16, 188, N'May', 2019, CAST(128083.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (606, 16, 188, N'Jun', 2019, CAST(134570.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (607, 16, 188, N'Jul', 2019, CAST(127995.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (608, 16, 188, N'Aug', 2019, CAST(150765.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (609, 16, 188, N'Sep', 2019, CAST(120088.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (610, 16, 188, N'Oct', 2019, CAST(140910.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (611, 16, 188, N'Nov', 2019, CAST(123641.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (612, 16, 188, N'Dec', 2019, CAST(119784.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (613, 33, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (614, 33, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (615, 33, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (616, 33, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (617, 33, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (618, 33, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (619, 33, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (620, 33, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (621, 33, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (622, 33, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (623, 33, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (624, 33, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (625, 33, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (626, 33, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (627, 33, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (628, 33, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (629, 33, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (630, 33, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (631, 33, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (632, 33, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (633, 33, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (634, 33, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (635, 33, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (636, 33, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (637, 33, 205, N'Jan', 2019, CAST(1020.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (638, 33, 205, N'Feb', 2019, CAST(1064.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (639, 33, 205, N'Mar', 2019, CAST(1287.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (640, 33, 205, N'Apr', 2019, CAST(1136.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (641, 33, 205, N'May', 2019, CAST(1242.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (642, 33, 205, N'Jun', 2019, CAST(1305.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (643, 33, 205, N'Jul', 2019, CAST(1241.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (644, 33, 205, N'Aug', 2019, CAST(1462.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (645, 33, 205, N'Sep', 2019, CAST(1165.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (646, 33, 205, N'Oct', 2019, CAST(1367.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (647, 33, 205, N'Nov', 2019, CAST(1199.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (648, 33, 205, N'Dec', 2019, CAST(1162.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (649, 33, 188, N'Jan', 2019, CAST(86822.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (650, 33, 188, N'Feb', 2019, CAST(90519.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (651, 33, 188, N'Mar', 2019, CAST(109539.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (652, 33, 188, N'Apr', 2019, CAST(96656.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (653, 33, 188, N'May', 2019, CAST(105689.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (654, 33, 188, N'Jun', 2019, CAST(111043.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (655, 33, 188, N'Jul', 2019, CAST(105616.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (656, 33, 188, N'Aug', 2019, CAST(124406.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (657, 33, 188, N'Sep', 2019, CAST(99092.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (658, 33, 188, N'Oct', 2019, CAST(116274.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (659, 33, 188, N'Nov', 2019, CAST(102024.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (660, 33, 188, N'Dec', 2019, CAST(98841.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (661, 17, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (662, 17, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (663, 17, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (664, 17, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (665, 17, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (666, 17, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (667, 17, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (668, 17, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (669, 17, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (670, 17, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (671, 17, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (672, 17, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (673, 17, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (674, 17, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (675, 17, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (676, 17, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (677, 17, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (678, 17, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (679, 17, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (680, 17, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (681, 17, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (682, 17, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (683, 17, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (684, 17, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (685, 17, 205, N'Jan', 2019, CAST(951.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (686, 17, 205, N'Feb', 2019, CAST(992.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (687, 17, 205, N'Mar', 2019, CAST(1200.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (688, 17, 205, N'Apr', 2019, CAST(1059.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (689, 17, 205, N'May', 2019, CAST(1158.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (690, 17, 205, N'Jun', 2019, CAST(1217.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (691, 17, 205, N'Jul', 2019, CAST(1157.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (692, 17, 205, N'Aug', 2019, CAST(1363.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (693, 17, 205, N'Sep', 2019, CAST(1086.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (694, 17, 205, N'Oct', 2019, CAST(1274.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (695, 17, 205, N'Nov', 2019, CAST(1118.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (696, 17, 205, N'Dec', 2019, CAST(1083.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (697, 17, 188, N'Jan', 2019, CAST(68428.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (698, 17, 188, N'Feb', 2019, CAST(71342.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (699, 17, 188, N'Mar', 2019, CAST(86332.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (700, 17, 188, N'Apr', 2019, CAST(76179.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (701, 17, 188, N'May', 2019, CAST(83298.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (702, 17, 188, N'Jun', 2019, CAST(87517.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (703, 17, 188, N'Jul', 2019, CAST(83241.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (704, 17, 188, N'Aug', 2019, CAST(98049.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (705, 17, 188, N'Sep', 2019, CAST(78099.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (706, 17, 188, N'Oct', 2019, CAST(91641.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (707, 17, 188, N'Nov', 2019, CAST(80409.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (708, 17, 188, N'Dec', 2019, CAST(77901.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (709, 35, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (710, 35, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (711, 35, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (712, 35, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (713, 35, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (714, 35, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (715, 35, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (716, 35, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (717, 35, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (718, 35, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (719, 35, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (720, 35, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (721, 35, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (722, 35, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (723, 35, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (724, 35, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (725, 35, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (726, 35, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (727, 35, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (728, 35, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (729, 35, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (730, 35, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (731, 35, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (732, 35, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (733, 35, 205, N'Jan', 2019, CAST(356.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (734, 35, 205, N'Feb', 2019, CAST(371.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (735, 35, 205, N'Mar', 2019, CAST(449.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (736, 35, 205, N'Apr', 2019, CAST(397.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (737, 35, 205, N'May', 2019, CAST(434.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (738, 35, 205, N'Jun', 2019, CAST(456.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (739, 35, 205, N'Jul', 2019, CAST(433.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (740, 35, 205, N'Aug', 2019, CAST(510.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (741, 35, 205, N'Sep', 2019, CAST(407.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (742, 35, 205, N'Oct', 2019, CAST(477.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (743, 35, 205, N'Nov', 2019, CAST(419.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (744, 35, 205, N'Dec', 2019, CAST(405.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (745, 35, 188, N'Jan', 2019, CAST(67150.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (746, 35, 188, N'Feb', 2019, CAST(70009.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (747, 35, 188, N'Mar', 2019, CAST(84720.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (748, 35, 188, N'Apr', 2019, CAST(74757.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (749, 35, 188, N'May', 2019, CAST(81743.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (750, 35, 188, N'Jun', 2019, CAST(85883.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (751, 35, 188, N'Jul', 2019, CAST(81687.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (752, 35, 188, N'Aug', 2019, CAST(96219.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (753, 35, 188, N'Sep', 2019, CAST(76641.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (754, 35, 188, N'Oct', 2019, CAST(89930.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (755, 35, 188, N'Nov', 2019, CAST(78908.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (756, 35, 188, N'Dec', 2019, CAST(76446.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (757, 18, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (758, 18, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (759, 18, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (760, 18, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (761, 18, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (762, 18, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (763, 18, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (764, 18, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (765, 18, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (766, 18, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (767, 18, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (768, 18, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (769, 18, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (770, 18, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (771, 18, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (772, 18, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (773, 18, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (774, 18, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (775, 18, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (776, 18, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (777, 18, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (778, 18, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (779, 18, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (780, 18, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (781, 18, 205, N'Jan', 2019, CAST(561.7300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (782, 18, 205, N'Feb', 2019, CAST(585.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (783, 18, 205, N'Mar', 2019, CAST(708.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (784, 18, 205, N'Apr', 2019, CAST(625.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (785, 18, 205, N'May', 2019, CAST(683.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (786, 18, 205, N'Jun', 2019, CAST(718.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (787, 18, 205, N'Jul', 2019, CAST(683.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (788, 18, 205, N'Aug', 2019, CAST(804.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (789, 18, 205, N'Sep', 2019, CAST(641.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (790, 18, 205, N'Oct', 2019, CAST(752.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (791, 18, 205, N'Nov', 2019, CAST(660.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (792, 18, 205, N'Dec', 2019, CAST(639.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (793, 18, 188, N'Jan', 2019, CAST(49139.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (794, 18, 188, N'Feb', 2019, CAST(51231.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (795, 18, 188, N'Mar', 2019, CAST(61996.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (796, 18, 188, N'Apr', 2019, CAST(54705.3800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (797, 18, 188, N'May', 2019, CAST(59817.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (798, 18, 188, N'Jun', 2019, CAST(62847.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (799, 18, 188, N'Jul', 2019, CAST(59776.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (800, 18, 188, N'Aug', 2019, CAST(70410.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (801, 18, 188, N'Sep', 2019, CAST(56084.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (802, 18, 188, N'Oct', 2019, CAST(65808.5900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (803, 18, 188, N'Nov', 2019, CAST(57743.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (804, 18, 188, N'Dec', 2019, CAST(55941.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (805, 37, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (806, 37, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (807, 37, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (808, 37, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (809, 37, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (810, 37, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (811, 37, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (812, 37, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (813, 37, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (814, 37, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (815, 37, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (816, 37, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (817, 37, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (818, 37, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (819, 37, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (820, 37, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (821, 37, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (822, 37, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (823, 37, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (824, 37, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (825, 37, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (826, 37, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (827, 37, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (828, 37, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (829, 37, 205, N'Jan', 2019, CAST(183.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (830, 37, 205, N'Feb', 2019, CAST(191.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (831, 37, 205, N'Mar', 2019, CAST(231.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (832, 37, 205, N'Apr', 2019, CAST(204.3000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (833, 37, 205, N'May', 2019, CAST(223.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (834, 37, 205, N'Jun', 2019, CAST(234.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (835, 37, 205, N'Jul', 2019, CAST(223.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (836, 37, 205, N'Aug', 2019, CAST(262.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (837, 37, 205, N'Sep', 2019, CAST(209.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (838, 37, 205, N'Oct', 2019, CAST(245.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (839, 37, 205, N'Nov', 2019, CAST(215.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (840, 37, 205, N'Dec', 2019, CAST(208.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (841, 37, 188, N'Jan', 2019, CAST(69012.3900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (842, 37, 188, N'Feb', 2019, CAST(71950.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (843, 37, 188, N'Mar', 2019, CAST(87069.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (844, 37, 188, N'Apr', 2019, CAST(76829.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (845, 37, 188, N'May', 2019, CAST(84009.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (846, 37, 188, N'Jun', 2019, CAST(88264.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (847, 37, 188, N'Jul', 2019, CAST(83951.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (848, 37, 188, N'Aug', 2019, CAST(98886.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (849, 37, 188, N'Sep', 2019, CAST(78765.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (850, 37, 188, N'Oct', 2019, CAST(92423.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (851, 37, 188, N'Nov', 2019, CAST(81096.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (852, 37, 188, N'Dec', 2019, CAST(78566.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (853, 20, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (854, 20, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (855, 20, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (856, 20, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (857, 20, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (858, 20, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (859, 20, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (860, 20, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (861, 20, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (862, 20, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (863, 20, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (864, 20, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (865, 20, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (866, 20, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (867, 20, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (868, 20, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (869, 20, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (870, 20, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (871, 20, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (872, 20, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (873, 20, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (874, 20, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (875, 20, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (876, 20, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (877, 20, 205, N'Jan', 2019, CAST(1391.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (878, 20, 205, N'Feb', 2019, CAST(1450.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (879, 20, 205, N'Mar', 2019, CAST(1755.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (880, 20, 205, N'Apr', 2019, CAST(1548.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (881, 20, 205, N'May', 2019, CAST(1693.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (882, 20, 205, N'Jun', 2019, CAST(1779.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (883, 20, 205, N'Jul', 2019, CAST(1692.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (884, 20, 205, N'Aug', 2019, CAST(1993.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (885, 20, 205, N'Sep', 2019, CAST(1587.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (886, 20, 205, N'Oct', 2019, CAST(1863.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (887, 20, 205, N'Nov', 2019, CAST(1634.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (888, 20, 205, N'Dec', 2019, CAST(1583.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (889, 20, 188, N'Jan', 2019, CAST(69722.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (890, 20, 188, N'Feb', 2019, CAST(72691.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (891, 20, 188, N'Mar', 2019, CAST(87965.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (892, 20, 188, N'Apr', 2019, CAST(77620.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (893, 20, 188, N'May', 2019, CAST(84873.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (894, 20, 188, N'Jun', 2019, CAST(89172.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (895, 20, 188, N'Jul', 2019, CAST(84815.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (896, 20, 188, N'Aug', 2019, CAST(99904.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (897, 20, 188, N'Sep', 2019, CAST(79576.4600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (898, 20, 188, N'Oct', 2019, CAST(93374.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (899, 20, 188, N'Nov', 2019, CAST(81930.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (900, 20, 188, N'Dec', 2019, CAST(79374.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (901, 21, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (902, 21, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (903, 21, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (904, 21, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (905, 21, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (906, 21, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (907, 21, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (908, 21, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (909, 21, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (910, 21, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (911, 21, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (912, 21, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (913, 21, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (914, 21, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (915, 21, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (916, 21, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (917, 21, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (918, 21, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (919, 21, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (920, 21, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (921, 21, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (922, 21, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (923, 21, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (924, 21, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (925, 21, 205, N'Jan', 2019, CAST(461.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (926, 21, 205, N'Feb', 2019, CAST(481.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (927, 21, 205, N'Mar', 2019, CAST(582.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (928, 21, 205, N'Apr', 2019, CAST(514.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (929, 21, 205, N'May', 2019, CAST(562.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (930, 21, 205, N'Jun', 2019, CAST(590.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (931, 21, 205, N'Jul', 2019, CAST(561.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (932, 21, 205, N'Aug', 2019, CAST(661.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (933, 21, 205, N'Sep', 2019, CAST(527.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (934, 21, 205, N'Oct', 2019, CAST(618.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (935, 21, 205, N'Nov', 2019, CAST(542.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (936, 21, 205, N'Dec', 2019, CAST(525.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (937, 21, 188, N'Jan', 2019, CAST(71991.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (938, 21, 188, N'Feb', 2019, CAST(75057.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (939, 21, 188, N'Mar', 2019, CAST(90828.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (940, 21, 188, N'Apr', 2019, CAST(80146.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (941, 21, 188, N'May', 2019, CAST(87636.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (942, 21, 188, N'Jun', 2019, CAST(92075.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (943, 21, 188, N'Jul', 2019, CAST(87576.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (944, 21, 188, N'Aug', 2019, CAST(103155.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (945, 21, 188, N'Sep', 2019, CAST(82166.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (946, 21, 188, N'Oct', 2019, CAST(96413.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (947, 21, 188, N'Nov', 2019, CAST(84597.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (948, 21, 188, N'Dec', 2019, CAST(81958.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (949, 8, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (950, 8, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (951, 8, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (952, 8, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (953, 8, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (954, 8, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (955, 8, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (956, 8, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (957, 8, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (958, 8, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (959, 8, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (960, 8, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (961, 8, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (962, 8, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (963, 8, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (964, 8, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (965, 8, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (966, 8, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (967, 8, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (968, 8, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (969, 8, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (970, 8, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (971, 8, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (972, 8, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (973, 8, 205, N'Jan', 2019, CAST(328.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (974, 8, 205, N'Feb', 2019, CAST(342.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (975, 8, 205, N'Mar', 2019, CAST(414.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (976, 8, 205, N'Apr', 2019, CAST(366.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (977, 8, 205, N'May', 2019, CAST(400.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (978, 8, 205, N'Jun', 2019, CAST(420.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (979, 8, 205, N'Jul', 2019, CAST(400.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (980, 8, 205, N'Aug', 2019, CAST(471.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (981, 8, 205, N'Sep', 2019, CAST(375.3800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (982, 8, 205, N'Oct', 2019, CAST(440.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (983, 8, 205, N'Nov', 2019, CAST(386.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (984, 8, 205, N'Dec', 2019, CAST(374.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (985, 8, 188, N'Jan', 2019, CAST(69006.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (986, 8, 188, N'Feb', 2019, CAST(71944.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (987, 8, 188, N'Mar', 2019, CAST(87061.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (988, 8, 188, N'Apr', 2019, CAST(76822.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (989, 8, 188, N'May', 2019, CAST(84001.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (990, 8, 188, N'Jun', 2019, CAST(88256.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (991, 8, 188, N'Jul', 2019, CAST(83944.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (992, 8, 188, N'Aug', 2019, CAST(98877.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (993, 8, 188, N'Sep', 2019, CAST(78758.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (994, 8, 188, N'Oct', 2019, CAST(92414.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (995, 8, 188, N'Nov', 2019, CAST(81088.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (996, 8, 188, N'Dec', 2019, CAST(78559.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (997, 25, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (998, 25, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (999, 25, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1000, 25, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1001, 25, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1002, 25, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1003, 25, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1004, 25, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1005, 25, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1006, 25, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1007, 25, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1008, 25, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1009, 25, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1010, 25, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1011, 25, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1012, 25, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1013, 25, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1014, 25, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1015, 25, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1016, 25, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1017, 25, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1018, 25, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1019, 25, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1020, 25, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1021, 25, 205, N'Jan', 2019, CAST(722.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1022, 25, 205, N'Feb', 2019, CAST(753.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1023, 25, 205, N'Mar', 2019, CAST(911.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1024, 25, 205, N'Apr', 2019, CAST(804.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1025, 25, 205, N'May', 2019, CAST(879.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1026, 25, 205, N'Jun', 2019, CAST(923.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1027, 25, 205, N'Jul', 2019, CAST(878.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1028, 25, 205, N'Aug', 2019, CAST(1034.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1029, 25, 205, N'Sep', 2019, CAST(824.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1030, 25, 205, N'Oct', 2019, CAST(967.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1031, 25, 205, N'Nov', 2019, CAST(848.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1032, 25, 205, N'Dec', 2019, CAST(822.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1033, 25, 188, N'Jan', 2019, CAST(51778.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1034, 25, 188, N'Feb', 2019, CAST(53983.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1035, 25, 188, N'Mar', 2019, CAST(65326.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1036, 25, 188, N'Apr', 2019, CAST(57643.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1037, 25, 188, N'May', 2019, CAST(63030.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1038, 25, 188, N'Jun', 2019, CAST(66223.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1039, 25, 188, N'Jul', 2019, CAST(62987.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1040, 25, 188, N'Aug', 2019, CAST(74192.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1041, 25, 188, N'Sep', 2019, CAST(59096.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1042, 25, 188, N'Oct', 2019, CAST(69343.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1043, 25, 188, N'Nov', 2019, CAST(60845.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1044, 25, 188, N'Dec', 2019, CAST(58946.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1045, 27, 181, N'Jan', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1046, 27, 181, N'Feb', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1047, 27, 181, N'Mar', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1048, 27, 181, N'Apr', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1049, 27, 181, N'May', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1050, 27, 181, N'Jun', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1051, 27, 181, N'Jul', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1052, 27, 181, N'Aug', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1053, 27, 181, N'Sep', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1054, 27, 181, N'Oct', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1055, 27, 181, N'Nov', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1056, 27, 181, N'Dec', 2019, CAST(35649.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1057, 27, 211, N'Jan', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1058, 27, 211, N'Feb', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1059, 27, 211, N'Mar', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1060, 27, 211, N'Apr', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1061, 27, 211, N'May', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1062, 27, 211, N'Jun', 2019, CAST(28960.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1063, 27, 211, N'Jul', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1064, 27, 211, N'Aug', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1065, 27, 211, N'Sep', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1066, 27, 211, N'Oct', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1067, 27, 211, N'Nov', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1068, 27, 211, N'Dec', 2019, CAST(36200.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1069, 27, 205, N'Jan', 2019, CAST(949.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1070, 27, 205, N'Feb', 2019, CAST(989.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1071, 27, 205, N'Mar', 2019, CAST(1197.5900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1072, 27, 205, N'Apr', 2019, CAST(1056.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1073, 27, 205, N'May', 2019, CAST(1155.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1074, 27, 205, N'Jun', 2019, CAST(1214.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1075, 27, 205, N'Jul', 2019, CAST(1154.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1076, 27, 205, N'Aug', 2019, CAST(1360.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1077, 27, 205, N'Sep', 2019, CAST(1083.3800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1078, 27, 205, N'Oct', 2019, CAST(1271.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1079, 27, 205, N'Nov', 2019, CAST(1115.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1080, 27, 205, N'Dec', 2019, CAST(1080.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1081, 27, 188, N'Jan', 2019, CAST(57995.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1082, 27, 188, N'Feb', 2019, CAST(60465.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1083, 27, 188, N'Mar', 2019, CAST(73170.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1084, 27, 188, N'Apr', 2019, CAST(64565.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1085, 27, 188, N'May', 2019, CAST(70598.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1086, 27, 188, N'Jun', 2019, CAST(74175.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1087, 27, 188, N'Jul', 2019, CAST(70550.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1088, 27, 188, N'Aug', 2019, CAST(83101.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1089, 27, 188, N'Sep', 2019, CAST(66192.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1090, 27, 188, N'Oct', 2019, CAST(77669.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1091, 27, 188, N'Nov', 2019, CAST(68150.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportItemCategoryQuota] ([Id], [SalesRepId], [ItemCategoryId], [Month], [Year], [Revenue]) VALUES (1092, 27, 188, N'Dec', 2019, CAST(66024.7100 AS Decimal(19, 4)))
GO
SET IDENTITY_INSERT [dbo].[ReportItemCategoryQuota] OFF
GO

/****** Object:  Table [dbo].[ReportProductFamilyQuota]    Script Date: 14.8.2019. 16:16:34 ******/

 IF exists(SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ReportProductFamilyQuota' AND TABLE_CATALOG = 'ProfitOptics.Framework')
 BEGIN
 Drop Table ReportProductFamilyQuota
 END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportProductFamilyQuota](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesRepId] [int] NOT NULL,
	[ProductFamilyId] [int] NOT NULL,
	[Month] [nvarchar](20) NOT NULL,
	[Year] [int] NOT NULL,
	[Revenue] [decimal](19, 4) NOT NULL,
 CONSTRAINT [PK_ReportProductFamilyQuota] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET IDENTITY_INSERT [dbo].[ReportProductFamilyQuota] ON 
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1, 8, 39, N'Apr', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (2, 8, 39, N'Aug', 2018, CAST(13173.9439 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (3, 8, 39, N'Dec', 2018, CAST(12054.1586 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (4, 8, 39, N'Feb', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (5, 8, 39, N'Jan', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (6, 8, 39, N'Jul', 2018, CAST(13173.9439 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (7, 8, 39, N'Jun', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (8, 8, 39, N'Mar', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (9, 8, 39, N'May', 2018, CAST(13539.1645 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (10, 8, 39, N'Nov', 2018, CAST(12054.1586 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (11, 8, 39, N'Oct', 2018, CAST(12054.1586 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (12, 8, 39, N'Sep', 2018, CAST(12054.1586 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (13, 8, 40, N'Apr', 2018, CAST(57616.3793 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (14, 8, 40, N'Aug', 2018, CAST(69687.3861 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (15, 8, 40, N'Dec', 2018, CAST(63763.9583 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (16, 8, 40, N'Feb', 2018, CAST(57616.3793 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (17, 8, 40, N'Jan', 2018, CAST(57616.3793 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (18, 8, 40, N'Jul', 2018, CAST(69687.3861 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (19, 8, 40, N'Jun', 2018, CAST(57616.3793 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (20, 8, 40, N'Mar', 2018, CAST(57616.3793 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (21, 8, 40, N'May', 2018, CAST(57616.3793 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (22, 8, 40, N'Nov', 2018, CAST(63763.9583 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (23, 8, 40, N'Oct', 2018, CAST(63763.9583 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (24, 8, 40, N'Sep', 2018, CAST(63763.9583 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (25, 8, 41, N'Apr', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (26, 8, 41, N'Aug', 2018, CAST(81167.2665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (27, 8, 41, N'Dec', 2018, CAST(74268.0488 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (28, 8, 41, N'Feb', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (29, 8, 41, N'Jan', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (30, 8, 41, N'Jul', 2018, CAST(81167.2665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (31, 8, 41, N'Jun', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (32, 8, 41, N'Mar', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (33, 8, 41, N'May', 2018, CAST(64369.8668 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (34, 8, 41, N'Nov', 2018, CAST(74268.0488 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (35, 8, 41, N'Oct', 2018, CAST(74268.0488 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (36, 8, 41, N'Sep', 2018, CAST(74268.0488 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (37, 8, 42, N'Apr', 2018, CAST(9974.0167 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (38, 8, 42, N'Aug', 2018, CAST(12391.2013 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (39, 8, 42, N'Dec', 2018, CAST(11337.9492 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (40, 8, 42, N'Feb', 2018, CAST(9974.0167 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (41, 8, 42, N'Jan', 2018, CAST(9974.0167 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (42, 8, 42, N'Jul', 2018, CAST(12391.2013 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (43, 8, 42, N'Jun', 2018, CAST(9974.0167 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (44, 8, 42, N'Mar', 2018, CAST(9974.0167 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (45, 8, 42, N'May', 2018, CAST(9974.0167 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (46, 8, 42, N'Nov', 2018, CAST(11337.9492 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (47, 8, 42, N'Oct', 2018, CAST(11337.9492 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (48, 8, 42, N'Sep', 2018, CAST(11337.9492 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (49, 13, 39, N'Apr', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (50, 13, 39, N'Aug', 2018, CAST(38947.2382 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (51, 13, 39, N'Dec', 2018, CAST(35636.7230 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (52, 13, 39, N'Feb', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (53, 13, 39, N'Jan', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (54, 13, 39, N'Jul', 2018, CAST(38947.2382 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (55, 13, 39, N'Jun', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (56, 13, 39, N'Mar', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (57, 13, 39, N'May', 2018, CAST(27738.7576 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (58, 13, 39, N'Nov', 2018, CAST(35636.7230 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (59, 13, 39, N'Oct', 2018, CAST(35636.7230 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (60, 13, 39, N'Sep', 2018, CAST(35636.7230 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (61, 13, 40, N'Apr', 2018, CAST(110225.7197 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (62, 13, 40, N'Aug', 2018, CAST(127722.0853 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (63, 13, 40, N'Dec', 2018, CAST(116865.7080 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (64, 13, 40, N'Feb', 2018, CAST(110225.7197 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (65, 13, 40, N'Jan', 2018, CAST(110225.7197 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (66, 13, 40, N'Jul', 2018, CAST(127722.0853 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (67, 13, 40, N'Jun', 2018, CAST(110225.7197 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (68, 13, 40, N'Mar', 2018, CAST(110225.7197 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (69, 13, 40, N'May', 2018, CAST(110225.7197 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (70, 13, 40, N'Nov', 2018, CAST(116865.7080 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (71, 13, 40, N'Oct', 2018, CAST(116865.7080 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (72, 13, 40, N'Sep', 2018, CAST(116865.7080 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (73, 13, 41, N'Apr', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (74, 13, 41, N'Aug', 2018, CAST(75672.0538 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (75, 13, 41, N'Dec', 2018, CAST(69239.9292 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (76, 13, 41, N'Feb', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (77, 13, 41, N'Jan', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (78, 13, 41, N'Jul', 2018, CAST(75672.0538 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (79, 13, 41, N'Jun', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (80, 13, 41, N'Mar', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (81, 13, 41, N'May', 2018, CAST(66050.3129 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (82, 13, 41, N'Nov', 2018, CAST(69239.9292 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (83, 13, 41, N'Oct', 2018, CAST(69239.9292 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (84, 13, 41, N'Sep', 2018, CAST(69239.9292 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (85, 13, 42, N'Apr', 2018, CAST(17102.5921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (86, 13, 42, N'Aug', 2018, CAST(19516.2782 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (87, 13, 42, N'Dec', 2018, CAST(17857.3946 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (88, 13, 42, N'Feb', 2018, CAST(17102.5921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (89, 13, 42, N'Jan', 2018, CAST(17102.5921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (90, 13, 42, N'Jul', 2018, CAST(19516.2782 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (91, 13, 42, N'Jun', 2018, CAST(17102.5921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (92, 13, 42, N'Mar', 2018, CAST(17102.5921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (93, 13, 42, N'May', 2018, CAST(17102.5921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (94, 13, 42, N'Nov', 2018, CAST(17857.3946 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (95, 13, 42, N'Oct', 2018, CAST(17857.3946 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (96, 13, 42, N'Sep', 2018, CAST(17857.3946 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (97, 14, 39, N'Apr', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (98, 14, 39, N'Aug', 2018, CAST(43269.7190 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (99, 14, 39, N'Dec', 2018, CAST(39591.7929 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (100, 14, 39, N'Feb', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (101, 14, 39, N'Jan', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (102, 14, 39, N'Jul', 2018, CAST(43269.7190 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (103, 14, 39, N'Jun', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (104, 14, 39, N'Mar', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (105, 14, 39, N'May', 2018, CAST(31717.4810 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (106, 14, 39, N'Nov', 2018, CAST(39591.7929 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (107, 14, 39, N'Oct', 2018, CAST(39591.7929 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (108, 14, 39, N'Sep', 2018, CAST(39591.7929 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (109, 14, 40, N'Apr', 2018, CAST(75916.9826 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (110, 14, 40, N'Aug', 2018, CAST(88225.1635 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (111, 14, 40, N'Dec', 2018, CAST(80726.0246 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (112, 14, 40, N'Feb', 2018, CAST(75916.9826 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (113, 14, 40, N'Jan', 2018, CAST(75916.9826 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (114, 14, 40, N'Jul', 2018, CAST(88225.1635 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (115, 14, 40, N'Jun', 2018, CAST(75916.9826 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (116, 14, 40, N'Mar', 2018, CAST(75916.9826 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (117, 14, 40, N'May', 2018, CAST(75916.9826 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (118, 14, 40, N'Nov', 2018, CAST(80726.0246 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (119, 14, 40, N'Oct', 2018, CAST(80726.0246 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (120, 14, 40, N'Sep', 2018, CAST(80726.0246 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (121, 14, 41, N'Apr', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (122, 14, 41, N'Aug', 2018, CAST(119051.9236 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (123, 14, 41, N'Dec', 2018, CAST(108932.5101 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (124, 14, 41, N'Feb', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (125, 14, 41, N'Jan', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (126, 14, 41, N'Jul', 2018, CAST(119051.9236 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (127, 14, 41, N'Jun', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (128, 14, 41, N'Mar', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (129, 14, 41, N'May', 2018, CAST(103640.1164 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (130, 14, 41, N'Nov', 2018, CAST(108932.5101 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (131, 14, 41, N'Oct', 2018, CAST(108932.5101 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (132, 14, 41, N'Sep', 2018, CAST(108932.5101 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (133, 14, 42, N'Apr', 2018, CAST(12878.0790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (134, 14, 42, N'Aug', 2018, CAST(14755.3988 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (135, 14, 42, N'Dec', 2018, CAST(13501.1899 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (136, 14, 42, N'Feb', 2018, CAST(12878.0790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (137, 14, 42, N'Jan', 2018, CAST(12878.0790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (138, 14, 42, N'Jul', 2018, CAST(14755.3988 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (139, 14, 42, N'Jun', 2018, CAST(12878.0790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (140, 14, 42, N'Mar', 2018, CAST(12878.0790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (141, 14, 42, N'May', 2018, CAST(12878.0790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (142, 14, 42, N'Nov', 2018, CAST(13501.1899 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (143, 14, 42, N'Oct', 2018, CAST(13501.1899 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (144, 14, 42, N'Sep', 2018, CAST(13501.1899 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (145, 15, 39, N'Apr', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (146, 15, 39, N'Aug', 2018, CAST(29709.4318 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (147, 15, 39, N'Dec', 2018, CAST(27184.1301 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (148, 15, 39, N'Feb', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (149, 15, 39, N'Jan', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (150, 15, 39, N'Jul', 2018, CAST(29709.4318 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (151, 15, 39, N'Jun', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (152, 15, 39, N'Mar', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (153, 15, 39, N'May', 2018, CAST(26156.8848 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (154, 15, 39, N'Nov', 2018, CAST(27184.1301 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (155, 15, 39, N'Oct', 2018, CAST(27184.1301 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (156, 15, 39, N'Sep', 2018, CAST(27184.1301 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (157, 15, 40, N'Apr', 2018, CAST(51985.6397 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (158, 15, 40, N'Aug', 2018, CAST(60628.5662 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (159, 15, 40, N'Dec', 2018, CAST(55475.1381 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (160, 15, 40, N'Feb', 2018, CAST(51985.6397 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (161, 15, 40, N'Jan', 2018, CAST(51985.6397 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (162, 15, 40, N'Jul', 2018, CAST(60628.5662 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (163, 15, 40, N'Jun', 2018, CAST(51985.6397 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (164, 15, 40, N'Mar', 2018, CAST(51985.6397 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (165, 15, 40, N'May', 2018, CAST(51985.6397 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (166, 15, 40, N'Nov', 2018, CAST(55475.1381 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (167, 15, 40, N'Oct', 2018, CAST(55475.1381 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (168, 15, 40, N'Sep', 2018, CAST(55475.1381 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (169, 15, 41, N'Apr', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (170, 15, 41, N'Aug', 2018, CAST(62540.5899 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (171, 15, 41, N'Dec', 2018, CAST(57224.6398 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (172, 15, 41, N'Feb', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (173, 15, 41, N'Jan', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (174, 15, 41, N'Jul', 2018, CAST(62540.5899 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (175, 15, 41, N'Jun', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (176, 15, 41, N'Mar', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (177, 15, 41, N'May', 2018, CAST(54634.6934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (178, 15, 41, N'Nov', 2018, CAST(57224.6398 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (179, 15, 41, N'Oct', 2018, CAST(57224.6398 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (180, 15, 41, N'Sep', 2018, CAST(57224.6398 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (181, 15, 42, N'Apr', 2018, CAST(11541.9571 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (182, 15, 42, N'Aug', 2018, CAST(13355.6228 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (183, 15, 42, N'Dec', 2018, CAST(12220.3948 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (184, 15, 42, N'Feb', 2018, CAST(11541.9571 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (185, 15, 42, N'Jan', 2018, CAST(11541.9571 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (186, 15, 42, N'Jul', 2018, CAST(13355.6228 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (187, 15, 42, N'Jun', 2018, CAST(11541.9571 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (188, 15, 42, N'Mar', 2018, CAST(11541.9571 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (189, 15, 42, N'May', 2018, CAST(11541.9571 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (190, 15, 42, N'Nov', 2018, CAST(12220.3948 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (191, 15, 42, N'Oct', 2018, CAST(12220.3948 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (192, 15, 42, N'Sep', 2018, CAST(12220.3948 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (193, 16, 39, N'Apr', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (194, 16, 39, N'Aug', 2018, CAST(39830.1967 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (195, 16, 39, N'Dec', 2018, CAST(36444.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (196, 16, 39, N'Feb', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (197, 16, 39, N'Jan', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (198, 16, 39, N'Jul', 2018, CAST(39830.1967 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (199, 16, 39, N'Jun', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (200, 16, 39, N'Mar', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (201, 16, 39, N'May', 2018, CAST(41913.5376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (202, 16, 39, N'Nov', 2018, CAST(36444.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (203, 16, 39, N'Oct', 2018, CAST(36444.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (204, 16, 39, N'Sep', 2018, CAST(36444.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (205, 16, 40, N'Apr', 2018, CAST(84109.1581 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (206, 16, 40, N'Aug', 2018, CAST(61294.0823 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (207, 16, 40, N'Dec', 2018, CAST(56084.0853 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (208, 16, 40, N'Feb', 2018, CAST(84109.1581 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (209, 16, 40, N'Jan', 2018, CAST(84109.1581 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (210, 16, 40, N'Jul', 2018, CAST(61294.0823 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (211, 16, 40, N'Jun', 2018, CAST(84109.1581 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (212, 16, 40, N'Mar', 2018, CAST(84109.1581 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (213, 16, 40, N'May', 2018, CAST(84109.1581 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (214, 16, 40, N'Nov', 2018, CAST(56084.0853 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (215, 16, 40, N'Oct', 2018, CAST(56084.0853 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (216, 16, 40, N'Sep', 2018, CAST(56084.0853 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (217, 16, 41, N'Apr', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (218, 16, 41, N'Aug', 2018, CAST(66884.3408 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (219, 16, 41, N'Dec', 2018, CAST(61199.1719 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (220, 16, 41, N'Feb', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (221, 16, 41, N'Jan', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (222, 16, 41, N'Jul', 2018, CAST(66884.3408 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (223, 16, 41, N'Jun', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (224, 16, 41, N'Mar', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (225, 16, 41, N'May', 2018, CAST(74986.4140 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (226, 16, 41, N'Nov', 2018, CAST(61199.1719 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (227, 16, 41, N'Oct', 2018, CAST(61199.1719 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (228, 16, 41, N'Sep', 2018, CAST(61199.1719 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (229, 16, 42, N'Apr', 2018, CAST(14982.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (230, 16, 42, N'Aug', 2018, CAST(9993.5581 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (231, 16, 42, N'Dec', 2018, CAST(9144.1057 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (232, 16, 42, N'Feb', 2018, CAST(14982.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (233, 16, 42, N'Jan', 2018, CAST(14982.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (234, 16, 42, N'Jul', 2018, CAST(9993.5581 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (235, 16, 42, N'Jun', 2018, CAST(14982.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (236, 16, 42, N'Mar', 2018, CAST(14982.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (237, 16, 42, N'May', 2018, CAST(14982.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (238, 16, 42, N'Nov', 2018, CAST(9144.1057 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (239, 16, 42, N'Oct', 2018, CAST(9144.1057 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (240, 16, 42, N'Sep', 2018, CAST(9144.1057 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (241, 17, 39, N'Apr', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (242, 17, 39, N'Aug', 2018, CAST(42189.5149 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (243, 17, 39, N'Dec', 2018, CAST(38603.4061 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (244, 17, 39, N'Feb', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (245, 17, 39, N'Jan', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (246, 17, 39, N'Jul', 2018, CAST(42189.5149 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (247, 17, 39, N'Jun', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (248, 17, 39, N'Mar', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (249, 17, 39, N'May', 2018, CAST(29956.4685 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (250, 17, 39, N'Nov', 2018, CAST(38603.4061 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (251, 17, 39, N'Oct', 2018, CAST(38603.4061 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (252, 17, 39, N'Sep', 2018, CAST(38603.4061 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (253, 17, 40, N'Apr', 2018, CAST(99023.8163 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (254, 17, 40, N'Aug', 2018, CAST(102474.8792 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (255, 17, 40, N'Dec', 2018, CAST(93764.5144 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (256, 17, 40, N'Feb', 2018, CAST(99023.8163 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (257, 17, 40, N'Jan', 2018, CAST(99023.8163 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (258, 17, 40, N'Jul', 2018, CAST(102474.8792 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (259, 17, 40, N'Jun', 2018, CAST(99023.8163 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (260, 17, 40, N'Mar', 2018, CAST(99023.8163 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (261, 17, 40, N'May', 2018, CAST(99023.8163 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (262, 17, 40, N'Nov', 2018, CAST(93764.5144 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (263, 17, 40, N'Oct', 2018, CAST(93764.5144 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (264, 17, 40, N'Sep', 2018, CAST(93764.5144 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (265, 17, 41, N'Apr', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (266, 17, 41, N'Aug', 2018, CAST(104159.8350 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (267, 17, 41, N'Dec', 2018, CAST(95306.2490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (268, 17, 41, N'Feb', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (269, 17, 41, N'Jan', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (270, 17, 41, N'Jul', 2018, CAST(104159.8350 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (271, 17, 41, N'Jun', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (272, 17, 41, N'Mar', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (273, 17, 41, N'May', 2018, CAST(102070.6483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (274, 17, 41, N'Nov', 2018, CAST(95306.2490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (275, 17, 41, N'Oct', 2018, CAST(95306.2490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (276, 17, 41, N'Sep', 2018, CAST(95306.2490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (277, 17, 42, N'Apr', 2018, CAST(17701.5688 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (278, 17, 42, N'Aug', 2018, CAST(18098.3162 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (279, 17, 42, N'Dec', 2018, CAST(16559.9593 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (280, 17, 42, N'Feb', 2018, CAST(17701.5688 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (281, 17, 42, N'Jan', 2018, CAST(17701.5688 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (282, 17, 42, N'Jul', 2018, CAST(18098.3162 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (283, 17, 42, N'Jun', 2018, CAST(17701.5688 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (284, 17, 42, N'Mar', 2018, CAST(17701.5688 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (285, 17, 42, N'May', 2018, CAST(17701.5688 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (286, 17, 42, N'Nov', 2018, CAST(16559.9593 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (287, 17, 42, N'Oct', 2018, CAST(16559.9593 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (288, 17, 42, N'Sep', 2018, CAST(16559.9593 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (289, 18, 39, N'Apr', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (290, 18, 39, N'Aug', 2018, CAST(29479.0748 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (291, 18, 39, N'Dec', 2018, CAST(26973.3535 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (292, 18, 39, N'Feb', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (293, 18, 39, N'Jan', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (294, 18, 39, N'Jul', 2018, CAST(29479.0748 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (295, 18, 39, N'Jun', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (296, 18, 39, N'Mar', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (297, 18, 39, N'May', 2018, CAST(23845.2805 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (298, 18, 39, N'Nov', 2018, CAST(26973.3535 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (299, 18, 39, N'Oct', 2018, CAST(26973.3535 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (300, 18, 39, N'Sep', 2018, CAST(26973.3535 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (301, 18, 40, N'Apr', 2018, CAST(123456.9456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (302, 18, 40, N'Aug', 2018, CAST(107578.7572 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (303, 18, 40, N'Dec', 2018, CAST(98434.5629 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (304, 18, 40, N'Feb', 2018, CAST(123456.9456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (305, 18, 40, N'Jan', 2018, CAST(123456.9456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (306, 18, 40, N'Jul', 2018, CAST(107578.7572 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (307, 18, 40, N'Jun', 2018, CAST(123456.9456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (308, 18, 40, N'Mar', 2018, CAST(123456.9456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (309, 18, 40, N'May', 2018, CAST(123456.9456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (310, 18, 40, N'Nov', 2018, CAST(98434.5629 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (311, 18, 40, N'Oct', 2018, CAST(98434.5629 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (312, 18, 40, N'Sep', 2018, CAST(98434.5629 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (313, 18, 41, N'Apr', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (314, 18, 41, N'Aug', 2018, CAST(44391.6794 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (315, 18, 41, N'Dec', 2018, CAST(40618.3866 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (316, 18, 41, N'Feb', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (317, 18, 41, N'Jan', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (318, 18, 41, N'Jul', 2018, CAST(44391.6794 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (319, 18, 41, N'Jun', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (320, 18, 41, N'Mar', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (321, 18, 41, N'May', 2018, CAST(83160.2265 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (322, 18, 41, N'Nov', 2018, CAST(40618.3866 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (323, 18, 41, N'Oct', 2018, CAST(40618.3866 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (324, 18, 41, N'Sep', 2018, CAST(40618.3866 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (325, 18, 42, N'Apr', 2018, CAST(17306.4723 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (326, 18, 42, N'Aug', 2018, CAST(17122.5199 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (327, 18, 42, N'Dec', 2018, CAST(15667.1057 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (328, 18, 42, N'Feb', 2018, CAST(17306.4723 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (329, 18, 42, N'Jan', 2018, CAST(17306.4723 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (330, 18, 42, N'Jul', 2018, CAST(17122.5199 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (331, 18, 42, N'Jun', 2018, CAST(17306.4723 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (332, 18, 42, N'Mar', 2018, CAST(17306.4723 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (333, 18, 42, N'May', 2018, CAST(17306.4723 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (334, 18, 42, N'Nov', 2018, CAST(15667.1057 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (335, 18, 42, N'Oct', 2018, CAST(15667.1057 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (336, 18, 42, N'Sep', 2018, CAST(15667.1057 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (337, 20, 39, N'Apr', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (338, 20, 39, N'Aug', 2018, CAST(29559.9881 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (339, 20, 39, N'Dec', 2018, CAST(27047.3891 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (340, 20, 39, N'Feb', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (341, 20, 39, N'Jan', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (342, 20, 39, N'Jul', 2018, CAST(29559.9881 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (343, 20, 39, N'Jun', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (344, 20, 39, N'Mar', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (345, 20, 39, N'May', 2018, CAST(28930.6665 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (346, 20, 39, N'Nov', 2018, CAST(27047.3891 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (347, 20, 39, N'Oct', 2018, CAST(27047.3891 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (348, 20, 39, N'Sep', 2018, CAST(27047.3891 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (349, 20, 40, N'Apr', 2018, CAST(104448.8085 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (350, 20, 40, N'Aug', 2018, CAST(92996.7388 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (351, 20, 40, N'Dec', 2018, CAST(85092.0160 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (352, 20, 40, N'Feb', 2018, CAST(104448.8085 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (353, 20, 40, N'Jan', 2018, CAST(104448.8085 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (354, 20, 40, N'Jul', 2018, CAST(92996.7388 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (355, 20, 40, N'Jun', 2018, CAST(104448.8085 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (356, 20, 40, N'Mar', 2018, CAST(104448.8085 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (357, 20, 40, N'May', 2018, CAST(104448.8085 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (358, 20, 40, N'Nov', 2018, CAST(85092.0160 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (359, 20, 40, N'Oct', 2018, CAST(85092.0160 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (360, 20, 40, N'Sep', 2018, CAST(85092.0160 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (361, 20, 41, N'Apr', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (362, 20, 41, N'Aug', 2018, CAST(90110.6628 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (363, 20, 41, N'Dec', 2018, CAST(82451.2565 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (364, 20, 41, N'Feb', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (365, 20, 41, N'Jan', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (366, 20, 41, N'Jul', 2018, CAST(90110.6628 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (367, 20, 41, N'Jun', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (368, 20, 41, N'Mar', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (369, 20, 41, N'May', 2018, CAST(106483.2125 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (370, 20, 41, N'Nov', 2018, CAST(82451.2565 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (371, 20, 41, N'Oct', 2018, CAST(82451.2565 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (372, 20, 41, N'Sep', 2018, CAST(82451.2565 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (373, 20, 42, N'Apr', 2018, CAST(12544.3490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (374, 20, 42, N'Aug', 2018, CAST(11057.5102 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (375, 20, 42, N'Dec', 2018, CAST(10117.6218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (376, 20, 42, N'Feb', 2018, CAST(12544.3490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (377, 20, 42, N'Jan', 2018, CAST(12544.3490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (378, 20, 42, N'Jul', 2018, CAST(11057.5102 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (379, 20, 42, N'Jun', 2018, CAST(12544.3490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (380, 20, 42, N'Mar', 2018, CAST(12544.3490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (381, 20, 42, N'May', 2018, CAST(12544.3490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (382, 20, 42, N'Nov', 2018, CAST(10117.6218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (383, 20, 42, N'Oct', 2018, CAST(10117.6218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (384, 20, 42, N'Sep', 2018, CAST(10117.6218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (385, 21, 39, N'Apr', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (386, 21, 39, N'Aug', 2018, CAST(27123.8849 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (387, 21, 39, N'Dec', 2018, CAST(24818.3547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (388, 21, 39, N'Feb', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (389, 21, 39, N'Jan', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (390, 21, 39, N'Jul', 2018, CAST(27123.8849 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (391, 21, 39, N'Jun', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (392, 21, 39, N'Mar', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (393, 21, 39, N'May', 2018, CAST(25411.0456 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (394, 21, 39, N'Nov', 2018, CAST(24818.3547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (395, 21, 39, N'Oct', 2018, CAST(24818.3547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (396, 21, 39, N'Sep', 2018, CAST(24818.3547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (397, 21, 40, N'Apr', 2018, CAST(122630.7725 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (398, 21, 40, N'Aug', 2018, CAST(137691.3232 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (399, 21, 40, N'Dec', 2018, CAST(125987.5608 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (400, 21, 40, N'Feb', 2018, CAST(122630.7725 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (401, 21, 40, N'Jan', 2018, CAST(122630.7725 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (402, 21, 40, N'Jul', 2018, CAST(137691.3232 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (403, 21, 40, N'Jun', 2018, CAST(122630.7725 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (404, 21, 40, N'Mar', 2018, CAST(122630.7725 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (405, 21, 40, N'May', 2018, CAST(122630.7725 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (406, 21, 40, N'Nov', 2018, CAST(125987.5608 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (407, 21, 40, N'Oct', 2018, CAST(125987.5608 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (408, 21, 40, N'Sep', 2018, CAST(125987.5608 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (409, 21, 41, N'Apr', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (410, 21, 41, N'Aug', 2018, CAST(55547.1531 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (411, 21, 41, N'Dec', 2018, CAST(50825.6451 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (412, 21, 41, N'Feb', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (413, 21, 41, N'Jan', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (414, 21, 41, N'Jul', 2018, CAST(55547.1531 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (415, 21, 41, N'Jun', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (416, 21, 41, N'Mar', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (417, 21, 41, N'May', 2018, CAST(48816.7649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (418, 21, 41, N'Nov', 2018, CAST(50825.6451 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (419, 21, 41, N'Oct', 2018, CAST(50825.6451 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (420, 21, 41, N'Sep', 2018, CAST(50825.6451 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (421, 21, 42, N'Apr', 2018, CAST(19362.5981 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (422, 21, 42, N'Aug', 2018, CAST(21371.0186 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (423, 21, 42, N'Dec', 2018, CAST(19554.4820 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (424, 21, 42, N'Feb', 2018, CAST(19362.5981 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (425, 21, 42, N'Jan', 2018, CAST(19362.5981 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (426, 21, 42, N'Jul', 2018, CAST(21371.0186 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (427, 21, 42, N'Jun', 2018, CAST(19362.5981 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (428, 21, 42, N'Mar', 2018, CAST(19362.5981 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (429, 21, 42, N'May', 2018, CAST(19362.5981 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (430, 21, 42, N'Nov', 2018, CAST(19554.4820 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (431, 21, 42, N'Oct', 2018, CAST(19554.4820 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (432, 21, 42, N'Sep', 2018, CAST(19554.4820 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (433, 25, 39, N'Apr', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (434, 25, 39, N'Aug', 2018, CAST(28989.4034 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (435, 25, 39, N'Dec', 2018, CAST(26525.3041 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (436, 25, 39, N'Feb', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (437, 25, 39, N'Jan', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (438, 25, 39, N'Jul', 2018, CAST(28989.4034 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (439, 25, 39, N'Jun', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (440, 25, 39, N'Mar', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (441, 25, 39, N'May', 2018, CAST(19104.3675 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (442, 25, 39, N'Nov', 2018, CAST(26525.3041 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (443, 25, 39, N'Oct', 2018, CAST(26525.3041 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (444, 25, 39, N'Sep', 2018, CAST(26525.3041 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (445, 25, 40, N'Apr', 2018, CAST(64900.4770 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (446, 25, 40, N'Aug', 2018, CAST(78408.5380 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (447, 25, 40, N'Dec', 2018, CAST(71743.8123 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (448, 25, 40, N'Feb', 2018, CAST(64900.4770 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (449, 25, 40, N'Jan', 2018, CAST(64900.4770 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (450, 25, 40, N'Jul', 2018, CAST(78408.5380 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (451, 25, 40, N'Jun', 2018, CAST(64900.4770 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (452, 25, 40, N'Mar', 2018, CAST(64900.4770 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (453, 25, 40, N'May', 2018, CAST(64900.4770 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (454, 25, 40, N'Nov', 2018, CAST(71743.8123 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (455, 25, 40, N'Oct', 2018, CAST(71743.8123 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (456, 25, 40, N'Sep', 2018, CAST(71743.8123 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (457, 25, 41, N'Apr', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (458, 25, 41, N'Aug', 2018, CAST(41939.2588 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (459, 25, 41, N'Dec', 2018, CAST(38374.4218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (460, 25, 41, N'Feb', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (461, 25, 41, N'Jan', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (462, 25, 41, N'Jul', 2018, CAST(41939.2588 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (463, 25, 41, N'Jun', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (464, 25, 41, N'Mar', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (465, 25, 41, N'May', 2018, CAST(34714.5828 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (466, 25, 41, N'Nov', 2018, CAST(38374.4218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (467, 25, 41, N'Oct', 2018, CAST(38374.4218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (468, 25, 41, N'Sep', 2018, CAST(38374.4218 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (469, 25, 42, N'Apr', 2018, CAST(10339.7209 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (470, 25, 42, N'Aug', 2018, CAST(12580.9206 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (471, 25, 42, N'Dec', 2018, CAST(11511.5424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (472, 25, 42, N'Feb', 2018, CAST(10339.7209 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (473, 25, 42, N'Jan', 2018, CAST(10339.7209 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (474, 25, 42, N'Jul', 2018, CAST(12580.9206 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (475, 25, 42, N'Jun', 2018, CAST(10339.7209 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (476, 25, 42, N'Mar', 2018, CAST(10339.7209 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (477, 25, 42, N'May', 2018, CAST(10339.7209 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (478, 25, 42, N'Nov', 2018, CAST(11511.5424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (479, 25, 42, N'Oct', 2018, CAST(11511.5424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (480, 25, 42, N'Sep', 2018, CAST(11511.5424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (481, 27, 39, N'Apr', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (482, 27, 39, N'Aug', 2018, CAST(40911.2026 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (483, 27, 39, N'Dec', 2018, CAST(37433.7504 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (484, 27, 39, N'Feb', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (485, 27, 39, N'Jan', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (486, 27, 39, N'Jul', 2018, CAST(40911.2026 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (487, 27, 39, N'Jun', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (488, 27, 39, N'Mar', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (489, 27, 39, N'May', 2018, CAST(24487.9391 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (490, 27, 39, N'Nov', 2018, CAST(37433.7504 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (491, 27, 39, N'Oct', 2018, CAST(37433.7504 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (492, 27, 39, N'Sep', 2018, CAST(37433.7504 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (493, 27, 40, N'Apr', 2018, CAST(79437.3060 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (494, 27, 40, N'Aug', 2018, CAST(100352.3497 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (495, 27, 40, N'Dec', 2018, CAST(91822.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (496, 27, 40, N'Feb', 2018, CAST(79437.3060 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (497, 27, 40, N'Jan', 2018, CAST(79437.3060 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (498, 27, 40, N'Jul', 2018, CAST(100352.3497 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (499, 27, 40, N'Jun', 2018, CAST(79437.3060 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (500, 27, 40, N'Mar', 2018, CAST(79437.3060 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (501, 27, 40, N'May', 2018, CAST(79437.3060 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (502, 27, 40, N'Nov', 2018, CAST(91822.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (503, 27, 40, N'Oct', 2018, CAST(91822.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (504, 27, 40, N'Sep', 2018, CAST(91822.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (505, 27, 41, N'Apr', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (506, 27, 41, N'Aug', 2018, CAST(59227.5492 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (507, 27, 41, N'Dec', 2018, CAST(54193.2076 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (508, 27, 41, N'Feb', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (509, 27, 41, N'Jan', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (510, 27, 41, N'Jul', 2018, CAST(59227.5492 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (511, 27, 41, N'Jun', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (512, 27, 41, N'Mar', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (513, 27, 41, N'May', 2018, CAST(47712.9008 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (514, 27, 41, N'Nov', 2018, CAST(54193.2076 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (515, 27, 41, N'Oct', 2018, CAST(54193.2076 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (516, 27, 41, N'Sep', 2018, CAST(54193.2076 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (517, 27, 42, N'Apr', 2018, CAST(15414.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (518, 27, 42, N'Aug', 2018, CAST(19253.6990 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (519, 27, 42, N'Dec', 2018, CAST(17617.1346 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (520, 27, 42, N'Feb', 2018, CAST(15414.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (521, 27, 42, N'Jan', 2018, CAST(15414.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (522, 27, 42, N'Jul', 2018, CAST(19253.6990 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (523, 27, 42, N'Jun', 2018, CAST(15414.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (524, 27, 42, N'Mar', 2018, CAST(15414.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (525, 27, 42, N'May', 2018, CAST(15414.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (526, 27, 42, N'Nov', 2018, CAST(17617.1346 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (527, 27, 42, N'Oct', 2018, CAST(17617.1346 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (528, 27, 42, N'Sep', 2018, CAST(17617.1346 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (529, 33, 39, N'Apr', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (530, 33, 39, N'Aug', 2018, CAST(72619.2760 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (531, 33, 39, N'Dec', 2018, CAST(66446.6376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (532, 33, 39, N'Feb', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (533, 33, 39, N'Jan', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (534, 33, 39, N'Jul', 2018, CAST(72619.2760 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (535, 33, 39, N'Jun', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (536, 33, 39, N'Mar', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (537, 33, 39, N'May', 2018, CAST(58271.9290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (538, 33, 39, N'Nov', 2018, CAST(66446.6376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (539, 33, 39, N'Oct', 2018, CAST(66446.6376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (540, 33, 39, N'Sep', 2018, CAST(66446.6376 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (541, 33, 40, N'Apr', 2018, CAST(80289.8686 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (542, 33, 40, N'Aug', 2018, CAST(89191.3575 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (543, 33, 40, N'Dec', 2018, CAST(81610.0921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (544, 33, 40, N'Feb', 2018, CAST(80289.8686 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (545, 33, 40, N'Jan', 2018, CAST(80289.8686 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (546, 33, 40, N'Jul', 2018, CAST(89191.3575 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (547, 33, 40, N'Jun', 2018, CAST(80289.8686 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (548, 33, 40, N'Mar', 2018, CAST(80289.8686 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (549, 33, 40, N'May', 2018, CAST(80289.8686 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (550, 33, 40, N'Nov', 2018, CAST(81610.0921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (551, 33, 40, N'Oct', 2018, CAST(81610.0921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (552, 33, 40, N'Sep', 2018, CAST(81610.0921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (553, 33, 41, N'Apr', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (554, 33, 41, N'Aug', 2018, CAST(144866.2306 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (555, 33, 41, N'Dec', 2018, CAST(132552.6010 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (556, 33, 41, N'Feb', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (557, 33, 41, N'Jan', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (558, 33, 41, N'Jul', 2018, CAST(144866.2306 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (559, 33, 41, N'Jun', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (560, 33, 41, N'Mar', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (561, 33, 41, N'May', 2018, CAST(133914.7611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (562, 33, 41, N'Nov', 2018, CAST(132552.6010 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (563, 33, 41, N'Oct', 2018, CAST(132552.6010 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (564, 33, 41, N'Sep', 2018, CAST(132552.6010 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (565, 33, 42, N'Apr', 2018, CAST(11820.6429 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (566, 33, 42, N'Aug', 2018, CAST(12852.9940 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (567, 33, 42, N'Dec', 2018, CAST(11760.4895 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (568, 33, 42, N'Feb', 2018, CAST(11820.6429 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (569, 33, 42, N'Jan', 2018, CAST(11820.6429 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (570, 33, 42, N'Jul', 2018, CAST(12852.9940 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (571, 33, 42, N'Jun', 2018, CAST(11820.6429 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (572, 33, 42, N'Mar', 2018, CAST(11820.6429 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (573, 33, 42, N'May', 2018, CAST(11820.6429 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (574, 33, 42, N'Nov', 2018, CAST(11760.4895 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (575, 33, 42, N'Oct', 2018, CAST(11760.4895 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (576, 33, 42, N'Sep', 2018, CAST(11760.4895 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (577, 35, 39, N'Apr', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (578, 35, 39, N'Aug', 2018, CAST(58237.1994 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (579, 35, 39, N'Dec', 2018, CAST(53287.0374 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (580, 35, 39, N'Feb', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (581, 35, 39, N'Jan', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (582, 35, 39, N'Jul', 2018, CAST(58237.1994 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (583, 35, 39, N'Jun', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (584, 35, 39, N'Mar', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (585, 35, 39, N'May', 2018, CAST(37304.7840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (586, 35, 39, N'Nov', 2018, CAST(53287.0374 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (587, 35, 39, N'Oct', 2018, CAST(53287.0374 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (588, 35, 39, N'Sep', 2018, CAST(53287.0374 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (589, 35, 40, N'Apr', 2018, CAST(78242.2790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (590, 35, 40, N'Aug', 2018, CAST(95287.3925 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (591, 35, 40, N'Dec', 2018, CAST(87187.9642 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (592, 35, 40, N'Feb', 2018, CAST(78242.2790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (593, 35, 40, N'Jan', 2018, CAST(78242.2790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (594, 35, 40, N'Jul', 2018, CAST(95287.3925 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (595, 35, 40, N'Jun', 2018, CAST(78242.2790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (596, 35, 40, N'Mar', 2018, CAST(78242.2790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (597, 35, 40, N'May', 2018, CAST(78242.2790 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (598, 35, 40, N'Nov', 2018, CAST(87187.9642 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (599, 35, 40, N'Oct', 2018, CAST(87187.9642 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (600, 35, 40, N'Sep', 2018, CAST(87187.9642 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (601, 35, 41, N'Apr', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (602, 35, 41, N'Aug', 2018, CAST(91135.5789 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (603, 35, 41, N'Dec', 2018, CAST(83389.0547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (604, 35, 41, N'Feb', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (605, 35, 41, N'Jan', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (606, 35, 41, N'Jul', 2018, CAST(91135.5789 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (607, 35, 41, N'Jun', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (608, 35, 41, N'Mar', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (609, 35, 41, N'May', 2018, CAST(77224.4878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (610, 35, 41, N'Nov', 2018, CAST(83389.0547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (611, 35, 41, N'Oct', 2018, CAST(83389.0547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (612, 35, 41, N'Sep', 2018, CAST(83389.0547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (613, 35, 42, N'Apr', 2018, CAST(15182.8917 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (614, 35, 42, N'Aug', 2018, CAST(17914.7723 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (615, 35, 42, N'Dec', 2018, CAST(16392.0166 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (616, 35, 42, N'Feb', 2018, CAST(15182.8917 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (617, 35, 42, N'Jan', 2018, CAST(15182.8917 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (618, 35, 42, N'Jul', 2018, CAST(17914.7723 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (619, 35, 42, N'Jun', 2018, CAST(15182.8917 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (620, 35, 42, N'Mar', 2018, CAST(15182.8917 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (621, 35, 42, N'May', 2018, CAST(15182.8917 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (622, 35, 42, N'Nov', 2018, CAST(16392.0166 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (623, 35, 42, N'Oct', 2018, CAST(16392.0166 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (624, 35, 42, N'Sep', 2018, CAST(16392.0166 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (625, 37, 39, N'Apr', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (626, 37, 39, N'Aug', 2018, CAST(27179.7085 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (627, 37, 39, N'Dec', 2018, CAST(24869.4333 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (628, 37, 39, N'Feb', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (629, 37, 39, N'Jan', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (630, 37, 39, N'Jul', 2018, CAST(27179.7085 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (631, 37, 39, N'Jun', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (632, 37, 39, N'Mar', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (633, 37, 39, N'May', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (634, 37, 39, N'Nov', 2018, CAST(24869.4333 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (635, 37, 39, N'Oct', 2018, CAST(24869.4333 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (636, 37, 39, N'Sep', 2018, CAST(24869.4333 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (637, 37, 40, N'Apr', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (638, 37, 40, N'Aug', 2018, CAST(82106.7398 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (639, 37, 40, N'Dec', 2018, CAST(75127.6669 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (640, 37, 40, N'Feb', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (641, 37, 40, N'Jan', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (642, 37, 40, N'Jul', 2018, CAST(82106.7398 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (643, 37, 40, N'Jun', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (644, 37, 40, N'Mar', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (645, 37, 40, N'May', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (646, 37, 40, N'Nov', 2018, CAST(75127.6669 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (647, 37, 40, N'Oct', 2018, CAST(75127.6669 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (648, 37, 40, N'Sep', 2018, CAST(75127.6669 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (649, 37, 41, N'Apr', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (650, 37, 41, N'Aug', 2018, CAST(83360.6626 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (651, 37, 41, N'Dec', 2018, CAST(76275.0062 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (652, 37, 41, N'Feb', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (653, 37, 41, N'Jan', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (654, 37, 41, N'Jul', 2018, CAST(83360.6626 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (655, 37, 41, N'Jun', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (656, 37, 41, N'Mar', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (657, 37, 41, N'May', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (658, 37, 41, N'Nov', 2018, CAST(76275.0062 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (659, 37, 41, N'Oct', 2018, CAST(76275.0062 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (660, 37, 41, N'Sep', 2018, CAST(76275.0062 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (661, 37, 42, N'Apr', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (662, 37, 42, N'Aug', 2018, CAST(10971.8745 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (663, 37, 42, N'Dec', 2018, CAST(10039.2652 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (664, 37, 42, N'Feb', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (665, 37, 42, N'Jan', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (666, 37, 42, N'Jul', 2018, CAST(10971.8745 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (667, 37, 42, N'Jun', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (668, 37, 42, N'Mar', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (669, 37, 42, N'May', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (670, 37, 42, N'Nov', 2018, CAST(10039.2652 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (671, 37, 42, N'Oct', 2018, CAST(10039.2652 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (672, 37, 42, N'Sep', 2018, CAST(10039.2652 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (673, 13, 39, N'Jan', 2019, CAST(41805.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (674, 13, 39, N'Feb', 2019, CAST(43585.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (675, 13, 39, N'Mar', 2019, CAST(52744.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (676, 13, 39, N'Apr', 2019, CAST(46541.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (677, 13, 39, N'May', 2019, CAST(50890.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (678, 13, 39, N'Jun', 2019, CAST(53468.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (679, 13, 39, N'Jul', 2019, CAST(50855.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (680, 13, 39, N'Aug', 2019, CAST(59902.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (681, 13, 39, N'Sep', 2019, CAST(47714.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (682, 13, 39, N'Oct', 2019, CAST(55987.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (683, 13, 39, N'Nov', 2019, CAST(49125.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (684, 13, 39, N'Dec', 2019, CAST(47593.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (685, 13, 40, N'Jan', 2019, CAST(107806.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (686, 13, 40, N'Feb', 2019, CAST(112396.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (687, 13, 40, N'Mar', 2019, CAST(136014.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (688, 13, 40, N'Apr', 2019, CAST(120018.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (689, 13, 40, N'May', 2019, CAST(131233.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (690, 13, 40, N'Jun', 2019, CAST(137881.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (691, 13, 40, N'Jul', 2019, CAST(131143.7300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (692, 13, 40, N'Aug', 2019, CAST(154474.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (693, 13, 40, N'Sep', 2019, CAST(123042.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (694, 13, 40, N'Oct', 2019, CAST(144377.3900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (695, 13, 40, N'Nov', 2019, CAST(126683.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (696, 13, 40, N'Dec', 2019, CAST(122731.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (697, 13, 41, N'Jan', 2019, CAST(70175.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (698, 13, 41, N'Feb', 2019, CAST(73163.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (699, 13, 41, N'Mar', 2019, CAST(88536.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (700, 13, 41, N'Apr', 2019, CAST(78124.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (701, 13, 41, N'May', 2019, CAST(85424.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (702, 13, 41, N'Jun', 2019, CAST(89751.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (703, 13, 41, N'Jul', 2019, CAST(85366.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (704, 13, 41, N'Aug', 2019, CAST(100552.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (705, 13, 41, N'Sep', 2019, CAST(80093.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (706, 13, 41, N'Oct', 2019, CAST(93980.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (707, 13, 41, N'Nov', 2019, CAST(82462.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (708, 13, 41, N'Dec', 2019, CAST(79890.0400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (709, 13, 42, N'Jan', 2019, CAST(14819.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (710, 13, 42, N'Feb', 2019, CAST(15450.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (711, 13, 42, N'Mar', 2019, CAST(18697.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (712, 13, 42, N'Apr', 2019, CAST(16498.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (713, 13, 42, N'May', 2019, CAST(18040.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (714, 13, 42, N'Jun', 2019, CAST(18954.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (715, 13, 42, N'Jul', 2019, CAST(18027.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (716, 13, 42, N'Aug', 2019, CAST(21235.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (717, 13, 42, N'Sep', 2019, CAST(16914.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (718, 13, 42, N'Oct', 2019, CAST(19847.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (719, 13, 42, N'Nov', 2019, CAST(17414.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (720, 13, 42, N'Dec', 2019, CAST(16871.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (721, 14, 39, N'Jan', 2019, CAST(44434.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (722, 14, 39, N'Feb', 2019, CAST(46326.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (723, 14, 39, N'Mar', 2019, CAST(56061.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (724, 14, 39, N'Apr', 2019, CAST(49467.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (725, 14, 39, N'May', 2019, CAST(54090.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (726, 14, 39, N'Jun', 2019, CAST(56830.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (727, 14, 39, N'Jul', 2019, CAST(54053.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (728, 14, 39, N'Aug', 2019, CAST(63669.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (729, 14, 39, N'Sep', 2019, CAST(50714.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (730, 14, 39, N'Oct', 2019, CAST(59508.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (731, 14, 39, N'Nov', 2019, CAST(52215.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (732, 14, 39, N'Dec', 2019, CAST(50586.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (733, 14, 40, N'Jan', 2019, CAST(82673.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (734, 14, 40, N'Feb', 2019, CAST(86193.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (735, 14, 40, N'Mar', 2019, CAST(104305.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (736, 14, 40, N'Apr', 2019, CAST(92037.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (737, 14, 40, N'May', 2019, CAST(100638.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (738, 14, 40, N'Jun', 2019, CAST(105736.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (739, 14, 40, N'Jul', 2019, CAST(100569.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (740, 14, 40, N'Aug', 2019, CAST(118461.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (741, 14, 40, N'Sep', 2019, CAST(94357.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (742, 14, 40, N'Oct', 2019, CAST(110718.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (743, 14, 40, N'Nov', 2019, CAST(97149.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (744, 14, 40, N'Dec', 2019, CAST(94118.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (745, 14, 41, N'Jan', 2019, CAST(106501.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (746, 14, 41, N'Feb', 2019, CAST(111036.5900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (747, 14, 41, N'Mar', 2019, CAST(134368.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (748, 14, 41, N'Apr', 2019, CAST(118565.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (749, 14, 41, N'May', 2019, CAST(129645.5900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (750, 14, 41, N'Jun', 2019, CAST(136212.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (751, 14, 41, N'Jul', 2019, CAST(129556.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (752, 14, 41, N'Aug', 2019, CAST(152604.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (753, 14, 41, N'Sep', 2019, CAST(121553.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (754, 14, 41, N'Oct', 2019, CAST(142630.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (755, 14, 41, N'Nov', 2019, CAST(125149.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (756, 14, 41, N'Dec', 2019, CAST(121245.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (757, 14, 42, N'Jan', 2019, CAST(10759.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (758, 14, 42, N'Feb', 2019, CAST(11217.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (759, 14, 42, N'Mar', 2019, CAST(13574.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (760, 14, 42, N'Apr', 2019, CAST(11978.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (761, 14, 42, N'May', 2019, CAST(13097.4600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (762, 14, 42, N'Jun', 2019, CAST(13760.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (763, 14, 42, N'Jul', 2019, CAST(13088.4600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (764, 14, 42, N'Aug', 2019, CAST(15416.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (765, 14, 42, N'Sep', 2019, CAST(12279.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (766, 14, 42, N'Oct', 2019, CAST(14409.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (767, 14, 42, N'Nov', 2019, CAST(12643.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (768, 14, 42, N'Dec', 2019, CAST(12248.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (769, 15, 39, N'Jan', 2019, CAST(33617.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (770, 15, 39, N'Feb', 2019, CAST(35049.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (771, 15, 39, N'Mar', 2019, CAST(42413.7300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (772, 15, 39, N'Apr', 2019, CAST(37425.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (773, 15, 39, N'May', 2019, CAST(40922.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (774, 15, 39, N'Jun', 2019, CAST(42995.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (775, 15, 39, N'Jul', 2019, CAST(40894.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (776, 15, 39, N'Aug', 2019, CAST(48170.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (777, 15, 39, N'Sep', 2019, CAST(38368.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (778, 15, 39, N'Oct', 2019, CAST(45021.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (779, 15, 39, N'Nov', 2019, CAST(39503.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (780, 15, 39, N'Dec', 2019, CAST(38271.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (781, 15, 40, N'Jan', 2019, CAST(59267.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (782, 15, 40, N'Feb', 2019, CAST(61791.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (783, 15, 40, N'Mar', 2019, CAST(74775.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (784, 15, 40, N'Apr', 2019, CAST(65981.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (785, 15, 40, N'May', 2019, CAST(72147.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (786, 15, 40, N'Jun', 2019, CAST(75801.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (787, 15, 40, N'Jul', 2019, CAST(72097.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (788, 15, 40, N'Aug', 2019, CAST(84923.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (789, 15, 40, N'Sep', 2019, CAST(67644.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (790, 15, 40, N'Oct', 2019, CAST(79373.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (791, 15, 40, N'Nov', 2019, CAST(69645.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (792, 15, 40, N'Dec', 2019, CAST(67472.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (793, 15, 41, N'Jan', 2019, CAST(51740.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (794, 15, 41, N'Feb', 2019, CAST(53943.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (795, 15, 41, N'Mar', 2019, CAST(65278.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (796, 15, 41, N'Apr', 2019, CAST(57600.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (797, 15, 41, N'May', 2019, CAST(62983.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (798, 15, 41, N'Jun', 2019, CAST(66174.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (799, 15, 41, N'Jul', 2019, CAST(62940.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (800, 15, 41, N'Aug', 2019, CAST(74137.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (801, 15, 41, N'Sep', 2019, CAST(59052.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (802, 15, 41, N'Oct', 2019, CAST(69291.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (803, 15, 41, N'Nov', 2019, CAST(60799.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (804, 15, 41, N'Dec', 2019, CAST(58902.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (805, 15, 42, N'Jan', 2019, CAST(8190.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (806, 15, 42, N'Feb', 2019, CAST(8539.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (807, 15, 42, N'Mar', 2019, CAST(10333.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (808, 15, 42, N'Apr', 2019, CAST(9118.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (809, 15, 42, N'May', 2019, CAST(9970.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (810, 15, 42, N'Jun', 2019, CAST(10475.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (811, 15, 42, N'Jul', 2019, CAST(9963.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (812, 15, 42, N'Aug', 2019, CAST(11736.3900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (813, 15, 42, N'Sep', 2019, CAST(9348.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (814, 15, 42, N'Oct', 2019, CAST(10969.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (815, 15, 42, N'Nov', 2019, CAST(9624.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (816, 15, 42, N'Dec', 2019, CAST(9324.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (817, 16, 39, N'Jan', 2019, CAST(40524.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (818, 16, 39, N'Feb', 2019, CAST(42249.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (819, 16, 39, N'Mar', 2019, CAST(51127.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (820, 16, 39, N'Apr', 2019, CAST(45114.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (821, 16, 39, N'May', 2019, CAST(49330.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (822, 16, 39, N'Jun', 2019, CAST(51829.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (823, 16, 39, N'Jul', 2019, CAST(49296.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (824, 16, 39, N'Aug', 2019, CAST(58066.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (825, 16, 39, N'Sep', 2019, CAST(46251.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (826, 16, 39, N'Oct', 2019, CAST(54271.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (827, 16, 39, N'Nov', 2019, CAST(47620.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (828, 16, 39, N'Dec', 2019, CAST(46134.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (829, 16, 40, N'Jan', 2019, CAST(61368.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (830, 16, 40, N'Feb', 2019, CAST(63981.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (831, 16, 40, N'Mar', 2019, CAST(77425.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (832, 16, 40, N'Apr', 2019, CAST(68319.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (833, 16, 40, N'May', 2019, CAST(74704.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (834, 16, 40, N'Jun', 2019, CAST(78488.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (835, 16, 40, N'Jul', 2019, CAST(74653.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (836, 16, 40, N'Aug', 2019, CAST(87933.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (837, 16, 40, N'Sep', 2019, CAST(70041.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (838, 16, 40, N'Oct', 2019, CAST(82186.3800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (839, 16, 40, N'Nov', 2019, CAST(72113.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (840, 16, 40, N'Dec', 2019, CAST(69864.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (841, 16, 41, N'Jan', 2019, CAST(106980.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (842, 16, 41, N'Feb', 2019, CAST(111535.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (843, 16, 41, N'Mar', 2019, CAST(134971.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (844, 16, 41, N'Apr', 2019, CAST(119098.1800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (845, 16, 41, N'May', 2019, CAST(130228.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (846, 16, 41, N'Jun', 2019, CAST(136824.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (847, 16, 41, N'Jul', 2019, CAST(130138.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (848, 16, 41, N'Aug', 2019, CAST(153290.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (849, 16, 41, N'Sep', 2019, CAST(122099.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (850, 16, 41, N'Oct', 2019, CAST(143270.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (851, 16, 41, N'Nov', 2019, CAST(125712.0400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (852, 16, 41, N'Dec', 2019, CAST(121790.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (853, 16, 42, N'Jan', 2019, CAST(10417.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (854, 16, 42, N'Feb', 2019, CAST(10860.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (855, 16, 42, N'Mar', 2019, CAST(13143.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (856, 16, 42, N'Apr', 2019, CAST(11597.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (857, 16, 42, N'May', 2019, CAST(12681.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (858, 16, 42, N'Jun', 2019, CAST(13323.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (859, 16, 42, N'Jul', 2019, CAST(12672.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (860, 16, 42, N'Aug', 2019, CAST(14926.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (861, 16, 42, N'Sep', 2019, CAST(11889.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (862, 16, 42, N'Oct', 2019, CAST(13951.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (863, 16, 42, N'Nov', 2019, CAST(12241.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (864, 16, 42, N'Dec', 2019, CAST(11859.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (865, 33, 39, N'Jan', 2019, CAST(49635.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (866, 33, 39, N'Feb', 2019, CAST(51748.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (867, 33, 39, N'Mar', 2019, CAST(62622.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (868, 33, 39, N'Apr', 2019, CAST(55257.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (869, 33, 39, N'May', 2019, CAST(60421.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (870, 33, 39, N'Jun', 2019, CAST(63482.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (871, 33, 39, N'Jul', 2019, CAST(60380.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (872, 33, 39, N'Aug', 2019, CAST(71121.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (873, 33, 39, N'Sep', 2019, CAST(56650.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (874, 33, 39, N'Oct', 2019, CAST(66473.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (875, 33, 39, N'Nov', 2019, CAST(58326.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (876, 33, 39, N'Dec', 2019, CAST(56506.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (877, 33, 40, N'Jan', 2019, CAST(67288.0400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (878, 33, 40, N'Feb', 2019, CAST(70153.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (879, 33, 40, N'Mar', 2019, CAST(84894.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (880, 33, 40, N'Apr', 2019, CAST(74909.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (881, 33, 40, N'May', 2019, CAST(81910.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (882, 33, 40, N'Jun', 2019, CAST(86059.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (883, 33, 40, N'Jul', 2019, CAST(81853.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (884, 33, 40, N'Aug', 2019, CAST(96415.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (885, 33, 40, N'Sep', 2019, CAST(76797.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (886, 33, 40, N'Oct', 2019, CAST(90113.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (887, 33, 40, N'Nov', 2019, CAST(79069.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (888, 33, 40, N'Dec', 2019, CAST(76603.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (889, 33, 41, N'Jan', 2019, CAST(87843.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (890, 33, 41, N'Feb', 2019, CAST(91583.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (891, 33, 41, N'Mar', 2019, CAST(110827.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (892, 33, 41, N'Apr', 2019, CAST(97793.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (893, 33, 41, N'May', 2019, CAST(106932.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (894, 33, 41, N'Jun', 2019, CAST(112348.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (895, 33, 41, N'Jul', 2019, CAST(106858.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (896, 33, 41, N'Aug', 2019, CAST(125868.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (897, 33, 41, N'Sep', 2019, CAST(100257.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (898, 33, 41, N'Oct', 2019, CAST(117641.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (899, 33, 41, N'Nov', 2019, CAST(103223.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (900, 33, 41, N'Dec', 2019, CAST(100003.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (901, 33, 42, N'Jan', 2019, CAST(8780.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (902, 33, 42, N'Feb', 2019, CAST(9154.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (903, 33, 42, N'Mar', 2019, CAST(11077.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (904, 33, 42, N'Apr', 2019, CAST(9774.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (905, 33, 42, N'May', 2019, CAST(10688.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (906, 33, 42, N'Jun', 2019, CAST(11229.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (907, 33, 42, N'Jul', 2019, CAST(10680.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (908, 33, 42, N'Aug', 2019, CAST(12581.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (909, 33, 42, N'Sep', 2019, CAST(10021.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (910, 33, 42, N'Oct', 2019, CAST(11758.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (911, 33, 42, N'Nov', 2019, CAST(10317.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (912, 33, 42, N'Dec', 2019, CAST(9995.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (913, 17, 39, N'Jan', 2019, CAST(29153.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (914, 17, 39, N'Feb', 2019, CAST(30394.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (915, 17, 39, N'Mar', 2019, CAST(36781.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (916, 17, 39, N'Apr', 2019, CAST(32455.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (917, 17, 39, N'May', 2019, CAST(35488.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (918, 17, 39, N'Jun', 2019, CAST(37285.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (919, 17, 39, N'Jul', 2019, CAST(35463.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (920, 17, 39, N'Aug', 2019, CAST(41772.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (921, 17, 39, N'Sep', 2019, CAST(33273.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (922, 17, 39, N'Oct', 2019, CAST(39042.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (923, 17, 39, N'Nov', 2019, CAST(34257.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (924, 17, 39, N'Dec', 2019, CAST(33188.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (925, 17, 40, N'Jan', 2019, CAST(79969.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (926, 17, 40, N'Feb', 2019, CAST(83374.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (927, 17, 40, N'Mar', 2019, CAST(100893.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (928, 17, 40, N'Apr', 2019, CAST(89027.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (929, 17, 40, N'May', 2019, CAST(97347.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (930, 17, 40, N'Jun', 2019, CAST(102278.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (931, 17, 40, N'Jul', 2019, CAST(97280.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (932, 17, 40, N'Aug', 2019, CAST(114586.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (933, 17, 40, N'Sep', 2019, CAST(91271.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (934, 17, 40, N'Oct', 2019, CAST(107097.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (935, 17, 40, N'Nov', 2019, CAST(93971.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (936, 17, 40, N'Dec', 2019, CAST(91040.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (937, 17, 41, N'Jan', 2019, CAST(69380.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (938, 17, 41, N'Feb', 2019, CAST(72334.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (939, 17, 41, N'Mar', 2019, CAST(87533.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (940, 17, 41, N'Apr', 2019, CAST(77239.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (941, 17, 41, N'May', 2019, CAST(84457.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (942, 17, 41, N'Jun', 2019, CAST(88735.1800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (943, 17, 41, N'Jul', 2019, CAST(84399.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (944, 17, 41, N'Aug', 2019, CAST(99413.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (945, 17, 41, N'Sep', 2019, CAST(79185.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (946, 17, 41, N'Oct', 2019, CAST(92915.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (947, 17, 41, N'Nov', 2019, CAST(81528.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (948, 17, 41, N'Dec', 2019, CAST(78985.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (949, 17, 42, N'Jan', 2019, CAST(13218.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (950, 17, 42, N'Feb', 2019, CAST(13781.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (951, 17, 42, N'Mar', 2019, CAST(16676.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (952, 17, 42, N'Apr', 2019, CAST(14715.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (953, 17, 42, N'May', 2019, CAST(16090.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (954, 17, 42, N'Jun', 2019, CAST(16905.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (955, 17, 42, N'Jul', 2019, CAST(16079.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (956, 17, 42, N'Aug', 2019, CAST(18940.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (957, 17, 42, N'Sep', 2019, CAST(15086.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (958, 17, 42, N'Oct', 2019, CAST(17702.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (959, 17, 42, N'Nov', 2019, CAST(15532.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (960, 17, 42, N'Dec', 2019, CAST(15048.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (961, 35, 39, N'Jan', 2019, CAST(49681.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (962, 35, 39, N'Feb', 2019, CAST(51796.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (963, 35, 39, N'Mar', 2019, CAST(62680.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (964, 35, 39, N'Apr', 2019, CAST(55308.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (965, 35, 39, N'May', 2019, CAST(60477.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (966, 35, 39, N'Jun', 2019, CAST(63540.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (967, 35, 39, N'Jul', 2019, CAST(60435.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (968, 35, 39, N'Aug', 2019, CAST(71187.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (969, 35, 39, N'Sep', 2019, CAST(56702.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (970, 35, 39, N'Oct', 2019, CAST(66534.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (971, 35, 39, N'Nov', 2019, CAST(58380.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (972, 35, 39, N'Dec', 2019, CAST(56558.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (973, 35, 40, N'Jan', 2019, CAST(72362.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (974, 35, 40, N'Feb', 2019, CAST(75443.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (975, 35, 40, N'Mar', 2019, CAST(91296.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (976, 35, 40, N'Apr', 2019, CAST(80559.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (977, 35, 40, N'May', 2019, CAST(88087.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (978, 35, 40, N'Jun', 2019, CAST(92549.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (979, 35, 40, N'Jul', 2019, CAST(88026.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (980, 35, 40, N'Aug', 2019, CAST(103686.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (981, 35, 40, N'Sep', 2019, CAST(82589.4600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (982, 35, 40, N'Oct', 2019, CAST(96909.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (983, 35, 40, N'Nov', 2019, CAST(85032.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (984, 35, 40, N'Dec', 2019, CAST(82380.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (985, 35, 41, N'Jan', 2019, CAST(67507.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (986, 35, 41, N'Feb', 2019, CAST(70381.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (987, 35, 41, N'Mar', 2019, CAST(85170.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (988, 35, 41, N'Apr', 2019, CAST(75154.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (989, 35, 41, N'May', 2019, CAST(82177.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (990, 35, 41, N'Jun', 2019, CAST(86339.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (991, 35, 41, N'Jul', 2019, CAST(82120.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (992, 35, 41, N'Aug', 2019, CAST(96730.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (993, 35, 41, N'Sep', 2019, CAST(77048.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (994, 35, 41, N'Oct', 2019, CAST(90407.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (995, 35, 41, N'Nov', 2019, CAST(79327.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (996, 35, 41, N'Dec', 2019, CAST(76852.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (997, 35, 42, N'Jan', 2019, CAST(13399.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (998, 35, 42, N'Feb', 2019, CAST(13970.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (999, 35, 42, N'Mar', 2019, CAST(16905.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1000, 35, 42, N'Apr', 2019, CAST(14917.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1001, 35, 42, N'May', 2019, CAST(16311.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1002, 35, 42, N'Jun', 2019, CAST(17137.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1003, 35, 42, N'Jul', 2019, CAST(16300.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1004, 35, 42, N'Aug', 2019, CAST(19200.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1005, 35, 42, N'Sep', 2019, CAST(15293.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1006, 35, 42, N'Oct', 2019, CAST(17945.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1007, 35, 42, N'Nov', 2019, CAST(15745.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1008, 35, 42, N'Dec', 2019, CAST(15254.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1009, 18, 39, N'Jan', 2019, CAST(24675.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1010, 18, 39, N'Feb', 2019, CAST(25725.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1011, 18, 39, N'Mar', 2019, CAST(31131.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1012, 18, 39, N'Apr', 2019, CAST(27470.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1013, 18, 39, N'May', 2019, CAST(30037.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1014, 18, 39, N'Jun', 2019, CAST(31558.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1015, 18, 39, N'Jul', 2019, CAST(30016.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1016, 18, 39, N'Aug', 2019, CAST(35356.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1017, 18, 39, N'Sep', 2019, CAST(28162.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1018, 18, 39, N'Oct', 2019, CAST(33045.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1019, 18, 39, N'Nov', 2019, CAST(28995.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1020, 18, 39, N'Dec', 2019, CAST(28091.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1021, 18, 40, N'Jan', 2019, CAST(89057.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1022, 18, 40, N'Feb', 2019, CAST(92849.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1023, 18, 40, N'Mar', 2019, CAST(112359.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1024, 18, 40, N'Apr', 2019, CAST(99145.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1025, 18, 40, N'May', 2019, CAST(108410.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1026, 18, 40, N'Jun', 2019, CAST(113902.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1027, 18, 40, N'Jul', 2019, CAST(108336.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1028, 18, 40, N'Aug', 2019, CAST(127609.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1029, 18, 40, N'Sep', 2019, CAST(101644.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1030, 18, 40, N'Oct', 2019, CAST(119268.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1031, 18, 40, N'Nov', 2019, CAST(104651.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1032, 18, 40, N'Dec', 2019, CAST(101386.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1033, 18, 41, N'Jan', 2019, CAST(49701.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1034, 18, 41, N'Feb', 2019, CAST(51817.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1035, 18, 41, N'Mar', 2019, CAST(62705.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1036, 18, 41, N'Apr', 2019, CAST(55330.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1037, 18, 41, N'May', 2019, CAST(60501.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1038, 18, 41, N'Jun', 2019, CAST(63566.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1039, 18, 41, N'Jul', 2019, CAST(60459.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1040, 18, 41, N'Aug', 2019, CAST(71215.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1041, 18, 41, N'Sep', 2019, CAST(56725.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1042, 18, 41, N'Oct', 2019, CAST(66560.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1043, 18, 41, N'Nov', 2019, CAST(58403.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1044, 18, 41, N'Dec', 2019, CAST(56581.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1045, 18, 42, N'Jan', 2019, CAST(10989.4600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1046, 18, 42, N'Feb', 2019, CAST(11457.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1047, 18, 42, N'Mar', 2019, CAST(13864.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1048, 18, 42, N'Apr', 2019, CAST(12234.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1049, 18, 42, N'May', 2019, CAST(13377.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1050, 18, 42, N'Jun', 2019, CAST(14055.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1051, 18, 42, N'Jul', 2019, CAST(13368.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1052, 18, 42, N'Aug', 2019, CAST(15746.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1053, 18, 42, N'Sep', 2019, CAST(12542.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1054, 18, 42, N'Oct', 2019, CAST(14717.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1055, 18, 42, N'Nov', 2019, CAST(12913.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1056, 18, 42, N'Dec', 2019, CAST(12510.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1057, 37, 39, N'Jan', 2019, CAST(22714.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1058, 37, 39, N'Feb', 2019, CAST(23682.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1059, 37, 39, N'Mar', 2019, CAST(28658.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1060, 37, 39, N'Apr', 2019, CAST(25287.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1061, 37, 39, N'May', 2019, CAST(27650.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1062, 37, 39, N'Jun', 2019, CAST(29051.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1063, 37, 39, N'Jul', 2019, CAST(27631.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1064, 37, 39, N'Aug', 2019, CAST(32547.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1065, 37, 39, N'Sep', 2019, CAST(25925.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1066, 37, 39, N'Oct', 2019, CAST(30420.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1067, 37, 39, N'Nov', 2019, CAST(26692.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1068, 37, 39, N'Dec', 2019, CAST(25859.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1069, 37, 40, N'Jan', 2019, CAST(58525.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1070, 37, 40, N'Feb', 2019, CAST(61017.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1071, 37, 40, N'Mar', 2019, CAST(73839.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1072, 37, 40, N'Apr', 2019, CAST(65155.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1073, 37, 40, N'May', 2019, CAST(71243.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1074, 37, 40, N'Jun', 2019, CAST(74852.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1075, 37, 40, N'Jul', 2019, CAST(71194.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1076, 37, 40, N'Aug', 2019, CAST(83860.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1077, 37, 40, N'Sep', 2019, CAST(66797.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1078, 37, 40, N'Oct', 2019, CAST(78379.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1079, 37, 40, N'Nov', 2019, CAST(68773.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1080, 37, 40, N'Dec', 2019, CAST(66627.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1081, 37, 41, N'Jan', 2019, CAST(69195.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1082, 37, 41, N'Feb', 2019, CAST(72142.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1083, 37, 41, N'Mar', 2019, CAST(87301.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1084, 37, 41, N'Apr', 2019, CAST(77033.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1085, 37, 41, N'May', 2019, CAST(84232.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1086, 37, 41, N'Jun', 2019, CAST(88499.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1087, 37, 41, N'Jul', 2019, CAST(84174.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1088, 37, 41, N'Aug', 2019, CAST(99149.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1089, 37, 41, N'Sep', 2019, CAST(78975.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1090, 37, 41, N'Oct', 2019, CAST(92668.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1091, 37, 41, N'Nov', 2019, CAST(81311.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1092, 37, 41, N'Dec', 2019, CAST(78775.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1093, 37, 42, N'Jan', 2019, CAST(9099.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1094, 37, 42, N'Feb', 2019, CAST(9486.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1095, 37, 42, N'Mar', 2019, CAST(11480.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1096, 37, 42, N'Apr', 2019, CAST(10130.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1097, 37, 42, N'May', 2019, CAST(11076.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1098, 37, 42, N'Jun', 2019, CAST(11637.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1099, 37, 42, N'Jul', 2019, CAST(11069.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1100, 37, 42, N'Aug', 2019, CAST(13038.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1101, 37, 42, N'Sep', 2019, CAST(10385.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1102, 37, 42, N'Oct', 2019, CAST(12186.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1103, 37, 42, N'Nov', 2019, CAST(10692.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1104, 37, 42, N'Dec', 2019, CAST(10359.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1105, 20, 39, N'Jan', 2019, CAST(29926.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1106, 20, 39, N'Feb', 2019, CAST(31200.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1107, 20, 39, N'Mar', 2019, CAST(37756.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1108, 20, 39, N'Apr', 2019, CAST(33316.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1109, 20, 39, N'May', 2019, CAST(36429.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1110, 20, 39, N'Jun', 2019, CAST(38274.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1111, 20, 39, N'Jul', 2019, CAST(36404.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1112, 20, 39, N'Aug', 2019, CAST(42881.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1113, 20, 39, N'Sep', 2019, CAST(34155.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1114, 20, 39, N'Oct', 2019, CAST(40078.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1115, 20, 39, N'Nov', 2019, CAST(35166.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1116, 20, 39, N'Dec', 2019, CAST(34069.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1117, 20, 40, N'Jan', 2019, CAST(82537.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1118, 20, 40, N'Feb', 2019, CAST(86051.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1119, 20, 40, N'Mar', 2019, CAST(104133.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1120, 20, 40, N'Apr', 2019, CAST(91886.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1121, 20, 40, N'May', 2019, CAST(100473.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1122, 20, 40, N'Jun', 2019, CAST(105562.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1123, 20, 40, N'Jul', 2019, CAST(100403.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1124, 20, 40, N'Aug', 2019, CAST(118265.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1125, 20, 40, N'Sep', 2019, CAST(94202.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1126, 20, 40, N'Oct', 2019, CAST(110535.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1127, 20, 40, N'Nov', 2019, CAST(96988.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1128, 20, 40, N'Dec', 2019, CAST(93963.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1129, 20, 41, N'Jan', 2019, CAST(71113.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1130, 20, 41, N'Feb', 2019, CAST(74141.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1131, 20, 41, N'Mar', 2019, CAST(89720.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1132, 20, 41, N'Apr', 2019, CAST(79169.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1133, 20, 41, N'May', 2019, CAST(86567.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1134, 20, 41, N'Jun', 2019, CAST(90952.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1135, 20, 41, N'Jul', 2019, CAST(86508.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1136, 20, 41, N'Aug', 2019, CAST(101897.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1137, 20, 41, N'Sep', 2019, CAST(81164.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1138, 20, 41, N'Oct', 2019, CAST(95237.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1139, 20, 41, N'Nov', 2019, CAST(83565.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1140, 20, 41, N'Dec', 2019, CAST(80958.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1141, 20, 42, N'Jan', 2019, CAST(8823.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1142, 20, 42, N'Feb', 2019, CAST(9199.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1143, 20, 42, N'Mar', 2019, CAST(11132.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1144, 20, 42, N'Apr', 2019, CAST(9823.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1145, 20, 42, N'May', 2019, CAST(10741.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1146, 20, 42, N'Jun', 2019, CAST(11285.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1147, 20, 42, N'Jul', 2019, CAST(10734.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1148, 20, 42, N'Aug', 2019, CAST(12643.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1149, 20, 42, N'Sep', 2019, CAST(10071.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1150, 20, 42, N'Oct', 2019, CAST(11817.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1151, 20, 42, N'Nov', 2019, CAST(10369.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1152, 20, 42, N'Dec', 2019, CAST(10045.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1153, 21, 39, N'Jan', 2019, CAST(30370.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1154, 21, 39, N'Feb', 2019, CAST(31663.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1155, 21, 39, N'Mar', 2019, CAST(38316.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1156, 21, 39, N'Apr', 2019, CAST(33810.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1157, 21, 39, N'May', 2019, CAST(36969.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1158, 21, 39, N'Jun', 2019, CAST(38842.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1159, 21, 39, N'Jul', 2019, CAST(36944.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1160, 21, 39, N'Aug', 2019, CAST(43516.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1161, 21, 39, N'Sep', 2019, CAST(34662.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1162, 21, 39, N'Oct', 2019, CAST(40672.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1163, 21, 39, N'Nov', 2019, CAST(35687.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1164, 21, 39, N'Dec', 2019, CAST(34574.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1165, 21, 40, N'Jan', 2019, CAST(117589.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1166, 21, 40, N'Feb', 2019, CAST(122595.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1167, 21, 40, N'Mar', 2019, CAST(148356.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1168, 21, 40, N'Apr', 2019, CAST(130908.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1169, 21, 40, N'May', 2019, CAST(143142.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1170, 21, 40, N'Jun', 2019, CAST(150392.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1171, 21, 40, N'Jul', 2019, CAST(143043.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1172, 21, 40, N'Aug', 2019, CAST(168491.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1173, 21, 40, N'Sep', 2019, CAST(134207.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1174, 21, 40, N'Oct', 2019, CAST(157478.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1175, 21, 40, N'Nov', 2019, CAST(138178.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1176, 21, 40, N'Dec', 2019, CAST(133867.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1177, 21, 41, N'Jan', 2019, CAST(72453.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1178, 21, 41, N'Feb', 2019, CAST(75538.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1179, 21, 41, N'Mar', 2019, CAST(91411.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1180, 21, 41, N'Apr', 2019, CAST(80660.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1181, 21, 41, N'May', 2019, CAST(88198.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1182, 21, 41, N'Jun', 2019, CAST(92665.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1183, 21, 41, N'Jul', 2019, CAST(88137.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1184, 21, 41, N'Aug', 2019, CAST(103817.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1185, 21, 41, N'Sep', 2019, CAST(82693.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1186, 21, 41, N'Oct', 2019, CAST(97031.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1187, 21, 41, N'Nov', 2019, CAST(85139.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1188, 21, 41, N'Dec', 2019, CAST(82483.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1189, 21, 42, N'Jan', 2019, CAST(13500.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1190, 21, 42, N'Feb', 2019, CAST(14075.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1191, 21, 42, N'Mar', 2019, CAST(17033.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1192, 21, 42, N'Apr', 2019, CAST(15029.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1193, 21, 42, N'May', 2019, CAST(16434.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1194, 21, 42, N'Jun', 2019, CAST(17266.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1195, 21, 42, N'Jul', 2019, CAST(16423.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1196, 21, 42, N'Aug', 2019, CAST(19344.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1197, 21, 42, N'Sep', 2019, CAST(15408.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1198, 21, 42, N'Oct', 2019, CAST(18080.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1199, 21, 42, N'Nov', 2019, CAST(15864.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1200, 21, 42, N'Dec', 2019, CAST(15369.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1201, 8, 39, N'Jan', 2019, CAST(14737.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1202, 8, 39, N'Feb', 2019, CAST(15365.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1203, 8, 39, N'Mar', 2019, CAST(18594.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1204, 8, 39, N'Apr', 2019, CAST(16407.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1205, 8, 39, N'May', 2019, CAST(17940.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1206, 8, 39, N'Jun', 2019, CAST(18849.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1207, 8, 39, N'Jul', 2019, CAST(17928.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1208, 8, 39, N'Aug', 2019, CAST(21117.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1209, 8, 39, N'Sep', 2019, CAST(16820.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1210, 8, 39, N'Oct', 2019, CAST(19737.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1211, 8, 39, N'Nov', 2019, CAST(17318.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1212, 8, 39, N'Dec', 2019, CAST(16778.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1213, 8, 40, N'Jan', 2019, CAST(54800.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1214, 8, 40, N'Feb', 2019, CAST(57134.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1215, 8, 40, N'Mar', 2019, CAST(69139.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1216, 8, 40, N'Apr', 2019, CAST(61008.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1217, 8, 40, N'May', 2019, CAST(66709.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1218, 8, 40, N'Jun', 2019, CAST(70088.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1219, 8, 40, N'Jul', 2019, CAST(66663.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1220, 8, 40, N'Aug', 2019, CAST(78523.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1221, 8, 40, N'Sep', 2019, CAST(62545.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1222, 8, 40, N'Oct', 2019, CAST(73390.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1223, 8, 40, N'Nov', 2019, CAST(64396.1800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1224, 8, 40, N'Dec', 2019, CAST(62387.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1225, 8, 41, N'Jan', 2019, CAST(69335.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1226, 8, 41, N'Feb', 2019, CAST(72287.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1227, 8, 41, N'Mar', 2019, CAST(87476.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1228, 8, 41, N'Apr', 2019, CAST(77188.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1229, 8, 41, N'May', 2019, CAST(84402.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1230, 8, 41, N'Jun', 2019, CAST(88677.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1231, 8, 41, N'Jul', 2019, CAST(84344.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1232, 8, 41, N'Aug', 2019, CAST(99348.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1233, 8, 41, N'Sep', 2019, CAST(79134.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1234, 8, 41, N'Oct', 2019, CAST(92855.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1235, 8, 41, N'Nov', 2019, CAST(81475.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1236, 8, 41, N'Dec', 2019, CAST(78933.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1237, 8, 42, N'Jan', 2019, CAST(13091.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1238, 8, 42, N'Feb', 2019, CAST(13648.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1239, 8, 42, N'Mar', 2019, CAST(16516.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1240, 8, 42, N'Apr', 2019, CAST(14574.1800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1241, 8, 42, N'May', 2019, CAST(15936.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1242, 8, 42, N'Jun', 2019, CAST(16743.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1243, 8, 42, N'Jul', 2019, CAST(15925.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1244, 8, 42, N'Aug', 2019, CAST(18758.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1245, 8, 42, N'Sep', 2019, CAST(14941.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1246, 8, 42, N'Oct', 2019, CAST(17532.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1247, 8, 42, N'Nov', 2019, CAST(15383.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1248, 8, 42, N'Dec', 2019, CAST(14903.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1249, 25, 39, N'Jan', 2019, CAST(21814.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1250, 25, 39, N'Feb', 2019, CAST(22742.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1251, 25, 39, N'Mar', 2019, CAST(27521.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1252, 25, 39, N'Apr', 2019, CAST(24285.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1253, 25, 39, N'May', 2019, CAST(26554.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1254, 25, 39, N'Jun', 2019, CAST(27899.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1255, 25, 39, N'Jul', 2019, CAST(26536.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1256, 25, 39, N'Aug', 2019, CAST(31257.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1257, 25, 39, N'Sep', 2019, CAST(24897.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1258, 25, 39, N'Oct', 2019, CAST(29214.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1259, 25, 39, N'Nov', 2019, CAST(25633.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1260, 25, 39, N'Dec', 2019, CAST(24834.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1261, 25, 40, N'Jan', 2019, CAST(69220.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1262, 25, 40, N'Feb', 2019, CAST(72168.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1263, 25, 40, N'Mar', 2019, CAST(87332.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1264, 25, 40, N'Apr', 2019, CAST(77061.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1265, 25, 40, N'May', 2019, CAST(84263.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1266, 25, 40, N'Jun', 2019, CAST(88531.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1267, 25, 40, N'Jul', 2019, CAST(84205.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1268, 25, 40, N'Aug', 2019, CAST(99185.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1269, 25, 40, N'Sep', 2019, CAST(79003.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1270, 25, 40, N'Oct', 2019, CAST(92702.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1271, 25, 40, N'Nov', 2019, CAST(81341.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1272, 25, 40, N'Dec', 2019, CAST(78803.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1273, 25, 41, N'Jan', 2019, CAST(52501.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1274, 25, 41, N'Feb', 2019, CAST(54736.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1275, 25, 41, N'Mar', 2019, CAST(66238.1800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1276, 25, 41, N'Apr', 2019, CAST(58448.0400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1277, 25, 41, N'May', 2019, CAST(63910.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1278, 25, 41, N'Jun', 2019, CAST(67147.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1279, 25, 41, N'Jul', 2019, CAST(63866.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1280, 25, 41, N'Aug', 2019, CAST(75227.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1281, 25, 41, N'Sep', 2019, CAST(59921.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1282, 25, 41, N'Oct', 2019, CAST(70310.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1283, 25, 41, N'Nov', 2019, CAST(61693.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1284, 25, 41, N'Dec', 2019, CAST(59769.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1285, 25, 42, N'Jan', 2019, CAST(9981.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1286, 25, 42, N'Feb', 2019, CAST(10406.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1287, 25, 42, N'Mar', 2019, CAST(12593.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1288, 25, 42, N'Apr', 2019, CAST(11112.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1289, 25, 42, N'May', 2019, CAST(12150.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1290, 25, 42, N'Jun', 2019, CAST(12766.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1291, 25, 42, N'Jul', 2019, CAST(12142.3800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1292, 25, 42, N'Aug', 2019, CAST(14302.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1293, 25, 42, N'Sep', 2019, CAST(11392.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1294, 25, 42, N'Oct', 2019, CAST(13367.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1295, 25, 42, N'Nov', 2019, CAST(11729.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1296, 25, 42, N'Dec', 2019, CAST(11363.4600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1297, 27, 39, N'Jan', 2019, CAST(37936.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1298, 27, 39, N'Feb', 2019, CAST(39551.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1299, 27, 39, N'Mar', 2019, CAST(47862.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1300, 27, 39, N'Apr', 2019, CAST(42233.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1301, 27, 39, N'May', 2019, CAST(46180.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1302, 27, 39, N'Jun', 2019, CAST(48519.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1303, 27, 39, N'Jul', 2019, CAST(46148.4600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1304, 27, 39, N'Aug', 2019, CAST(54358.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1305, 27, 39, N'Sep', 2019, CAST(43297.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1306, 27, 39, N'Oct', 2019, CAST(50805.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1307, 27, 39, N'Nov', 2019, CAST(44578.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1308, 27, 39, N'Dec', 2019, CAST(43188.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1309, 27, 40, N'Jan', 2019, CAST(75503.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1310, 27, 40, N'Feb', 2019, CAST(78718.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1311, 27, 40, N'Mar', 2019, CAST(95259.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1312, 27, 40, N'Apr', 2019, CAST(84055.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1313, 27, 40, N'May', 2019, CAST(91911.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1314, 27, 40, N'Jun', 2019, CAST(96566.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1315, 27, 40, N'Jul', 2019, CAST(91847.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1316, 27, 40, N'Aug', 2019, CAST(108187.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1317, 27, 40, N'Sep', 2019, CAST(86174.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1318, 27, 40, N'Oct', 2019, CAST(101116.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1319, 27, 40, N'Nov', 2019, CAST(88723.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1320, 27, 40, N'Dec', 2019, CAST(85955.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1321, 27, 41, N'Jan', 2019, CAST(58945.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1322, 27, 41, N'Feb', 2019, CAST(61454.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1323, 27, 41, N'Mar', 2019, CAST(74368.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1324, 27, 41, N'Apr', 2019, CAST(65622.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1325, 27, 41, N'May', 2019, CAST(71754.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1326, 27, 41, N'Jun', 2019, CAST(75389.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1327, 27, 41, N'Jul', 2019, CAST(71705.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1328, 27, 41, N'Aug', 2019, CAST(84461.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1329, 27, 41, N'Sep', 2019, CAST(67275.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1330, 27, 41, N'Oct', 2019, CAST(78940.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1331, 27, 41, N'Nov', 2019, CAST(69266.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1332, 27, 41, N'Dec', 2019, CAST(67105.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1333, 27, 42, N'Jan', 2019, CAST(12160.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1334, 27, 42, N'Feb', 2019, CAST(12678.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1335, 27, 42, N'Mar', 2019, CAST(15342.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1336, 27, 42, N'Apr', 2019, CAST(13538.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1337, 27, 42, N'May', 2019, CAST(14803.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1338, 27, 42, N'Jun', 2019, CAST(15553.4600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1339, 27, 42, N'Jul', 2019, CAST(14793.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1340, 27, 42, N'Aug', 2019, CAST(17425.1800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1341, 27, 42, N'Sep', 2019, CAST(13879.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1342, 27, 42, N'Oct', 2019, CAST(16286.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1343, 27, 42, N'Nov', 2019, CAST(14290.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportProductFamilyQuota] ([Id], [SalesRepId], [ProductFamilyId], [Month], [Year], [Revenue]) VALUES (1344, 27, 42, N'Dec', 2019, CAST(13844.4600 AS Decimal(19, 4)))
GO
SET IDENTITY_INSERT [dbo].[ReportProductFamilyQuota] OFF

/****** Object:  Table [dbo].[ReportSalesChannelQuota]    Script Date: 14.8.2019. 16:16:34 ******/

 IF exists(SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ReportSalesChannelQuota' AND TABLE_CATALOG = 'ProfitOptics.Framework')
 BEGIN
 Drop Table ReportSalesChannelQuota
 END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportSalesChannelQuota](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesRepId] [int] NOT NULL,
	[SalesChannelId] [int] NOT NULL,
	[Month] [nvarchar](20) NOT NULL,
	[Year] [int] NOT NULL,
	[Revenue] [decimal](19, 4) NOT NULL,
 CONSTRAINT [PK_ReportSalesChannelQuota] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ReportSalesChannelQuota] ON 
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1, 8, 31, N'Apr', 2018, CAST(59778.6565 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (2, 8, 31, N'Aug', 2018, CAST(84442.0515 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (3, 8, 31, N'Dec', 2018, CAST(136920.2621 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (4, 8, 31, N'Feb', 2018, CAST(69696.2193 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (5, 8, 31, N'Jan', 2018, CAST(71694.6555 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (6, 8, 31, N'Jul', 2018, CAST(112487.6830 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (7, 8, 31, N'Jun', 2018, CAST(100747.3642 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (8, 8, 31, N'Mar', 2018, CAST(89863.1624 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (9, 8, 31, N'May', 2018, CAST(73478.8611 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (10, 8, 31, N'Nov', 2018, CAST(70894.2381 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (11, 8, 31, N'Oct', 2018, CAST(87959.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (12, 8, 31, N'Sep', 2018, CAST(90512.7646 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (13, 8, 34, N'Apr', 2018, CAST(36061.3867 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (14, 8, 34, N'Aug', 2018, CAST(33583.8043 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (15, 8, 34, N'Dec', 2018, CAST(50995.3416 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (16, 8, 34, N'Feb', 2018, CAST(46188.4157 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (17, 8, 34, N'Jan', 2018, CAST(42185.3068 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (18, 8, 34, N'Jul', 2018, CAST(45968.3559 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (19, 8, 34, N'Jun', 2018, CAST(26067.6782 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (20, 8, 34, N'Mar', 2018, CAST(58054.4188 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (21, 8, 34, N'May', 2018, CAST(37983.7735 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (22, 8, 34, N'Nov', 2018, CAST(46705.6081 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (23, 8, 34, N'Oct', 2018, CAST(47236.7518 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (24, 8, 34, N'Sep', 2018, CAST(47580.0864 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (25, 8, 35, N'Apr', 2018, CAST(32416.0553 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (26, 8, 35, N'Aug', 2018, CAST(22820.6092 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (27, 8, 35, N'Dec', 2018, CAST(24979.7132 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (28, 8, 35, N'Feb', 2018, CAST(25373.2894 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (29, 8, 35, N'Jan', 2018, CAST(9395.1778 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (30, 8, 35, N'Jul', 2018, CAST(19174.7942 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (31, 8, 35, N'Jun', 2018, CAST(32568.2895 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (32, 8, 35, N'Mar', 2018, CAST(29885.2874 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (33, 8, 35, N'May', 2018, CAST(22639.6793 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (34, 8, 35, N'Nov', 2018, CAST(30463.5420 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (35, 8, 35, N'Oct', 2018, CAST(28341.8271 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (36, 8, 35, N'Sep', 2018, CAST(22709.3780 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (37, 13, 31, N'Apr', 2018, CAST(95568.5771 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (38, 13, 31, N'Aug', 2018, CAST(91346.4658 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (39, 13, 31, N'Dec', 2018, CAST(104167.5150 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (40, 13, 31, N'Feb', 2018, CAST(70536.9059 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (41, 13, 31, N'Jan', 2018, CAST(72147.8550 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (42, 13, 31, N'Jul', 2018, CAST(78602.1657 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (43, 13, 31, N'Jun', 2018, CAST(100613.4406 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (44, 13, 31, N'Mar', 2018, CAST(77671.4991 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (45, 13, 31, N'May', 2018, CAST(76830.5714 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (46, 13, 31, N'Nov', 2018, CAST(65143.2352 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (47, 13, 31, N'Oct', 2018, CAST(80480.9441 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (48, 13, 31, N'Sep', 2018, CAST(77258.7052 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (49, 13, 34, N'Apr', 2018, CAST(80700.1317 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (50, 13, 34, N'Aug', 2018, CAST(149613.5157 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (51, 13, 34, N'Dec', 2018, CAST(91936.5071 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (52, 13, 34, N'Feb', 2018, CAST(78639.3224 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (53, 13, 34, N'Jan', 2018, CAST(74300.0429 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (54, 13, 34, N'Jul', 2018, CAST(93595.3971 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (55, 13, 34, N'Jun', 2018, CAST(79711.0488 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (56, 13, 34, N'Mar', 2018, CAST(134653.5452 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (57, 13, 34, N'May', 2018, CAST(89188.6793 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (58, 13, 34, N'Nov', 2018, CAST(94031.5676 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (59, 13, 34, N'Oct', 2018, CAST(95576.6628 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (60, 13, 34, N'Sep', 2018, CAST(101554.1965 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (61, 13, 35, N'Apr', 2018, CAST(44710.3071 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (62, 13, 35, N'Aug', 2018, CAST(61286.7170 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (63, 13, 35, N'Dec', 2018, CAST(67209.5405 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (64, 13, 35, N'Feb', 2018, CAST(45327.1666 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (65, 13, 35, N'Jan', 2018, CAST(36146.2567 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (66, 13, 35, N'Jul', 2018, CAST(53683.3359 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (67, 13, 35, N'Jun', 2018, CAST(66241.1187 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (68, 13, 35, N'Mar', 2018, CAST(65495.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (69, 13, 35, N'May', 2018, CAST(57225.4457 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (70, 13, 35, N'Nov', 2018, CAST(46904.8283 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (71, 13, 35, N'Oct', 2018, CAST(61521.8712 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (72, 13, 35, N'Sep', 2018, CAST(51188.3025 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (73, 14, 31, N'Apr', 2018, CAST(131240.9858 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (74, 14, 31, N'Aug', 2018, CAST(154587.2235 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (75, 14, 31, N'Dec', 2018, CAST(118687.0751 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (76, 14, 31, N'Feb', 2018, CAST(132294.1864 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (77, 14, 31, N'Jan', 2018, CAST(114257.4650 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (78, 14, 31, N'Jul', 2018, CAST(137915.2785 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (79, 14, 31, N'Jun', 2018, CAST(112018.4203 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (80, 14, 31, N'Mar', 2018, CAST(149846.8301 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (81, 14, 31, N'May', 2018, CAST(124065.4302 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (82, 14, 31, N'Nov', 2018, CAST(146914.3742 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (83, 14, 31, N'Oct', 2018, CAST(126413.2474 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (84, 14, 31, N'Sep', 2018, CAST(115442.7354 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (85, 14, 34, N'Apr', 2018, CAST(107886.3623 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (86, 14, 34, N'Aug', 2018, CAST(94285.1968 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (87, 14, 34, N'Dec', 2018, CAST(86941.4681 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (88, 14, 34, N'Feb', 2018, CAST(72151.7327 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (89, 14, 34, N'Jan', 2018, CAST(73224.3440 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (90, 14, 34, N'Jul', 2018, CAST(92994.2230 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (91, 14, 34, N'Jun', 2018, CAST(135853.1776 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (92, 14, 34, N'Mar', 2018, CAST(83460.5861 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (93, 14, 34, N'May', 2018, CAST(66669.0122 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (94, 14, 34, N'Nov', 2018, CAST(111862.9543 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (95, 14, 34, N'Oct', 2018, CAST(89107.6822 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (96, 14, 34, N'Sep', 2018, CAST(108857.3978 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (97, 14, 35, N'Apr', 2018, CAST(14012.8971 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (98, 14, 35, N'Aug', 2018, CAST(26362.0539 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (99, 14, 35, N'Dec', 2018, CAST(19737.6011 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (100, 14, 35, N'Feb', 2018, CAST(14891.5945 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (101, 14, 35, N'Jan', 2018, CAST(5540.8931 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (102, 14, 35, N'Jul', 2018, CAST(12499.9698 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (103, 14, 35, N'Jun', 2018, CAST(8866.5370 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (104, 14, 35, N'Mar', 2018, CAST(7815.9515 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (105, 14, 35, N'May', 2018, CAST(9753.0754 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (106, 14, 35, N'Nov', 2018, CAST(10167.1606 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (107, 14, 35, N'Oct', 2018, CAST(19378.2661 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (108, 14, 35, N'Sep', 2018, CAST(11115.7550 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (109, 15, 31, N'Apr', 2018, CAST(110349.1079 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (110, 15, 31, N'Aug', 2018, CAST(118653.0182 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (111, 15, 31, N'Dec', 2018, CAST(90630.1875 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (112, 15, 31, N'Feb', 2018, CAST(117826.7577 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (113, 15, 31, N'Jan', 2018, CAST(61124.0479 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (114, 15, 31, N'Jul', 2018, CAST(149687.6853 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (115, 15, 31, N'Jun', 2018, CAST(128062.3882 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (116, 15, 31, N'Mar', 2018, CAST(102155.8510 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (117, 15, 31, N'May', 2018, CAST(141549.6094 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (118, 15, 31, N'Nov', 2018, CAST(104818.9338 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (119, 15, 31, N'Oct', 2018, CAST(124915.7005 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (120, 15, 31, N'Sep', 2018, CAST(110292.1976 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (121, 15, 34, N'Apr', 2018, CAST(23902.7772 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (122, 15, 34, N'Aug', 2018, CAST(42363.2309 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (123, 15, 34, N'Dec', 2018, CAST(27990.8996 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (124, 15, 34, N'Feb', 2018, CAST(21096.5045 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (125, 15, 34, N'Jan', 2018, CAST(31893.5865 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (126, 15, 34, N'Jul', 2018, CAST(23942.6956 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (127, 15, 34, N'Jun', 2018, CAST(39239.3705 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (128, 15, 34, N'Mar', 2018, CAST(25165.7010 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (129, 15, 34, N'May', 2018, CAST(21288.1753 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (130, 15, 34, N'Nov', 2018, CAST(37218.4757 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (131, 15, 34, N'Oct', 2018, CAST(35102.5469 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (132, 15, 34, N'Sep', 2018, CAST(27546.7044 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (133, 15, 35, N'Apr', 2018, CAST(10058.4621 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (134, 15, 35, N'Aug', 2018, CAST(7379.5640 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (135, 15, 35, N'Dec', 2018, CAST(7531.8844 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (136, 15, 35, N'Feb', 2018, CAST(6863.2414 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (137, 15, 35, N'Jan', 2018, CAST(4952.1096 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (138, 15, 35, N'Jul', 2018, CAST(4729.6137 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (139, 15, 35, N'Jun', 2018, CAST(7539.2530 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (140, 15, 35, N'Mar', 2018, CAST(7482.0967 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (141, 15, 35, N'May', 2018, CAST(4209.3460 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (142, 15, 35, N'Nov', 2018, CAST(10788.6412 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (143, 15, 35, N'Oct', 2018, CAST(13018.5160 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (144, 15, 35, N'Sep', 2018, CAST(6547.9134 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (145, 16, 31, N'Apr', 2018, CAST(120347.8707 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (146, 16, 31, N'Aug', 2018, CAST(92432.7040 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (147, 16, 31, N'Dec', 2018, CAST(84575.9242 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (148, 16, 31, N'Feb', 2018, CAST(93432.6641 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (149, 16, 31, N'Jan', 2018, CAST(71982.6998 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (150, 16, 31, N'Jul', 2018, CAST(92432.7040 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (151, 16, 31, N'Jun', 2018, CAST(122607.3337 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (152, 16, 31, N'Mar', 2018, CAST(128759.3491 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (153, 16, 31, N'May', 2018, CAST(158839.3695 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (154, 16, 31, N'Nov', 2018, CAST(84575.9242 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (155, 16, 31, N'Oct', 2018, CAST(84575.9242 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (156, 16, 31, N'Sep', 2018, CAST(84575.9242 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (157, 16, 34, N'Apr', 2018, CAST(96868.3943 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (158, 16, 34, N'Aug', 2018, CAST(74949.6514 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (159, 16, 34, N'Dec', 2018, CAST(68578.9310 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (160, 16, 34, N'Feb', 2018, CAST(60031.8939 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (161, 16, 34, N'Jan', 2018, CAST(52402.4015 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (162, 16, 34, N'Jul', 2018, CAST(74949.6514 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (163, 16, 34, N'Jun', 2018, CAST(104409.0457 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (164, 16, 34, N'Mar', 2018, CAST(97042.5582 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (165, 16, 34, N'May', 2018, CAST(73674.1027 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (166, 16, 34, N'Nov', 2018, CAST(68578.9310 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (167, 16, 34, N'Oct', 2018, CAST(68578.9310 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (168, 16, 34, N'Sep', 2018, CAST(68578.9310 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (169, 16, 35, N'Apr', 2018, CAST(34148.1667 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (170, 16, 35, N'Aug', 2018, CAST(10619.8226 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (171, 16, 35, N'Dec', 2018, CAST(9717.1377 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (172, 16, 35, N'Feb', 2018, CAST(16576.7331 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (173, 16, 35, N'Jan', 2018, CAST(12979.9431 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (174, 16, 35, N'Jul', 2018, CAST(10619.8226 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (175, 16, 35, N'Jun', 2018, CAST(11409.0166 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (176, 16, 35, N'Mar', 2018, CAST(36292.7770 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (177, 16, 35, N'May', 2018, CAST(19153.0686 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (178, 16, 35, N'Nov', 2018, CAST(9717.1377 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (179, 16, 35, N'Oct', 2018, CAST(9717.1377 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (180, 16, 35, N'Sep', 2018, CAST(9717.1377 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (181, 17, 31, N'Apr', 2018, CAST(138361.4814 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (182, 17, 31, N'Aug', 2018, CAST(144054.6648 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (183, 17, 31, N'Dec', 2018, CAST(177451.7633 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (184, 17, 31, N'Feb', 2018, CAST(114657.2541 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (185, 17, 31, N'Jan', 2018, CAST(173149.5387 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (186, 17, 31, N'Jul', 2018, CAST(142309.1660 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (187, 17, 31, N'Jun', 2018, CAST(149088.7594 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (188, 17, 31, N'Mar', 2018, CAST(194654.4174 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (189, 17, 31, N'May', 2018, CAST(193175.2161 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (190, 17, 31, N'Nov', 2018, CAST(138792.9647 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (191, 17, 31, N'Oct', 2018, CAST(128225.0187 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (192, 17, 31, N'Sep', 2018, CAST(172306.1493 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (193, 17, 34, N'Apr', 2018, CAST(54586.2327 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (194, 17, 34, N'Aug', 2018, CAST(84206.8503 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (195, 17, 34, N'Dec', 2018, CAST(87984.8004 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (196, 17, 34, N'Feb', 2018, CAST(63395.4378 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (197, 17, 34, N'Jan', 2018, CAST(76139.1771 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (198, 17, 34, N'Jul', 2018, CAST(54401.7821 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (199, 17, 34, N'Jun', 2018, CAST(102405.6547 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (200, 17, 34, N'Mar', 2018, CAST(107211.2899 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (201, 17, 34, N'May', 2018, CAST(67272.2206 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (202, 17, 34, N'Nov', 2018, CAST(82561.8424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (203, 17, 34, N'Oct', 2018, CAST(68988.8761 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (204, 17, 34, N'Sep', 2018, CAST(48692.1392 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (205, 17, 35, N'Apr', 2018, CAST(13799.1262 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (206, 17, 35, N'Aug', 2018, CAST(16146.4515 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (207, 17, 35, N'Dec', 2018, CAST(33847.3256 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (208, 17, 35, N'Feb', 2018, CAST(10005.3084 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (209, 17, 35, N'Jan', 2018, CAST(15279.8585 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (210, 17, 35, N'Jul', 2018, CAST(19464.8585 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (211, 17, 35, N'Jun', 2018, CAST(14644.3262 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (212, 17, 35, N'Mar', 2018, CAST(17069.6684 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (213, 17, 35, N'May', 2018, CAST(19462.7041 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (214, 17, 35, N'Nov', 2018, CAST(32216.2687 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (215, 17, 35, N'Oct', 2018, CAST(15306.8604 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (216, 17, 35, N'Sep', 2018, CAST(28460.5778 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (217, 18, 31, N'Apr', 2018, CAST(115898.9702 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (218, 18, 31, N'Aug', 2018, CAST(94091.2210 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (219, 18, 31, N'Dec', 2018, CAST(86093.4673 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (220, 18, 31, N'Feb', 2018, CAST(99713.9058 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (221, 18, 31, N'Jan', 2018, CAST(130135.4133 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (222, 18, 31, N'Jul', 2018, CAST(94091.2210 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (223, 18, 31, N'Jun', 2018, CAST(130587.7212 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (224, 18, 31, N'Mar', 2018, CAST(101080.9023 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (225, 18, 31, N'May', 2018, CAST(108683.9167 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (226, 18, 31, N'Nov', 2018, CAST(86093.4673 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (227, 18, 31, N'Oct', 2018, CAST(86093.4673 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (228, 18, 31, N'Sep', 2018, CAST(86093.4673 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (229, 18, 34, N'Apr', 2018, CAST(62386.8404 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (230, 18, 34, N'Aug', 2018, CAST(55420.8304 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (231, 18, 34, N'Dec', 2018, CAST(50710.0598 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (232, 18, 34, N'Feb', 2018, CAST(75697.6389 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (233, 18, 34, N'Jan', 2018, CAST(65452.5223 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (234, 18, 34, N'Jul', 2018, CAST(55420.8304 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (235, 18, 34, N'Jun', 2018, CAST(120920.2573 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (236, 18, 34, N'Mar', 2018, CAST(87167.6590 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (237, 18, 34, N'May', 2018, CAST(105721.4452 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (238, 18, 34, N'Nov', 2018, CAST(50710.0598 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (239, 18, 34, N'Oct', 2018, CAST(50710.0598 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (240, 18, 34, N'Sep', 2018, CAST(50710.0598 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (241, 18, 35, N'Apr', 2018, CAST(50604.1934 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (242, 18, 35, N'Aug', 2018, CAST(49059.9799 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (243, 18, 35, N'Dec', 2018, CAST(44889.8816 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (244, 18, 35, N'Feb', 2018, CAST(48774.6936 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (245, 18, 35, N'Jan', 2018, CAST(40960.8956 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (246, 18, 35, N'Jul', 2018, CAST(49059.9799 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (247, 18, 35, N'Jun', 2018, CAST(45194.7817 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (248, 18, 35, N'Mar', 2018, CAST(61445.2557 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (249, 18, 35, N'May', 2018, CAST(64966.1353 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (250, 18, 35, N'Nov', 2018, CAST(44889.8816 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (251, 18, 35, N'Oct', 2018, CAST(44889.8816 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (252, 18, 35, N'Sep', 2018, CAST(44889.8816 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (253, 20, 31, N'Apr', 2018, CAST(69938.2075 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (254, 20, 31, N'Aug', 2018, CAST(51840.6583 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (255, 20, 31, N'Dec', 2018, CAST(47434.2024 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (256, 20, 31, N'Feb', 2018, CAST(73395.3152 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (257, 20, 31, N'Jan', 2018, CAST(59371.3968 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (258, 20, 31, N'Jul', 2018, CAST(51840.6583 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (259, 20, 31, N'Jun', 2018, CAST(121293.2663 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (260, 20, 31, N'Mar', 2018, CAST(67138.9483 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (261, 20, 31, N'May', 2018, CAST(83760.9543 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (262, 20, 31, N'Nov', 2018, CAST(47434.2024 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (263, 20, 31, N'Oct', 2018, CAST(47434.2024 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (264, 20, 31, N'Sep', 2018, CAST(47434.2024 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (265, 20, 34, N'Apr', 2018, CAST(124370.1273 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (266, 20, 34, N'Aug', 2018, CAST(114982.6692 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (267, 20, 34, N'Dec', 2018, CAST(105209.1424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (268, 20, 34, N'Feb', 2018, CAST(128976.4383 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (269, 20, 34, N'Jan', 2018, CAST(93807.8473 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (270, 20, 34, N'Jul', 2018, CAST(114982.6692 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (271, 20, 34, N'Jun', 2018, CAST(153000.3374 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (272, 20, 34, N'Mar', 2018, CAST(153047.6684 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (273, 20, 34, N'May', 2018, CAST(129573.6978 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (274, 20, 34, N'Nov', 2018, CAST(105209.1424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (275, 20, 34, N'Oct', 2018, CAST(105209.1424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (276, 20, 34, N'Sep', 2018, CAST(105209.1424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (277, 20, 35, N'Apr', 2018, CAST(37428.0435 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (278, 20, 35, N'Aug', 2018, CAST(56901.5724 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (279, 20, 35, N'Dec', 2018, CAST(52064.9387 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (280, 20, 35, N'Feb', 2018, CAST(34114.9147 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (281, 20, 35, N'Jan', 2018, CAST(40360.9081 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (282, 20, 35, N'Jul', 2018, CAST(56901.5724 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (283, 20, 35, N'Jun', 2018, CAST(45315.0440 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (284, 20, 35, N'Mar', 2018, CAST(47955.3967 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (285, 20, 35, N'May', 2018, CAST(63540.7740 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (286, 20, 35, N'Nov', 2018, CAST(52064.9387 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (287, 20, 35, N'Oct', 2018, CAST(52064.9387 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (288, 20, 35, N'Sep', 2018, CAST(52064.9387 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (289, 21, 31, N'Apr', 2018, CAST(112960.8545 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (290, 21, 31, N'Aug', 2018, CAST(113547.3916 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (291, 21, 31, N'Dec', 2018, CAST(103895.8633 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (292, 21, 31, N'Feb', 2018, CAST(99525.5149 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (293, 21, 31, N'Jan', 2018, CAST(87869.4409 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (294, 21, 31, N'Jul', 2018, CAST(113547.3916 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (295, 21, 31, N'Jun', 2018, CAST(96933.5646 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (296, 21, 31, N'Mar', 2018, CAST(95509.5730 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (297, 21, 31, N'May', 2018, CAST(128343.4758 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (298, 21, 31, N'Nov', 2018, CAST(103895.8633 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (299, 21, 31, N'Oct', 2018, CAST(103895.8633 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (300, 21, 31, N'Sep', 2018, CAST(103895.8633 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (301, 21, 34, N'Apr', 2018, CAST(59958.5032 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (302, 21, 34, N'Aug', 2018, CAST(63275.0392 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (303, 21, 34, N'Dec', 2018, CAST(57896.6609 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (304, 21, 34, N'Feb', 2018, CAST(49773.8620 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (305, 21, 34, N'Jan', 2018, CAST(38275.0344 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (306, 21, 34, N'Jul', 2018, CAST(63275.0392 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (307, 21, 34, N'Jun', 2018, CAST(63113.8293 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (308, 21, 34, N'Mar', 2018, CAST(78934.6830 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (309, 21, 34, N'May', 2018, CAST(58166.7488 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (310, 21, 34, N'Nov', 2018, CAST(57896.6609 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (311, 21, 34, N'Oct', 2018, CAST(57896.6609 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (312, 21, 34, N'Sep', 2018, CAST(57896.6609 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (313, 21, 35, N'Apr', 2018, CAST(56591.2302 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (314, 21, 35, N'Aug', 2018, CAST(64910.9490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (315, 21, 35, N'Dec', 2018, CAST(59393.5183 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (316, 21, 35, N'Feb', 2018, CAST(53580.7111 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (317, 21, 35, N'Jan', 2018, CAST(40096.5674 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (318, 21, 35, N'Jul', 2018, CAST(64910.9490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (319, 21, 35, N'Jun', 2018, CAST(66431.7460 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (320, 21, 35, N'Mar', 2018, CAST(55554.3702 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (321, 21, 35, N'May', 2018, CAST(53394.0866 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (322, 21, 35, N'Nov', 2018, CAST(59393.5183 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (323, 21, 35, N'Oct', 2018, CAST(59393.5183 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (324, 21, 35, N'Sep', 2018, CAST(59393.5183 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (325, 25, 31, N'Apr', 2018, CAST(36337.4657 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (326, 25, 31, N'Aug', 2018, CAST(54558.4093 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (327, 25, 31, N'Dec', 2018, CAST(56133.3981 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (328, 25, 31, N'Feb', 2018, CAST(27994.1184 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (329, 25, 31, N'Jan', 2018, CAST(39867.2119 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (330, 25, 31, N'Jul', 2018, CAST(44659.3181 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (331, 25, 31, N'Jun', 2018, CAST(35750.1350 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (332, 25, 31, N'Mar', 2018, CAST(70996.1329 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (333, 25, 31, N'May', 2018, CAST(47749.4798 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (334, 25, 31, N'Nov', 2018, CAST(45270.6721 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (335, 25, 31, N'Oct', 2018, CAST(51826.8993 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (336, 25, 31, N'Sep', 2018, CAST(62558.3454 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (337, 25, 34, N'Apr', 2018, CAST(63514.3045 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (338, 25, 34, N'Aug', 2018, CAST(64587.1885 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (339, 25, 34, N'Dec', 2018, CAST(63169.8650 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (340, 25, 34, N'Feb', 2018, CAST(39177.7430 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (341, 25, 34, N'Jan', 2018, CAST(39493.4372 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (342, 25, 34, N'Jul', 2018, CAST(65975.4639 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (343, 25, 34, N'Jun', 2018, CAST(46491.3424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (344, 25, 34, N'Mar', 2018, CAST(57024.9410 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (345, 25, 34, N'May', 2018, CAST(56261.4649 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (346, 25, 34, N'Nov', 2018, CAST(68257.4977 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (347, 25, 34, N'Oct', 2018, CAST(47329.8834 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (348, 25, 34, N'Sep', 2018, CAST(41348.0884 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (349, 25, 35, N'Apr', 2018, CAST(27829.4562 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (350, 25, 35, N'Aug', 2018, CAST(58455.6509 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (351, 25, 35, N'Dec', 2018, CAST(43016.2077 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (352, 25, 35, N'Feb', 2018, CAST(40095.7274 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (353, 25, 35, N'Jan', 2018, CAST(40441.1039 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (354, 25, 35, N'Jul', 2018, CAST(33752.1808 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (355, 25, 35, N'Jun', 2018, CAST(41671.1593 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (356, 25, 35, N'Mar', 2018, CAST(37671.7875 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (357, 25, 35, N'May', 2018, CAST(41175.0344 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (358, 25, 35, N'Nov', 2018, CAST(29939.2250 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (359, 25, 35, N'Oct', 2018, CAST(26267.7070 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (360, 25, 35, N'Sep', 2018, CAST(45297.2328 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (361, 27, 31, N'Apr', 2018, CAST(100839.9512 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (362, 27, 31, N'Aug', 2018, CAST(116835.2523 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (363, 27, 31, N'Dec', 2018, CAST(81435.0593 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (364, 27, 31, N'Feb', 2018, CAST(76770.3246 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (365, 27, 31, N'Jan', 2018, CAST(96154.0117 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (366, 27, 31, N'Jul', 2018, CAST(106792.4424 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (367, 27, 31, N'Jun', 2018, CAST(111901.3713 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (368, 27, 31, N'Mar', 2018, CAST(90904.1433 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (369, 27, 31, N'May', 2018, CAST(113387.0760 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (370, 27, 31, N'Nov', 2018, CAST(118119.5490 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (371, 27, 31, N'Oct', 2018, CAST(157163.5496 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (372, 27, 31, N'Sep', 2018, CAST(115519.7921 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (373, 27, 34, N'Apr', 2018, CAST(45179.2249 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (374, 27, 34, N'Aug', 2018, CAST(51914.4010 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (375, 27, 34, N'Dec', 2018, CAST(53106.1766 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (376, 27, 34, N'Feb', 2018, CAST(40244.3431 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (377, 27, 34, N'Jan', 2018, CAST(56273.6919 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (378, 27, 34, N'Jul', 2018, CAST(69158.0542 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (379, 27, 34, N'Jun', 2018, CAST(58051.0396 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (380, 27, 34, N'Mar', 2018, CAST(66028.3088 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (381, 27, 34, N'May', 2018, CAST(58348.0408 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (382, 27, 34, N'Nov', 2018, CAST(53307.5092 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (383, 27, 34, N'Oct', 2018, CAST(73472.3752 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (384, 27, 34, N'Sep', 2018, CAST(48532.9192 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (385, 27, 35, N'Apr', 2018, CAST(26017.3476 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (386, 27, 35, N'Aug', 2018, CAST(19333.1942 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (387, 27, 35, N'Dec', 2018, CAST(37047.4955 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (388, 27, 35, N'Feb', 2018, CAST(17507.7073 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (389, 27, 35, N'Jan', 2018, CAST(11539.5605 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (390, 27, 35, N'Jul', 2018, CAST(24617.4360 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (391, 27, 35, N'Jun', 2018, CAST(28033.7752 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (392, 27, 35, N'Mar', 2018, CAST(25399.9903 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (393, 27, 35, N'May', 2018, CAST(6351.8199 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (394, 27, 35, N'Nov', 2018, CAST(23302.3707 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (395, 27, 35, N'Oct', 2018, CAST(38523.6036 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (396, 27, 35, N'Sep', 2018, CAST(26897.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (397, 33, 31, N'Apr', 2018, CAST(135168.0264 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (398, 33, 31, N'Aug', 2018, CAST(147823.2677 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (399, 33, 31, N'Dec', 2018, CAST(153237.4786 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (400, 33, 31, N'Feb', 2018, CAST(156478.1996 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (401, 33, 31, N'Jan', 2018, CAST(116594.2920 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (402, 33, 31, N'Jul', 2018, CAST(141175.1631 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (403, 33, 31, N'Jun', 2018, CAST(169836.4232 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (404, 33, 31, N'Mar', 2018, CAST(141482.0486 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (405, 33, 31, N'May', 2018, CAST(144431.5780 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (406, 33, 31, N'Nov', 2018, CAST(157511.5707 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (407, 33, 31, N'Oct', 2018, CAST(127268.3601 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (408, 33, 31, N'Sep', 2018, CAST(117688.0108 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (409, 33, 34, N'Apr', 2018, CAST(67788.8288 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (410, 33, 34, N'Aug', 2018, CAST(75246.6450 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (411, 33, 34, N'Dec', 2018, CAST(48149.0703 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (412, 33, 34, N'Feb', 2018, CAST(69265.3792 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (413, 33, 34, N'Jan', 2018, CAST(49960.6285 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (414, 33, 34, N'Jul', 2018, CAST(78671.3290 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (415, 33, 34, N'Jun', 2018, CAST(67231.0874 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (416, 33, 34, N'Mar', 2018, CAST(112918.4580 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (417, 33, 34, N'May', 2018, CAST(84054.6353 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (418, 33, 34, N'Nov', 2018, CAST(67559.5030 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (419, 33, 34, N'Oct', 2018, CAST(75898.3389 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (420, 33, 34, N'Sep', 2018, CAST(56469.1176 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (421, 33, 35, N'Apr', 2018, CAST(64745.0662 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (422, 33, 35, N'Aug', 2018, CAST(97809.3494 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (423, 33, 35, N'Dec', 2018, CAST(119714.7406 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (424, 33, 35, N'Feb', 2018, CAST(55948.7559 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (425, 33, 35, N'Jan', 2018, CAST(63199.6233 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (426, 33, 35, N'Jul', 2018, CAST(70809.7763 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (427, 33, 35, N'Jun', 2018, CAST(94413.2918 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (428, 33, 35, N'Mar', 2018, CAST(64335.9103 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (429, 33, 35, N'May', 2018, CAST(69945.1434 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (430, 33, 35, N'Nov', 2018, CAST(72784.2840 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (431, 33, 35, N'Oct', 2018, CAST(74720.4860 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (432, 33, 35, N'Sep', 2018, CAST(96569.3820 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (433, 35, 31, N'Apr', 2018, CAST(127391.0204 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (434, 35, 31, N'Aug', 2018, CAST(122471.9967 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (435, 35, 31, N'Dec', 2018, CAST(106635.2666 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (436, 35, 31, N'Feb', 2018, CAST(95559.5731 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (437, 35, 31, N'Jan', 2018, CAST(70357.9216 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (438, 35, 31, N'Jul', 2018, CAST(119790.7274 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (439, 35, 31, N'Jun', 2018, CAST(113311.7144 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (440, 35, 31, N'Mar', 2018, CAST(111656.1887 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (441, 35, 31, N'May', 2018, CAST(128711.2780 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (442, 35, 31, N'Nov', 2018, CAST(121005.1074 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (443, 35, 31, N'Oct', 2018, CAST(104277.9912 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (444, 35, 31, N'Sep', 2018, CAST(88996.3566 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (445, 35, 34, N'Apr', 2018, CAST(92469.9211 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (446, 35, 34, N'Aug', 2018, CAST(116589.6754 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (447, 35, 34, N'Dec', 2018, CAST(124046.8552 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (448, 35, 34, N'Feb', 2018, CAST(82840.8878 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (449, 35, 34, N'Jan', 2018, CAST(70323.2089 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (450, 35, 34, N'Jul', 2018, CAST(112143.1821 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (451, 35, 34, N'Jun', 2018, CAST(92170.4112 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (452, 35, 34, N'Mar', 2018, CAST(118610.7210 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (453, 35, 34, N'May', 2018, CAST(124040.8673 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (454, 35, 34, N'Nov', 2018, CAST(110769.3289 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (455, 35, 34, N'Oct', 2018, CAST(138592.1484 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (456, 35, 34, N'Sep', 2018, CAST(79068.2895 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (457, 35, 35, N'Apr', 2018, CAST(12023.6655 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (458, 35, 35, N'Aug', 2018, CAST(12878.9138 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (459, 35, 35, N'Dec', 2018, CAST(26859.4372 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (460, 35, 35, N'Feb', 2018, CAST(1462.6568 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (461, 35, 35, N'Jan', 2018, CAST(7610.2860 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (462, 35, 35, N'Jul', 2018, CAST(8661.1366 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (463, 35, 35, N'Jun', 2018, CAST(13020.5362 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (464, 35, 35, N'Mar', 2018, CAST(13636.2410 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (465, 35, 35, N'May', 2018, CAST(8313.4049 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (466, 35, 35, N'Nov', 2018, CAST(16604.9428 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (467, 35, 35, N'Oct', 2018, CAST(32165.7232 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (468, 35, 35, N'Sep', 2018, CAST(9102.6652 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (469, 37, 31, N'Apr', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (470, 37, 31, N'Aug', 2018, CAST(75933.9120 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (471, 37, 31, N'Dec', 2018, CAST(83840.8388 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (472, 37, 31, N'Feb', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (473, 37, 31, N'Jan', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (474, 37, 31, N'Jul', 2018, CAST(84339.7996 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (475, 37, 31, N'Jun', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (476, 37, 31, N'Mar', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (477, 37, 31, N'May', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (478, 37, 31, N'Nov', 2018, CAST(72117.6637 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (479, 37, 31, N'Oct', 2018, CAST(75034.9714 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (480, 37, 31, N'Sep', 2018, CAST(66925.6682 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (481, 37, 34, N'Apr', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (482, 37, 34, N'Aug', 2018, CAST(75991.6902 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (483, 37, 34, N'Dec', 2018, CAST(85729.7801 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (484, 37, 34, N'Feb', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (485, 37, 34, N'Jan', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (486, 37, 34, N'Jul', 2018, CAST(70010.0993 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (487, 37, 34, N'Jun', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (488, 37, 34, N'Mar', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (489, 37, 34, N'May', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (490, 37, 34, N'Nov', 2018, CAST(91653.3175 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (491, 37, 34, N'Oct', 2018, CAST(69279.0351 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (492, 37, 34, N'Sep', 2018, CAST(79071.5817 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (493, 37, 35, N'Apr', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (494, 37, 35, N'Aug', 2018, CAST(38365.9519 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (495, 37, 35, N'Dec', 2018, CAST(37238.8380 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (496, 37, 35, N'Feb', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (497, 37, 35, N'Jan', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (498, 37, 35, N'Jul', 2018, CAST(31799.1155 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (499, 37, 35, N'Jun', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (500, 37, 35, N'Mar', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (501, 37, 35, N'May', 2018, CAST(0.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (502, 37, 35, N'Nov', 2018, CAST(43190.5369 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (503, 37, 35, N'Oct', 2018, CAST(34311.3972 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (504, 37, 35, N'Sep', 2018, CAST(35031.4811 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (505, 13, 31, N'Jan', 2019, CAST(80874.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (506, 13, 31, N'Feb', 2019, CAST(71301.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (507, 13, 31, N'Mar', 2019, CAST(106061.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (508, 13, 31, N'Apr', 2019, CAST(55125.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (509, 13, 31, N'May', 2019, CAST(119101.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (510, 13, 31, N'Jun', 2019, CAST(66236.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (511, 13, 31, N'Jul', 2019, CAST(102277.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (512, 13, 31, N'Aug', 2019, CAST(131403.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (513, 13, 31, N'Sep', 2019, CAST(86322.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (514, 13, 31, N'Oct', 2019, CAST(103162.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (515, 13, 31, N'Nov', 2019, CAST(93732.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (516, 13, 31, N'Dec', 2019, CAST(112101.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (517, 13, 34, N'Jan', 2019, CAST(97671.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (518, 13, 34, N'Feb', 2019, CAST(91671.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (519, 13, 34, N'Mar', 2019, CAST(99974.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (520, 13, 34, N'Apr', 2019, CAST(118616.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (521, 13, 34, N'May', 2019, CAST(138147.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (522, 13, 34, N'Jun', 2019, CAST(145123.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (523, 13, 34, N'Jul', 2019, CAST(70729.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (524, 13, 34, N'Aug', 2019, CAST(153933.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (525, 13, 34, N'Sep', 2019, CAST(60194.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (526, 13, 34, N'Oct', 2019, CAST(162291.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (527, 13, 34, N'Nov', 2019, CAST(133671.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (528, 13, 34, N'Dec', 2019, CAST(138147.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (529, 13, 35, N'Jan', 2019, CAST(97875.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (530, 13, 35, N'Feb', 2019, CAST(48348.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (531, 13, 35, N'Mar', 2019, CAST(61179.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (532, 13, 35, N'Apr', 2019, CAST(49517.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (533, 13, 35, N'May', 2019, CAST(63771.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (534, 13, 35, N'Jun', 2019, CAST(76080.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (535, 13, 35, N'Jul', 2019, CAST(19739.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (536, 13, 35, N'Aug', 2019, CAST(65173.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (537, 13, 35, N'Sep', 2019, CAST(47133.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (538, 13, 35, N'Oct', 2019, CAST(46045.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (539, 13, 35, N'Nov', 2019, CAST(91688.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (540, 13, 35, N'Dec', 2019, CAST(63771.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (541, 13, 36, N'Jan', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (542, 13, 36, N'Feb', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (543, 13, 36, N'Mar', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (544, 13, 36, N'Apr', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (545, 13, 36, N'May', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (546, 13, 36, N'Jun', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (547, 13, 36, N'Jul', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (548, 13, 36, N'Aug', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (549, 13, 36, N'Sep', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (550, 13, 36, N'Oct', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (551, 13, 36, N'Nov', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (552, 13, 36, N'Dec', 2019, CAST(8342.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (553, 14, 31, N'Jan', 2019, CAST(95748.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (554, 14, 31, N'Feb', 2019, CAST(192423.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (555, 14, 31, N'Mar', 2019, CAST(43063.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (556, 14, 31, N'Apr', 2019, CAST(90251.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (557, 14, 31, N'May', 2019, CAST(93869.0400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (558, 14, 31, N'Jun', 2019, CAST(166798.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (559, 14, 31, N'Jul', 2019, CAST(143760.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (560, 14, 31, N'Aug', 2019, CAST(223512.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (561, 14, 31, N'Sep', 2019, CAST(187158.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (562, 14, 31, N'Oct', 2019, CAST(175382.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (563, 14, 31, N'Nov', 2019, CAST(175382.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (564, 14, 31, N'Dec', 2019, CAST(175382.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (565, 14, 34, N'Jan', 2019, CAST(74745.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (566, 14, 34, N'Feb', 2019, CAST(80808.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (567, 14, 34, N'Mar', 2019, CAST(106119.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (568, 14, 34, N'Apr', 2019, CAST(135024.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (569, 14, 34, N'May', 2019, CAST(172688.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (570, 14, 34, N'Jun', 2019, CAST(127253.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (571, 14, 34, N'Jul', 2019, CAST(89097.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (572, 14, 34, N'Aug', 2019, CAST(151132.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (573, 14, 34, N'Sep', 2019, CAST(94150.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (574, 14, 34, N'Oct', 2019, CAST(183313.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (575, 14, 34, N'Nov', 2019, CAST(159002.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (576, 14, 34, N'Dec', 2019, CAST(172688.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (577, 14, 35, N'Jan', 2019, CAST(8246.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (578, 14, 35, N'Feb', 2019, CAST(-2090.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (579, 14, 35, N'Mar', 2019, CAST(10931.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (580, 14, 35, N'Apr', 2019, CAST(17308.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (581, 14, 35, N'May', 2019, CAST(17393.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (582, 14, 35, N'Jun', 2019, CAST(8527.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (583, 14, 35, N'Jul', 2019, CAST(8307.0400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (584, 14, 35, N'Aug', 2019, CAST(17061.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (585, 14, 35, N'Sep', 2019, CAST(17061.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (586, 14, 35, N'Oct', 2019, CAST(17061.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (587, 14, 35, N'Nov', 2019, CAST(12333.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (588, 14, 35, N'Dec', 2019, CAST(17393.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (589, 14, 36, N'Jan', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (590, 14, 36, N'Feb', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (591, 14, 36, N'Mar', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (592, 14, 36, N'Apr', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (593, 14, 36, N'May', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (594, 14, 36, N'Jun', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (595, 14, 36, N'Jul', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (596, 14, 36, N'Aug', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (597, 14, 36, N'Sep', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (598, 14, 36, N'Oct', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (599, 14, 36, N'Nov', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (600, 14, 36, N'Dec', 2019, CAST(4181.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (601, 15, 31, N'Jan', 2019, CAST(100468.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (602, 15, 31, N'Feb', 2019, CAST(99863.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (603, 15, 31, N'Mar', 2019, CAST(97682.4600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (604, 15, 31, N'Apr', 2019, CAST(207084.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (605, 15, 31, N'May', 2019, CAST(120734.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (606, 15, 31, N'Jun', 2019, CAST(158574.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (607, 15, 31, N'Jul', 2019, CAST(142886.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (608, 15, 31, N'Aug', 2019, CAST(136229.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (609, 15, 31, N'Sep', 2019, CAST(128779.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (610, 15, 31, N'Oct', 2019, CAST(151037.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (611, 15, 31, N'Nov', 2019, CAST(154794.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (612, 15, 31, N'Dec', 2019, CAST(113734.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (613, 15, 34, N'Jan', 2019, CAST(18378.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (614, 15, 34, N'Feb', 2019, CAST(14286.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (615, 15, 34, N'Mar', 2019, CAST(22913.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (616, 15, 34, N'Apr', 2019, CAST(32383.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (617, 15, 34, N'May', 2019, CAST(45326.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (618, 15, 34, N'Jun', 2019, CAST(1864.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (619, 15, 34, N'Jul', 2019, CAST(43190.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (620, 15, 34, N'Aug', 2019, CAST(37440.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (621, 15, 34, N'Sep', 2019, CAST(47567.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (622, 15, 34, N'Oct', 2019, CAST(40290.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (623, 15, 34, N'Nov', 2019, CAST(23158.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (624, 15, 34, N'Dec', 2019, CAST(45326.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (625, 15, 35, N'Jan', 2019, CAST(2750.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (626, 15, 35, N'Feb', 2019, CAST(5678.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (627, 15, 35, N'Mar', 2019, CAST(12701.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (628, 15, 35, N'Apr', 2019, CAST(7150.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (629, 15, 35, N'May', 2019, CAST(6035.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (630, 15, 35, N'Jun', 2019, CAST(7658.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (631, 15, 35, N'Jul', 2019, CAST(18217.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (632, 15, 35, N'Aug', 2019, CAST(18217.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (633, 15, 35, N'Sep', 2019, CAST(18217.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (634, 15, 35, N'Oct', 2019, CAST(15653.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (635, 15, 35, N'Nov', 2019, CAST(11692.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (636, 15, 35, N'Dec', 2019, CAST(6035.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (637, 15, 36, N'Jan', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (638, 15, 36, N'Feb', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (639, 15, 36, N'Mar', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (640, 15, 36, N'Apr', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (641, 15, 36, N'May', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (642, 15, 36, N'Jun', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (643, 15, 36, N'Jul', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (644, 15, 36, N'Aug', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (645, 15, 36, N'Sep', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (646, 15, 36, N'Oct', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (647, 15, 36, N'Nov', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (648, 15, 36, N'Dec', 2019, CAST(6667.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (649, 16, 31, N'Jan', 2019, CAST(76755.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (650, 16, 31, N'Feb', 2019, CAST(103686.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (651, 16, 31, N'Mar', 2019, CAST(116330.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (652, 16, 31, N'Apr', 2019, CAST(113695.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (653, 16, 31, N'May', 2019, CAST(162275.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (654, 16, 31, N'Jun', 2019, CAST(119618.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (655, 16, 31, N'Jul', 2019, CAST(187465.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (656, 16, 31, N'Aug', 2019, CAST(178621.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (657, 16, 31, N'Sep', 2019, CAST(172627.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (658, 16, 31, N'Oct', 2019, CAST(105126.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (659, 16, 31, N'Nov', 2019, CAST(149832.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (660, 16, 31, N'Dec', 2019, CAST(155275.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (661, 16, 34, N'Jan', 2019, CAST(55090.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (662, 16, 34, N'Feb', 2019, CAST(44875.3900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (663, 16, 34, N'Mar', 2019, CAST(80905.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (664, 16, 34, N'Apr', 2019, CAST(90589.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (665, 16, 34, N'May', 2019, CAST(182275.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (666, 16, 34, N'Jun', 2019, CAST(152797.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (667, 16, 34, N'Jul', 2019, CAST(94478.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (668, 16, 34, N'Aug', 2019, CAST(125903.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (669, 16, 34, N'Sep', 2019, CAST(62237.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (670, 16, 34, N'Oct', 2019, CAST(82733.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (671, 16, 34, N'Nov', 2019, CAST(152946.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (672, 16, 34, N'Dec', 2019, CAST(182275.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (673, 16, 35, N'Jan', 2019, CAST(15122.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (674, 16, 35, N'Feb', 2019, CAST(5009.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (675, 16, 35, N'Mar', 2019, CAST(6747.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (676, 16, 35, N'Apr', 2019, CAST(13415.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (677, 16, 35, N'May', 2019, CAST(13415.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (678, 16, 35, N'Jun', 2019, CAST(13415.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (679, 16, 35, N'Jul', 2019, CAST(1318.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (680, 16, 35, N'Aug', 2019, CAST(21635.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (681, 16, 35, N'Sep', 2019, CAST(5386.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (682, 16, 35, N'Oct', 2019, CAST(18068.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (683, 16, 35, N'Nov', 2019, CAST(18068.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (684, 16, 35, N'Dec', 2019, CAST(18068.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (685, 16, 36, N'Jan', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (686, 16, 36, N'Feb', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (687, 16, 36, N'Mar', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (688, 16, 36, N'Apr', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (689, 16, 36, N'May', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (690, 16, 36, N'Jun', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (691, 16, 36, N'Jul', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (692, 16, 36, N'Aug', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (693, 16, 36, N'Sep', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (694, 16, 36, N'Oct', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (695, 16, 36, N'Nov', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (696, 16, 36, N'Dec', 2019, CAST(4192.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (697, 33, 31, N'Jan', 2019, CAST(73796.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (698, 33, 31, N'Feb', 2019, CAST(60953.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (699, 33, 31, N'Mar', 2019, CAST(135321.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (700, 33, 31, N'Apr', 2019, CAST(65857.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (701, 33, 31, N'May', 2019, CAST(113410.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (702, 33, 31, N'Jun', 2019, CAST(93127.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (703, 33, 31, N'Jul', 2019, CAST(64251.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (704, 33, 31, N'Aug', 2019, CAST(126390.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (705, 33, 31, N'Sep', 2019, CAST(104774.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (706, 33, 31, N'Oct', 2019, CAST(130387.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (707, 33, 31, N'Nov', 2019, CAST(91426.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (708, 33, 31, N'Dec', 2019, CAST(106410.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (709, 33, 34, N'Jan', 2019, CAST(25069.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (710, 33, 34, N'Feb', 2019, CAST(57599.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (711, 33, 34, N'Mar', 2019, CAST(85522.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (712, 33, 34, N'Apr', 2019, CAST(69447.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (713, 33, 34, N'May', 2019, CAST(84880.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (714, 33, 34, N'Jun', 2019, CAST(112981.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (715, 33, 34, N'Jul', 2019, CAST(125352.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (716, 33, 34, N'Aug', 2019, CAST(100555.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (717, 33, 34, N'Sep', 2019, CAST(81979.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (718, 33, 34, N'Oct', 2019, CAST(85183.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (719, 33, 34, N'Nov', 2019, CAST(76748.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (720, 33, 34, N'Dec', 2019, CAST(84880.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (721, 33, 35, N'Jan', 2019, CAST(46012.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (722, 33, 35, N'Feb', 2019, CAST(78819.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (723, 33, 35, N'Mar', 2019, CAST(46673.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (724, 33, 35, N'Apr', 2019, CAST(49124.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (725, 33, 35, N'May', 2019, CAST(106838.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (726, 33, 35, N'Jun', 2019, CAST(39249.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (727, 33, 35, N'Jul', 2019, CAST(12457.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (728, 33, 35, N'Aug', 2019, CAST(63354.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (729, 33, 35, N'Sep', 2019, CAST(80230.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (730, 33, 35, N'Oct', 2019, CAST(84235.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (731, 33, 35, N'Nov', 2019, CAST(75551.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (732, 33, 35, N'Dec', 2019, CAST(106838.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (733, 33, 36, N'Jan', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (734, 33, 36, N'Feb', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (735, 33, 36, N'Mar', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (736, 33, 36, N'Apr', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (737, 33, 36, N'May', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (738, 33, 36, N'Jun', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (739, 33, 36, N'Jul', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (740, 33, 36, N'Aug', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (741, 33, 36, N'Sep', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (742, 33, 36, N'Oct', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (743, 33, 36, N'Nov', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (744, 33, 36, N'Dec', 2019, CAST(10020.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (745, 17, 31, N'Jan', 2019, CAST(82182.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (746, 17, 31, N'Feb', 2019, CAST(103803.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (747, 17, 31, N'Mar', 2019, CAST(118073.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (748, 17, 31, N'Apr', 2019, CAST(131409.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (749, 17, 31, N'May', 2019, CAST(101586.7300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (750, 17, 31, N'Jun', 2019, CAST(159752.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (751, 17, 31, N'Jul', 2019, CAST(138824.3900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (752, 17, 31, N'Aug', 2019, CAST(103499.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (753, 17, 31, N'Sep', 2019, CAST(156605.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (754, 17, 31, N'Oct', 2019, CAST(172802.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (755, 17, 31, N'Nov', 2019, CAST(116878.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (756, 17, 31, N'Dec', 2019, CAST(94586.7300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (757, 17, 34, N'Jan', 2019, CAST(81647.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (758, 17, 34, N'Feb', 2019, CAST(66163.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (759, 17, 34, N'Mar', 2019, CAST(77624.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (760, 17, 34, N'Apr', 2019, CAST(88031.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (761, 17, 34, N'May', 2019, CAST(71208.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (762, 17, 34, N'Jun', 2019, CAST(100606.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (763, 17, 34, N'Jul', 2019, CAST(112416.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (764, 17, 34, N'Aug', 2019, CAST(85211.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (765, 17, 34, N'Sep', 2019, CAST(72687.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (766, 17, 34, N'Oct', 2019, CAST(69567.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (767, 17, 34, N'Nov', 2019, CAST(70746.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (768, 17, 34, N'Dec', 2019, CAST(71208.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (769, 17, 35, N'Jan', 2019, CAST(23473.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (770, 17, 35, N'Feb', 2019, CAST(23473.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (771, 17, 35, N'Mar', 2019, CAST(23473.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (772, 17, 35, N'Apr', 2019, CAST(17244.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (773, 17, 35, N'May', 2019, CAST(28211.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (774, 17, 35, N'Jun', 2019, CAST(28211.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (775, 17, 35, N'Jul', 2019, CAST(28211.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (776, 17, 35, N'Aug', 2019, CAST(-7298.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (777, 17, 35, N'Sep', 2019, CAST(12812.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (778, 17, 35, N'Oct', 2019, CAST(32772.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (779, 17, 35, N'Nov', 2019, CAST(27287.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (780, 17, 35, N'Dec', 2019, CAST(16625.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (781, 17, 36, N'Jan', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (782, 17, 36, N'Feb', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (783, 17, 36, N'Mar', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (784, 17, 36, N'Apr', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (785, 17, 36, N'May', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (786, 17, 36, N'Jun', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (787, 17, 36, N'Jul', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (788, 17, 36, N'Aug', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (789, 17, 36, N'Sep', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (790, 17, 36, N'Oct', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (791, 17, 36, N'Nov', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (792, 17, 36, N'Dec', 2019, CAST(4246.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (793, 35, 31, N'Jan', 2019, CAST(58370.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (794, 35, 31, N'Feb', 2019, CAST(122658.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (795, 35, 31, N'Mar', 2019, CAST(161775.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (796, 35, 31, N'Apr', 2019, CAST(82068.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (797, 35, 31, N'May', 2019, CAST(50993.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (798, 35, 31, N'Jun', 2019, CAST(132017.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (799, 35, 31, N'Jul', 2019, CAST(130041.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (800, 35, 31, N'Aug', 2019, CAST(126248.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (801, 35, 31, N'Sep', 2019, CAST(162928.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (802, 35, 31, N'Oct', 2019, CAST(155928.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (803, 35, 31, N'Nov', 2019, CAST(155928.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (804, 35, 31, N'Dec', 2019, CAST(43993.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (805, 35, 34, N'Jan', 2019, CAST(79463.3900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (806, 35, 34, N'Feb', 2019, CAST(108978.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (807, 35, 34, N'Mar', 2019, CAST(100648.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (808, 35, 34, N'Apr', 2019, CAST(95568.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (809, 35, 34, N'May', 2019, CAST(122796.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (810, 35, 34, N'Jun', 2019, CAST(119136.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (811, 35, 34, N'Jul', 2019, CAST(162560.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (812, 35, 34, N'Aug', 2019, CAST(120332.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (813, 35, 34, N'Sep', 2019, CAST(60713.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (814, 35, 34, N'Oct', 2019, CAST(166268.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (815, 35, 34, N'Nov', 2019, CAST(121950.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (816, 35, 34, N'Dec', 2019, CAST(122796.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (817, 35, 35, N'Jan', 2019, CAST(602.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (818, 35, 35, N'Feb', 2019, CAST(8441.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (819, 35, 35, N'Mar', 2019, CAST(9219.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (820, 35, 35, N'Apr', 2019, CAST(4967.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (821, 35, 35, N'May', 2019, CAST(9600.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (822, 35, 35, N'Jun', 2019, CAST(582.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (823, 35, 35, N'Jul', 2019, CAST(7773.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (824, 35, 35, N'Aug', 2019, CAST(7773.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (825, 35, 35, N'Sep', 2019, CAST(7773.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (826, 35, 35, N'Oct', 2019, CAST(2685.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (827, 35, 35, N'Nov', 2019, CAST(4771.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (828, 35, 35, N'Dec', 2019, CAST(9600.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (829, 35, 36, N'Jan', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (830, 35, 36, N'Feb', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (831, 35, 36, N'Mar', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (832, 35, 36, N'Apr', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (833, 35, 36, N'May', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (834, 35, 36, N'Jun', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (835, 35, 36, N'Jul', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (836, 35, 36, N'Aug', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (837, 35, 36, N'Sep', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (838, 35, 36, N'Oct', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (839, 35, 36, N'Nov', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (840, 35, 36, N'Dec', 2019, CAST(6320.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (841, 18, 31, N'Jan', 2019, CAST(62509.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (842, 18, 31, N'Feb', 2019, CAST(133725.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (843, 18, 31, N'Mar', 2019, CAST(61782.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (844, 18, 31, N'Apr', 2019, CAST(112504.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (845, 18, 31, N'May', 2019, CAST(115350.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (846, 18, 31, N'Jun', 2019, CAST(54234.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (847, 18, 31, N'Jul', 2019, CAST(101164.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (848, 18, 31, N'Aug', 2019, CAST(74420.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (849, 18, 31, N'Sep', 2019, CAST(92816.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (850, 18, 31, N'Oct', 2019, CAST(72980.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (851, 18, 31, N'Nov', 2019, CAST(129634.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (852, 18, 31, N'Dec', 2019, CAST(108350.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (853, 18, 34, N'Jan', 2019, CAST(82973.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (854, 18, 34, N'Feb', 2019, CAST(43099.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (855, 18, 34, N'Mar', 2019, CAST(44532.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (856, 18, 34, N'Apr', 2019, CAST(31890.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (857, 18, 34, N'May', 2019, CAST(20756.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (858, 18, 34, N'Jun', 2019, CAST(29929.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (859, 18, 34, N'Jul', 2019, CAST(72289.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (860, 18, 34, N'Aug', 2019, CAST(51868.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (861, 18, 34, N'Sep', 2019, CAST(59086.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (862, 18, 34, N'Oct', 2019, CAST(75713.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (863, 18, 34, N'Nov', 2019, CAST(53816.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (864, 18, 34, N'Dec', 2019, CAST(20756.1700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (865, 18, 35, N'Jan', 2019, CAST(35220.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (866, 18, 35, N'Feb', 2019, CAST(49308.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (867, 18, 35, N'Mar', 2019, CAST(49925.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (868, 18, 35, N'Apr', 2019, CAST(23125.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (869, 18, 35, N'May', 2019, CAST(55576.5900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (870, 18, 35, N'Jun', 2019, CAST(66390.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (871, 18, 35, N'Jul', 2019, CAST(77716.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (872, 18, 35, N'Aug', 2019, CAST(77716.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (873, 18, 35, N'Sep', 2019, CAST(77716.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (874, 18, 35, N'Oct', 2019, CAST(70232.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (875, 18, 35, N'Nov', 2019, CAST(59065.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (876, 18, 35, N'Dec', 2019, CAST(55576.5900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (877, 18, 36, N'Jan', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (878, 18, 36, N'Feb', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (879, 18, 36, N'Mar', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (880, 18, 36, N'Apr', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (881, 18, 36, N'May', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (882, 18, 36, N'Jun', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (883, 18, 36, N'Jul', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (884, 18, 36, N'Aug', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (885, 18, 36, N'Sep', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (886, 18, 36, N'Oct', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (887, 18, 36, N'Nov', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (888, 18, 36, N'Dec', 2019, CAST(8373.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (889, 37, 31, N'Jan', 2019, CAST(35331.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (890, 37, 31, N'Feb', 2019, CAST(68299.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (891, 37, 31, N'Mar', 2019, CAST(73508.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (892, 37, 31, N'Apr', 2019, CAST(110542.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (893, 37, 31, N'May', 2019, CAST(53870.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (894, 37, 31, N'Jun', 2019, CAST(91152.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (895, 37, 31, N'Jul', 2019, CAST(45957.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (896, 37, 31, N'Aug', 2019, CAST(78250.0400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (897, 37, 31, N'Sep', 2019, CAST(82998.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (898, 37, 31, N'Oct', 2019, CAST(51940.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (899, 37, 31, N'Nov', 2019, CAST(55865.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (900, 37, 31, N'Dec', 2019, CAST(46870.2300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (901, 37, 34, N'Jan', 2019, CAST(47388.3800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (902, 37, 34, N'Feb', 2019, CAST(88214.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (903, 37, 34, N'Mar', 2019, CAST(88214.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (904, 37, 34, N'Apr', 2019, CAST(88214.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (905, 37, 34, N'May', 2019, CAST(84749.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (906, 37, 34, N'Jun', 2019, CAST(86954.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (907, 37, 34, N'Jul', 2019, CAST(118923.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (908, 37, 34, N'Aug', 2019, CAST(76363.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (909, 37, 34, N'Sep', 2019, CAST(79969.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (910, 37, 34, N'Oct', 2019, CAST(96747.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (911, 37, 34, N'Nov', 2019, CAST(58026.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (912, 37, 34, N'Dec', 2019, CAST(84749.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (913, 37, 35, N'Jan', 2019, CAST(33363.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (914, 37, 35, N'Feb', 2019, CAST(17803.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (915, 37, 35, N'Mar', 2019, CAST(58946.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (916, 37, 35, N'Apr', 2019, CAST(13324.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (917, 37, 35, N'May', 2019, CAST(24418.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (918, 37, 35, N'Jun', 2019, CAST(33450.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (919, 37, 35, N'Jul', 2019, CAST(64051.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (920, 37, 35, N'Aug', 2019, CAST(30756.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (921, 37, 35, N'Sep', 2019, CAST(34821.7500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (922, 37, 35, N'Oct', 2019, CAST(58260.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (923, 37, 35, N'Nov', 2019, CAST(52030.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (924, 37, 35, N'Dec', 2019, CAST(24418.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (925, 37, 36, N'Jan', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (926, 37, 36, N'Feb', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (927, 37, 36, N'Mar', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (928, 37, 36, N'Apr', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (929, 37, 36, N'May', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (930, 37, 36, N'Jun', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (931, 37, 36, N'Jul', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (932, 37, 36, N'Aug', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (933, 37, 36, N'Sep', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (934, 37, 36, N'Oct', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (935, 37, 36, N'Nov', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (936, 37, 36, N'Dec', 2019, CAST(4311.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (937, 20, 31, N'Jan', 2019, CAST(63224.3000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (938, 20, 31, N'Feb', 2019, CAST(63224.3000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (939, 20, 31, N'Mar', 2019, CAST(63224.3000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (940, 20, 31, N'Apr', 2019, CAST(37104.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (941, 20, 31, N'May', 2019, CAST(37462.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (942, 20, 31, N'Jun', 2019, CAST(29642.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (943, 20, 31, N'Jul', 2019, CAST(50241.0600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (944, 20, 31, N'Aug', 2019, CAST(69666.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (945, 20, 31, N'Sep', 2019, CAST(26651.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (946, 20, 31, N'Oct', 2019, CAST(53697.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (947, 20, 31, N'Nov', 2019, CAST(61732.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (948, 20, 31, N'Dec', 2019, CAST(30462.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (949, 20, 34, N'Jan', 2019, CAST(102579.5900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (950, 20, 34, N'Feb', 2019, CAST(144819.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (951, 20, 34, N'Mar', 2019, CAST(69695.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (952, 20, 34, N'Apr', 2019, CAST(33492.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (953, 20, 34, N'May', 2019, CAST(150073.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (954, 20, 34, N'Jun', 2019, CAST(84818.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (955, 20, 34, N'Jul', 2019, CAST(166868.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (956, 20, 34, N'Aug', 2019, CAST(88411.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (957, 20, 34, N'Sep', 2019, CAST(113175.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (958, 20, 34, N'Oct', 2019, CAST(100944.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (959, 20, 34, N'Nov', 2019, CAST(127728.3500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (960, 20, 34, N'Dec', 2019, CAST(150073.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (961, 20, 35, N'Jan', 2019, CAST(20266.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (962, 20, 35, N'Feb', 2019, CAST(68564.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (963, 20, 35, N'Mar', 2019, CAST(69686.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (964, 20, 35, N'Apr', 2019, CAST(11235.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (965, 20, 35, N'May', 2019, CAST(72405.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (966, 20, 35, N'Jun', 2019, CAST(59295.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (967, 20, 35, N'Jul', 2019, CAST(49742.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (968, 20, 35, N'Aug', 2019, CAST(100621.1800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (969, 20, 35, N'Sep', 2019, CAST(88726.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (970, 20, 35, N'Oct', 2019, CAST(67618.1800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (971, 20, 35, N'Nov', 2019, CAST(112279.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (972, 20, 35, N'Dec', 2019, CAST(72405.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (973, 20, 36, N'Jan', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (974, 20, 36, N'Feb', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (975, 20, 36, N'Mar', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (976, 20, 36, N'Apr', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (977, 20, 36, N'May', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (978, 20, 36, N'Jun', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (979, 20, 36, N'Jul', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (980, 20, 36, N'Aug', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (981, 20, 36, N'Sep', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (982, 20, 36, N'Oct', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (983, 20, 36, N'Nov', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (984, 20, 36, N'Dec', 2019, CAST(4207.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (985, 21, 31, N'Jan', 2019, CAST(140564.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (986, 21, 31, N'Feb', 2019, CAST(92531.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (987, 21, 31, N'Mar', 2019, CAST(144452.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (988, 21, 31, N'Apr', 2019, CAST(118112.1400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (989, 21, 31, N'May', 2019, CAST(207570.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (990, 21, 31, N'Jun', 2019, CAST(160026.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (991, 21, 31, N'Jul', 2019, CAST(153127.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (992, 21, 31, N'Aug', 2019, CAST(202686.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (993, 21, 31, N'Sep', 2019, CAST(224010.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (994, 21, 31, N'Oct', 2019, CAST(152041.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (995, 21, 31, N'Nov', 2019, CAST(160777.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (996, 21, 31, N'Dec', 2019, CAST(200570.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (997, 21, 34, N'Jan', 2019, CAST(52887.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (998, 21, 34, N'Feb', 2019, CAST(42507.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (999, 21, 34, N'Mar', 2019, CAST(69379.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1000, 21, 34, N'Apr', 2019, CAST(66586.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1001, 21, 34, N'May', 2019, CAST(55963.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1002, 21, 34, N'Jun', 2019, CAST(85623.1800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1003, 21, 34, N'Jul', 2019, CAST(54749.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1004, 21, 34, N'Aug', 2019, CAST(73874.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1005, 21, 34, N'Sep', 2019, CAST(48872.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1006, 21, 34, N'Oct', 2019, CAST(68089.7200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1007, 21, 34, N'Nov', 2019, CAST(99224.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1008, 21, 34, N'Dec', 2019, CAST(55963.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1009, 21, 35, N'Jan', 2019, CAST(46051.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1010, 21, 35, N'Feb', 2019, CAST(62403.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1011, 21, 35, N'Mar', 2019, CAST(44167.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1012, 21, 35, N'Apr', 2019, CAST(46888.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1013, 21, 35, N'May', 2019, CAST(53863.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1014, 21, 35, N'Jun', 2019, CAST(34703.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1015, 21, 35, N'Jul', 2019, CAST(40621.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1016, 21, 35, N'Aug', 2019, CAST(30230.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1017, 21, 35, N'Sep', 2019, CAST(30159.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1018, 21, 35, N'Oct', 2019, CAST(36738.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1019, 21, 35, N'Nov', 2019, CAST(57674.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1020, 21, 35, N'Dec', 2019, CAST(53863.0100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1021, 21, 36, N'Jan', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1022, 21, 36, N'Feb', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1023, 21, 36, N'Mar', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1024, 21, 36, N'Apr', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1025, 21, 36, N'May', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1026, 21, 36, N'Jun', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1027, 21, 36, N'Jul', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1028, 21, 36, N'Aug', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1029, 21, 36, N'Sep', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1030, 21, 36, N'Oct', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1031, 21, 36, N'Nov', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1032, 21, 36, N'Dec', 2019, CAST(7565.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1033, 8, 31, N'Jan', 2019, CAST(72123.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1034, 8, 31, N'Feb', 2019, CAST(40788.3000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1035, 8, 31, N'Mar', 2019, CAST(120940.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1036, 8, 31, N'Apr', 2019, CAST(106831.7300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1037, 8, 31, N'May', 2019, CAST(76192.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1038, 8, 31, N'Jun', 2019, CAST(90107.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1039, 8, 31, N'Jul', 2019, CAST(90107.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1040, 8, 31, N'Aug', 2019, CAST(90107.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1041, 8, 31, N'Sep', 2019, CAST(31881.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1042, 8, 31, N'Oct', 2019, CAST(13970.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1043, 8, 31, N'Nov', 2019, CAST(43039.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1044, 8, 31, N'Dec', 2019, CAST(69192.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1045, 8, 34, N'Jan', 2019, CAST(27657.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1046, 8, 34, N'Feb', 2019, CAST(76655.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1047, 8, 34, N'Mar', 2019, CAST(81754.9100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1048, 8, 34, N'Apr', 2019, CAST(81457.7300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1049, 8, 34, N'May', 2019, CAST(71563.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1050, 8, 34, N'Jun', 2019, CAST(60174.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1051, 8, 34, N'Jul', 2019, CAST(51750.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1052, 8, 34, N'Aug', 2019, CAST(83744.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1053, 8, 34, N'Sep', 2019, CAST(42018.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1054, 8, 34, N'Oct', 2019, CAST(40179.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1055, 8, 34, N'Nov', 2019, CAST(55237.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1056, 8, 34, N'Dec', 2019, CAST(71563.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1057, 8, 35, N'Jan', 2019, CAST(36718.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1058, 8, 35, N'Feb', 2019, CAST(33743.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1059, 8, 35, N'Mar', 2019, CAST(33743.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1060, 8, 35, N'Apr', 2019, CAST(33743.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1061, 8, 35, N'May', 2019, CAST(52434.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1062, 8, 35, N'Jun', 2019, CAST(14337.1000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1063, 8, 35, N'Jul', 2019, CAST(29451.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1064, 8, 35, N'Aug', 2019, CAST(61620.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1065, 8, 35, N'Sep', 2019, CAST(43723.5900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1066, 8, 35, N'Oct', 2019, CAST(57694.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1067, 8, 35, N'Nov', 2019, CAST(57694.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1068, 8, 35, N'Dec', 2019, CAST(57694.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1069, 8, 36, N'Jan', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1070, 8, 36, N'Feb', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1071, 8, 36, N'Mar', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1072, 8, 36, N'Apr', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1073, 8, 36, N'May', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1074, 8, 36, N'Jun', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1075, 8, 36, N'Jul', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1076, 8, 36, N'Aug', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1077, 8, 36, N'Sep', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1078, 8, 36, N'Oct', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1079, 8, 36, N'Nov', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1080, 8, 36, N'Dec', 2019, CAST(6679.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1081, 25, 31, N'Jan', 2019, CAST(54299.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1082, 25, 31, N'Feb', 2019, CAST(25112.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1083, 25, 31, N'Mar', 2019, CAST(56955.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1084, 25, 31, N'Apr', 2019, CAST(34251.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1085, 25, 31, N'May', 2019, CAST(46961.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1086, 25, 31, N'Jun', 2019, CAST(68959.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1087, 25, 31, N'Jul', 2019, CAST(38396.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1088, 25, 31, N'Aug', 2019, CAST(66500.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1089, 25, 31, N'Sep', 2019, CAST(89522.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1090, 25, 31, N'Oct', 2019, CAST(71282.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1091, 25, 31, N'Nov', 2019, CAST(38819.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1092, 25, 31, N'Dec', 2019, CAST(39961.4900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1093, 25, 34, N'Jan', 2019, CAST(17131.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1094, 25, 34, N'Feb', 2019, CAST(50273.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1095, 25, 34, N'Mar', 2019, CAST(46272.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1096, 25, 34, N'Apr', 2019, CAST(48130.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1097, 25, 34, N'May', 2019, CAST(72951.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1098, 25, 34, N'Jun', 2019, CAST(72951.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1099, 25, 34, N'Jul', 2019, CAST(72951.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1100, 25, 34, N'Aug', 2019, CAST(81205.0800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1101, 25, 34, N'Sep', 2019, CAST(85579.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1102, 25, 34, N'Oct', 2019, CAST(85579.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1103, 25, 34, N'Nov', 2019, CAST(85579.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1104, 25, 34, N'Dec', 2019, CAST(44653.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1105, 25, 35, N'Jan', 2019, CAST(24616.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1106, 25, 35, N'Feb', 2019, CAST(80462.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1107, 25, 35, N'Mar', 2019, CAST(56112.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1108, 25, 35, N'Apr', 2019, CAST(36897.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1109, 25, 35, N'May', 2019, CAST(63921.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1110, 25, 35, N'Jun', 2019, CAST(46744.0400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1111, 25, 35, N'Jul', 2019, CAST(34870.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1112, 25, 35, N'Aug', 2019, CAST(51428.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1113, 25, 35, N'Sep', 2019, CAST(76887.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1114, 25, 35, N'Oct', 2019, CAST(76887.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1115, 25, 35, N'Nov', 2019, CAST(76887.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1116, 25, 35, N'Dec', 2019, CAST(63921.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1117, 25, 36, N'Jan', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1118, 25, 36, N'Feb', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1119, 25, 36, N'Mar', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1120, 25, 36, N'Apr', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1121, 25, 36, N'May', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1122, 25, 36, N'Jun', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1123, 25, 36, N'Jul', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1124, 25, 36, N'Aug', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1125, 25, 36, N'Sep', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1126, 25, 36, N'Oct', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1127, 25, 36, N'Nov', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1128, 25, 36, N'Dec', 2019, CAST(10013.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1129, 27, 31, N'Jan', 2019, CAST(165627.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1130, 27, 31, N'Feb', 2019, CAST(156683.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1131, 27, 31, N'Mar', 2019, CAST(159555.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1132, 27, 31, N'Apr', 2019, CAST(80705.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1133, 27, 31, N'May', 2019, CAST(142147.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1134, 27, 31, N'Jun', 2019, CAST(165132.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1135, 27, 31, N'Jul', 2019, CAST(114229.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1136, 27, 31, N'Aug', 2019, CAST(106274.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1137, 27, 31, N'Sep', 2019, CAST(99807.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1138, 27, 31, N'Oct', 2019, CAST(149353.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1139, 27, 31, N'Nov', 2019, CAST(87576.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1140, 27, 31, N'Dec', 2019, CAST(135147.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1141, 27, 34, N'Jan', 2019, CAST(61069.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1142, 27, 34, N'Feb', 2019, CAST(47611.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1143, 27, 34, N'Mar', 2019, CAST(46984.1600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1144, 27, 34, N'Apr', 2019, CAST(61386.6600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1145, 27, 34, N'May', 2019, CAST(92718.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1146, 27, 34, N'Jun', 2019, CAST(71574.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1147, 27, 34, N'Jul', 2019, CAST(58803.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1148, 27, 34, N'Aug', 2019, CAST(102906.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1149, 27, 34, N'Sep', 2019, CAST(51376.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1150, 27, 34, N'Oct', 2019, CAST(51268.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1151, 27, 34, N'Nov', 2019, CAST(48210.3100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1152, 27, 34, N'Dec', 2019, CAST(92718.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1153, 27, 35, N'Jan', 2019, CAST(23184.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1154, 27, 35, N'Feb', 2019, CAST(23184.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1155, 27, 35, N'Mar', 2019, CAST(23184.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1156, 27, 35, N'Apr', 2019, CAST(18559.9200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1157, 27, 35, N'May', 2019, CAST(22358.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1158, 27, 35, N'Jun', 2019, CAST(19552.3600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1159, 27, 35, N'Jul', 2019, CAST(22803.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1160, 27, 35, N'Aug', 2019, CAST(23593.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1161, 27, 35, N'Sep', 2019, CAST(11830.0700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1162, 27, 35, N'Oct', 2019, CAST(8489.0000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1163, 27, 35, N'Nov', 2019, CAST(30596.7100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1164, 27, 35, N'Dec', 2019, CAST(22358.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1165, 27, 36, N'Jan', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1166, 27, 36, N'Feb', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1167, 27, 36, N'Mar', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1168, 27, 36, N'Apr', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1169, 27, 36, N'May', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1170, 27, 36, N'Jun', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1171, 27, 36, N'Jul', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1172, 27, 36, N'Aug', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1173, 27, 36, N'Sep', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1174, 27, 36, N'Oct', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1175, 27, 36, N'Nov', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportSalesChannelQuota] ([Id], [SalesRepId], [SalesChannelId], [Month], [Year], [Revenue]) VALUES (1176, 27, 36, N'Dec', 2019, CAST(4249.7800 AS Decimal(19, 4)))
GO
SET IDENTITY_INSERT [dbo].[ReportSalesChannelQuota] OFF
GO
