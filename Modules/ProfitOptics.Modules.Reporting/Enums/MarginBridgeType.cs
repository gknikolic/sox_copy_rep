﻿using System.ComponentModel;

namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum MarginBridgeType
    {
        [Description("GP%")]
        GPPercentage = 0,

        [Description("GP$")]
        GPDollarAmount
    }
}
