﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.BusinessReview;
using ProfitOptics.Modules.Reporting.Services;
using ProfitOptics.Modules.Reporting.Services.BusinessReview;

namespace ProfitOptics.Modules.Reporting.Factories.BusinessReview
{
    public class BusinessReviewModelFactory : IBusinessReviewModelFactory
    {
        #region Fields

        private readonly IUserHierarchyService _userHierarchyService;
        private readonly IBusinessReviewDataService _businessReviewDataService;
        private readonly ReportingSettings _reportingSettings;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private const string DefaultBusinessReviewReportTemplatePath = @"\Modules\ProfitOptics.Modules.Reporting\Content\Templates\BusinessReviewReportTemplate.xlsx";

        #endregion

        #region Ctor

        public BusinessReviewModelFactory(IUserHierarchyService userHierarchyService,
                                          IBusinessReviewDataService businessReviewDataService,
                                          ReportingSettings reportingSettings,
                                          IWebHostEnvironment hostingEnvironment)
        {
            this._userHierarchyService = userHierarchyService;
            this._businessReviewDataService = businessReviewDataService;
            this._reportingSettings = reportingSettings;
            this._hostingEnvironment = hostingEnvironment;
        }

        #endregion

        #region Methods

        public BusinessReviewViewModel PrepareBusinessReviewViewModel(int currentUserId)
        {
            var reportTypeClassModel = PrepareReportTypeClassSelectorModels();
            var viewModel = new BusinessReviewViewModel
            {
                ReportTypes = reportTypeClassModel,
                MaxNumberOfCharactersInNote = _reportingSettings.MaxNumberOfCharactersInNote
            };

            return viewModel;
        }

        public BusinessReviewReportFileModel PrepareBusinessReviewReportModel(int customerId, BusinessReviewReportType reportType, string notes)
        {
            var customerName = _businessReviewDataService.GetCustomerName(customerId, reportType);

            var model = new BusinessReviewReportFileModel();

            List<BusinessReviewExcelModel> data = PrepareBusinessReviewDataForExcelReport(customerId, reportType);

            var path = _reportingSettings.BusinessReviewReportTemplatePath;

            model.FileName = $"{customerName} - {DateTime.Now:MM-dd-yyyy}.xlsx";

            model.ContentType = ExcelFile.ExcelContentType;

            if (!string.IsNullOrWhiteSpace(path))
            {
                model.Stream = PrepareBusinessReviewExcelReport(path, data, notes == null ? "" : notes.Replace("<br/>", Environment.NewLine), reportType);
            }
            else
            {
                model.Stream = PrepareDefaultBusinessReviewExcelReport(model.FileName, data, notes == null ? "" : notes.Replace("<br/>", Environment.NewLine), reportType);
            }

            return model;
        }

        public Select2PagedResult PrepareBusinessReviewDropdownModelForReportType(int customerId, BusinessReviewReportType reportType, string search, int page = 0, int take = int.MaxValue)
        {
            var result = new Select2PagedResult();

            switch (reportType)
            {
                case BusinessReviewReportType.BillTo:
                    {
                        result = _userHierarchyService.GetUserBillTos(customerId, search, out int totalCount, (page - 1) * take, take).ToSelect2PagedResultFromModel<ReportBillTo>(totalCount);
                    }
                    break;
                case BusinessReviewReportType.ShipTo:
                    {
                        result = _userHierarchyService.GetUserShipTos(customerId, search, out int totalCount, (page - 1) * take, take).ToSelect2PagedResultFromModel<ReportShipTo>(totalCount);
                    }
                    break;
            }

            return result;
        }

        public List<BusinessReviewExcelModel> PrepareBusinessReviewDataForExcelReport(int customerId, BusinessReviewReportType reportType)
        {
            var prevPrevYear = DateTime.Now.AddYears(-2).Year;
            var prevYear = DateTime.Now.AddYears(-1).Year;

            var prevPrevYearStart = new DateTime(DateTime.Now.AddYears(-2).Year, 1, 1);
            var prevYearStart = new DateTime(DateTime.Now.AddYears(-1).Year, 1, 1);
            var currentYearStart = new DateTime(DateTime.Now.Year, 1, 1);

            var prevYtd = DateTime.Now.AddYears(-1).AddDays(-1);
            var prevPrevYtd = DateTime.Now.AddYears(-2).AddDays(-1);
            var currentYtd = DateTime.Now.AddDays(-1);

            var rawSales = _businessReviewDataService.GetReportSalesHistoryData(customerId, reportType);

            var inMemorySales = (from s in rawSales
                                 select new
                                 {
                                     ProductFamily = s.ReportVendor.Name,
                                     s.ReportItem.ModelName,
                                     s.CustomerClassId,
                                     BillToId = s.ReportBillTo.Id,
                                     ShipToId = s.ReportShipTo.Id,
                                     s.ReportBillTo.BillToName,
                                     s.ReportShipTo.ShipToName,
                                     s.ReportBillTo.BillToNum,
                                     s.ReportShipTo.ShipToNum,
                                     s.InvoiceDate,
                                     s.ExtPrice,
                                     s.Qty
                                 }).ToList();

            List<BusinessReviewExcelModel> result = (from sale in inMemorySales
                group sale by new
                {
                    sale.ProductFamily,
                    sale.ModelName,
                }
                into g
                select new BusinessReviewExcelModel
                {
                    CustomerId = customerId,
                    CustomerClassId = g.Select(x => x.CustomerClassId ?? -1).FirstOrDefault(),
                    CustomerName = reportType == BusinessReviewReportType.BillTo ? g.FirstOrDefault(x => x.BillToId == customerId)?.BillToName :
                        reportType == BusinessReviewReportType.ShipTo ? g.FirstOrDefault(x => x.ShipToId == customerId)?.ShipToName : string.Empty,
                    CustomerNum = reportType == BusinessReviewReportType.BillTo
                        ? g.FirstOrDefault(x => x.BillToId == customerId)?.BillToNum
                        : g.FirstOrDefault(x => x.ShipToId == customerId)?.ShipToNum,
                    ProductFamily = g.Key.ProductFamily,
                    ModelName = g.Key.ModelName,
                    PrevPrevYearRevenue = g.Any(x => x.InvoiceDate.Year == prevPrevYear)
                        ? g.Where(x => x.InvoiceDate.Year == prevPrevYear).Sum(x => x.ExtPrice)
                        : 0.0m,
                    PrevYearRevenue = g.Any(x => x.InvoiceDate.Year == prevYear)
                        ? g.Where(x => x.InvoiceDate.Year == prevYear).Sum(x => x.ExtPrice)
                        : 0.0m,
                    PrevPrevYTDRevenue = g.Any(x => x.InvoiceDate >= prevPrevYearStart && x.InvoiceDate <= prevPrevYtd)
                        ? g.Where(x => x.InvoiceDate >= prevPrevYearStart && x.InvoiceDate <= prevPrevYtd).Sum(x => x.ExtPrice)
                        : 0.0m,
                    PrevYTDRevenue = g.Any(x => x.InvoiceDate >= prevYearStart && x.InvoiceDate <= prevYtd)
                        ? g.Where(x => x.InvoiceDate >= prevYearStart && x.InvoiceDate <= prevYtd).Sum(x => x.ExtPrice)
                        : 0.0m,
                    CurrentYTDRevenue = g.Any(x => x.InvoiceDate >= currentYearStart && x.InvoiceDate <= currentYtd)
                        ? g.Where(x => x.InvoiceDate >= currentYearStart && x.InvoiceDate <= currentYtd).Sum(x => x.ExtPrice)
                        : 0.0m,
                    PrevPrevYearQty = g.Any(x => x.InvoiceDate.Year == prevPrevYear)
                        ? g.Where(x => x.InvoiceDate.Year == prevPrevYear).Sum(x => x.Qty)
                        : 0.0m,
                    PrevYearQty = g.Any(x => x.InvoiceDate.Year == prevYear)
                        ? g.Where(x => x.InvoiceDate.Year == prevYear).Sum(x => x.Qty)
                        : 0.0m,
                    PrevYTDQty = g.Any(x => x.InvoiceDate >= prevYearStart && x.InvoiceDate <= prevYtd)
                        ? g.Where(x => x.InvoiceDate >= prevYearStart && x.InvoiceDate <= prevYtd).Sum(x => x.Qty)
                        : 0.0m,
                    CurrentYTDQty = g.Any(x => x.InvoiceDate >= currentYearStart && x.InvoiceDate <= currentYtd)
                        ? g.Where(x => x.InvoiceDate >= currentYearStart && x.InvoiceDate <= currentYtd).Sum(x => x.Qty)
                        : 0.0m,
                    Growth = g.Any(x => x.InvoiceDate >= prevYearStart && x.InvoiceDate <= prevYtd) 
                             && g.Where(x => x.InvoiceDate >= prevYearStart && x.InvoiceDate <= prevYtd).Sum(x => x.ExtPrice) != 0
                        ? ((g.Any(x => x.InvoiceDate >= currentYearStart && x.InvoiceDate <= currentYtd)
                               ? g.Where(x => x.InvoiceDate >= currentYearStart && x.InvoiceDate <= currentYtd).Sum(x => x.ExtPrice)
                               : 0.0m) - g.Where(x => x.InvoiceDate >= prevYearStart && x.InvoiceDate <= prevYtd).Sum(x => x.ExtPrice))
                          / g.Where(x => x.InvoiceDate >= prevYearStart && x.InvoiceDate <= prevYtd).Sum(x => x.ExtPrice)
                        : 0.0m,
                    ASP = g.Any(x => x.InvoiceDate.Year == prevYear)
                        ? g.Where(x => x.InvoiceDate.Year == prevYear).Sum(x => x.Qty) != 0
                            ?
                            g.Where(x => x.InvoiceDate.Year == prevYear).Sum(x => x.ExtPrice) / g.Where(x => x.InvoiceDate.Year == prevYear).Sum(x => x.Qty)
                            : 0.0m
                        : 0.0m
                }).ToList();

            return result;
        }

        #endregion

        #region Util

        /// <summary>
        /// Generates Excel report based on the given template located in templates folder in reporting area of the web project.
        /// </summary>
        /// <param name="fileName">Name of the generated excel report.</param>
        /// <param name="records">List of items used to populate field in the report.</param>
        /// <param name="notes">The notes that will be added to the notes section of the report.</param>
        /// <param name="reportType">The report type.</param>
        /// <returns>A <see cref="MemoryStream"/> containing the report.</returns>
        private MemoryStream PrepareBusinessReviewExcelReport(string fileName, List<BusinessReviewExcelModel> records, string notes, BusinessReviewReportType reportType)
        {
            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                using (var pck = new ExcelPackage())
                {
                    pck.Load(stream);
                    var ws0 = pck.Workbook.Worksheets[1];
                    var ws1 = pck.Workbook.Worksheets[2];
                    var ws2 = pck.Workbook.Worksheets[3];

                    ws0.Cells["A5"].Value = "Product Category";
                    ws0.Cells["D5"].Value = DateTime.UtcNow.AddYears(-2).Year;
                    ws0.Cells["E5"].Value = DateTime.UtcNow.AddYears(-1).Year;
                    ws0.Cells["F5"].Value = "YTD " + DateTime.UtcNow.AddYears(-1).Year;
                    ws0.Cells["G5"].Value = "YTD " + DateTime.UtcNow.Year;

                    ws1.Cells["A1"].Value = records.Sum(item => item.PrevPrevYTDRevenue);
                    ws1.Cells["A2"].Value = records.Sum(item => item.PrevYTDRevenue);
                    ws1.Cells["A3"].Value = records.Sum(item => item.CurrentYTDRevenue);

                    ws2.Cells["A1"].Value = records.Where(item => item.ProductFamily == "Ankle").Sum(item => item.PrevPrevYTDRevenue);
                    ws2.Cells["A2"].Value = records.Where(item => item.ProductFamily == "Feet").Sum(item => item.PrevPrevYTDRevenue);
                    ws2.Cells["A3"].Value = records.Where(item => item.ProductFamily == "Knee").Sum(item => item.PrevPrevYTDRevenue);
                    ws2.Cells["A4"].Value = records.Where(item => item.ProductFamily == "Other").Sum(item => item.PrevPrevYTDRevenue);

                    ws2.Cells["B1"].Value = records.Where(item => item.ProductFamily == "Ankle").Sum(item => item.PrevYTDRevenue);
                    ws2.Cells["B2"].Value = records.Where(item => item.ProductFamily == "Feet").Sum(item => item.PrevYTDRevenue);
                    ws2.Cells["B3"].Value = records.Where(item => item.ProductFamily == "Knee").Sum(item => item.PrevYTDRevenue);
                    ws2.Cells["B4"].Value = records.Where(item => item.ProductFamily == "Other").Sum(item => item.PrevYTDRevenue);

                    ws2.Cells["C1"].Value = records.Where(item => item.ProductFamily == "Ankle").Sum(item => item.CurrentYTDRevenue);
                    ws2.Cells["C2"].Value = records.Where(item => item.ProductFamily == "Feet").Sum(item => item.CurrentYTDRevenue);
                    ws2.Cells["C3"].Value = records.Where(item => item.ProductFamily == "Knee").Sum(item => item.CurrentYTDRevenue);
                    ws2.Cells["C4"].Value = records.Where(item => item.ProductFamily == "Other").Sum(item => item.CurrentYTDRevenue);

                    //Add notes to the note cells
                    var noteCells = ws0.MergedCells[0];
                    ws0.Cells[noteCells].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws0.Cells[noteCells].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    ws0.Cells[noteCells].Style.Font.Size = 10;
                    ws0.Cells[noteCells].Style.WrapText = true;
                    ws0.Cells[noteCells].Value = notes;

                    ws0.HeaderFooter.differentFirst = false;

                    string parsedCustomerName = records.First().CustomerName.Replace("&", "&&");

                    switch (reportType)
                    {
                        case BusinessReviewReportType.BillTo:
                            ws0.HeaderFooter.OddHeader.LeftAlignedText = "&\"-,Bold\"&9\nCustomer Name: " + parsedCustomerName + " (" + records.First().CustomerNum + ") " + "\nDate Thru: " + DateTime.UtcNow.AddDays(-1).ToShortDateString();
                            break;
                        case BusinessReviewReportType.ShipTo:
                            ws0.HeaderFooter.OddHeader.LeftAlignedText = "&\"-,Bold\"&9\nCustomer Number: " + records.First().CustomerNum + "\nDate Thru: " + DateTime.UtcNow.AddDays(-1).ToShortDateString();
                            break;
                        default:
                            throw new ArgumentException(@"Unsupported report type specified", nameof(reportType));
                    }

                    ws0.HeaderFooter.OddHeader.RightAlignedText = ws0.HeaderFooter.OddHeader.RightAlignedText;
                    ws0.HeaderFooter.OddFooter.CenteredText = "&KFF0000Report Run Date: " + DateTime.UtcNow.ToShortDateString() + " - CONFIDENTIAL Freedom Innovations, LLC";

                    ws0.Cells["H6:H39"].Style.Numberformat.Format = "0%;[Red](0)%";

                    var columnA = ws0.Cells["A5:A" + ws0.Cells.Max(item => item.End.Row)];
                    for (var startRow = columnA.Start.Row; startRow < columnA.End.Row; startRow++)
                    {
                        if (ws0.Cells[startRow, 1].Value != null)
                        {
                            var row = ws0.Cells[startRow, 1, startRow, 3];
                            var rowData = row.Select(item => (string)item.Value).ToArray();
                            var record = records.FirstOrDefault(item => item.ProductFamily == rowData[0] && item.ModelName == rowData[1]);
                            if (record != null)
                            {
                                ws0.Cells[startRow, 4].Value = record.PrevPrevYearQty;
                                ws0.Cells[startRow, 5].Value = record.PrevYearQty;
                                ws0.Cells[startRow, 6].Value = record.PrevYTDQty;
                                ws0.Cells[startRow, 7].Value = record.CurrentYTDQty;
                                ws0.Cells[startRow, 8].Value = record.Growth;
                                ws1.Cells[startRow, 13].Value = record.PrevPrevYTDRevenue;
                                records.Remove(record);
                            }
                        }
                    }
                    var feetRange = columnA.Where(item => item.Value != null && item.Value.ToString() == "Feet").Select(item => item.Start.Row).ToList();
                    var ankleRange = columnA.Where(item => item.Value != null && item.Value.ToString() == "Ankle").Select(item => item.Start.Row).ToList();
                    var kneeRange = columnA.Where(item => item.Value != null && item.Value.ToString() == "Knee").Select(item => item.Start.Row).ToList();

                    var feetRangeEnd = feetRange.Max();
                    var kneeRangeEnd = kneeRange.Max();

                    //Populate "other" rows
                    var otherRecords = new Dictionary<int, string>
                    {
                        {feetRangeEnd, "Feet"},
                        {kneeRangeEnd, "Knee"},
                        {kneeRangeEnd + 2, "Other"}
                    };

                    foreach (var record in otherRecords)
                    {
                        ws0.Cells[record.Key, 4].Value = records.Where(item => item.ProductFamily != null && item.ProductFamily.ToString() == record.Value).Sum(item => item.PrevPrevYearQty);
                        ws0.Cells[record.Key, 5].Value = records.Where(item => item.ProductFamily != null && item.ProductFamily.ToString() == record.Value).Sum(item => item.PrevYearQty);
                        ws0.Cells[record.Key, 6].Value = records.Where(item => item.ProductFamily != null && item.ProductFamily.ToString() == record.Value).Sum(item => item.PrevYTDQty);
                        ws0.Cells[record.Key, 7].Value = records.Where(item => item.ProductFamily != null && item.ProductFamily.ToString() == record.Value).Sum(item => item.CurrentYTDQty);
                        ws0.Cells[record.Key, 8].Value = records.Where(item => item.ProductFamily != null && item.ProductFamily.ToString() == record.Value).Sum(item => item.PrevYTDRevenue) != 0 ? (records.Where(item => item.ProductFamily != null && item.ProductFamily.ToString() == record.Value).Sum(item => item.CurrentYTDRevenue) - records.Where(item => item.ProductFamily != null && item.ProductFamily.ToString() == record.Value).Sum(item => item.PrevYTDRevenue)) / records.Where(item => item.ProductFamily != null && item.ProductFamily.ToString() == record.Value).Sum(item => item.PrevYTDRevenue) : 0;
                    }

                    //ws1.Cells["A1:A3"].Style.Numberformat.Format = numberFormat;

                    ws1.Cells["C6"].Value = DateTime.UtcNow.AddYears(-2).Year;
                    ws1.Cells["D6"].Value = DateTime.UtcNow.AddYears(-1).Year;
                    ws1.Cells["E6"].Value = DateTime.UtcNow.Year;

                    ws2.Cells["G1"].Value = DateTime.UtcNow.AddYears(-2).Year;
                    ws2.Cells["G2"].Value = DateTime.UtcNow.AddYears(-1).Year;
                    ws2.Cells["G3"].Value = DateTime.UtcNow.Year;

                    ws0.Cells["H30"].Formula = "IF(" + ws2.Cells["B2"].FullAddressAbsolute + "=0,,(" + ws2.Cells["C2"].FullAddressAbsolute + "-" + ws2.Cells["B2"].FullAddressAbsolute + ")/" + ws2.Cells["B2"].FullAddressAbsolute + ")";//"IF(F30=0,,(G30-F30)/F30)";
                    ws0.Cells["H34"].Formula = "IF(" + ws2.Cells["B1"].FullAddressAbsolute + "=0,,(" + ws2.Cells["C1"].FullAddressAbsolute + "-" + ws2.Cells["B1"].FullAddressAbsolute + ")/" + ws2.Cells["B1"].FullAddressAbsolute + ")";//"IF(F33=0,,(G33-F33)/F33)";

                    ws0.Cells["H38"].Formula = "IF(" + ws2.Cells["B3"].FullAddressAbsolute + "=0,,(" + ws2.Cells["C3"].FullAddressAbsolute + "-" + ws2.Cells["B3"].FullAddressAbsolute + ")/" + ws2.Cells["B3"].FullAddressAbsolute + ")";//"IF(F37=0,,(G37-F37)/F37)";
                    ws0.Cells["H39"].Formula = "IF(" + ws1.Cells["A2"].FullAddressAbsolute + "=0,,(" + ws1.Cells["A3"].FullAddressAbsolute + "-" + ws1.Cells["A2"].FullAddressAbsolute + ")/" + ws1.Cells["A2"].FullAddressAbsolute + ")";//"IF(F39=0,,(G39-F39)/F39)";

                    ws2.Workbook.FullCalcOnLoad = true;
                    ws1.Workbook.FullCalcOnLoad = true;
                    ws0.Workbook.FullCalcOnLoad = true;

                    ws0.Workbook.Calculate();
                    ws1.Workbook.Calculate();
                    ws2.Workbook.Calculate();


                    return new MemoryStream(pck.GetAsByteArray());
                }
            }
        }

        private List<BusinessReviewReportTypeClassSelectorsModel> PrepareReportTypeClassSelectorModels()
        {
            var result = new List<BusinessReviewReportTypeClassSelectorsModel>();
            foreach (var reportType in Enum.GetValues(typeof(BusinessReviewReportType)))
            {
                //var rt = (BusinessReviewReportType)reportType;
                var reportTypeName = reportType.ToString();

                result.Add(new BusinessReviewReportTypeClassSelectorsModel
                {
                    ReportButtonClassName = $"generate-report-{reportTypeName}",
                    PdfButtonClassName = $"generate-pdf-{reportTypeName}",
                    SelectControlSelector = $"{reportTypeName}s option:selected",
                    SelectControlClassName = $"{reportTypeName}s",
                    NotesClassName = $"{reportTypeName}s-notes",
                    NotesCharacterClassName = $"{reportTypeName}s-notes-characters",
                    MessageClassName = $"{reportTypeName}",
                    PdfPreviewIdName = $"pdfPreview-{reportTypeName}",
                    NavigationTabId = $"{reportTypeName}-tab",
                    ReportType = ((int)reportType).ToString()
                });
            }
            return result;
        }

        private MemoryStream PrepareDefaultBusinessReviewExcelReport(string fileName, List<BusinessReviewExcelModel> records, string notes, BusinessReviewReportType reportType)
        {
            var filePath = _hostingEnvironment.ContentRootPath + DefaultBusinessReviewReportTemplatePath;
            
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var pck = new ExcelPackage())
                {
                    pck.Load(stream);
                    var ws0 = pck.Workbook.Worksheets[1];
                    var ws1 = pck.Workbook.Worksheets[2];
                    var ws2 = pck.Workbook.Worksheets[3];

                    //set headers
                    ws0.Cells["A5"].Value = "Product Category";
                    ws0.Cells["D5"].Value = DateTime.UtcNow.AddYears(-2).Year;
                    ws0.Cells["E5"].Value = DateTime.UtcNow.AddYears(-1).Year;
                    ws0.Cells["F5"].Value = "YTD " + DateTime.UtcNow.AddYears(-1).Year;
                    ws0.Cells["G5"].Value = "YTD " + DateTime.UtcNow.Year;

                    ws1.Cells["A1"].Value = records.Sum(item => item.PrevPrevYTDRevenue);
                    ws1.Cells["A2"].Value = records.Sum(item => item.PrevYTDRevenue);
                    ws1.Cells["A3"].Value = records.Sum(item => item.CurrentYTDRevenue);

                    var topProductFamilies = (from record in records
                                               group record by record.ProductFamily into g
                                               select new { productFamily = g.Key, sum = g.Sum(item => item.CurrentYTDRevenue + item.PrevYTDRevenue + item.PrevPrevYTDRevenue) })
                                               .OrderByDescending(item => item.sum).Select(item => item.productFamily).Take(3).ToList();

                    var firstProductFamily = topProductFamilies.Count > 0 ? topProductFamilies[0] : "" ;
                    var secondProductFamily = topProductFamilies.Count > 1 ? topProductFamilies[1] : "";
                    var thirdProductFamily = topProductFamilies.Count > 2 ? topProductFamilies[2] : "";

                    ws2.Cells["A1"].Value = records.Where(item => item.ProductFamily == firstProductFamily).Sum(item => item.PrevPrevYTDRevenue);
                    ws2.Cells["A2"].Value = records.Where(item => item.ProductFamily == secondProductFamily).Sum(item => item.PrevPrevYTDRevenue);
                    ws2.Cells["A3"].Value = records.Where(item => item.ProductFamily == thirdProductFamily).Sum(item => item.PrevPrevYTDRevenue);
                    ws2.Cells["A4"].Value = records.Where(item => topProductFamilies.Contains(item.ProductFamily)).Sum(item => item.PrevPrevYTDRevenue);

                    ws2.Cells["B1"].Value = records.Where(item => item.ProductFamily == firstProductFamily).Sum(item => item.PrevYTDRevenue);
                    ws2.Cells["B2"].Value = records.Where(item => item.ProductFamily == secondProductFamily).Sum(item => item.PrevYTDRevenue);
                    ws2.Cells["B3"].Value = records.Where(item => item.ProductFamily == thirdProductFamily).Sum(item => item.PrevYTDRevenue);
                    ws2.Cells["B4"].Value = records.Where(item => topProductFamilies.Contains(item.ProductFamily)).Sum(item => item.PrevYTDRevenue);

                    ws2.Cells["C1"].Value = records.Where(item => item.ProductFamily == firstProductFamily).Sum(item => item.CurrentYTDRevenue);
                    ws2.Cells["C2"].Value = records.Where(item => item.ProductFamily == secondProductFamily).Sum(item => item.CurrentYTDRevenue);
                    ws2.Cells["C3"].Value = records.Where(item => item.ProductFamily == thirdProductFamily).Sum(item => item.CurrentYTDRevenue);
                    ws2.Cells["C4"].Value = records.Where(item => topProductFamilies.Contains(item.ProductFamily)).Sum(item => item.CurrentYTDRevenue);

                    ws2.Cells["E1"].Value = firstProductFamily;
                    ws2.Cells["E2"].Value = secondProductFamily;
                    ws2.Cells["E3"].Value = thirdProductFamily;

                    string parsedCustomerName = records.First().CustomerName.Replace("&", "&&");

                    switch (reportType)
                    {
                        case BusinessReviewReportType.BillTo:
                            ws0.HeaderFooter.OddHeader.LeftAlignedText = "&\"-,Bold\"&9Customer Name: " + parsedCustomerName + " (" + records.First().CustomerNum + ") " + "\nDate Thru: " + DateTime.UtcNow.AddDays(-1).ToShortDateString();
                            break;
                        case BusinessReviewReportType.ShipTo:
                            ws0.HeaderFooter.OddHeader.LeftAlignedText = "&\"-,Bold\"&9Customer Number: " + records.First().CustomerNum + "\nDate Thru: " + DateTime.UtcNow.AddDays(-1).ToShortDateString();
                            break;
                        default:
                            throw new ArgumentException(@"Unsupported report type specified", nameof(reportType));
                    }

                    ws0.HeaderFooter.OddHeader.RightAlignedText = ws0.HeaderFooter.OddHeader.RightAlignedText;
                    ws0.HeaderFooter.OddFooter.CenteredText = "&KFF0000Report Run Date: " + DateTime.UtcNow.ToShortDateString() + " - CONFIDENTIAL Document";

                    ws0.Cells[$"H6:H{6 + records.Count}"].Style.Numberformat.Format = "0%;[Red](0)%";

                    var columnA = ws0.Cells["A5:A" + ws0.Cells.Max(item => item.End.Row)];

                    var row = columnA.Start.Row;

                    records = records.OrderBy(item => item.ProductFamily).ToList();

                    for (var i = 0; i < records.Count; i++)
                    {
                        //Populate rows with data
                        row++;

                        ws0.Cells[row, 1].Value = records[i].ProductFamily;
                        ws0.Cells[row, 2].Value = records[i].ModelName;
                        ws0.Cells[row, 3].Value = records[i].Code;
                        ws0.Cells[row, 4].Value = records[i].PrevPrevYearQty;
                        ws0.Cells[row, 5].Value = records[i].PrevYearQty;
                        ws0.Cells[row, 6].Value = records[i].PrevYTDQty;
                        ws0.Cells[row, 7].Value = records[i].CurrentYTDQty;
                        ws0.Cells[row, 8].Value = records[i].Growth;
                        ws1.Cells[row, 13].Value = records[i].PrevPrevYTDRevenue;

                        //Set row style
                        ws0.Row(row).CustomHeight = false;
                        ws0.Row(row).Style.Font.Color.SetColor(Color.Black);

                        //Add "Total" row for each product family
                        if ((i < records.Count - 1 && records[i].ProductFamily != records[i + 1].ProductFamily) || i == records.Count - 1)
                        {
                            row++;
                            ws0.Cells[row, 1].Value = records[i].ProductFamily + " Total";
                            ws0.Cells[row, 4].Value = records.Where(item => item.ProductFamily == records[i].ProductFamily).Sum(item => item.PrevPrevYearQty);
                            ws0.Cells[row, 5].Value = records.Where(item => item.ProductFamily == records[i].ProductFamily).Sum(item => item.PrevYearQty);
                            ws0.Cells[row, 6].Value = records.Where(item => item.ProductFamily == records[i].ProductFamily).Sum(item => item.PrevYTDQty);
                            ws0.Cells[row, 7].Value = records.Where(item => item.ProductFamily == records[i].ProductFamily).Sum(item => item.CurrentYTDQty);
                            ws0.Cells[row, 8].Value = records.Where(item => item.ProductFamily == records[i].ProductFamily).Sum(item => item.PrevYTDRevenue) != 0
                                ? (records.Where(item => item.ProductFamily == records[i].ProductFamily).Sum(item => item.CurrentYTDRevenue)
                                   - records.Where(item => item.ProductFamily == records[i].ProductFamily).Sum(item => item.PrevYTDRevenue))
                                  / records.Where(item => item.ProductFamily == records[i].ProductFamily).Sum(item => item.PrevYTDRevenue)
                                : 0;

                            ws0.Row(row).CustomHeight = false;
                            ws0.Row(row).Style.Font.Bold = true;

                            Color colFromHex = ColorTranslator.FromHtml("#B4C7E7");
                            ws0.Cells[row, 1, row, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws0.Cells[row, 1, row, 8].Style.Fill.BackgroundColor.SetColor(colFromHex);
                        }
                    }

                    //Insert "Total" row for ALL products
                    row++;
                    ws0.Cells[row, 1].Value ="Total";
                    ws0.Cells[row, 4].Value = records.Sum(item => item.PrevPrevYearQty);
                    ws0.Cells[row, 5].Value = records.Sum(item => item.PrevYearQty);
                    ws0.Cells[row, 6].Value = records.Sum(item => item.PrevYTDQty);
                    ws0.Cells[row, 7].Value = records.Sum(item => item.CurrentYTDQty);
                    ws0.Cells[row, 8].Value = records.Sum(item => item.PrevYTDRevenue) != 0
                        ? (records.Sum(item => item.CurrentYTDRevenue)
                           - records.Sum(item => item.PrevYTDRevenue))
                          / records.Sum(item => item.PrevYTDRevenue)
                        : 0;

                    ws0.Row(row).CustomHeight = false;
                    ws0.Row(row).Style.Font.Bold = true;

                    Color color = ColorTranslator.FromHtml("#3366CC");
                    ws0.Cells[row, 1, row, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws0.Cells[row, 1, row, 8].Style.Fill.BackgroundColor.SetColor(color);
                    ws0.Cells[row, 1, row, 8].Style.Font.Color.SetColor(Color.White);

                    //Stylize the report, add borders and fonts.
                    var dataStartRow = columnA.Start.Row + 1;
                    var dataEndRow = row;

                    ws0.Cells[dataStartRow, 1, dataEndRow, 8].Style.Border.Left.Style = ExcelBorderStyle.Hair;
                    ws0.Cells[dataStartRow, 1, dataEndRow, 8].Style.Border.Right.Style = ExcelBorderStyle.Hair;
                    ws0.Cells[dataStartRow, 1, dataEndRow, 8].Style.Border.Top.Style = ExcelBorderStyle.Hair;
                    ws0.Cells[dataStartRow, 1, dataEndRow, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Hair;

                    ws0.Cells[dataStartRow, 1, dataEndRow, 8].Style.Font.Size = 8;
                    ws0.Cells[dataStartRow, 1, dataEndRow, 8].Style.Font.Name = "Arial Narrow";


                    //Add "notes" area
                    ws0.Cells[dataEndRow + 1, 1, dataEndRow + 21, 8].Merge = true;
                    ws0.Cells[dataEndRow + 1, 1, dataEndRow + 21, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws0.Cells[dataEndRow + 1, 1, dataEndRow + 21, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    ws0.Cells[dataEndRow + 1, 1, dataEndRow + 21, 8].Style.Font.Size = 10;
                    ws0.Cells[dataEndRow + 1, 1, dataEndRow + 21, 8].Style.WrapText = true;
                    ws0.Cells[dataEndRow + 1, 1, dataEndRow + 21, 8].Style.Font.Name = "Arial Narrow";
                    ws0.Cells[dataEndRow + 1, 1, dataEndRow + 21, 8].Value ="Notes: \n" + notes;

                    //add borders
                    ws0.Cells[dataStartRow, 1, dataEndRow + 21, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws0.Cells[dataEndRow, 1, dataEndRow + 21, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws0.Cells[dataStartRow, 8, dataEndRow + 21, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    ws0.HeaderFooter.differentFirst = false;

                    //Populate cells for graphs
                    ws1.Cells["C6"].Value = DateTime.UtcNow.AddYears(-2).Year;
                    ws1.Cells["D6"].Value = DateTime.UtcNow.AddYears(-1).Year;
                    ws1.Cells["E6"].Value = DateTime.UtcNow.Year;

                    ws2.Cells["G1"].Value = DateTime.UtcNow.AddYears(-2).Year;
                    ws2.Cells["G2"].Value = DateTime.UtcNow.AddYears(-1).Year;
                    ws2.Cells["G3"].Value = DateTime.UtcNow.Year;

                    ws2.Workbook.FullCalcOnLoad = true;
                    ws1.Workbook.FullCalcOnLoad = true;
                    ws0.Workbook.FullCalcOnLoad = true;

                    ws0.Workbook.Calculate();
                    ws1.Workbook.Calculate();
                    ws2.Workbook.Calculate();

                    return new MemoryStream(pck.GetAsByteArray());
                }
            }
        }

        #endregion
    }
}
