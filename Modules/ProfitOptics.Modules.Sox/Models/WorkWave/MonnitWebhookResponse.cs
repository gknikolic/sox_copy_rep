﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class MonnitWebhookResponse
    {
        [JsonProperty("gatewayMessage")]
        public GatewayMessage GatewayMessage { get; set; }
        [JsonProperty("sensorMessages")]
        public List<SensorMessage> SensorMessages { get; set; }
    }
}
