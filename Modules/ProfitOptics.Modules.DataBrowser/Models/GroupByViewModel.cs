﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ProfitOptics.Modules.DataBrowser.Models
{
    public class GroupByViewModel
    {
        public string[] SelectedColumns { get; set; }
        public IEnumerable<ColumnModel> Columns { get; set; }
    }
}