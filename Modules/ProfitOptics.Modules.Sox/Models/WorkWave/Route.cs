﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class Route
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("revision")]
		public int Revision { get; set; }
		[JsonProperty("date")]
		public string Date { get; set; }
		[JsonProperty("vehicleId")]
		public string VehicleId { get; set; }
		[JsonProperty("driverId")]
		public string DriverId { get; set; }
		//[JsonProperty("vehicleViolations")]
		//public VehicleViolationType VehicleViolations { get; set; }
		[JsonProperty("steps")]
		public List<RouteStep> Steps { get; set; }

	}
}
