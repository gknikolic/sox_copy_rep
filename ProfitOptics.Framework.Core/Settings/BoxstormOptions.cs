﻿namespace ProfitOptics.Framework.Core.Settings
{
	public class BoxstormOptions
	{
		public string BaseUrl { get; set; }
		public string ResponseType { get; set; } = "json";
		public string ApiToken { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public string AppIdiOS { get; set; }
	}
}
