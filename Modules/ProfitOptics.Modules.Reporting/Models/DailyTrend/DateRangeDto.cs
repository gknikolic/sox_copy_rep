﻿using System;

namespace ProfitOptics.Modules.Reporting.Models.DailyTrend
{
    public class DateRangeDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
