﻿using System.Collections.Generic;

namespace ProfitOptics.Framework.Etl.Models
{
    public class DbStatusInfoModel
    {
        public List<EtlLogStatus> EtlStatuses { get; set; }
        public List<DbTableStatus> DbTableStatuses { get; set; }
    }
}
