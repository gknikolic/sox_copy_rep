﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.Resources.Auth
{
    public class RefreshTokenQueryResource
    {
        [Required]
        public string Token { get; set; }

        [Required]
        [StringLength(255)]
        public string Username { get; set; }
    }
}
