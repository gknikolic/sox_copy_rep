﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class SetCallbackUrl
	{
		[JsonProperty("url")]
		public string Url { get; set; }
		//[JsonProperty("signaturePassword")]
		//public string SignaturePassword { get; set; }
		[JsonProperty("test")]
		public bool Test { get; set; }
		//[JsonProperty("headers")]
		//public Dictionary<string,string> Headers{ get; set; }
	}
}
