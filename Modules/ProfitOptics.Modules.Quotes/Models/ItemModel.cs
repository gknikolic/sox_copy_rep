﻿using System;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class ItemModel
    {
        public int ItemId { get; set; }
        public int LineNumber { get; set; }
        public string ItemNum { get; set; }
        public string ItemDescription { get; set; }
        public string ItemUoM { get; set; }
        public DateTime? ItemStartDate { get; set; }

        //for items table
        public string ItemVendor { get; set; }

        public bool ItemPrivateLabel { get; set; }
        public int? ItemQa { get; set; }
        public int? ItemQoo { get; set; }
        public int? BillTo12MoQty { get; set; }
        public int? ShipTo12MoQty { get; set; }

        public decimal? FreightClass { get; set; }
        public decimal? QuoteAverageShippingCost { get; set; }
        public int? Units { get; set; }

        //From Summary
        public int? MoQty { get; set; }

        public decimal? LastPrice { get; set; }
        public DateTime? LastDate { get; set; }
        public decimal? LastGpPct { get; set; }

        //Weight
        public decimal? WeightProduct { get; set; }

        public decimal WeightTotal { get; set; }

        //Current
        public decimal? CurrentCost { get; set; }

        public decimal? CurrentPrice { get; set; }
        public string CurrentSource { get; set; }
        public bool CurrentC { get; set; }
        public bool CurrentB { get; set; }
        public decimal? CurrentGmPercent { get; set; }

        //Quote
        public int QuoteItemId { get; set; }

        public decimal? QuotePrice { get; set; }
        public decimal? QuoteGmPercent { get; set; }

        //Price Guidance
        //Ammount
        public decimal? PriceGuidanceFloor { get; set; }

        public decimal? PriceGuidanceTarget { get; set; }
        public decimal? PriceGuidanceList { get; set; }

        //GM%
        public decimal? PriceGuidanceFloorGmPercent { get; set; }

        public decimal? PriceGuidanceTargetGmPercent { get; set; }
        public decimal? PriceGuidanceListGmPercent { get; set; }

        //Quote Extended
        public decimal QuoteExtendedCost { get; set; }

        public decimal QuoteExtendedRevenue { get; set; }

        //Competitor
        public decimal? CompetitorPrice { get; set; }

        public string CompetitorUoM { get; set; }
        public decimal? CompetitorCf { get; set; }
        public decimal? CompetitorPriceVarDollar { get; set; }
        public decimal? CompetitorExtPriceVarDollar { get; set; }
        public decimal? CompetitorNormalizedPrice { get; set; }

        //Quote Extended
        public decimal? QuoteExtendedMBeforeFreight { get; set; }

        public decimal? QuoteExtendedNetShippingCost { get; set; }
        public decimal? QuoteExtendedNetShippingRevenue { get; set; }
        public decimal? QuoteExtendedAdjustedMarginDollar { get; set; }
        public decimal? QuoteExtendedAdjustedMarginPercent { get; set; }

        public string FreightPaymentType { get; set; }
        public decimal? FreightCap { get; set; }

        //Floor
        public decimal FloorRevenue { get; set; }

        public decimal FloorCost { get; set; }
        public decimal FloorMDollar { get; set; }
        public decimal? FloorMPercent { get; set; }
        public decimal? PlcPrice { get; set; }
    }
}