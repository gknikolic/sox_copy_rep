﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Areas.UserManagement.Data;
using ProfitOptics.Framework.Web.Areas.UserManagement.Factories;
using ProfitOptics.Framework.Web.Areas.UserManagement.Services;
using ProfitOptics.Framework.Web.Framework.Infrastructure;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Infrastructure
{
    public sealed class UserManagementServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddScoped<IUserManagementService, UserManagementService>();
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));
            services.AddScoped<IUserManagementModelFactory, UserManagementModelFactory>();
        }
    }
}
