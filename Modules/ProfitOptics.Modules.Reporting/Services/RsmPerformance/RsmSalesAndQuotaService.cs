﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Services.RsmPerformance
{
    public class RsmSalesAndQuotaService : IRsmSalesAndQuotaService
    {
        #region Fields

        private readonly Entities _context;
        private readonly IDaysOffService _daysOffService;

        #endregion Fields

        #region Ctor

        public RsmSalesAndQuotaService(Entities context,
            IDaysOffService daysOffService)
        {
            _context = context;
            _daysOffService = daysOffService;
        }

        #endregion Ctor

        #region QuotaProvider

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesChannelQuota"/> entity, grouped by SalesChannelId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesChannelQuota"/> entity, grouped by SalesChannelId.</returns>
        public IQueryable<IGrouping<int, ReportCustomerClassQuota>> GetSalesChannelQuotaGroupedBySalesChannelId(int year = 0, int salesRepId = 0)
        {
            IQueryable<IGrouping<int, ReportCustomerClassQuota>> groupedQuota =
                from quota in _context.ReportCustomerClassQuotas
                where (year == 0 || quota.Year == year) &&
                      (salesRepId == 0 || quota.SalesRepId == salesRepId)
                group quota by quota.CustomerClassId;

            return groupedQuota;
        }

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesChannelQuota"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesChannelQuota"/> entity, grouped by SalesRepId.</returns>
        public IQueryable<IGrouping<int, ReportCustomerClassQuota>> GetSalesChannelQuotaGroupedBySalesRepId(int year = 0)
        {
            IQueryable<IGrouping<int, ReportCustomerClassQuota>> groupedQuota =
                from quota in _context.ReportCustomerClassQuotas
                where year == 0 || quota.Year == year
                group quota by quota.SalesRepId;

            return groupedQuota;
        }

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportProductFamilyQuota"/> entity, grouped by ProductFamilyId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportProductFamilyQuota"/> entity, grouped by ProductFamilyId.</returns>
        public IQueryable<IGrouping<int, ReportVendorQuota>>
            GetProductFamilyQuotaGroupedByProductFamilyId(int year = 0, int salesRepId = 0)
        {
            IQueryable<IGrouping<int, ReportVendorQuota>> groupedQuota =
                from quota in _context.ReportVendorQuotas
                where (year == 0 || quota.Year == year) &&
                      (salesRepId == 0 || quota.SalesRepId == salesRepId)
                group quota by quota.VendorId;

            return groupedQuota;
        }

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportProductFamilyQuota"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportProductFamilyQuota"/> entity, grouped by SalesRepId.</returns>
        public IQueryable<IGrouping<int, ReportVendorQuota>> GetProductFamilyQuotaGroupedBySalesRepId(int year = 0)
        {
            IQueryable<IGrouping<int, ReportVendorQuota>> groupedQuota =
                from quota in _context.ReportVendorQuotas
                where year == 0 || quota.Year == year
                group quota by quota.SalesRepId;

            return groupedQuota;
        }

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportItemCategoryQuota"/> entity, grouped by ItemCategoryId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportItemCategoryQuota"/> entity, grouped by ItemCategoryId.</returns>
        public IQueryable<IGrouping<int, ReportItemCategoryQuota>>
            GetItemCategoryQuotaGroupedByItemCategoryId(int year = 0, int salesRepId = 0)
        {
            IQueryable<IGrouping<int, ReportItemCategoryQuota>> groupedQuota =
                from quota in _context.ReportItemCategoryQuotas
                where (year == 0 || quota.Year == year) &&
                      (salesRepId == 0 || quota.SalesRepId == salesRepId)
                group quota by quota.ItemCategoryId;

            return groupedQuota;
        }

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportItemCategoryQuota"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportItemCategoryQuota"/> entity, grouped by SalesRepId.</returns>
        public IQueryable<IGrouping<int, ReportItemCategoryQuota>>
            GetItemCategoryQuotaGroupedBySalesRepId(int year = 0)
        {
            IQueryable<IGrouping<int, ReportItemCategoryQuota>> groupedQuota =
                from quota in _context.ReportItemCategoryQuotas
                where year == 0 || quota.Year == year
                group quota by quota.SalesRepId;

            return groupedQuota;
        }

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportRegionQuota"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportRegionQuota"/> entity, grouped by SalesRepId.</returns>
        public IQueryable<IGrouping<int, ReportRegionQuota>>
         GetRegionQuotaGroupedBySalesRepId(int year = 0)
        {
            IQueryable<IGrouping<int, ReportRegionQuota>> groupedQuota =
                from quota in _context.ReportRegionQuotas
                where year == 0 || quota.Year == year
                group quota by quota.SalesRepId;

            return groupedQuota;
        }

        #endregion QuotaProvider

        #region Quota Validator

        private int _lastValidYear;
        private int _lastValidMonth;
        private Dictionary<string, int> _monthNameToNumberMapping;

        public bool QuotaExistsForYearAndMonth(int year, int month)
        {
            if (_lastValidYear == 0 || _lastValidMonth == 0)
            {
                FillMonthNameToNumberMappingDictionary();
                GetLastValidYearAndMonth();
            }

            var validDt = new DateTime(_lastValidYear, _lastValidMonth, 1);

            var dtToCheck = new DateTime(year, month, 1);

            return validDt <= dtToCheck;
        }

        private void FillMonthNameToNumberMappingDictionary()
        {
            _monthNameToNumberMapping = new Dictionary<string, int>();
            var monthCount = 0;
            while (monthCount < 12)
            {
                var month = DateTime.Now.AddMonths(-monthCount).Month;
                _monthNameToNumberMapping.Add(_daysOffService.GetMonthName(month), month);

                monthCount++;
            }
        }

        private void GetLastValidYearAndMonth()
        {
            var validYears = new[] { DateTime.Now.Year, DateTime.Now.Year - 1 };

            var validMonths = _monthNameToNumberMapping.Values.ToArray();
            var validMonthNames = _monthNameToNumberMapping.Keys.ToArray();

            var result = (from q in _context.ReportQuotas
                          where validYears.Contains(q.Year) && validMonthNames.Contains(q.Month)
                          group q by new { q.Year, q.Month } into g
                          select new
                          {
                              g.Key.Year,
                              g.Key.Month
                          }).ToList();

            var minYear = result.Min(x => x.Year);
            int month;
            var minMonth = result
                .Select(x => _monthNameToNumberMapping.TryGetValue(x.Month, out month) ? month : 0).Min();

            _lastValidYear = validYears.Contains(minYear) ? minYear : DateTime.Now.Year;

            _lastValidMonth = validMonths.Contains(minMonth) ? minMonth : DateTime.Now.Month;
        }

        #endregion Quota Validator

        #region Sales Provider

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <param name="zoneId">The zone id filter.</param>
        /// <param name="regionId">The region id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesRepId.</returns>
        public IQueryable<IGrouping<int, ReportSalesHistory>> GetSalesGroupedBySalesRepId(int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null, int? regionId = null)
        {
            IQueryable<IGrouping<int, ReportSalesHistory>> groupedSales = from sales in _context.ReportSalesHistory
                                                                          where sales.InvoiceDate < DateTime.Now &&
                                                                                (year == 0 || sales.InvoiceDate.Year == year) &&
                                                                                (month == 0 || sales.InvoiceDate.Month <= month) &&
                                                                                (zoneId == null || zoneId <= 0 || sales.ZoneId == zoneId) &&
                                                                                (regionId == null || regionId <= 0 || sales.RegionId == regionId) &&
                                                                                (salesRepId == 0 || sales.SalesRepId == salesRepId)
                                                                          group sales by (int)sales.SalesRepId;

            return groupedSales;
        }

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesChannelId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <param name="zoneId">The zone id filter.</param>
        /// <param name="regionId">The region id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesChannelId.</returns>
        public IQueryable<IGrouping<int, ReportSalesHistory>> GetSalesGroupedBySalesChannelId(int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null, int? regionId = null)
        {
            // Sales channel are represented in the CustomerClass table.

            IQueryable<IGrouping<int, ReportSalesHistory>> groupedSales = from sales in _context.ReportSalesHistory
                                                                          where sales.InvoiceDate < DateTime.Now &&
                                                                                (year == 0 || sales.InvoiceDate.Year == year) &&
                                                                                (month == 0 || sales.InvoiceDate.Month <= month) &&
                                                                                (zoneId == null || zoneId <= 0 || sales.ZoneId == zoneId) &&
                                                                                (regionId == null || regionId <= 0 || sales.RegionId == regionId) &&
                                                                                (salesRepId == 0 || sales.SalesRepId == salesRepId)
                                                                          group sales by (int)sales.CustomerClassId;

            return groupedSales;
        }

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by ProductFamilyId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <param name="zoneId">The zone id filter.</param>
        /// <param name="regionId">The region id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by ProductFamilyId.</returns>
        public IQueryable<IGrouping<int, ReportSalesHistory>> GetSalesGroupedByProductFamilyId(
            int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null, int? regionId = null)
        {
            // Product families are represented in the Vendor table.

            IQueryable<IGrouping<int, ReportSalesHistory>> groupedSales = from sales in _context.ReportSalesHistory
                                                                          where sales.InvoiceDate < DateTime.Now &&
                                                                                (year == 0 || sales.InvoiceDate.Year == year) &&
                                                                                (month == 0 || sales.InvoiceDate.Month <= month) &&
                                                                                (zoneId == null || zoneId <= 0 || sales.ZoneId == zoneId) &&
                                                                                (regionId == null || regionId <= 0 || sales.RegionId == regionId) &&
                                                                                (salesRepId == 0 || sales.SalesRepId == salesRepId)
                                                                          group sales by (int)sales.VendorId;

            return groupedSales;
        }

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by ItemCategoryId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <param name="zoneId">The zone id filter.</param>
        /// <param name="regionId">The region id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by ItemCategoryId.</returns>
        public IQueryable<IGrouping<int, ReportSalesHistory>> GetSalesGroupedByItemCategoryId(
            int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null, int? regionId = null)
        {
            IQueryable<IGrouping<int, ReportSalesHistory>> groupedSales = from sales in _context.ReportSalesHistory
                                                                          where sales.InvoiceDate < DateTime.Now &&
                                                                                (year == 0 || sales.InvoiceDate.Year == year) &&
                                                                                (month == 0 || sales.InvoiceDate.Month <= month) &&
                                                                                (zoneId == null || zoneId <= 0 || sales.ZoneId == zoneId) &&
                                                                                (regionId == null || regionId <= 0 || sales.RegionId == regionId) &&
                                                                                (salesRepId == 0 || sales.SalesRepId == salesRepId)
                                                                          group sales by (int)sales.ItemCategoryId;

            return groupedSales;
        }

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="zoneId">The zone id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesRepId.</returns>
        public IQueryable<IGrouping<int, ReportSalesHistory>> GetSalesGroupedBySalesRepIdYearOverYear(int year = 0, int month = 0, int? zoneId = null)
        {
            DateTime? currentPeriodStart, currentPeriodEnd, previousPeriodStart, previousPeriodEnd;
            currentPeriodStart = currentPeriodEnd = previousPeriodStart = previousPeriodEnd = null;

            if (DateTime.Now.Year == year && DateTime.Now.Month == month)
            {
                currentPeriodStart = new DateTime(DateTime.Now.Year, 1, 1).Date;

                currentPeriodEnd = DateTime.Now.Date;

                previousPeriodStart = currentPeriodStart.Value.AddYears(-1);

                previousPeriodEnd = currentPeriodEnd.Value.AddYears(-1);
            }

            IQueryable<IGrouping<int, ReportSalesHistory>> groupedSales = from sales in _context.ReportSalesHistory
                                                                          where (
                                                                                ((currentPeriodStart == null || sales.InvoiceDate >= currentPeriodStart) &&
                                                                                 (currentPeriodEnd == null || sales.InvoiceDate < currentPeriodEnd))
                                                                                 ||
                                                                                ((previousPeriodStart == null || sales.InvoiceDate >= previousPeriodStart) &&
                                                                                 (previousPeriodEnd == null || sales.InvoiceDate < previousPeriodEnd))
                                                                                ) &&
                                                                                (year == 0 || sales.InvoiceDate.Year == year || sales.InvoiceDate.Year == year - 1) &&
                                                                                (month == 0 || sales.InvoiceDate.Month <= month) &&
                                                                                (zoneId == null || zoneId <= 0 || sales.ZoneId == zoneId)
                                                                          group sales by (int)sales.SalesRepId;

            return groupedSales;
        }

        #endregion Sales Provider
    }
}