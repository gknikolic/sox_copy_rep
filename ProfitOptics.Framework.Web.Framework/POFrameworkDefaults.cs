﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Routing;

namespace ProfitOptics.Framework.Web.Framework
{
    public class POFrameworkDefaults
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public POFrameworkDefaults(IWebHostEnvironment hostingEnvironment)
        {
            this._hostingEnvironment = hostingEnvironment;
        }

        public string ProfilePictureAbsoluteFilePath => _hostingEnvironment.WebRootPath + "/img/nouser.jpg";
        public string ProfilePictureRelativeFilePath => "/img/nouser.jpg";

        public static readonly string ConfirmEmailRouteName = "ConfirmEmail";
        public static readonly string UserManagementRouteName = "UserManagement";
        public static readonly string PasswordResetRouteName = "PasswordReset";
        public static readonly string AddPasswordRouteName = "AddPassword";
        public static readonly string LoginPath = "/login";
        public static readonly string LogoutPath = "/logout";
        public static readonly string AccessDenied = "/access-denied";
    }
}