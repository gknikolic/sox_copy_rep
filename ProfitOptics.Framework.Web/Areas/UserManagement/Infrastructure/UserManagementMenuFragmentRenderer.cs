﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Infrastructure
{
    public class UserManagementMenuFragmentRenderer : IMenuFragmentRenderer
    {
        /// <inheritdoc />
        public bool Validate(Common settings, ClaimsPrincipal user)
        {
            return user.IsInRole("admin") ||
                user.IsInRole("superadmin") ||
                user.IsInRole("usermanager");
        }

        /// <inheritdoc />
        public Task<IHtmlContent> RenderMenuFragmentAsync(IHtmlHelper htmlHelper)
        {
            return htmlHelper.PartialAsync("~/Areas/UserManagement/Views/Shared/_Menu.Fragment.cshtml");
        }

        /// <inheritdoc />
        public int Order => 27;
    }
}
