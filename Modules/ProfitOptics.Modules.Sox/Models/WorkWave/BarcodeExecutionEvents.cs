﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class BarcodeExecutionEvents
	{
		[JsonProperty("events")]
		public List<ExecutionEventBarcode> Events{ get; set; }

		[JsonProperty("strict")]
		public bool Strict { get; set; }
	}


}
