﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class POSetting
    {
        public string SettingKey { get; set; }
        public string Value { get; set; }
        public string LastChangedBy { get; set; }
        public DateTime LastChangedTime { get; set; }
    }
}
