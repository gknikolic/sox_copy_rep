﻿//Update when needed !

var RequestStatuses = {
    newStatus: 1,
    SAPending: 2,
    FPCPending: 3,
    CPCPending: 4,
    approved: 5,
    exported: 6,
    closed: 7,
}

var RequestCostStatuses = {
    newStatus: 1,
    CAPending: 2,
    CMPending: 3,
    approved: 4,
    exported: 5,
    closed: 6,
}

var RequestPromotionStatuses = {
    newStatus: 1,
    creatorPending: 2,
    ownerPending: 3,
    FPCPending: 4,
    CPCPending: 5,
    approved: 6,
    exported: 7,
    closed: 8,
    recalled: 9
}

var Roles = {
    SalesAssociate: 1,
    FPC: 2,
    CPC: 3,
    NPC: 4
}

var CustomerBudgetStatuses = {
    newStatus: 1,
    submited: 2,
    locked: 3,
    refused: 4
}

var PromotionsHierarchyTypes = {
    divisions: 'divisions',
    territories: 'territories',
    customerclasses: 'customerclasses'
}

var SalesReportType = {
    SalesPerson: 0,
    SalesManager: 1,
    GeneralManager: 2,
    National: 3
}
