﻿using System;

namespace ProfitOptics.Modules.Reporting.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class FieldTitleAttribute : Attribute
    {
        public FieldTitleAttribute(string name, ColumnType type = ColumnType.Default, TitleStyle titleStyle = TitleStyle.Style1)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));

            ColumnType = type;
            TitleStyle = titleStyle;
        }

        public string Name { get; private set; }
        public ColumnType ColumnType { get; private set; }
        public TitleStyle TitleStyle { get; set; }
    }

    public enum TitleStyle
    {
        Style1,
        Style2
    }

    public enum ColumnType
    {
        Default,
        Dollars,
        DollarsInThousands,
        Percent,
        PercentThree,
        PercentFour,
        Count
    }

    public enum ExcelFileType
    {
        Locations,
        Products,
        Sales
    }
}