﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTCustomerPricingConfig : IEntityTypeConfiguration<QTCustomerPricing>
    {
        public void Configure(EntityTypeBuilder<QTCustomerPricing> builder)
        {
            builder.ToTable("QTCustomerPricing");
            builder.HasKey(entity => new
            {
                entity.Company,
                entity.BillToNum,
                entity.ShipToNum,
                entity.ItemNum,
                entity.UOM,
                entity.PriceListCode,
                entity.SeqNum,
                entity.BasePrice,
                entity.EndDate
            });

            builder.Property(entity => entity.Company).HasMaxLength(50);
            builder.Property(entity => entity.BillToNum).ValueGeneratedNever();
            builder.Property(entity => entity.ShipToNum).HasMaxLength(50);
            builder.Property(entity => entity.ItemNum).HasMaxLength(50);
            builder.Property(entity => entity.UOM).HasMaxLength(50);
            builder.Property(entity => entity.PriceListCode).HasMaxLength(50);
            builder.Property(entity => entity.SeqNum);
            builder.Property(entity => entity.BasePrice);
            builder.Property(entity => entity.StartDate).HasColumnType("date");
            builder.Property(entity => entity.EndDate).HasColumnType("date");
        }
    }
}
