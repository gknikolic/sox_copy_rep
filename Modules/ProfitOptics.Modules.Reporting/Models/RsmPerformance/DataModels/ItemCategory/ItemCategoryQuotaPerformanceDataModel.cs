﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory
{
    public class ItemCategoryQuotaPerformanceDataModel : GenericQuotaPerformanceModel
    {
        public int SalesRepId { get; set; }
        public int ItemCategoryId { get; set; }
        public string ItemCategoryName { get; set; }
    }
}