﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTQuoteConfig : IEntityTypeConfiguration<QTQuote>
    {
        public void Configure(EntityTypeBuilder<QTQuote> builder)
        {
            builder.ToTable("QTQuote");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Name).HasMaxLength(50).IsRequired();
            builder.Property(entity => entity.Description).HasMaxLength(255);
            builder.Property(entity => entity.Status).HasMaxLength(10);
            builder.Property(entity => entity.HeaderComments).HasMaxLength(255);
            builder.Property(entity => entity.FooterComments).HasMaxLength(255);
            builder.Property(entity => entity.ContactEmail).HasMaxLength(50);
            builder.Property(entity => entity.CustomerReference).HasMaxLength(50);
            builder.Property(entity => entity.OrderFrequency).HasMaxLength(50);
            builder.Property(entity => entity.FreightPaymentType).HasMaxLength(50);
            builder.Property(entity => entity.ShipToZipOverride).HasMaxLength(50);
        }
    }
}
