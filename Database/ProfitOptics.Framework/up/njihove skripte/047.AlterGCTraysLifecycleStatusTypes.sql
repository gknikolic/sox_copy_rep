use [Sox]

begin
  
  alter table [Sox].[dbo].[GCTraysLifecycleStatusTypes]
  add [IsActiveStatus] bit;

end