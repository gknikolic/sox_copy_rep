﻿using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.ManualEtl.Data.Domain;
using ProfitOptics.Modules.ManualEtl.Models;
using ProfitOptics.Modules.ManualEtl.Services;

namespace ProfitOptics.Modules.ManualEtl.Factories
{
    public class ManualEtlFactory : IManualEtlFactory
    {
        #region Fields

        private readonly IManualEtlService _manualEtlService;

        #endregion Fields

        #region Ctor

        public ManualEtlFactory(IManualEtlService manualEtlService)
        {
            _manualEtlService = manualEtlService;
        }

        #endregion Ctor

        #region Methods

        public PagedList<EtlLogDataModel> GetEtlLogDataModels(IDataTablesRequest request, string source)
        {
            var logs = _manualEtlService.GetEtlLogsForSource(source)
                .Select(log => CreateAnnouncementModelPredicate(log));

            var paged = ProcessRequest(request, logs);

            return paged;
        }

        private EtlLogDataModel CreateAnnouncementModelPredicate(ETLLog log)
        {
            return new EtlLogDataModel
            {
                Source = log.Source,
                FileName = log.FileName,
                Username = log.Username,
                ProcessingTime = log.ProcessingTime,
                Status = log.Status,
                StatusMessage = log.Message,
                RowCount = log.RowCount
            };
        }

        private PagedList<EtlLogDataModel> ProcessRequest(IDataTablesRequest request, IQueryable<EtlLogDataModel> records)
        {
            // sort
            records = records.ApplySort(request);

            // page
            var paged = records.ApplyPagination(request).ToList();

            // return data table response
            var totalCount = records.Count();

            return new PagedList<EtlLogDataModel>(paged, totalCount);
        }

        public ManualUploadViewModel GetManualUploadViewModel()
        {
            var model = new ManualUploadViewModel();

            foreach (var config in ManualUploadViewModel.FileTypeConfigs)
            {
                model.FileTypes.Add(_manualEtlService.GetFileDetails(config));
            }

            return model;
        }

        public List<DropdownItemModel> GetEtlSourceDropdownList()
        {
            var logItems = _manualEtlService.GetEtlLogs();

            var result = logItems.Select(x =>
                new DropdownItemModel
                {
                    Text = x.Source,
                    Value = x.Source
                }
            );

            return result.Distinct().ToList();
        }

        #endregion Methods
    }
}