﻿using ProfitOptics.Framework.DataLayer.WorkWave;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
	public class GCLifecycleStatusType
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Order { get; set; }
        public int Code { get; set; }
        public int GCLifecycleStatusTypeCategoryId { get; set; }
        public string Origin { get; set; }
        public bool IsLoaner { get; set; }
        public bool? IsRequired { get; set; }

        public virtual GCLifecycleStatusTypeCategory GCLifecycleStatusTypeCategory { get; set; }
        public virtual ICollection<CTLocationTypeGCLifecycleStatusType> CTLocationTypeGCLifecycleStatusTypes { get; set; }
    }
}
