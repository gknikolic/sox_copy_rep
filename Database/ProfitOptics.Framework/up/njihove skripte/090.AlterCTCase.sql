USE [Sox]

BEGIN

 	ALTER TABLE [Sox].[dbo].[CTCase] DROP COLUMN CaseTime;
	ALTER TABLE [Sox].[dbo].[CTCase] ADD CaseTime INT NULL;
	
END
