﻿using System;

namespace ProfitOptics.Framework.DataLayer
{
    public partial class POHelpArticle
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string RelatedUrls { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Tags { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
