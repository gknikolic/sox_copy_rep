﻿using Microsoft.AspNetCore.Identity;

namespace ProfitOptics.Framework.Web.Framework.ServiceResults
{
    public class SignInActionResult : AuthenticationServiceResult
    {
        public SignInResult SignInResult { get; set; }
        public string ConfirmEmailUrl { get; set; }

        public SignInActionResult()
        {
        }

        public SignInActionResult(SignInResult result)
        {
            this.SignInResult = result;
            Status = AuthenticationServiceResultStatusCode.Success;
        }

        public SignInActionResult(SignInResult result, string confirmEmailUrl)
        {
            this.SignInResult = result;
            this.ConfirmEmailUrl = confirmEmailUrl;
        }
        
        public static SignInActionResult Failed = new SignInActionResult(SignInResult.Failed);
        public static SignInActionResult LockedOut = new SignInActionResult(SignInResult.LockedOut);
        public static SignInActionResult NotAllowed = new SignInActionResult(SignInResult.NotAllowed);
        public static SignInActionResult Success = new SignInActionResult(SignInResult.Success);
        public static SignInActionResult TwoFactorRequired = new SignInActionResult(SignInResult.TwoFactorRequired);

        public bool IsLockedOut() => this.SignInResult.IsLockedOut;
        public bool IsNotAllowed() => this.SignInResult.IsNotAllowed;
        public new bool Succeeded() => this.SignInResult.Succeeded;
        public bool RequiresTwoFactor() => this.SignInResult.RequiresTwoFactor;

    }
}
