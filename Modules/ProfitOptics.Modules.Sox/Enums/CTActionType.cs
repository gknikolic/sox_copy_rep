﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Enums
{
    public enum CTActionType
    {
        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -f "0" -a "1" 
        GetCTCases = 1,

        // censi_track_module -a "2"
        GetCTFacilities = 2,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "3" -k "1"
        GetCTContainerServices = 3,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "4" -f "10000000" -s "10000002"
        GetCTContainersByService = 4,

        // censi_track_module -a "5"
        GetCTProducts = 5,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "6"
        GetCTVendors = 6,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "7" -t "10000056"
        GetCTContainerItems = 7,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -f "0" -a "8" -o "11111111"
        GetCTCaseContainers = 8,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -f "0" -a "9" -h "1"
        // -h "1" -b "1"
        GetCTCasesContainersAndSubmitToWW = 9,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "10" -f "0" -s "10000069"
        GetCTContainerActualsByContainerType = 10,

        // censi_track_module -a "11"
        GetCTLocationTypes = 11,

        // censi_track_module -a "12"
        GetCTLocations = 12,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "13"
        GetContainerTypeActualHistory = 13,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "14"
        GetCTContainerTypeGraphic = 14,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "15"
        GetCTContainerTypeGraphicMedia = 15,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "16"
        GetCTBILoadResults = 16,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "17"
        SyncTimestamps = 17,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "18"
        GetContainerAssemblyMediaForActuals = 18,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -a "19"
        SyncLCForPastCases = 19,

        // censi_track_module -u "dragan" -p "Vested123!" -c "1906" -f "0" -a "20" 
        GetServerTime = 20,

        // censi_track_module -u "1" -p "1" -c "1906" -a "21"
        CreateGCImageThumbnails = 21,

        // censi_track_module -u "1" -p "1" -c "1906" -a "22"
        CompleteBrokenTrayHistory = 22,

        // censi_track_module -a "23"
        GetQualityFeed = 23,
    }

    public enum WWActionType
	{
        // workwave_module -a "1"
        SyncOrder = 1,

        // workwave_module -a "2"
        SyncRoutes = 2,
        // workwave_module -a "3"
        DeleteOldETLLogs = 3,
        // workwave_module -a "4"
        GetOrdersFromWW = 4,
        // workwave_module -a "5"
        GetLatestGPSDeviceSamples = 5,
        // workwave_module -a "6"
        SyncGPSDevices = 6
    }

    public enum IMActionType
    {
        // imonnit_module -a "1"
        SyncSensors = 1,
    }

    public enum ReportParsing
	{
        ParseBASReportFromEmail = 1
	}
}
