USE [ProfitOptics.Framework]
GO

/****** Object:  Table [dbo].[MarginBridge_SalesHistory]    Script Date: 23.9.2019. 12:23:45 ******/


 IF exists(SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MarginBridge_SalesHistory' AND TABLE_CATALOG = 'ProfitOptics.Framework')
 BEGIN
 Drop Table MarginBridge_SalesHistory
 END

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MarginBridge_SalesHistory](
	[SalesHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[LocationName] [varchar](255) NOT NULL,
	[ProductNumber] [varchar](255) NOT NULL,
	[InvoiceCost] [decimal](20, 4) NOT NULL,
	[InvoiceAmount] [decimal](20, 4) NOT NULL,
	[InvoiceDT] [datetime] NULL,
	[QuantityShipped] [decimal](20, 4) NOT NULL,
	[RebatedCost] [decimal](20, 4) NOT NULL,
	[InvoiceNumber] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[SalesHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

