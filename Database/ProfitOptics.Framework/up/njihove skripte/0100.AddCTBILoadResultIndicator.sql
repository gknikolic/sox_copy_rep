USE [Sox]

CREATE TABLE [dbo].[CTBILoadResultIndicator]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[CTBILoadResultId] INT NOT NULL,
	[LoadIndicatorId] BIGINT NOT NULL,
	[LoadId] BIGINT NOT NULL,
	[IndicatorType] INT NOT NULL,
	[IndicatorName] NVARCHAR(100) NULL,
	[PassLabel] NVARCHAR(20) NULL,
	[FailLabel] NVARCHAR(20) NULL,
	[IncubationFlag] NVARCHAR(20) NULL,
	[DisplayOrder] INT NOT NULL,
	[DefaultProduct] NVARCHAR(200) NULL,
	[SterilizerGroupId] NVARCHAR(100) NULL,
	[IndicatorProduct] NVARCHAR(200) NULL,
	[IndicatorLot] NVARCHAR(100) NULL,
	[ResultFlag] NVARCHAR(10) NULL,
	[InsertTimestamp] DATETIME NULL,
	[StartUser] NVARCHAR(100) NULL,
	[UpdateTimestamp] DATETIME NULL,
	[UserId] NVARCHAR(100) NULL,
	[Quantity] INT NULL,
	[Exclude] BIT NOT NULL,
	[CreatedAtUtc] DATETIME NOT NULL,
	[ModifiedAtUtc] DATETIME NULL
CONSTRAINT [PK_CTBILoadResultIndicator] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTBILoadResultIndicator]  WITH CHECK ADD  CONSTRAINT [FK_CTBILoadResultIndicator_CTBILoadResult] FOREIGN KEY([CTBILoadResultId])
REFERENCES [dbo].[CTBILoadResult] ([Id])
GO
ALTER TABLE [dbo].[CTBILoadResultIndicator] CHECK CONSTRAINT [FK_CTBILoadResultIndicator_CTBILoadResult]
GO