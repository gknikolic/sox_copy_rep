namespace ProfitOptics.Framework.Web.Framework.Identity.Domain
{
    public partial class POUserHierarchy
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }

        public int ChildId { get; set; }

        public virtual ApplicationUser ChildUser { get; set; }

        public virtual ApplicationUser ParentUser { get; set; }
    }
}