using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.Web.RestApi.DataLayer;
using ProfitOptics.Framework.Web.RestApi.Models;
using ProfitOptics.Framework.Web.RestApi.Models.Distributor;
using ProfitOptics.Framework.Web.RestApiExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.Services.Distributor
{
    public class DistributorService : IDistributorService
    {
        private readonly ApiEntitiesContext _context;

        public DistributorService(ApiEntitiesContext context)
        {
            _context = context;
        }

        public async Task<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor>> ListAsync(DistributorQuery query)
        {
            IQueryable<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor> queryable = _context.Distributors
                                                .Include(p => p.Customers)
                                                .Include(p => p.DistributorProducts)
                                                .AsNoTracking();

            if (query.CustomerId.HasValue && query.CustomerId > 0)
            {
                queryable = queryable.Where(p => p.Customers.Any(c => c.Id == query.CustomerId));
            }

            if (query.ProductId.HasValue && query.ProductId > 0)
            {
                queryable = queryable.Where(p => p.DistributorProducts.Any(ap => ap.ProductId == query.ProductId));
            }

            int totalItems = await queryable.CountAsync();

            List<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor> distributors = await queryable
                .ApplyFiltering(query)
                .ApplySort(query)
                .ApplyPagination(query)
                .ToListAsync();

            return new QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor>
            {
                Items = distributors,
                TotalItems = totalItems,
            };
        }

        public async Task<List<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor>> ListAsyncByIds(List<int> ids)
        {
            IQueryable<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor> queryable = _context.Distributors
                                                .Where(d => ids.Contains(d.Id))
                                                .AsNoTracking();

            List<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor> distributors = await queryable.ToListAsync();

            return distributors;
        }

        public async Task<DistributorResponse> SaveAsync(ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor distributor)
        {
            try
            {
                await _context.Distributors.AddAsync(distributor);
                await _context.SaveChangesAsync();

                return new DistributorResponse(distributor);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new DistributorResponse($"An error occurred when saving the distributor: {ex.Message}");
            }
        }

        public async Task<DistributorResponse> UpdateAsync(int id, ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor distributor)
        {
            var existingDistributor = await _context.Distributors.FindAsync(id);

            if (existingDistributor == null)
                return new DistributorResponse("Distributor not found.");

            existingDistributor.Name = distributor.Name;

            try
            {
                _context.Distributors.Update(existingDistributor);
                await _context.SaveChangesAsync();

                return new DistributorResponse(existingDistributor);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new DistributorResponse($"An error occurred when updating the distributor: {ex.Message}");
            }
        }

        public async Task<DistributorResponse> DeleteAsync(int id)
        {
            var existingDistributor = await _context.Distributors.FindAsync(id);

            if (existingDistributor == null)
                return new DistributorResponse("Distributor not found.");

            try
            {
                _context.Distributors.Remove(existingDistributor);
                await _context.SaveChangesAsync();

                return new DistributorResponse(existingDistributor);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new DistributorResponse($"An error occurred when deleting the distributor: {ex.Message}");
            }
        }
    }
}