﻿namespace ProfitOptics.Modules.Quotes.Models
{
    public class CustomerModel
    {
        public string CustId { get; set; }
        public string CustNo { get; set; }
        public string ShipToName { get; set; }
        public string ShipToAddress { get; set; }
        public string BillToAddress { get; set; }
        public int ShipToId { get; set; }
        public int? BillToId { get; set; }
        public string CustName { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
    }
}
