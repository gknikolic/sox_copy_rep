﻿(function (page, $, undefined) {
    page.table = (function () {
        //private
        var _editAnnouncementUrl = $("#editAnnouncementUrl").val();
        var _getAnnouncementsUrl = $("#getAnnouncementsUrl").val();
        var _announcementsFilter = $("#announcementsFilter").val();

        var _dataTable = null;
        var _columnsBase = [
            {
                data: "Title",
                orderable: true,
                className: "text-left",
                render: function (data, type, row, meta) {
                    return "<span class='datacell'>" + (data != null ? helpers.encodeHtml(data) : "") + "</span>";
                }
            }, {
                data: "Text",
                orderable: true,
                className: "text-left",
                render: function (data, type, row, meta) {
                    return "<span class='datacell'>" + helpers.decodeHtml(data) + "</span>";
                }
            }
        ];

        var showLoading = function (showLoading) {
            if (showLoading) {
                $(".table-overlay").show();
            } else {
                $(".table-overlay").hide();
            }

        };

        var getTextFromHtml = function (html) {
            var tmp = document.createElement("DIV");
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        };

        var prepareColumnsForAllAnnouncementsTable = function() {
            var columns = _columnsBase;
            columns.push({
                    data: "CreateTime",
                    orderable: true,
                    className: "text-left",
                    width: "120px",
                    render: function(data, type, row, meta) {
                        return "<span class='datacell'>" + row.CreateTimeString + "</span>";
                    }
                },
                {
                    data: "ValidUntil",
                    orderable: true,
                    className: "text-left",
                    width: "90px",
                    render: function (data, type, row, meta) {
                        return "<span class='datacell'>" + row.ValidUntilString + "</span>";
                    }
                },
                {
                    data: null,
                    orderable: false,
                    className: "text-center",
                    width: "70px",
                    render: function(data, type, row, meta) {
                            // Update Roles, Unlock, Login as User, Reset Password
                            return '<div class="btn-group btn-group-xs">' +
                                '    <a title="Edit" class="btn btn-default" href="' +
                                _editAnnouncementUrl +
                                '/' +
                                row.Id +
                                '"><span class="glyphicon glyphicon-pencil"></span></a>' +
                                '    <button title="Delete" type="button" class="btn btn-default" data-id="' +
                                row.Id +
                                '" onclick="page.deleteAnnouncement(' +
                                row.Id +
                                ');"><span class="glyphicon glyphicon-trash"></span></button>' +
                                '</div>';
                    }
                });
            return columns;
        };

        var prepareColumnsForUserAnnouncementsTable = function() {
            var columns = _columnsBase;
            columns.push({
                    data: "ValidUntil",
                    orderable: true,
                    className: "text-left",
                    width: "90px",
                    render: function(data, type, row, meta) {
                        return "<span class='datacell'>" + row.ValidUntilString + "</span>";
                    }
                },
                {
                    data: "CreateTime",
                    orderable: true,
                    className: "text-left",
                    width: "120px",
                    render: function(data, type, row, meta) {
                        return "<span class='datacell'>" + row.CreateTimeString + "</span>";
                    }
                },
                {
                    data: "CreatedBy",
                    orderable: true,
                    className: "text-left",
                    width: "120px",
                    render: function(data, type, row, meta) {
                        return "<span class='datacell'>" + helpers.encodeHtml(data) + "</span>";
                    }
                });
            return columns;
        };

        var initDataTable = function (dataTableSelector, ajaxUrl, columns) {
            var dataTableAjaxProperties = {
                url: ajaxUrl,
                type: "POST",
                data: {
                    currentUserOnly: _announcementsFilter == "UserSpecific"
                }
            };

            _dataTable = $(dataTableSelector).DataTable({
                order: [[2, "desc"]],
                lengthChange: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                searching: true,
                ordering: true,
                paging: true,
                info: true,
                pageLength: 10,
                lengthMenu: [3, 10, 25, 50, 100, 250, 500],
                ajax: dataTableAjaxProperties,
                language: {
                    processing: ""
                },
                columns: columns
            }).on("preXhr.dt",
                function(e, settings, data) {
                    //this is executed before ajax request 
                    showLoading(true);
                }).on("xhr.dt",
                function(e, settings, json, xhr) {
                    var searchDelay = null;

                    $("div.dataTables_filter input")
                        .off("keyup.DT input.DT")
                        .on("keyup.DT input.DT",
                            function() {
                                var search = $('div.dataTables_filter input').val();
                                clearTimeout(searchDelay);
                                searchDelay = setTimeout(function() {
                                        if (search != null) {
                                            _dataTable.search(search).draw();
                                        }
                                    },
                                    500);
                            });

                    showLoading(false);
                });
        };

        
        //public
        var initAnnouncementsTable = function () {
            
            var columns = _announcementsFilter == "All" ? prepareColumnsForAllAnnouncementsTable() : prepareColumnsForUserAnnouncementsTable();
            initDataTable("#announcements-table", _getAnnouncementsUrl, columns);

            page.deleteAnnouncement = function (id) {
                var data = {
                    Id: id
                };

                if (!confirm("Are you sure?")) {
                    return;
                }

                app.AjaxDelete(data).done(function (returnData) {
                    _dataTable.ajax.reload(null, true);
                });
            };
        }();

        return {
            initAnnouncementsTable: initAnnouncementsTable,
        };
    })();
})(window.page = window.page || {}, jQuery);