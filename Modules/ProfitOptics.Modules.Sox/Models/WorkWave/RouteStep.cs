﻿using Newtonsoft.Json;
using System.Reflection.Metadata;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class RouteStep
	{
		[JsonProperty("type")]
		//public RouteStepType Type { get; set; }
		public string Type { get; set; }
		[JsonProperty("orderId")]
		public string OrderId { get; set; }
		[JsonProperty("trackingData")]
		public TrackingData TrackingData { get; set; }
	}
}
