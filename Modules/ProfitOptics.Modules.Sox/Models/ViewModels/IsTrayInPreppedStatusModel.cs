﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class IsTrayInPreppedStatusModel
    {
        public int? TrayId { get; set; }
        public string Code { get; set; }
    }
}
