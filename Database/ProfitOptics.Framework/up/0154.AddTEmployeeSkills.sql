use [SoxQATest]

CREATE TABLE [dbo].[TEmployeeSkills]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[EmployeeId] INT NOT NULL,
	[SkillId] INT NOT NULL

	CONSTRAINT [PK_TEmloyeeSkills] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TEmployeeSkills]  WITH CHECK ADD  CONSTRAINT [FK_TEmployeeSkills_TEmployee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[TEmployee] ([Id])
GO
ALTER TABLE [dbo].[TEmployeeSkills]  CHECK CONSTRAINT [FK_TEmployeeSkills_TEmployee]
GO

ALTER TABLE [dbo].[TEmployeeSkills]  WITH CHECK ADD  CONSTRAINT [FK_TEmployeeSkills_TSkill] FOREIGN KEY([SkillId])
REFERENCES [dbo].[TSkill] ([Id])
GO
ALTER TABLE [dbo].[TEmployeeSkills]  CHECK CONSTRAINT [FK_TEmployeeSkills_TSkill]
GO