﻿using System;

namespace ProfitOptics.Modules.Reporting.Extensions
{
    public static class DoubleExtensions
    {
        public static double Power(this double one, double two)
        {
            return Math.Pow(one, two);
        }
    }
}