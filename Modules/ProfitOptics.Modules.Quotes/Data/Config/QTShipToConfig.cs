﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTShipToConfig : IEntityTypeConfiguration<QTShipTo>
    {
        public void Configure(EntityTypeBuilder<QTShipTo> builder)
        {
            builder.ToTable("QTShipTo");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Company).HasMaxLength(50);
            builder.Property(entity => entity.ShipToNum).HasMaxLength(50);
            builder.Property(entity => entity.ShipToName).HasMaxLength(50);
            builder.Property(entity => entity.BillToName).HasMaxLength(50);
            builder.Property(entity => entity.Address1).HasMaxLength(50);
            builder.Property(entity => entity.Address2).HasMaxLength(50);
            builder.Property(entity => entity.Address3).HasMaxLength(50);
            builder.Property(entity => entity.City).HasMaxLength(50);
            builder.Property(entity => entity.State).HasMaxLength(50);
            builder.Property(entity => entity.Zip).HasMaxLength(50);
            builder.Property(entity => entity.SalesRepCode).HasMaxLength(50);

        }
    }
}
