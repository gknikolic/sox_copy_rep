USE [Sox]

BEGIN
	ALTER TABLE [dbo].[CTContainerType] DROP COLUMN ProcurementReferenceId;
	ALTER TABLE [dbo].[CTContainerType] ADD ProcurementReferenceId NVARCHAR(100) NULL;
END
