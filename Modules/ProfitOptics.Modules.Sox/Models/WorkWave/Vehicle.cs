﻿using Newtonsoft.Json;
using System;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class Vehicle
	{
		[JsonProperty("id")]
		public string Id { get; set; }
		[JsonProperty("externalId")]
		public string ExternalId { get; set; }
		[JsonProperty("settings")]
		public VehicleSettings Settings { get; set; }
		[JsonProperty("gpsDeviceId")]
		public string GPSDeviceId { get; set; }
	}
}
