﻿using Microsoft.AspNetCore.Mvc;

namespace ProfitOptics.Framework.Web.Controllers
{
    public class SessionController : Controller
    {
        [HttpPost]
        public IActionResult RefreshSession()
        {
            return Json(new
            {
                Status = "OK"
            });
        }
    }
}
