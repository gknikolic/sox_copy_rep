﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class EEPodBarcodes
	{
		[JsonProperty("sec")]
		public int Sec { get; set; }

		[JsonProperty("barcode")]
		public string Barcode { get; set; }
	
		public BarcodeStatusType BarcodeStatusType { get; set; }
		[JsonProperty("barcodeStatus")]
		public string BarcodeStatus { get { return BarcodeStatusType.ToString(); } }
	}


}
