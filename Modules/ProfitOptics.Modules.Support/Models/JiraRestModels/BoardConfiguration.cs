﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class BoardConfiguration
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
   
        public string Self { get; set; }

        public BoardColumnConfig ColumnConfig { get; set; }

        public RankingInfo Ranking { get; set; }
    }
}