﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Helpers.HtmlHelpers;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.Announcements.Factories;
using ProfitOptics.Modules.Announcements.Models;

namespace ProfitOptics.Modules.Announcements.Components
{
    public class AnnouncementViewComponent : ViewComponent, IHeaderWidget
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IAnnouncementModelFactory _announcementModelFactory;
        private readonly IMemoryCacheHelper _memoryCacheHelper;

        public AnnouncementViewComponent(IHttpContextAccessor contextAccessor,
            IAnnouncementModelFactory announcementModelFactory,
            IMemoryCacheHelper memoryCacheHelper)
        {
            _contextAccessor = contextAccessor;
            _announcementModelFactory = announcementModelFactory;
            _memoryCacheHelper = memoryCacheHelper;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (_contextAccessor.HttpContext.User?.Identity?.IsAuthenticated ?? false)
            {
                await ReloadAnnouncementsAsync();

                var model =
                    _contextAccessor.HttpContext.Session.GetObject<List<AnnouncementNotificationModel>>(
                        SessionKeys.AnnouncementList);

                return View(PartialViewHelper.BuildModulePartialViewPath(Assembly.GetExecutingAssembly(), "Views/Shared/Components/Announcement/Default.cshtml"), model);
            }

            return Content(string.Empty);
        }

        private async Task ReloadAnnouncementsAsync()
        {
            var announcementUpdateTime = _memoryCacheHelper.Get(SessionKeys.AnnouncementUpdateTime.ToString(), () => DateTime.MaxValue, 30);
           
            DateTime announcementReloadTime = DateTime.UtcNow;
            if (_contextAccessor.HttpContext.Session.GetObject<DateTime>(SessionKeys.AnnouncementReloadTime) != default(DateTime))
            {
                announcementReloadTime = Convert.ToDateTime(_contextAccessor.HttpContext.Session.GetObject<DateTime>(SessionKeys.AnnouncementReloadTime));
            }

            if (!(_contextAccessor.HttpContext.Session.GetObject<List<AnnouncementNotificationModel>>(SessionKeys.AnnouncementList)?.Any() ?? false)
                || announcementUpdateTime > announcementReloadTime)
            {
                var userAnnouncements = await _announcementModelFactory.PrepareActiveAnnouncementModelsForUserIdAsync(_contextAccessor.HttpContext.User.GetUserId());

                _contextAccessor.HttpContext.Session.SetObject(SessionKeys.AnnouncementList, userAnnouncements);

                _contextAccessor.HttpContext.Session.SetObject(SessionKeys.AnnouncementReloadTime, DateTime.UtcNow);
            }
        }

        public int OrderNumber() => 1;
    }
}