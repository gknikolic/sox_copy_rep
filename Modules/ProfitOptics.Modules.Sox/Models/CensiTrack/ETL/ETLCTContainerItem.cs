﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTContainerItem
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("requiredCount")]
        public int RequiredCount { get; set; }

        [JsonProperty("productId")]
        public decimal ProductId { get; set; }

        [JsonProperty("product")]
        public ETLCTProduct Product { get; set; }

        [JsonProperty("placement")]
        public string Placement { get; set; }

        [JsonProperty("substitutionsAllowed")]
        public bool SubstitutionsAllowed { get; set; }

        [JsonProperty("criticalItem")]
        public bool CriticalItem { get; set; }

        [JsonProperty("changeState")]
        public string ChangeState { get; set; }

        [JsonProperty("sequenceNumber")]
        public int SequenceNumber { get; set; }

    }
}
