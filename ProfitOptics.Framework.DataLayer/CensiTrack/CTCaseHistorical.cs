﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTCaseHistorical
    {
        public int Id { get; set; }
        public int CTCaseId { get; set; }
        public string CaseReference { get; set; }
        public DateTime DueTimestamp { get; set; }
        public string DestinationName { get; set; }
        public string CaseDoctor { get; set; } //Surgeon
        public long? CaseCartId { get; set; }
        public int? CaseTime { get; set; }
        public int? CaseStatus { get; set; }
        public bool? IsCartComplete { get; set; }
        public string CartStatus { get; set; }
        public long? CartStatusId { get; set; }
        public string CaseCartName { get; set; }
        public string CaseCartLocationName { get; set; }
        public bool? IsCanceled { get; set; }
        public string Shipped { get; set; }
        public DateTime? PreviousDueTimestamp { get; set; }
        public string PreviousDestinationName { get; set; }
        public int? ScheduleId { get; set; }
        public DateTime? ExpediteTime { get; set; }
        public decimal? DueInMinutes { get; set; }
        public string DueInHoursAndMinutes { get; set; }
        public decimal? PreviousDueInMinutes { get; set; }
        public string PreviousDueInHoursAndMinutes { get; set; }
        public string Changed { get; set; }
        public bool? Priority { get; set; }
        public DateTime? DateTimeNow { get; set; }
        public string EstimatedTime { get; set; }
        public long? DestinationFacilityId { get; set; }
        public DateTime CreatedAtUtc { get; set; }
    }
}
