using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTShipToItemSummary")]
    public partial class QTShipToItemSummary
    {
        public int CustomerBillToId { get; set; }

        public int? CustomerShipToId { get; set; }

        public int ItemId { get; set; }

        public string Company { get; set; }

        public string BillToNum { get; set; }

        public string ShipToNum { get; set; }

        public string ItemNum { get; set; }

        public DateTime? LastDate { get; set; }

        public decimal? LastPrice { get; set; }

        public decimal? LastCost { get; set; }

        public decimal? LastGPPct { get; set; }

        public string LastSource { get; set; }

        public int? Frequency3Mo { get; set; }

        public decimal? Sales3Mo { get; set; }

        public decimal? Cost3Mo { get; set; }

        public int? Frequency12Mo { get; set; }

        public decimal? Sales12Mo { get; set; }

        public decimal? Cost12Mo { get; set; }
    }
}
