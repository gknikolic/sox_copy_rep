﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayAssemblyParts
	{
		public List<TrayPart> TrayParts { get; set; }
		public DateTime StatusTimestamp { get; set; }
		public string CaseReference { get; set; }
	
	}
}
