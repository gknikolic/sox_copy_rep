﻿using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.DataBrowser.Models;

namespace ProfitOptics.Modules.DataBrowser.Factories
{
    public interface IDataBrowserModelFactory
    {
        /// <summary>
        /// Creates a new DataBrowserViewModel object.
        /// </summary>
        /// <returns>DataBrowserViewModel</returns>
        DataBrowserViewModel PrepareDataBrowserViewModel();

        /// <summary>
        /// Returns a list of all tables for a given database name.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="searchQuery">Search query for filtering out tables.</param>
        /// <returns>List of Select2Item object.</returns>
        IEnumerable<TableModel> PrepareTables(string databaseName, string searchQuery);

        /// <summary>
        /// Creates a new FilterViewModel object.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <returns>FilterViewModel object.</returns>
        FilterViewModel PrepareFilterViewModel(string databaseName, string tableName);

        /// <summary>
        /// Returns all columns for a given database and table name.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <returns>List of all columns, their names and types.</returns>
        IEnumerable<ColumnModel> PrepareTableDataColumns(string databaseName, string tableName);

        /// <summary>
        /// Returns table data for the given table query model
        /// </summary>
        /// <param name="request">Data tabels request</param>
        /// <param name="tableQueryModel">Table query model</param>
        /// <returns></returns>
        DataBrowserPaginationResult PrepareTableData(IDataTablesRequest request, TableQueryModel tableQueryModel);

        /// <summary>
        /// Builds and executes a where statement for the given table query model
        /// </summary>
        /// <param name="tableQueryModel">Table query model</param>
        /// <returns></returns>
        DataBrowserResult PrepareWhereStatement(TableQueryModel tableQueryModel);

        /// <summary>
        /// Prepares DataBrowserDownloadModel 
        /// </summary>
        /// <param name="tableQueryModel"></param>
        /// <returns></returns>
        DataBrowserDownloadModel PrepareDataBrowserDownloadModel(TableQueryModel tableQueryModel);
    }
}
