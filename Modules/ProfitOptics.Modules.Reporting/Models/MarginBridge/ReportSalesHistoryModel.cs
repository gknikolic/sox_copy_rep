﻿using ProfitOptics.Modules.Reporting.Attributes;
using ProfitOptics.Modules.Reporting.Data.Domain;
using System;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class ReportSalesHistoryModel
    {
        public int Id { get; set; }

        [FieldTitle("InvoiceDate")]
        public DateTime InvoiceDate { get; set; }

        [FieldTitle("ZoneId")]
        public int? ZoneId { get; set; }

        [FieldTitle("RegionId")]
        public int? RegionId { get; set; }

        [FieldTitle("SalesRepId")]
        public int? SalesRepId { get; set; }

        [FieldTitle("BillToId")]
        public int BillToId { get; set; }

        [FieldTitle("ShipToId")]
        public int ShipToId { get; set; }

        [FieldTitle("VendorId")]
        public int? VendorId { get; set; }

        [FieldTitle("ItemId")]
        public int ItemId { get; set; }

        [FieldTitle("UnitPrice")]
        public decimal? UnitPrice { get; set; }

        [FieldTitle("UnitCost")]
        public decimal? UnitCost { get; set; }

        [FieldTitle("ExtPrice")]
        public decimal ExtPrice { get; set; }

        [FieldTitle("ExtCost")]
        public decimal ExtCost { get; set; }

        [FieldTitle("Qty")]
        public decimal Qty { get; set; }

        [FieldTitle("BuyGroupId")]
        public int? BuyGroupId { get; set; }

        [FieldTitle("CustomerClassId")]
        public int? CustomerClassId { get; set; }

        [FieldTitle("ItemCategoryId")]
        public int? ItemCategoryId { get; set; }

        [FieldTitle("StartDate")]
        public DateTime StartDate { get; set; }

        [FieldTitle("EndDate")]
        public DateTime? EndDate { get; set; }

        [FieldTitle("InvoiceYearMonth")]
        public int? InvoiceYearMonth { get; set; }

        [FieldTitle("SOPType")]
        public string SOPType { get; set; }

        [FieldTitle("SOPNumber")]
        public string SOPNumber { get; set; }

        [FieldTitle("RebateRate")]
        public decimal RebateRate { get; set; }

        [FieldTitle("Comment1")]
        public string Comment1 { get; set; }

        [FieldTitle("Comment2")]
        public string Comment2 { get; set; }

        [FieldTitle("Comment3")]
        public string Comment3 { get; set; }

        [FieldTitle("Comment4")]
        public string Comment4 { get; set; }

        [FieldTitle("ShipToName")]
        public string ShipTo { get; set; }

        [FieldTitle("BillToName")]
        public string BillTo { get; set; }

        [FieldTitle("Territory")]
        public string Territory { get; set; }

        [FieldTitle("Region")]
        public string Region { get; set; }

        [FieldTitle("Zone")]
        public string Zone { get; set; }

        [FieldTitle("Item")]
        public string Item { get; set; }

        [FieldTitle("ItemCategory")]
        public string ItemCategory { get; set; }
    }
}