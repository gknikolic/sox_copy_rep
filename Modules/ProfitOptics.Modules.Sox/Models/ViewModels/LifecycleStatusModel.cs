﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class LifecycleStatusModel
    {
		public int Id { get; set; }
		public string Title { get; set; }
        public int Order { get; set; }
        public DateTime Timestamp { get; set; }

		public bool ShowTimestamp { get; set; }
		public int NumberOfPassedTrays { get; set; }
        public int NumberOfActiveTrays { get; set; }
        public int NumberOfTraysInCase { get; set; }
        public int NumberOfTraysInStatus { get; set; }
        public List<TrayModel> Trays { get; set; }
		public List<TrayLifecycleStatus> TrayLifecycleStatuses { get; set; }
	}
}
