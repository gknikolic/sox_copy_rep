﻿using System;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class StandingOrdersModel
    {
        public int CustNum { get; set; }
        public int OrderNum { get; set; }
        public int OrderLineNum { get; set; }
        public string ItemNum { get; set; }
        public decimal OrderQty { get; set; }
        public string StandingOrderCode { get; set; }
        public string OrderPattern { get; set; }
        public string OrderWeek { get; set; }
        public DateTime? OrderStartDate { get; set; }
        public DateTime? OrderEndDate { get; set; }
        public string ShipToNum { get; set; }
        public string ShipToName { get; set; }
        public bool OpenOrder { get; set; }
        public bool OrderHeld { get; set; }
        public string PoNumber { get; set; }
        public DateTime? RequestDate { get; set; }
    }
}