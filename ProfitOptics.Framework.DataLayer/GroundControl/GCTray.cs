﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCTray
    {
        public int Id { get; set; }
        public int? CTContainerTypeId { get; set; }
        public int? GCCaseId { get; set; }
        public int? CTCaseContainerId { get; set; }
        public int? CTContainerTypeActualId { get; set; }
        public string Barcode { get; set; }
        public bool? IsActiveStatus { get; set; }
        public bool? IsLoaner { get; set; }
        public int? GCLifecycleStatusTypeId { get; set; }
        public bool? IsActualCurrentlyAvailable { get; set; }
        public bool? IsTrayHistoryComplete { get; set; }
        public int? ActualLapCount { get; set; }

        public CTContainerType CTContainerType { get; set; }
        public GCCase GCCase { get; set; }
        public virtual CTCaseContainer CTCaseContainer { get; set; }
        public virtual CTContainerTypeActual CTContainerTypeActual { get; set; }
        public virtual ICollection<GCTraysGCRequiredActionTypes> GCTrayGCRequiredActionTypes { get; set; }
        public virtual ICollection<GCTrayColorStatus> GCTrayColorStatuses { get; set; }
        public virtual ICollection<GCTraysLifecycleStatusTypes> GCTraysLifecycleStatusTypes { get; set; }
        public virtual GCLifecycleStatusType GCLifecycleStatusType { get; set; }
    }
}
