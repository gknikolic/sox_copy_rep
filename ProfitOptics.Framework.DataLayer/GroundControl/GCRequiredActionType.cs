﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCRequiredActionType
    {
        public int Id { get; set; }
        public string Enumerator { get; set; }
        public string Caption { get; set; }
        public int Sequence { get; set; }
        public int Priority { get; set; }
    }
}
