﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.Collections.Generic;
using System.Globalization;

namespace ProfitOptics.Modules.Announcements.Models
{
    public class AnnouncementModel
    {
        public AnnouncementModel()
        {
            AllRoles = new List<RoleModel>();
            RoleIds = new int[] { };
        }

        public int? Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }

        public DateTime? ValidUntil { get; set; }
        public string ValidUntilString => ValidUntil.HasValue
            ? ValidUntil.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)
            : "";

        public DateTime? CreateTime { get; set; }
        public string CreateTimeString => CreateTime.HasValue
            ? CreateTime.Value.ToString("MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture)
            : "";

        public bool? IsDeleted { get; set; }
        public List<RoleModel> AllRoles { get; set; }
        public IEnumerable<int> RoleIds { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CreatedBy => $"{FirstName} {LastName}";
        public int CreatedById { get; set; }
    }
}