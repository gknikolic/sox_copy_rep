﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class StandingOrderListModel
    {
        public int? QuoteId { get; set; }

        public IEnumerable<SelectListItem> StandingOrderList { get; set; }
    }
}