﻿using System.Linq;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Services.RsmPerformance
{
    public interface IRsmSalesAndQuotaService
    {
        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesChannelQuota"/> entity, grouped by SalesChannelId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesChannelQuota"/> entity, grouped by SalesChannelId.</returns>
        IQueryable<IGrouping<int, ReportCustomerClassQuota>> GetSalesChannelQuotaGroupedBySalesChannelId(int year = 0, int salesRepId = 0);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesChannelQuota"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesChannelQuota"/> entity, grouped by SalesRepId.</returns>
        IQueryable<IGrouping<int, ReportCustomerClassQuota>> GetSalesChannelQuotaGroupedBySalesRepId(int year = 0);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportProductFamilyQuota"/> entity, grouped by ProductFamilyId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportProductFamilyQuota"/> entity, grouped by ProductFamilyId.</returns>
        IQueryable<IGrouping<int, ReportVendorQuota>>
            GetProductFamilyQuotaGroupedByProductFamilyId(int year = 0, int salesRepId = 0);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportProductFamilyQuota"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportProductFamilyQuota"/> entity, grouped by SalesRepId.</returns>
        IQueryable<IGrouping<int, ReportVendorQuota>> GetProductFamilyQuotaGroupedBySalesRepId(int year = 0);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportItemCategoryQuota"/> entity, grouped by ItemCategoryId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportItemCategoryQuota"/> entity, grouped by ItemCategoryId.</returns>
        IQueryable<IGrouping<int, ReportItemCategoryQuota>>
            GetItemCategoryQuotaGroupedByItemCategoryId(int year = 0, int salesRepId = 0);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportItemCategoryQuota"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportItemCategoryQuota"/> entity, grouped by SalesRepId.</returns>
        IQueryable<IGrouping<int, ReportItemCategoryQuota>>
            GetItemCategoryQuotaGroupedBySalesRepId(int year = 0);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportRegionQuota"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportRegionQuota"/> entity, grouped by SalesRepId.</returns>
        IQueryable<IGrouping<int, ReportRegionQuota>>
            GetRegionQuotaGroupedBySalesRepId(int year = 0);

        bool QuotaExistsForYearAndMonth(int year, int month);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <param name="zoneId">The zone id filter.</param>
        /// <param name="regionId">The region id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesRepId.</returns>
        IQueryable<IGrouping<int, ReportSalesHistory>> GetSalesGroupedBySalesRepId(int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null, int? regionId = null);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesChannelId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <param name="zoneId">The zone id filter.</param>
        /// <param name="regionId">The region id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesChannelId.</returns>
        IQueryable<IGrouping<int, ReportSalesHistory>> GetSalesGroupedBySalesChannelId(int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null, int? regionId = null);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by ProductFamilyId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <param name="zoneId">The zone id filter.</param>
        /// <param name="regionId">The region id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by ProductFamilyId.</returns>
        IQueryable<IGrouping<int, ReportSalesHistory>> GetSalesGroupedByProductFamilyId(
            int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null, int? regionId = null);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by ItemCategoryId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep id filter.</param>
        /// <param name="zoneId">The zone id filter.</param>
        /// <param name="regionId">The region id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by ItemCategoryId.</returns>
        IQueryable<IGrouping<int, ReportSalesHistory>> GetSalesGroupedByItemCategoryId(
            int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null, int? regionId = null);

        /// <summary>
        /// Returns an <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesRepId.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="zoneId">The zone id filter.</param>
        /// <returns>The <see cref="IQueryable{T}"/> for the <see cref="ReportSalesHistory"/> entity, grouped by SalesRepId.</returns>
        IQueryable<IGrouping<int, ReportSalesHistory>> GetSalesGroupedBySalesRepIdYearOverYear(int year = 0, int month = 0, int? zoneId = null);
    }
}