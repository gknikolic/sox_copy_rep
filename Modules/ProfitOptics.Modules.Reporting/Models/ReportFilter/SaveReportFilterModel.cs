﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Reporting.Models.ReportFilter
{
    public class SaveReportFilterModel : ReportFilterModel
    {
        [JsonIgnore]
        public string Name { get; set; }
    }
}
