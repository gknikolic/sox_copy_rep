﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer.Test;
using ProfitOptics.Modules.Employee.Data;
using ProfitOptics.Modules.Employee.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace ProfitOptics.Modules.Employee.Services
{
    public class EmployeeService : IEmployeeService
    {
        #region Fields

        private readonly ProfitOptics.Framework.DataLayer.Entities _context;

        #endregion Fields

        #region Ctor

        public EmployeeService(ProfitOptics.Framework.DataLayer.Entities context)
        {
            _context = context;
        }



        #endregion Ctor

        #region Methods

        public async Task<List<TManager>> GetAllManagersAsync()
        {
            return await _context.TManagers
                .Include(x => x.Employees)
                .ThenInclude(x => x.EmployeeSkills)
                .ThenInclude(x => x.Skill)
                .ToListAsync();
        }

        public async Task<List<TSkill>> GetAllSkillsAsync()
        {
            return await _context.TSkills
                .Include(x => x.EmployeeSkills)
                .ThenInclude(x => x.Employee)
                .ToListAsync();
        }
        public async Task<TEmployee> GetEmployeeByIdAsync(int id)
        {
            return await _context.TEmployees
                   .Where(x => x.Id == id)
                   .Include(x => x.Manager)
                   .Include(x => x.EmployeeSkills)
                   .ThenInclude(x => x.Skill)
                   .FirstOrDefaultAsync();
        }

        public async Task UpdateEmployeeAsync(EmployeeModel model)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                //var employee = await GetEmployeeByIdAsync(model.Id);
                var employee = (TEmployee)_context.TEmployees.Where(x => x.Id == model.Id).FirstOrDefault();
                try
                {
                    employee.FirstName = model.FirstName;
                    employee.LastName = model.LastName;
                    employee.ManagerId = model.ManagerId;
                    employee.EmployeeSkills.Clear();
                    _context.TEmployees.Attach(employee);
                    await _context.SaveChangesAsync(); //da bi moga da izbrise veze koje se skinu iz select liste
                    foreach (var skillId in model.EmployeeSkillIds)
                    {
                        employee.EmployeeSkills.Add(new TEmployeeSkills
                        {
                            EmployeeId = model.Id,
                            SkillId = skillId
                        });
                    }
                    _context.TEmployees.Attach(employee);
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (System.Exception e)
                {
                    transaction.Rollback();
                }
            }

        }

        //public async Task DeleteEmployeeAsync(EmployeeModel model)
        //{
        //    //var employee = await GetEmployeeByIdAsync(model.Id);
        //    var employee = (TEmployee)_context.TEmployees.Where(x => x.Id == model.Id).FirstOrDefault();
        //    _context.TEmployees.Remove(employee);
        //    await _context.SaveChangesAsync();
        //}

        public async Task DeleteEmployeeAsync(EmployeeModel model)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                //var employee = await GetEmployeeByIdAsync(model.Id);
                var employee = _context.TEmployees.Where(x => x.Id == model.Id).FirstOrDefault();
                try
                {
                    foreach (var employeeSkill in _context.TEmployeeSkills.Where(x => x.EmployeeId == model.Id).ToList())
                    {
                        _context.TEmployeeSkills.Remove(employeeSkill);
                    }
                    _context.TEmployees.Remove(employee);
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (System.Exception e)
                {
                    transaction.Rollback();
                }
            }
        }

        public async Task<List<TEmployee>> GetAllEmployeesAsync()
        {
            return await _context.TEmployees
                .Include(x => x.Manager)
                .Include(x => x.EmployeeSkills)
                .ThenInclude(x => x.Skill)
                .ToListAsync();
        }

        public async Task AddEmployeeAsync(EmployeeModel model)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                var employee = new TEmployee();
                try
                {
                    employee.FirstName = model.FirstName;
                    employee.LastName = model.LastName;
                    employee.ManagerId = model.ManagerId;

                    await _context.TEmployees.AddAsync(employee);
                    await _context.SaveChangesAsync();

                    employee.EmployeeSkills.Clear();
                    foreach (var skillId in model.EmployeeSkillIds)
                    {
                        employee.EmployeeSkills.Add(new TEmployeeSkills
                        {
                            EmployeeId = employee.Id,
                            SkillId = skillId
                        });
                    }
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (System.Exception e)
                {
                    transaction.Rollback();
                }
            }
        }

        public async Task<bool> AddManagerAsync(ManagerModel model)
        {
            try
            {
                TManager manager = new TManager
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Title = model.Title
                };
                await _context.TManagers.AddAsync(manager);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (System.Exception e)
            {
                return false;
            }

        }
        //public async Task<bool> AddManagerAsync(string firstName, string lastName, string title)
        //{
        //    try
        //    {
        //        TManager manager = new TManager
        //        {
        //            FirstName = firstName,
        //            LastName = lastName,
        //            Title = title
        //        };
        //        await _context.TManagers.AddAsync(manager);
        //        await _context.SaveChangesAsync();
        //        return true;
        //    } catch(System.Exception e)
        //    {
        //        return false;
        //    }

        //}


        #endregion
    }
}