﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
	public class LifecycleStatusTypeCategoryConfig : IEntityTypeConfiguration<GCLifecycleStatusTypeCategory>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCLifecycleStatusTypeCategory> builder)
        {
            builder.ToTable("GCLifecycleStatusTypeCategory");
            builder.HasKey(entity => entity.Id);
        }
    }
}
