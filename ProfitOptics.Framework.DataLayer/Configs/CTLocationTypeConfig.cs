﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTLocationTypeConfig : IEntityTypeConfiguration<CTLocationType>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTLocationType> builder)
        {
            builder.ToTable("CTLocationType");
            builder.HasKey(e => e.Id);
        }
    }
}
