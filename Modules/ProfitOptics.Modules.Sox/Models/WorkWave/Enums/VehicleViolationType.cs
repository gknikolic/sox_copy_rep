﻿namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public enum VehicleViolationType
	{
		LD, //One or more maximum load allowances have been exceeded
		TWS, // Route starts before the working time window opens
		TWE, // Route ends after the working time window closes
		DT, // Maximum driving time exceeded
		WT, // Maximum working time exceeded
		ML, //Maximum drive distance exceeded
		OR //Maximum number of serviced Orders exceeded
	}
}
