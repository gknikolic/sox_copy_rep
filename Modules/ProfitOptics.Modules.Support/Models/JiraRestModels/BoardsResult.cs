﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class BoardsResult
    {
        public int? StartAt { get; set; }

        public int? MaxResults { get; set; }

        public int? Total { get; set; }

        public bool? IsLast { get; set; }

        public List<string> ErrorMessages { get; set; }

        public List<BoardDetails> Values { get; set; }
    }
}