USE [Sox]

BEGIN

ALTER TABLE [dbo].[GCLocationInfos] ALTER  COLUMN [Temperature] float(5);
ALTER TABLE [dbo].[GCLocationInfos] ALTER  COLUMN [Humidity] float(5);
ALTER TABLE [dbo].[GCLocationInfos] ALTER  COLUMN [ACPH] float(5);
ALTER TABLE [dbo].[GCLocationInfos] ALTER  COLUMN [Pressure] float(5);

END