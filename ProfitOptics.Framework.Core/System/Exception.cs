﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Core.System
{
    public class ProfitOpticsException:Exception
    {
        public ProfitOpticsException(string message) : base(message) { }
    }
}
