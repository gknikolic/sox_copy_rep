﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCTrayActualHistory
    {
        public int Id { get; set; }
        public int GCTrayId { get; set; }
        public int CTContainerTypeActualId { get; set; }
        public DateTime CreatedAtUtc { get; set; }
    }
}
