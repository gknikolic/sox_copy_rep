﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTBILoadResultGraphicConfig : IEntityTypeConfiguration<CTBILoadResultGraphic>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTBILoadResultGraphic> builder)
        {
            builder.ToTable("CTBILoadResultGraphic");
            builder.HasKey(entity => entity.Id);
        }
    }
}
