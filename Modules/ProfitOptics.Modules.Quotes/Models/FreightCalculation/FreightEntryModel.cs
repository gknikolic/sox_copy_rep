﻿namespace ProfitOptics.Modules.Quotes.Models.FreightCalculation
{
    public class FreightEntryModel
    {
        public decimal Class { get; set; }
        public decimal WeightInPounds { get; set; }
    }
}