﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;

namespace ProfitOptics.Modules.Sox.Helpers
{
    public static class Helpers
    {
        private const int CENTRAL_TIMEZONE_OFFSET_IN_MINS = -360;
        private const int CENTRAL_TIMEZONE_OFFSET_IN_MINS_SUMMER_TIME = -300;

        public static dynamic ToDynamic(this object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return JsonConvert.DeserializeObject(json, typeof(ExpandoObject));
        }

        public static DateTime GetLocalDateTime(this DateTime datetime)
        {
            TimeZoneInfo chicagoZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            var res = TimeZoneInfo.ConvertTime(datetime, chicagoZone);
            return res;
            //return datetime.AddMinutes(CENTRAL_TIMEZONE_OFFSET_IN_MINS);
        }

        public static string GetLocalDateTimeString(this DateTime? datetime)
        {
            if(datetime!=null)
			{
                TimeZoneInfo chicagoZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                var res = TimeZoneInfo.ConvertTime((DateTime)datetime, chicagoZone);
                return res.ToString();
                //return ((DateTime)datetime).AddMinutes(CENTRAL_TIMEZONE_OFFSET_IN_MINS).ToString();
            }
            else
			{
                return String.Empty;
			}
            
        }

        private static T GetAttribute<T>(this Enum value) where T : Attribute
        {
            var type = value.GetType();
            var memberInfo = type.GetMember(value.ToString());
            var attributes = memberInfo[0].GetCustomAttributes(typeof(T), false);
            return attributes.Length > 0
              ? (T)attributes[0]
              : null;
        }

        public static string GetDescription(this Enum value)
        {
            var attribute = value.GetAttribute<DescriptionAttribute>();
            return attribute == null ? value.ToString() : attribute.Description;
        }

        public static string GetLocalDateTimeString(this DateTime datetime)
        {
            return $"{datetime.GetLocalDateTime()} (CT)";
        }

        public static bool IsETLCycleExceeded(DateTime lastCycleTime)
        {
            int etlCycleInMins = 5;
            return lastCycleTime.AddMinutes(etlCycleInMins) < DateTime.UtcNow;
        }

        public static int GetServerOffsetInMinsFromUTC(int serverOffsetInMinsFromUTC)
        {
            var result = DateTime.Now.IsDaylightSavingTime() ? (serverOffsetInMinsFromUTC + 60) : serverOffsetInMinsFromUTC;
            return result * -1;
        }

        public static bool CurrentStatusIsInduction(int lifeCycleStatisId)
        {
            return
                lifeCycleStatisId == ((int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.DeconStaged) ||
                lifeCycleStatisId == ((int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Sink) ||
                lifeCycleStatisId == ((int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Washer) ||
                lifeCycleStatisId == ((int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.AssemblyStaged) ||
                lifeCycleStatisId == ((int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly) ||
                lifeCycleStatisId == ((int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.CleanStaged) ||
                lifeCycleStatisId == ((int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.QualityHold);
        }

        public static bool DoesGCTrayHaveAllCTStatuses(GCTray gcTray)
        {
            var result = true;

            if(gcTray != null && gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
            {
                var ids = LifeCycleCTIds(gcTray.IsLoaner);
                foreach (var id in ids)
                {
                    if(!gcTray.GCTraysLifecycleStatusTypes.Any(x => x.LifecycleStatusTypeId == id))
                    {
                        return false;
                    }
                }
            }

            return result;
        }

        public static List<int> LifeCycleCTIds(bool? isLoaner = false)
        {
            var ids = new List<int>();

            if(isLoaner.HasValue && isLoaner.Value)
            {
                ids.Add((int)LifecycleStatusType.DeconStagedLoaner);
                ids.Add((int)LifecycleStatusType.SinkLoaner);
                ids.Add((int)LifecycleStatusType.WasherLoaner);
                ids.Add((int)LifecycleStatusType.AssemblyStagedLoaner);
                ids.Add((int)LifecycleStatusType.AssemblyLoaner);
            }

            ids.Add((int)LifecycleStatusType.DeconStaged);
            ids.Add((int)LifecycleStatusType.Sink);
            ids.Add((int)LifecycleStatusType.Washer);
            ids.Add((int)LifecycleStatusType.AssemblyStaged);
            ids.Add((int)LifecycleStatusType.Assembly);
            //ids.Add((int)LifecycleStatusType.Quarantine);
            ids.Add((int)LifecycleStatusType.CleanStaged);
            ids.Add((int)LifecycleStatusType.Verification);
            ids.Add((int)LifecycleStatusType.Sterilize);
            ids.Add((int)LifecycleStatusType.SterileStaged);
            ids.Add((int)LifecycleStatusType.CaseAssembly);
            ids.Add((int)LifecycleStatusType.TransportationStaging);

            return ids;
        }

        public static string GenerateGCTrayBarcode(long? containerId)
        {
            var itemType = Constants.CT_CONTAINER_TYPE_ACTUAL_ITEM_TYPE;

            var result = ProfitOptics.Modules.Sox.Helpers.Constants.NO_BARCODE_DATA;
            if (itemType != null && containerId.HasValue && containerId.Value > 0)
            {
                result = $"{itemType}{containerId}";
            }
            return result;
        }

        public static int MapSystemAlertTypeForMissingLifeCycleId(LifecycleStatusType lifecycleId)
        {
            int gCSystemAlertTypeId = -1;

            switch (lifecycleId)
            {
                case LifecycleStatusType.Verification:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepVerification;
                    break;
                case LifecycleStatusType.Sterilize:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepSterilize;
                    break;
                case LifecycleStatusType.SterileStaged:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepSterileStaged;
                    break;
                case LifecycleStatusType.TransportationStaging:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepTransportation;
                    break;
                case LifecycleStatusType.Sink:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepSink;
                    break;
                case LifecycleStatusType.Washer:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepWasher;
                    break;
                case LifecycleStatusType.Assembly:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepAssembly;
                    break;
                case LifecycleStatusType.SinkLoaner:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepSinkLoaner;
                    break;
                case LifecycleStatusType.WasherLoaner:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepWasherLoaner;
                    break;
                case LifecycleStatusType.AssemblyLoaner:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepAssemblyLoaner;
                    break;
                case LifecycleStatusType.LoanerInbound:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepLoanerInbound;
                    break;
                case LifecycleStatusType.LoanerOutbound:
                    gCSystemAlertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTraySkippedRequiredStepLoanerOutbound;
                    break;
            }

            return gCSystemAlertTypeId;
        }
    }
}
