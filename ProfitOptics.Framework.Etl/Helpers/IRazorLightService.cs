﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public interface IRazorLightService
    {
        /// <summary>
        /// Compiles and renders the template
        /// </summary>
        /// <param name="template">Template name</param>
        /// <param name="model">Template model</param>
        /// <returns>Rendered template as a string</returns>
        Task<string> RenderTemplate(string template, object model);
    }
}
