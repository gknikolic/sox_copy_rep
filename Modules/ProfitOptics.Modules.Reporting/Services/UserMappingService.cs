﻿using System.Linq;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Services
{
    public class UserMappingService : IUserMappingService
    {
        private readonly Entities _context;

        public UserMappingService(Entities context)
        {
            _context = context;
        }

        /// <inheritdoc />
        public ReportUserMapping GetReportUserMappingForUserId(int userId)
        {
            if (userId <= 0)
            {
                return null;
            }

            ReportUserMapping reportUserMapping = _context.ReportUserMapping.SingleOrDefault(m => m.UserId == userId);

            return reportUserMapping;
        }

        public IQueryable<ReportUserMapping> GetAllUserMappings()
        {
            return _context.ReportUserMapping;
        } 
    }
}