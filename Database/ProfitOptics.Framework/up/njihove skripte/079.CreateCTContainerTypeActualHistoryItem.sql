USE [Sox]

CREATE TABLE [dbo].[CTContainerTypeActualHistoryItem]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[CTContainerTypeActualId] INT NOT NULL,
	[StatusId] INT NOT NULL,
	[Status] NVARCHAR(100) NOT NULL,
	[LoadId] NVARCHAR(20) NULL,
	[UpdateTimestamp] DATETIME NOT NULL,
	[UpdateUserId] NVARCHAR(100) NOT NULL,
	[LocationElapsedCase] NVARCHAR(200) NOT NULL,
	[CreatedAtUtc] DATETIME NOT NULL,
	[ModifiedAtUtc] DATETIME NULL
CONSTRAINT [PK_CTContainerTypeActualHistoryItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem]  WITH CHECK ADD  CONSTRAINT [FK_CTContainerTypeActualHistoryItem_CTContainerTypeActual] FOREIGN KEY([CTContainerTypeActualId])
REFERENCES [dbo].[CTContainerTypeActual] ([Id])
GO
ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem] CHECK CONSTRAINT [FK_CTContainerTypeActualHistoryItem_CTContainerTypeActual]
GO