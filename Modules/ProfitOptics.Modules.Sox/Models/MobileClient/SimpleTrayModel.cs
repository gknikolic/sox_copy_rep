﻿using System;

namespace ProfitOptics.Modules.Sox.Models
{
	public class SimpleTrayModel
	{
		public int Id { get; set; }
		public string CaseReference { get; set; }
		public string Title { get; set; }
	}

	public class TrayForPickup : SimpleTrayModel
	{
		public DateTime DueTimestamp{ get; set; }
		public bool IsProccessed { get; set; }
	}
}
