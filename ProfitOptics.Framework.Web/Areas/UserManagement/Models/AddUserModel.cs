﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Helpers.Attributes;
using ProfitOptics.Modules.Sox.Models.ViewModels;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Models
{
    public class AddUserModel : StatusResult
    {
        [Required]
        [MaxLength(100, ErrorMessage = "The {0} can't be longer than {1} characters.")]
        [MinLength(2, ErrorMessage = "The {0} can't be shorter than {1} characters.")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(100, ErrorMessage = "The {0} can't be longer than {1} characters.")]
        [MinLength(2, ErrorMessage = "The {0} can't be shorter than {1} characters.")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        //[ValidDomain(ErrorMessage = "You are not allowed to register with an email address from this domain.")]
        [Display(Name = "Email")]
        [VerifyEmail(ErrorMessage = "Email already exists!")]
        [MaxLength(100, ErrorMessage = "The {0} can't be longer than {1} characters.")]
        [MinLength(6, ErrorMessage = "The {0} can't be shorter than {1} characters.")]
        public string Email { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "The {0} can't be longer than {1} characters.")]
        [MinLength(2, ErrorMessage = "The {0} can't be shorter than {1} characters.")]
        [Display(Name = "Phone number")]
        
        //[RegularExpression(@"^[0-9\.\-\/]+$", ErrorMessage = "You can not have that")]
        public string PhoneNumber { get; set; }

        public bool ShouldShowModal { get; set; } = false;

        public IEnumerable<SelectListItem> RegisterModelRoles { get; set; }

        [Required]
        [Display(Name="User Role")]
        public List<string> SelectedUserRoles { get; set; }
        //public string SelectedUserRole { get; set; }

        public IEnumerable<SelectListItem> GCParentCompanies { get; set; }
        public List<string> SelectedGCParentCompanies { get; set; }

        public AddUserModel()
        {

        }
    }
}
