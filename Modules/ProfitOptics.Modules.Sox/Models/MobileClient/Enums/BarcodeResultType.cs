﻿namespace ProfitOptics.Modules.Sox.Models
{
	public enum BarcodeResultType
	{
		Ok = 1,
		AlreadyProcessed = 2,
		NotFound = 3,
		FoundMultiple = 4
	}
}
