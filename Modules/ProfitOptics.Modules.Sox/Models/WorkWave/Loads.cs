﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class Loads
	{
		[JsonProperty("clean trays")]
		public int CleanTrays { get; set; }

		[JsonProperty("soiled trays")]
		public int SoiledTrays { get; set; }
	}
}