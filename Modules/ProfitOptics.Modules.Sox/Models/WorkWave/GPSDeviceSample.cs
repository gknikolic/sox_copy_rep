﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class GPSDeviceSample
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("ts")]
        public long Ts { get; set; }

        [JsonProperty("latLng")]
        public long[] LatLng { get; set; }

        [JsonProperty("heading")]
        public int Heading { get; set; }

        [JsonProperty("speedMs")]
        public int SpeedMs { get; set; }
    }

    public class ListGPSDevices
	{
        [JsonProperty("devices")]
        public Dictionary<string,GPSDevice> Devices { get; set; }
	}
}
