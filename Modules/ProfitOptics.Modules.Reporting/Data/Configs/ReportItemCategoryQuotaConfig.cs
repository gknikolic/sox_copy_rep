﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportItemCategoryQuotaConfig : IEntityTypeConfiguration<ReportItemCategoryQuota>
    {
        public void Configure(EntityTypeBuilder<ReportItemCategoryQuota> builder)
        {
            builder.ToTable(nameof(ReportItemCategoryQuota));
            builder.HasKey(q => q.Id);

            builder.Property(q => q.Month).IsRequired();
            builder.Property(q => q.Year).IsRequired();
            builder.Property(q => q.Revenue).HasColumnType("decimal(19,4)").IsRequired();

            builder.HasOne(q => q.ReportItemCategory)
                .WithMany()
                .HasForeignKey(q => q.ItemCategoryId)
                .IsRequired();

            builder.HasOne(q => q.ReportHierarchySalesRep)
                .WithMany()
                .HasForeignKey(q => q.SalesRepId)
                .IsRequired();
        }
    }
}