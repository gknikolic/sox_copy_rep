﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueTypeStatus
    {
        public string expand { get; set; }

        public int? id { get; set; }

        public string self { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public IssueTypeStatusCategory statusCategory { get; set; }
    }
}