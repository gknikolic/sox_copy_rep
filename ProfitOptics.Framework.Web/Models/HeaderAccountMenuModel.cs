﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Models
{
    public class HeaderAccountMenuModel
    {
        public string FullName { get; set; }
        public string MemeberSince { get; set; }
        public string Base64ProfilePicture { get; set; }
        
        public bool IsImpersonating { get; set; }
    }
}
