﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Data.SqlClient;
using System.Linq;
using DataBrowser;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Modules.DataBrowser.Models;

namespace ProfitOptics.Modules.DataBrowser.Services
{
    public class DataBrowserConfigurationService : IDataBrowserConfigurationService
    {
        #region Fields

        private readonly DataBrowserSettings _dataBrowserSettings;
        private readonly ConnectionStrings _connectionStringSettings;

        #endregion

        #region Ctor

        public DataBrowserConfigurationService(DataBrowserSettings dataBrowserSettings,
                                                ConnectionStrings connectionStringSettings)
        {
            this._dataBrowserSettings = dataBrowserSettings;
            this._connectionStringSettings = connectionStringSettings;
        }

        #endregion

        #region Methods

        public ConnectionStringSettings GetConnectionString()
        {
            string connectionString = _dataBrowserSettings.ConnectionString;
            if (!string.IsNullOrEmpty(connectionString))
            {
                var connectionStringSettings = new ConnectionStringSettings("databrowser", connectionString);
                return connectionStringSettings;
            }

            connectionString = _connectionStringSettings.AreaEntities;
            if (!string.IsNullOrEmpty(connectionString))
            {
                var connectionStringSettings = new ConnectionStringSettings("default", connectionString);
                return connectionStringSettings;
            }

            throw new DataBrowserException(
                "Cannot find app setting databrowser.connectionstring or databrowser.connectionstring.ref");
        }

        public IEnumerable<string> GetAllowedDatabases()
        {
            var allowedDatabasesValue = _dataBrowserSettings.Databases;
            if (string.IsNullOrEmpty(allowedDatabasesValue))
                throw new DataBrowserException("Cannot find app setting databrowser.databases");

            var allowedDatabases = allowedDatabasesValue.Split('|').ToList();
            return allowedDatabases;

        }

        public int GetMaxAllowedDownloadRecords()
        {
            return 5000000;
        }

        public int GetMinWarningDownloadRecords()
        {
            return 1000000;
        }

        public IEnumerable<string> GetAllowedTablesForDatabase(string database)
        {
            string allowedTablesValue = "";

            if (string.IsNullOrEmpty(database))
            {
                throw new ArgumentException("database argument is illegal", "database");
            }

            if (_dataBrowserSettings.AllowedTables.ContainsKey(database))
            {
                allowedTablesValue = _dataBrowserSettings.AllowedTables[database];
            }

            IEnumerable<string> allowedTables = !string.IsNullOrEmpty(allowedTablesValue)
                ? allowedTablesValue.Split('|').ToList()
                : new List<string>();

            return allowedTables;
        }

        public IEnumerable<TableModel> GetTablesFromDatabase(string database)
        {
            using (var conn = new SqlConnection(GetConnectionString().ConnectionString).CreateCommand())
            {
                conn.CommandText = GetQueryForDatabase(database);
                conn.Connection.Open();
                using (var sqlDataReader = conn.ExecuteReader())
                {
                    List<TableModel> tables = new List<TableModel>();
                    while (sqlDataReader.Read())
                    {
                        var tableModel = new TableModel
                        {
                            Id = sqlDataReader["Id"] as int?,
                            Name = sqlDataReader["Name"] as string
                        };
                        tables.Add(tableModel);
                    }

                    var allowedTables = GetAllowedTablesForDatabase(database);
                    if (allowedTables.Any())
                    {
                        tables = tables.Where(t => allowedTables.Select(x => x.ToLower()).Contains(t.Name.ToLower())).ToList();
                    }

                    return tables;
                }

            }
        }

        #endregion

        #region Util

        private static string GetQueryForDatabase(string database)
        {
            var query = string.Format(
                @" select tab.name as Name, tab.object_id as Id " +
                " from {0}.sys.tables tab " +
                " inner join {0}.sys.columns col on col.object_id = tab.object_id " +
                " where tab.type = 'U' " +
                " group by tab.name, tab.object_id " +
                " order by tab.name ", database);

            return query;
        }

        #endregion

    }
}