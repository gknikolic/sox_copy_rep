﻿namespace ProfitOptics.Modules.Sox.Models
{
	public enum ExecutionEventType
	{
		timeIn, 
		timeOut, 
		detectedTimeIn, 
		detectedTimeOut, 
		statusUpdate, 
		podSignature, 
		podPicture, 
		podAudio,
		podBarcodes,
		podNote,
		departure, 
		arrival
	}


}
