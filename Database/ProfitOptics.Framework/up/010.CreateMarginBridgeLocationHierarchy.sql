USE [ProfitOptics.Framework]
GO

/****** Object:  Table [dbo].[MarginBridge_LocationHierarchy]    Script Date: 23.9.2019. 12:23:08 ******/

 IF exists(SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MarginBridge_LocationHierarchy' AND TABLE_CATALOG = 'ProfitOptics.Framework')
 BEGIN
 Drop Table MarginBridge_LocationHierarchy
 END

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MarginBridge_LocationHierarchy](
	[LocationHierarchyID] [int] IDENTITY(1,1) NOT NULL,
	[LocationLevel1] [varchar](255) NULL,
	[LocationLevel2] [varchar](255) NULL,
	[LocationLevel3] [varchar](255) NULL,
	[LocationLevel4] [varchar](255) NULL,
	[LocationLevel5] [varchar](255) NULL,
	[LocationLevel6] [varchar](255) NULL,
	[LocationName] [varchar](255) NULL,
 CONSTRAINT [PK_MarginBridge_LocationHierarchy] PRIMARY KEY CLUSTERED 
(
	[LocationHierarchyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

