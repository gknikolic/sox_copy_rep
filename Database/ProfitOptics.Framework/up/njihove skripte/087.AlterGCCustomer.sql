USE [Sox]

ALTER TABLE [Sox].[dbo].[GCCustomer] 
ADD [PickupInstructions] NVARCHAR(max),
    [DropoffInstructions] NVARCHAR(max),
    [PickupName] NVARCHAR(max);

GO
