﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.DataLayer.GroundControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCLocationInfoConfig : IEntityTypeConfiguration<GCLocationInfo>
    {
        public void Configure(EntityTypeBuilder<GCLocationInfo> builder)
        {
            builder.ToTable("GCLocationInfos");
            builder.HasKey(entity => entity.Id);
        }
    }
}
