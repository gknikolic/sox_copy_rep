﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class QBUpdateTokenResponse
    {
        public long AccessTokenExpiresIn { get; set; }
        public long RefreshTokenExpiresIn { get; set; }
        public string ErrorDescription { get; set; }
        public string RefreshToken { get; set; }
        public string TokenType { get; set; }
        public string IdentityToken { get; set; }
        public string AccessToken { get; set; }
        public string HttpErrorReason { get; set; }
    }
}
