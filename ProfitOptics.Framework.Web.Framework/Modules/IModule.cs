﻿namespace ProfitOptics.Framework.Web.Framework.Modules
{
    public interface IModule
    {
        string GetConfigurationPageUrl();

        ModuleDescriptor ModuleDescriptor { get; set; }

        void Install();

        void Uninstall();
    }
}
