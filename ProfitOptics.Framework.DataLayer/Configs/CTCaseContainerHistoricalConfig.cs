﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTCaseContainerHistoricalConfig : IEntityTypeConfiguration<CTCaseContainerHistorical>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTCaseContainerHistorical> builder)
        {
            builder.ToTable("CTCaseContainerHistorical");
            builder.HasKey(e => e.Id);
        }
    }
}
