﻿namespace ProfitOptics.Modules.Announcements.Models
{
    public enum AnnouncementsFilter
    {
        All = 0,
        UserSpecific = 1
    }
}
