﻿using ProfitOptics.Modules.Sox.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Services
{
	public interface IMonnitService
	{
		public Task SyncMonnitSensorsAsync();
		void HandleMonnitWebhookResponse(MonnitWebhookResponse response);
	}
}
