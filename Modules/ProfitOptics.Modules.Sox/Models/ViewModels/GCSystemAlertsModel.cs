﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCSystemAlertsModel
    { 
        public List<GCSystemAlertModel> Alerts { get; set; }
    }
}
