﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.Showcase.Factories;

namespace ProfitOptics.Modules.Showcase.Infrastructure
{
    public sealed class ShowcaseServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddScoped<IShowcaseFactory, ShowcaseFactory>();
        }
    }
}