﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProfitOptics.Framework.Core.TokenizationSearch
{
    public class TokenCollection
    {
        public List<TokenAND> mTokensAND = new List<TokenAND>();
    }

    public class TokenAND
    {
        public List<string> mTokensOR = new List<string>();
    }
}