﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class AccountingsVM
    {
        public string Title { get; set; }
        public bool QBAuthorized { get; set; }
    }
}
