﻿using ProfitOptics.Framework.Core.Data;

namespace ProfitOptics.Modules.CrossReference.Models
{
    public class CrossReferenceExportModel : IDataRecord
    {
        [DataField("Competitor", "", 1)]
        public string CompetitorName { get; set; }


        [DataField("Competitor Item #", "", 2)]
        public string CompetitorItemNumber { get; set; }


        [DataField("Competitor Item Description", "", 3)]
        public string CompetitorItemDescription { get; set; }


        [DataField("Client Item #", "", 4)]
        public string ClientItemNumber { get; set; }


        [DataField("Client Item Description", "", 5)]
        public string ClientItemDescription { get; set; }


        [DataField("Compatibility", "", 6)]
        public string Compatibility { get; set; }
    }
}