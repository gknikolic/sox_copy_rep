﻿using System.Collections.Generic;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.CrossReference.Models;

namespace ProfitOptics.Modules.CrossReference.Factories
{
    public interface ICrossReferenceFactory
    {
        CrossReferenceIndexModel PrepareIndexModel();

        List<CompatibilityOptionModel> GetCompatibilityOptionModels(bool getDeleted = false);

        List<CompetitorModel> GetCompetitorModels(bool getDeleted = false);
        
        List<CrossReferenceExportModel> GetCrossReferenceExportModelList(List<int?> crossReferenceIds);

        List<ItemLookupSelect2DataModel> GetItemAutocompleteData(string term);

        PagedList<CrossReferenceRecordModel> GetCrossReferenceRecordModelsForApproval(IDataTablesRequest request,
            int userId);

        PagedList<CrossReferenceRecordModel> GetCrossReferenceRecordModels(IDataTablesRequest request,
            int? competitorId, int? compatibilityId, string competitorItem, int userId);
    }
}