﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTLocationTypeGCLifecycleStatusType
    {
        public int Id { get; set; }
        public int GCLifecycleStatusTypeId { get; set; }
        public int CTLocationTypeId { get; set; }

        public virtual GCLifecycleStatusType GCLifecycleStatusType { get; set; }
        public virtual CTLocationType CTLocationType { get; set; }
    }
}
