﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class PriceListCodeModel
    {
        public int? QuoteId { get; set; }

        public IEnumerable<SelectListItem> PricingList { get; set; }
    }
}