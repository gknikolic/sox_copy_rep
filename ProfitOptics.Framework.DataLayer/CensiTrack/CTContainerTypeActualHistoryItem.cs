﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerTypeActualHistoryItem
    {
        public int Id { get; set; }
        public int CTContainerTypeActualId { get; set; }
        public int? CTLocationId { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string LoadId { get; set; }
        public DateTime UpdateTimestamp { get; set; }
        public string UpdateUserId { get; set; }
        public string LocationElapsedCase { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
        public DateTime UpdateTimestampOriginal { get; set; }
        public int? ActualLapCount { get; set; }

        public virtual CTContainerTypeActual CTContainerTypeActual { get; set; }
        public virtual CTLocation CTLocation { get; set; }
    }
}
