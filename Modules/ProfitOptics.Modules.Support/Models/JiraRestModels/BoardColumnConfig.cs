﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class BoardColumnConfig
    {
        public BoardColumn[] Columns { get; set; }
    }
}