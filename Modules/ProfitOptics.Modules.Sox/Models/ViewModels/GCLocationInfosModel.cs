﻿using ProfitOptics.Framework.DataLayer.GroundControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCLocationInfosModel
    {
        public int LocationId { get; set; }
        public List<GCLocationInfoModel> LocationInfos { get; set; }
        public GCLocation Location { get; set; }
		public string LocationTitle { get; set; }
	}
}
