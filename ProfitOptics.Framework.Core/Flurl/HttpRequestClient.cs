﻿using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Core.Flurl
{
	public class HttpRequestClient : IHttpRequestClient
	{
		public string Url { get; set; }
		public string Token { get; set; }
		public string ApiKey { get; set; }
        public IFlurlRequest _Request
        {
            get
            {
                //TODO: use policies
                //https://stackoverflow.com/questions/40745809/how-to-use-polly-with-flurl-http
                if(ApiKey!=String.Empty)
				{
                    return new Url(Url).WithHeaders(new { Accept = "application/json", Content_Type = "application/json", X_WorkWave_Key = ApiKey });
                }
                else
				{
                    return new Url(Url).WithHeaders(new { Accept = "application/json", Content_Type = "application/json" });
                }
                
                //.AllowAnyHttpStatus();//TODO: remove after testing;
            }
        }

        public HttpRequestClient(string url)
		{
            Url = url;
           // _Request.Url = new Url(url);

        }

        public HttpRequestClient(string url, string token, string apiKey)
        {
            Url = url;
            Token = token;
            ApiKey = apiKey;
            //_Request.Url = new Url(url);
            //_Request = _Request.WithOAuthBearerToken(token);

        }

    }
}
