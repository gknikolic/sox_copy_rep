USE [ProfitOptics.Framework]

ALTER TABLE POAnnouncementRoles ALTER COLUMN AnnouncementId int NOT NULL
ALTER TABLE POAnnouncementRoles ALTER COLUMN RoleId int NOT NULL