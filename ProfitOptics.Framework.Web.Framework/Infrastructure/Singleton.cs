﻿namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    public class Singleton<T> : Web.Framework.Infrastructure.BaseSingleton
    {
        private static T _instance;

        public static T Instance
        {
            get => _instance;
            set
            {
                _instance = value;
                AllSingletons[typeof(T)] = value;
            }
        }
    }
}
