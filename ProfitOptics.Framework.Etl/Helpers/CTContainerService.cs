﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Linq;
using Serilog;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Modules.Sox.Helpers;
using ProfitOptics.Modules.Sox.Services;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using ProfitOptics.Framework.Etl.Models;
using System.IO;
using System.Drawing;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public class CTContainerService : ICTContainerService
    {
        private readonly Entities _context;
        private readonly IGroundControlService _groundControlService;

        public CTContainerService(Entities context, IGroundControlService groundControlService)
        {
            _context = context;
            _groundControlService = groundControlService;
        }

        //public void AddOrEditGCVendors(List<Framework.DataLayer.GCVendor> gcVendors)
        //{
        //    gcVendors.ForEach(item =>
        //    {
        //        var existing = _context.GCVendors.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

        //        if (existing == null)
        //        {
        //            item.CreatedAtUtc = DateTime.UtcNow;
        //            item.Active = true;
        //            _context.GCVendors.Add(item);
        //        }
        //        else
        //        {
        //            existing.LegacyId = item.LegacyId;
        //            existing.Title = item.Title;
        //            existing.Active = true;
        //            existing.ModifiedAtUtc = DateTime.UtcNow;
        //        }
        //    });

        //    _context.SaveChanges();
        //}

        public void AddOrEditCTContainerTypes(List<CTContainerType> ctContainerTypes, long serviceId)
        {
            //var gcVendor = _entities.GCVendors.Where(x => x.LegacyId == serviceId).FirstOrDefault();
            var ctContainerService = _context.CTContainerServices.Where(x => x.LegacyId == serviceId).FirstOrDefault();

            if (ctContainerService == null)
            {
                Log.Error($"No CTContainerService found for LegacyId: {serviceId}.");
                throw new Exception();
            }

            ctContainerTypes.ForEach(item =>
            {
                var existing = _context.CTContainerTypes.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existing == null)
                {
                    //item.GCVendor.Id = gcVendor.Id;
                    //item.GCVendor = gcVendor;
                    item.CTContainerService = ctContainerService;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTContainerTypes.Add(item);
                }
                else
                {
                    existing.LegacyId = item.LegacyId;
                    existing.ServiceId = item.ServiceId;
                    existing.ServiceName = item.ServiceName;
                    existing.HomeLocationName = item.HomeLocationName;
                    existing.ProcessingLocationName = item.ProcessingLocationName;
                    existing.Name = item.Name;
                    existing.SterilizationMethodName = item.SterilizationMethodName;
                    existing.Modified = item.Modified;
                    existing.IsDiscarded = item.IsDiscarded;
                    existing.AutomaticActuals = item.AutomaticActuals;

                    existing.FacilityId = item.FacilityId;
                    existing.SterilizationMethodId = item.SterilizationMethodId;
                    existing.FirstAlternateMethodId = item.FirstAlternateMethodId;
                    existing.SecondAlternateMethodId = item.SecondAlternateMethodId;
                    existing.ThirdAlternateMethodId = item.ThirdAlternateMethodId;
                    existing.HomeLocationId = item.HomeLocationId;
                    existing.ProcessingLocationId = item.ProcessingLocationId;
                    existing.PhysicianName = item.PhysicianName;
                    existing.UsesBeforeService = item.UsesBeforeService;
                    existing.UsageInterval = item.UsageInterval;
                    existing.UsageIntervalUnitOfMeasure = item.UsageIntervalUnitOfMeasure;
                    existing.StandardAssemblyTime = item.StandardAssemblyTime;
                    existing.Weight = item.Weight;
                    existing.ProcessingCost = item.ProcessingCost;
                    existing.ProcurementReferenceId = item.ProcurementReferenceId;
                    existing.DecontaminationInstructions = item.DecontaminationInstructions;
                    existing.AssemblyInstructions = item.AssemblyInstructions;
                    existing.SterilizationInstructions = item.SterilizationInstructions;
                    existing.CountSheetComments = item.CountSheetComments;
                    existing.PriorityId = item.PriorityId;
                    existing.BiologicalTestRequired = item.BiologicalTestRequired;
                    existing.ClassSixTestRequired = item.ClassSixTestRequired;
                    existing.AssemblyByException = item.AssemblyByException;
                    existing.SoftWrap = item.SoftWrap;
                    existing.ComplexityLevelId = item.ComplexityLevelId;
                    existing.OriginalContainerTypeId = item.OriginalContainerTypeId;
                    existing.VendorId = item.VendorId;
                    existing.VendorName = item.VendorName;

                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void AddOrEditCTContainerItems(List<CTContainerItem> ctContainerItems, long containerId)
        {
            var ctContainer = _context.CTContainerTypes.Where(x => x.LegacyId == containerId).FirstOrDefault();

            if (ctContainer == null)
            {
                Log.Error($"No CTContainerType found for LegacyId: {containerId}.");
                throw new Exception();
            }

            ctContainerItems.ForEach(item =>
            {
                var existing = _context.CTContainerItems.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existing == null)
                {
                    var product = _context.CTProducts.Where(x => x.LegacyId == item.ProductId).FirstOrDefault();

                    if(product == null)
                    {
                        Log.Information($"No CTProduct found for LegacyId: {item.ProductId}.");
                        //throw new Exception();
                        product = new CTProduct()
                        {
                            CreatedAtUtc = DateTime.UtcNow
                        };
                    }
                    product.LegacyId = item.CTProduct.LegacyId;
                    product.ModelNumber = item.CTProduct.ModelNumber;
                    product.VendorName = item.CTProduct.VendorName;
                    product.VendorProductName = item.CTProduct.VendorProductName;
                    product.ModifiedAtUtc = DateTime.UtcNow;
                    if(product.Id == 0)
                    {
                        _context.CTProducts.Add(product);
                    }

                    item.CTContainerTypeId = ctContainer.Id;
                    item.CTProduct = product;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTContainerItems.Add(item);
                }
                else
                {
                    existing.LegacyId = item.LegacyId;
                    existing.RequiredCount = item.RequiredCount;
                    existing.Placement = item.Placement;
                    existing.SubstitutionsAllowed = item.SubstitutionsAllowed;
                    existing.CriticalItem = item.CriticalItem;
                    existing.ChangeState = item.ChangeState;
                    existing.SequenceNumber = item.SequenceNumber;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void AddOrEditCTContainerTypeActuals(List<CTContainerTypeActual> ctContainerTypeActuals, long containerTypeId, int serverOffsetInMinsFromUTC)
        {
            var ctContainerType = _context.CTContainerTypes.Where(x => x.LegacyId == containerTypeId).FirstOrDefault();
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            if (ctContainerType == null)
            {
                Log.Error($"No CTContainerType found for LegacyId: {containerTypeId}.");
                throw new Exception();
            }

            ctContainerTypeActuals.ForEach(item =>
            {
                var existing = _context.CTContainerTypeActuals.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                item.UpdateTimestamp = item.UpdateTimestampOriginal.AddMinutes(minsToAdd);

                if (existing == null)
                {
                    item.CTContainerTypeId = ctContainerType.Id;
                    item.VendorName = ctContainerType.VendorName;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTContainerTypeActuals.Add(item);
                }
                else
                {
                    existing.CTContainerTypeId = ctContainerType.Id;
                    existing.LegacyId = item.LegacyId;
                    existing.ParentId = item.ParentId;
                    existing.Weight = item.Weight;
                    existing.PhysicianName = item.PhysicianName;
                    existing.SterilizationMethodName = item.SterilizationMethodName;
                    existing.ServiceId = item.ServiceId;
                    existing.ServiceName = item.ServiceName;
                    //existing.ProcurementReferenceId = item.ProcurementReferenceId;
                    existing.Name = item.Name;
                    //existing.ParentName = item.ParentName;
                    existing.Status = item.Status;
                    existing.UpdateTimestamp = item.UpdateTimestamp;
                    existing.UpdateUserId = item.UpdateUserId;
                    existing.LocationElapsed = item.LocationElapsed;
                    existing.CaseCart = item.CaseCart;
                    existing.ProcessingLocationName = item.ProcessingLocationName;
                    existing.ParentProcessingLocationName = item.ParentProcessingLocationName;
                    existing.ParentUsageIntervalUnitOfMeasure = item.ParentUsageIntervalUnitOfMeasure;
                    existing.HomeLocationName = item.HomeLocationName;
                    existing.ParentHomeLocationName = item.ParentHomeLocationName;
                    existing.ParentIntervalUsageCount = item.ParentIntervalUsageCount;
                    existing.IntervalUsageCount = item.IntervalUsageCount;
                    existing.Usage = item.Usage;
                    existing.CensisSetLastRequestDate = item.CensisSetLastRequestDate;
                    existing.LastMaintenance = item.LastMaintenance;
                    existing.Assembly = item.Assembly;
                    existing.Storage = item.Storage;
                    existing.RtlsLocationName = item.RtlsLocationName;
                    existing.RtlsLocationUpdateTimestamp = item.RtlsLocationUpdateTimestamp;
                    existing.IsDiscarded = item.IsDiscarded;
                    existing.Priority = item.Priority;
                    existing.ShelfLife = item.ShelfLife;
                    existing.NotificationCount = item.NotificationCount;
                    existing.Expedite = item.Expedite;
                    existing.Rigidity = item.Rigidity;
                    existing.ParentExpedite = item.ParentExpedite;
                    existing.ParentRigidity = item.ParentRigidity;
                    existing.CensisSetHistoryBuildStartDate = item.CensisSetHistoryBuildStartDate;
                    existing.FirstAlternateMethodId = item.FirstAlternateMethodId;
                    existing.FirstAlternateMethodName = item.FirstAlternateMethodName;
                    existing.SecondAlternateMethodId = item.SecondAlternateMethodId;
                    existing.SecondAlternateMethodName = item.SecondAlternateMethodName;
                    existing.ThirdAlternateMethodId = item.ThirdAlternateMethodId;
                    existing.ThirdAlternateMethodName = item.ThirdAlternateMethodName;
                    existing.ComplexityLevelDescription = item.ComplexityLevelDescription;
                    existing.DisposableFlag = item.DisposableFlag;
                    existing.SerialNumber = item.SerialNumber;
                    //existing.VendorName = item.VendorName;
                    existing.VendorName = ctContainerType.VendorName;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                    existing.UpdateTimestampOriginal = item.UpdateTimestampOriginal;
                }
            });

            _context.SaveChanges();
        }

        public void AddOrEditCTContainerServices(List<Framework.DataLayer.CTContainerService> ctContainerServices)
        {
            ctContainerServices.ForEach(item =>
            {
                var existing = _context.CTContainerServices.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existing == null)
                {
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTContainerServices.Add(item);
                }
                else
                {
                    existing.LegacyId = item.LegacyId;
                    existing.Name = item.Name;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        //public void AddOrEditCTContainerTypeActualHistoryItemsAndGCTrays(List<Framework.DataLayer.CTContainerTypeActualHistoryItem> ctContainerTypeActualHistoryItems, long serviceId, int serverOffsetInMinsFromUTC, bool? isStopLifecycleHistory = false, bool? updateLapCountAndLocation = false)
        //{
        //    var ctContainerTypeActual = _context.CTContainerTypeActuals.Include(x => x.CTContainerType).Where(x => x.LegacyId == serviceId).FirstOrDefault();
        //    int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

        //    if (ctContainerTypeActual == null)
        //    {
        //        Log.Error($"No CTContainerTypeActual found for LegacyId: {serviceId}.");
        //        throw new Exception();
        //    }

        //    bool isActualLoaner = ctContainerTypeActualHistoryItems.Any(x => x.LocationElapsedCase.Equals(Constants.CT_LOANERS_CHECK_IN_LOCATION));

        //    ctContainerTypeActualHistoryItems = ctContainerTypeActualHistoryItems.OrderBy(x => x.UpdateTimestampOriginal).ToList();

        //    // First iteration (updating HistoryItems)
        //    ctContainerTypeActualHistoryItems.ForEach(hi =>
        //    {
        //        if (hi.CTContainerTypeActualId == 0)
        //        {
        //            hi.CTContainerTypeActualId = ctContainerTypeActual.Id;
        //        }
        //        _groundControlService.UpdateHistoryItemAndLapCount(hi, minsToAdd, isActualLoaner, updateLapCountAndLocation);
        //    });

        //    if (!isStopLifecycleHistory.HasValue || !isStopLifecycleHistory.Value)
        //    {
        //        GCTray gcTray = _context.GCTrays.Include(x => x.GCTraysLifecycleStatusTypes)
        //           .Where(x => x.CTContainerTypeActualId == ctContainerTypeActual.Id
        //           && x.GCCaseId == null
        //           ).FirstOrDefault();
        //        if (gcTray == null)
        //        {
        //            gcTray = new GCTray()
        //            {
        //                CTContainerTypeActualId = ctContainerTypeActual.Id,
        //                CTContainerTypeId = ctContainerTypeActual.CTContainerType.Id,
        //                GCTraysLifecycleStatusTypes = new List<GCTraysLifecycleStatusTypes>(),
        //                GCCaseId = null,
        //                IsActiveStatus = false
        //            };
        //        }

        //        // Second iteration (updating LCs)
        //        ctContainerTypeActualHistoryItems.ForEach(item =>
        //        {
        //            if (item.CTContainerTypeActualId == 0)
        //            {
        //                item.CTContainerTypeActualId = ctContainerTypeActual.Id;
        //            }

        //            // Used for making a reference with CTLocation into CTContainerTypeActualHistoryItem
        //            int? ctLocationId = null;
        //            CTLocation ctLocation = null;

        //            if (item.Status.Equals(Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG) || item.Status.Equals(Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER))
        //            {
        //                // Silly Hack because of CT
        //                ctLocation = _groundControlService.GetCTLocationForLocationName(item.LocationElapsedCase);

        //                if (ctLocation != null)
        //                {
        //                    Log.Information($"(2nd iteration HI) - Found CTLocation: {ctLocation.LocationName}, for LocationName: {item.LocationElapsedCase}");

        //                    ctLocationId = ctLocation.Id;
        //                }
        //                else
        //                {
        //                    Log.Error($"(2nd iteration HI) - NOT Found CTLocation for provided LocationName: {item.LocationElapsedCase}");
        //                }
        //            }

        //            if ((item.Status.Equals(Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG) || item.Status.Equals(Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)) && ctLocation != null)
        //            {
        //                //var ctLocationTypesGCLifecycleStatusType = _entities.CTLocationTypeGCLifecycleStatusTypes.Where(x => x.CTLocationTypeId == ctLocation.CTLocationTypeId).FirstOrDefault();

        //                // Silly Hack because of CT
        //                var gcLifecycleStatusType = _groundControlService.GetGCLifeCycleBasedOnLocationType(item.UpdateTimestamp, ctLocation.CTLocationTypeId, ctContainerTypeActual.Id, null);
        //                if (gcLifecycleStatusType != null)
        //                {
        //                    var gcTrayLifeCycle = gcTray.GCTraysLifecycleStatusTypes.Where(x => x.Timestamp == item.UpdateTimestamp).FirstOrDefault();
        //                    if (gcTrayLifeCycle == null)
        //                    {
        //                        gcTrayLifeCycle = new GCTraysLifecycleStatusTypes()
        //                        {
        //                            LifecycleStatusTypeId = gcLifecycleStatusType.Id,
        //                            Timestamp = item.UpdateTimestamp,
        //                            IsActiveStatus = false,
        //                            User = item.UpdateUserId,
        //                            TimestampOriginal = item.UpdateTimestampOriginal
        //                        };
        //                        gcTray.GCTraysLifecycleStatusTypes.Add(gcTrayLifeCycle);
        //                        Log.Information($"-----------------Created LC with timestamp: {item.UpdateTimestamp} and original timestamp: {item.UpdateTimestampOriginal}");
        //                    }
        //                    else
        //                    {
        //                        gcTrayLifeCycle.User = item.UpdateUserId;
        //                        gcTrayLifeCycle.LifecycleStatusTypeId = gcLifecycleStatusType.Id;
        //                    }
        //                }
        //            }

        //            _context.SaveChanges();
        //        });

        //        if (gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
        //        {
        //            gcTray.GCTraysLifecycleStatusTypes = gcTray.GCTraysLifecycleStatusTypes.OrderBy(x => x.Timestamp).ToList();
        //            for (int i = 0; i < gcTray.GCTraysLifecycleStatusTypes.Count; i++)
        //            {
        //                gcTray.GCTraysLifecycleStatusTypes.ToList()[i].IsActiveStatus = (i == (gcTray.GCTraysLifecycleStatusTypes.Count - 1));
        //            }
        //        }

        //        if (gcTray.Id == 0)
        //        {
        //            _context.GCTrays.Add(gcTray);
        //        }
        //    }

        //    _context.SaveChanges();
        //}

        public void AddOrEditCTContainerTypeGraphics(List<CTContainerTypeGraphic> ctContainerTypeGraphics, long containerTypeId)
        {
            var ctContainer = _context.CTContainerTypes.Where(x => x.LegacyId == containerTypeId).FirstOrDefault();

            if (ctContainer == null)
            {
                Log.Error($"No CTContainerType found for LegacyId: {containerTypeId}.");
                throw new Exception();
            }

            ctContainerTypeGraphics.ForEach(item =>
            {
                var existing = _context.CTContainerTypeGraphics.Where(x => x.GraphicId == item.GraphicId).FirstOrDefault();

                if (existing == null)
                {
                    item.CTContainerTypeId = ctContainer.Id;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    item.GCImageId = null;
                    _context.CTContainerTypeGraphics.Add(item);
                }
                else
                {
                    existing.ContainerTypeId = item.ContainerTypeId;
                    existing.ImageName = item.ImageName;
                    existing.IsGlobal = item.IsGlobal;
                    existing.UsageFlags = item.UsageFlags;
                    existing.ReferenceId = item.ReferenceId;
                    existing.IsAssembly = item.IsAssembly;
                    existing.IsDecontam = item.IsDecontam;
                    existing.IsSterilization = item.IsSterilization;
                    existing.Path = item.Path;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        //public List<CTContainerTypeGraphic> GetCTContainerTypeGraphic(bool? isWithoutImage)
        //{
        //    var result = new List<CTContainerTypeGraphic>();

        //    var query = _context.CTContainerTypeGraphics.Where(x => 1==1);

        //    if(isWithoutImage.HasValue)
        //    {
        //        if(isWithoutImage.Value == true)
        //        {
        //            query = query.Where(x => !x.GCImageId.HasValue);
        //        }
        //        else
        //        {
        //            query = query.Where(x => x.GCImageId.HasValue);
        //        }
        //    }

        //    result = query.ToList();

        //    return result;
        //}

        //public List<CTAssemblyGraphic> GetCTAssemblyGraphics(bool? isWithoutImage)
        //{
        //    var result = new List<CTAssemblyGraphic>();

        //    var query = _context.CTAssemblyGraphics.Where(x => 1 == 1);

        //    if (isWithoutImage.HasValue)
        //    {
        //        if (isWithoutImage.Value == true)
        //        {
        //            query = query.Where(x => !x.GCImageId.HasValue);
        //        }
        //        else
        //        {
        //            query = query.Where(x => x.GCImageId.HasValue);
        //        }
        //    }

        //    result = query.ToList();

        //    return result;
        //}

        //public void UpdateCTContainerTypeGraphic(int ctContainerTypeGraphicId, byte[] ctContainerTypeImage)
        //{
        //    var graphic = _context.CTContainerTypeGraphics.Find(ctContainerTypeGraphicId);

        //    if(graphic != null)
        //    {
        //        var gcImage = new GCImage()
        //        {
        //            Data = ctContainerTypeImage,
        //            Origin = Constants.CT_ABBREVIATION_TWO_CHAR,
        //            Title = graphic.ImageName,
        //            CreatedAtUtc = DateTime.UtcNow
        //        };

        //        graphic.GCImage = gcImage;
        //        _groundControlService.CreateThumbnailsForGCImage(gcImage);

        //        _context.SaveChanges();
        //    }
        //}

        public void SyncTimestamps(int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            var gcTraysLifecycleStatusTypes = _context.GCTraysLifecycleStatusTypes.ToList();
            if (gcTraysLifecycleStatusTypes != null && gcTraysLifecycleStatusTypes.Count > 0)
            {
                gcTraysLifecycleStatusTypes.ForEach(x =>
                {
                    x.Timestamp = x.TimestampOriginal.AddMinutes(minsToAdd);
                });
            }

            var ctContainerTypeActualHistoryItems = _context.CTContainerTypeActualHistoryItems.ToList();
            if (ctContainerTypeActualHistoryItems != null && ctContainerTypeActualHistoryItems.Count > 0)
            {
                ctContainerTypeActualHistoryItems.ForEach(x =>
                {
                    x.UpdateTimestamp = x.UpdateTimestampOriginal.AddMinutes(minsToAdd);
                });
            }

            var ctCaseContainers = _context.CTCaseContainers.ToList();
            if (ctCaseContainers != null && ctCaseContainers.Count > 0)
            {
                ctCaseContainers.ForEach(x =>
                {
                    x.DueTimestamp = x.DueTimestampOriginal.AddMinutes(minsToAdd);
                    if (x.EndTimestampOriginal.HasValue) x.EndTimestamp = x.EndTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (x.UpdateTimestampOriginal.HasValue) x.UpdateTimestamp = x.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (x.MultipleBasisOriginal.HasValue) x.MultipleBasis = x.MultipleBasisOriginal.Value.AddMinutes(minsToAdd);
                });
            }

            var ctBILoadResults = _context.CTBILoadResults.ToList();
            if (ctBILoadResults != null && ctBILoadResults.Count > 0)
            {
                ctBILoadResults.ForEach(x =>
                {
                    if (x.LoadDateOriginal.HasValue) x.LoadDate = x.LoadDateOriginal.Value.AddMinutes(minsToAdd);
                    if (x.InsertTimestampOriginal.HasValue) x.InsertTimestamp = x.InsertTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (x.UpdateTimestampOriginal.HasValue) x.UpdateTimestamp = x.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (x.LoadTimestampOriginal.HasValue) x.LoadTimestamp = x.LoadTimestampOriginal.Value.AddMinutes(minsToAdd);
                });
            }

            var ctBILoadResultContents = _context.CTBILoadResultContents.ToList();
            if (ctBILoadResultContents != null && ctBILoadResultContents.Count > 0)
            {
                ctBILoadResultContents.ForEach(x =>
                {
                    x.LastUpdate = x.LastUpdateOriginal.AddMinutes(minsToAdd);
                });
            }

            var ctBILoadResultIndicators = _context.CTBILoadResultIndicators.ToList();
            if (ctBILoadResultIndicators != null && ctBILoadResultIndicators.Count > 0)
            {
                ctBILoadResultIndicators.ForEach(x =>
                {
                    if (x.InsertTimestampOriginal.HasValue) x.InsertTimestamp = x.InsertTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (x.UpdateTimestampOriginal.HasValue) x.UpdateTimestamp = x.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd);
                });
            }

            var ctBILoadResultGraphics = _context.CTBILoadResultGraphics.ToList();
            if (ctBILoadResultGraphics != null && ctBILoadResultGraphics.Count > 0)
            {
                ctBILoadResultGraphics.ForEach(x =>
                {
                    x.UpdateTimestamp = x.UpdateTimestampOriginal.AddMinutes(minsToAdd);
                });
            }

            _context.SaveChanges();
        }

        public List<long> GetContainerAssemblyActualIds()
        {
            return _groundControlService.GetContainerAssemblyActualIds();
        }

        public void SyncLCForPastCases()
        {
            var completedOrPastCases = _context.GCCases.Include(x => x.CTCase).Where(x => 
                x.CTCase.DueTimestamp < DateTime.Now || 
                x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE ||
                x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID ||
                x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE ||
                x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID
            ).Include(x => x.GCTrays).ThenInclude(x => x.GCTraysLifecycleStatusTypes).ToList();
            if(completedOrPastCases != null || completedOrPastCases.Count > 0)
            {
                Log.Information($"Found {completedOrPastCases.Count} GCCases");
                completedOrPastCases.ForEach(gcCase =>
                {
                    Log.Information($"Processing GCCase: {gcCase.Title}");
                    bool isCaseComplete = gcCase.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE ||
                                gcCase.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID ||
                                gcCase.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE ||
                                gcCase.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID;

                    if (gcCase.GCTrays != null && gcCase.GCTrays.Count > 0)
                    {
                        Log.Information($"Found {gcCase.GCTrays.Count} GCTrays");
                        gcCase.GCTrays.ToList().ForEach(gcTray =>
                        {
                            if (gcTray.CTContainerTypeActualId.HasValue)
                            {
                                Log.Information($"Processing GCTrayID: {gcTray.Id}, ActualID: {gcTray.CTContainerTypeActualId.Value}");

                                var historyItems = _context.CTContainerTypeActualHistoryItems.Where(x =>
                                    x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value &&
                                    (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG || x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)).ToList();

                                Log.Information($"Found {historyItems.Count} HistoryItems");
                                if (historyItems != null && historyItems.Count > 0)
                                {
                                    if (gcTray.GCTraysLifecycleStatusTypes == null)
                                    {
                                        gcTray.GCTraysLifecycleStatusTypes = new List<GCTraysLifecycleStatusTypes>();
                                    }

                                    foreach (var hi in historyItems)
                                    {
                                        if(!gcTray.GCTraysLifecycleStatusTypes.Any(x => x.Timestamp == hi.UpdateTimestamp))
                                        {
                                            Log.Information($"No GCTrayLC for timestamp: {hi.UpdateTimestamp}");
                                            bool isLoaner = false;
                                            GCLifecycleStatusType gcLifecycleStatusType = null;

                                            if (string.IsNullOrWhiteSpace(hi.LocationElapsedCase) ||
                                                hi.LocationElapsedCase.Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION) ||
                                                hi.LocationElapsedCase.Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION))
                                            {
                                                isLoaner = true;
                                                gcLifecycleStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Code == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerInbound).FirstOrDefault();
                                                Log.Information($"Tray is a loaner!");
                                            }
                                            else
                                            {
                                                // Silly Hack because of CT
                                                var ctLocation = _groundControlService.GetCTLocationForLocationName(hi.LocationElapsedCase, hi.Status == Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER);
                                                gcLifecycleStatusType = _groundControlService.GetGCLifeCycleBasedOnLocationType(hi.UpdateTimestamp, ctLocation.CTLocationTypeId, gcTray.CTContainerTypeActualId.Value, null);
                                            }

                                            Log.Information($"Lifecycle: {gcLifecycleStatusType.Title}");


                                            gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes
                                            {
                                                IsActiveStatus = false,
                                                GCTrayId = gcTray.Id,
                                                User = hi.UpdateUserId,
                                                Timestamp = hi.UpdateTimestamp,
                                                TimestampOriginal = hi.UpdateTimestampOriginal,
                                                LifecycleStatusTypeId = gcLifecycleStatusType.Id
                                            });
                                        }
                                        _context.SaveChanges();
                                    }

                                    gcTray.GCTraysLifecycleStatusTypes = gcTray.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).ToList();

                                    for (int i = 0; i < gcTray.GCTraysLifecycleStatusTypes.ToList().Count(); i++)
                                    {
                                        var lc = gcTray.GCTraysLifecycleStatusTypes.ToList()[i];
                                        lc.IsActiveStatus = i == 0;
                                    }
                                    
                                }
                            }
                            _context.SaveChanges();
                        });
                    }
                });
            }
        }

        public void UpdateLCForCompletedCases(int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);
            _groundControlService.UpdateLCForCompletedCases(minsToAdd);
        }

        public void CancelOrDeleteCTCanceledCases(List<string> caseReferences, int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);
            _groundControlService.CancelOrDeleteCTCanceledCases(caseReferences, minsToAdd);
        }

        public void UpdateColorStatusForGCCasesAndGCTrays(List<CTCase> ctCasesToUse)
        {
            Log.Information("Running UpdateColorStatusForGCCasesAndGCTrays...");
            Log.Information($"{ctCasesToUse.Count} cases to color...");
            ctCasesToUse.ForEach(ctCase =>
            {
                var gcCase = _context.GCCases.Where(x => x.CTCase.CaseReference == ctCase.CaseReference).Include(x => x.GCTrays).FirstOrDefault();
                if (gcCase != null)
                {
                    Log.Information($"-Processing GCCase: {gcCase.Title}...");
                    if (gcCase.GCTrays != null)
                    {
                        gcCase.GCTrays.ToList().ForEach(gcTray =>
                        {
                            Log.Information($"--Processing GCTrayId: {gcTray.Id}, with actualID: {gcTray.CTContainerTypeActualId}");

                            //GCTray is Active: GCTray.CTCaseContainerId != NULL && GCTray.CTContainerTypeActualId != NULL && GCTray.GCCase.CTCase.CartStatus != "Completed"

                            // by default, setting to RED, because some trays will have an Actual, but not a Location, hence no LifeCycle, hence no Color,
                            // which in theory won't happen, but reality hits us strong
                            int colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Red;

                            bool isAcutalAvailable = gcTray.CTContainerTypeActualId.HasValue;
                            Log.Information($"--IsAcutalAvailable: {isAcutalAvailable}");

                            GCTraysLifecycleStatusTypes currentLifeCycleStatus = null;
                            var activeLifeCycleStatustes = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == gcTray.Id && x.IsActiveStatus == true).OrderByDescending(x => x.Timestamp).ToList();
                            if (activeLifeCycleStatustes.Count > 1)
                            {
                                var error = $"More than one active LifeCycle statuses detected for GCTrayId = ${gcTray.Id}";
                                Log.Error(error);
                                // do not throw, handle for now:
                                //throw new Exception(error);
                                // instead, set only latest to active:
                                for (int i = 0; i < activeLifeCycleStatustes.Count; i++)
                                {
                                    if (i == 0)
                                    {
                                        currentLifeCycleStatus = activeLifeCycleStatustes[i];
                                    }
                                    else
                                    {
                                        activeLifeCycleStatustes[i].IsActiveStatus = false;
                                    }
                                }
                                _context.SaveChanges();
                            }
                            else if (activeLifeCycleStatustes.Count > 0)
                            {
                                currentLifeCycleStatus = activeLifeCycleStatustes.First();
                            }
                            else // activeLifeCycleStatustes.Count == 0;
                            {
                                if(isAcutalAvailable)
                                {
                                    if (gcTray.IsActualCurrentlyAvailable.HasValue && gcTray.IsActualCurrentlyAvailable.Value)
                                    {
                                        colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Yellow;
                                        Log.Information($"--Acutal is Available, setting to YELLOW");
                                    }
                                    else
                                    {
                                        colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Orange;
                                        Log.Information($"--Acutal is NOT Available, setting to ORANGE");
                                    }
                                }
                            }

                            if (isAcutalAvailable)
                            {
                                if (currentLifeCycleStatus != null)
                                {
                                    if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.LoanerInbound)
                                    {
                                        colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Orange;
                                        Log.Information($"--Current Status is LoanerInbound, setting to Orange");
                                    }
                                    else if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.LoanerOutbound ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Loaded ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.ShippedOutbound ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Delivered ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.InSurgery ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.SurgeryComplete ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Prepped ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Picked ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.ShippedInbound ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Received)
                                    {
                                        colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Green;
                                    }
                                    else
                                    {
                                        if (!isAcutalAvailable)
                                        {
                                            colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Red;
                                        }
                                        else
                                        {
                                            // If in Induction, it's RED
                                            if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.DeconStaged ||
                                                    currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.DeconStagedLoaner ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Sink ||
                                                    currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.SinkLoaner ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Washer ||
                                                    currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.WasherLoaner ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.AssemblyStaged ||
                                                    currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.AssemblyStagedLoaner ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Assembly ||
                                                    currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.AssemblyLoaner ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.CleanIncomplete)
                                            {
                                                colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Yellow;
                                            }
                                            else if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.QualityHold)
                                            {
                                                colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Purple;
                                            }
                                            else if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged)
                                            {
                                                colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Yellow;
                                            }
                                            else if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Verification ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Sterilize ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.SterileStaged ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.CaseAssembly ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.TransportationStaging)
                                            {
                                                var caseDate = ctCase.DueTimestamp.Date;
                                                List<GCTray> traysForActual = new List<GCTray>();
                                                bool isOrange = false;

                                                // Checking if Actual is allocated to a more recent case, if Yes, current tray is ORANGE
                                                if (gcTray.CTContainerTypeActualId.HasValue)
                                                {
                                                    traysForActual = _context.GCTrays.Include(x => x.GCCase).ThenInclude(x => x.CTCase).Where(x => 
                                                        x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value && 
                                                        x.GCCase.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE &&
                                                        x.GCCase.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID &&
                                                        x.GCCase.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE &&
                                                        x.GCCase.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID &&
                                                        x.GCCase.CTCase.DueTimestamp > DateTime.UtcNow).OrderBy(x => x.GCCase.CTCase.DueTimestamp).ToList();
                                                    if (traysForActual != null && traysForActual.Count > 1)
                                                    {
                                                        var nextCase = traysForActual.First().GCCase;
                                                        if (nextCase.Id != gcCase.Id)
                                                        {
                                                            isOrange = true;
                                                        }
                                                    }
                                                }

                                                if (isOrange)
                                                {
                                                    colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Orange;
                                                }
                                                else
                                                {
                                                    colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Yellow;

                                                    //if (caseDate == DateTime.UtcNow.Date)
                                                    //{
                                                    var gcTrayActiveLifecycleStatuses = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == gcTray.Id && x.IsActiveStatus == true).ToList();
                                                    if (gcTrayActiveLifecycleStatuses != null && gcTrayActiveLifecycleStatuses.Count > 0)
                                                    {
                                                        if (gcTrayActiveLifecycleStatuses.Count > 1)
                                                        {
                                                            var error = $"More than one active LifeCycle statuses detected for GCTrayId = ${gcTray.Id}";
                                                            Log.Error(error);
                                                            throw new Exception(error);
                                                        }
                                                        var activeLifeCycleStatus = gcTrayActiveLifecycleStatuses.First();
                                                        if (activeLifeCycleStatus.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.SterileStaged ||
                                                            activeLifeCycleStatus.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.CaseAssembly ||
                                                            activeLifeCycleStatus.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.TransportationStaging)
                                                        {
                                                            colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Green;
                                                        }
                                                    }
                                                    //}
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            _groundControlService.UpdateGCTrayColorStatus(gcTray.Id, colorId);
                        });
                    }
                    _groundControlService.UpdateGCCaseColorStatus(gcCase.Id);
                }
            });
        }

        public void SyncColorForPastCases()
        {
            var allCases = _context.CTCases.Include(x => x.GCCase).Include(x => x.GCCase.GCTrays).ThenInclude(x => x.GCTraysLifecycleStatusTypes)
                .Where(x => x.DueTimestamp < DateTime.Now || x.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID).ToList();
            if(allCases != null && allCases.Count > 0)
            {
                UpdateColorStatusForGCCasesAndGCTrays(allCases);
            }
        }

        public void AddCTAssemblyGraphic(ETLCTAssemblyGraphic assemblyGraphic, int statusTypeId, long legacyId)
		{
            CTAssemblyGraphic graphic = new CTAssemblyGraphic() { 
                CensisSetHistoryId = assemblyGraphic.CensisSetHistoryId.ToString(),
                ContainerTypeId = assemblyGraphic.ContainerTypeId, 
                Description = assemblyGraphic.Description, 
                GraphicId = assemblyGraphic.GraphicId,
                LifecycleStatusTypeId = statusTypeId,
                ActualLegacyId = legacyId};
            _groundControlService.AddCTAssemblyGraphic(graphic);
		}

        public void AddCTAssemblyCountSheet(ETLCTContainerAssemblyCountSheetItem countSheet,string setName ,int statusTypeId)
        {
			CTAssemblyCountSheet cs = new CTAssemblyCountSheet()
			{
				ContainerTypeId = countSheet.SetId,
				Description = countSheet.ItemName,
                VendorName = countSheet.VendorName,
                ModelNumber = countSheet.ModelNumber,
                RequiredCount = countSheet.RequiredCount,
                ActualCount = countSheet.ActualCount,
                SetName = setName,
                TrayLifecycleStatusTypeId = statusTypeId
			};
			_groundControlService.AddCTAssemblyCountSheet(cs);
        }

        public List<GCTraysLifecycleStatusTypes> GetAssemblyStatuses()
		{
            return _groundControlService.GetAssemblyStatuses();
		}

        public void UpdateCTAssemblyGraphicsGraphic(int id, byte[] imageBytes)
        {
            var graphic = _context.CTAssemblyGraphics.Find(id);

            if (graphic != null)
            {
                var gcImage = new GCImage()
                {
                    Data = imageBytes,
                    Origin = Constants.CT_ABBREVIATION_TWO_CHAR,
                    Title = graphic.Description,
                    CreatedAtUtc = DateTime.UtcNow
                };

                graphic.GCImage = gcImage;
                _groundControlService.CreateThumbnailsForGCImage(gcImage);

                _context.SaveChanges();
            }
        }

        //public void UpdateTrayInductionSynchronization(string runAnyway)
        //{
        //    _groundControlService.UpdateTrayInductionSynchronization(runAnyway);
        //}

        public void UpdateGCTrayLifecycles(bool updateAllTrays)
        {
            _groundControlService.UpdateGCTrayLifecycles(updateAllTrays);
        }

        public void CreateGCImageThumbnails()
        {
            _groundControlService.CreateGCImageThumbnails();
        }

        public void CompleteBrokenTrayHistory(List<int> gcTrayIds, bool scanAllTrays, int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);
            _groundControlService.CompleteBrokenCTTrayHistory(gcTrayIds, minsToAdd, scanAllTrays);
        }

        public void UpdateGCTrayLapCount(bool? allTrays = false)
        {
            _groundControlService.UpdateGCTraysLapCount(allTrays);
        }

        public void UpdateHistoryItemAndLapCount(int serverOffsetInMinsFromUTC, bool? allHistoryItems = false)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);
            _groundControlService.UpdateAllHistoryItemsAndLapCount(minsToAdd, allHistoryItems);
        }
    }
}
