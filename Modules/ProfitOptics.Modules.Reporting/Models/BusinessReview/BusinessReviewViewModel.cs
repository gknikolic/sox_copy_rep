﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting.Models.BusinessReview
{
    public class BusinessReviewViewModel
    {
        public int MaxNumberOfCharactersInNote { get; set; }

        public List<BusinessReviewReportTypeClassSelectorsModel> ReportTypes { get; set; }
    }
}
