﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class TaxDetail
    {
        public decimal TotalTax { get; set; }
    }
}
