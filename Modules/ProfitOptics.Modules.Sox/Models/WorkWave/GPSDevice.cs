﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class GPSDevice
	{
		[JsonProperty("id")]
		public string Id { get; set; }
		[JsonProperty("label")]
		public string Label { get; set; }
		[JsonProperty("category")]
		public string Category { get; set; }
	}
}
