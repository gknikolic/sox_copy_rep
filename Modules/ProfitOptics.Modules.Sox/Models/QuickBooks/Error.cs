﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class Error
    {
        public string Message { get; set; }
        public string Detail { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("element")]
        public string Element { get; set; }
    }
}
