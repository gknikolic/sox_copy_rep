﻿using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Modules.Quotes.Data;

namespace ProfitOptics.Modules.Quotes.Services
{
    public class SalesRepService : ISalesRepService
    {
        private readonly Entities _context;

        public SalesRepService(Entities context)
        {
            _context = context;
        }

        public string GetSalesRepCode(int userId)
        {
            var salesRepCode = _context.QTUserProfiles.FirstOrDefault(p => p.UserId == userId)?.SalesRepCode;

            return salesRepCode;
        }

        public List<string> GetSalesRepCodesForManager(string managerCode)
        {
            var salesRepCodes = _context.QTUserProfiles.Where(p => p.SalesManagerCode == managerCode)
                    .Select(p => p.SalesRepCode).ToList();

            return salesRepCodes;
        }

        public List<int?> GetSalesRepIdsForManager(string managerCode)
        {
            var salesRepId = _context.QTUserProfiles.Where(p => p.SalesManagerCode == managerCode)
                    .Select(p => p.UserId).ToList();

            return salesRepId;
        }
    }
}