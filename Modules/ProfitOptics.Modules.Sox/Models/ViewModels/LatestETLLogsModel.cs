﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class LatestETLLogsModel
    {
        public ETLLogModel LatestETLLog { get; set; }
        public ETLLogModel LatestSuccessfulETLLog { get; set; }
    }
}
