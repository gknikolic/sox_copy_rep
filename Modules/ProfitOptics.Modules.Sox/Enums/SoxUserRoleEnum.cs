﻿namespace ProfitOptics.Modules.Sox.Enums
{
    public enum SoxUserRoleEnum
    {
        admin = 1,
        superadmin = 4,
        user = 5,
        vendoruser = 6,
        customeruser = 7
    }
}
