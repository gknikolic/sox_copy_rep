﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTShipToItemSummaryConfig : IEntityTypeConfiguration<QTShipToItemSummary>
    {
        public void Configure(EntityTypeBuilder<QTShipToItemSummary> builder)
        {
            builder.ToTable("QTShipToItemSummary");
            builder.HasKey(entity => new
            {
                entity.CustomerBillToId,
                entity.ItemId,
                entity.BillToNum,
                entity.ShipToNum,
                entity.ItemNum
            });

            builder.Property(entity => entity.CustomerBillToId).ValueGeneratedNever();
            builder.Property(entity => entity.ItemId).ValueGeneratedNever();

            builder.Property(entity => entity.Company).HasMaxLength(20);

            builder.Property(entity => entity.BillToNum).HasMaxLength(50);
            builder.Property(entity => entity.ShipToNum).HasMaxLength(50);
            builder.Property(entity => entity.ItemNum).HasMaxLength(50);
            builder.Property(entity => entity.LastDate).HasColumnType("date");

            builder.Property(entity => entity.LastSource).HasMaxLength(20);

        }
    }
}
