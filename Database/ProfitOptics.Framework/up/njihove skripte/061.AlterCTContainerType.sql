USE [Sox]

BEGIN TRANSACTION

	ALTER TABLE [dbo].[CTContainerType] DROP CONSTRAINT [CTContainerType_CTContainerType]
	
	ALTER TABLE [dbo].[CTContainerType]  WITH CHECK ADD  CONSTRAINT [CTContainerType_GCVendor] FOREIGN KEY([GCVendorId])
	REFERENCES [dbo].[GCVendor] ([Id])
	GO
	ALTER TABLE [dbo].[CTContainerType] CHECK CONSTRAINT [CTContainerType_GCVendor]
	GO
	
COMMIT TRANSACTION