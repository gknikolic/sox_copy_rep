﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.DataBrowser.Models
{
    public class Select2Result
    {
        public List<Select2Item> Results { get; set; }
    }
}