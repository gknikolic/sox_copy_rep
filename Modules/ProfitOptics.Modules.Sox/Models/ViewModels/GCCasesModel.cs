﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class GCCasesModel
	{
		public List<GCCaseModel> GCCases { get; set; }
		public IEnumerable<SelectListItem> GCCustomers { get; set; }
		public IEnumerable<SelectListItem> GCVendors { get; set; }
		public string SelectedCustomer { get; set; }
		public string SelectedVendor { get; set; }
		public string CaseReference { get; set; }
		public string Surgeon { get; set; }
		public ClaimsPrincipal User { get; set; }
		public bool AllCasesInSterileStagingForLoading { get; set; }
		public DateTime? DateFrom { get; set; } 
		public DateTime? DateTo { get; set; }
		public CaseScheduleColorType CaseScheduleColorType { get; set; }

		public GCCasesModel()
		{

		}
    }

}
