﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using PdfSharpCore;
using VetCV.HtmlRendererCore.PdfSharpCore;

namespace ProfitOptics.Framework.Core.PDF
{
    class HTML2PDF
    {
        public static void Convert(string sourceHTML, string destinationFile, PageSize pageSize)
        {
            using (var pdf = PdfGenerator.GeneratePdf(sourceHTML, PageSize.Letter))
            {
                pdf.Save(destinationFile);
            }
        }
    }
}
