﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable
{
    public class RegionQuotaPerformanceSalesRepModel
    {
        public int SalesRepId { get; set; }

        public string SalesRepName { get; set; }

        public bool IsRegionLeader { get; set; }

        public decimal MtdRevenue { get; set; }

        public decimal MtdQuota { get; set; }

        public decimal MtdPercentToQuotaTrend => MtdQuota == decimal.Zero ? decimal.Zero : MtdRevenue / MtdQuota;

        public int MtdRanking { get; set; }

        public decimal QtdRevenue { get; set; }

        public decimal QtdQuota { get; set; }

        public decimal QtdPercentToQuotaTrend => QtdQuota == decimal.Zero ? decimal.Zero : QtdRevenue / QtdQuota;

        public int QtdRanking { get; set; }

        public decimal YtdRevenue { get; set; }

        public decimal YtdQuota { get; set; }

        public decimal YtdPercentToQuotaTrend => YtdQuota == decimal.Zero ? decimal.Zero : YtdRevenue / YtdQuota;

        public int YtdRanking { get; set; }

        public decimal PreviousYtdRevenue { get; set; }

        public decimal CurrentYtdRevenue => YtdRevenue;

        public decimal YearOverYearComparison => PreviousYtdRevenue == decimal.Zero ? decimal.Zero : CurrentYtdRevenue / PreviousYtdRevenue;
    }
}