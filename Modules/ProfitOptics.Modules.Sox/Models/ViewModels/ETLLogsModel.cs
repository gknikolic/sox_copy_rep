﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class ETLLogsModel
    {
        public List<ETLLogModel> ETLLogs { get; set; }
    }
}
