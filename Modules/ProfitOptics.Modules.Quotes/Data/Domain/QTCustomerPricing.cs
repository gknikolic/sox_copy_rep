using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTCustomerPricing")]
    public partial class QTCustomerPricing
    {
        public string Company { get; set; }

        public int BillToNum { get; set; }

        public string ShipToNum { get; set; }

        public string ItemNum { get; set; }

        public string UOM { get; set; }

        public string PriceListCode { get; set; }

        public int SeqNum { get; set; }

        public decimal BasePrice { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool? CurrentContract { get; set; }

        public bool? CurrentBlitzedEnable { get; set; }
    }
}
