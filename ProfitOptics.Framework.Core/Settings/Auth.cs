﻿namespace ProfitOptics.Framework.Core.Settings
{
    public class Auth
    {
        public int SessionTimeout { get; set; }
        public bool EnableGoogleAuth { get; set; }
        public string GoogleClientId { get; set; }
        public string GoogleClientSecret { get; set; }
    }
}
