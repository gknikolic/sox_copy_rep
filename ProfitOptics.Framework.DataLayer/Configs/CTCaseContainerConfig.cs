﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTCaseContainerConfig : IEntityTypeConfiguration<CTCaseContainer>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTCaseContainer> builder)
        {
            builder.ToTable("CTCaseContainer");
            builder.HasKey(e => e.Id);
            builder.HasOne(d => d.CTCase)
                .WithMany(p => p.CTCaseContainers)
                .HasForeignKey(d => d.CTCaseId)
                .HasConstraintName("FK_CTCaseContainer_CTCase");
        }
    }
}
