﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Models
{
    public enum UserManagementServiceResultStatus
    {
        Success,
        Failed
    }
}
