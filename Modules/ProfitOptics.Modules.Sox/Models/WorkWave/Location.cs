﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class Location
	{
		[JsonProperty("address")]
		public string Address { get; set; }
	}
}
