﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerItem
    {
        public int Id { get; set; }
        public long LegacyId { get; set; }
        public decimal ProductId { get; set; }
        public int CTProductId { get; set; }
        public int CTContainerTypeId { get; set; }
        public int RequiredCount { get; set; }
        public string Placement { get; set; }
        public bool SubstitutionsAllowed { get; set; }
        public bool CriticalItem { get; set; }
        public string ChangeState { get; set; }
        public int SequenceNumber { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual CTProduct CTProduct { get; set; }
        public virtual CTContainerType CTContainerType { get; set; }
    }
}
