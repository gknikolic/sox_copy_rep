﻿//app.activeMenus = ["menuFieldService", "menuFieldServiceSearch"];

function goToTray() {

	var code = $("#code").val();

	$.ajax({
		type: "GET",
		url: "GoToTray",
		contentType: 'application/json',
		data: { code: code },
		success: function (response) {
			if (response == "NotFound") {
				document.getElementById('barcodeWarning').innerHTML = "Tray with entered barcode does not exist!"
				document.getElementById('barcodeWarning').style.color = "red";
			}
			else {
				window.location.href = response;
			}
		},
		error: function (response) {
			var error = 400;
		}
	});

}

function InitBarcodeScanner() {
	var width = $("#camera").innerWidth();
	var height = $("#camera").innerHeight();
	console.log(width, height);
	Quagga.init({
		inputStream: {
			name: "Live",
			type: "LiveStream",
			constraints: {
				width: width,
				height: height,
				facingMode: "environment"
			},
			target: document.querySelector('#camera')    // Or '#yourElement' (optional)

		},
		decoder: {
			//readers: ["code_128_reader", "ean_reader", "ean_8_reader", "code_39_reader", "code_39_vin_reader", "codabar_reader", "upc_reader", "upc_e_reader"]
			readers: ["code_128_reader", "code_39_reader"]
		}
	}, function (err) {
		if (err) {
			console.log(err);
			return
		}
		console.log("Initialization finished. Ready to start");
		Quagga.start();
	});

	Quagga.onDetected(function (result) {
		var code = result.codeResult.code;
		console.log(code);
		$("#code").val(code);
		document.getElementById("code").scrollIntoView();
		document.getElementById("cameraBox").setAttribute("style", "background-color: palegreen;");
		//Quagga.stop();
	});
}

function ResizeCamera() {
	InitBarcodeScanner();
}

//$(document).ready(function () {
//	$(window).resize(function () {
//		clearTimeout(window.resizedFinished);
//		window.resizedFinished = setTimeout(function () {
//			console.log('Resized finished.');
//			ResizeCamera();
//		}, 250);
//	});
//	var windowsHeight = $("body").innerHeight();
//	var windowPercentage = 0.30
//	//$('#camera').height(windowsHeight * windowPercentage);
//	InitBarcodeScanner();

//});

