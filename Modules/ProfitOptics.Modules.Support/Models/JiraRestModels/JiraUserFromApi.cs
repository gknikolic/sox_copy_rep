﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class JiraUserFromApi
    {
        public string Self { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string DisplayName { get; set; }
    }
}
