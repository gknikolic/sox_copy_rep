﻿namespace ProfitOptics.Modules.Reporting.Models.ReportFilter
{
    public class UserFilterModel
    {
        public UserFilterModel(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get; }

        public string Name { get; }
    }
}