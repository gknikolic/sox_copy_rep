﻿using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum ComparisonType
    {
        [Display(Name = "Month to date")]
        MonthToDateYearOverYear = 0,

        [Display(Name = "Quarter to date")]
        QuarterToDateYearOverYear = 1,

        [Display(Name = "Year to date")]
        YearToDateYearOverYear = 2,

        [Display(Name = "Last 12 months")]
        Last12Months = 3,

        [Display(Name = "Custom date range")]
        CustomDateRangeYearOverYear = 4
    }
}