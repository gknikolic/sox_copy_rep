﻿using ComponentSpace.Saml2.Assertions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.DataLayer.GroundControl;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.Anotations;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Sox.Factories;
using ProfitOptics.Modules.Sox.Infrastructure;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using ProfitOptics.Modules.Sox.Models.QuickBooks;
using ProfitOptics.Modules.Sox.Models.ViewModels;
using ProfitOptics.Modules.Sox.Models.ViewModels.Paging;
using ProfitOptics.Modules.Sox.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Controllers
{
    [Area("Sox")]
    [Authorize]
    public class SoxController : BaseController
    {
        private readonly ISoxFactory _SoxFactory;
        private readonly ISoxService _soxService;
        private readonly IGroundControlService _groundControlService;
        private readonly IUserService _userService;
        private readonly IHelperService _helperService;
        private IWebHostEnvironment _environment;
        private readonly ISoxLogger soxLogger;
        private readonly ILogger<SoxController> _nlogger;
        private readonly IReportingService _reportingService;
        public GlobalOptions GlobalOptions { get; set; }


        public SoxController(ISoxFactory SoxFactory,
                             ISoxService _soxService,
                             IGroundControlService _groundControlService,
                             IUserService _userService,
                             IHelperService _helperService,
                             ISoxLogger soxLogger,
                             IWebHostEnvironment _environment,
                             ILogger<SoxController> nlogger,
                             IReportingService reportingService,
                             GlobalOptions globalOptions)
        {
            _SoxFactory = SoxFactory;
            this._soxService = _soxService;
            this._groundControlService = _groundControlService;
            this._userService = _userService;
            this._helperService = _helperService;
            this._environment = _environment;
            this.soxLogger = soxLogger;
            this._nlogger = nlogger;
            _reportingService = reportingService;
            this.GlobalOptions = globalOptions;
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet("CaseList")]
        public IActionResult Index(GCCasesModel filterModel)
        {
            ///test
            ///
            //_soxService.UpdateCaseBarcodesAsync(1156);
            filterModel.User = User;
            var model = _SoxFactory.PrepareGCCasesModel(filterModel);
            return View(model);
            //return View();
        }

        public async Task<IActionResult> UpdateCaseBarcodesInWWAsync(int caseId)
		{
            try
			{
               await  _soxService.UpdateCaseBarcodesAsync(caseId);
                return Ok();
            }
            catch(Exception ex)
			{
                return BadRequest();
			}
          
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser,driver")]
        [HttpGet]
        public IActionResult Dashboard(DashboardModel filterModel)
        {
            if (User.IsInRole(ApplicationRoleNames.Driver))
                return RedirectToAction("ManageGCCaseLifecycleStatuses");
            else if(User.IsInRole(ApplicationRoleNames.CustomerUser) || User.IsInRole(ApplicationRoleNames.VendorUser))
			{
                return RedirectToAction("CaseSchedule");
            }

            filterModel.User = User;
            //filterModel.UserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            //_nlogger.LogInformation("Hello, this is the index!");
            var dashboardModel = _SoxFactory.PrepareDashboardModel(filterModel);

            return View(dashboardModel);
        }
        public IActionResult DashboardNew(DashboardModel filterModel)
        {
            if (User.IsInRole(ApplicationRoleNames.Driver))
                return RedirectToAction("ManageGCCaseLifecycleStatuses");
            else if (User.IsInRole(ApplicationRoleNames.CustomerUser) || User.IsInRole(ApplicationRoleNames.VendorUser))
            {
                return RedirectToAction("CaseSchedule");
            }
            filterModel.User = User;
            filterModel.GetForSpecificTimeframe = true;
            filterModel.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month,1);
            filterModel.EndDate = filterModel.StartDate.AddMonths(1);
            var dashboardModel = _SoxFactory.PrepareNewDashboardModel(filterModel);
            return View(dashboardModel);
        }
        [HttpPost]
        public IActionResult GetDashboardMonthEvents([FromBody]DashboardModel filterModel)
        {
            filterModel.User = User;
            filterModel.GetForSpecificTimeframe = true;
            filterModel.EndDate = filterModel.StartDate.AddMonths(1); //get for one month
            var dashboardModel = _SoxFactory.PrepareNewDashboardModel(filterModel);
            return Json(dashboardModel);
        }
        [HttpPost]
        public IActionResult GetCalendarDayDetails([FromBody] DashboardModel filterModel)
		{
            filterModel.User = User;
            filterModel.GetForSpecificTimeframe = true;
            filterModel.EndDate = filterModel.StartDate.AddMonths(1);
            var model = _SoxFactory.PrepareDashboardHourPreviewModel(filterModel);
            return PartialView("_DashboardHourPreview", model);
        }

        [HttpPost]
        public IActionResult GetCalendarHourDetails([FromBody] DashboardModel filterModel)
        {
            filterModel.User = User;
            var model = _SoxFactory.PrepareDashboardDetailedHourPreviewModel(filterModel);
            return PartialView("_DashboardDetailedHourPreview", model);
        }

        [HttpGet]
        public IActionResult GetCalendarCaseDetails(int caseId)
        {
            var model = _SoxFactory.PrepareDashboardCaseDetailsPreviewModel(caseId);
            return PartialView("_DashboardCaseDetailsPreview", model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult CasePlanning()
        {
            CasePlanningModel model = new CasePlanningModel();
            model.WWDrivers = _soxService.GetWWDriverSelectList();
            model.WWVehicles = _soxService.GetWWVehicleSelectList();

            return View(model);
        }

        //[Authorize(Roles = "driver")]
        [HttpGet]
        public IActionResult ScanningSearch(bool generalInformation)
        {
            if (generalInformation == true)
            {
                ViewBag.GeneralInforamtion = true;
            }
            else
            {
                ViewBag.GeneralInforamtion = false;
            }

            return View();
        }

        //[Authorize(Roles = "driver")]
        [HttpGet]
        public IActionResult ScanningTray(int customerId)
        {
            ScanningTray model = new ScanningTray();
            model.CustomerId = customerId;
            return View(model);
        }

        [HttpGet]
        public IActionResult RedirectToScanningTray(int customerId)
        {
            return Json(Url.Action("ScanningTray", "Sox", new { customerId = customerId }));
        }

        [HttpGet]
        public IActionResult RedirectToIsTrayInPreppedStatus(int trayId)
        {
            return Json(Url.Action("IsTrayInPreppedStatus", "Sox", new { trayId = trayId }));
        }

        [HttpGet]
        public IActionResult GetGCImageById(int imageId)
        {
            var src = _soxService.GetImageSource(imageId);

            return Json(src);
        }

        [HttpGet]
        public IActionResult GetGCImageWithCountSheetById(int imageId, bool isAssembly, int statusId)
        {
            var model = _soxService.GetPhotoWithCountSheet(imageId,isAssembly,statusId);

            return PartialView("_ImageAndCountSheetCarousel", model);
        }

        [HttpGet]
        public IActionResult ImageFile(int id)
        {
            var bytes = _soxService.GetImageBytes(id);
            return File(bytes, System.Net.Mime.MediaTypeNames.Application.Octet, "image.jpg");
        }


        [Authorize(Roles = "superadmin,admin,driver")]
        [HttpGet]
        public IActionResult TrayPickup()
        {
            var gcCustomers = _soxService.GetGCCustomersSelectListByDate(DateTime.Now);

            TrayPickupModel model = new TrayPickupModel();
            model.GCCustomers = gcCustomers;
            return View(model);
        }

        //[Authorize(Roles = "driver")]
        //[HttpPost]
        //public IActionResult StoreTrayPicturesAndNote()
        //{
        //    var files = HttpContext.Request.Form.Files;

        //    return null;
        //}

        //[Authorize(Roles = "driver")]
        [HttpGet]
        public IActionResult IsTrayInPreppedStatus(string code, int trayId)
        {
            IsTrayInPreppedStatusModel model = new IsTrayInPreppedStatusModel()
            {
                Code = code,
                TrayId = trayId
            };

            return View(model);
        }

        //[Authorize(Roles = "driver")]
        [HttpGet]
        public IActionResult PhotoCapture(bool isYes, string code, int trayId)
        {
            _soxService.SetTrayStatus(code, "Pickup", trayId, User);

            _soxService.UpdateBarcodeToWWAsync(trayId);
            var tray = _soxService.GetTrayById(trayId);
            var traysForCase = _soxService.GetTraysForGCCase((int)tray.GCCaseId);

            int traysProcessed = 0;
            foreach (var item in traysForCase)
            {
                if (_soxService.CheckTrayWasInStatus("Pickup", item.Id))
                    traysProcessed++;
            }

            if (traysProcessed == traysForCase.Count)
                _soxService.SetOrderStatusInWWAsync((int)tray.GCCaseId, Models.WorkWave.RouteStepStatusType.done);

            if (isYes)
            {
                _soxService.Foamed(trayId, code, true);
            }
            else
            {
                _soxService.Foamed(trayId, code, false);
            }

            PhotoCaptureModel model = new PhotoCaptureModel();
            model.IsYes = isYes;
            model.Code = code;
            model.TrayId = trayId;
            model.TrayImages = _soxService.GetTrayImagesByBarcode(code);

            return View(model);
        }

        //[HttpGet]
        //public IActionResult Foamed(string code)
        //{
        //    _soxService.Foamed(code);

        //    return Ok();
        //    //return RedirectToAction("PhotoCapture", new { isYes = isYes});
        //}

        //[Authorize(Roles = "driver")]
        [HttpGet]
        public IActionResult StoreNote(string note, string code, int trayId)
        {
            //_soxService.SetTrayStatus(code, "Prepped");
            _soxService.StoreNote(note, trayId);
            _soxService.SetOrderStatusInWWIfTraysArePickedUp(trayId);

            return Json(Url.Action("TrayPickup", "Sox"));
        }

        [HttpGet]
        public IActionResult PassPhotoCapture(string code, int trayId)
        {
            _soxService.SetOrderStatusInWWIfTraysArePickedUp(trayId);
            //_soxService.SetTrayStatus(code, "Pickup");

            return Json(Url.Action("TrayPickup", "Sox"));
        }

        //[Authorize(Roles = "driver")]
        [HttpGet]
        public async Task<IActionResult> UpdateBarcodeToWW(string code, int customerId)
        {
            var trays = _soxService.CheckTrayBarcodeForCustomer(code, customerId);



            if (trays.Count == 1)
            {
                //var status = _soxService.GetLifecycleStatusByTitle("Prepped");          
                //var trayStatus = _soxService.GetActiveLifecycleStatusAndTimestampForTray(trays[0].Id);

                var wasInStatus = _soxService.CheckTrayWasInStatus("Pickup", trays[0].Id);

                //if (trayStatus.LifecycleStatus.Order >= status.Order)
                if (wasInStatus)
                    return Ok("AlreadyProcessed");

                //await _soxService.UpdateBarcodeToWWAsync((int)trays[0].Id);
            }
            else if (trays.Count > 1)
            {
                var model = new ChooseCaseModel();
                model.Trays = new List<TrayModel>();

                foreach (var tray in trays)
                {
                    var trayModel = new TrayModel();
                    trayModel.Id = tray.Id;
                    trayModel.CaseReference = tray.GCCase.CTCase.CaseReference;

                    model.Trays.Add(trayModel);
                }

                return PartialView("_ChooseCaseFromSameCustomer", model);
            }
            else
            {
                return Ok("NotFound");
            }

            return Json(Url.Action("IsTrayInPreppedStatus", "Sox", new { code = code, trayId = trays[0].Id }));
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser,driver")]
        [HttpGet]
        public IActionResult ManageGCCaseLifecycleStatuses()
        {
            var model = new ManageGCCaseLifecycleStatuses();

            model.GCCases = _soxService.GetGCCasesList();
            model.GCLifecycleStatuses = _soxService.GetGCLifecycleStatusesList();

            return View(model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult ChangeGCCaseLifecycleStatus(ManageGCCaseLifecycleStatuses model)
        {
            //validation
            var statusForCase = _soxService.GetActiveLifecycleStatusForCase(model.SelectedGCCase);
            var newStatus = _soxService.GetLifecycleStatusById(model.SelectedGCLifecycleStatus);

            if (newStatus.Order < statusForCase.Order)
                return View("WarningPage");

            _soxService.ChangeGCCaseLifecycleStatus(model);

            return RedirectToAction("ManageGCCaseLifecycleStatuses");
        }

        [Authorize(Roles = "superadmin,admin")]
        [HttpGet]
        public IActionResult DeleteGCCase(int gcCaseId)
        {
            _soxService.DeleteGCCase(gcCaseId);

            return Ok();
        }

        [Authorize(Roles = "superadmin,admin")]
        [HttpGet]
        public IActionResult DeleteAllGCCases(int key)
        {
            if (key == 123)
            {
                _soxService.DeleteAllGCCases();
                return Ok("success");
            }
            else
            {
                return BadRequest();
            }

        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult GetLifecycleStatusForCase(int gcCaseId)
        {
            var status = _soxService.GetActiveLifecycleStatusForCase(gcCaseId);

            return Json(new { status = status });
        }
        //public IActionResult CasesByDate(DateTime date)
        //{
        //    var cases = _SoxFactory.PrepareCasesForPlanning(date, User);

        //    return PartialView("_CasesByDate", cases);
        //}

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult GetGCCasesByDate(DateTime date)
        {
            var cases = _SoxFactory.PrepareCasesForPlanning(date, User, null, null, null);

            return PartialView("_CasesByDate", cases);
        }

        public IActionResult LocationInfos(int locationId)
        {
            LocationInfoPagingModel pagingModel = new LocationInfoPagingModel();

            var locationInfos = _soxService.GetLocationInfosPaged(pagingModel, locationId, null, null);
            var location = _soxService.GetGCLocationById(locationId);

            //foreach (var locInfo in locationInfos)
            //{
            //    locInfo.LocationTitle = location.Title;
            //}


            var model = new GCLocationInfosModel()
            {
                LocationInfos = locationInfos,
                LocationId = locationId,
                LocationTitle = location?.Title
            };

            return View(model);
        }

        public IActionResult ETLLogs()
        {
            var model = _soxService.ETLLogs();

            return View(model);
        }

        public IActionResult LoadLocations(DateTime? date)
        {
            LocationInfoPagingModel pagingModel = new LocationInfoPagingModel();

            int start = Convert.ToInt32(Request.Query["start"]);

            string startDateStr = Request.Query["startDate"];
            string endDateStr = Request.Query["endDate"];
            string format = "ddd MMM dd yyyy HH:mm:ss 'GMT'K";
            var locationId = Int32.Parse(Request.Query["locationId"]);
        
            DateTime startDate;
            if (startDateStr != "")
            {
                DateTime.TryParseExact(startDateStr, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
            }
            else
            {
                startDate = DateTime.Now.AddDays(-1);
            }

            DateTime endDate;
            if (endDateStr != "")
            {
                DateTime.TryParseExact(endDateStr, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);
            }
            else
            {
                endDate = DateTime.Now;
            }


            int pageSize = Convert.ToInt32(Request.Query["length"]);
            if (pageSize == 0)
                pageSize = 10;

            int pageNumber = start / pageSize + 1;

            if (pageNumber != 0)
                pagingModel.PageNumber = pageNumber;
            if (pageSize != 0)
                pagingModel.PageSize = pageSize;

            pagingModel.SearchValue = Request.Query["search[value]"];

            var locationInfos = _soxService.GetLocationInfosPaged(pagingModel, null, startDate, endDate);

            if (locationId != null)
            {
                locationInfos = _soxService.GetLocationInfosPaged(pagingModel, locationId, startDate, endDate);

                var location = _soxService.GetGCLocationById(locationId);

                //foreach (var locInfo in locationInfos)
                //{
                //    locInfo.LocationTitle = location.Title;
                //}
            }



            return Json(
                        new
                        {
                            data = locationInfos,
                            draw = Request.Query["draw"],
                            recordsFiltered = locationInfos.TotalCount, //locationInfos.Count,
                            recordsTotal = locationInfos.TotalCount,
                            pageActive = pageNumber
                        }
                    );
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult FilterGCCasesByDate(DateTime date, string caseReference, int vehicleId, int driverId)
        {
            var cases = _SoxFactory.PrepareCasesForPlanning(date, User, caseReference, vehicleId, driverId);

            return PartialView("_CasesByDate", cases);
        }

        //[HttpPost]
        //[AjaxCall(DataType = AjaxCallDataType.Html)]
        //public IActionResult GetEventDetails(DateTime date)
        //{
        //    var model = _SoxFactory.PrepareEventPreviewModel(date);
        //    return Json(model);
        //    return PartialView("_EventPreviewDialog", model);
        //}

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult GetEventDetails(DateTime date, string btnColor)
        {
            var model = _SoxFactory.PrepareEventPreviewModel(date, User);

            model.BtnColor = btnColor;

            return PartialView("_EventPreviewDialog", model);
        }

        //[HttpPost]
        //public IActionResult CasesFilter(DashboardModel filterModel)
        //{
        //    var model = _SoxFactory.PrepareDashboardModel(filterModel);

        //    return View("Dashboard", model);
        //}

        [Authorize(Roles = "superadmin,admin,user")]
        [HttpGet]
        public IActionResult CustomerManagement()
        {
            var model = _SoxFactory.PrepareGCCustomersModel();
            string qbInstanceUrl = null;
            var qbOptions = _groundControlService.GetQuickBooksOptions();
            if (qbOptions != null && !string.IsNullOrWhiteSpace(qbOptions.InstanceUrl))
            {
                qbInstanceUrl = qbOptions.InstanceUrl;
            }
            model.QBCusomerExternalUrl = qbInstanceUrl;
            return View(model);
        }

        [Authorize(Roles = "superadmin,admin,user")]
        [HttpGet]
        public IActionResult VendorManagement()
        {
            var model = _SoxFactory.PrepareGCVendorsModel();

            return View(model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [Route("CaseDetails/{id}")]
        [HttpGet]
        public IActionResult CaseDetails(int id)
        {
            bool hasPermission = false;
            var cases = _soxService.FilterCasesByUserRole(User);
            foreach (var item in cases)
            {
                if (item.Id == id)
                    hasPermission = true;
            }

            var model = _SoxFactory.PrepareCaseDetailsModel(id, User);
            if (model == null)
                return Forbid();

            if (hasPermission)
                return View(model);
            else
                return Forbid();
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult UpdateCustomer(UpdateCustomerModel model)
        {
            _soxService.UpdateCustomer(model);

            return RedirectToAction("PartnerManagement", "Users", new { area = "UserManagement" });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult UpdateVendor(UpdateVendorModel model)
        {
            _soxService.UpdateVendor(model);

            return RedirectToAction("PartnerManagement", "Users", new { area = "UserManagement" });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult TraysManagement(int? actualId)
        {
            var model = _SoxFactory.PrepareTraysModel(User, actualId);

            return View(model);
        }
        public IActionResult CaseSchedule(GCCasesModel filterModel)
        {
            filterModel.User = User;
            if(filterModel.DateFrom == null)
			{
                filterModel.DateFrom = DateTime.Now.TrimHoursMinutesSeconds();
			}
            if(filterModel.DateTo == null)
			{
                filterModel.DateTo = ((DateTime)filterModel.DateFrom).AddDays(1);
            }
            var model = _SoxFactory.PrepareGCCasesModelForCaseSchedule(filterModel);
            return View(model);
        }
        [HttpGet]
        public IActionResult FacilityManagement()
        {
            var locationsModel = _soxService.GetGCLocationsModel();

            return View(locationsModel);
        }
        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult SystemAlerts()
        {
            GCSystemAlertsModel model = _SoxFactory.PrepareSystemAlerts(User);

            return View(model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public JsonResult MarkAlertReadUrl(int id)
        {
            bool success = false;
            string message = "Something went wrong";

            success = _soxService.MarkGCSystemAlertRead(id);
            if(success)
            {
                message = "Success!";
            }

            return Json(new { success, message });
        }

        //[Authorize(Roles = "driver")]
        [HttpGet]
        public IActionResult GoToTray(string code)
        {
            var actualId = _soxService.GetActualIdByTrayBarcode(code);
            if (actualId == null)
            {
                return Json("NotFound");
            }

            bool userCanViewTray = false;

            if (User.IsInRole(ApplicationRoleNames.SuperAdmin) || User.IsInRole(ApplicationRoleNames.Admin) || User.IsInRole(ApplicationRoleNames.User))
            {
                userCanViewTray = true;
            }
            else
            {
                var cases = _soxService.FilterCasesByUserRole(User);
                foreach (var gcCase in cases)
                {
                    var traysForCase = _soxService.GetTraysForGCCase(gcCase.Id);
                    if (traysForCase != null && traysForCase.Count > 0)
                    {
                        userCanViewTray = traysForCase.Any(x => x.CTContainerTypeActualId.HasValue && x.CTContainerTypeActualId.Value == actualId);
                        if (userCanViewTray)
                        {
                            break;
                        }
                    }
                }
            }

            if(userCanViewTray)
            {
                if(User.IsInRole(ApplicationRoleNames.VendorUser) || User.IsInRole(ApplicationRoleNames.CustomerUser))
				{
                    return Json(Url.Action("TrayDetailsForVendors", "Sox", new { actualId = actualId }));
                }
                else
				{
                    return Json(Url.Action("TraysManagement", "Sox", new { actualId = actualId }));
                }
                
            }
            else
            {
                return Json("NotFound");
            }
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult AddCustomerDialog()
        {
            var companies = _userService.GetGCParentCompanies();
            AddCustomerModel model = new AddCustomerModel();
            model.Customer = new GCCustomerModel();
            model.Customer.GCParentCompanies = companies;
            model.Customer.QBBillingType = (int)ProfitOptics.Modules.Sox.Enums.QBBillingType.Tray;

            return PartialView("_AddCustomerModal", model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult EditCustomerDialog(int customerId)
        {
            var companies = _userService.GetGCParentCompanies();
            UpdateCustomerModel model = new UpdateCustomerModel();
            model.Customer = _SoxFactory.PrepareGCCustomerModel(customerId);
            model.Customer.GCParentCompanies = companies;
            if (model.Customer.QBBillingType == 0)
            {
                model.Customer.QBBillingType = (int)ProfitOptics.Modules.Sox.Enums.QBBillingType.Tray;
            }

            return PartialView("_UpdateCustomerModal", model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult EditVendorDialog(int vendorId)
        {
            var companies = _userService.GetGCParentCompanies();
            UpdateVendorModel model = new UpdateVendorModel();
            model.Vendor = _SoxFactory.PrepareGCVendorModel(vendorId);
            model.Vendor.GCParentCompanies = companies;
            //if (model.Customer.QBBillingType == 0)
            //{
            //    model.Customer.QBBillingType = (int)ProfitOptics.Modules.Sox.Enums.QBBillingType.Tray;
            //}

            return PartialView("_UpdateVendorModal", model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult AddVendorDialog()
        {
            var companies = _userService.GetGCParentCompanies();
            AddVendorModel model = new AddVendorModel();
            model.Vendor = new GCVendorModel();
            model.Vendor.GCParentCompanies = companies;

            return PartialView("_AddVendorModal", model);
        }

        //[Authorize(Roles = "driver")]
        [HttpGet]
        public IActionResult GetTraysForCustomer(int customerId)
        {
            var model = _SoxFactory.PrepareTarysForCaseModel(customerId);

            return PartialView("_TraysForCaseTable", model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult AddCustomer(AddCustomerModel model)
        {

            _soxService.AddCustomer(model);

            return RedirectToAction("PartnerManagement", "Users", new { area = "UserManagement" });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult AddVendor(AddVendorModel model)
        {
            _soxService.AddVendor(model);

            return RedirectToAction("PartnerManagement", "Users", new { area = "UserManagement" });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult DeactivateCustomerDialog(int customerId)
        {
            ViewBag.CustomerId = customerId;

            return PartialView("_DeactivateCustomerDialog");
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult DeactivateVendorDialog(int vendorId)
        {
            ViewBag.VendorId = vendorId;

            return PartialView("_DeactivateVendorDialog");
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult DeactivateCustomer(GCCustomerModel model)
        {
            _soxService.DeactivateCustomer(model);
            return RedirectToAction("PartnerManagement", "Users", new { area = "UserManagement" });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult DeactivateVendor(GCVendorModel model)
        {
            _soxService.DeactivateVendor(model);
            return RedirectToAction("PartnerManagement", "Users", new { area = "UserManagement" });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public JsonResult SubmitCustomerToQB(int customerId)
        {
            GCCustomer customer = _soxService.GetCustomer(customerId);
            bool success = false;
            string message = "Something went wrong";

            if (customer != null)
            {
                Models.QuickBooks.Customer qbCustomer = _groundControlService.MapGcToQbCustomer(customer);
                var qbCustomerResponse = _groundControlService.EditQBCustomer(qbCustomer, _helperService.GetQuickBooksAccessToken(User.GetUserId()));

                if (qbCustomerResponse.Fault != null && qbCustomerResponse.Fault.Errors.Count > 0)
                {
                    success = false;
                    message = qbCustomerResponse.Fault.Errors[0].Detail;
                }
                else
                {
                    if (qbCustomerResponse.Customer != null)
                    {
                        try
                        {
                            DateTime? startTime = customer.DeliveryStart != null ? Converters.ConvertSecondsToDateTime(customer.DeliveryStart) : (DateTime?)null;
                            DateTime? endTime = customer.DeliveryEnd != null ? Converters.ConvertSecondsToDateTime(customer.DeliveryEnd) : (DateTime?)null;

                            var customerModel = new UpdateCustomerModel()
                            {
                                Customer = new GCCustomerModel()
                                {
                                    Active = customer.Active,
                                    CustomerName = customer.CustomerName,
                                    DeliverName = customer.DeliverName,
                                    DeliverNotes = customer.DeliverNotes,
                                    DeliverAccessCode = customer.DeliverAccessCode,
                                    DeliverPhoneNumber = customer.DeliverPhoneNumber,
                                    DeliveryCity = customer.DeliveryCity,
                                    FacilityId = customer.FacilityId,
                                    FacilityCode = customer.FacilityCode,
                                    DeliveryStart = startTime.HasValue ? startTime.Value.ToString("hh:mm tt") : null,
                                    DeliveryEnd = endTime.HasValue ? endTime.Value.ToString("hh:mm tt") : null,
                                    DeliveryState = customer.DeliveryState,
                                    DeliveryStreet = customer.DeliveryStreet,
                                    DeliveryZip = customer.DeliveryZip,
                                    QBCustomerId = qbCustomerResponse.Customer.Id,
                                    Id = customer.Id,
                                    WWDepoId = customer.WWDepoId,
                                    ServiceTime = customer.ServiceTime / 60,
                                    APContactName = customer.APContactName,
                                    APEmail = customer.APEmail,
                                    APPhone = customer.APPhone
                                }
                            };
                            _soxService.UpdateCustomer(customerModel, false);
                            success = true;
                            message = Helpers.Constants.QBO_RESULT_MESSAGE_SUCCESS;
                        }
                        catch (Exception e)
                        {
                            success = false;
                            message = e.Message;
                        }
                    }
                }
            }

            return Json(new { success = success, message = message });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public JsonResult UpdateCustomerFromQB(int customerId)
        {
            GCCustomer customer = _soxService.GetCustomer(customerId);
            bool success = false;
            string message = "Something went wrong";

            if (customer != null)
            {
                try
                {
                    var qbCustomerResponse = _groundControlService.GetQBCustomer(customer.QBCustomerId.Value, _helperService.GetQuickBooksAccessToken(User.GetUserId()));

                    var contactName = new List<string>();
                    contactName.Add(qbCustomerResponse.GivenName);
                    contactName.Add(qbCustomerResponse.MiddleName);
                    contactName.Add(qbCustomerResponse.FamilyName);
                    customer.APContactName = string.Join(" ", contactName);

                    if (qbCustomerResponse != null)
                    {
                        DateTime? startTime = customer.DeliveryStart != null ? Converters.ConvertSecondsToDateTime(customer.DeliveryStart) : (DateTime?)null;
                        DateTime? endTime = customer.DeliveryEnd != null ? Converters.ConvertSecondsToDateTime(customer.DeliveryEnd) : (DateTime?)null;

                        var customerModel = new UpdateCustomerModel()
                        {
                            Customer = new GCCustomerModel()
                            {
                                Active = qbCustomerResponse.Active,
                                CustomerName = qbCustomerResponse.DisplayName,
                                DeliverName = qbCustomerResponse.DisplayName,
                                DeliverNotes = qbCustomerResponse.Notes,
                                DeliverAccessCode = customer.DeliverAccessCode,
                                DeliverPhoneNumber = qbCustomerResponse.AlternatePhone != null ? qbCustomerResponse.AlternatePhone.FreeFormNumber : customer.DeliverPhoneNumber,
                                DeliveryCity = qbCustomerResponse.BillAddr != null ? qbCustomerResponse.BillAddr.City : customer.DeliveryCity,
                                FacilityId = customer.FacilityId,
                                FacilityCode = customer.FacilityCode,
                                DeliveryStart = startTime.HasValue ? startTime.Value.ToString("hh:mm tt") : null,
                                DeliveryEnd = endTime.HasValue ? endTime.Value.ToString("hh:mm tt") : null,
                                DeliveryState = qbCustomerResponse.BillAddr != null ? qbCustomerResponse.BillAddr.CountrySubDivisionCode : customer.DeliveryState,
                                DeliveryStreet = qbCustomerResponse.BillAddr != null ? qbCustomerResponse.BillAddr.Line1 : customer.DeliveryStreet,
                                DeliveryZip = qbCustomerResponse.BillAddr != null ? qbCustomerResponse.BillAddr.PostalCode : customer.DeliveryZip,
                                QBCustomerId = customer.QBCustomerId,
                                Id = customer.Id,
                                WWDepoId = customer.WWDepoId,
                                ServiceTime = customer.ServiceTime / 60,
                                APContactName = customer.APContactName,
                                APEmail = qbCustomerResponse.PrimaryEmailAddr != null ? qbCustomerResponse.PrimaryEmailAddr.Address : customer.APEmail,
                                APPhone = qbCustomerResponse.PrimaryPhone != null ? qbCustomerResponse.PrimaryPhone.FreeFormNumber : customer.APPhone
                            }
                        };
                        _soxService.UpdateCustomer(customerModel, false);
                        success = true;
                        message = Helpers.Constants.QBO_RESULT_MESSAGE_SUCCESS;
                    }
                }
                catch (Exception e)
                {
                    success = false;
                    message = e.Message;
                }
            }

            return Json(new { success = success, message = message });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public JsonResult UpdateCustomerToQB(int customerId)
        {
            GCCustomer customer = _soxService.GetCustomer(customerId);
            bool success = false;
            string message = "Something went wrong";

            if (customer != null)
            {
                try
                {
                    var qbCustomerResponse = _groundControlService.GetQBCustomer(customer.QBCustomerId.Value, _helperService.GetQuickBooksAccessToken(User.GetUserId()));

                    if (qbCustomerResponse != null)
                    {
                        if(!string.IsNullOrWhiteSpace(customer.APContactName))
                        {
                            var contactNameList = customer.APContactName.Split(" ").ToList();
                            if (contactNameList != null && contactNameList.Count > 0)
                            {
                                qbCustomerResponse.GivenName = contactNameList[0];
                                if(contactNameList.Count == 2)
                                {
                                    qbCustomerResponse.FamilyName = contactNameList[1];
                                }
                                else if (contactNameList.Count > 2)
                                {
                                    qbCustomerResponse.MiddleName = contactNameList[1];
                                    qbCustomerResponse.FamilyName = contactNameList.Last();
                                }
                            }
                        }

                        qbCustomerResponse.DisplayName = customer.CustomerName;
                        if (qbCustomerResponse.BillAddr == null)
                        {
                            qbCustomerResponse.BillAddr = new Models.QuickBooks.Address();
                        }
                        qbCustomerResponse.BillAddr.Line1 = customer.DeliveryStreet;
                        qbCustomerResponse.BillAddr.City = customer.DeliveryCity;
                        qbCustomerResponse.BillAddr.CountrySubDivisionCode = customer.DeliveryState;
                        qbCustomerResponse.BillAddr.PostalCode = customer.DeliveryZip;
                        if (qbCustomerResponse.PrimaryPhone == null)
                        {
                            qbCustomerResponse.PrimaryPhone = new Models.QuickBooks.Phone();
                        }
                        qbCustomerResponse.PrimaryPhone.FreeFormNumber = customer.APPhone;
                        if (qbCustomerResponse.AlternatePhone == null)
                        {
                            qbCustomerResponse.AlternatePhone = new Models.QuickBooks.Phone();
                        }
                        qbCustomerResponse.AlternatePhone.FreeFormNumber = customer.DeliverPhoneNumber;
                        if (qbCustomerResponse.PrimaryEmailAddr == null)
                        {
                            qbCustomerResponse.PrimaryEmailAddr = new Models.QuickBooks.Email();
                        }
                        qbCustomerResponse.PrimaryEmailAddr.Address = customer.APEmail;
                        qbCustomerResponse.Notes = customer.DeliverNotes;
                        var result = _groundControlService.EditQBCustomer(qbCustomerResponse, _helperService.GetQuickBooksAccessToken(User.GetUserId()));

                        success = true;
                        message = Helpers.Constants.QBO_RESULT_MESSAGE_SUCCESS;
                    }
                }
                catch (Exception e)
                {
                    success = false;
                    message = e.Message;
                }
            }

            return Json(new { success = success, message = message });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public JsonResult SubmitVendorToQB(int vendorId)
        {
            GCVendor vendor = _soxService.GetGCVendor(vendorId);
            bool success = false;
            string message = "Something went wrong";

            if (vendor != null)
            {
                var qbVendor = new Models.QuickBooks.Vendor()
                {
                    DisplayName = vendor.Title,
                    BillAddr = new Models.QuickBooks.Address()
                    {
                        Line1 = string.Empty,
                        City = string.Empty,
                        Country = string.Empty,
                        CountrySubDivisionCode = string.Empty,
                        PostalCode = string.Empty
                    },
                    PrimaryPhone = new Models.QuickBooks.Phone()
                    {
                        FreeFormNumber = string.Empty
                    },
                    SyncToken = "0"
                };

                var token = _helperService.GetQuickBooksAccessToken(User.GetUserId());
                if (token == null)
                {

                }

                var qbVendorResponse = _groundControlService.EditQBVendor(qbVendor, token);

                if (qbVendorResponse == null)
                {
                    success = false;
                }
                else if (qbVendorResponse.Fault != null && qbVendorResponse.Fault.Errors.Count > 0)
                {
                    success = false;
                    message = qbVendorResponse.Fault.Errors[0].Detail;
                }
                else
                {
                    if (qbVendorResponse.Vendor != null)
                    {
                        try
                        {
                            var vendorModel = new UpdateVendorModel()
                            {
                                Vendor = new GCVendorModel()
                                {
                                    Id = vendor.Id,
                                    Active = vendor.Active,
                                    Title = vendor.Title,
                                    QBVendorId = qbVendorResponse.Vendor.Id
                                }
                            };
                            _soxService.UpdateVendor(vendorModel, false);
                            success = true;
                            message = string.Empty;
                        }
                        catch (Exception e)
                        {
                            success = false;
                            message = e.Message;
                        }
                    }
                }
            }

            return Json(new { success = success, message = message });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public JsonResult UpdateVendorFromQB(int vendorId)
        {
            GCVendor vendor = _soxService.GetGCVendor(vendorId);
            bool success = false;
            string message = "Something went wrong";

            if (vendor != null)
            {
                try
                {
                    Vendor qbVendor = _groundControlService.GetQBVendor(vendor.QBVendorId.Value, _helperService.GetQuickBooksAccessToken(User.GetUserId()));

                    if (qbVendor != null)
                    {
                        var vendorModel = new UpdateVendorModel()
                        {
                            Vendor = new GCVendorModel
                            {
                                Id = vendor.Id,
                                Active = true,
                                QBVendorId = qbVendor.Id,
                                Title = qbVendor.DisplayName,
                            }
                        };
                        _soxService.UpdateVendor(vendorModel, false);
                        success = true;
                        message = string.Empty;
                    }
                }
                catch (Exception e)
                {
                    success = false;
                    message = e.Message;
                }
            }

            return Json(new { success = success, message = message });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public JsonResult UpdateVendorToQB(int vendorId)
        {
            GCVendor vendor = _soxService.GetGCVendor(vendorId);
            bool success = false;
            string message = "Something went wrong";

            if (vendor != null)
            {
                try
                {
                    var qbVendor = _groundControlService.GetQBVendor(vendor.QBVendorId.Value, _helperService.GetQuickBooksAccessToken(User.GetUserId()));

                    if (qbVendor != null)
                    {
                        qbVendor.DisplayName = vendor.Title;
                        if (qbVendor.BillAddr == null)
                        {
                            qbVendor.BillAddr = new Address();
                        }
                        if (qbVendor.PrimaryPhone == null)
                        {
                            qbVendor.PrimaryPhone = new Phone();
                        }
                        if (qbVendor.Mobile == null)
                        {
                            qbVendor.Mobile = new Phone();
                        }
                        if (qbVendor.WebAddr == null)
                        {
                            qbVendor.WebAddr = new WebAddr();
                        }
                        var result = _groundControlService.EditQBVendor(qbVendor, _helperService.GetQuickBooksAccessToken(User.GetUserId()));

                        success = true;
                        message = Helpers.Constants.QBO_RESULT_MESSAGE_SUCCESS;
                    }
                }
                catch (Exception e)
                {
                    success = false;
                    message = e.Message;
                }
            }

            return Json(new { success = success, message = message });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public JsonResult SubmitEstimateToQB(int gcCaseId)
        {
            GCCase gcCase = _soxService.GetCaseById(gcCaseId);
            bool success = false;
            string message = "Something went wrong";

            if (gcCase != null)
            {
                _groundControlService.CreateQBEstimate(gcCase, _helperService.GetQuickBooksAccessToken(User.GetUserId()));
                success = true;
                message = Helpers.Constants.QBO_RESULT_MESSAGE_SUCCESS;
            }

            return Json(new { success = success, message = message });
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public async Task<IActionResult> PushTraysOrCasesToQB(int customerId, bool isTraysPush)
        {
            try
            {
                await _groundControlService.PushTrayOrCaseInfoToQuickBooks(customerId, null, _helperService.GetQuickBooksAccessToken(User.GetUserId()));
                return Ok(new { success = true, message = Helpers.Constants.QBO_RESULT_MESSAGE_SUCCESS });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public async Task<IActionResult> SyncWWRoutesAsync(DateTime date)
        {
            try
            {
                await _soxService.SyncRoutesWithWorkWaveAsync(date);
                var cases = _SoxFactory.PrepareCasesForPlanning(date, User, null, null, null);
                return PartialView("_CasesByDate", cases);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public async Task<IActionResult> SyncWWOrdersAsync(GCCasesFilterModel filterModel)
        {
            try
            {
                var result = await _soxService.SyncOrdersWithWorkWaveAsync();
                //return RedirectToAction("Index",filterModel);
                //await _soxService.SetOrderStatusInWWAsync(465, Models.WorkWave.RouteStepStatusType.done);
                //await _soxService.UpdateBarcodeToWWAsync(255);
                return Json(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public async Task<IActionResult> ResolveGCTrayColorStatusNote(int noteId)
        {
            try
            {
                await _soxService.ResolveGCTrayColorStatusNote(noteId);
                return Ok(new { success = true });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public async Task<IActionResult> UnlockGCTrayColorStatus(int statusId)
        {
            try
            {
                await _soxService.UnlockGCTrayColorStatus(statusId);
                return Ok(new { success = true });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public async Task<IActionResult> SaveColorComment(AddColorCommentModal model)
        {
            try
            {
                await _soxService.AddColorComment(model, User.GetFullName());
                GCTray gcTray = _soxService.GetTrayById(model.GCTrayId);
                var gcCaseId = gcTray.GCCaseId;
                return RedirectToAction("CaseDetails", new { id = gcCaseId });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult SyncCaseHistory(int gcCaseId)
        {
            try
            {
                _soxService.SyncCaseHistory(gcCaseId);
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [AjaxCall(DataType = AjaxCallDataType.Html)]
        public IActionResult GetRightSidebarData(int trayId)
        {
            var model = _SoxFactory.PrepareTrayHistoryModel(trayId);
            return PartialView("_SidebarRight", model);
        }

        [HttpGet]
        [AjaxCall(DataType = AjaxCallDataType.Html)]
        public IActionResult GetProposedHistoryItemsForTrayLC(int trayId, int lifecycleId, int trayLifecycleId)
        {
            ProposedTrayLCHistoryModel model = _SoxFactory.PrepareProposedHistoryItemsForLC(trayId, lifecycleId, trayLifecycleId);
            return PartialView("_ProposedHistoryItemsForLC", model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult UpdateProposedHistoryItemForLC([FromBody] ProposedTrayLCHistoryModel model)
        {
            var result = _soxService.AddOrEditProposedHistoryItemForLC(model);
            if(result.Id > 0)
            {
                List<GCLifecycleStatusType> ctLifeCycleList = _groundControlService.GetAllGCLifecycles();
                if (ctLifeCycleList != null && ctLifeCycleList.Count > 0)
                {
                    var ctLifeCycleIds = ctLifeCycleList.Where(x => x.Origin == Helpers.Constants.CT_ABBREVIATION_TWO_CHAR).Select(x => x.Id).ToList();

                    if (false) // this cycle is now back to CT - Cart Assembly
                    {
                        ctLifeCycleIds.Remove((int)LifecycleStatusType.CaseAssembly); // removing for now...
                    }

                    if (ctLifeCycleIds != null || ctLifeCycleIds.Count > 0)
                    {
                        var gcTray = _soxService.GetTrayById(result.GCTrayId);
                        if (gcTray != null)
                        {
                            _groundControlService.UpdateGCTrayLapCount(gcTray, ctLifeCycleIds);
                        }
                        if (gcTray.GCCaseId.HasValue)
                        {
                            int minsToAdd = 0;
                            int.TryParse(GlobalOptions.CENTRAL_TIMEZONE_OFFSET_IN_MINS, out minsToAdd);
                            minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(minsToAdd);
                            _groundControlService.SyncCaseHistory(gcTray.GCCaseId.Value, minsToAdd);
                        }
                    }
                }
            }
            return Json(result);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public JsonResult RemoveLifecycleFromTray(int id)
        {
            var result = _soxService.RemoveGCTrayLifecycleStatud(id);
            return Json(result);
        }

        [HttpGet]
        [AjaxCall(DataType = AjaxCallDataType.Html)]
        public IActionResult GetVendorsTrayHistoryData(int trayId)
        {
            //ChildCompanyModel model = _userManagementModelFactory.PrepareChildCompanyModel(childCompanyId);
            var model = _SoxFactory.PrepareTrayHistoryModelForVendors(trayId);
            return PartialView("_TrayHistoryForVendorsPV", model);
        }

        [HttpPost]
        public IActionResult StoreImage(string uri, string code, int trayId)
        {
            //_soxService.SetTrayStatus(code, "Prepped", null, null);
            var base64 = uri.Replace("data:image/png;base64,", "");
            byte[] bytes = System.Convert.FromBase64String(base64);

            var myUniqueFileName = Convert.ToString(Guid.NewGuid());
            var newFileName = string.Concat(myUniqueFileName, ".png");

            StorePictureInPodContainer(bytes, newFileName, trayId);
            return Ok();
        }

        private void StorePictureInPodContainer(byte[] bytes, string fileName, int trayId)
        {
            _soxService.StorePictureInPodContainer(bytes, fileName, trayId);
        }

        [HttpPost]
        public IActionResult UploadLocationInfoReport(IFormFile file)
        {
            if (file == null)
            {
                return RedirectToAction("FacilityManagement");
            }
            if(file.FileName == "Report.csv")
			{
                using (var stream = file.OpenReadStream())
                {
                    _reportingService.ParseBasReport(stream);
                }
            }
            else
			{
                _reportingService.UploadLocationInfoData(file);
            }
            return RedirectToAction("FacilityManagement");
        }

        public IActionResult TriggerJobs(int jobNumber)
        {
            _soxService.TriggerJobs(jobNumber);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> CancelWorkWaveOrdersAsync(GCCaseModel gcCase)
        {
            await _soxService.DeleteOrdersInWWAsync(gcCase.Id);
            return RedirectToAction("CaseDetails", new { id = gcCase.Id });
        }

        [HttpPost]
        public IActionResult GetInventoryAlertStatus()
        {
            try
            {
                var result = _SoxFactory.PrepareInventoryAlertStatusModel(User);
                return Json(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public IActionResult GetSystemAlertsStatus()
        {
            try
            {
                var result = _SoxFactory.PrepareSystemAlertsStatusModel(User);
                return Json(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult TrayDetailsForVendors(int actualId)
        {
            var model = _SoxFactory.PrepareTrayDetailsForVendorModel(actualId);
            return View(model);
        }

        [HttpGet]
        public IActionResult GetGoogleMapInfo()
		{
            var result = _soxService.GetWWMapMarkers();
            return Json(result);
		}
    }


}