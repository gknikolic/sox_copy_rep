﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting.Models.ReportFilter
{
    public class FilterWidgetViewModel
    {
        public FilterWidgetViewModel(IList<UserFilterModel> userFilters, bool isCorporate, bool isZone, bool isRegion)
        {
            IsCorporate = isCorporate;
            IsZone = isZone;
            IsRegion = isRegion;
            UserFilters = userFilters ?? new List<UserFilterModel>();
        }

        public IList<UserFilterModel> UserFilters { get; }

        public bool IsCorporate { get; }

        public bool IsZone { get; }

        public bool IsRegion { get; }
    }
}
