﻿using System.IO;

namespace ProfitOptics.Modules.Reporting.Models.BusinessReview
{
    public class BusinessReviewReportFileModel
    {
        public MemoryStream Stream { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }
    }
}
