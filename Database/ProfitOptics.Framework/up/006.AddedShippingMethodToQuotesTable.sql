/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'ShippingMethod'
          AND Object_ID = Object_ID(N'QTQuote'))
BEGIN
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
ALTER TABLE dbo.QTQuote ADD
	ShippingMethod nvarchar(50) NULL
ALTER TABLE dbo.QTQuote SET (LOCK_ESCALATION = TABLE)
COMMIT

END

