﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTUserMappingConfig : IEntityTypeConfiguration<QTUserMapping>
    {
        public void Configure(EntityTypeBuilder<QTUserMapping> builder)
        {
            builder.ToTable("QTUserMapping");
            builder.HasKey(entity => entity.UserId);

            builder.Property(entity => entity.UserId).ValueGeneratedNever();
        }
    }
}
