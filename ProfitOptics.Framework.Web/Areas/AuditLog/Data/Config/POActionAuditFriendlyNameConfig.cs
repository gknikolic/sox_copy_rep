﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.Web.Areas.AuditLog.Data.Domain;

namespace ProfitOptics.Framework.Web.Areas.AuditLog.Data.Config
{
    public class POActionAuditFriendlyNameConfig : IEntityTypeConfiguration<POActionAuditFriendlyName>
    {
        public void Configure(EntityTypeBuilder<POActionAuditFriendlyName> builder)
        {
            builder.ToTable("POActionAuditFriendlyName");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.ReflectedType).HasMaxLength(256);
            builder.Property(entity => entity.ReflectedActionName).HasMaxLength(256);
            builder.Property(entity => entity.FriendlyName).HasMaxLength(256);
        }
    }
}