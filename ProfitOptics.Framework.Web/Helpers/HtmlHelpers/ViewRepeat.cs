﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.AspNetCore.Mvc
{
    public static class ViewRepeat
    {
        public static IHtmlContent Repeat(this HtmlHelper htmlHelper, int count, string html)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < count; i++)
                sb.Append(html);

            var contentBuilder = new HtmlContentBuilder();

            return contentBuilder.SetContent(sb.ToString());
        }
    }
}
