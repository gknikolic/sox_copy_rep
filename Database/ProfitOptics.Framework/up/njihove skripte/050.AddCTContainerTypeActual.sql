USE [Sox]

CREATE TABLE CTContainerTypeActual
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[CTContainerTypeId] [INT] NOT NULL,
	[LegacyId] [BIGINT] NOT NULL,
	[ParentId] [BIGINT] NOT NULL,
	[Weight] [NUMERIC](18,2) NULL,
	[PhysicianName] [NVARCHAR](100) NULL,
	[SterilizationMethodName] [NVARCHAR](100) NULL,
	[ServiceName] [NVARCHAR](100) NULL,
	[ServiceId] [BIGINT] NOT NULL,
	[ProcurementReferenceId] [BIGINT] NULL,
	[Name] [NVARCHAR](100) NULL,
	[Status] [NVARCHAR](100) NULL,
	[UpdateTimestamp] [DateTime] NOT NULL,
	[UpdateUserId] [NVARCHAR](100) NULL,
	[LocationElapsed] [NVARCHAR](100) NULL,
	[CaseCart] [NVARCHAR](100) NULL,
	[ProcessingLocationName] [NVARCHAR](100) NULL,
	[ParentProcessingLocationName] [NVARCHAR](100) NULL,
	[ParentUsageIntervalUnitOfMeasure] [NVARCHAR](100) NULL,
	[HomeLocationName] [NVARCHAR](100) NULL,
	[ParentHomeLocationName] [NVARCHAR](100) NULL,
	[ParentIntervalUsageCount] [INT] NULL,
	[IntervalUsageCount] [INT] NULL,
	[Usage] [NVARCHAR](100) NULL,
	[CensisSetLastRequestDate] [DateTime] NULL,
	[LastMaintenance] [DateTime] NULL,
	[Assembly] [NVARCHAR](100) NULL,
	[Storage] [NVARCHAR](100) NULL,
	[RtlsLocationName] [NVARCHAR](100) NULL,
	[RtlsLocationUpdateTimestamp] [DateTime] NULL,
	[IsDiscarded] [BIT] NULL,
	[Priority] [NVARCHAR](100) NULL,
	[ShelfLife] [NVARCHAR](100) NULL,
	[NotificationCount] [BIGINT] NULL,
	[Expedite] [NVARCHAR](100) NULL,
	[Rigidity] [NVARCHAR](100) NULL,
	[ParentExpedite] [NVARCHAR](100) NULL,
	[ParentRigidity] [NVARCHAR](100) NULL,
	[CensisSetHistoryBuildStartDate] [DateTime] NULL,
	[FirstAlternateMethodId] [BIGINT] NULL,
	[FirstAlternateMethodName] [NVARCHAR](100) NULL,
	[SecondAlternateMethodId] [BIGINT] NULL,
	[SecondAlternateMethodName] [NVARCHAR](100) NULL,
	[ThirdAlternateMethodId] [BIGINT] NULL,
	[ThirdAlternateMethodName] [NVARCHAR](100) NULL,
	[ComplexityLevelDescription] [NVARCHAR](100) NULL,
	[DisposableFlag] [NVARCHAR](20) NULL,
	[SerialNumber] [NVARCHAR](50) NULL,
	[VendorName] [NVARCHAR](100) NULL,
	[CreatedAtUtc] DateTime NOT NULL,
	[ModifiedAtUtc] DateTime NULL
CONSTRAINT [PK_CTContainerTypeActual] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTContainerTypeActual]  WITH CHECK ADD  CONSTRAINT [CTContainerTypeActual_CTContainerType] FOREIGN KEY([CTContainerTypeId])
REFERENCES [dbo].[CTContainerType] ([Id])
GO
ALTER TABLE [dbo].[CTContainerTypeActual] CHECK CONSTRAINT [CTContainerTypeActual_CTContainerType]
GO

ALTER TABLE [Sox].[dbo].[GCCase] ADD VendorName NVARCHAR(200) NULL;

ALTER TABLE [Sox].[dbo].[GCTray] ADD CTCaseContainerId INT NULL;

ALTER TABLE [dbo].[GCTray]  WITH CHECK ADD  CONSTRAINT [FK_GCTray_CTCaseContainer] FOREIGN KEY([CTCaseContainerId])
REFERENCES [dbo].[CTCaseContainer] ([Id])
GO
ALTER TABLE [dbo].[GCTray] CHECK CONSTRAINT [FK_GCTray_CTCaseContainer]
GO