﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Support.Attributes;
using ProfitOptics.Modules.Support.Data.Domain;
using ProfitOptics.Modules.Support.Factories;
using ProfitOptics.Modules.Support.Models.ViewModels;
using ProfitOptics.Modules.Support.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmailNotificationModel = ProfitOptics.Modules.Support.Models.EmailNotificationModel;
using IssueDataModel = ProfitOptics.Modules.Support.Models.IssueDataModel;

namespace ProfitOptics.Modules.Support.Controllers
{
    [Area("Support")]
    [Authorize(Roles = "admin")]
    [JiraUserSync]
    public class JiraController : BaseController
    {
        #region Fields

        private readonly ISupportService _supportService;
        private readonly ISupportFactory _supportFactory;
        private readonly IPersistedDataService _persistedDataService;
        private readonly IJiraService _jiraService;
        private readonly ISupportMemoryCacheService _supportMemoryCacheService;

        #endregion Fields

        #region Ctor

        public JiraController(ISupportService supportService,
            ISupportFactory supportFactory,
            IPersistedDataService persistedDataService,
            ISupportMemoryCacheService supportMemoryCache,
            IJiraService jiraService)
        {
            _supportService = supportService;
            _supportFactory = supportFactory;
            _persistedDataService = persistedDataService;
            _supportMemoryCacheService = supportMemoryCache;
            _jiraService = jiraService;
        }

        #endregion Ctor

        #region Methods

        #region View methods

        public IActionResult ViewIssues()
        {
            var suppMsg = GetSupportMessage();

            var model = _supportFactory.PrepareIssuesViewModel(suppMsg);

            return View(model);
        }

        public IActionResult BacklogIssues()
        {
            var suppMsg = GetSupportMessage();

            var model = _supportFactory.PrepareIssuesViewModel(suppMsg);

            return View(model);
        }

        public IActionResult ClosedIssues()
        {
            var suppMsg = GetSupportMessage();

            var model = _supportFactory.PrepareIssuesViewModel(suppMsg);

            return View(model);
        }

        public IActionResult GlobalWatchers()
        {
            return View();
        }

        public IActionResult CreateIssue()
        {
            var referrerUrl = string.Empty;
            var uriReferer = Request.GetTypedHeaders().Referer;

            if (uriReferer != null)
            {
                if (uriReferer.AbsolutePath != Url.Action("ViewIssues", "Jira", new { area = "Support" }))
                {
                    referrerUrl = uriReferer.AbsoluteUri;
                }
            }

            var model = _supportFactory.PrepareCreateIssueModel(GetSupportMessage(), referrerUrl);

            return View(model);
        }

        [HttpPost]
        public IActionResult CreateIssue(CreateIssueViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!model.Type.ToLower().Equals("sub-task"))
                    {
                        model.ParentIssueKey = null;
                    }

                    if (model.Component.ToLower().Equals("no component"))
                    {
                        model.Component = null;
                    }

                    var result = Task.Run(async () => await _jiraService
                        .CreateJiraIssueAsync(model, GetCurrentUserUsername())).GetAwaiter().GetResult();

                    if (result != null)
                    {
                        return RedirectToAction("ViewIssues", "Jira");
                    }
                }
                catch (Exception ex)
                {
                    model.SupportMessage = "Error: " + ex.Message;
                }
            }

            // We have to fill these in manually because they are not part of the viewModel,
            // and the page will crash without these present.
            model.ComponentList = _supportFactory.PrepareComponentsList(model.Project);
            model.ProjectList = _supportFactory.PrepareProjectsList();
            model.PriorityList = _supportFactory.PreparePriorityList(model.Project);
            model.TypeList = _supportFactory.PrepareIssueTypeList(model.Project);

            return View(model);
        }

        public IActionResult EditIssue(string id)
        {
            var suppMsg = GetSupportMessage();

            var model = _supportFactory.PrepareUpdateIssueModel(id);

            if (!string.IsNullOrEmpty(suppMsg))
            {
                model.SupportMessage = suppMsg;
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult EditIssue(UpdateIssueViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _jiraService.UpdateIssue(model);
                    TempData["SupportMessage"] = "Successfully updated issue: " + model.IssueKey;
                    return RedirectToAction("EditIssue", "Jira");
                }
                catch (Exception ex)
                {
                    model.SupportMessage = "Error: " + ex.Message;
                }
            }

            if (model.Attachments == null)
            {
                _jiraService.GetAttachmentsAndCommentsForIssue(model.IssueKey, out var attachments, out var comments);
                model.InitializeAttachments(attachments);
                model.InitializeComments(comments);
            }

            var project = _persistedDataService.GetSelectedProject();

            model.PriorityList = _supportFactory.PreparePriorityList(project);
            model.PriorityList.Single(x => x.Text == model.Priority).Selected = true;
            model.TypeList = _supportFactory.PrepareIssueTypeList(project);
            model.TypeList.Single(x => x.Text == model.Type).Selected = true;

            return View(model);
        }

        #endregion View methods

        #region Get Methods

        [HttpPost]
        public IActionResult GetComponentsForProject(string projectKey)
        {
            var respstatus = "OK";
            var message = string.Empty;
            var components = new List<SelectListItem>();

            try
            {
                components = _supportFactory.PrepareComponentsList(projectKey, true);
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message,
                Components = components
            });
        }

        [HttpPost]
        public IActionResult GetIssueTypesForProject(string projectKey)
        {
            var respstatus = "OK";
            var message = string.Empty;
            var types = new List<SelectListItem>();

            try
            {
                types = _supportFactory.PrepareIssueTypeList(projectKey);
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message,
                Types = types
            });
        }

        [HttpPost]
        public IActionResult GetPrioritiesForProject(string projectKey)
        {
            var respstatus = "OK";
            var message = string.Empty;
            var priorities = new List<SelectListItem>();

            try
            {
                priorities = _supportFactory.PreparePriorityList(projectKey);
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message,
                Priorities = priorities
            });
        }

        [HttpPost]
        public IActionResult GetIssuesForProject(string projectKey)
        {
            var respstatus = "OK";
            var message = string.Empty;
            var issues = new List<SelectListItem>();

            try
            {
                issues = _supportFactory.PrepareIssueList(projectKey);
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message,
                Issues = issues
            });
        }

        #endregion Get Methods

        #region Change Methods

        [HttpPost]
        public IActionResult ChangeProject(string project)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                _persistedDataService.SetSelectedProject(project);
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult ChangeComponent(string component)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                _persistedDataService.SetSelectedComponent(component);
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult ChangeAssignee(string assignee)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                _persistedDataService.SetSelectedAssignee(assignee);
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult ChangeReporter(string reporterText, string reporterValue)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                _persistedDataService.SetSelectedReporter(reporterValue);
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult ChangeMultipleComponent(string components)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                var sentComponents = new List<string>();

                if (components != null)
                {
                    sentComponents = JsonConvert.DeserializeObject<List<string>>(components);
                }

                _persistedDataService.SetSelectedMultipleComponents(sentComponents);
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        #endregion Change Methods

        public IActionResult DownloadAttachment(string issueKey, string attachmentId)
        {
            var attachment = _jiraService.DownloadAttachment(issueKey, attachmentId);
            var data = attachment.DownloadData();

            return new FileContentResult(data, attachment.MimeType) { FileDownloadName = attachment.FileName };
        }

        [HttpPost]
        public IActionResult RankBeforeIssue(string issueIdToRank, string rankBeforeIssueId, string project)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                var configuration = _supportMemoryCacheService.GetBoardConfiguration(project);

                var customSortField = configuration?.Ranking?.RankCustomFieldId ?? string.Empty;

                var response = _jiraService.RankBeforeIssue(issueIdToRank, rankBeforeIssueId, customSortField);

                if (response.ResponseStatus != ResponseStatus.Completed)
                {
                    message = "Request status - not completed.";
                    respstatus = "Error";
                }
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult RankAfterIssue(string issueIdToRank, string rankAfterIssueId, string project)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                var configuration = _supportMemoryCacheService.GetBoardConfiguration(project);

                var customSortField = configuration?.Ranking?.RankCustomFieldId ?? string.Empty;

                var response = _jiraService.RankAfterIssue(issueIdToRank, rankAfterIssueId, customSortField);

                if (response.ResponseStatus != ResponseStatus.Completed)
                {
                    message = "Request status - not completed.";
                    respstatus = "Error";
                }
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult GetPossibleTransitions(string projectKey, string forIssue, string oldContainer, string newContainer)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                var transitions = _jiraService.GetPossibleTransitionsForIssue(forIssue);

                var configuration = _supportMemoryCacheService.GetBoardConfiguration(projectKey);

                var allowedNewTransitions = _jiraService.GetPossibleBoxTransitions(newContainer, configuration);

                var acceptableTransitions = transitions
                    .FindAll(t => allowedNewTransitions.Any(at => at == t.To.Id)).Select(x => new { x.Id, x.Name })
                    .ToList();

                return Json(new
                {
                    Status = respstatus,
                    Message = message,
                    Transitions = acceptableTransitions
                });
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message,
            });
        }

        [HttpPost]
        public IActionResult UpdateStatus(string issueToUpdate, string transitionName)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                if (_jiraService.GetPossibleTransitionsForIssue(issueToUpdate).Any(x => x.Name.Equals(transitionName)))
                {
                    _jiraService.ChangeIssueStatus(issueToUpdate, transitionName);
                }
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult MoveToBacklog(string issue)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                var transitions = _jiraService.GetPossibleTransitionsForIssue(issue);

                var backlogTransition = transitions.Find(t => t.Name.ToLower() == "backlog");

                if (backlogTransition != null)
                {
                    _jiraService.ChangeIssueStatus(issue, backlogTransition.Name);
                }
                else
                {
                    respstatus = "Error";
                    message = "Workflow does not allow transition to Backlog for this particular issue, from this status.";
                }
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult DeleteAttachment(string issueKey, string attachmentId)
        {
            var status = "ERROR";
            var message = string.Empty;

            try
            {
                _jiraService.DeleteAttachment(issueKey, attachmentId);
                status = "OK";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return Json(new
            {
                Status = status,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult AddWatch(string projectId, string issueId)
        {
            var respstatus = "OK";

            var message = string.Empty;

            try
            {
                var emailAddress = User.Identity.Name;

                if (!string.IsNullOrEmpty(emailAddress))
                {
                    _supportService.AddWatch(emailAddress, projectId, issueId);
                }
                else
                {
                    message = "We could not find your email in user mapping, please contact administration.";
                    respstatus = "Error";
                }
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult RemoveWatch(string projectId, string issueId)
        {
            var respstatus = "OK";
            var message = string.Empty;

            try
            {
                var emailAddress = User.Identity.Name;

                if (!string.IsNullOrEmpty(emailAddress))
                {
                    _supportService.RemoveWatch(emailAddress, projectId, issueId);
                }
                else
                {
                    message = "We could not find your email in user mapping, please contact administration.";
                    respstatus = "Error";
                }
            }
            catch (Exception ex)
            {
                respstatus = "Error";
                message = ex.Message;
            }

            return Json(new
            {
                Status = respstatus,
                Message = message
            });
        }

        [HttpPost]
        public IActionResult GetGlobalEmailNotifications()
        {
            var status = "ERROR";
            var message = string.Empty;
            var emails = new List<string>();

            try
            {
                var notifEmails = _supportService.GetGlobalWatchers();
                foreach (var row in notifEmails)
                {
                    emails.Add(row.Email);
                }

                status = "OK";
            }
            catch (Exception ex)
            {
                message = "Error fetching Global Watchers.";
            }

            return Json(new
            {
                Status = status,
                Message = message,
                Emails = emails
            });
        }

        [HttpPost]
        public IActionResult GlobalEmailNotifications(EmailNotificationModel model)
        {
            string status = "ERROR";
            string message = string.Empty;

            if (ModelState.IsValid)
            {
                try
                {
                    var toAdd = new List<JiraSupportGlobalNotification>();

                    foreach (var email in model.Emails)
                    {
                        if (!string.IsNullOrEmpty(email))
                        {
                            toAdd.Add(new JiraSupportGlobalNotification
                            {
                                Email = email
                            });
                        }
                    }

                    _supportService.RemoveAllGlobalWatchers();
                    _supportService.AddOrUpdateGlobalWatchers(toAdd);

                    status = "OK";
                    message = "Global Watchers updated!";
                }
                catch (Exception ex)
                {
                    message = "Error updating Global Watchers.";
                }
            }
            else
            {
                message = "Email list not valid.";
            }

            return Json(new
            {
                Status = status,
                Message = message
            });
        }

        public IActionResult UserSearch(string search, int page, string filterName)
        {
            Select2PagedResult users;

            try
            {
                var projectKey = _persistedDataService.GetSelectedProject();

                users = _supportFactory.PrepareUsersPagedResult(projectKey, search, page);
            }
            catch (Exception ex)
            {
                users = new Select2PagedResult();
            }

            return Json(users);
        }

        [HttpPost]
        public IActionResult IssuesData([ModelBinder(typeof(DataTablesBinder))]
            IDataTablesRequest request, string statuses, string components, string project, string assignee,
            string reporter)
        {
            var selectedStatuses = JsonConvert.DeserializeObject<List<string>>(statuses ?? string.Empty);

            if (!selectedStatuses.Any())
            {
                return new LargeJsonResult(new DataTablesResponse<IssueDataModel>(0, new List<IssueDataModel>(), 0, 0));
            }
            var selectedComponents = JsonConvert.DeserializeObject<List<string>>(components ?? string.Empty);

            var data = _supportFactory.PrepareIssueDataModels(request, selectedStatuses, selectedComponents, project, assignee, reporter, User.Identity.Name);

            return new LargeJsonResult(data);
        }

        [HttpPost]
        public IActionResult BacklogIssuesData([ModelBinder(typeof(DataTablesBinder))]
            IDataTablesRequest request, string components, string project, string assignee,
            string reporter)
        {
            var selectedComponents = JsonConvert.DeserializeObject<List<string>>(components ?? string.Empty);

            var data = _supportFactory.PrepareBacklogIssueDataModels(request, selectedComponents, project, assignee, reporter);

            return new LargeJsonResult(data);
        }

        #endregion Methods

        #region Util

        private string GetSupportMessage()
        {
            return TempData["SupportMessage"] as string;
        }

        #endregion Util
    }
}