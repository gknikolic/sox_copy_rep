﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTBillToConfig : IEntityTypeConfiguration<QTBillTo>
    {
        public void Configure(EntityTypeBuilder<QTBillTo> builder)
        {
            builder.ToTable("QTBillTo");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Company).HasMaxLength(50);
            builder.Property(entity => entity.BillToNum);
            builder.Property(entity => entity.BillToName).HasMaxLength(50);
            builder.Property(entity => entity.Address1).HasMaxLength(50);
            builder.Property(entity => entity.Address2).HasMaxLength(50);
            builder.Property(entity => entity.Address3).HasMaxLength(50);
            builder.Property(entity => entity.City).HasMaxLength(50);
            builder.Property(entity => entity.State).HasMaxLength(50);
            builder.Property(entity => entity.Zip).HasMaxLength(50);
            builder.Property(entity => entity.CustomerId).HasMaxLength(50);

            builder.Property(entity => entity.StartDate).HasColumnType("datetime");
            builder.Property(entity => entity.EndDate).HasColumnType("datetime");

        }
    }
}
