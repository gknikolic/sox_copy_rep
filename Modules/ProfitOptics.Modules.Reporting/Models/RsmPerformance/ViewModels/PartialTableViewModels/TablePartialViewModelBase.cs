﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.PartialTableViewModels
{
    public abstract class TablePartialViewModelBase
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
        public int RemainingWorkDaysCount { get; set; }
    }
}