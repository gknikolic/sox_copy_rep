﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class AspNetRoleConfig : IEntityTypeConfiguration<AspNetRole>
    {
        public void Configure(EntityTypeBuilder<AspNetRole> builder)
        {
            builder.ToTable("AspNetRoles");
            builder.HasKey(role => role.Id);

            builder.HasIndex(role => role.Name).HasName("RoleNameIndex").IsUnique();

            builder.Property(role => role.Name).HasMaxLength(256).IsRequired();
        }
    }
}
