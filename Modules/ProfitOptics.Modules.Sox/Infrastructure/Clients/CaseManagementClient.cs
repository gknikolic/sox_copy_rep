﻿using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Serilog;
using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Modules.Sox.Infrastructure.Clients
{
	public class CaseManagementClient : ICaseManagementClient
	{
		private readonly Entities _context;
		public CaseManagementClient(Entities context)
		{
			_context = context;
		}

		public void ChangeGCCaseLifecycleStatus(ManageGCCaseLifecycleStatuses model)
		{
			Log.Information($"Enter ChangeGCCaseLifecycleStatus");
			if (model.Timestamp == null)
			{
				model.Timestamp = DateTime.UtcNow;
			}
			var lifecycleStatus = _context.GCLifecycleStatusTypes.Find(model.SelectedGCLifecycleStatus);
			var trays = _context.GCTrays.Where(x => x.GCCaseId == model.SelectedGCCase).ToList();

			foreach (var tray in trays)
			{
				Log.Information($"ChangeGCCaseLifecycleStatus - processing tray - {tray.Id} - status: {lifecycleStatus.Title}");
				var trayStatus = GetActiveLifecycleStatusAndTimestampForTray(tray.Id);
				var checkStatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.LifecycleStatusTypeId == lifecycleStatus.Id && x.GCTrayId == tray.Id).FirstOrDefault();
				if (checkStatus != null) //if this status already exists for this tray return to prevent creating duplicate
				{
					Log.Information($"ChangeGCCaseLifecycleStatus - processing tray - Timestap with status {lifecycleStatus.Title} already exists");
					return;
				}
				Log.Information($"ChangeGCCaseLifecycleStatus - processing tray - Creating new status - {lifecycleStatus.Title}");
				if (trayStatus?.LifecycleStatus != null && trayStatus.LifecycleStatus.Order < lifecycleStatus.Order) //invalidate previous active status and set this as new one
				{
					var trayLifecycleStatusOld = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id && x.IsActiveStatus == true).FirstOrDefault();
					trayLifecycleStatusOld.IsActiveStatus = false;
					_context.GCTraysLifecycleStatusTypes.Update(trayLifecycleStatusOld);

					var trayLifecycleStatusNew = new GCTraysLifecycleStatusTypes()
					{
						GCTrayId = tray.Id,
						LifecycleStatusTypeId = lifecycleStatus.Id,
						Timestamp = (DateTime)model.Timestamp,
						IsActiveStatus = true,
						User = model.User
					};
					_context.GCTraysLifecycleStatusTypes.Add(trayLifecycleStatusNew);
				}
				else //do not invalidate current active status, only insert this as inactive or insert as active if there are no previous statuses
				{
					var trayLifecycleStatusNew = new GCTraysLifecycleStatusTypes()
					{
						GCTrayId = tray.Id,
						LifecycleStatusTypeId = lifecycleStatus.Id,
						Timestamp = (DateTime)model.Timestamp,
						IsActiveStatus = trayStatus?.LifecycleStatus != null ? false : true, //if previous lifecycle status exists, that means that previous status has bigger order so new status should not be set to active
						User = model.User
					};
					_context.GCTraysLifecycleStatusTypes.Add(trayLifecycleStatusNew);
				}
				tray.GCLifecycleStatusTypeId = lifecycleStatus.Id;
				_context.SaveChanges();
			}

			UpdateGCCaseLifecycleStatus(model.SelectedGCCase);
		}

		public void ChangeTrayLifecycleStatusByBarcode(int caseId, int statusCode, string barcode, string user, DateTime? timestamp = null)
		{
			Log.Information($"Enter ChangeTrayLifecycleStatusByBarcode - { barcode}");
			if (timestamp == null)
			{
				timestamp = DateTime.UtcNow;
			}
			var lifecycleStatus = _context.GCLifecycleStatusTypes.Where(x => x.Code == statusCode).FirstOrDefault();
			if (lifecycleStatus != null)
			{
				var tray = _context.GCTrays.Where(x => x.GCCaseId == caseId && x.Barcode == barcode).FirstOrDefault();

				if (tray != null)
				{
					Log.Information($"ChangeTrayLifecycleStatusByBarcode - processing tray - {tray.Id} - status: {lifecycleStatus.Title}");
					var checkStatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.LifecycleStatusTypeId == lifecycleStatus.Id && x.GCTrayId == tray.Id).FirstOrDefault();
					if (checkStatus != null) //if this status already exists for this tray return to prevent creating duplicate
					{
						return;
					}
					var trayStatus = GetActiveLifecycleStatusAndTimestampForTray(tray.Id);

					if (trayStatus?.LifecycleStatus != null && trayStatus.LifecycleStatus.Order < lifecycleStatus.Order)
					{
						var trayLifecycleStatusOld = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id && x.IsActiveStatus == true).FirstOrDefault();
						trayLifecycleStatusOld.IsActiveStatus = false;
						_context.GCTraysLifecycleStatusTypes.Update(trayLifecycleStatusOld);

						var trayLifecycleStatusNew = new GCTraysLifecycleStatusTypes()
						{
							GCTrayId = tray.Id,
							LifecycleStatusTypeId = lifecycleStatus.Id,
							Timestamp = (DateTime)timestamp,
							IsActiveStatus = true,
							User = user
						};
						tray.GCLifecycleStatusTypeId = lifecycleStatus.Id;
						_context.GCTraysLifecycleStatusTypes.Add(trayLifecycleStatusNew);
					}
					else
					{
						var trayLifecycleStatusNew = new GCTraysLifecycleStatusTypes()
						{
							GCTrayId = tray.Id,
							LifecycleStatusTypeId = lifecycleStatus.Id,
							Timestamp = (DateTime)timestamp,
							IsActiveStatus = trayStatus?.LifecycleStatus != null ? false : true, //if previous lifecycle status exists, that means that previous status has bigger order so new status should not be set to active
							User = user
						};
						tray.GCLifecycleStatusTypeId = lifecycleStatus.Id;
						_context.GCTraysLifecycleStatusTypes.Add(trayLifecycleStatusNew);
					}
					tray.GCLifecycleStatusTypeId = lifecycleStatus.Id;
					_context.SaveChanges();
					UpdateGCCaseLifecycleStatus(caseId);
				}
			}
		}


		public LifecycleStatusWithTimestamp GetActiveLifecycleStatusAndTimestampForTray(int trayId)
		{
			var model = new LifecycleStatusWithTimestamp();
			var gcTrayLifecycleStatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == trayId && x.IsActiveStatus == true).FirstOrDefault();
			if (gcTrayLifecycleStatus != null)
			{
				model.LifecycleStatus = _context.GCLifecycleStatusTypes
					                                    .Include(x => x.GCLifecycleStatusTypeCategory)
					                                    .Where(x => x.Id == gcTrayLifecycleStatus.LifecycleStatusTypeId)
														.FirstOrDefault();
																		
				model.Timestamp = gcTrayLifecycleStatus.Timestamp;
			}

			return model;
		}

		public void UpdateGCCaseLifecycleStatus(int gcCaseId)
		{
			var gcCase = _context.GCCases.Find(gcCaseId);

			//var containersIds = _context.CTCaseContainers.Where(x => x.CTCaseId == gcCase.CTCaseId).Select(x => x.Id);
			//var traysIds = _context.GCTray.Where(x => containersIds.Contains(x.CTCaseContainerId)).Select(x => x.Id);
			//var traysIds = _context.GCCasesGCTrays.Where(x => x.GCCaseId == gcCaseId).Select(x => x.GCTrayId);
			var traysIds = _context.GCTrays.Where(x => x.GCCaseId == gcCaseId).Select(x => x.Id).ToList();

			var lifecycleStatusTypeIds = _context.GCTraysLifecycleStatusTypes.Where(x => traysIds.Contains(x.GCTrayId) && x.IsActiveStatus == true).Select(x => x.LifecycleStatusTypeId).Distinct();

			var lifecycleStatusTypeOrders = _context.GCLifecycleStatusTypes.Where(x => lifecycleStatusTypeIds.Contains(x.Id)).Select(x => x.Order).ToList();
			if (lifecycleStatusTypeOrders.Count > 0)
			{
				var minOrder = lifecycleStatusTypeOrders.Min();

				var lifecycleStatusTypeId = _context.GCLifecycleStatusTypes.Where(x => x.Order == minOrder).FirstOrDefault().Id;

				//var timestamps = _context.GCCasesLifecycleStatusTypes.Where(x => x.GCCaseId == gcCase.Id).Select(x => x.Timestamp);//add flag in db.GCCasesLifecycleStatusTypes for active status
				//var maxTime = timestamps.Max();
				var gcCaseLifecycleTypeStatusOld = _context.GCCasesLifecycleStatusTypes.Where(x => x.GCCaseId == gcCaseId && x.IsActiveStatus == true).FirstOrDefault();

				if (gcCaseLifecycleTypeStatusOld == null || gcCaseLifecycleTypeStatusOld.LifecycleStatusTypeId != lifecycleStatusTypeId)
				{
					if (gcCaseLifecycleTypeStatusOld != null)
					{
						gcCaseLifecycleTypeStatusOld.IsActiveStatus = false;
						_context.GCCasesLifecycleStatusTypes.Update(gcCaseLifecycleTypeStatusOld);
					}

					GCCasesLifecycleStatusTypes gcCaseLifecycleTypeStatusNew = new GCCasesLifecycleStatusTypes();
					gcCaseLifecycleTypeStatusNew.GCCaseId = gcCaseId;
					gcCaseLifecycleTypeStatusNew.LifecycleStatusTypeId = lifecycleStatusTypeId;
					gcCaseLifecycleTypeStatusNew.Timestamp = DateTime.Now;
					gcCaseLifecycleTypeStatusNew.IsActiveStatus = true;
					_context.GCCasesLifecycleStatusTypes.Add(gcCaseLifecycleTypeStatusNew);

					_context.SaveChanges();
				}
			}
		}

	}
}
