﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Web.Framework.Models;
using System.Security.Claims;

namespace ProfitOptics.Framework.Web.Framework.Services
{
    /// <summary>
    /// Service responsible for working with Identity domains
    /// </summary>
    public interface IUserService
    {
        Task<ApplicationUser> FindUserByUsernameAsync(string username);

        Task<ApplicationUser> FindUserByIdAsync(int id);

        Task<ApplicationUser> FindUserByEmailAsync(string email);

        Task<IdentityResult> CreateUserAsync(string firstName, string lastName, string email, string password = null, string phone = null, byte[] profilePictureData = null);

        Task<IdentityResult> AddExternalLoginAsync(ApplicationUser user, UserLoginInfo userLoginInfo);

        Task<IdentityResult> ConfirmEmailAsync(ApplicationUser user, string confirmationToken);

        Task<IdentityResult> ResetAccessFailedCountAsync(ApplicationUser user);

        Task<IdentityResult> AddAccessFailedAttemptToUser(ApplicationUser user);

        Task<IList<string>> GetValidTwoFactorProvidersForUserAsync(ApplicationUser user);

        Task<string> GenerateTwoFactorTokenAsync(ApplicationUser user, string providerName);

        Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user);

        Task<string> GenerateEmailConfirmationTokenAsync(ApplicationUser user);

        Task<IList<ApplicationUser>> GetUsersInRoleAsync(string roleName);

        Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string code, string password);

        Task<bool> IsLockedOutAsync(ApplicationUser user);

        Task<IdentityResult> SetLockoutEndDateAsync(ApplicationUser user, DateTimeOffset lockoutEndOffset);

        Task<IList<ApplicationRole>> GetUserRolesAsync(ApplicationUser user);

        Task<byte[]> GetProfilePictureAsync(int userId);
        Task<POUserProfile> GetUserProfileAsync(int userId);

        Task<bool> IsEmailConfirmedAsync(ApplicationUser user);

        Task<IdentityResult> UpdateUserAsync(ApplicationUser user);

        Task<int> UpdateProfilePictureAsync(ApplicationUser user, byte[] profilePhotoBytes);

        Task<bool> HasPasswordAsync(ApplicationUser user);

        Task<string> GetPhoneNumberAsync(ApplicationUser user);

        Task<bool> GetTwoFactorEnabledAsync(ApplicationUser user);

        Task<string> GenerateChangePhoneNumberTokenAsync(ApplicationUser user, string number);

        Task<IdentityResult> ChangePhoneNumberAsync(ApplicationUser user, string phoneNumber, string code);

        Task<bool> IsTwoFactorAuthenticationEnabledAsync(ApplicationUser user);

        Task<IdentityResult> SetTwoFactorAuthenticationEnabledAsync(ApplicationUser user, bool enabled);

        Task<bool> IsGoogleAuthenticatorEnabledAsync(ApplicationUser user);

        Task<IdentityResult> SetAccountGoogleAuthenticatorAsync(ApplicationUser user, string key);

        Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string currentPassword, string newPassword);

        Task<IList<IdentityUserLogin<int>>> GetExternalLoginsForUser(ApplicationUser user);

        Task<IdentityResult> RemoveLoginAsync(ApplicationUser user, string loginProvider, string providerKey);

        Task<IdentityResult> UpdateUserRolesAsync(ApplicationUser user, List<string> roles);

        Task<IList<ApplicationRole>> GetRolesAsync();

        IQueryable<ApplicationUser> GetUsersAsQueryable(bool? isEnabled = true);
        IQueryable<POUserProfilePicture> GetProfilePicturesQueryable();
        Task<IdentityResult> RemovePhoneNumberAsync(ApplicationUser user);
        Task<IdentityResult> SetPasswordAsync(ApplicationUser user, string password);
        Task<IdentityResult> AddUserToRolesAsync(ApplicationUser user, string[] roles);
        Task<int> EnableAndAddRolesToUser(string username, List<string> roles);

        Task<int> ConfirmEmail(string email, string token);

        IEnumerable<SelectListItem> GetGCParentCompanies();
        IEnumerable<GCParentCompany> GetParentCompanies();
        IEnumerable<GCChildCompany> GetChildCompanies();
        GCChildCompany GetChildCompanyById(int id);
        void AddUserToGCParentCompany(int userId, IEnumerable<int> parentCompanyIds);
        ParentsWithChildCompaniesModel GetChildrenForGCParentCompany(List<int> parentCompanyIds);
        IEnumerable<GCParentCompany> GetParentCompaniesByUserId(int userId);
        void EditUsersParentCompanies(int userId, IEnumerable<int> parentCompaniesIds);
        void RemoveClaim(string claimType, int? userId = null);
        void EditParentCompany(ParentCompanyModel company);
        void AddParentCompany(ParentCompanyModel company);
        void DeleteParentCompany(int companyId);
        void EditChildCompany(ChildCompanyModel company);
        void AddChildCompany(ChildCompanyModel company);
        void DeleteChildCompany(int companyId);

        ParentCompanyModel GetParentCompanyWithChilder(int companyId, string childCompanyPermissionValues = null);
        IEnumerable<IdentityUserClaim<int>> GetUserClaims(int userId, string startsWith = null);
        GCChildCompany GetChildCompanyForCustomer(int customerId);

        GCChildCompany GetChildCompanyForVendor(int vendorId);
    }
}
