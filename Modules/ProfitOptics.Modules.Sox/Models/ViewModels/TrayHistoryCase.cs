﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayHistoryCase
	{
		public int CaseId { get; set; }
		public int? GCTrayId { get; set; }
		public string CaseReference { get; set; }
		public string CustomerName { get; set; }
		public DateTime DueDate { get; set; }
		public bool IsCanceled { get; set; }
		public bool isLoaner { get; set; }
        public int? LapCount { get; set; }
        public List<TrayHistoryTrayLifecycle> TrayLifecycles{ get; set; }
		public List<TrayHistoryTrayLifecycle> PreviousLifecycles { get; set; }
	}
}
