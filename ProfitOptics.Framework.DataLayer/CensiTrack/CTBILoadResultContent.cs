﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTBILoadResultContent
    {
        public int Id { get; set; }
        public int CTBILoadResultId { get; set; }
        public int CTContainerTypeActualId { get; set; }
        public long LoadId { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public string Container { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime LastUpdate { get; set; }
        public string LastContainer { get; set; }
        public string LastLocation { get; set; }
        public string Sort { get; set; }
        public string BiotestFlag { get; set; }
        public long AssetId { get; set; }
        public string AssetType { get; set; }
        public string CaseReference { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
        public DateTime LastUpdateOriginal { get; set; }

        public virtual CTBILoadResult CTBILoadResult { get; set; }
    }
}
