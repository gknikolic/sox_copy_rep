﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Services
{
    public class UserHierarchyService : IUserHierarchyService
    {
        private readonly IUserMappingService _userMappingService;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly Entities _context;

        public UserHierarchyService(IUserMappingService userMappingService, IHttpContextAccessor contextAccessor, Entities entities)
        {
            _userMappingService = userMappingService;
            _contextAccessor = contextAccessor;
            _context = entities;
        }

        /// <inheritdoc />
        public bool IsCorporateUser(int userId)
        {
            if (userId <= 0)
            {
                return false;
            }

            ReportUserMapping reportUserMapping = _userMappingService.GetReportUserMappingForUserId(userId);

            return reportUserMapping != null &&
                reportUserMapping.ZoneId == null &&
                reportUserMapping.RegionId == null &&
                reportUserMapping.SalesRepId == null;
        }

        /// <inheritdoc />
        public bool IsZoneUser(int userId)
        {
            if (userId <= 0)
            {
                return false;
            }

            ReportUserMapping reportUserMapping = _userMappingService.GetReportUserMappingForUserId(userId);

            return reportUserMapping?.ZoneId != null;
        }

        /// <inheritdoc />
        public bool IsRegionUser(int userId)
        {
            if (userId <= 0)
            {
                return false;
            }

            ReportUserMapping reportUserMapping = _userMappingService.GetReportUserMappingForUserId(userId);

            return reportUserMapping != null &&
                   reportUserMapping.ZoneId == null &&
                   reportUserMapping.RegionId != null;
        }

        /// <inheritdoc />
        public bool IsSalesRepUser(int userId)
        {
            if (userId <= 0)
            {
                return false;
            }

            ReportUserMapping reportUserMapping = _userMappingService.GetReportUserMappingForUserId(userId);

            return reportUserMapping != null &&
                   reportUserMapping.ZoneId == null &&
                   reportUserMapping.RegionId == null &&
                   reportUserMapping.SalesRepId != null;
        }

        /// <inheritdoc />
        public int? GetUserZoneId(int userId)
        {
            if (userId <= 0)
            {
                return null;
            }

            ReportUserMapping reportUserMapping = _userMappingService.GetReportUserMappingForUserId(userId);

            if (reportUserMapping == null)
            {
                return null;
            }

            if (reportUserMapping.ZoneId != null)
            {
                return reportUserMapping.ZoneId;
            }

            if (reportUserMapping.RegionId != null)
            {
                return reportUserMapping.ReportHierarchyRegion?.ZoneId;
            }

            if (reportUserMapping.SalesRepId != null)
            {
                return reportUserMapping.ReportHierarchySalesRep?.ReportHierarchyRegion?.ZoneId;
            }

            return null;
        }

        public int? GetUserRegionIdIfExistsForCurrentUser()
        {
            return GetUserRegionId(_contextAccessor.HttpContext.User.GetUserId());
        }

        /// <inheritdoc />
        public int? GetUserRegionId(int userId)
        {
            if (userId <= 0)
            {
                return null;
            }

            ReportUserMapping reportUserMapping = _userMappingService.GetReportUserMappingForUserId(userId);

            if (reportUserMapping == null)
            {
                return null;
            }

            if (reportUserMapping.RegionId != null)
            {
                return reportUserMapping.ReportHierarchyRegion?.Id;
            }

            if (reportUserMapping.SalesRepId != null)
            {
                return reportUserMapping.ReportHierarchySalesRep?.ReportHierarchyRegion?.Id;
            }

            return null;
        }

        /// <inheritdoc />
        public int? GetUserSalesRepId(int userId)
        {
            if (userId <= 0)
            {
                return null;
            }

            ReportUserMapping reportUserMapping = _userMappingService.GetReportUserMappingForUserId(userId);

            return reportUserMapping?.SalesRepId;
        }

        public List<ReportBillTo> GetUserBillTos(int userId, string search, out int totalCount, int skip = 0, int take = int.MaxValue)
        {
            IQueryable<ReportBillTo> result;

            if (search == null)
                search = "";

            if (IsCorporateUser(userId))
            {
                result = _context.ReportBillTo;
            }
            else if (IsZoneUser(userId))
            {
                var zoneId = GetUserZoneId(userId).Value;

                result = (from b in _context.ReportBillTo
                          from s in _context.ReportHierarchySalesRep.Where(p => p.Id == b.SalesRepId)
                          from r in _context.ReportHierarchyRegion.Where(p => p.Id == s.RegionId)
                          where r.ZoneId == zoneId
                          select b);
            }
            else if (IsRegionUser(userId))
            {
                var regionId = GetUserRegionId(userId).Value;

                result = (from b in _context.ReportBillTo
                          from s in _context.ReportHierarchySalesRep.Where(p => p.Id == b.SalesRepId)
                          where s.RegionId == regionId
                          select b);
            }
            else
            {
                var salesRepId = GetUserSalesRepId(userId);

                result = _context.ReportBillTo.Where(p => salesRepId == null || p.SalesRepId == salesRepId.Value);
            }

            //result = result.Where(item => EF.Functions.Like(item.FullBillToName, $"%{search}%")); efcore 3.1 simply wont translate this, try filtering when the new version comes out
            totalCount = result.Count();
            return result.Skip(skip).Take(take).ToList();
        }

        public List<ReportShipTo> GetUserShipTos(int userId, string search, out int totalCount, int skip = 0, int take = int.MaxValue)
        {
            IQueryable<ReportShipTo> result;

            if (search == null)
                search = "";

            if (IsCorporateUser(userId))
            {
                result = _context.ReportShipTo.AsQueryable();
            }
            else if (IsZoneUser(userId))
            {
                var zoneId = GetUserZoneId(userId).Value;

                result = (from b in _context.ReportShipTo
                          from s in _context.ReportHierarchySalesRep.Where(p => p.Id == b.SalesRepId)
                          from r in _context.ReportHierarchyRegion.Where(p => p.Id == s.RegionId)
                          where r.ZoneId == zoneId
                          select b).AsQueryable();
            }
            else if (IsRegionUser(userId))
            {
                var regionId = GetUserRegionId(userId).Value;

                result = (from b in _context.ReportShipTo
                          from s in _context.ReportHierarchySalesRep.Where(p => p.Id == b.SalesRepId)
                          where s.RegionId == regionId
                          select b).AsQueryable();
            }
            else
            {
                var salesRepId = GetUserSalesRepId(userId);

                result = _context.ReportShipTo.Where(p => salesRepId == null || p.SalesRepId == salesRepId.Value).AsQueryable();
            }

            //result = result.Where(item => item.FullShipToName.ToLower().Contains(search.ToLower()));
            //result = result.Where(item => EF.Functions.Like(item.FullShipToName, $"%{search}%")); efcore 3.1 simply wont translate this, try filtering when the new version comes out

            totalCount = result.Count();

            return result.Skip(skip).Take(take).ToList();
        }

        /// <summary>
        /// Checks if the user with the specified identifier can view data for the specified sales rep identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="salesRepId">The sales rep identifier.</param>
        /// <returns>true if the user can view data for the sales rep.</returns>
        public bool CanUserViewDataForSalesRep(int userId, int salesRepId)
        {
            if (salesRepId <= 0)
            {
                return false;
            }

            if (IsCorporateUser(userId))
            {
                return true;
            }

            int userIdForSalesRepId = GetUserIdForSalesRepId(salesRepId);

            if (IsZoneUser(userId))
            {
                bool areUsersInSameZone = AreUserInSameZone(userId, userIdForSalesRepId);

                return areUsersInSameZone;
            }

            if (IsRegionUser(userId))
            {
                bool areUsersInSameRegion = AreUsersInSameRegion(userId, userIdForSalesRepId);

                return areUsersInSameRegion;
            }

            if (IsSalesRepUser(userId))
            {
                int userSalesRepId = GetUserSalesRepId(userId) ?? 0;

                bool areEqual = userSalesRepId == salesRepId;

                return areEqual;
            }

            return false;
        }

        /// <summary>
        /// Gets the user identifier for the specified sales rep identifier.
        /// </summary>
        /// <param name="salesRepId">The sales rep identifier.</param>
        /// <returns>The user identifier if found.</returns>
        private int GetUserIdForSalesRepId(int salesRepId)
        {
            IQueryable<int> query = from sr in _context.ReportHierarchySalesRep
                                    join usr in _context.ReportUserMapping on sr.Id equals usr.SalesRepId
                                    where sr.Id == salesRepId && usr.SalesRepId == salesRepId
                                    select usr.UserId;

            int userId = query.FirstOrDefault();

            return userId;
        }

        /// <summary>
        /// Checks if the users are in the same zone.
        /// </summary>
        /// <param name="userId1">The first user identifier.</param>
        /// <param name="userId2">The second user identifier.</param>
        /// <returns>true if the users are in the same zone.</returns>
        private bool AreUserInSameZone(int userId1, int userId2)
        {
            int zoneId1 = GetUserZoneId(userId1) ?? 0;

            int zoneId2 = GetUserZoneId(userId2) ?? 0;

            bool areUsersInSameZone = zoneId1 == zoneId2;

            return areUsersInSameZone;
        }

        /// <summary>
        /// Checks if the users are in the same region.
        /// </summary>
        /// <param name="userId1">The first user identifier.</param>
        /// <param name="userId2">The second user identifier.</param>
        /// <returns>true if the users are in the same region.</returns>
        private bool AreUsersInSameRegion(int userId1, int userId2)
        {
            int regionId1 = GetUserRegionId(userId1) ?? 0;

            int regionId2 = GetUserRegionId(userId2) ?? 0;

            bool areUsersInSameRegion = regionId1 == regionId2;

            return areUsersInSameRegion;
        }

        /// <summary>
        /// Gets Sales Rep Name by Sales Rep Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetSalesRepNameById(int id)
        {
            var name = _context.ReportHierarchySalesRep.FirstOrDefault(x => x.Id == id)?.Name ?? string.Empty;

            return name;
        }
    }
}