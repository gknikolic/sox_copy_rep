﻿namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVarianceSalesForMonthModel
    {
        public int Year { get; set; }

        public int Month { get; set; }

        public decimal ExtPrice { get; set; }

        public decimal ExtCost { get; set; }

        public decimal GrossProfit => ExtPrice - ExtCost;
    }
}