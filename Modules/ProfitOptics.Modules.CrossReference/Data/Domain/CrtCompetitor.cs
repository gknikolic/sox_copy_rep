using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.CrossReference.Data.Domain
{
    public partial class CrtCompetitor
    {
        public CrtCompetitor()
        {
            CrtCrossReferenceRecords = new HashSet<CrtCrossReferenceRecord>();
        }

        public int Id { get; set; }

        public string CompetitorName { get; set; }

        public bool IsMainCompetitorName { get; set; }

        public string CompetitorDescription { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual ICollection<CrtCrossReferenceRecord> CrtCrossReferenceRecords { get; set; }
    }
}
