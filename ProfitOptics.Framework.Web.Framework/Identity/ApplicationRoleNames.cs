﻿namespace ProfitOptics.Framework.Web.Framework.Identity
{
    public static class ApplicationRoleNames
    {
        public static string Admin => "admin";
        public static string SuperAdmin => "superadmin";
        public static string User => "user";
        public static string VendorUser => "vendoruser";
        public static string CustomerUser => "customeruser";
        public static string Driver => "driver";
        public static string Reporting => "reporting";
        public static string Quoting => "quoting";
        public static string UserManager => "usermanager";
    }
}