﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.DataLayer.Test;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    class TManagerConfig : IEntityTypeConfiguration<TManager>
    {
        public void Configure(EntityTypeBuilder<TManager> builder)
        {
            builder.ToTable("TManager");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.FirstName).HasMaxLength(50).IsRequired();
            builder.Property(x => x.LastName).HasMaxLength(50).IsRequired();
            builder.Property(x => x.Title).HasMaxLength(50);

            builder.HasMany(x => x.Employees)
                .WithOne(x => x.Manager)
                .HasForeignKey(x => x.ManagerId);
        }
    }
}
