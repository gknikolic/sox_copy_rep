﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;

namespace ProfitOptics.Modules.UpsellItem.Controllers
{
    [Area("UpsellItem")]
    [Authorize(Roles = "upsellitem,admin")]
    public class UpsellItemController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Update()
        {
            return View();
        }
    }
}
