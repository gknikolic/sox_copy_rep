﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class AspNetUserConfig : IEntityTypeConfiguration<AspNetUser>
    {
        public void Configure(EntityTypeBuilder<AspNetUser> builder)
        {
            builder.ToTable("AspNetUsers");
            builder.HasKey(u => u.Id);

            builder.Property(u => u.FirstName).HasMaxLength(100);
            builder.Property(u => u.LastName).HasMaxLength(100);
            builder.Property(u => u.StartTime);
            builder.Property(u => u.EndTime);
            builder.Property(u => u.Approved);
            builder.Property(u => u.Email).HasMaxLength(256);
            builder.Property(u => u.EmailConfirmed);
            builder.Property(u => u.PasswordHash).HasMaxLength(255);
            builder.Property(u => u.SecurityStamp).HasMaxLength(255);
            builder.Property(u => u.PhoneNumber).HasMaxLength(50);
            builder.Property(u => u.PhoneNumberConfirmed);
            builder.Property(u => u.TwoFactorEnabled);
            builder.Property(u => u.LockoutEndDateUtc);
            builder.Property(u => u.LockoutEnabled);
            builder.Property(u => u.AccessFailedCount);
            builder.Property(u => u.UserName).HasMaxLength(256).IsRequired();
            builder.Property(u => u.IsGoogleAuthenticatorEnabled);
            builder.Property(u => u.GoogleAuthenticatorSecretKey);
            builder.Property(u => u.IsEnabled);

            builder.HasMany(u => u.ReportUserFilters)
                .WithOne(f => f.AspNetUser)
                .HasForeignKey(f => f.UserId);

            builder.HasMany(u => u.ReportUserLogins)
                .WithOne(l => l.AspNetUser)
                .HasForeignKey(l => l.UserId);
        }
    }
}
