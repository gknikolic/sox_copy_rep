﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class ExternalLoginListModel
    {
        public ExternalLoginListModel()
        {
            ExternalLoginProviders = new List<ExternalLoginProviderModel>();
        }

        public string ReturnUrl { get; set; }
        public List<ExternalLoginProviderModel> ExternalLoginProviders { get; set; }
        public string ExternalLoginCallbackUrl { get; set; }
    }
}
