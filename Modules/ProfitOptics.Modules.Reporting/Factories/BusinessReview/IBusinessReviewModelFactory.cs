﻿using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.BusinessReview;

namespace ProfitOptics.Modules.Reporting.Factories.BusinessReview
{
    public interface IBusinessReviewModelFactory
    {
        /// <summary>
        /// Prepares BusinessReviewViewModel, sets report types and max allowed number of characters in notes.
        /// </summary>
        /// <param name="currentUserId">Customer identifier</param>
        /// <returns></returns>
        BusinessReviewViewModel PrepareBusinessReviewViewModel(int currentUserId);

        /// <summary>
        /// Prepares BusinessReviewReportFileModel, generates excel report file, sets file name and content type.
        /// </summary>
        /// <param name="customerId">Customer identifier.</param>
        /// <param name="reportType">Report type. ex. BillTo, ShipTo..</param>
        /// <param name="notes">Notes to be placed in the excel report.</param>
        /// <returns></returns>
        BusinessReviewReportFileModel PrepareBusinessReviewReportModel(int customerId, BusinessReviewReportType reportType, string notes);

        /// <summary>
        /// Prepares models for select 2 dropdowns for the provided report type and customer identifier
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="reportType"></param>
        /// <param name="page"></param>
        /// <param name="take"></param>
        /// <returns>Select2PagedResult</returns>
        Select2PagedResult PrepareBusinessReviewDropdownModelForReportType(int customerId, BusinessReviewReportType reportType, string search, int page = 0, int take = int.MaxValue);
    }
}
