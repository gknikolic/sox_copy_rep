﻿//using System;
//using System.Drawing;
//using System.IO;
////using System.Xml.Linq;
//using Dynamsoft.Barcode;

//namespace BarcodeDLL
//{
//    public class BarrecodeReaderRepo
//    {
//        private static readonly char CSep = Path.DirectorySeparatorChar;
//        private static readonly string StrPath = AppDomain.CurrentDomain.BaseDirectory + "Images";
//        private static readonly string CollectFolder = StrPath + CSep + "Collect";

//        public static string Barcode(string strImgBase64, Int64 iformat, int iMaxNumbers, ref string strResult)
//        {
//            if (string.IsNullOrEmpty(strImgBase64.Trim())) throw new Exception("No barcode exist.");

//            return DoBarcode(strImgBase64, iformat, iMaxNumbers, ref strResult);
//        }

//        private static string DoBarcode(string strImgBase64, Int64 format, int iMaxNumbers, ref string strResult)
//        {
//            strResult = "";
//            var strResturn = "[";

//            var listResult = GetBarcode(strImgBase64, format, iMaxNumbers);
//            if (listResult == null || listResult.Length == 0)
//            {
//                strResult = "No barcode found. ";
//                return "[]";
//            }
//            strResult = "Total barcode(s) found: " + listResult.Length + "";
//            var i = 1;
//            foreach (var item in listResult)
//            {
//                strResult = strResult + "&nbsp&nbspBarcode " + i + ":";
//                strResult = strResult + "&nbsp&nbsp&nbsp&nbspType: " + item.BarcodeFormat + "";
//                strResult = strResult + "&nbsp&nbsp&nbsp&nbspValue: " + item.BarcodeText + "";
//                strResult = strResult + "&nbsp&nbsp&nbsp&nbspRegion: {Left: " + item.ResultPoints[0].X
//                            + ", Top: " + item.ResultPoints[0].Y
//                            + ", Width: " + item.BoundingRect.Width
//                            + ", Height: " + item.BoundingRect.Height + "}";
//                i++;
//            }
//            strResturn = strResturn.Substring(0, strResturn.Length - 1);
//            strResturn += "]";
//            return strResturn;
//        }

//        public static BarcodeResult[] GetBarcode(string strImgBase64, Int64 format, int iMaxNumbers)
//        {
//            var reader = new BarcodeReader();
//            var options = new ReaderOptions
//            {
//                MaxBarcodesToReadPerPage = iMaxNumbers,
//                BarcodeFormats = (BarcodeFormat)format
//            };

//            reader.ReaderOptions = options;
//            reader.LicenseKeys = "t0076xQAAABwun8gt4q2KPhUMuU9O+NweXAc8rv8LggNTX6SH+Sp03BUn/wLEGE7wUQ02kdrYTHxHt1M4Jk4LuDfV7W7fHdxNNcsHtKQrVg==";
//            return reader.DecodeBase64String(strImgBase64);
//        }
//    }
//}