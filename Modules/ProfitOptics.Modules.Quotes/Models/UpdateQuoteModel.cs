﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class UpdateQuoteModel
    {
        public int QuoteId { get; set; }

        [Required(ErrorMessage = "Quote name is required.")]
        [MaxLength(50, ErrorMessage = "Quote name is limited to 50 characters.")]
        public string QuoteName { get; set; }

        [MaxLength(250, ErrorMessage = "Quote description is limited to 250 characters.")]
        public string QuoteDescription { get; set; }

        public DateTime? PresentedToCustomer { get; set; }
        public string Status { get; set; }
        public DateTime? ValidThroughDate { get; set; }
        public string HeaderComments { get; set; }
        public string FooterComments { get; set; }
        public string ContactEmail { get; set; }
        public string CustomerReference { get; set; }
        public bool NewBusiness { get; set; }
        public DateTime? EstimatedFirstOrderDate { get; set; }
        public decimal? Probability { get; set; }
        public decimal? ShippingCost { get; set; }
        public decimal? TaxAmount { get; set; }
        public int? CustomerShipToId { get; set; }
        public string ShippingMethod { get; set; }
    }
}