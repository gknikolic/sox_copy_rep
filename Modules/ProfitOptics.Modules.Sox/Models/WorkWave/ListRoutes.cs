﻿using Newtonsoft.Json;
using ProfitOptics.Modules.Sox.Models.WorkWave;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class ListRoutes
	{
		[JsonProperty("routes")]
		public Dictionary<string,Route> Routes { get; set; }

		[JsonProperty("drivers")]
		public Dictionary<string, Driver> Drivers { get; set; }
		[JsonProperty("vehicles")]
		public Dictionary<string, Vehicle> Vehicles { get; set; }
	}


}
