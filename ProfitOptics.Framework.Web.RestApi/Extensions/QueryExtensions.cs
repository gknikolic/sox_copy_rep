﻿using ProfitOptics.Framework.Web.RestApi.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ProfitOptics.Framework.Web.RestApiExtensions
{
    public static class QueryExtensions
    {
        public static IQueryable<T> ApplySort<T>(this IQueryable<T> source, Query query)
        {
            if (query.SortColumn == null)
                return source;

            return source.OrderBy(query.SortColumn, query.SortDirection == "desc");
        }

        public static IQueryable<T> ApplyPagination<T>(this IQueryable<T> source, Query query)
        {
            return source
                .Skip((query.Page - 1) * query.ItemsPerPage)
                .Take(query.ItemsPerPage);
        }

        public static IQueryable<T> ApplyFiltering<T>(this IQueryable<T> source, Query query)
        {
            if (string.IsNullOrEmpty(query.FilterColumn) || string.IsNullOrEmpty(query.FilterValue))
                return source;

            return source.FilterBy(query.FilterColumn, query.FilterValue);
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, string orderByProperty, bool desc)
        {
            string command = desc ? "OrderByDescending" : "OrderBy";
            
            var type = typeof(T);
            var property = type.GetProperty(orderByProperty, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType }, source.Expression, Expression.Quote(orderByExpression));

            return source.Provider.CreateQuery<T>(resultExpression);
        }

        public static IQueryable<T> FilterBy<T>(this IQueryable<T> source, string filterByProperty, string filterValue)
        {
            var type = typeof(T);
            var property = type.GetProperty(filterByProperty, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);   

            var parameter = Expression.Parameter(type, "p");
            var memberExpression = Expression.MakeMemberAccess(parameter, property);

            MethodInfo toStringMethodInfo = typeof(string).GetMethod("ToString", new Type[] { });
            Expression toStringCall = Expression.Call(memberExpression, toStringMethodInfo);

            MethodInfo toLowerMethodInfo = typeof(string).GetMethod("ToLower", new Type[] { });
            Expression toStringToLowerCall = Expression.Call(toStringCall, toLowerMethodInfo);

            MethodInfo containsMethodInfo = typeof(string).GetMethod("Contains", new Type[] { typeof(string) });
            ConstantExpression constantExpression = Expression.Constant(filterValue, typeof(string));

            Expression toStringToLowerContainsCall = Expression.Call(toStringToLowerCall, containsMethodInfo, constantExpression);

            var lambda = Expression.Lambda<Func<T, bool>>(toStringToLowerContainsCall, parameter);

            var resultExpression = Expression.Call(typeof(Queryable), "Where", new Type[] { source.ElementType }, source.Expression, Expression.Quote(lambda));

            return source.Provider.CreateQuery<T>(resultExpression);
        }
    }
}
