﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerType
    {
        public int Id { get; set; }
        public int? GCVendorId { get; set; }
        public int CTContainerServiceId { get; set; }
        public long LegacyId { get; set; }
        public long ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string HomeLocationName { get; set; }
        public string ProcessingLocationName { get; set; }
        public string Name { get; set; }
        public string SterilizationMethodName { get; set; }
        public string Modified { get; set; }
        public bool IsDiscarded { get; set; }
        public bool AutomaticActuals { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
        //public string SKU { get; set; }

        public long? FacilityId { get; set; }
        public long? SterilizationMethodId { get; set; }
        public long? FirstAlternateMethodId { get; set; }
        public long? SecondAlternateMethodId { get; set; }
        public long? ThirdAlternateMethodId { get; set; }
        public long? HomeLocationId { get; set; }
        public long? ProcessingLocationId { get; set; }
        public string PhysicianName { get; set; }
        public bool? UsesBeforeService { get; set; }
        public string UsageInterval { get; set; }
        public string UsageIntervalUnitOfMeasure { get; set; }
        public string StandardAssemblyTime { get; set; }
        public string Weight { get; set; }
        public string ProcessingCost { get; set; }
        public string ProcurementReferenceId { get; set; }
        public string DecontaminationInstructions { get; set; }
        public string AssemblyInstructions { get; set; }
        public string SterilizationInstructions { get; set; }
        public string CountSheetComments { get; set; }
        public long? PriorityId { get; set; }
        public bool? BiologicalTestRequired { get; set; }
        public bool? ClassSixTestRequired { get; set; }
        public bool? AssemblyByException { get; set; }
        public bool? SoftWrap { get; set; }
        public long? ComplexityLevelId { get; set; }
        public long? OriginalContainerTypeId { get; set; }
        public long? VendorId { get; set; }
        public string VendorName { get; set; }


        //public virtual GCVendor GCVendor { get; set; }
        public virtual CTContainerService CTContainerService { get; set; }

        public virtual List<CTContainerItem> CTContainerItems { get; set; }
    }
}
