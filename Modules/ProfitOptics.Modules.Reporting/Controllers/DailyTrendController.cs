﻿using System;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Factories.DailyTrend;
using ProfitOptics.Modules.Reporting.Models.DailyTrend;
using ProfitOptics.Modules.Reporting.Services.DailyTrend;

namespace ProfitOptics.Modules.Reporting.Controllers
{
    [Authorize(Roles = "admin, reporting")]
    public class DailyTrendController : BaseReportingController
    {
        private readonly IDailyTrendModelFactory _dailyTrendModelFactory;
        private readonly IDailyTrendExportService _dailyTrendExportService;

        public DailyTrendController(IDailyTrendModelFactory dailyTrendModelFactory, IDailyTrendExportService dailyTrendExportService)
        {
            _dailyTrendModelFactory = dailyTrendModelFactory;
            _dailyTrendExportService = dailyTrendExportService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            DailyTrendViewModel vm = _dailyTrendModelFactory.CreateDailyTrendViewModel(GetCurrentUserId());

            return View(vm);
        }

        [HttpGet]
        public IActionResult GetDailyTrends([ModelBinder(typeof(DataTablesBinder))]IDataTablesRequest request,
            [FromQuery]DateRangeDto dateRange)
        {
            PagedList<DailyTrendModel> models = _dailyTrendModelFactory.CreateDailyTrendModels(GetCurrentUserId(), dateRange);

            return new LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpGet]
        public IActionResult DrillThroughAll([FromQuery]DateTime drillThroughDate)
        {
            DailyTrendDrillThroughAllViewModel vm = _dailyTrendModelFactory.CreateDailyTrendDrillThroughAllViewModel(GetCurrentUserId(),
                drillThroughDate);

            return View(vm);
        }

        [HttpPost]
        public IActionResult GetInvoicesForDate([ModelBinder(typeof(DataTablesBinder))]IDataTablesRequest request,
            [FromQuery]DateTime drillThroughDate)
        {
            PagedList<DailyTrendDetailsModel> models = _dailyTrendModelFactory.CreateDailyTrendDetailsModels(
                GetCurrentUserId(), request, drillThroughDate);

            return new LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpGet]
        public IActionResult DrillThroughByType(DailyTrendDrillThroughType drillThroughType, DateTime drillThroughDate)
        {
            DailyTrendDrillThroughByTypeViewModel vm = _dailyTrendModelFactory.CreateDailyTrendDrillThroughByTypeViewModel(
                GetCurrentUserId(), drillThroughType, drillThroughDate);

            return View(vm);
        }

        [HttpPost]
        public IActionResult GetInvoicesGroupedByType([ModelBinder(typeof(DataTablesBinder))]IDataTablesRequest request,
            [FromQuery]DailyTrendDrillThroughType drillThroughType, 
            [FromQuery]DateTime drillThroughDate)
        {
            PagedList<DailyTrendGroupedModel> models = _dailyTrendModelFactory.CreateDailyTrendGroupedModels(
                GetCurrentUserId(), request, drillThroughType, drillThroughDate);

            return new LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpGet]
        public IActionResult DrillThroughInvoice(string billToNum, DateTime drillThroughDate)
        {
            DailyTrendDrillThroughInvoiceViewModel vm = _dailyTrendModelFactory.DailyTrendDrillThroughInvoiceViewModel(
                GetCurrentUserId(), billToNum, drillThroughDate);

            return View(vm);
        }

        [HttpPost]
        public IActionResult GetInvoiceItemsForDate([ModelBinder(typeof(DataTablesBinder))]IDataTablesRequest request,
            [FromQuery]int billToId, [FromQuery]DateTime drillThroughDate)
        {
            PagedList<DailyTrendInvoiceItemModel> models = _dailyTrendModelFactory.CreateDailyTrendInvoiceItemModels(
                GetCurrentUserId(), request, billToId, drillThroughDate);

            return new LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpGet]
        public IActionResult ExportDailyTrendsForDateRangeToExcel([FromQuery]DateRangeDto dateRange)
        {
            return ExportDailyTrendsForDateRangeToExcel(dateRange.StartDate, dateRange.EndDate);
        }

        [HttpGet]
        public IActionResult ExportDailyTrendsForDate([FromQuery]DateTime dateTime)
        {
            return ExportDailyTrendsForDateRangeToExcel(dateTime, dateTime);
        }

        [HttpGet]
        public IActionResult ExportDailyTrendsForCurrentMonths()
        {
            DateTime now = DateTime.Now;

            return ExportDailyTrendsForDateRangeToExcel(new DateTime(now.Year, now.Month, 1), now);
        }

        private IActionResult ExportDailyTrendsForDateRangeToExcel(DateTime startDate, DateTime endDate)
        {
            MemoryStream excelFile =
                _dailyTrendExportService.ExportDailyTrendsExcel(GetCurrentUserId(), startDate, endDate);

            return File(excelFile,
                ExcelFile.ExcelContentType,
                $"Daily Trend Report {DateTime.Now:MM-dd-yyyy--hh-mm}.xlsx");
        }
    }
}
