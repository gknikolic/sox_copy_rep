﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.DailyTrend;
using ProfitOptics.Modules.Reporting.Services;
using ProfitOptics.Modules.Reporting.Services.DailyTrend;

namespace ProfitOptics.Modules.Reporting.Factories.DailyTrend
{
    public class DailyTrendModelFactory : IDailyTrendModelFactory
    {
        private readonly IDailyTrendDataService _dailyTrendDataService;
        private readonly IUserHierarchyService _userHierarchyService;

        public DailyTrendModelFactory(IDailyTrendDataService dailyTrendDataService,
            IUserHierarchyService userHierarchyService)
        {
            _dailyTrendDataService = dailyTrendDataService;
            _userHierarchyService = userHierarchyService;
        }

        /// <inheritdoc />
        public DailyTrendViewModel CreateDailyTrendViewModel(int userId)
        {
            return new DailyTrendViewModel
            {
                IsCorporate = _userHierarchyService.IsCorporateUser(userId),
                IsSalesRep = _userHierarchyService.IsSalesRepUser(userId),
                StartDateDefault = DateTime.Now.AddDays(-90).ToString("MM-dd-yyyy"),
                EndDateDefault = DateTime.Now.AddDays(-1).ToString("MM-dd-yyyy")
            };
        }

        /// <inheritdoc />
        public PagedList<DailyTrendModel> CreateDailyTrendModels(int userId, DateRangeDto dateRange)
        {
            bool isCorporate = _userHierarchyService.IsCorporateUser(userId);

            PagedList<IGrouping<DateTime, ReportSalesHistory>> salesHistoriesGroupedByDate = _dailyTrendDataService
                .GetSalesHistoriesGroupedByDate(userId, dateRange);

            List<DailyTrendModel> models = salesHistoriesGroupedByDate
            .Select(dailySales => new DailyTrendModel
            {
                InvoiceDate = dailySales.Key,
                Sales = dailySales.Sum(x => x.ExtPrice),
                GrossProfit = isCorporate ? dailySales.Sum(x => x.ExtPrice - x.ExtCost) : decimal.Zero,
                GrossMarginPercent = isCorporate ?
                    dailySales.Sum(x => x.ExtPrice) != decimal.Zero ?
                        (dailySales.Sum(x => x.ExtPrice) - dailySales.Sum(x => x.ExtCost)) / dailySales.Sum(x => x.ExtPrice)
                        : decimal.Zero :
                    dailySales.Sum(x => x.ExtPrice) != decimal.Zero ?
                        (dailySales.Sum(x => x.ExtPrice) - dailySales.Sum(x => x.ExtCost)) / dailySales.Sum(x => 2.0m * x.ExtPrice)
                        : decimal.Zero
            }).ToList();

            List<DailyTrendModel> orderedModels = AddMissingMonthsData(dateRange, models)
                .OrderByDescending(d => d.InvoiceDate)
                .ToList();

            return new PagedList<DailyTrendModel>(orderedModels, orderedModels.Count);
        }

        /// <inheritdoc />
        public DailyTrendDrillThroughAllViewModel CreateDailyTrendDrillThroughAllViewModel(int userId, DateTime drillThroughDate)
        {
            return new DailyTrendDrillThroughAllViewModel
            {
                IsCorporate = _userHierarchyService.IsCorporateUser(userId),
                DrillThroughDate = drillThroughDate.ToString("MM-dd-yyyy")
            };
        }

        /// <inheritdoc />
        public PagedList<DailyTrendDetailsModel> CreateDailyTrendDetailsModels(int userId,
            IDataTablesRequest request, DateTime drillThroughDate)
        {
            PagedList<ReportSalesHistory> salesHistoriesForDate = _dailyTrendDataService
                .GetSalesHistoriesForDate(userId, request, drillThroughDate);

            List<DailyTrendDetailsModel> models = salesHistoriesForDate
                .Select(salesHistory => new DailyTrendDetailsModel
                {
                    BillToNum = salesHistory.ReportBillTo.BillToNum,
                    BillToName = salesHistory.ReportBillTo.BillToName,
                    ShipToNum = salesHistory.ReportShipTo.ShipToNum,
                    ShipToName = salesHistory.ReportShipTo.ShipToName,
                    VendorName = salesHistory.ReportVendor.Name,
                    ItemNum = salesHistory.ReportItem.ItemNum,
                    InvoiceDate = salesHistory.InvoiceDate,
                    CustomerClass = salesHistory.ReportCustomerClass.Name,
                    ItemCategory = salesHistory.ReportItemCategory.Name,
                    Quantity = salesHistory.Qty,
                    ExtPrice = salesHistory.ExtPrice
                })
                .ToList();

            return new PagedList<DailyTrendDetailsModel>(models, salesHistoriesForDate.TotalCount);
        }

        /// <inheritdoc />
        public DailyTrendDrillThroughByTypeViewModel CreateDailyTrendDrillThroughByTypeViewModel(int userId,
            DailyTrendDrillThroughType drillThroughType, DateTime drillThroughDate)
        {
            return new DailyTrendDrillThroughByTypeViewModel
            {
                IsCorporate = _userHierarchyService.IsCorporateUser(userId),
                DrillThroughType = drillThroughType,
                DrillThroughDate = drillThroughDate.ToString("MM-dd-yyyy")
            };
        }

        /// <inheritdoc />
        public PagedList<DailyTrendGroupedModel> CreateDailyTrendGroupedModels(int userId, IDataTablesRequest request, DailyTrendDrillThroughType drillThroughType,
            DateTime drillThroughDate)
        {
            bool isCorporate = _userHierarchyService.IsCorporateUser(userId);

            PagedList<IGrouping<DrillThroughTypeKey, ReportSalesHistory>> salesHistoriesGroupedByType = _dailyTrendDataService
                .GetSalesHistoriesGroupedByType(userId, request, drillThroughType, drillThroughDate);

            List<DailyTrendGroupedModel> models = salesHistoriesGroupedByType
                .Select(groupedSales => new DailyTrendGroupedModel
                {
                    GroupCode = groupedSales.Key.GroupCode,
                    GroupName = groupedSales.Key.GroupName,
                    Sales = groupedSales.Sum(x => x.ExtPrice),
                    GrossProfit = isCorporate ? groupedSales.Sum(x => x.ExtPrice - x.ExtCost) : decimal.Zero,
                    GrossMarginPercent = groupedSales.Sum(x => x.ExtPrice) != decimal.Zero ?
                        (groupedSales.Sum(x => x.ExtPrice) - groupedSales.Sum(x => x.ExtCost)) / groupedSales.Sum(x => x.ExtPrice)
                        : decimal.Zero
                })
                .ToList();

            return new PagedList<DailyTrendGroupedModel>(models, salesHistoriesGroupedByType.TotalCount);
        }

        /// <inheritdoc />
        public DailyTrendDrillThroughInvoiceViewModel DailyTrendDrillThroughInvoiceViewModel(int userId, string billToNum,
            DateTime drillThroughDate)
        {
            ReportBillTo billTo = _dailyTrendDataService.GetBillToForBillToNum(billToNum);

            return new DailyTrendDrillThroughInvoiceViewModel
            {
                IsCorporate = _userHierarchyService.IsCorporateUser(userId),
                Date = drillThroughDate.ToString("MM-dd-yyyy"),
                BillToId = billTo?.Id ?? 0,
                BillToNum = billTo?.BillToNum ?? string.Empty,
                BillToName = billTo?.BillToName ?? string.Empty
            };
        }

        /// <inheritdoc />
        public PagedList<DailyTrendInvoiceItemModel> CreateDailyTrendInvoiceItemModels(int userId, IDataTablesRequest request, int billToId,
            DateTime drillThroughDate)
        {
            PagedList<ReportSalesHistory> salesHistoriesForDate = _dailyTrendDataService
                .GetSalesHistoriesForBillToIdAndDate(userId, request, billToId, drillThroughDate);

            List<DailyTrendInvoiceItemModel> models = salesHistoriesForDate
                .Select(salesHistory => new DailyTrendInvoiceItemModel
                {
                    ItemNumber = salesHistory.ReportItem.ItemNum,
                    ItemDescription = salesHistory.ReportItem.ItemDescription,
                    UnitPrice = salesHistory.UnitPrice ?? 0.0m,
                    UnitCost = salesHistory.UnitCost ?? 0.0m,
                    ExtPrice = salesHistory.ExtPrice,
                    ExtCost = salesHistory.ExtCost,
                    Qty = salesHistory.Qty,
                    GrossProfitPercent = salesHistory.ExtPrice != decimal.Zero ? 
                        (salesHistory.ExtPrice - salesHistory.ExtCost) / salesHistory.ExtPrice 
                        : decimal.Zero
                }).ToList();

            return new PagedList<DailyTrendInvoiceItemModel>(models, salesHistoriesForDate.TotalCount);
        }

        private static IEnumerable<DailyTrendModel> AddMissingMonthsData(DateRangeDto dateRange, IEnumerable<DailyTrendModel> models)
        {
            Dictionary<DateTime, DailyTrendModel> resultsDict = models.ToDictionary(p => p.InvoiceDate, p => p);
            DateTime currentDate = dateRange.StartDate;
            while (currentDate <= dateRange.EndDate)
            {
                if (!resultsDict.ContainsKey(currentDate))
                {
                    resultsDict.Add(currentDate, new DailyTrendModel
                    {
                        InvoiceDate = currentDate,
                        Sales = decimal.Zero,
                        GrossProfit = decimal.Zero,
                        GrossMarginPercent = decimal.Zero
                    });
                }

                currentDate = currentDate.AddDays(1);
            }

            return resultsDict.Values;
        }
    }
}