USE [Sox]

ALTER TABLE [Sox].[dbo].[GCTray] ADD [GCLifecycleStatusTypeId] INT NULL;
GO

ALTER TABLE [dbo].[GCTray]  WITH CHECK ADD  CONSTRAINT [FK_GCTray_GCLifecycleStatusType] FOREIGN KEY([GCLifecycleStatusTypeId])
REFERENCES [dbo].[GCLifecycleStatusType] ([Id])
GO
ALTER TABLE [dbo].[GCTray] CHECK CONSTRAINT [FK_GCTray_GCLifecycleStatusType]
GO
