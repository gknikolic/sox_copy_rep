﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCVendor
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
        public long? LegacyId { get; set; }
        public DateTime? CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
        public int? QBVendorId { get; set; }
        public string Abbreviation { get; set; }

        public virtual ICollection<CTContainerType> CTContainerTypes { get; set; }
    }
}
