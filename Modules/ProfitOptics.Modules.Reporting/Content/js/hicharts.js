﻿function renderHicharts() {
    var chartTypePercentage;

    if ($("#StatisticsModel_MarginBridgeType").val() === "") {
        chartTypePercentage = $("#view-by-percent").hasClass("btn-primary");
    } else {
        chartTypePercentage = $("#StatisticsModel_MarginBridgeType").val() === "0";
    }

    var chartStartMargin = parseFloat($('#chartStartMargin').val().replace(',', '.'));
    var chartPrice = parseFloat($('#chartPrice').val().replace(',', '.'));
    var chartCost = parseFloat($('#chartCost').val().replace(',', '.'));
    var chartVolume = parseFloat($('#chartVolume').val().replace(',', '.'));
    var chartProductMix = parseFloat($('#chartProductMix').val().replace(',', '.'));
    var chartRebateRate = parseFloat($('#chartRebateRate').val().replace(',', '.'));
    var chartRebateVol = parseFloat($('#chartRebateVol').val().replace(',', '.'));
    var chartRebateProdMix = parseFloat($('#chartRebateProdMix').val().replace(',', '.'));
    var chartEndMargin = parseFloat($('#chartEndMargin').val().replace(',', '.'));

    function LabelFormatByMarginType(value) {
        return chartTypePercentage ? value === 0 ? "0%" : value > 0 ? "+" + value + '%' : value + '%'
            : value === 0 ? "$0K" : value > 0 ? "+$" + (value / 1000).toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,') + "K" : "-$" + (Math.abs(value) / 1000).toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,') + "K";
    }

    var chart = new Highcharts
   .Chart({
        exporting: { enabled: false },
        credits: { enabled: false },
        chart: {
            type: 'waterfall',
            renderTo: 'container',
        },

        title: {
            text: ''
        },

        xAxis: {
            type: 'category',
            legend: {
                text: ''
            },
            tickWidth: 0
        },

        yAxis: {
            gridLineWidth: 0,
            labels:
            {
                enabled: false
            },
            legend: {
                text: ''
            },
            title: {
                text: ''
            },
            tickWidth: 0,
            min: 0
        },

        legend: {
            enabled: false
        },

        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true,
            headerFormat: '',
            enabled: false,
        },

        series: [{
            upColor: '#00aa00',
            color: '#ff0000',
            data: [{
                name: 'START MARGIN',
                y: chartStartMargin,
                color: '#5A5B5D',
                dataLabels: {
                    formatter: function () {
                        return LabelFormatByMarginType(this.y);
                    },
                    verticalAlign: 'center',
                    color: '#111111',
                    style: {
                        textShadow: false
                    }
                }
            },
            {
                name: 'PRICE',
                y: chartPrice,
                dataLabels: {
                    formatter: function () {
                        return LabelFormatByMarginType(this.y);
                    },
                    color: chartPrice == 0 ? '#5A5B5D' : chartPrice > 0 ? '#00aa00' : '#aa0000',
                    style: {
                        textShadow: false
                    }

                }
            },
            {
                name: 'COST',
                y: chartCost,
                dataLabels: {
                    formatter: function () {
                        return LabelFormatByMarginType(this.y);
                    },
                    color: chartCost == 0 ? '#5A5B5D' : chartCost > 0 ? '#00aa00' : '#aa0000',
                    style: {
                        textShadow: false
                    }
                }
                
            },

            {
                name: 'VOLUME',
                y: chartVolume,
                dataLabels: {
                    formatter: function () {
                        return LabelFormatByMarginType(this.y);
                    },
                    color: chartVolume == 0 ? '#5A5B5D' : chartVolume > 0 ? '#00aa00' : '#aa0000',
                }
            },

            {
                name: 'PRODUCT MIX',
                y: chartProductMix,
                dataLabels: {
                    formatter: function () {
                        return LabelFormatByMarginType(this.y);
                    },
                    color: chartProductMix == 0 ? '#5A5B5D' : chartProductMix > 0 ? '#00aa00' : '#aa0000',
                    style: {
                        textShadow: false
                    }
                },

            },
            {
                name: 'REBATE RATE',
                y: chartRebateRate,
                dataLabels: {
                    formatter: function () {
                        return LabelFormatByMarginType(this.y);
                    },
                    color: chartRebateRate == 0 ? '#5A5B5D' : chartRebateRate > 0 ? '#00aa00' : '#aa0000',
                    style: {
                        textShadow: false
                    }
                },

                
            },
            {
                name: 'REBATE VOL',
                y: chartRebateVol,
                dataLabels: {
                    formatter: function () {
                        return LabelFormatByMarginType(this.y);
                    },
                    color: chartRebateVol == 0 ? '#5A5B5D' : chartRebateVol > 0 ? '#00aa00' : '#aa0000',
                },
                style: {
                    textShadow: false
                }
            },
            {
                name: 'REBATE PROD MIX',
                y: chartRebateProdMix,
                dataLabels: {
                    formatter: function () {
                        return LabelFormatByMarginType(this.y);
                    },
                    color: chartRebateProdMix == 0 ? '#5A5B5D' : chartRebateProdMix > 0 ? '#00aa00' : '#aa0000',
                    style: {
                        textShadow: false
                    }
                }
            },
            {
                name: 'END MARGIN',
                y: -1 * chartEndMargin,
                color: '#5A5B5D',
                dataLabels: {
                    formatter: function () {
                        return LabelFormatByMarginType(-this.y);
                    },
                    color: '#5A5B5D',
                    style: {
                        textShadow: false
                    }
                }
            }],
            dataLabels: {
                enabled: true,
                formatter: function () {
                    return this.y > 0 ? "+" + this.y + '%' : this.y + '%';
                },
                style: {
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                    textShadow: false,
                    fontSize: '16px'
                }
            },
            pointPadding: 0
        }]
   }, function (chart) {
       var maximumY = chart.axes[0].height - 4;

       $.each(chart.series, function (i, serie) {
           $.each(serie.data, function (j, data) {
               if (data.shapeArgs.y < chart.axes[0].height) {
                   if (data.y >= 0 || j == serie.data.length - 1) {
                       data.dataLabel.translate(data.dataLabel.translateX, data.shapeArgs.y - 24);
                   } else {
                       data.dataLabel.translate(data.dataLabel.translateX, Math.min(data.shapeArgs.height + data.shapeArgs.y - 4, maximumY));
                   }
               }
           });
       });
   });

}