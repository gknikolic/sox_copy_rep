using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.Web.RestApi.DataLayer;
using ProfitOptics.Framework.Web.RestApi.Models;
using ProfitOptics.Framework.Web.RestApi.Models.Customer;
using ProfitOptics.Framework.Web.RestApi.Services.Product;
using ProfitOptics.Framework.Web.RestApiExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.Services.Customer
{
    public class CustomerService : ICustomerService
    {
        private readonly ApiEntitiesContext _context;

        public CustomerService(ApiEntitiesContext context, IProductService productService)
        {
            _context = context;
        }

        public async Task<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer>> ListAsync(CustomerQuery query)
        {
            IQueryable<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer> queryable = _context.Customers
                                                .Include(p => p.Distributor)
                                                .Include(p => p.CustomerProducts)
                                                .AsNoTracking();

            if (query.DistributorId.HasValue && query.DistributorId > 0)
            {
                queryable = queryable.Where(p => p.DistributorId == query.DistributorId);
            }

            if (query.ProductId.HasValue && query.ProductId > 0)
            {
                queryable = queryable.Where(p => p.CustomerProducts.Any(ap => ap.ProductId == query.ProductId));
            }

            int totalItems = await queryable.CountAsync();

            List<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer> customers = await queryable
                .ApplyFiltering(query)
                .ApplySort(query)
                .ApplyPagination(query)
                .ToListAsync();

            return new QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer>
            {
                Items = customers,
                TotalItems = totalItems,
            };
        }

        public async Task<List<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer>> ListAsyncByIds(List<int> ids)
        {
            IQueryable<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer> queryable = _context.Customers
                                                .Where(d => ids.Contains(d.Id))
                                                .AsNoTracking();

            List<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer> customers = await queryable.ToListAsync();

            return customers;
        }

        public async Task<CustomerResponse> SaveAsync(ProfitOptics.Framework.Web.RestApi.DataLayer.Customer customer)
        {
            try
            {
                var existingDistributor = await _context.Distributors.FindAsync(customer.DistributorId);
                if (existingDistributor == null)
                    return new CustomerResponse("Invalid distributor.");

                if (customer.CustomerProducts.Count > 0)
                {
                    var productIds = customer.CustomerProducts.Select(cp => cp.ProductId).Distinct().ToList();

                    var existingProducts = _context.Products
                                            .Where(p => productIds.Contains(p.Id))
                                            .AsNoTracking();

                    if (existingProducts == null)
                        return new CustomerResponse("Invalid products.");

                    if (existingProducts.Count() != productIds.Count())
                        return new CustomerResponse("Could not find all the products.");
                }

                await _context.Customers.AddAsync(customer);
                await _context.SaveChangesAsync();

                return new CustomerResponse(customer);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new CustomerResponse($"An error occurred when saving the customer: {ex.Message}");
            }
        }

        public async Task<CustomerResponse> UpdateAsync(int id, ProfitOptics.Framework.Web.RestApi.DataLayer.Customer customer)
        {
            var existingCustomer = await _context.Customers.FindAsync(id);

            if (existingCustomer == null)
                return new CustomerResponse("Customer not found.");

            existingCustomer.Name = customer.Name;
            existingCustomer.Address = customer.Address;

            try
            {
                var existingDistributor = await _context.Distributors.FindAsync(customer.DistributorId);
                if (existingDistributor == null)
                    return new CustomerResponse("Invalid distributor.");

                if (customer.CustomerProducts.Count > 0)
                {
                    var productIds = customer.CustomerProducts.Select(cp => cp.ProductId).Distinct().ToList();

                    var existingProducts = _context.Products
                                            .Where(p => productIds.Contains(p.Id))
                                            .AsNoTracking();

                    if (existingProducts == null)
                        return new CustomerResponse("Invalid products.");

                    if (existingProducts.Count() != productIds.Count())
                        return new CustomerResponse("Could not find all the products.");
                }

                _context.Customers.Update(existingCustomer);
                await _context.SaveChangesAsync();

                return new CustomerResponse(existingCustomer);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new CustomerResponse($"An error occurred when updating the customer: {ex.Message}");
            }
        }

        public async Task<CustomerResponse> DeleteAsync(int id)
        {
            var existingCustomer = await _context.Customers.FindAsync(id);

            if (existingCustomer == null)
                return new CustomerResponse("Customer not found.");

            try
            {
                _context.Customers.Remove(existingCustomer);
                await _context.SaveChangesAsync();

                return new CustomerResponse(existingCustomer);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new CustomerResponse($"An error occurred when deleting the customer: {ex.Message}");
            }
        }
    }
}