﻿using System;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class OperationStatus
    {
        public bool Status { get; set; }
        public int RecordsAffected { get; set; }
        public string Message { get; set; }
        public object ContextObject { get; set; }
        public Exception Exception { get; set; }
        public GroupByType Group { get; set; }

        public static OperationStatus CreateFromException(string message, Exception ex)
        {
            var opStatus = new OperationStatus
            {
                Status = false,
                Message = message,
                ContextObject = null,
                Exception = ex
            };

            return opStatus;
        }
    }
}
