use [Sox]

begin

  alter table [Sox].[dbo].[GCTray] drop constraint [FK__GCTray__CTCaseCo__691284DE];

  alter table [Sox].[dbo].[GCTray] drop column CTCaseContainerId;
  
  alter table [Sox].[dbo].[GCTray]
  add [CTContainerTypeId] int,
  foreign key ([CTContainerTypeId]) references [dbo].[CTContainerType] ([Id]);
  
  create table [dbo].[GCCasesGCTrays]
  (
	[Id] [int] primary key identity(1,1) not null,
	[GCCaseId] int,
    [GCTrayId] int,
	foreign key ([GCCaseId]) references [dbo].[GCCase] ([Id]),
	foreign key ([GCTrayId]) references [dbo].[GCTray] ([Id])
  )
  
  alter table [Sox].[dbo].[GCCasesLifecycleStatusTypes]
  add [IsActiveStatus] bit;

end