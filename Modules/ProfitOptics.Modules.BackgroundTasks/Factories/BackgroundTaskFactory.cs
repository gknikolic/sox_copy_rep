﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProfitOptics.Modules.BackgroundTasks.Models;
using ProfitOptics.Modules.BackgroundTasks.Services;

namespace ProfitOptics.Modules.BackgroundTasks.Factories
{
    public class BackgroundTaskFactory : IBackgroundTaskFactory
    {
        private readonly IBackgroundTaskService _backgroundTaskService;

        public BackgroundTaskFactory(IBackgroundTaskService backgroundTaskService)
        {
            _backgroundTaskService = backgroundTaskService;
        }

        /// <inheritdoc />
        public async Task<List<BackgroundTaskViewModel>> GetActiveBackgroundTasksViewModelsAsync(int userId)
        {
            List<BackgroundTaskViewModel> result = (await _backgroundTaskService.GetActiveTasksAsync(userId))
                .Select(task => new BackgroundTaskViewModel
                {
                    Progress = task.Progress,
                    Description = task.Description
                }).ToList();

            return result;
        }
    }
}