﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class EditInvoiceResponse : EditBase
    {
        public Invoice Invoice { get; set; }
    }
}
