﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models
{
	public class Proposal
	{
		[JsonProperty("order")]
		public Order Order { get; set; }
		[JsonProperty("timeSlots")]
		public List<TimeSlot> TimeSlots { get; set; }
		[JsonProperty("overrideLockedRoutes")]
		public bool OverrideLockedRoutes { get; set; }
	}
}
