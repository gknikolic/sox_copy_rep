﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Modules.Reporting.Factories;
using ProfitOptics.Modules.Reporting.Factories.CustomerReport;

namespace ProfitOptics.Modules.Reporting.Controllers
{
    [Authorize(Roles = "admin, reporting")]
    public class CustomerReportController : BaseReportingController
    {
        private readonly ICustomerReportFactory _customerReportFactory;
        private readonly IReportFilterModelFactory _reportFilterModelFactory;

        public CustomerReportController(ICustomerReportFactory customerReportFactory,
            IReportFilterModelFactory reportFilterModelFactory)
        {
            _customerReportFactory = customerReportFactory;
            _reportFilterModelFactory = reportFilterModelFactory;
        }

        public IActionResult Index()
        {
            var model = _customerReportFactory.GetCustomerReportViewModel(GetCurrentUserId());

            string controllerName = nameof(CustomerReportController).Replace("Controller", "");
            const string areaName = "Reporting";

            model.TopCustomers.ActionName = nameof(GetTopCustomersTableData);
            model.TopCustomers.ControllerName = controllerName;
            model.TopCustomers.AreaName = areaName;

            model.BottomCustomers.ActionName = nameof(GetBottomCustomersTableData);
            model.BottomCustomers.ControllerName = controllerName;
            model.BottomCustomers.AreaName = areaName;

            return View(model);
        }

        [HttpPost]
        public IActionResult GetTopCustomersTableData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request, bool isShipTo)
        {
            var filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(GetCurrentUserId());

            var data = _customerReportFactory.GetTopCustomersReportModels(request, filter, isShipTo);

            return LargeJsonResult(data.ToDataTablesResponse(request));
        }

        [HttpPost]
        public IActionResult GetBottomCustomersTableData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request, bool isShipTo)
        {
            var filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(GetCurrentUserId());

            var data = _customerReportFactory.GetBottomCustomersReportModels(request, filter, isShipTo);

            return LargeJsonResult(data.ToDataTablesResponse(request));
        }
    }
}