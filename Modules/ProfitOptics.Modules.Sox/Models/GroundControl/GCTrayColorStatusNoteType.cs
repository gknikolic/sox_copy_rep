﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.GroundControl
{
    public enum GCTrayColorStatusNoteType
    {
        SystemGenerated = 1,
        UserGenerated = 2
    }
}
