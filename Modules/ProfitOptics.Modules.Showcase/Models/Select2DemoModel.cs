﻿using ProfitOptics.Framework.Web.Framework.Models.Select2;

namespace ProfitOptics.Modules.Showcase.Models
{
    public class Select2DemoModel
    {
        [Select2Id]
        public string StringProperty1 { get; set; }

        public string StringProperty2 { get; set; }

        [Select2Text]
        public string StringProperty3 { get; set; }

        public string StringProperty4 { get; set; }
    }
}
