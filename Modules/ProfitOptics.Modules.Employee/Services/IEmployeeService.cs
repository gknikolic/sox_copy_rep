﻿
using ProfitOptics.Framework.DataLayer.Test;
using ProfitOptics.Modules.Employee.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Employee.Services
{
    public interface IEmployeeService
    {
        /// <summary>
        /// Retrieves a employee with id.
        /// </summary>
        /// <param name="id">Employee id</param>
        /// <returns>Employee</returns>
        Task<TEmployee> GetEmployeeByIdAsync(int id);

        /// <summary>
        /// Retrieves all managers
        /// </summary>
        /// <returns>List of managers</returns>
        Task<List<TManager>> GetAllManagersAsync();

        /// <summary>
        /// Retrieves all skills
        /// </summary>
        /// <returns>List of skills</returns>
        Task<List<TSkill>> GetAllSkillsAsync();

        /// <summary>
        /// Updates employee details
        /// </summary>
        /// <param name="model">Employee update model</param>
        /// <returns></returns>
        Task UpdateEmployeeAsync(EmployeeModel model);

        Task AddEmployeeAsync(EmployeeModel model);

        //TODO: Napravi AddEmployee kao transakciju

        Task DeleteEmployeeAsync(EmployeeModel model);

        Task<List<TEmployee>> GetAllEmployeesAsync();

        Task<bool> AddManagerAsync(ManagerModel model);

        //Task<bool> AddManagerAsync(string firstName, string lastName, string title);
    }
}