﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Framework.Models.Select2
{
    public class Select2PagedResult
    {
        public List<Select2Model> results { get; set; }
        public int count_filtered { get; set; }

        public Select2PagedResult()
        {
            results = new List<Select2Model>();
            count_filtered = 0;
        }

        public Select2PagedResult(List<Select2Model> data, int totalCount)
        {
            if (totalCount < 0)
            {
                throw new ArgumentException(@"Value must be greater or equal to zero", nameof(totalCount));
            }

            results = data;
            count_filtered = totalCount;
        }
    }
}
