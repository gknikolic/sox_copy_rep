﻿using System;
using System.Globalization;

namespace ProfitOptics.Modules.Showcase.Models
{
    public class DataTablesExampleModel
    {
        public int Id { get; set; }
        public string TextValue { get; set; }
        public DateTime? DateValue { get; set; }
        public string DateString => DateValue.HasValue ? DateValue.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) : "";
        public double? PercentValue { get; set; }
        public decimal? CurrencyValue { get; set; }
        public string Notes { get; set; }
    }
}