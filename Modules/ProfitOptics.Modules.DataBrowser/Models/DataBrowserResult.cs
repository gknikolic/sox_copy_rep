﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

namespace ProfitOptics.Modules.DataBrowser.Models
{
    public class DataBrowserResult
    {
        public DataBrowserResult()
        {
            Status = ResultStatus.OK.ToString();
            Message = string.Empty;
        }

        public string Status { get; set; }
        public string Message { get; set; }
        public DataBrowserPaginationResult Result { get; set; }
        public string WhereStatement { get; set; }
    }

    public enum ResultStatus
    {
        OK,
        ERROR
    }
}