﻿
function AddCustomerDialog() {

    $.ajax({
        type: "GET",
        url: "AddCustomerDialog",
        contentType: 'application/json',
        dataType: "html",
        success: function (response) {
            $("#AddCustomerDialogHolder").html(response);
            $("#AddCustomerDialog").modal({ show: true });

        },
        error: function (response) {
            var error = 400;
        }
    });
}