﻿using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Infrastructure.Clients
{
	public interface IBoxstormClient
	{
		public Task<bool> AuthAsync();
	}
}
