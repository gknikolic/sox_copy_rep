﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.Test
{
    public class TEmployee
    {
        public TEmployee()
        {
            EmployeeSkills = new HashSet<TEmployeeSkills>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ManagerId { get; set; }

        public virtual TManager Manager { get; set; }
        public virtual ICollection<TEmployeeSkills> EmployeeSkills { get; set; }

    }
}
