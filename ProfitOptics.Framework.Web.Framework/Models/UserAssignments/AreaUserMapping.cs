﻿namespace ProfitOptics.Framework.Web.Framework.Models.UserAssignments
{
    public class AreaUserMapping
    {
        public int UserId { get; set; }
        public int? ZoneId { get; set; }
        public int? RegionId { get; set; }
        public int? SalesRepId { get; set; }
    }
}
