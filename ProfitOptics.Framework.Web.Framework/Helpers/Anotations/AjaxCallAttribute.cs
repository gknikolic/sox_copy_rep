﻿using System;

namespace ProfitOptics.Framework.Web.Framework.Helpers.Anotations
{
    public class AjaxCallAttribute : Attribute
    {
        public string Method { get; set; }
        public string DataType { get; set; }

        public AjaxCallAttribute()
        {
            Method = AjaxCallMethod.Post;
            DataType = AjaxCallDataType.Json;
        }

        public AjaxCallAttribute(string method, string dataType)
        {
            Method = method;
            DataType = dataType;
        }
    }

    public class AjaxCallMethod : Attribute
    {
        public const string Post = "post";
        public const string Get = "get";
    }

    public class AjaxCallDataType : Attribute
    {
        public const string Json = "json";
        public const string Html = "html";
    }
}
