﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class CTCaseHistoricalConfig : IEntityTypeConfiguration<CTCaseHistorical>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTCaseHistorical> builder)
        {
            builder.ToTable("CTCaseHistorical");
            builder.HasKey(entity => entity.Id);
        }
    }
}
