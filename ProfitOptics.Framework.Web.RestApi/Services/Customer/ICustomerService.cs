﻿using ProfitOptics.Framework.Web.RestApi.Models;
using ProfitOptics.Framework.Web.RestApi.Models.Customer;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.Services.Customer
{
    public interface ICustomerService
    {
        Task<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer>> ListAsync(CustomerQuery query);
        Task<List<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer>> ListAsyncByIds(List<int> ids);
        Task<CustomerResponse> SaveAsync(ProfitOptics.Framework.Web.RestApi.DataLayer.Customer customer);
        Task<CustomerResponse> UpdateAsync(int id, ProfitOptics.Framework.Web.RestApi.DataLayer.Customer customer);
        Task<CustomerResponse> DeleteAsync(int id);
    }
}
