﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCProcedureTypeConfig : IEntityTypeConfiguration<GCProcedureType>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCProcedureType> builder)
        {
            builder.ToTable("GCProcedureType");
            builder.HasKey(entity => entity.Id);
        }
    }
}
