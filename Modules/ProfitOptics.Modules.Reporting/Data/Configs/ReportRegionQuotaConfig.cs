﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportRegionQuotaConfig : IEntityTypeConfiguration<ReportRegionQuota>
    {
        public void Configure(EntityTypeBuilder<ReportRegionQuota> builder)
        {
            builder.ToTable(nameof(ReportRegionQuota));
            builder.HasKey(q => q.Id);

            builder.Property(q => q.Month).IsRequired();
            builder.Property(q => q.Year).IsRequired();
            builder.Property(q => q.Revenue).HasColumnType("decimal(19,4)").IsRequired();

            builder.HasOne(q => q.ReportHierarchyRegion)
                .WithMany()
                .HasForeignKey(q => q.RegionId)
                .IsRequired();

            builder.HasOne(q => q.ReportHierarchySalesRep)
                .WithMany()
                .HasForeignKey(q => q.SalesRepId)
                .IsRequired();
        }
    }
}