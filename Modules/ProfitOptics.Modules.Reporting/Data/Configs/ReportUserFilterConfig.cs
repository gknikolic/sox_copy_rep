﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportUserFilterConfig : IEntityTypeConfiguration<ReportUserFilter>
    {
        public void Configure(EntityTypeBuilder<ReportUserFilter> builder)
        {
            builder.ToTable(nameof(ReportUserFilter));
            builder.HasKey(f => f.Id);

            builder.Property(f => f.Name).HasMaxLength(50).IsRequired();
            builder.Property(f => f.IsDefault).IsRequired().HasDefaultValue(false);
            builder.Property(f => f.Value).IsRequired();
            builder.Property(f => f.StartDate).IsRequired();
        }
    }
}
