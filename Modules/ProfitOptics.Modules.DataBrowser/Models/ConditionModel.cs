﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.ComponentModel.DataAnnotations;
using ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Enums;

namespace ProfitOptics.Modules.DataBrowser.Models
{
    public class ConditionModel
    {
        public string ColumnName { get; set; }
        public Comparison? ComparisonOperator { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Value { get; set; }

        public LogicOperator? LogicOperator { get; set; }
    }
}