﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCSystemAlertInfoModel
    {
        public int SystemAlertCount { get; set; }
    }
}
