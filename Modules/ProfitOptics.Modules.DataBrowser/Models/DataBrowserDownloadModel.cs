﻿namespace ProfitOptics.Modules.DataBrowser.Models
{
    public class DataBrowserDownloadModel
    {
        public string Id { get; set; }
        public bool DownloadWarning { get; set; }
        public bool TooManyRecords { get; set; }
        public string Count { get; set; }
    }
}
