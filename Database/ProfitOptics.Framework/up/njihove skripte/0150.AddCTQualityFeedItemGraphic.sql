USE [Sox]

CREATE TABLE [dbo].[CTQualityFeedItemGraphic]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[CTQualityFeedItemId] INT NOT NULL,
	[GCImageId] INT NULL,
	[Description] NVARCHAR(200) NULL,
	[ReferenceId] BIGINT NOT NULL,
	[GraphicId] BIGINT NOT NULL,
	[IsGlobal] BIT NOT NULL,
	[LoadId] BIGINT NOT NULL,
	[UpdateTimestamp] [DATETIME] NULL,
	[UpdateUserId] BIGINT NOT NULL,
	[CreatedAtUtc] [DATETIME] NOT NULL,
	[ModifiedAtUtc] [DATETIME] NULL
CONSTRAINT [PK_CTQualityFeedItemGraphic] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTQualityFeedItemGraphic]  WITH CHECK ADD  CONSTRAINT [FK_CTQualityFeedItemGraphic_CTQualityFeedItem] FOREIGN KEY([CTQualityFeedItemId])
REFERENCES [dbo].[CTQualityFeedItem] ([Id])
GO
ALTER TABLE [dbo].[CTQualityFeedItemGraphic] CHECK CONSTRAINT [FK_CTQualityFeedItemGraphic_CTQualityFeedItem]
GO

ALTER TABLE [dbo].[CTQualityFeedItemGraphic]  WITH CHECK ADD  CONSTRAINT [FK_CTQualityFeedItemGraphic_GCImage] FOREIGN KEY([GCImageId])
REFERENCES [dbo].[GCImage] ([Id])
GO
ALTER TABLE [dbo].[CTQualityFeedItemGraphic] CHECK CONSTRAINT [FK_CTQualityFeedItemGraphic_GCImage]
GO