﻿var enableDialog = true;
var src = "";
//function ShowEventDetails(date, btnColor, el) {
//    if (enableEventsDialog == false) {
//        return;
//    }
//    enableEventsDialog = false;
//    $.ajax({
//        url: $("#GetEventDetailsUrl").val(),
//        type: "json",
//        method: "POST",
//        data: {
//            date: date,
//            btnColor: btnColor
//        },
//        success: function (result) {
//            $("#eventDetailsDialogHolder").html(result);
//            $("#eventDetailsDialog").modal({ show: true });
//            enableEventsDialog = true;
//        },
//        error: function (response) {
//            var error = 400;
//            enableEventsDialog = true;
//        }
//    });
//}

function AddCustomerDialog() {
    if (enableDialog == false) {
        return;
    }
    enableDialog = false;

    $.ajax({
        type: "GET",
        url: "AddCustomerDialog",
        contentType: 'application/json',
        dataType: "html",
        success: function (response) {
            $("#AddCustomerDialogHolder").html(response);
            $("#AddCustomerDialog").modal({ show: true });
            $('.timepickerInput').timepicker();
            enableDialog = true;
        },
        error: function (response) {
            var error = 400;
            enableDialog = true;
        }
    });
}

//function ShowEditCustomerModal(modalName) {
//    $(modalName).find('.updateTimepickerInput').each(function (i, e) {
//        var timeToSet = new Date($(e).val()).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
//        if (timeToSet !== "Invalid Date") {
//            $(e).timepicker('setTime', timeToSet);
//        }
//    });
//    $(modalName).modal('show');
//}

function AddVendorDialog() {
    if (enableDialog == false) {
        return;
    }
    enableDialog = false;

    $.ajax({
        type: "GET",
        url: "AddVendorDialog",
        contentType: 'application/json',
        dataType: "html",
        success: function (response) {
            $("#AddVendorDialogHolder").html(response);
            $("#AddVendorDialog").modal({ show: true });
            enableDialog = true;
        },
        error: function (response) {
            var error = 400;
            enableDialog = true;
        }
    });
}

function DeactivateCustomerDialog(customerId) {
    if (enableDialog == false) {
        return;
    }
    enableDialog = false;

    $.ajax({
        type: "GET",
        url: "DeactivateCustomerDialog",
        contentType: 'application/json',
        data: { customerId: customerId },
        dataType: "html",
        success: function (response) {
            $("#DeactivateCustomerDialogHolder").html(response);
            $("#DeactivateCustomerDialog").modal({ show: true });
            enableDialog = true;
        },
        error: function (response) {
            var error = 400;
            enableDialog = true;
        }
    });
}



function SubmitCustomerToQB(customerId) {
    $.blockUI();
    $.ajax({
        type: "POST",
        url: "SubmitCustomerToQB",
        data: { customerId: customerId },
        dataType: "json",
        success: function (response) {
            $.unblockUI();
            if (response.success) {
                app.showAlert(response.message);
                setTimeout(function () {
                    location.reload();
                }, 2500);
            } else {
                app.showAlert(response.message);
            }
        },
        error: function (response) {
            var error = 400;
            $.unblockUI();
        }
    });
}

function UpdateCustomerFromQB(customerId) {
    $.blockUI();
    $.ajax({
        type: "POST",
        url: "UpdateCustomerFromQB",
        data: { customerId: customerId },
        dataType: "json",
        success: function (response) {
            $.unblockUI();
            if (response.success) {
                app.showAlert(response.message);
                setTimeout(function () {
                    location.reload();
                }, 2500);
            } else {
                app.showAlert(response.message);
            }
        },
        error: function (response) {
            var error = 400;
        }
    });
}

function UpdateCustomerToQB(customerId) {
    $.blockUI();
    $.ajax({
        type: "POST",
        url: "UpdateCustomerToQB",
        data: { customerId: customerId },
        dataType: "json",
        success: function (response) {
            $.unblockUI();
            app.showAlert(response.message);
        },
        error: function (response) {
            var error = 400;
            $.unblockUI();
        }
    });
}

function PushTraysOrCasesToQB(customerId, isTraysPush) {
    $.ajax({
        type: "POST",
        url: "PushTraysOrCasesToQB",
        data: { customerId: customerId, isTraysPush: isTraysPush },
        dataType: "json",
        success: function (response) {
            alert(response.message);
        },
        error: function (response) {
            var error = 400;
        }
    });
}



function DeactivateVendorDialog(vendorId) {
    if (enableDialog == false) {
        return;
    }
    enableDialog = false;

    $.ajax({
        type: "GET",
        url: "DeactivateVendorDialog",
        contentType: 'application/json',
        data: { vendorId: vendorId },
        dataType: "html",
        success: function (response) {
            $("#DeactivateVendorDialogHolder").html(response);
            $("#DeactivateVendorDialog").modal({ show: true });
            enableDialog = true;
        },
        error: function (response) {
            var error = 400;
            enableDialog = true;
        }
    });
}

function SubmitVendorToQB(vendorId) {

    $.ajax({
        type: "POST",
        url: "SubmitVendorToQB",
        data: { vendorId: vendorId },
        dataType: "json",
        success: function (response) {
            if (response.success) {
                location.reload();
            } else {
                alert(response.message);
            }
        },
        error: function (response) {
            var error = 400;
        }
    });
}

function UpdateVendorFromQB(vendorId) {

    $.ajax({
        type: "POST",
        url: "UpdateVendorFromQB",
        data: { vendorId: vendorId },
        dataType: "json",
        success: function (response) {
            if (response.success) {
                location.reload();
            } else {
                alert(response.message);
            }
        },
        error: function (response) {
            var error = 400;
        }
    });
}

function UpdateVendorToQB(vendorId) {

    $.ajax({
        type: "POST",
        url: "UpdateVendorToQB",
        data: { vendorId: vendorId },
        dataType: "json",
        success: function (response) {
            alert(response.message);
        },
        error: function (response) {
            var error = 400;
        }
    });
}

function SubmitEstimateToQB(gcCaseId) {

    $.ajax({
        type: "POST",
        url: "../Sox/Sox/SubmitEstimateToQB",
        data: { gcCaseId: gcCaseId },
        dataType: "json",
        success: function (response) {
            if (response.success) {
                location.reload();
            } else {
                alert(response.message);
            }
        },
        error: function (response) {
            var error = 400;
        }
    });
}


//$('.fc-next-button').click(function () {
//    var date = $('.fc-center')[0].innerText;
//    var p = 10;
//});

//$(document).ready(function () {
//    $("body").on("click", ".fc-next-button", function () {
//        var date = $('.fc-center')[0].innerText;
//        var p = 10;
//    });
//})

//$(document).on("click", ".fc-next-button", function () {
//    var date = $('.fc-center')[0].innerText;

//    $.ajax({
//        type: "GET",
//        url: "GetGCCasesByDate",
//        contentType: 'application/json',
//        data: { date: date },
//        success: function (response) {
//            var val = 1;

//        },
//        error: function (response) {
//            var error = 400;
//        }
//    });

//});

$(document).ready(function () {

    //$('#gcCasesSelect').on('change', function () {
    //    var gcCaseId = $(this).find(":selected").val();

    //    $.ajax({
    //        type: "GET",
    //        url: "GetLifecycleStatusForCase",
    //        contentType: 'application/json',
    //        data: { gcCaseId: gcCaseId },
    //        success: function (response) {
    //            $("#gcLifecycleStatusesSelect").val(response.status.Id).change();

    //        },
    //        error: function (response) {
    //            var error = 400;
    //        }
    //    });
    //});
    $('.datepicker').datepicker();
    $('.dataTable').DataTable();
    $("#datepicker").datepicker().on('changeDate', function (ev) {
        var date = $(this).datepicker('getDate');
        date.setHours(12);
        app.blockUI('Please wait...');

        $.ajax({
            type: "GET",
            url: "GetGCCasesByDate",
            contentType: 'application/json',
            data: { date: date.toUTCString() },
            success: function (response) {
                $("#casesList").html(response);
            },
            error: function (response) {
                var error = 400;
            },
            complete: function () {
                $('#next').prop('disabled', false);
                $('#today').prop('disabled', false);
                $('#prev').prop('disabled', false);
                $.unblockUI();
            }
        });

    })

    $("#applyFilter").click(function () {
        FilterCasesByDate();
    })

    function FilterCasesByDate() {
        var date = $("#datepicker").datepicker('getDate');
        var caseRef = $('#caseReference').val();
        var vehicleId = $('#vehicleList').val();
        var driverId = $('#driverList').val();
        app.blockUI('Please wait...');

        $.ajax({
            type: "GET",
            url: "FilterGCCasesByDate",
            contentType: 'application/json',
            data: { date: date.toUTCString(), caseReference: caseRef, vehicleId: vehicleId, driverId: driverId },
            success: function (response) {
                $("#casesList").html(response);
                app.unblockUI();
            },
            error: function (response) {
                var error = 400;
                app.unblockUI();
            }
            //complete: function () {
            //    $('#next').prop('disabled', false);
            //    $('#today').prop('disabled', false);
            //    $('#prev').prop('disabled', false);
            //}
        });
    }

    $("#prev").click(function () {
        var date = $('#datepicker').datepicker('getDate', '-1d');
        $('#next').prop('disabled', true);
        $('#today').prop('disabled', true);
        $('#prev').prop('disabled', true);

        date.setDate(date.getDate() - 1);
        $('#datepicker').datepicker('setDate', date);

        //$.ajax({
        //    type: "GET",
        //    url: "GetGCCasesByDate",
        //    contentType: 'application/json',
        //    data: { date: date.toUTCString() },
        //    success: function (response) {
        //        $("#casesList").html(response);

        //    },
        //    error: function (response) {
        //        var error = 400;
        //    }
        //});
    })


    $("#next").click(function () {

        var date = $('#datepicker').datepicker('getDate', '+1d');

        $('#next').prop('disabled', true);
        $('#today').prop('disabled', true);
        $('#prev').prop('disabled', true);

        date.setDate(date.getDate() + 1);
        $('#datepicker').datepicker('setDate', date);

        //$.ajax({
        //type: "GET",
        //url: "GetGCCasesByDate",
        //contentType: 'application/json',
        //    data: { date: date.toUTCString() },
        //success: function (response) {
        //    $("#casesList").html(response);

        //},
        //error: function (response) {
        //    var error = 400;
        //}
        //});
    })

    $('#today').click(function () {
        var date = new Date($.now());

        $('#next').prop('disabled', true);
        $('#today').prop('disabled', true);
        $('#prev').prop('disabled', true);

        $('#datepicker').datepicker('setDate', date);

        //$.ajax({
        //    type: "GET",
        //    url: "GetGCCasesByDate",
        //    contentType: 'application/json',
        //    data: { date: date.toUTCString() },
        //    success: function (response) {
        //        $("#casesList").html(response);

        //    },
        //    error: function (response) {
        //        var error = 400;
        //    }
        //});
    })

    $('#syncRoutesWithWorkWave').click(function () {
        var date = $("#datepicker").datepicker('getDate');
        app.blockUI('Please wait...');
        $.ajax({
            type: "GET",
            url: "SyncWWRoutesAsync",
            contentType: 'application/json',
            data: { date: date.toUTCString() },
            success: function (response) {
                $("#casesList").html(response);
                app.unblockUI();

            },
            error: function (response) {
                var error = 400;
                app.showAlertMessage("Error", "There was an error processing request.", "error");
                app.unblockUI();
            }
        });
    })

    $('#syncOrdersWithWorkWave').click(function () {
        var data = getFilterData();
        app.blockUI('Please wait...');
        $.ajax({
            type: "POST",
            url: "Sox/Sox/SyncWWOrdersAsync",
            contentType: 'application/json',
            data: { filterModel: JSON.stringify(data) },
            success: function (response) {
                $("#messageDiv").html(response);
                app.unblockUI();
                $("#ShowResultModal").modal({ show: true });
                //location.reload();
            },
            error: function (response) {
                var error = 400;
                app.showAlertMessage("Error", "There was an error processing request.", "error");
                app.unblockUI();
            }
        });
    });

    $('.resolve-note').click(function () {
        noteId = parseInt($(this).data('noteid'));
        $.ajax({
            type: "POST",
            url: "../Sox/Sox/ResolveGCTrayColorStatusNote",
            dataType: "json",
            data: { noteId: noteId },
            success: function (response) {
                if (response.success) {
                    $('a.resolve-note[data-noteid="' + noteId + '"]').parent().remove();
                }
            },
            error: function (response) {
                var error = 400;
            }
        });
    });

    $('.unlock-color-status').click(function () {
        statusId = parseInt($(this).data('statusid'));
        $.ajax({
            type: "POST",
            url: "../Sox/Sox/UnlockGCTrayColorStatus",
            dataType: "json",
            data: { statusId: statusId },
            success: function (response) {
                if (response.success) {
                    $('a.unlock-color-status[data-statusid="' + statusId + '"]').parent().remove();
                }
            },
            error: function (response) {
                var error = 400;
            }
        });
    });

    function getFilterData() {
        var filterModel = {};
        filterModel.CaseReference = $("#CaseReference").val();
        filterModel.Surgeon = $("#Surgeon").val();
        filterModel.SelectedCustomer = $("#customerList option:selected").text();
        //filterModel.SelectedVendor = $("#customerList option:selected").text();
        return filterModel;
    }

    $('#today').trigger('click');
})



function openRightSidebar(id) {
    var url = $("#GetRightSidebarDataUrl").val();
    app.blockUI();
    $.ajax({
        type: "GET",
        url: url,
        data: { trayId: id },
        contentType: 'application/json',
        dataType: "html",
        success: function (response) {
            app.unblockUI();
            $("#sidebar-placeholder").html(response);
            $(".control-sidebar").addClass('control-sidebar-open');

        },
        error: function (response) {
            var error = 400;
            app.unblockUI();
            app.showAlertMessage("Error", "There was an error processing this file.", "error");
        }
    });
}

function getProposedHistoryItemsForTrayLC(trayId, lifecycleId, trayLifecycleId) {
    var url = $("#GetProposedHistoryItemsForTrayLC").val();
    app.blockUI();
    $.ajax({
        type: "GET",
        url: url,
        data: { trayId: trayId, lifecycleId: lifecycleId, trayLifecycleId: trayLifecycleId },
        contentType: 'application/json',
        dataType: "html",
        success: function (response) {
            app.unblockUI();
            $("#proposed-history-placeholder").html(response);
            $('#proposedTrayLCHistoryDialog').show();
            registerSelectHI();
        },
        error: function (response) {
            var error = 400;
            app.unblockUI();
            app.showAlertMessage("Error", "There was an error processing this file.", "error");
        }
    });
}

function updateGCTrayLifecycleClose() {
    $('#proposedTrayLCHistoryDialog').hide();
}

function updateGCTrayLifecycle() {
    var data = {}
    data.GCTrayId = parseInt($('#frmProposedTrayLCHistory').find('#GCTrayId').val());
    data.GCLifeCycleId = parseInt($('#frmProposedTrayLCHistory').find('#GCLifeCycleId').val());
    data.GCTrayLifeCycleId = parseInt($('#frmProposedTrayLCHistory').find('#GCTrayLifeCycleId').val());
    data.SelectedHI = parseInt($('#frmProposedTrayLCHistory').find('[name=SelectedHI]').find(":selected").val());

    var url = $("#UpdateProposedHistoryItemForLcUrl").val();
    app.blockUI('Please wait...');
    $.ajax({
        type: "POST",
        url: url,
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function (response) {
            app.showAlert("Success!");
            app.unblockUI();
            location.reload();
        },
        error: function (response) {
            var error = 400;
            app.showAlertMessage("Error", "There was an error processing request.", "error");
            app.unblockUI();
        }
    });
}

function showDeleteGCTrayLifecycleDialog(id) {
    $('#LifeCyceleToRemoveTray').val(id);
    $("#DeleteGCTrayLifecycleDialog").modal({ show: true });
}

function removeLifecycleFromTray() {
    var id = $('#LifeCyceleToRemoveTray').val();
    var url = $("#RemoveLifecycleFromTray").val();
    app.blockUI('Please wait...');
    $.ajax({
        type: "POST",
        url: url,
        data: { id: id },
        dataType: 'json',
        success: function (response) {
            app.showAlert("Success!");
            app.unblockUI();
            location.reload();
        },
        error: function (response) {
            var error = 400;
            app.showAlertMessage("Error", "There was an error processing request.", "error");
            app.unblockUI();
        }
    });
}

function registerSelectHI() {
    $('#SelectedHI').change(function () {
        var selectedVal = $(this).find(":selected").val();
        isDisabled = selectedVal === '0';
        $('#btnUpdateGCTrayLifecycle').prop('disabled', isDisabled);
    });
}

function openVendorsTrayHistory(id) {
    var accordion = "#collapsInfo-" + id;
    var placeholder = "#collapsInfoPlaceholder-" + id;
    if ($(accordion).css('display') === 'block') {
        $(accordion).toggle();
        $(placeholder).html("");
        return;
    }
    var url = $("#GetVendorsTrayHistoryDataUrl").val();
    app.blockUI();
    $.ajax({
        type: "GET",
        url: url,
        data: { trayId: id },
        contentType: 'application/json',
        dataType: "html",
        success: function (response) {
            $(placeholder).html(response);
            //var carouselInner = "#carousel-inner-" + id;
            //$(carouselInner).find('.item').first().addClass('active');
            $(accordion).toggle();

            app.unblockUI();
        },
        error: function (response) {
            var error = 400;
            app.unblockUI();
            app.showAlertMessage("Error", "There was an error processing this file.", "error");
        }
    });
}

function HideSideBar() {
    $(".control-sidebar").removeClass('control-sidebar-open');
}

function syncCaseHistory(id) {
    var url = $("#SyncCaseHistoryUrl").val();
    app.blockUI();
    $.ajax({
        type: "POST",
        url: url,
        data: { gcCaseId: id },
        dataType: "json",
        success: function (response) {
            app.unblockUI();
            location.reload();
        },
        error: function (response) {
            var error = 400;
            app.unblockUI();
            app.showAlertMessage("Error", "There was an error.", "error");
        }
    });
}

//$(".fc-icon fc-icon-chevron-right").on("click", function () {
//    var date = $('.fc-center')[0].innerText;
//    var p = 10;
//});

//var BtnColor = $('#BtnColor').val();

//if (BtnColor == "blue") {
//    document.getElementById("blue").style.display = "block";
//}


//function Base64ToArrayBuffer(b64Data) {
//    const byteCharacters = atob(b64Data);

//    const byteNumbers = new Array(byteCharacters.length);
//    for (let i = 0; i < byteCharacters.length; i++) {
//        byteNumbers[i] = byteCharacters.charCodeAt(i);
//    }

//    const byteArray = new Uint8Array(byteNumbers);

//    const blob = new Blob([byteArray], { type: contentType });

//    return blob;
//}


//$("#timepicker0").change(function () {
//    var start = $("#timepicker0").val();
//    var end = $("#timepicker1").val();

//    if (start < end)
//        $('#addCustomerBtn').prop('disabled', false);
//    else
//        $('#addCustomerBtn').prop('disabled', true);
//});

//$("#timepicker1").change(function () {
//    var start = $("#timepicker0").val();
//    var end = $("#timepicker1").val();

//    if (start < end)
//        $('#addCustomerBtn').prop('disabled', false);
//    else
//        $('#addCustomerBtn').prop('disabled', true);
//});

$(document).on('change', '#timepicker0', function () {
    var start = $("#timepicker0").val();
    var end = $("#timepicker1").val();

    var defaultDate = '1/1/0001';
    var startDateTime = new Date(defaultDate + ' ' + start);
    var endDateTime = new Date(defaultDate + ' ' + end);

    //var startTime = new Date(startDateTime).getTime();
    //var endTime = new Date(endDateTime).getTime();

    if (startDateTime < endDateTime) {
        $('#addCustomerBtn').prop('disabled', false);
        document.getElementById('deliveryTimeWarning').innerHTML = "";
    }
    else {
        $('#addCustomerBtn').prop('disabled', true);
        document.getElementById('deliveryTimeWarning').innerHTML = "'Delivery Start' must be lower than 'Delivery End'."
        document.getElementById('deliveryTimeWarning').style.color = "#a94442";
    }
});

$(document).on('change', '#timepicker1', function () {
    var start = $("#timepicker0").val();
    var end = $("#timepicker1").val();

    var defaultDate = '1/1/0001';
    var startDateTime = new Date(defaultDate + ' ' + start);
    var endDateTime = new Date(defaultDate + ' ' + end);

    //var startTime = new Date(startDateTime).getTime();
    //var endTime = new Date(endDateTime).getTime();

    if (startDateTime < endDateTime) {
        $('#addCustomerBtn').prop('disabled', false);
        document.getElementById('deliveryTimeWarning').innerHTML = "";
    }
    else {
        $('#addCustomerBtn').prop('disabled', true);
        document.getElementById('deliveryTimeWarning').innerHTML = "'Delivery Start' must be lower than 'Delivery End'."
        document.getElementById('deliveryTimeWarning').style.color = "#a94442";
    }
});

$(document).on('change', '#timepicker2', function () {
    var start = $("#timepicker2").val();
    var end = $("#timepicker3").val();

    var defaultDate = '1/1/0001';
    var startDateTime = new Date(defaultDate + ' ' + start);
    var endDateTime = new Date(defaultDate + ' ' + end);

    if (startDateTime < endDateTime) {
        $('#addCustomerBtn').prop('disabled', false);
        document.getElementById('deliveryTimeWarning').innerHTML = "";
    }
    else {
        $('#addCustomerBtn').prop('disabled', true);
        document.getElementById('deliveryTimeWarning').innerHTML = "'Delivery Start' must be lower than 'Delivery End'."
        document.getElementById('deliveryTimeWarning').style.color = "#a94442";
    }
});

$(document).on('change', '#timepicker3', function () {
    var start = $("#timepicker2").val();
    var end = $("#timepicker3").val();

    var defaultDate = '1/1/0001';
    var startDateTime = new Date(defaultDate + ' ' + start);
    var endDateTime = new Date(defaultDate + ' ' + end);

    if (startDateTime < endDateTime) {
        $('#addCustomerBtn').prop('disabled', false);
        document.getElementById('deliveryTimeWarning').innerHTML = "";
    }
    else {
        $('#addCustomerBtn').prop('disabled', true);
        document.getElementById('deliveryTimeWarning').innerHTML = "'Delivery Start' must be lower than 'Delivery End'."
        document.getElementById('deliveryTimeWarning').style.color = "#a94442";
    }
});



//$('#timepicker').timepicker().on('changeTime.timepicker', function (e) {
//    console.log('The time is ' + e.time.value);
//    console.log('The hour is ' + e.time.hours);
//    console.log('The minute is ' + e.time.minutes);
//    console.log('The meridian is ' + e.time.meridian);
//});


var enableEventsDialog = true;
function ShowEventDetails(date, btnColor, el) {
    if (enableEventsDialog == false) {
        return;
    }
    enableEventsDialog = false;
    $.ajax({
        url: $("#GetEventDetailsUrl").val(),
        type: "json",
        method: "POST",
        data: {
            date: date,
            btnColor: btnColor
        },
        success: function (result) {
            $("#eventDetailsDialogHolder").html(result);
            $("#eventDetailsDialog").modal({ show: true });
            enableEventsDialog = true;
        },
        error: function (response) {
            var error = 400;
            enableEventsDialog = true;
        }
    });
}


function GoToIsFoamedPage() {

    var trayId = $(".active").find('input').val();

    $.ajax({
        type: "GET",
        url: "RedirectToIsTrayInPreppedStatus",
        contentType: 'application/json',
        data: { trayId: trayId },
        success: function (response) {
            window.location.href = response;
        },
        error: function (response) {
            var error = 400;
        }
    });

}

function SetInventoryAlertStatus() {
    $('#numberLabelInventory').hide();
    $.ajax({
        type: "POST",
        url: "GetInventoryAlertStatus",
        contentType: 'application/json',
        success: function (response) {
            $('#numberLabelInventory').hide();
            var alertCount = 0;
            if (response.QuarantinedTrayCount > 0) {
                alertCount++;
            }
            if (response.IncompleteCountSheetCount > 0) {
                alertCount++;
            }
            if (alertCount > 0) {
                $('#numberLabelInventory').html(alertCount);
                $('#numberLabelInventory').show();
            }
        },
        error: function (response) {
            $('#numberLabelInventory').hide();
            var error = 400;
        }
    });
}

function MarkAlertRead(id) {
    $.blockUI();
    $.ajax({
        type: "POST",
        url: $('#MarkAlertReadUrl').val(),
        data: { id: id },
        dataType: "json",
        success: function (response) {
            $.unblockUI();
            if (response.success) {
                app.showAlert(response.message);
                setTimeout(function () {
                    location.reload();
                }, 2500);
            } else {
                app.showAlert(response.message);
            }
        },
        error: function (response) {
            var error = 400;
            $.unblockUI();
        }
    });
}

function SetSystemAlertStatus() {
    $('#numberLabelInventory').hide();
    $.ajax({
        type: "POST",
        url: "GetSystemAlertsStatus",
        contentType: 'application/json',
        success: function (response) {
            $('#numberLabelTrayAlers').hide();
            if (response.SystemAlertCount > 0) {
                $('#numberLabelTrayAlers').html(response.SystemAlertCount);
                $('#numberLabelTrayAlers').show();
            }
        },
        error: function (response) {
            $('#numberLabelTrayAlers').hide();
            var error = 400;
        }
    });
}

//function GetGCImageById(imageId) {

//    $.ajax({
//        type: "GET",
//        url: "GetGCImageById",
//        contentType: 'application/json',
//        data: { imageId: imageId },
//        success: function (response) {
//            src = response;
//            return src;
//        },
//        error: function (response) {
//            var error = 400;
//        }
//    });
//}

function enlargeImg(img) {
    // Get the modal
    $.blockUI();
    var modal = document.getElementById("imgModal");

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    //var img = document.getElementById(imgId);

    var modalImg = document.getElementById("imgPlaceholder");
    modalImg.src = "";
    var captionText = document.getElementById("caption");
    //      img.onclick = function () {
    //          modal.style.display = "block";
    //          modalImg.src = this.src;
    //          captionText.innerHTML = this.alt;
    //}

    //var imageSrc = GetGCImageById(img.id)
    if (img.id != "") {
        $.ajax({
            type: "GET",
            url: $("#GetGCImageByIdUrl").val(),
            contentType: 'application/json',
            data: { imageId: img.id },
            success: function (response) {
                modalImg.src = response;
                $.unblockUI();
            },
            error: function (response) {
                var error = 400;
                $.unblockUI();

            }
        });
    }
    else {
        modalImg.src = img.src;
        $.unblockUI();
    }


    modal.style.display = "block";
    //modalImg.src = img.src;
    captionText.innerHTML = img.alt;

    // Get the <span> element that closes the modal
    //var span = document.getElementsByClassName("close")[0];

    var span = document.getElementById("closeBtn");
    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    modal.onclick = function () {
        modal.style.display = "none";
    }
}

function EnlargeImageFromSource(element) {
    var modalImg = document.getElementById("enlargedImageModalImg");
    modalImg.src = element.src;
    $("#enlargedImageModa").modal({ show: true });
}

function enlargeImgWithCarousel(imgId, isAssembly, statusId) {
    // Get the modal
    $.blockUI();
    if (imgId != "") {
        var isAssemblyBool = "false"
        if (isAssembly === 1) {
            isAssemblyBool = "true"
        }

        $.ajax({
            type: "GET",
            url: $("#GetGCImageWithCountSheetByIdUrl").val(),
            contentType: 'application/json',
            data: { imageId: imgId, isAssembly: isAssemblyBool, statusId: statusId },
            success: function (response) {
                $("#photoWithCountSheetModalPlaceholder").html(response)
                $("#photoWithCountSheetModal").modal({ show: true });
                $.unblockUI();
            },
            error: function (response) {
                var error = 400;
                $.unblockUI();

            }
        });
    }
    else {
        $.unblockUI();
    }
}

function ShowMapWithMarkers(mapPlaceholder)  {
    $.ajax({
        type: "GET",
        url: $("#GetGoogleMapInfoUrl").val(),
        contentType: 'application/json',
        data: {},
        success: function (response) {


            var chicagoCenter = new google.maps.LatLng(41.881832, -87.623177);
            var mapOptions = {
                center: chicagoCenter,
                zoom: 15,
                minZoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById(mapPlaceholder), mapOptions);
            var markerForCenter = {};
            for (let i = 0; i < response.length; ++i) {
                var mapMarkerLatLng = new google.maps.LatLng(response[i].Lat, response[i].Long);
                var marker = new google.maps.Marker({ position: mapMarkerLatLng, label: response[i].Title, animation: google.maps.Animation.DROP });
                marker.setMap(map);
                markerForCenter = marker;
            }
            map.setCenter(marker.getPosition());
        },
        error: function (response) {
            var error = 400;
        }
    });
}
