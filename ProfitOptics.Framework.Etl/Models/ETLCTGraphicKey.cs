﻿using Newtonsoft.Json;

namespace ProfitOptics.Framework.Etl.Models
{
    public partial class ETLCTGraphicKey
    {
        [JsonProperty("graphicId")]
        public long GraphicId { get; set; }

        [JsonProperty("isGlobal")]
        public bool IsGlobal { get; set; }
    }
}
