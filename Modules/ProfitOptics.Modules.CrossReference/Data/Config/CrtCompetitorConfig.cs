﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.CrossReference.Data.Domain;

namespace ProfitOptics.Modules.CrossReference.Data.Config
{
    public class CrtCompetitorConfig : IEntityTypeConfiguration<CrtCompetitor>
    {
        public void Configure(EntityTypeBuilder<CrtCompetitor> builder)
        {
            builder.ToTable("CrtCompetitor");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.CompetitorName).HasMaxLength(50).IsRequired();
            builder.Property(entity => entity.CompetitorDescription).HasMaxLength(250).IsRequired();

        }
    }
}
