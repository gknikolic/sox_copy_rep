﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class ChangedOrder
	{
		[JsonProperty("id")]
		public Guid? Id { get; set; }

		[JsonProperty("geocodeStatus")]
		public Dictionary<GeocodeType,GeocodeStatusType> GeocodeStatus { get; set; }
		[JsonProperty("error")]
		public ErrorResponse Error { get; set; }
	}
}
