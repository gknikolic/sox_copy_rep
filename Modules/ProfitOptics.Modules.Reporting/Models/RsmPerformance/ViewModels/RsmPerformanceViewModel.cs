﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels
{
    public class RsmPerformanceViewModel
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int? UserRegionId { get; set; }
    }
}