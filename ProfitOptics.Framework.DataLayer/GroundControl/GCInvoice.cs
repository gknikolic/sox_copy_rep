﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCInvoice
    {
        public int Id { get; set; }
        public int GCCustomerId { get; set; }
        public int? QBInvoiceId { get; set; }
        public bool AllowIpnPayment { get; set; }
        public bool AllowOnlinePayment { get; set; }
        public bool AllowOnlineCreditCardPayment { get; set; }
        public bool AllowOnlineAchPayment { get; set; }
        public string Domain { get; set; }
        public bool Sparse { get; set; }
        public string SyncToken { get; set; }
        public DateTime? MetaDataCreateTime { get; set; }
        public DateTime? MetaDataLastUpdatedTime { get; set; }
        //public CustomField[] CustomField { get; set; }
        public string DocNumber { get; set; }
        public DateTime TxnDate { get; set; }
        public string CurrencyValue { get; set; }
        public string CurrencyName { get; set; }
        //public TxnTaxDetail TxnTaxDetail { get; set; }
        public string QBCustomerId { get; set; }
        public string QBCustomerName { get; set; }
        public string CustomerMemo { get; set; }
        //public BillAddr BillAddr { get; set; }
        //public CustomerMemo SalesTermRef { get; set; }
        public DateTime DueDate { get; set; }
        public decimal TotalAmt { get; set; }
        public bool ApplyTaxAfterDiscount { get; set; }
        public string PrintStatus { get; set; }
        public string EmailStatus { get; set; }
        public decimal Balance { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual ICollection<GCInvoiceLine> GCInvoiceLines { get; set; }
    }
}
