﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class MonnitResponse
    {
        [JsonProperty("Method")]
        public string Method { get; set; }

        [JsonProperty("Result")]
        public string Result { get; set; }
    }
}
