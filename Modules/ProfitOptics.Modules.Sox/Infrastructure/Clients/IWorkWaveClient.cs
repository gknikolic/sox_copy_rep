﻿using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Models.WorkWave;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Infrastructure.Clients
{
	public interface IWorkWaveClient
	{
		Task<WorkWaveResponse> AddOrder(AddOrdersModel order);
		Task<ListOrders> GetOrdersFromWWAsync();
		//Task<WorkWaveResponse> BuildRoutes(string teritoryId, DateTime from, DateTime to);
		Task HandleWorkwaweResponseAsync(CallbackResponse response);

		Task<SetCallbackUrlResponse> SetCallbackUrl(SetCallbackUrl model);
		Task<TerritoriesResponse> GetTerritories();

		Task<ListRoutes> GetRoutesFromWWByVehicleIdAsync(string VehicleId);
		Task<ListRoutes> GetRoutesFromWWAsync(DateTime? date = null);

		Task SyncRoutesWithWorkWaveAsync(DateTime? date = null);

		GCLifecycleStatusType GetActiveLifecycleStatusForCase(int caseId);
		Task GetOrdersFromWorWaveAsync();

		void CheckWorkWaveCaseStatus(GCCase gcCase, RouteStep routeStep, RouteStepStatusType? stepStatus, string User);

		Task SyncImagesWithWorkWave();
		Task<byte[]> GetImageFromWorkWave(string token);

		Task SendBarcodeExecutionEvent(BarcodeExecutionEvents data);

		Task SendStatusUpdateExecutionEvent(StatusUpdateExecutionEvents data);

		Task<ListOrders> GetOrderByIdFromWWAsync(string orderId);
		void DeleteOldEtlLogs();
		public Task DeleteOrdersInWW(List<string> orderIds);

		public Task<ListOrders> GetOrdersByIdFromWWAsync(List<string> orderIds);
		public Task<WorkWaveResponse> ReplaceOrders(AddOrdersModel orders);
		void CreateThumbnailsForGCImage(GCImage gcImage);
		Task<GPSDeviceCurrentInfos> GetGPSDevicesCurrentPos();
		Task<ListGPSDevices> GetGPSDevices();
	}
}
