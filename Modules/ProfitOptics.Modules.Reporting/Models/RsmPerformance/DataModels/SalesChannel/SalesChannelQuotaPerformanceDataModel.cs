﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel
{
    public class SalesChannelQuotaPerformanceDataModel : GenericQuotaPerformanceModel
    {
        public int SalesChannelId { get; set; }
        public string SalesChannelName { get; set; }
    }
}