﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportHierarchyZoneConfig : IEntityTypeConfiguration<ReportHierarchyZone>
    {
        public void Configure(EntityTypeBuilder<ReportHierarchyZone> builder)
        {
            builder.ToTable(nameof(ReportHierarchyZone));
            builder.HasKey(z => z.Id);

            builder.Property(z => z.Name).HasMaxLength(50).IsRequired();
            builder.Property(z => z.StartDate).IsRequired();

            builder.HasMany(z => z.ReportHierarchyRegions)
                .WithOne(r => r.ReportHierarchyZone)
                .HasForeignKey(r => r.ZoneId)
                .IsRequired(false);
        }
    }
}
