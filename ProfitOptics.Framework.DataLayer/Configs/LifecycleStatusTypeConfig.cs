﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
	public class LifecycleStatusTypeConfig : IEntityTypeConfiguration<GCLifecycleStatusType>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCLifecycleStatusType> builder)
        {
            builder.ToTable("GCLifecycleStatusType");
            builder.HasKey(entity => entity.Id);

            builder.HasOne(d => d.GCLifecycleStatusTypeCategory)
                .WithMany(p => p.GCLifecycleStatusTypes)
                .HasForeignKey(d => d.GCLifecycleStatusTypeCategoryId)
                .HasConstraintName("GCLifecycleStatusType_GCLifecycleStatusTypeCategory");
        }
    }
}
