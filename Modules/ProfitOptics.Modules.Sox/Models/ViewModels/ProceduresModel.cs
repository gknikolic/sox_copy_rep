﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class ProceduresModel
    {
        public string Title { get; set; }
        public List<ProcedureModel> Procedures { get; set; }
    }
}
