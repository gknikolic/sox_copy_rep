USE [Sox]

CREATE TABLE [dbo].[GCSystemAlertType]
(
	[Id] INT NOT NULL,
	[Name]  NVARCHAR(200) NULL,
	[Message]  NVARCHAR(500) NULL
CONSTRAINT [PK_GCSystemAlertType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (1, 'GCTrayIncompleteCountSheet', 'Incomplete Count Sheet')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (2, 'GCTrayQualityHold', 'Trays in Quality Hold')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (3, 'GCTraySkippedAssemblyAfterQualityHold', 'Tray skipped Assembly after Quality Hold')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (4, 'GCTraySkippedRequiredStepSink', 'Tray skipped required step - Sink')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (5, 'GCTraySkippedRequiredStepSinkLoaner', 'Tray skipped required step - Sink (Loaner)')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (6, 'GCTraySkippedRequiredStepWasher', 'Tray skipped required step - Washer')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (7, 'GCTraySkippedRequiredStepWasherLoaner', 'Tray skipped required step - Washer (Loaner)')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (8, 'GCTraySkippedRequiredStepAssembly', 'Tray skipped required step - Assembly')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (9, 'GCTraySkippedRequiredStepAssemblyLoaner', 'Tray skipped required step - Assembly (Loaner)')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (10, 'GCTraySkippedRequiredStepVerification', 'Tray skipped required step - Verification')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (11, 'GCTraySkippedRequiredStepSterilize', 'Tray skipped required step - Sterilize')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (12, 'GCTraySkippedRequiredStepSterileStaged', 'Tray skipped required step - Sterile Staged')

CREATE TABLE [dbo].[GCSystemAlert]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[GCSystemAlertTypeId] INT NOT NULL,
	[GCTrayId] INT NULL,
	[CreatedAtUtc] DATETIME NOT NULL,
	[MarkedAsReadAtUtc] DATETIME NULL
CONSTRAINT [PK_GCSystemAlert] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[GCSystemAlert]  WITH CHECK ADD  CONSTRAINT [FK_GCSystemAlert_GCSystemAlertType] FOREIGN KEY([GCSystemAlertTypeId])
REFERENCES [dbo].[GCSystemAlertType] ([Id])
GO
ALTER TABLE [dbo].[GCSystemAlert] CHECK CONSTRAINT [FK_GCSystemAlert_GCSystemAlertType]
GO

ALTER TABLE [dbo].[GCSystemAlert]  WITH CHECK ADD  CONSTRAINT [FK_GCSystemAlert_GCTray] FOREIGN KEY([GCTrayId])
REFERENCES [dbo].[GCTray] ([Id])
GO
ALTER TABLE [dbo].[GCSystemAlert] CHECK CONSTRAINT [FK_GCSystemAlert_GCTray]
GO