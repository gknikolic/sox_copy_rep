﻿using ProfitOptics.Modules.Sox.Models.GroundControl;
using System;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayHistoryTrayLifecycle
	{
		public DateTime Timestamp { get; set; }
		public bool ShowTimestamp { get; set; }
		public int LifecycleStatusId { get; set; }
		public string LifecycleTitle { get; set; }
		public LifecycleStatusType StatusType { get; set; }
		public string User { get; set; }
		public int LifecycleOrder { get; set; }
		public TrayHistoryPodContainer PodContainer { get; set; }
		public bool ShowPods { get; set; }
		public bool ShowSterilizeBI { get; set; }
		public TrayHistorySterilizeBIResult SterilizeBIResult { get; set; }
        public bool ShowVerificationQualityFeed { get; set; }
        public TrayHistoryVerificationQualityFeed VerificationQualityFeed { get; set; }
		public bool ShowCountSheets { get; set; }
		public AssemblyCountSheet AssemblyCountSheet { get; set; }
		public int Id { get; set; }
        public bool IsCTOrigin { get; set; }
    }
}
