﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class InventoryAlerStatusModel
    {
        public int QuarantinedTrayCount { get; set; }
        public int IncompleteCountSheetCount { get; set; }
    }
}
