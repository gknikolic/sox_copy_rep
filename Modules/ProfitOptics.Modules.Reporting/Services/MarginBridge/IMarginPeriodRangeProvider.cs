﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Services.MarginBridge
{
    public interface IMarginPeriodRangeProvider
    {
        MarginBridgePeriodRange Get(string id);
        MarginBridgePeriodRange GetDefault();
        IEnumerable<MarginBridgePeriodRange> GetAll();
        
    }
}