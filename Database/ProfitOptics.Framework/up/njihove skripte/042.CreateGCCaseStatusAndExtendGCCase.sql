USE [Sox]

BEGIN

  CREATE TABLE [dbo].[GCCaseStatus]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Code] int,
	[Title] [varchar](50)	
  )
  
  ALTER TABLE [Sox].[dbo].[GCCase]
  ADD [ProcedureName] [varchar](100),
      [GCCaseStatusId] int,
      FOREIGN KEY ([GCCaseStatusId]) REFERENCES [dbo].[GCCaseStatus] ([Id]);

END