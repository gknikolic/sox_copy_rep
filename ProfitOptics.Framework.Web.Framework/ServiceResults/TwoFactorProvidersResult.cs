﻿using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Framework.ServiceResults
{
    public class TwoFactorProvidersResult : AuthenticationServiceResult
    {
        public override bool Succeeded => Status == AuthenticationServiceResultStatusCode.Success &&
                                          Providers != null && Providers.Count > 0;

        public TwoFactorProvidersResult()
        {
            Providers = new List<string>();
        }

        public IList<string> Providers { get; set; }

        public static TwoFactorProvidersResult Success(IList<string> providers) => new TwoFactorProvidersResult
        {
            Status = AuthenticationServiceResultStatusCode.Success,
            Error = null,
            Providers = providers
        };
    }
}
