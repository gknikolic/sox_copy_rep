USE [Sox]
ALTER TABLE [dbo].[CTCase] ADD CaseCartId BIGINT NULL;
ALTER TABLE [dbo].[CTCase] ADD CaseTime DateTime NULL;
ALTER TABLE [dbo].[CTCase] ADD CartStatusId BIGINT NULL;
ALTER TABLE [dbo].[CTCase] ADD CaseCartName NVARCHAR(200) NULL;
ALTER TABLE [dbo].[CTCase] ADD CaseCartLocationName NVARCHAR(200) NULL;
ALTER TABLE [dbo].[CTCase] ADD ExpediteTime DateTime NULL;