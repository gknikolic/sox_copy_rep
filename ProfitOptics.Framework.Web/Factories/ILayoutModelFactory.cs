﻿using ProfitOptics.Framework.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Factories
{
    /// <summary>
    /// This factory is used for generating models for Layout and related view components
    /// </summary>
    public interface ILayoutModelFactory
    {
        Task<HeaderAccountMenuModel> PrepareHeaderAccountMenuModelAsync();

        Task<SideBarModel> PrepareSideBarModelAsync();
    }
}
