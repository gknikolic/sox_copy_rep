namespace ProfitOptics.Framework.Web.RestApi.Models.Distributor
{
    public class DistributorQuery : Query
    {
        public int? CustomerId { get; set; }
        public int? ProductId { get; set; }
        public DistributorQuery(int? customerId, int? productId, int page, int itemsPerPage, string sortColumn, string sortDirection, string filterColumn, string filterValue)
            : base(page, itemsPerPage, sortColumn, sortDirection, filterColumn, filterValue)
        {
            CustomerId = customerId;
            ProductId = productId;
        }
    }
}