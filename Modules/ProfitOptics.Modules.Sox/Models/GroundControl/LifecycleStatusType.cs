﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.GroundControl
{
	public enum LifecycleStatusType
	{
		Undefined = 0,
		Verification  = 1,
		Sterilize = 2,
		SterileStaged = 3,
		CaseAssembly = 4,
		TransportationStaging = 5,
		Loaded = 6,
		ShippedOutbound = 7,
		Delivered = 8,
		InSurgery = 9,
		SurgeryComplete = 10,
		Prepped = 11,
		Picked = 12,
		ShippedInbound = 13,
		Received = 14,
		DeconStaged = 15,
		Sink = 16,
		//Sonic = 16,
		Washer = 17,
		AssemblyStaged = 18,
		Assembly = 19,
		CleanIncomplete = 20,
		QualityHold = 21,
		CleanStaged = 22,

		// Loaners:
		LoanerInbound = 23,
		DeconStagedLoaner = 24,
		SinkLoaner = 25,
		WasherLoaner = 26,
		AssemblyStagedLoaner = 27,
		AssemblyLoaner = 28,
		LoanerOutbound = 29
	}
}
