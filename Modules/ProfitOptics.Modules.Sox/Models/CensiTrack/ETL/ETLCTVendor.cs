﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTVendor
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("contactName")]
        public string ContactName { get; set; }

        [JsonProperty("vendorTypes")]
        public int VendorTypes { get; set; }

        [JsonProperty("friendlyName")]
        public string FriendlyName { get; set; }
    }
}
