﻿namespace ProfitOptics.Framework.Web.RestApi.DataLayer
{
    public class DistributorProduct
    {
        public int DistributorId { get; set; }

        public Distributor Distributor { get; set; }

        public int ProductId { get; set; }

        public Product Product { get; set; }
    }
}
