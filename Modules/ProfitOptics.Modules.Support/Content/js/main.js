﻿(function (support, $, undefined) {
    support.getAjaxFilterOptions = function (options) {
        var defaults = {
            allowClear: true,
            width: "100%",
            placeholder: 'Any',
            minimumInputLength: 1,
            ajax: {
                dataType: 'json',
                data: function (term) {
                    return {
                        searchQuery: term
                    };
                },
                results: function (results) {
                    return results;
                },
                quietMillis: 2000
            },
            formatResult: function (result) {
                return result.text;
            },
            formatSelection: function (data, container, escapeFn) {
                return data.text;
            }
        };

        return $.extend(true, {}, defaults, options);
    };

    // Urls will be set in support area _Layout.cshtml
    support.urls = {};

    var onReporterChange = function (callback) {
        app.blockUI("Persisting reporter...");

        $.ajax({
            url: support.urls["changeReporter"],
            data: { reporterValue: $("#Reporter").val(), reporterText: $("#Reporter").val() },
            type: 'POST',
            cache: false,
            success: function (data) {
                app.unblockUI();
                if (typeof (data) != 'undefined' && data != null && data != '' && data.Status == 'OK') {
                    if (typeof (callback) != 'undefined' && callback != null && typeof (callback) === 'function') {
                        callback();
                    }
                } else {
                    bootbox.alert('Error while persisting reporter.');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.unblockUI();
                bootbox.alert('Error while persisting reporter.');
            }
        });
    };

    var onProjectChange = function (callback) {
        app.blockUI("Persisting project...");

        $.ajax({
            url: support.urls["changeProject"],
            data: { project: $("#Project").val() },
            type: 'POST',
            cache: false,
            success: function (data) {
                app.unblockUI();

                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.unblockUI();
                bootbox.alert('Error while persisting project.');
            }
        });
    };

    var onComponentChange = function (callback) {
        components = [];
        $('.js_componentButton').each(function () {
            var $this = $(this);
            var isActive = $this.attr('data-active').toLowerCase() === 'true' ? true : false;
            if (isActive) {
                components.push($this.attr('data-componentid'));
            }
        });

        app.blockUI("Persisting components...");
        $.ajax({
            url: support.urls["changeMultipleComponent"],
            data: { components: JSON.stringify(components) },
            type: 'POST',
            cache: false,
            success: function (data) {
                app.unblockUI();
                if (typeof (data) != 'undefined' && data != null && data != '' && data.Status == 'OK') {

                    if (typeof (callback) != 'undefined' && callback != null && typeof (callback) === 'function') {
                        callback();
                    }
                } else {
                    bootbox.alert('Error while persisting component.');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.unblockUI();
                bootbox.alert('Error while persisting component.');
            }
        });
    };

    var onAssigneeChange = function (callback) {
        components = [];
        app.blockUI("Persisting assignee...");
        $.ajax({
            url: support.urls["changeAssignee"],
            data: { assignee: $('#Assignee').val() },
            type: 'POST',
            cache: false,
            success: function (data) {
                app.unblockUI();
                if (typeof (data) != 'undefined' && data != null && data != '' && data.Status == 'OK') {

                    if (typeof (callback) != 'undefined' && callback != null && typeof (callback) === 'function') {
                        callback();
                    }
                } else {
                    bootbox.alert('Error while persisting assignee.');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.unblockUI();
                bootbox.alert('Error while persisting assignee.');
            }
        });
    };

    var componentChangeCallback = function () {
        onComponentChange(InitTables);
    };

    var assigneeChangeCallback = function () {
        onAssigneeChange(componentChangeCallback);
    };

    var reporterChangeCallback = function () {
        onReporterChange(assigneeChangeCallback);
    };

    var projectChangeCallback = function () {
        screenBlockCounter = 0;
    };

    // use it like this: moveIssueToBacklog(issueId, InitTables);
    var moveIssueToBacklog = function (issueId, callback) {
        app.blockUI("Moving to Backlog...");
        $.ajax({
            url: support.urls["moveToBacklog"],
            data: { issue: issueId },
            type: 'POST',
            cache: false,
            success: function (data) {
                app.unblockUI();
                if (typeof (data) != 'undefined' && data != null && data != '' && data.Status == 'OK') {
                    // chill
                } else {
                    if (typeof (data.Message) != 'undefined' && data.Message != null && data.Message != '') {
                        bootbox.alert('Error while moving issue to backlog. ' + data.Message);
                    } else {
                        bootbox.alert('Error while moving issue to backlog.');
                    }
                }

                if (typeof (callback) != 'undefined' && callback != null && typeof (callback) === 'function') {
                    callback();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.unblockUI();
                bootbox.alert('Error while moving issue to backlog.');
            }
        });
    };

    var changeStatus = function (issueId, transitionName, callback) {
        app.blockUI("Updating status...");
        $.ajax({
            url: support.urls["updateStatus"],
            data: {
                issueToUpdate: issueId,
                transitionName: transitionName
            },
            type: 'POST',
            cache: false,
            success: function (data) {
                app.unblockUI();
                if (typeof (data) != 'undefined' && data != null && data != '' && data.Status == 'OK') {
                    if (typeof (callback) != 'undefined' && callback != null && typeof (callback) === 'function') {
                        callback();
                    }
                } else {
                    bootbox.alert('Error while updating status.');
                    InitTables();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.unblockUI();
                bootbox.alert('Error while updating status.');
                InitTables();
            }
        });
    };

    var rankBeforeIssue = function (issueId, rankBeforeIssueId, callback) {
        app.blockUI("Updating rank...");
        $.ajax({
            url: support.urls["rankBeforeIssue"],
            data: {
                issueIdToRank: issueId,
                rankBeforeIssueId: rankBeforeIssueId,
                project: $("#Project").val()
            },
            type: 'POST',
            cache: false,
            success: function (data) {
                app.unblockUI();
                if (typeof (data) != 'undefined' && data != null && data != '' && data.Status == 'OK') {
                    if (typeof (callback) != 'undefined' && callback != null && typeof (callback) === 'function') {
                        callback();
                    }
                } else {
                    bootbox.alert('Error while updating rank.');
                    InitTables();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.unblockUI();
                bootbox.alert('Error while updating rank.');
                InitTables();
            }
        });
    };

    var rankAfterIssue = function (issueId, rankAfterIssueId, callback) {
        app.blockUI("Updating rank...");
        $.ajax({
            url: support.urls["rankAfterIssue"],
            data: {
                issueIdToRank: issueId,
                rankAfterIssueId: rankAfterIssueId,
                project: $("#Project").val()
            },
            type: 'POST',
            cache: false,
            success: function (data) {
                app.unblockUI();
                if (typeof (data) != 'undefined' && data != null && data != '' && data.Status == 'OK') {
                    if (typeof (callback) != 'undefined' && callback != null && typeof (callback) === 'function') {
                        callback();
                    }
                } else {
                    bootbox.alert('Error while updating rank.');
                    InitTables();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.unblockUI();
                bootbox.alert('Error while updating rank.');
                InitTables();
            }
        });
    };

    var getPossibleTransitions = function (issueId, source, target, callback) {
        app.blockUI("Fetching possible transitions...");
        $.ajax({
            url: support.urls["getPossibleTransitions"],
            data: {
                projectKey: $("#Project").val(),
                forIssue: issueId,
                oldContainer: source,
                newContainer: target
            },
            type: 'POST',
            cache: false,
            success: function (data) {
                app.unblockUI();
                if (typeof (data) != 'undefined' && data != null && data != '' && data.Status == 'OK') {
                    if (data.Transitions.length == 0) {
                        bootbox.alert('We could not find allowed transition for this issue. The workflow is not configured to support changing status for this issue.');
                        InitTables();
                    } else {
                        var toAdd = '';
                        for (var i = 0; i < data.Transitions.length; i++) {
                            var transName = data.Transitions[i].Name;

                            toAdd += '<button class="btn btn-default js_transitionSelected" style="width:auto; margin: 5px;" data-transitionid="' + transName + '">' + transName + '</button>';
                        }
                        $('#transitionsModalButtonWrapper').html(toAdd);

                        $('.js_transitionSelected').unbind('click').bind('click', function () {
                            $("#transitionsModal").modal("hide");
                            var transName = $(this).attr('data-transitionid');
                            changeStatus(issueId, transName, callback);
                        });
                        $('#transitionsModalCancelButton').unbind('click').bind('click', function () {
                            bootbox.alert('Canceling action.');
                            $("#transitionsModal").modal("hide");
                            InitTables();
                        });
                        $('#transitionsModal').modal('show');
                    }
                } else {
                    bootbox.alert('Error while fetching possible transitions.');
                    InitTables();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.unblockUI();
                bootbox.alert('Error while fetching possible transitions.');
                InitTables();
            }
        });

    };

    support.setupDragNDrop = function () {
        drake = dragula({
            isContainer: function (el) {
                return el.classList.contains('dragula-container');
            }
        }).on('drop', function (el, target, source, sibling) {
            var issueId = $(el).find('div.ddrop-box').attr('data-issueid');
            var targetContainer = $(target).attr('data-container');
            var sourceContainer = $(source).attr('data-container');
            var isAboveIssueId = $(el).prev().find('div.ddrop-box').attr('data-issueid'); 

            var afterStatusChangeCallback = function () {
                if (typeof (isAboveIssueId) != 'undefined' && isAboveIssueId != null && isAboveIssueId != '') {
                    rankAfterIssue(issueId, isAboveIssueId, InitTables);
                } else {
                    var isBellowIssueId = $(sibling).find('div.ddrop-box').attr('data-issueid');
                    rankBeforeIssue(issueId, isBellowIssueId, InitTables);
                }
            };
            if (targetContainer != sourceContainer) {
                getPossibleTransitions(issueId, sourceContainer, targetContainer, afterStatusChangeCallback);
            } else {
                afterStatusChangeCallback(); // no status change, just rank update
            }
        });
    };

    support.onReady = function () {
        $('#applyFiltersButton').click(function () {
            reporterChangeCallback();
        });

        $('#selectAllCompButton').click(function () {
            $('.js_componentButton').each(function () {
                var $this = $(this);
                $this.attr('data-active', true);
                $this.removeClass('btn-primary').removeClass('btn-default').addClass('btn-primary');
            });
        });

        $('#clearAllCompButton').click(function () {
            $('.js_componentButton').each(function () {
                var $this = $(this);
                $this.attr('data-active', false);
                $this.removeClass('btn-primary').removeClass('btn-default').addClass('btn-default');
            });
        });

        $('#Project').change(function () {
            onProjectChange(projectChangeCallback);
        });

        if ($('#Project').val() != '') {
            projectChangeCallback();
        }

        $('#Assignee').selectpicker();

        $('#Reporter').selectpicker();
    };
}(window.support = window.support || {}, jQuery));