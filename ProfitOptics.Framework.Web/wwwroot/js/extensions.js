﻿Number.prototype.formatMoney = function (decimals) {
    var n = this;
    var formatted = this.formatNumber(decimals);
    if (n < 0)
        formatted = formatted.replace("(", "($");
    else
        formatted = "$" + formatted;

    return formatted;
};

Number.prototype.formatNumber = function (decimals) {
    if (decimals == null)
        decimals = 2;

    var n = this;
    var toReturn = n.toFixed(decimals).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if (n < 0)
        toReturn = "(" + toReturn.replace("-", "") + ")";

    return toReturn;
};

Number.prototype.formatPercent = function (decimals) {
    return ((this * 100).formatNumber(decimals)) + "%";
};

String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}

String.prototype.formatJsonDate = function (showtime) {
    if (!this || this.substr(0, 4) == "0001")
    { return ""; }

    var date = new Date(Date.parse(this));//new Date(parseInt(this.substr(6)));
    var toReturn = date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
    if (showtime) {

        toReturn = toReturn + " " + pad(date.getHours(), 2) + ":" + pad(date.getMinutes(), 2) + ":" + pad(date.getSeconds(), 2);
    }
    return toReturn;
};

String.prototype.formatUTCJsonDate = function (showtime) {
    if (!this || this.substr(0, 4) == "0001")
    { return ""; }

    var date = new Date(parseInt(this.substr(6)));
    var toReturn = date.getUTCMonth() + 1 + "/" + date.getUTCDate() + "/" + date.getUTCFullYear();
    if (showtime) {

        toReturn = toReturn + " " + pad(date.getUTCHours(), 2) + ":" + pad(date.getUTCMinutes(), 2) + ":" + pad(date.getUTCSeconds(), 2);
    }
    return toReturn;
};

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

/**
**/
Function.prototype.trace = function () {
    var trace = [];
    var current = this;
    while (current) {
        trace.push(current.signature());
        current = current.caller;
    }
    return trace;
}

Function.prototype.signature = function () {
    var signature = {
        name: this.getName(),
        params: [],
        toString: function () {
            var params = this.params.length > 0 ?
                "'" + this.params.join("', '") + "'" : "";
            return this.name + "(" + params + ")"
        }
    };
    if (this.arguments) {
        for (var x = 0; x < this.arguments.length; x++)
            signature.params.push(this.arguments[x]);
    }
    return signature;
}

Function.prototype.getName = function () {
    if (this.name)
        return this.name;
    var definition = this.toString().split("\n")[0];
    var exp = /^function ([^\s(]+).+/;
    if (exp.test(definition))
        return definition.split("\n")[0].replace(exp, "$1") || "anonymous";
    return "anonymous";
}

$.fn.serializeObject = function () {
    var fields = $(this).serializeArray();
    var result = {};
    for (var i in fields) {
        result[fields[i].name] = fields[i].value;
    }
    return result;
};

$.fn.select2Original = $.fn.select2;
// Override the select2 plugin
$.fn.select2 = function (options) {
    if (options && !(options instanceof Object)) {
        // If .select2() was called with some non-empty non-object parameter, just forward the call,
        // since it was not an initialization, but something else like .select2('close')
        return this.select2Original(options);
    }
    return this.each(function () {
        var $select = $(this);
        // Call the original function
        $select.select2Original(options);
        // On open, read the previous search value from a data attribute, and simulate field input event
        $select.on('select2:open', function () {
            var prevValue = $select.data('prevSearch');
            if (prevValue) {
                // Delayed because "trigger" and "select" are sometimes buggy when called immediately
                setTimeout(function () {
                    $select.find('.select2-search__field').val(prevValue).trigger('keydown').trigger('input').select();
                }, 0);
            }
        });
        // On closing, save the previous search value to a data attribute
        $select.on('select2:closing', function () {
            $select.data('prevSearch', $select.find('.select2-search__field').val());
        });
    });
};
