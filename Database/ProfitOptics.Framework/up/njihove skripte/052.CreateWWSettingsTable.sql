USE [Sox]

BEGIN


  DROP TABLE IF EXISTS [dbo].[WWSettings]
  CREATE TABLE [dbo].[WWSettings]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[TerritoryId] [varchar](50),
	[IsActive] [bit] NOT NULL
  )


END