﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ProfitOptics.Modules.Reporting.Models.ReportFilter
{
    public class ReportFilterModel
    {
        public ReportFilterModel()
        {
            ZoneId = RegionId = SalesRepId = default(int);
            FilterZone = new List<int>();
            FilterRegion = new List<int>();
            FilterCustomerClass = new List<int>();
            FilterItemCategory = new List<int>();
            FilterVendor = new List<int>();
            FilterBuyGroup = new List<int>();
            FilterSalesRep = new List<int>();
            FilterBillTo = new List<int>();
            FilterShipTo = new List<int>();
        }

        [JsonIgnore]
        public int? ZoneId { get; set; }
        [JsonIgnore]
        public int? RegionId { get; set; }
        [JsonIgnore]
        public int? SalesRepId { get; set; }

        public List<int> FilterZone { get; set; }

        public List<int> FilterRegion { get; set; }

        public List<int> FilterCustomerClass { get; set; }

        public List<int> FilterItemCategory { get; set; }

        public List<int> FilterVendor { get; set; }

        public List<int> FilterBuyGroup { get; set; }

        public List<int> FilterSalesRep { get; set; }

        public List<int> FilterBillTo { get; set; }

        public List<int> FilterShipTo { get; set; }
    }
}
