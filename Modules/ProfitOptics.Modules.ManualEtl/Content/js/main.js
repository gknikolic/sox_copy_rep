﻿(function (manualEtl, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    // Public properties
    manualEtl.somePublicProperty = {};

    // Public Methods
    manualEtl.somePublicMethod = function () {
    };

    // Private Methods
    function somePrivateMethod(item) {
    }
}(window.manualEtl = window.manualEtl || {}, jQuery));