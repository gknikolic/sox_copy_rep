USE [Sox]
ALTER TABLE [dbo].[GCCase] DROP COLUMN OutgoingPickupOrderId;
ALTER TABLE [dbo].[GCCase] DROP COLUMN OutgoingDeliveryOrderId; 
ALTER TABLE [dbo].[GCCase] DROP COLUMN IncomingPickupOrderId; 
ALTER TABLE [dbo].[GCCase] DROP COLUMN IncomingDeliveryOrderId; 
ALTER TABLE [dbo].[GCCase] DROP COLUMN RequestId;
ALTER TABLE [dbo].[GCCase] ADD OutgoingOrderId NVARCHAR(50) NULL;
ALTER TABLE [dbo].[GCCase] ADD IncomingOrderId NVARCHAR(50) NULL;
ALTER TABLE [dbo].[GCCase] ADD OutgoingRequestId NVARCHAR(50) NULL;
ALTER TABLE [dbo].[GCCase] ADD IncomingRequestId NVARCHAR(50) NULL;

