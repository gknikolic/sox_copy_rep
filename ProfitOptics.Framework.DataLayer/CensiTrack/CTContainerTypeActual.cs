﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerTypeActual
    {
        public int Id { get; set; }
        public int CTContainerTypeId { get; set; }
        public long LegacyId { get; set; }
        public long ParentId { get; set; }
        public decimal? Weight { get; set; }
        public string PhysicianName { get; set; }
        public string SterilizationMethodName { get; set; }
        public string ServiceName { get; set; }
        public long ServiceId { get; set; }
        public string ProcurementReferenceId { get; set; }
        public string Name { get; set; }
        public string ParentName { get; set; }
        public string Status { get; set; }
        public DateTime UpdateTimestamp { get; set; }
        public string UpdateUserId { get; set; }
        public string LocationElapsed { get; set; }
        public string CaseCart { get; set; }
        public string ProcessingLocationName { get; set; }
        public string ParentProcessingLocationName { get; set; }
        public string ParentUsageIntervalUnitOfMeasure { get; set; }
        public string HomeLocationName { get; set; }
        public string ParentHomeLocationName { get; set; }
        public int? ParentIntervalUsageCount { get; set; }
        public int? IntervalUsageCount { get; set; }
        public string Usage { get; set; }
        public DateTime? CensisSetLastRequestDate { get; set; }
        public DateTime? LastMaintenance { get; set; }
        public string Assembly { get; set; }
        public string Storage { get; set; }
        public string RtlsLocationName { get; set; }
        public DateTime? RtlsLocationUpdateTimestamp { get; set; }
        public bool? IsDiscarded { get; set; }
        public string Priority { get; set; }
        public string ShelfLife { get; set; }
        public long? NotificationCount { get; set; }
        public string Expedite { get; set; }
        public string Rigidity { get; set; }
        public string ParentExpedite { get; set; }
        public string ParentRigidity { get; set; }
        public DateTime? CensisSetHistoryBuildStartDate { get; set; }
        public long? FirstAlternateMethodId { get; set; }
        public string FirstAlternateMethodName { get; set; }
        public long? SecondAlternateMethodId { get; set; }
        public string SecondAlternateMethodName { get; set; }
        public long? ThirdAlternateMethodId { get; set; }
        public string ThirdAlternateMethodName { get; set; }
        public string ComplexityLevelDescription { get; set; }
        public string DisposableFlag { get; set; }
        public string SerialNumber { get; set; }
        public string VendorName { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
        public DateTime UpdateTimestampOriginal { get; set; }
        public virtual CTContainerType CTContainerType { get; set; }
    }
}
