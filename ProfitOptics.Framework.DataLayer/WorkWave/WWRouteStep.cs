﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
	[Table("WWRouteStep")]
	public partial class WWRouteStep
	{
		public int Id { get; set; }
		public string Type { get; set; }
		public string WWOrderId { get; set; }
		public int WWRouteId { get; set; }
        public int WWTrackingDataId { get; set; }
        public virtual WWRoute Route { get; set; }
        public virtual WWTrackingData WWTrackingData { get; set; }
	}
}
