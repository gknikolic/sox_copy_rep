﻿using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using ProfitOptics.Framework.Etl.Models;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public interface ISQLServerHelper
    {
        /// <summary>
        /// Backups a database to a file
        /// </summary>
        /// <param name="csb"></param>
        /// <param name="destinationPath"></param>
        void BackupDatabase(SqlConnectionStringBuilder csb, string destinationPath);

        /// <summary>
        /// Restores a database from a file
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="backUpFile"></param>
        void RestoreDatabase(string serverName, string databaseName, string userName, string password, string backUpFile);

        /// <summary>
        /// Shrinks all of the log files to a specific size
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="size"></param>
        /// <param name="shrinkMethod"></param>
        void ShrinkLogFiles(string serverName, string databaseName, string userName, string password, int size, ShrinkMethod shrinkMethod);

        /// <summary>
        /// Changes the recovery model for the specific db.
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="recoveryModel"></param>
        void ChangeDbRecoveryModel(string serverName, string databaseName, string userName, string password, RecoveryModel recoveryModel);

        /// <summary>
        /// Queries the location of the mdf and log files
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        DbFilesLocationModel ReadDbMdfAndLogLocation(string serverName, string databaseName, string userName, string password);

        /// <summary>
        /// Adds a user to the db and assignes it a dbo_owner role
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="user"></param>
        void AddUserToDb(string serverName, string databaseName, string userName, string password, string user);
    }
}
