﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueType
    {
        public string name { get; set; }

        public bool subtask { get; set; }
    }
}