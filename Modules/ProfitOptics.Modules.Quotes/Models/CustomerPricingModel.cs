﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.Collections.Generic;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class CustomerPricingModel
    {
        public string PriceListCode { get; set; }
        public string Description { get; set; }
        public int SeqNum { get; set; }
        public List<ItemModel> Items { get; set; }
    }
}