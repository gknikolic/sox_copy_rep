﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Framework.Web.Framework.ServiceResults;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class AccountManagementServiceResult : ServiceActionResult<AccountManagementServiceResultStatusCode>
    {
        public override bool Succeeded => Status == AccountManagementServiceResultStatusCode.Success;

        public AccountManagementServiceResult()
        {
        }

        public AccountManagementServiceResult(IdentityResult result)
        {
            Status = result.Succeeded
                ? AccountManagementServiceResultStatusCode.Success
                : AccountManagementServiceResultStatusCode.Failed;
            Error = string.Join(";", result.Errors?.Select(x => x.Description) ?? new List<string>()).TrimEnd(';');
        }

        public static AccountManagementServiceResult Success => new AccountManagementServiceResult
        {
            Status = AccountManagementServiceResultStatusCode.Success,
            Error = null
        };

        public static TResult Failed<TResult>(string error) where TResult : AccountManagementServiceResult, new() => new TResult
        {
            Error = error,
            Status = AccountManagementServiceResultStatusCode.Failed
        };

        public static AccountManagementServiceResult Failed(string error)
        {
            return Failed<AccountManagementServiceResult>(error);
        }

        public static TResult UserNotFound<TResult>() where TResult : AccountManagementServiceResult, new() => new TResult
        {
            Error = null,
            Status = AccountManagementServiceResultStatusCode.UserNotFound
        };

        public static AccountManagementServiceResult UserNotFound()
        {
            return UserNotFound<AccountManagementServiceResult>();
        }
    }
}
