﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTUpsAdditionalSurchargeConfig : IEntityTypeConfiguration<QTUpsAdditionalSurcharge>
    {
        public void Configure(EntityTypeBuilder<QTUpsAdditionalSurcharge> builder)
        {
            builder.ToTable("QTUpsAdditionalSurcharge");
            builder.HasKey(entity => entity.Id);
        }
    }
}
