﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class TrayHistorySterilizeBIIndicator
    {
        public string Name { get; set; }
        public string Result { get; set; }
        public string Product { get; set; }
        public string LotNumber { get; set; }
        public int Order { get; set; }
    }
}
