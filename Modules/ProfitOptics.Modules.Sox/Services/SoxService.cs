﻿using AutoMapper;
using Flurl.Http;
using Intuit.Ipp.OAuth2PlatformClient;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.DataLayer.GroundControl;
using ProfitOptics.Framework.DataLayer.WorkWave;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Infrastructure;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Models.CensiTrack;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using ProfitOptics.Modules.Sox.Models.ViewModels;
using ProfitOptics.Modules.Sox.Models.ViewModels.Paging;
using ProfitOptics.Modules.Sox.Models.WorkWave;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static ProfitOptics.Modules.Sox.Services.GroundControlService;

namespace ProfitOptics.Modules.Sox.Services
{
    public class SoxService : ISoxService
    {
        #region Fields

        private readonly Framework.DataLayer.Entities _context;
        public IWorkWaveClient WorkWaveClient { get; set; }
        public IWorkWaveService WorkWaveService { get; set; }
        public IBoxstormService BoxstormService { get; set; }
        public readonly IMapper _mapper;
        public readonly IUserService _userService;
        public readonly IGroundControlService _groundControlService;
        public readonly ICaseManagementClient _caseManagementClient;
        private readonly string wwImageUrl = "https://wwrm.workwave.com/pod/";
        private readonly IConfiguration configuration;
        public GlobalOptions GlobalOptions { get; set; }
        //public CensiTrackOptions CensiTrackOptions { get; set; }


        #endregion Fields

        #region Ctor

        public SoxService(Framework.DataLayer.Entities context,
                         IWorkWaveClient workWaveClient,
                         IMapper mapper,
                         IUserService userService,
                         IGroundControlService groundControlService,
                         ICaseManagementClient caseManagementClient,
                         IWorkWaveService workWaveService,
                         GlobalOptions globalOptions,
                         //CensiTrackOptions censiTrackOptions,
                         IConfiguration configuration,
                         IBoxstormService boxstormService)
        {
            _context = context;
            WorkWaveClient = workWaveClient;
            _mapper = mapper;
            _userService = userService;
            _groundControlService = groundControlService;
            _caseManagementClient = caseManagementClient;
            WorkWaveService = workWaveService;
            GlobalOptions = globalOptions;
            //CensiTrackOptions = censiTrackOptions;
            this.configuration = configuration;
            BoxstormService = boxstormService;
        }

        #endregion Ctor

        #region Methods

        public List<GCCase> GetGCCases()
        {
            var result = _context.GCCases
                .Include(x => x.CTCase)
                .Include(x => x.GCCustomer)
                .Include(x => x.GCCaseStatus)
                .ToList();

            return result;
        }

        public GCCase GetGCCaseById(int id, ClaimsPrincipal user)
        {

            var gcCasesIds = FilterCasesByUserRole(user).Select(x => x.Id).ToList();

            var gcCase = _context.GCCases.Where(x => x.Id == id && gcCasesIds.Contains(x.Id))
                .Include(x => x.CTCase)
                .Include(x => x.GCCustomer)
                .Include(x => x.GCCaseStatus)
                .Include(x => x.GCTrays).ThenInclude(x => x.CTCaseContainer)
                //.Where(x => gcCasesIds.Contains(x.Id))
                .FirstOrDefault();

            return gcCase;
        }

        public IEnumerable<SelectListItem> GetGCCasesList()
        {
            var cases = _context.GCCases.ToList();

            var casesList = cases.Select(item => new SelectListItem
            {
                Text = item.Title,
                Value = item.Id.ToString()
            });

            return casesList;
        }

        public IEnumerable<SelectListItem> GetGCCasesListByDate(DateTime date)
        {
            var cases = _context.GCCases
                .Include(x => x.CTCase)
                .Include(x => x.GCCustomer)
                .ToList();

            var todaysCases = cases.Where(x => x.CTCase.DueTimestamp.TrimHoursMinutesSeconds() == date.TrimHoursMinutesSeconds()).ToList();

            var casesList = todaysCases.Select(item => new SelectListItem
            {
                Text = item.Title + " - " + item.GCCustomer.CustomerName,
                Value = item.Id.ToString()
            });

            return casesList;
        }

        public List<GCCase> GetCasesByCustomer(int customerId)
        {
            var cases = _context.GCCases.Where(x => x.GCCustomerId == customerId).Include(x => x.CTCase).ToList();

            return cases;
        }

        public void ChangeGCCaseLifecycleStatus(ManageGCCaseLifecycleStatuses model)
        {
            _caseManagementClient.ChangeGCCaseLifecycleStatus(model);
        }

        public GCLifecycleStatusType GetLifecycleStatusById(int id)
        {
            var status = _context.GCLifecycleStatusTypes.Find(id);

            return status;
        }

        public GCLifecycleStatusType GetLifecycleStatusByTitle(string title)
        {
            var status = _context.GCLifecycleStatusTypes.Where(x => x.Title == title).FirstOrDefault();
            return status;
        }

        public void DeleteGCCase(int gcCaseId)
        {
            _groundControlService.DeleteGCCase(gcCaseId);
        }

        public void DeleteAllGCCases()
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var caseIds = _context.GCCases.Select(x => x.Id).ToList();
                    foreach (var id in caseIds)
                    {
                        //DeleteGCCase(id);
                        _groundControlService.DeleteGCCase(id);
                    }

                    var trayIds = _context.GCTrays.Where(x => !x.GCCaseId.HasValue).Select(x => x.Id).ToList();
                    if (trayIds != null && trayIds.Count > 0)
                    {
                        foreach (var gcTrayId in trayIds)
                        {
                            _groundControlService.DeleteGCTray(gcTrayId);
                        }
                        _context.SaveChanges();
                    }

                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        public bool IsTrayInLifecycleStatus(int trayId, int statusId)
        {
            var trayStatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == trayId && x.LifecycleStatusTypeId == statusId).FirstOrDefault();

            if (trayStatus != null)
                return true;
            else
                return false;
        }

        public IEnumerable<SelectListItem> GetGCLifecycleStatusesList()
        {
            var statuses = _context.GCLifecycleStatusTypes.ToList();

            var statusesList = statuses.Select(item => new SelectListItem
            {
                Text = item.Title,
                Value = item.Id.ToString()
            });

            return statusesList;
        }


        public List<CTCaseContainer> GetTraysByCTCaseId(int ctCaseId)
        {
            var trays = _context.CTCaseContainers.Where(x => x.CTCaseId == ctCaseId).ToList();
            return trays;
        }

        public GCTray GetTrayById(int gCTrayId)
        {
            var tray = _context.GCTrays.Include(x => x.CTContainerType).Include(x => x.CTContainerTypeActual).Where(x => x.Id == gCTrayId).FirstOrDefault();
            return tray;
        }

        public GCTray GetTrayByActualId(int actualId)
        {
            var tray = _context.GCTrays
                                    //.Include(x => x.GCCase)
                                    //.ThenInclude(x => x.CTCase)
                                    //.Include(x => x.CTContainerTypeActual)
                                    //.Include(x => x.GCLifecycleStatusType)
                                    //.Include(x => x.GCLifecycleStatusType.GCLifecycleStatusTypeCategory)
                                    .Include(X => X.CTCaseContainer)
                                .Include(x => x.CTContainerType).Where(x => x.CTContainerTypeActualId == actualId).FirstOrDefault();
            return tray;
        }

        public List<GCLifecycleStatusType> GetLifecycleStatusTypes()
        {
            var statuses = _context.GCLifecycleStatusTypes.ToList();

            return statuses;
        }

        public LifecycleStatusWithTimestamp GetActiveLifecycleStatusAndTimestampForTray(int trayId)
        {
            return _caseManagementClient.GetActiveLifecycleStatusAndTimestampForTray(trayId);
        }

        public List<GCTray> GetTraysForGCCase(int gcCaseId, bool? includeCTContainerType = null)
        {
            var query = _context.GCTrays
                            .Include(x => x.GCCase)
                                    .ThenInclude(x => x.CTCase)
                            .Include(x => x.CTContainerTypeActual)
                            .Include(x => x.GCLifecycleStatusType)
                            .Include(x => x.GCLifecycleStatusType.GCLifecycleStatusTypeCategory).AsQueryable();

            if(includeCTContainerType.HasValue && includeCTContainerType.Value == true)
            {
                query = query.Include(x => x.CTContainerType);
            }

            query = query.Where(x => x.GCCaseId == gcCaseId);

            var trays = query.ToList();

            return trays;
        }

        public List<GCTray> GetAllGCTrays()
        {
            var trays = _context.GCTrays.Include(x => x.CTContainerTypeActual).Include(x => x.GCLifecycleStatusType).ToList();

            return trays;
        }

        public List<GCTray> GetAllGCTraysByCaseId(int caseId)
        {
            var trays = _context.GCTrays.Where(x=>x.GCCaseId == caseId).Include(x => x.CTContainerTypeActual).Include(x => x.GCLifecycleStatusType).ToList();

            return trays;
        }

        public List<GCTray> GetAllInventoryTrays(List<int> actualIds = null, bool? includeLoaners = false)
        {
            var query = _context.GCTrays
                .Include(x => x.CTContainerTypeActual)
                .Include(x => x.GCLifecycleStatusType)
                .Include(x => x.CTContainerType)
            .Where(x => x.CTContainerTypeActualId != null && x.CTContainerTypeId != null);
            //.Where(x => x.CTContainerTypeActualId != null && x.GCCaseId == null && x.CTContainerTypeId != null);

            if (includeLoaners.HasValue && includeLoaners.Value == true)
            {
                query = query.Where(x =>
                    (x.GCCaseId == null && (x.IsLoaner == null || x.IsLoaner.Value == false)) ||
                    (x.GCCaseId != null && (x.IsLoaner != null && x.IsLoaner.Value == true)));
            }
            else
            {
                query = query.Where(x => x.GCCaseId == null);
            }

            if(actualIds != null)
            {
                query = query.Where(x => x.CTContainerTypeActualId.HasValue && actualIds.Contains(x.CTContainerTypeActualId.Value));
            }

            var trays = query.ToList();
            return trays;
        }

        public List<CTCaseContainer> GetCTCaseContainers()
        {
            var trays = _context.CTCaseContainers.ToList();

            return trays;
        }

        public CTCaseContainer GetCTCaseContainerById(int id)
        {
            var caseContainer = _context.CTCaseContainers.Where(x => x.Id == id).FirstOrDefault();

            return caseContainer;
        }

        public CTContainerType GetCTContainerType(int id)
        {
            var ctContainerType = _context.CTContainerTypes.Where(x => x.Id == id).FirstOrDefault();

            return ctContainerType;
        }

        public List<CTContainerType> GetAllCTConntainerTypes(string vendorName = null)
        {
            var query = _context.CTContainerTypes.AsQueryable();
            if(vendorName != null)
            {
                query = query.Where(x => x.VendorName == vendorName);
            }
            var containerTypes = query.ToList();
            return containerTypes;
        }

        public CTContainerTypeActual GetCTContainerTypeActualById(int id)
        {
            var actual = _context.CTContainerTypeActuals.Find(id);

            return actual;
        }

        public void AddSKUToCTContainerTypes(bool? overwrite)
        {
            List<CTContainerType> ctConainerTypes = null;

            var query = _context.CTContainerTypes.Include(x => x.CTContainerService).Where(x => true);

            if (overwrite == null || overwrite.Value == false)
            {
                query = query.Where(x => x.ProcurementReferenceId == null);
            }

            ctConainerTypes = query.ToList();

            if (ctConainerTypes != null && ctConainerTypes.Count > 0)
            {
                ctConainerTypes.ForEach(x =>
                {
                    x.ProcurementReferenceId = GenerateCTCaseContainerSKU(x);
                });
                _context.SaveChanges();
            }
        }

        private string GenerateCTCaseContainerSKU(CTContainerType ctContainerType)
        {
            var sku = ctContainerType.ProcurementReferenceId;
            //var abbre = ctContainerType.GCVendor.Abbreviation;
            //var per = Regex.Replace(ctContainerType.Name, @"\s+", "").Substring(0, 10);
            //var surgeryType = "K";
            //var exTrax = "E1";
            //sku = $"{abbre}-{per}-{surgeryType}-{surgeryType}-{exTrax}";
            return sku;
        }

        public List<GCCase> FilterCasesDashboard(DashboardModel filterModel)
        {
            return null;
        }
        public List<CTCaseContainer> GetCTCaseContainersByCaseId(int id)
        {
            var containers = _context.CTCaseContainers.Where(x => x.CTCaseId == id).ToList();

            return containers;
        }

        public List<GCCase> FilterCases(DashboardModel filterModel)
        {

            IQueryable<GCCase> cases = FilterCasesByUserRole(filterModel.User); // _context.GCCases.Include(x => x.CTCase);  
            if (filterModel.CaseReference != null && filterModel.CaseReference != String.Empty)
            {
                cases = cases.Where(x => x.Title.Contains(filterModel.CaseReference));
            }
            if (filterModel.Surgeon != null && filterModel.Surgeon != String.Empty)
            {
                cases = cases.Where(x => x.CTCase.CaseDoctor.Contains(filterModel.Surgeon));
            }
            if (filterModel.SelectedCustomers != null && filterModel.SelectedCustomers != String.Empty)
            {
                cases = cases.Where(x => x.GCCustomerId == Int32.Parse(filterModel.SelectedCustomers));
            }
            if (filterModel.SelectedVendors != null && filterModel.SelectedVendors != String.Empty)
            {
                cases = cases.Where(x => x.GCVendorId == Int32.Parse(filterModel.SelectedVendors));
            }
            if(filterModel.GetForSpecificTimeframe)
			{
                cases = cases.Where(x => x.DueTimestamp>= filterModel.StartDate && x.DueTimestamp<filterModel.EndDate);
            }

            return cases?.ToList() ?? new List<GCCase>();
        }
        public DashboardHourPreviewModel GetDashboardHourPreviewModel(DashboardModel filterModel)
		{
            DashboardHourPreviewModel model = new DashboardHourPreviewModel() { Items = new List<DashboardHourPreviewItemModel>()};
            var date = filterModel.StartDate;
            model.Date = date;
            var filterDateStart = date.TrimHoursMinutesSeconds();
            var filterDateEnd = date.TrimHoursMinutesSeconds().AddDays(1);
            var usersCases = FilterCases(filterModel);
            var cases = usersCases.Where(x => x.DueTimestamp >= filterDateStart && x.DueTimestamp < filterDateEnd).ToList(); //check if this cases need additional filtering by role or customer/vendor
            var dateStart = new DateTime(date.Year, date.Month, date.Day, 6, 0, 0);//start at 6AM
            var dateEnd = dateStart.AddHours(13); //end at 8-9pm
            var currentTime = dateStart; //start while from dateStart
            while(currentTime<dateEnd) //goal of this while loop is to have items from 6AM to 6PM no matter if we have cases for those times
			{
                DashboardHourPreviewItemModel item = new DashboardHourPreviewItemModel();
                item.Start = currentTime;
                item.End = currentTime.AddHours(1);
                if(cases!=null && cases.Count>0)
				{
                    var casesForCurrentTime = cases.Where(x => x.DueTimestamp >= item.Start && x.DueTimestamp < item.End).ToList(); //take all cases between timewindow of 1h
                    if (casesForCurrentTime != null && casesForCurrentTime.Count > 0)
                    {
                        foreach (var singleCase in casesForCurrentTime) //sort cases for color
                        {
                            var color = GetCaseColorStatusType(singleCase.Id);
                            if (color == Models.GroundControl.GCColorStatusType.Yellow)
                                item.NumOfYellowCases++;

                            else if (color == Models.GroundControl.GCColorStatusType.Red)
                                item.NumOfRedCases++;

                            else if(color == Models.GroundControl.GCColorStatusType.Purple)
                                item.NumOfPurpleCases++;

                            else if (color == Models.GroundControl.GCColorStatusType.Blue)
                                item.NumOfBlueCases++;

                            else if (color == Models.GroundControl.GCColorStatusType.Green)
                                item.NumOfGreenCases++;

                            else if (color == Models.GroundControl.GCColorStatusType.Orange)
                                item.NumOfOrangeCases++;

                            else if (color == Models.GroundControl.GCColorStatusType.Gray)
                                item.NumOfGrayCases++;
                        }

                    }
                }
                model.Items.Add(item);
                currentTime = item.End;
            }
            return model;
		}
        public string GetColorStatusClassByStatusType(Models.GroundControl.GCColorStatusType color)
		{
            if (color == Models.GroundControl.GCColorStatusType.Yellow)
                return "fc-status-5";

            else if (color == Models.GroundControl.GCColorStatusType.Red)
                return "fc-status-2";

            else if (color == Models.GroundControl.GCColorStatusType.Purple)
                return "fc-status-3";

            else if (color == Models.GroundControl.GCColorStatusType.Blue)
                return "fc-status-1";

            else if (color == Models.GroundControl.GCColorStatusType.Green)
                return "fc-status-6";

            else if (color == Models.GroundControl.GCColorStatusType.Orange)
                return "fc-status-4";

            else//this is for gray
                return "fc-status-7";
        }
        public DashboardDetailedHourPreviewModel GetDashboardDetailedHourPreviewModel(DashboardModel filterModel)
		{
            var date = filterModel.StartDate;
            var model = new DashboardDetailedHourPreviewModel() { Date = date, Items = new List<DashboardDetailedHourPreviewItemModel>()};
           
            var filterDateStart = date.TrimMinutesAndSeconds();
            var filterDateEnd = date.TrimMinutesAndSeconds().AddHours(1);
            var userCases = FilterCases(filterModel);
            var cases = userCases.Where(x => x.DueTimestamp >= filterDateStart && x.DueTimestamp < filterDateEnd).ToList(); //check if this cases need additional filtering by role or customer/vendor
            if (cases != null && cases.Count > 0)
			{
				foreach (var singleCase in cases)
				{
                    var color = GetCaseColorStatusType(singleCase.Id);
                    var colorClass = GetColorStatusClassByStatusType(color);

                    var item = new DashboardDetailedHourPreviewItemModel()
                    {
                        CaseId = singleCase.Id,
                        CaseReference = singleCase.Title,
                        ColorClass = colorClass,
                        DueTimestamp = singleCase.DueTimestamp,
                        CustomerName = singleCase.GCCustomer?.CustomerName ?? "No data"
                    };
                    model.Items.Add(item);
                }
            }
            return model;
        }

        public GCCaseModel GetDashboardCaseDetailsModel(int caseId)
		{
            var model = new GCCaseModel();
            var singleCase = _context.GCCases.Where(x => x.Id == caseId).Include(x => x.GCCustomer).Include(x => x.CTCase).FirstOrDefault();
            if(singleCase!=null)
			{
                model = _mapper.Map<GCCaseModel>(singleCase);
                var trays = GetTraysForGCCase(caseId, true);
                if(trays != null && trays.Count>0)
				{
                    StringBuilder sb = new StringBuilder("");
                    for (var k = 0; k < trays.Count; k++)
                    {
                        var caseContainer = GetCTCaseContainerById((int)trays[k].CTCaseContainerId);

                        if (k > 0)
                            sb.Append(", ");

                        sb.Append(caseContainer.ContainerName);

                    }
                    model.InstrumentTrays = sb.ToString();
                }
            }
            return model;
		}
        public List<GCCase> FilterCasesForCasePlanning(IEnumerable<GCCase> cases, string caseRef, int? vehicleId, int? driverId)
        {
            if (caseRef != null)
            {
                cases = cases.Where(x => x.Title.Contains(caseRef));
            }


            if (vehicleId != 0 && vehicleId != null)
            {
                var routeIds = _context.WWRoutes.Where(x => x.VehicleId == vehicleId).Select(x => x.Id).ToList();


                cases = cases.Where(x => routeIds.Contains(x.WWPickupRouteId == null ? 0 : (int)x.WWPickupRouteId));
            }
            if (driverId != 0 && driverId != null)
            {
                var routeIds = _context.WWRoutes.Where(x => x.DriverId == driverId).Select(x => x.Id).ToList();
                cases = cases.Where(x => routeIds.Contains(x.WWPickupRouteId == null ? 0 : (int)x.WWPickupRouteId));
            }

            return cases.ToList();
        }

        public IQueryable<GCCase> FilterCasesByUserRole(ClaimsPrincipal user)
        {
            var claims = user.Claims.ToList();
            //SetDueTimeForGCCases();
            IQueryable<GCCase> cases;

            if (user.IsInRole(ApplicationRoleNames.VendorUser) && user.IsInRole(ApplicationRoleNames.CustomerUser))
            {
                //VendorUser
                //var companiesIdsVendor = claims[3].Value.Split(',');

                var companiesIds = claims.Where(x => x.Type == "CompanyPermissionTypeView").FirstOrDefault().Value.Split(',');
                var companiesIdsList = companiesIds.ToList();

                var vendorsIds = _userService.GetChildCompanies().Where(x => companiesIdsList.Contains(x.Id.ToString())).Select(x => x.GCVendorId);

                cases = _context.GCCases.Where(x => vendorsIds.Contains(x.GCVendorId))
                                                    .Include(x => x.CTCase)
                                                    .Include(x => x.WWPickupRoute)
                                                    .Include(x => x.WWPickupRoute.Driver)
                                                    .Include(x => x.WWPickupRoute.Vehicle)
                                                    .Include(x => x.GCCustomer)
                                                    .AsQueryable();
                //CustomerUser
                //var companiesIdsCustomer = claims[3].Value.Split(',');
                //var companiesIdsListCustomer = companiesIdsCustomer.ToList();
                var childCompanies = _userService.GetChildCompanies().Where(x => companiesIdsList.Contains(x.Id.ToString())).ToList();
                var customersIds = childCompanies.Select(x => x.GCCustomerId);

                cases = _context.GCCases.Where(x => customersIds.Contains(x.GCCustomerId)).Include(x => x.CTCase).Include(x => x.GCCustomer).Include(x => x.WWPickupRoute).Include(x => x.WWPickupRoute.Driver).Include(x => x.WWPickupRoute.Vehicle).Include(x => x.GCCustomer).AsQueryable();
            }
            else
            {
                if (user.IsInRole(ApplicationRoleNames.VendorUser))
                {
                    //var companiesIds = claims[3].Value.Split(',');
                    var companiesIds = claims.Where(x => x.Type == "CompanyPermissionTypeView").FirstOrDefault()?.Value.Split(',');
                    var companiesIdsList = companiesIds?.ToList()??new List<string>();

                    var vendorsIds = _userService.GetChildCompanies().Where(x => companiesIdsList.Contains(x.Id.ToString())).Select(x => x.GCVendorId);

                    var vendorsIdsNotNull = new List<int?>();
                    foreach (var vendorId in vendorsIds)
                    {
                        if (vendorId != null)
                        {
                            vendorsIdsNotNull.Add(vendorId);
                        }
                    }

 

                    cases = _context.GCCases.Where(x => vendorsIdsNotNull.Contains(x.GCVendorId)).Include(x => x.CTCase).Include(x => x.GCCustomer).Include(x => x.WWPickupRoute).Include(x => x.WWPickupRoute.Driver).Include(x => x.WWPickupRoute.Vehicle).AsQueryable();
                }
                else if (user.IsInRole(ApplicationRoleNames.CustomerUser))
                {
                    //var companiesIds = claims[3].Value.Split(',');
                    var companiesIds = claims.Where(x => x.Type == "CompanyPermissionTypeView").FirstOrDefault()?.Value.Split(',');
                    var companiesIdsList = companiesIds?.ToList() ?? new List<string>();

                    var customersIds = _userService.GetChildCompanies().Where(x => companiesIdsList.Contains(x.Id.ToString())).Select(x => x.GCCustomerId);
                    //var customersIds = childCompanies.Select(x => x.GCCustomerId);
                    //customersIds.RemoveAll(null);
                    var customersIdsNotNull = new List<int?>();
                    foreach (var customerId in customersIds)
                    {
                        if (customerId != null)
                        {
                            customersIdsNotNull.Add(customerId);
                        }
                    }

                    cases = _context.GCCases.Where(x => customersIdsNotNull.Contains(x.GCCustomerId)).Include(x => x.CTCase).Include(x => x.GCCustomer).Include(x => x.WWPickupRoute).Include(x => x.WWPickupRoute.Driver).Include(x => x.WWPickupRoute.Vehicle).Include(x => x.GCCustomer).AsQueryable();
                }
                else
                {
                    cases = _context.GCCases.Include(x => x.CTCase).Include(x => x.GCCustomer).Include(x => x.WWPickupRoute).Include(x => x.WWPickupRoute.Driver).Include(x => x.WWPickupRoute.Vehicle);
                }
            }

            return cases;
        }

        public IQueryable<GCCase> FilterCasesByUserRoleForTrayInventory(ClaimsPrincipal user)
        {
            var claims = user.Claims.ToList();

            IQueryable<GCCase> cases;
            if (user.IsInRole(ApplicationRoleNames.SuperAdmin) || user.IsInRole(ApplicationRoleNames.Admin) || user.IsInRole(ApplicationRoleNames.User))
            {

            }
            if (user.IsInRole(ApplicationRoleNames.VendorUser) && user.IsInRole(ApplicationRoleNames.CustomerUser))
            {
                //VendorUser
                //var companiesIdsVendor = claims[3].Value.Split(',');
                var companiesIds = claims.Where(x => x.Type == "CompanyPermissionTypeView").FirstOrDefault()?.Value.Split(',');
                var companiesIdsList = companiesIds?.ToList() ?? new List<string>();

                var vendorsIds = _userService.GetChildCompanies().Where(x => companiesIdsList.Contains(x.Id.ToString())).Select(x => x.GCVendorId);

                cases = _context.GCCases.Where(x => vendorsIds.Contains(x.GCVendorId))
                                                    .Include(x => x.CTCase)
                                                    .Include(x => x.WWPickupRoute)
                                                    .Include(x => x.WWPickupRoute.Driver)
                                                    .Include(x => x.WWPickupRoute.Vehicle)
                                                    .Include(x => x.GCCustomer)
                                                    .AsQueryable();
                //CustomerUser
                //var companiesIdsCustomer = claims[3].Value.Split(',');
                //var companiesIdsListCustomer = companiesIdsCustomer.ToList();
                var childCompanies = _userService.GetChildCompanies().Where(x => companiesIdsList.Contains(x.Id.ToString())).ToList();
                var customersIds = childCompanies.Select(x => x.GCCustomerId);

                cases = _context.GCCases.Where(x => customersIds.Contains(x.GCCustomerId)).Include(x => x.CTCase).Include(x => x.GCCustomer).Include(x => x.WWPickupRoute).Include(x => x.WWPickupRoute.Driver).Include(x => x.WWPickupRoute.Vehicle).Include(x => x.GCCustomer).AsQueryable();
            }
            else
            {
                if (user.IsInRole(ApplicationRoleNames.VendorUser))
                {
                    //var companiesIds = claims[3].Value.Split(',');
                    var companiesIds = claims.Where(x => x.Type == "CompanyPermissionTypeView").FirstOrDefault()?.Value.Split(',');
                    var companiesIdsList = companiesIds?.ToList() ?? new List<string>();

                    var vendorsIds = _userService.GetChildCompanies().Where(x => companiesIdsList.Contains(x.Id.ToString())).Select(x => x.GCVendorId);

                    var vendorsIdsNotNull = new List<int?>();
                    foreach (var vendorId in vendorsIds)
                    {
                        if (vendorId != null)
                        {
                            vendorsIdsNotNull.Add(vendorId);
                        }
                    }

                    //var vendorsNames = _context.GCVendors.Where(x => vendorsIdsNotNull.Contains(x.Id)).Select(x => x.Title);
                    //var containerTypesIds = _context.CTContainerTypes.Where(x => vendorsNames.Contains(x.VendorName)).Select(x => x.Id);
                    //var trays = new List<GCTray>();
                    //foreach (var containerTypeId in containerTypesIds)
                    //{
                    //    var tray = _context.GCTrays.Where(x => x.CTContainerTypeId == containerTypeId).FirstOrDefault();

                    //    if (tray != null)
                    //    {
                    //        trays.Add(tray);
                    //    }
                    //}
                    //var casesIds = trays.Select(x => x.GCCaseId).Distinct();

                    //cases = _context.GCCases.Where(x => casesIds.Contains(x.Id)).Include(x => x.CTCase).Include(x => x.GCCustomer).Include(x => x.WWPickupRoute).Include(x => x.WWPickupRoute.Driver).Include(x => x.WWPickupRoute.Vehicle).AsQueryable();

                    cases = _context.GCCases.Where(x => vendorsIdsNotNull.Contains(x.GCVendorId)).Include(x => x.CTCase).Include(x => x.GCCustomer).Include(x => x.WWPickupRoute).Include(x => x.WWPickupRoute.Driver).Include(x => x.WWPickupRoute.Vehicle).AsQueryable();
                }
                else if (user.IsInRole(ApplicationRoleNames.CustomerUser))
                {
                    //var companiesIds = claims[3].Value.Split(',');
                    var companiesIds = claims.Where(x => x.Type == "CompanyPermissionTypeView").FirstOrDefault()?.Value.Split(',');
                    var companiesIdsList = companiesIds?.ToList() ?? new List<string>();

                    var customersIds = _userService.GetChildCompanies().Where(x => companiesIdsList.Contains(x.Id.ToString())).Select(x => x.GCCustomerId);
                    //var customersIds = childCompanies.Select(x => x.GCCustomerId);
                    //customersIds.RemoveAll(null);
                    var customersIdsNotNull = new List<int?>();
                    foreach (var customerId in customersIds)
                    {
                        if (customerId != null)
                        {
                            customersIdsNotNull.Add(customerId);
                        }
                    }

                    cases = _context.GCCases.Where(x => customersIdsNotNull.Contains(x.GCCustomerId)).Include(x => x.CTCase).Include(x => x.GCCustomer).Include(x => x.WWPickupRoute).Include(x => x.WWPickupRoute.Driver).Include(x => x.WWPickupRoute.Vehicle).Include(x => x.GCCustomer).AsQueryable();
                }
                else
                {
                    cases = _context.GCCases.Include(x => x.CTCase).Include(x => x.GCCustomer).Include(x => x.WWPickupRoute).Include(x => x.WWPickupRoute.Driver).Include(x => x.WWPickupRoute.Vehicle);
                }
            }

            return cases;
        }

        public GCLifecycleStatusType GetActiveLifecycleStatusForCase(int caseId)
        {
            return WorkWaveClient.GetActiveLifecycleStatusForCase(caseId);

        }

        public List<GCCase> FilterCasesList(GCCasesModel filterModel)
        {
            IQueryable<GCCase> cases = FilterCasesByUserRole(filterModel.User);
            if (filterModel.DateFrom != null)
            {
                cases = cases.Where(x => x.DueTimestamp>=filterModel.DateFrom);
            }
            if (filterModel.DateTo != null)
            {
                cases = cases.Where(x => x.DueTimestamp <= filterModel.DateTo);
            }
            if (filterModel.CaseReference != null)
            {
                cases = cases.Where(x => x.Title.Contains(filterModel.CaseReference));
            }
            if (filterModel.Surgeon != null)
            {
                cases = cases.Where(x => x.CTCase.CaseDoctor.Contains(filterModel.Surgeon));
            }
            if (filterModel.SelectedCustomer != null)
            {
                cases = cases.Where(x => x.GCCustomerId == Int32.Parse(filterModel.SelectedCustomer));
            }
            if (filterModel.SelectedVendor != null)
            {
                cases = cases.Where(x => x.GCVendorId == Int32.Parse(filterModel.SelectedVendor));
            }

            return cases.ToList();

        }

        public CaseScheduleColorType GetCaseScheduleColorType(int caseId)
        {
            var color = GetCaseColorStatusType(caseId);
            if(color == GCColorStatusType.Red)
			{
                return CaseScheduleColorType.Red;
            }
            var deliveredStatus = _context.GCLifecycleStatusTypes.Where(x => x.Code == (int)LifecycleStatusType.Delivered).FirstOrDefault();
            if(deliveredStatus!=null)
			{
                var caseStatuses = _context.GCCasesLifecycleStatusTypes.Where(x => x.GCCaseId == caseId && x.LifecycleStatusTypeId == deliveredStatus.Id).FirstOrDefault();
                if (caseStatuses != null)
                {
                    return CaseScheduleColorType.Green;
                }
                else
                {
                    return CaseScheduleColorType.Yellow;
                }
            }
            else
			{
                return CaseScheduleColorType.Yellow;
            }
           
        }


        //      public async Task<WorkWaveResponse> BuildRoutes(DateTime from, DateTime to)
        //{
        //          string territoryId = "0";
        //          var result = await WorkWaveClient.BuildRoutes(territoryId, from,to);
        //          return result;
        //      }

        public List<GCCustomer> GetAllGCCustomers(bool? activeOnly = false)
        {
            // 'WHERE' clause is added to be able to generate query as IQueriable instead of DBSet
            var query = _context.GCCustomers.Where(x => x.Id > 0);
            if (activeOnly.HasValue && activeOnly.Value)
            {
                query = query.Where(x => x.Active == true);
            }
            var customers = query.ToList();

            return customers;
        }

        public List<GCVendor> GetVendors()
        {
            var vendors = _context.GCVendors.ToList();

            return vendors;
        }

        public GCVendor GetGCVendor(int vendorId)
        {
            return _context.GCVendors.Find(vendorId);
        }

        public IEnumerable<SelectListItem> GetGCCustomersSelectList()
        {
            var customers = _context.GCCustomers.ToList();

            var customersList = customers.Select(customer => new SelectListItem
            {
                Text = customer.CustomerName,
                Value = customer.Id.ToString()
            });

            return customersList;
        }
        public IEnumerable<SelectListItem> GetGCCustomersFilteredSelectList(ClaimsPrincipal user)
		{
            if (user.IsInRole(ApplicationRoleNames.CustomerUser) || user.IsInRole(ApplicationRoleNames.VendorUser))
            {

                var companiesIds = user.Claims.Where(x => x.Type == "CompanyPermissionTypeView").FirstOrDefault()?.Value?.Split(',');
                if (companiesIds != null && companiesIds.Length > 0)
                {
                    var companiesIdsList = companiesIds.ToList();

                    var customersIds = _userService.GetChildCompanies().Where(x => companiesIdsList.Contains(x.Id.ToString())).Select(x => x.GCCustomerId);
                    var customers = _context.GCCustomers.Where(x => customersIds.Contains(x.Id)).ToList();

                    var customersList = customers.Select(customer => new SelectListItem
                    {
                        Text = customer.CustomerName,
                        Value = customer.Id.ToString()
                    });

                    return customersList;
                }
                return new List<SelectListItem>();
            }
            else
			{
                var result = GetGCCustomersSelectList();
                return result;
            }
        }

        public IEnumerable<SelectListItem> GetGCVendorsFilteredSelectList(ClaimsPrincipal user)
        {
            if(user.IsInRole(ApplicationRoleNames.CustomerUser) || user.IsInRole(ApplicationRoleNames.VendorUser))
			{
                var companiesIds = user.Claims.Where(x => x.Type == "CompanyPermissionTypeView").FirstOrDefault()?.Value?.Split(',');
                if (companiesIds != null && companiesIds.Length > 0)
                {
                    var companiesIdsList = companiesIds.ToList();

                    var vendorIds = _userService.GetChildCompanies().Where(x => companiesIdsList.Contains(x.Id.ToString())).Select(x => x.GCVendorId);
                    var vendors = _context.GCVendors.Where(x => vendorIds.Contains(x.Id)).ToList();

                    var vendorsList = vendors.Select(vendor => new SelectListItem
                    {
                        Text = vendor.Title,
                        Value = vendor.Id.ToString()
                    });

                    return vendorsList;
                }
                return new List<SelectListItem>();
            }
            else
			{
                var result = GetGCVendorsSelectList();
                return result;
			}
            
        }
        public IEnumerable<SelectListItem> GetWWVehicleSelectList()
        {
            var vehicles = _context.WWVehicles.ToList();

            var vehiclesList = vehicles.Select(vehicle => new SelectListItem
            {
                Text = vehicle.ExternalId,
                Value = vehicle.Id.ToString()
            });

            return vehiclesList;
        }

        public IEnumerable<SelectListItem> GetWWDriverSelectList()
        {
            var drivers = _context.WWDrivers.ToList();

            var driversList = drivers.Select(driver => new SelectListItem
            {
                Text = driver.Name,
                Value = driver.Id.ToString()
            });

            return driversList;
        }

        public IEnumerable<SelectListItem> GetGCCustomersSelectListByDate(DateTime date)
        {
            var cases = _context.GCCases
                .Include(x => x.CTCase)
                .Include(x => x.GCCustomer)
                .ToList();

            var todaysCases = cases.Where(x => x.CTCase.DueTimestamp.TrimHoursMinutesSeconds() == date.TrimHoursMinutesSeconds()).ToList();

            List<GCCustomer> customers = new List<GCCustomer>();
            foreach (var item in todaysCases)
            {
                var customer = _context.GCCustomers.Where(x => x.Id == item.GCCustomerId).FirstOrDefault();

                if (customer != null)
                {
                    if (!customers.Contains(customer))
                        customers.Add(customer);
                }
            }

            //var customers = _context.GCCustomers.ToList();

            var customersList = customers.Select(customer => new SelectListItem
            {
                Text = customer.CustomerName,
                Value = customer.Id.ToString()
            });

            return customersList;
        }

        public IEnumerable<SelectListItem> GetGCVendorsSelectList()
        {
            var vendors = _context.GCVendors.ToList();

            var vendorsList = vendors.Select(vendor => new SelectListItem
            {
                Text = vendor.Title,
                Value = vendor.Id.ToString()
            });

            return vendorsList;
        }

        public GCCustomer GetCustomer(int customerId)
        {
            return _context.GCCustomers.Find(customerId);
        }

        public void UpdateCustomer(UpdateCustomerModel model, bool? editChildCompany = true)
        {
            var customer = _context.GCCustomers.Find(model.Customer.Id);

            model.Customer.SelectedDeliveryStart = DateTime.Parse(model.Customer.DeliveryStart);
            model.Customer.SelectedDeliveryEnd = DateTime.Parse(model.Customer.DeliveryEnd);

            //model.Customer.SelectedDeliveryStart = DateTime.Parse(model.Customer.DeliveryStart);
            //model.Customer.SelectedDeliveryEnd = DateTime.Parse(model.Customer.DeliveryEnd);

            var secondsEnd = Converters.ConvertDateTimeToSeconds(model.Customer.SelectedDeliveryEnd);
            var secondsStart = Converters.ConvertDateTimeToSeconds(model.Customer.SelectedDeliveryStart);

            bool isActive = customer.Active;
            _mapper.Map(model.Customer, customer);

            customer.Active = isActive;

            customer.DeliveryEnd = secondsEnd;
            customer.DeliveryStart = secondsStart;
            customer.ServiceTime = model.Customer.ServiceTime * 60;

            _context.GCCustomers.Update(customer);
            _context.SaveChanges();

            if (editChildCompany.HasValue && editChildCompany.Value)
            {
                var company = _userService.GetChildCompanies().Where(x => x.GCCustomerId == customer.Id).FirstOrDefault();

                if (company != null)
                {
                    company.Title = model.Customer.CustomerName;
                    //company.GCParentCompanyId = model.Customer.SelectedCompany;

                    var companyModel = _mapper.Map<ChildCompanyModel>(company);
                    companyModel.ParentCompanyId = model.Customer.SelectedCompany;

                    _userService.EditChildCompany(companyModel);
                }
                else
				{
                    ChildCompanyModel newcompany = new ChildCompanyModel();
                    newcompany.Title = model.Customer.CustomerName;
                    newcompany.ParentCompanyId = model.Customer.SelectedCompany;
                    newcompany.SelectedCustomer = customer.Id.ToString();

                    _userService.AddChildCompany(newcompany);
                }
            }
        }

        public void UpdateVendor(UpdateVendorModel model, bool? editChildCompany = true)
        {
            var vendor = _context.GCVendors.Find(model.Vendor.Id);

            _mapper.Map(model.Vendor, vendor);

            vendor.Active = true;

            _context.GCVendors.Update(vendor);
            _context.SaveChanges();

            if (editChildCompany.HasValue && editChildCompany.Value)
            {
                var company = _userService.GetChildCompanies().Where(x => x.GCVendorId == vendor.Id).FirstOrDefault();
                if (company != null)
                {
                    company.Title = model.Vendor.Title;

                    var companyModel = _mapper.Map<ChildCompanyModel>(company);
                    companyModel.ParentCompanyId = model.Vendor.SelectedCompany;

                    _userService.EditChildCompany(companyModel);
                }
                else
				{
                    ChildCompanyModel newcompany = new ChildCompanyModel();
                    newcompany.Title = model.Vendor.Title;
                    newcompany.ParentCompanyId = model.Vendor.SelectedCompany;
                    newcompany.SelectedVendor = vendor.Id.ToString();

                    _userService.AddChildCompany(newcompany);
                }
            }
        }

        public void AddCustomer(AddCustomerModel model)
        {
            //TODO: Refactor faciliy part
            //model.Customer.FacilityCode = "TST";
            model.Customer.FacilityId = "1";

            model.Customer.SelectedDeliveryEnd = DateTime.Parse(model.Customer.DeliveryEnd);
            model.Customer.SelectedDeliveryStart = DateTime.Parse(model.Customer.DeliveryStart);

            var secondsEnd = Converters.ConvertDateTimeToSeconds(model.Customer.SelectedDeliveryEnd);
            var secondsStart = Converters.ConvertDateTimeToSeconds(model.Customer.SelectedDeliveryStart);

            var customer = _mapper.Map<GCCustomer>(model.Customer);

            customer.DeliveryStart = secondsStart;
            customer.DeliveryEnd = secondsEnd;
            customer.Active = true;
            customer.ServiceTime = model.Customer.ServiceTime * 60;

            _context.GCCustomers.Add(customer);
            _context.SaveChanges();

            ChildCompanyModel company = new ChildCompanyModel();
            company.Title = model.Customer.CustomerName;
            company.ParentCompanyId = model.Customer.SelectedCompany;
            company.SelectedCustomer = customer.Id.ToString();

            _userService.AddChildCompany(company);
        }

        public void AddVendor(AddVendorModel model)
        {
            var vendor = _mapper.Map<GCVendor>(model.Vendor);
            vendor.Active = true;

            _context.GCVendors.Add(vendor);
            _context.SaveChanges();

            ChildCompanyModel company = new ChildCompanyModel();
            company.Title = model.Vendor.Title;
            company.ParentCompanyId = model.Vendor.SelectedCompany;
            company.SelectedVendor = vendor.Id.ToString();

            _userService.AddChildCompany(company);
        }

        public void DeactivateCustomer(GCCustomerModel model)
        {
            var customer = _context.GCCustomers.Find(model.Id);
            customer.Active = !customer.Active;

            _context.GCCustomers.Update(customer);
            _context.SaveChanges();
        }

        public void DeactivateVendor(GCVendorModel model)
        {
            var vendor = _context.GCVendors.Find(model.Id);
            vendor.Active = !vendor.Active;

            _context.GCVendors.Update(vendor);
            _context.SaveChanges();
        }

        public ETLLogs GetLatestCTETLLogCycle(bool? lastSuccessful = null)
        {
            var query = _context.ETLLogs.Where(x => x.Source == Helpers.Constants.ETL_CT_MODULE_NAME && x.ActionType.Value == (int)Enums.CTActionType.GetCTCasesContainersAndSubmitToWW);

            if (lastSuccessful.HasValue && lastSuccessful.Value)
            {
                query = query.Where(x => x.Status == Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE);
            }

            query = query.OrderByDescending(x => x.ProcessingTime);

            return query.FirstOrDefault();
        }

        public void GetLatestAndLatestSuccessfulETLLogs()
        {
            var etlLogs = _context.ETLLogs
                .Where(x => x.Source == Helpers.Constants.ETL_CT_MODULE_NAME && x.ActionType.Value == (int)Enums.CTActionType.GetCTCasesContainersAndSubmitToWW)
                .GroupBy(t => true)
                .Select(t => new
                {
                    LatestTime = t.Max(q => q.ProcessingTime),
                    LatestSuccessfulTime = t
                        .Where(q => q.Status == Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE)
                        .Max(q => q.ProcessingTime),
                    Transactions = t
                });

            var logs = etlLogs.Select(t => new
            {
                Latest = t.Transactions.FirstOrDefault(q => q.ProcessingTime == t.LatestTime),
                LatestSuccessful = t.Transactions.FirstOrDefault(q => q.ProcessingTime == t.LatestSuccessfulTime)
            })
                .AsNoTracking()
                .FirstOrDefault();

            //LatestETLLogsModel etLLogsModel = new LatestETLLogsModel();

            //var latestLog = etlLogs.OrderByDescending(x => x.ProcessingTime).FirstOrDefault();
            //etLLogsModel.LatestETLLog = mapper.Map<ETLLogModel>(latestLog);

            //var latestSuccessfulLog = etlLogs.Where(x => x.Status == Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE).OrderByDescending(x => x.ProcessingTime).FirstOrDefault();
            //etLLogsModel.LatestSuccessfulETLLog = mapper.Map<ETLLogModel>(latestSuccessfulLog);

            //return etLLogsModel;
        }

        public ETLLogs GetLatestSuccessfulllETLLogsWWOrder()
        {
            var query = _context.ETLLogs.Where(x => x.Source == Helpers.Constants.ETL_WW_MODULE_NAME && x.ActionType.Value == (int)Enums.WWActionType.SyncOrder);
            query = query.Where(x => x.Message == Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE);
            query = query.OrderByDescending(x => x.ProcessingTime);

            return query.FirstOrDefault();
        }

        public ETLLogs GetLatestSuccessfulllETLLogsWWRoute()
        {
            var query = _context.ETLLogs.Where(x => x.Source == Helpers.Constants.ETL_WW_MODULE_NAME && x.ActionType.Value == (int)Enums.WWActionType.SyncRoutes);
            query = query.Where(x => x.Message == Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE);
            query = query.OrderByDescending(x => x.ProcessingTime);

            return query.FirstOrDefault();
        }

        public void UpdateGCCaseLifecycleStatus(int gcCaseId)
        {
            _groundControlService.UpdateGCCaseLifecycleStatus(gcCaseId);
        }

        public GCCase GetCaseById(int id)
        {
            var result = _context.GCCases
                .Include(x => x.CTCase)
                .Include(x => x.GCCustomer)
                .Include(x => x.GCTrays).ThenInclude(x => x.CTContainerType)
                .Include(x => x.GCTrays).ThenInclude(x => x.CTCaseContainer)
                .Where(x => x.Id == id).FirstOrDefault();
            return result;
        }

        #region POSettings

        public POSetting GetPOSetting(POSettingsType type)
        {
            return _groundControlService.GetPOSetting(type);
        }

        public void AddOrUpdatePOSetting(POSettingsType type, string value, string user)
        {
            _groundControlService.AddOrUpdatePOSetting(type, value, user);
        }

        public bool RemovePOSetting(POSettingsType type)
        {
            return _groundControlService.RemovePOSetting(type);
        }

        #endregion POSettings

        #endregion

        public async Task SyncRoutesWithWorkWaveAsync(DateTime? date = null)
        {
            await WorkWaveClient.GetGPSDevicesCurrentPos();
            //await WorkWaveClient.SyncRoutesWithWorkWaveAsync(date);
            //await WorkWaveClient.SyncImagesWithWorkWave();
        }

        public async Task<string> SyncOrdersWithWorkWaveAsync()
        {
            var result = await WorkWaveService.SyncOrdersWithWorkWaveAsync();
            return result;
            //await WorkWaveClient.GetOrdersFromWorWaveAsync();

        }

        public async Task SyncImagesWithWorkWaveAsync()
        {
            await WorkWaveClient.SyncImagesWithWorkWave();
        }

        public List<LifecycleCategory> GetCaseLifecyceCategories(int caseId, bool getForPreviousHistory = false, bool isLoaner = false)
        {

            var model = new List<LifecycleCategory>();
            var categories = new List<GCLifecycleStatusTypeCategory>();
            if (isLoaner) //for loaners get all categories (from loaner inbound to loaner outbound) except Stage category
            {
                categories = _context.GCLifecycleStatusTypeCategories.Where(x => x.Title != "Stage").OrderBy(x => x.Order).ToList();
            }
            else if (getForPreviousHistory) //for previous history take Decontaminate,Inspect,Stage eg. Order >=5
            {
                categories = _context.GCLifecycleStatusTypeCategories.Where(x => x.Order >= 4 && x.Order < 8).OrderBy(x => x.Order).ToList(); //order for display purposes
            }
            else //for current status take Sterilize,Deliver,Service,Recover rg. Order < 5
            {
                categories = _context.GCLifecycleStatusTypeCategories.Where(x => x.Order < 5 && x.Order > 0).OrderBy(x => x.Order).ToList(); //order for display purposes
            }

            var categoryIds = categories.Select(x => x.Id).ToList();
            var statuses = _context.GCLifecycleStatusTypes.Where(x => categoryIds.Contains(x.GCLifecycleStatusTypeCategoryId)).OrderBy(x => x.Order).ToList().GroupBy(x => x.GCLifecycleStatusTypeCategoryId); //Group statuses by category
            var trays = new List<GCTray>();
            if (getForPreviousHistory)
            {
                //get gctrays for previous cases
                var gcTrays = _context.GCTrays.Where(x => x.GCCaseId == caseId).Include(x => x.CTCaseContainer).ToList();
                var gcCase = _context.GCCases.Where(x => x.Id == caseId).Include(x => x.CTCase).FirstOrDefault();
                foreach (var tray in gcTrays)
                {
                    //select all gctrays with coresponding cases for current actual
                    var traysWithCases = _context.GCTrays.Where(x => x.CTContainerTypeActualId != null && x.CTContainerTypeActualId == tray.CTContainerTypeActualId && x.GCCaseId != null).Include(x => x.GCCase).Include(x => x.GCCase.CTCase).ToList();
                    //filter only those cases that are before current actual and order them by descending
                    traysWithCases = traysWithCases.Where(x => x.GCCase.CTCase.DueTimestamp < gcCase.CTCase.DueTimestamp).OrderByDescending(x => x.GCCase.CTCase.DueTimestamp).ToList();
                    //there should always be at least one tray and that is current tray
                    if (traysWithCases != null && traysWithCases.Count > 0)//if we find at least one tray in history, add it to trays for processing
                    {
                        trays.Add(traysWithCases.First());
                    }
                    else //if we do not find any case history for specific tray, check if tray has induction
                    {
                        var inductionTray = _context.GCTrays.Where(x => x.CTContainerTypeActualId != null && x.CTContainerTypeActualId == tray.CTContainerTypeActualId && x.GCCaseId == null).Include(x => x.GCCase).Include(x => x.GCCase.CTCase).OrderByDescending(x => x.GCCase.CTCase.DueTimestamp).FirstOrDefault();
                        if (inductionTray != null)//if we find inducton tray, add it to trays for processing
                        {
                            trays.Add(inductionTray);
                        }
                    }

                }
            }
            else
            {
                trays = _context.GCTrays.Where(x => x.GCCaseId == caseId).Include(x => x.CTCaseContainer).ToList();
            }

            var trayIds = trays.Select(y => y.Id).ToList();

            foreach (var statusGroup in statuses) //foreach each status category
            {
                var currentCat = categories.Where(x => x.Id == statusGroup.Key).FirstOrDefault();
                if (currentCat != null)
                {
                    var percentageCounter = 0; //used to calculate number of trays in current category
                    var isTraysInStatus = false; //used for determing if timestamp should be shown
                    var category = new LifecycleCategory() { Id = currentCat.Id, Title = currentCat.Title, Order = currentCat.Order, LifecycleStatuses = new List<LifecycleStatusModel>() };
                    var lifeCycleStatuses = _context.GCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeCategoryId == currentCat.Id && x.Code != (int)LifecycleStatusType.QualityHold).OrderBy(x => x.Order).ToList(); //******** other condition is to remove quarantine status from displaying on view
                    foreach (var lifecycleStatus in lifeCycleStatuses)
                    {
                        var lifecycleStatusModel = new LifecycleStatusModel() { Title = lifecycleStatus.Title, Order = lifecycleStatus.Order, Id = lifecycleStatus.Id, TrayLifecycleStatuses = new List<TrayLifecycleStatus>() };
                        var trayStatuses = _context.GCTraysLifecycleStatusTypes.Where(x => trayIds.Contains(x.GCTrayId) && x.LifecycleStatusTypeId == lifecycleStatus.Id).Include(x => x.GCTray).Include(x => x.GCTray.CTCaseContainer).ToList();
                        lifecycleStatusModel.NumberOfTraysInStatus = trayStatuses.Count;
                        foreach (var tray in trays)
                        {
                            var title = "No Data";
                            if (tray.CTCaseContainer != null && tray.CTCaseContainer.ContainerName != null)
                            {
                                title = tray.CTCaseContainer.ContainerName;
                            }
                            else if (tray.CTContainerTypeActual != null && tray.CTContainerTypeActual.ParentName != null && tray.CTContainerTypeActual.Name != null)
                            {
                                title = $"{tray.CTContainerTypeActual.ParentName} {tray.CTContainerTypeActual.Name}";
                            }

                            var trayStatus = trayStatuses.Where(x => x.GCTrayId == tray.Id).FirstOrDefault();
                            if (trayStatus != null)
                            {
                                var trayLifecycleStatus = new TrayLifecycleStatus() { Title = title, Timestamp = trayStatus.Timestamp, IsFinished = true, User = trayStatus.User };
                                lifecycleStatusModel.TrayLifecycleStatuses.Add(trayLifecycleStatus);
                                percentageCounter++;
                                isTraysInStatus = true;
                            }
                            else
                            {
                                var trayLifecycleStatus = new TrayLifecycleStatus() { Title = title };
                                lifecycleStatusModel.TrayLifecycleStatuses.Add(trayLifecycleStatus);

                            }


                        }
                        if (isTraysInStatus) //if there is atleast one tray in status, calculate timestamp for entire status as latest tray status timestamp
                        {
                            lifecycleStatusModel.ShowTimestamp = true;
                            lifecycleStatusModel.Timestamp = lifecycleStatusModel.TrayLifecycleStatuses.OrderByDescending(t => t.Timestamp).Select(x => x.Timestamp).FirstOrDefault();
                            isTraysInStatus = false; //reset flag
                        }
                        else
                        {
                            lifecycleStatusModel.ShowTimestamp = false;
                        }
                        category.LifecycleStatuses.Add(lifecycleStatusModel);
                    }
                    //calculate percentage
                    float totalNumOfTrays = lifeCycleStatuses.Count * trays.Count; //total number of trays in category
                    if (totalNumOfTrays > 0)
                    {
                        int percentage = (int)Math.Round((percentageCounter / totalNumOfTrays) * 100);
                        category.Percentage = percentage;
                    }
                    else
                    {
                        category.Percentage = 100;
                    }
                    category.IsFinished = category.Percentage == 100 ? true : false;
                    category.IsDisabled = currentCat.IsDisabledInTimeline != null ? (bool)currentCat.IsDisabledInTimeline : false;
                    model.Add(category);
                }

            }
            return model;
        }

        public string GetCaseColorStatus(int gcCaseId)
        {
            var result = Models.GroundControl.GCColorStatusType.Red.ToString();
            var activeColorStatus = _context.GCCaseColorStatuses.Where(x => x.GCCaseId == gcCaseId && x.IsActiveStatus == true).Include(x => x.GCColorStatusType).FirstOrDefault();

            if (activeColorStatus != null)
            {
                result = ((Models.GroundControl.GCColorStatusType)activeColorStatus.GCColorStatusType.Id).ToString();
            }

            return result;
        }

        public GCColorStatusType GetCaseColorStatusType(int gcCaseId)
        {
            var result = Models.GroundControl.GCColorStatusType.Red;
            var activeColorStatus = _context.GCCaseColorStatuses.Where(x => x.GCCaseId == gcCaseId && x.IsActiveStatus == true).Include(x => x.GCColorStatusType).FirstOrDefault();

            if (activeColorStatus != null)
            {
                result = (Models.GroundControl.GCColorStatusType)activeColorStatus.GCColorStatusType.Id;
            }

            return result;
        }

        public string GetTrayColorStatus(int gcTrayId)
        {
            var result = Models.GroundControl.GCColorStatusType.Red.ToString();
            var activeColorStatus = _context.GCTrayColorStatuses.Where(x => x.GCTrayId == gcTrayId && x.IsActiveStatus == true).Include(x => x.GCColorStatusType).FirstOrDefault();

            if (activeColorStatus != null)
            {
                result = ((Models.GroundControl.GCColorStatusType)activeColorStatus.GCColorStatusType.Id).ToString();
            }

            return result;
        }

        public List<Framework.GCColorStatusType> GetCaseColorStatusTypes()
        {
            var result = _context.GCColorStatusTypes.ToList();
            return result;
        }

        public void UpdateGCCaseColorStatus(int gcCaseId)
        {
            _groundControlService.UpdateGCCaseColorStatus(gcCaseId);
        }

        public List<NoteGroup> GetCaseNotes(int caseId)
        {
            var noteGroups = new List<NoteGroup>();
            var trays = _context.GCTrays.Where(x => x.GCCaseId == caseId).Include(x => x.CTCaseContainer).ToList();
            if (trays != null && trays.Count > 0)
            {
                var trayIds = trays.Select(x => x.Id).ToList();
                var trayColorStatuses = _context.GCTrayColorStatuses.Where(x => trayIds.Contains(x.GCTrayId)).Include(x => x.GCColorStatusType).ToList();
                if (trayColorStatuses != null && trayColorStatuses.Count > 0)
                {
                    var trayColorStatusIds = trayColorStatuses.Select(x => x.Id).ToList();
                    var statusNotes = _context.GCTrayColorStatusNotes.Include(x => x.GCTrayColorStatus).Where(x => trayColorStatusIds.Contains(x.GCTrayColorStatusId)).ToList();
                    if (statusNotes != null && statusNotes.Count > 0)
                    {
                        var groupNotes = statusNotes.GroupBy(x => x.CreatedAtUtc.TrimHoursMinutesSeconds());

                        foreach (var group in groupNotes)
                        {
                            var noteGroupitem = new NoteGroup() { Date = group.Key, Notes = new List<Note>() };
                            var maxValue = group.OrderByDescending(x => x.CreatedAtUtc).FirstOrDefault();
                            foreach (var note in group)
                            {
                                var noteModel = new Note()
                                {
                                    Id = note.Id,
                                    Description = note.Description,
                                    Timestamp = note.CreatedAtUtc,
                                    Title = note.Title,
                                    RequiresResolution = note.RequiresResolution,
                                    IsSystemGenerated = note.Type == 1 ? true : false,
                                    ResolvedAtUtc = note.ResolvedAtUtc,
                                    ColorStatusId = note.GCTrayColorStatusId,
                                    IsColorStatusLocked =
                                        maxValue != null &&
                                        maxValue.CreatedAtUtc == note.CreatedAtUtc &&
                                        note.GCTrayColorStatus.IsLocked.HasValue &&
                                        note.GCTrayColorStatus.IsLocked.Value,
                                    CreatedBy = note.CreatedBy
                                };
                                if(noteModel.IsSystemGenerated == false) //if there is atleast one user comment, group should be shown as user group
								{
                                    noteGroupitem.HasUserNotes = true;

                                }

                                var tcs = trayColorStatuses.Where(x => x.Id == note.GCTrayColorStatusId).FirstOrDefault();
                                if (tcs != null)
                                {
                                    noteModel.Color = tcs.GCColorStatusType.ColorHexCode;
                                    var tray = trays.Where(x => x.Id == tcs.GCTrayId).FirstOrDefault();
                                    if (tray != null)
                                    {
                                        noteModel.TrayTitle = tray.CTCaseContainer.ContainerName;
                                    }
                                }
                                noteGroupitem.Notes.Add(noteModel);

                            }
                            noteGroupitem.Notes.OrderByDescending(x => x.Timestamp).ToList();
                            noteGroups.Add(noteGroupitem);
                        }
                    }
                }

            }


            return noteGroups;
        }

        public async Task ResolveGCTrayColorStatusNote(int noteId)
        {
            try
            {
                var note = await _context.GCTrayColorStatusNotes.Where(x => x.Id == noteId).FirstOrDefaultAsync();
                if (note != null)
                {
                    note.ResolvedAtUtc = DateTime.UtcNow;
                    await _context.SaveChangesAsync();
                }
            }
            catch
            {

            }
        }

        public async Task UnlockGCTrayColorStatus(int statusId)
        {
            try
            {
                var status = await _context.GCTrayColorStatuses.Where(x => x.Id == statusId).FirstOrDefaultAsync();
                if (status != null)
                {
                    status.IsLocked = false;
                    await _context.SaveChangesAsync();
                }
            }
            catch
            {

            }
        }

        public async Task AddColorComment(AddColorCommentModal model, string userName)
        {
            var isColorChanged = false;
            try
            {
                var gcTrayColorStatuses = _context.GCTrayColorStatuses.Where(x => x.IsActiveStatus && x.GCTrayId == model.GCTrayId).ToList();

                if (gcTrayColorStatuses.Count > 1)
                {
                    var error = $"Multiple active color statuses for GCTrayId: {model.GCTrayId}";
                    Log.Error(error);
                    throw new Exception("error");
                }
                else if (gcTrayColorStatuses.Count == 0)
                {
                    //var error = $"No active color statuses for GCTrayId: {model.GCTrayId}";
                    //Log.Error(error);
                    //throw new Exception("error");
                    _groundControlService.UpdateGCTrayColorStatus(model.GCTrayId, model.GCColorId);
                    gcTrayColorStatuses = _context.GCTrayColorStatuses.Where(x => x.IsActiveStatus && x.GCTrayId == model.GCTrayId).ToList();
                }

                var currentGCTrayColorStatus = gcTrayColorStatuses.First();

                var note = new GCTrayColorStatusNote()
                {
                    GCTrayColorStatusId = currentGCTrayColorStatus.Id,
                    Type = (int)GCTrayColorStatusNoteType.UserGenerated,
                    Title = model.Title,
                    Description = model.Description,
                    CreatedAtUtc = DateTime.UtcNow,
                    RequiresResolution = model.RequiresResolution,
                    CreatedBy = userName
                };
                if(model.GCColorId==0) //if color is not selected from UI, leave the same color
				{
                    model.GCColorId = currentGCTrayColorStatus.GCColorStatusTypeId;

                }

                if (currentGCTrayColorStatus.GCColorStatusTypeId != model.GCColorId)
                {
                    currentGCTrayColorStatus.IsActiveStatus = false;
                    currentGCTrayColorStatus.IsLocked = false;
                    note.GCTrayColorStatusId = 0;
                    note.GCTrayColorStatus = new GCTrayColorStatus()
                    {
                        GCColorStatusTypeId = model.GCColorId,
                        GCTrayId = model.GCTrayId,
                        Timestamp = DateTime.UtcNow,
                        IsActiveStatus = true,
                        IsLocked = true
                    };
                    isColorChanged = true;
                }
                else
                {
                    currentGCTrayColorStatus.IsLocked = true;
                }

                _context.GCTrayColorStatusNotes.Add(note);
                await _context.SaveChangesAsync();

                if (isColorChanged)
                {
                    var gcTray = _context.GCTrays.Find(model.GCTrayId);
                    if (gcTray != null && gcTray.GCCaseId.HasValue)
                    {
                        UpdateGCCaseColorStatus(gcTray.GCCaseId.Value);
                    }
                }
            }
            catch
            {

            }
        }

        public List<GCInvoice> GetGCInvoicesForCustomer(int gcCustomerId, bool? activeLinesOnly = false, bool? getUnSubmittedToQBOOnly = false)
        {
            var query = _context.GCInvoices.Include(x => x.GCInvoiceLines).Where(x => x.GCCustomerId == gcCustomerId);

            if (activeLinesOnly.HasValue && activeLinesOnly.Value)
            {
                foreach (var item in query)
                {
                    item.GCInvoiceLines = item.GCInvoiceLines.Where(x => x.EndDateUtc == null).OrderBy(x => x.Id).ToList();
                }
            }

            if (getUnSubmittedToQBOOnly.HasValue && getUnSubmittedToQBOOnly.Value)
            {
                query = query.Where(x => x.QBInvoiceId == null);
            }

            List<GCInvoice> result = query.ToList();
            return result;
        }

        public async Task SetEndDateToGCInvoiceLine(int gcInvoiceLineId)
        {
            var il = _context.GCInvoiceLines.Find(gcInvoiceLineId);
            if (il != null)
            {
                il.EndDateUtc = DateTime.UtcNow;
                //_context.SaveChanges();

                var gcInvoice = _context.GCInvoices.Include(x => x.GCInvoiceLines).Where(x => x.Id == il.GCInvoiceId).FirstOrDefault();

                int lineNumber = 1;
                if (gcInvoice != null && gcInvoice.GCInvoiceLines != null)
                {
                    gcInvoice.TotalAmt = gcInvoice.GCInvoiceLines.Where(x => x.EndDateUtc == null).Sum(x => x.Amount);
                    var gcInvoiceLinesToAdjust = gcInvoice.GCInvoiceLines.Where(x => x.EndDateUtc == null).OrderBy(x => x.LineNum).ToList();
                    foreach (var gcLineItem in gcInvoiceLinesToAdjust)
                    {
                        if (gcLineItem.QBInvoiceLineId.HasValue)
                        {
                            lineNumber = gcLineItem.LineNum;
                        }
                        else
                        {
                            gcLineItem.LineNum = lineNumber;
                        }
                        lineNumber++;
                    }
                }
            }

            await _context.SaveChangesAsync();
        }

        public string GetGCImageSource(int gcImageId, int type)
        {
            var fileName = "https://www.fujifilm.ca/products/digital_cameras/x/fujifilm_x20/sample_images/img/index/ff_x20_008.JPG";
            var webClient = new WebClient();
            byte[] fileBytes = webClient.DownloadData(fileName);

            //var mimeType = "application/jpg";
            //var fileBytes = await GetFileBytesById(id);
            //var fileBytes = System.IO.File.ReadAllBytes(fileName);

            string imageDataURL = string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(fileBytes));

            //return new FileContentResult(fileBytes, mimeType)
            //{
            //    FileDownloadName = fileName
            //};

            return imageDataURL;
        }

        public string GetImageSource(int imageId)
        {
            string imageDataURL = $"data:image/jpg;base64,{Convert.ToBase64String(GetImageBytes(imageId))}";
            return imageDataURL;
        }

        public PhotoWithCountSheet GetPhotoWithCountSheet(int imageId, bool isAsseblyPhoto, int statusId)
		{
            var result = new PhotoWithCountSheet();
            if(isAsseblyPhoto)
			{
                result.AssemblyCountSheet = GetAssemblyCountSheet(statusId);
                result.ImageSrc = $"data:image/jpg;base64,{Convert.ToBase64String(GetImageBytes(imageId))}";
                result.ShowCountSheet = true;
            }
            else
			{
                result.ImageSrc = $"data:image/jpg;base64,{Convert.ToBase64String(GetImageBytes(imageId))}";
                result.ShowCountSheet = false;
            }
            return result;
		}

        public byte[] GetImageBytes(int imageId)
        {
            byte[] bytes = null;
            var gcImage = _context.GCImages.Find(imageId);
            if (gcImage != null)
            {
                bytes = gcImage.Data;
            }
            return bytes;
        }

        public IEnumerable<GCTray> GetLoanerTrays(int caseId)
        {
            var model = _context.GCTrays.Where(x => x.GCCaseId == caseId && x.IsLoaner == true).ToList();
            return model;

        }

        /// <summary>
        /// gets tray history for single case
        /// </summary>
        /// <param name="caseId">Case Id</param>
        /// <param name="trayId">Actual Id</param>
        /// <returns></returns>
        public TrayHistoryCase GetTrayHistoryForSingleCase(int caseId, int trayId)
		{
           
            var tray = _context.GCTrays.Where(x => x.CTContainerTypeActualId == trayId && x.GCCaseId == caseId).Include(x => x.GCCase).Include(x => x.GCCase.CTCase).Include(x => x.GCCase.GCCustomer).FirstOrDefault(); //get gcTray for specific Actual
            var lifecycleStatusTypes = _context.GCLifecycleStatusTypes.OrderBy(x => x.Order).ToList();
            var isTrayLoaner = false;
            if (tray.IsLoaner != null)
            {
                isTrayLoaner = (bool)tray.IsLoaner;
            }
            var trayLifecycleStatuses = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id).Include(x => x.LifecycleStatusType).ToList();
            if (tray.GCCaseId != null) //If tray has case, then create History Case 
            {
                var trayHistoryCase = new TrayHistoryCase() { CaseId = (int)tray.GCCaseId, CaseReference = tray.GCCase.Title, TrayLifecycles = new List<TrayHistoryTrayLifecycle>(), DueDate = tray.GCCase.CTCase.DueTimestamp, CustomerName = tray.GCCase.GCCustomer?.CustomerName, isLoaner = isTrayLoaner, IsCanceled = tray.GCCase.IsCanceled, LapCount = tray.ActualLapCount, GCTrayId = tray.Id };
                List<GCLifecycleStatusType> statusTypes = new List<GCLifecycleStatusType>();
                if (isTrayLoaner) //get all lifecycles if tray is Loaner except Clean Staged
                {
                    statusTypes = lifecycleStatusTypes.Where(x => x.Code != (int)LifecycleStatusType.CleanStaged).ToList();

                }
                else //exclude Loaner lifecycles
                {
                    statusTypes = lifecycleStatusTypes.Where(x => x.IsLoaner == false).ToList();

                }
                foreach (var lifecycleStatus in statusTypes)
                {
                    var trayStatus = trayLifecycleStatuses.Where(x => x.LifecycleStatusTypeId == lifecycleStatus.Id).FirstOrDefault(); //get tray status for specific lifecycleStatusType
                    var historyLifecycle = new TrayHistoryTrayLifecycle();
                    if (trayStatus != null) //if status exists, take its data
                    {
                        historyLifecycle = new TrayHistoryTrayLifecycle() { 
                            LifecycleOrder = lifecycleStatus.Order, 
                            LifecycleTitle = lifecycleStatus.Title, 
                            Timestamp = trayStatus.Timestamp, 
                            ShowTimestamp = true, 
                            User = trayStatus.User, 
                            StatusType = (LifecycleStatusType)Enum.Parse(typeof(LifecycleStatusType), lifecycleStatus.Code.ToString()), 
                            LifecycleStatusId = lifecycleStatus.Id, 
                            Id = trayStatus.Id, 
                            IsCTOrigin = (!string.IsNullOrWhiteSpace(lifecycleStatus.Origin) && lifecycleStatus.Origin.Equals(Helpers.Constants.CT_ABBREVIATION_TWO_CHAR))
                        };

                    }
                    else //if not, just create placeholder for lifecycleStatus for UI
                    {
                        historyLifecycle = new TrayHistoryTrayLifecycle() { 
                            LifecycleOrder = lifecycleStatus.Order, 
                            LifecycleTitle = lifecycleStatus.Title, 
                            ShowTimestamp = false, 
                            StatusType = (LifecycleStatusType)Enum.Parse(typeof(LifecycleStatusType), lifecycleStatus.Code.ToString()), 
                            LifecycleStatusId = lifecycleStatus.Id, 
                            IsCTOrigin = (!string.IsNullOrWhiteSpace(lifecycleStatus.Origin) && lifecycleStatus.Origin.Equals(Helpers.Constants.CT_ABBREVIATION_TWO_CHAR)) 
                        };
                    }
                    if (lifecycleStatus.Code == (int)LifecycleStatusType.ShippedOutbound) //if step is ShippedOutbound, get PODs
                    {
                        var podContainer = GetTrayHistoryPodContainer("pickup", tray.GCCase.OutgoingOrderId, tray.Barcode);
                        historyLifecycle.PodContainer = podContainer;
                        historyLifecycle.ShowPods = true;
                    }
                    else if (lifecycleStatus.Code == (int)LifecycleStatusType.Delivered) //if step is Delivered, get PODs
                    {
                        var podContainer = GetTrayHistoryPodContainer("delivery", tray.GCCase.OutgoingOrderId, tray.Barcode);
                        historyLifecycle.PodContainer = podContainer;
                        historyLifecycle.ShowPods = true;
                    }
                    else if (lifecycleStatus.Code == (int)LifecycleStatusType.ShippedInbound) //if step is ShippedInbound, get PODs
                    {
                        var podContainer = GetTrayHistoryPodContainer("pickup", tray.GCCase.IncomingOrderId, tray.Barcode);
                        historyLifecycle.PodContainer = podContainer;
                        historyLifecycle.ShowPods = true;
                    }
                    else if (lifecycleStatus.Code == (int)LifecycleStatusType.Received) //if step is Received, get PODs
                    {
                        var podContainer = GetTrayHistoryPodContainer("delivery", tray.GCCase.IncomingOrderId, tray.Barcode);
                        historyLifecycle.PodContainer = podContainer;
                        historyLifecycle.ShowPods = true;
                    }
                    else if (lifecycleStatus.Code == (int)LifecycleStatusType.Picked) //if step is Prepped, get PODs
                    {
                        var podContainer = GetFullPodContainerForStatus(trayStatus?.Id ?? 0);
                        historyLifecycle.PodContainer = podContainer;
                        historyLifecycle.ShowPods = true;
                    }
                    else if (lifecycleStatus.Code == (int)LifecycleStatusType.Verification)
                    {
                        TrayHistoryVerificationQualityFeed verificationQualityFeed = GetTrayHistoryVerificationQualityFeed(trayStatus?.Id ?? 0);
                        if (verificationQualityFeed != null)
                        {
                            historyLifecycle.VerificationQualityFeed = verificationQualityFeed;
                            historyLifecycle.ShowVerificationQualityFeed = true;
                        }
                    }
                    else if (lifecycleStatus.Code == (int)LifecycleStatusType.Sterilize)
                    {
                        TrayHistorySterilizeBIResult sterilizeBIContainer = GetSterilizeBIResult(trayStatus?.Id ?? 0);
                        if (sterilizeBIContainer != null)
                        {
                            historyLifecycle.SterilizeBIResult = sterilizeBIContainer;
                            historyLifecycle.ShowSterilizeBI = true;
                        }
                    }
                    else if (lifecycleStatus.Code == (int)LifecycleStatusType.Assembly || lifecycleStatus.Code == (int)LifecycleStatusType.AssemblyLoaner)
                    {
                        var podContainer = GetFullPodContainerForAssemblyStatus(trayStatus?.Id ?? 0);
                        historyLifecycle.PodContainer = podContainer;
                        historyLifecycle.ShowPods = true;

                        var countSheets = GetAssemblyCountSheet(trayStatus?.Id ?? 0);
                        historyLifecycle.AssemblyCountSheet = countSheets;
                        historyLifecycle.ShowCountSheets = countSheets.AssemblyCountSheets.Count > 0 ? true : false;
                    }
                    trayHistoryCase.TrayLifecycles.Add(historyLifecycle);
                }
                return trayHistoryCase;
            }
            else//else means that there is no case for this tray, but this should never be the case
            {
                return null;

            }
           
        }

        public TrayHistoryModel GetTrayHistory(int trayId)
        {
            var model = new TrayHistoryModel() { Cases = new List<TrayHistoryCase>(), TrayPreviousHistory = new List<TrayHistoryTrayLifecycle>(), TrayParts = new List<TrayPart>(), TrayImages = new List<TrayImage>() };
            var gcTrays = _context.GCTrays.Where(x => x.CTContainerTypeActualId == trayId).Include(x => x.GCCase).Include(x => x.GCCase.CTCase).Include(x => x.GCCase.GCCustomer).ToList(); //get all gcTrays for specific Actual
            var lifecycleStatusTypes = _context.GCLifecycleStatusTypes.OrderBy(x => x.Order).ToList();
            if (gcTrays != null && gcTrays.Count > 0)
            {
                foreach (var tray in gcTrays)
                {
                    var isTrayLoaner = false;
                    if (tray.IsLoaner != null)
                    {
                        isTrayLoaner = (bool)tray.IsLoaner;
                    }
                    var trayLifecycleStatuses = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id).Include(x => x.LifecycleStatusType).ToList();
                    if (tray.GCCaseId != null) //If tray has case, then create History Case 
                    {
                        var trayHistoryCase = new TrayHistoryCase() { CaseId = (int)tray.GCCaseId, CaseReference = tray.GCCase.Title, TrayLifecycles = new List<TrayHistoryTrayLifecycle>(), DueDate = tray.GCCase.CTCase.DueTimestamp, CustomerName = tray.GCCase.GCCustomer?.CustomerName, isLoaner = isTrayLoaner, IsCanceled = tray.GCCase.IsCanceled, LapCount = tray.ActualLapCount, GCTrayId = tray.Id };
                        List<GCLifecycleStatusType> statusTypes = new List<GCLifecycleStatusType>();
                        if (isTrayLoaner) //get all lifecycles if tray is Loaner except Clean Staged
                        {
                            statusTypes = lifecycleStatusTypes.Where(x => x.Code != (int)LifecycleStatusType.CleanStaged).ToList();

                        }
                        else //exclude Loaner lifecycles
                        {
                            statusTypes = lifecycleStatusTypes.Where(x => x.IsLoaner == false).ToList();

                        }
                        foreach (var lifecycleStatus in statusTypes)
                        {
                            var trayStatus = trayLifecycleStatuses.Where(x => x.LifecycleStatusTypeId == lifecycleStatus.Id).FirstOrDefault(); //get tray status for specific lifecycleStatusType
                            var historyLifecycle = new TrayHistoryTrayLifecycle();
                            if (trayStatus != null) //if status exists, take its data
                            {
                                historyLifecycle = new TrayHistoryTrayLifecycle() 
                                { 
                                    LifecycleOrder = lifecycleStatus.Order, 
                                    LifecycleTitle = lifecycleStatus.Title, 
                                    Timestamp = trayStatus.Timestamp, 
                                    ShowTimestamp = true, 
                                    User = trayStatus.User, 
                                    StatusType = (LifecycleStatusType)Enum.Parse(typeof(LifecycleStatusType), lifecycleStatus.Code.ToString()), 
                                    LifecycleStatusId = lifecycleStatus.Id, 
                                    Id = trayStatus.Id, 
                                    IsCTOrigin = (!string.IsNullOrWhiteSpace(lifecycleStatus.Origin) && lifecycleStatus.Origin.Equals(Helpers.Constants.CT_ABBREVIATION_TWO_CHAR))
                                };

                            }
                            else //if not, just create placeholder for lifecycleStatus for UI
                            {
                                historyLifecycle = new TrayHistoryTrayLifecycle()
                                { 
                                    LifecycleOrder = lifecycleStatus.Order, 
                                    LifecycleTitle = lifecycleStatus.Title, 
                                    ShowTimestamp = false, 
                                    StatusType = (LifecycleStatusType)Enum.Parse(typeof(LifecycleStatusType), 
                                    lifecycleStatus.Code.ToString()), 
                                    LifecycleStatusId = lifecycleStatus.Id, 
                                    IsCTOrigin = (!string.IsNullOrWhiteSpace(lifecycleStatus.Origin) && lifecycleStatus.Origin.Equals(Helpers.Constants.CT_ABBREVIATION_TWO_CHAR))
                                };
                            }
                            if (lifecycleStatus.Code == (int)LifecycleStatusType.ShippedOutbound) //if step is ShippedOutbound, get PODs
                            {
                                var podContainer = GetTrayHistoryPodContainer("pickup", tray.GCCase.OutgoingOrderId, tray.Barcode);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Delivered) //if step is Delivered, get PODs
                            {
                                var podContainer = GetTrayHistoryPodContainer("delivery", tray.GCCase.OutgoingOrderId, tray.Barcode);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.ShippedInbound) //if step is ShippedInbound, get PODs
                            {
                                var podContainer = GetTrayHistoryPodContainer("pickup", tray.GCCase.IncomingOrderId, tray.Barcode);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Received) //if step is Received, get PODs
                            {
                                var podContainer = GetTrayHistoryPodContainer("delivery", tray.GCCase.IncomingOrderId, tray.Barcode);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Picked) //if step is Prepped, get PODs
                            {
                                var podContainer = GetFullPodContainerForStatus(trayStatus?.Id ?? 0);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Verification)
                            {
                                TrayHistoryVerificationQualityFeed verificationQualityFeed = GetTrayHistoryVerificationQualityFeed(trayStatus?.Id ?? 0);
                                if (verificationQualityFeed != null)
                                {
                                    historyLifecycle.VerificationQualityFeed = verificationQualityFeed;
                                    historyLifecycle.ShowVerificationQualityFeed = true;
                                }
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Sterilize)
                            {
                                TrayHistorySterilizeBIResult sterilizeBIContainer = GetSterilizeBIResult(trayStatus?.Id ?? 0);
                                if (sterilizeBIContainer != null)
                                {
                                    historyLifecycle.SterilizeBIResult = sterilizeBIContainer;
                                    historyLifecycle.ShowSterilizeBI = true;
                                }
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Assembly || lifecycleStatus.Code == (int)LifecycleStatusType.AssemblyLoaner)
                            {
                                var podContainer = GetFullPodContainerForAssemblyStatus(trayStatus?.Id ?? 0);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;

                                var countSheets = GetAssemblyCountSheet(trayStatus?.Id ?? 0);
                                historyLifecycle.AssemblyCountSheet = countSheets;
                                historyLifecycle.ShowCountSheets = countSheets.AssemblyCountSheets.Count > 0 ? true : false;
                            }
                            trayHistoryCase.TrayLifecycles.Add(historyLifecycle);
                        }
                        model.Cases.Add(trayHistoryCase);
                    }
                    else//else add to previous tray history (Tray Induction)
                    {
                        var trayInductionLifecycles = trayLifecycleStatuses.Where(x => x.LifecycleStatusType.Code >= (int)LifecycleStatusType.DeconStaged).ToList();
                        foreach (var lifecycleStatus in trayInductionLifecycles)
                        {

                            //var trayStatus = trayLifecycleStatuses.Where(x => x.Id == lifecycleStatus.Id).FirstOrDefault();
                            var historyLifecycle = new TrayHistoryTrayLifecycle()
                            { 
                                LifecycleOrder = lifecycleStatus.LifecycleStatusType.Order, 
                                LifecycleTitle = lifecycleStatus.LifecycleStatusType.Title, 
                                Timestamp = lifecycleStatus.Timestamp, 
                                ShowTimestamp = true, 
                                User = lifecycleStatus?.User, 
                                Id = lifecycleStatus.Id 
                            };
                            if (lifecycleStatus.LifecycleStatusType.Code == (int)LifecycleStatusType.Assembly || lifecycleStatus.LifecycleStatusType.Code == (int)LifecycleStatusType.AssemblyLoaner)
                            {
                                var podContainer = GetFullPodContainerForAssemblyStatus(lifecycleStatus?.Id ?? 0);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;

                                var countSheets = GetAssemblyCountSheet(lifecycleStatus?.Id ?? 0);
                                historyLifecycle.AssemblyCountSheet = countSheets;
                                historyLifecycle.ShowCountSheets = countSheets.AssemblyCountSheets.Count > 0 ? true : false;
                            }
                            model.TrayPreviousHistory.Add(historyLifecycle);
                        }

                    }
                }

                #region Getting tray history (Induction) anyway for the tray
                //Getting tray history (Induction) anyway for the tray
                //       var firstTray = gcTrays.OrderBy(x => x.Id).First();
                //       if (firstTray == null || firstTray.CTContainerTypeActualId.HasValue)
                //       {
                //           GCTraysLifecycleStatusTypes earliestLC = null;

                //           if (firstTray != null)
                //           {
                //               earliestLC = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == firstTray.Id).OrderBy(x => x.Timestamp).FirstOrDefault();
                //           }

                //           var inductionLifeCycleLocationTypes = _context.CTLocationTypeGCLifecycleStatusTypes.Include(x => x.GCLifecycleStatusType).Where(x =>
                //               x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.DeconStaged ||
                //               x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Sink ||
                //               x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Washer ||
                //               x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.AssemblyStaged ||
                //               x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Assembly ||
                //               x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.CleanIncomplete ||
                //               x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Quarantine ||
                //               x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged).ToList();

                //           var deconLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.DeconStaged).FirstOrDefault();
                //           var sinkLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Sink).FirstOrDefault();
                //           var washerLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Washer).FirstOrDefault();
                //           var assemblyStagedLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.AssemblyStaged).FirstOrDefault();
                //           var assemblyLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Assembly).FirstOrDefault();
                //           var cleanInclompleteLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.CleanIncomplete).FirstOrDefault();
                //           var quarantineLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Quarantine).FirstOrDefault();
                //           var cleanStagedLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged).FirstOrDefault();

                //           if (inductionLifeCycleLocationTypes != null && inductionLifeCycleLocationTypes.Count > 0)
                //           {
                //               var trayHistoryItems = _context.CTContainerTypeActualHistoryItems.Include(x => x.CTLocation).Where(x =>
                //                   x.CTContainerTypeActualId == firstTray.CTContainerTypeActualId.Value &&
                //                   (earliestLC == null ||x.UpdateTimestamp < earliestLC.Timestamp) &&
                //                   (x.Status == Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG || x.Status == Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER) &&
                //                   (x.CTLocationId == null || 
                //                       (deconLCLT != null && x.CTLocation.CTLocationTypeId == deconLCLT.CTLocationTypeId) ||
                //                       (sinkLCLT != null && x.CTLocation.CTLocationTypeId == sinkLCLT.CTLocationTypeId) ||
                //                       (washerLCLT != null && x.CTLocation.CTLocationTypeId == washerLCLT.CTLocationTypeId) ||
                //                       (assemblyStagedLCLT != null && x.CTLocation.CTLocationTypeId == assemblyStagedLCLT.CTLocationTypeId) ||
                //                       (assemblyLCLT != null && x.CTLocation.CTLocationTypeId == assemblyLCLT.CTLocationTypeId) ||
                //                       (cleanInclompleteLCLT != null && x.CTLocation.CTLocationTypeId == cleanInclompleteLCLT.CTLocationTypeId) ||
                //                       (quarantineLCLT != null && x.CTLocation.CTLocationTypeId == quarantineLCLT.CTLocationTypeId) ||
                //                       (cleanStagedLCLT != null && x.CTLocation.CTLocationTypeId == cleanStagedLCLT.CTLocationTypeId))
                //               ).OrderByDescending(x => x.UpdateTimestamp)
                //               .Take(8) // There are 8 Induction Steps (LifeCycles)
                //               .ToList();

                //                   if (trayHistoryItems != null && trayHistoryItems.Count > 0)
                //                   {
                //                       trayHistoryItems.ForEach(x =>
                //                       {
                //                           //var ctLocation = _context.CTLocations.Find(x.CTLocationId);
                //                           if(x.CTLocation!=null)
                //{
                //                               var lifecycleStatus = inductionLifeCycleLocationTypes.Where(y => y.CTLocationTypeId == x.CTLocation.CTLocationTypeId).FirstOrDefault();
                //                               if (lifecycleStatus != null)
                //                               {
                //                                   var historyLifecycle = new TrayHistoryTrayLifecycle() { LifecycleOrder = lifecycleStatus.GCLifecycleStatusType.Order, LifecycleTitle = lifecycleStatus.GCLifecycleStatusType.Title, Timestamp = x.UpdateTimestamp, ShowTimestamp = true, User = x.UpdateUserId };
                //                                   if (!model.TrayPreviousHistory.Any(item => item.LifecycleStatusId == lifecycleStatus.Id))
                //                                   {
                //                                       model.TrayPreviousHistory.Add(historyLifecycle);
                //                                   }
                //                               }
                //                           }

                //                       });
                //                   }
                //               }
                //           }
                #endregion
            }

            model.TrayPreviousHistory = model.TrayPreviousHistory.OrderBy(x => x.LifecycleOrder).ToList();
            model.Cases = model.Cases.DistinctBy(x => x.CaseId).OrderByDescending(x => x.DueDate).ToList();
            model.Cases = GetPreviousTrayHistory(model.Cases, (int)LifecycleStatusType.Prepped); //get everything from DeconStaged to CleanStaged as previous history
            model.TrayAssemblyParts = GetTrayPartsWithActualCount(model.Cases);
            model.TrayAssemblyPhotos = GetLatestAssemblyPhotos(model.Cases);
            model.TrayImages = GetTrayImages(trayId);
            return model;
        }

        public TrayHistoryModel GetTrayHistoryForVendors(int trayId)
        {
            var model = new TrayHistoryModel() { Cases = new List<TrayHistoryCase>(), TrayPreviousHistory = new List<TrayHistoryTrayLifecycle>(), TrayParts = new List<TrayPart>(), TrayImages = new List<TrayImage>() };
            var gcTrays = _context.GCTrays.Where(x => x.CTContainerTypeActualId == trayId).Include(x => x.GCCase).Include(x => x.GCCase.CTCase).Include(x => x.GCCase.GCCustomer).ToList(); //get all gcTrays for specific Actual
            var lifecycleStatusTypes = _context.GCLifecycleStatusTypes.OrderBy(x => x.Order).ToList();
            if (gcTrays != null && gcTrays.Count > 0)
            {
                foreach (var tray in gcTrays)
                {
                    var isTrayLoaner = false;
                    if (tray.IsLoaner != null)
                    {
                        isTrayLoaner = (bool)tray.IsLoaner;
                    }
                    var trayLifecycleStatuses = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id).Include(x => x.LifecycleStatusType).ToList();
                    if (tray.GCCaseId != null) //If tray has case, then create History Case 
                    {
                        var trayHistoryCase = new TrayHistoryCase() { CaseId = (int)tray.GCCaseId, CaseReference = tray.GCCase.Title, TrayLifecycles = new List<TrayHistoryTrayLifecycle>(), DueDate = tray.GCCase.CTCase.DueTimestamp, CustomerName = tray.GCCase.GCCustomer?.CustomerName, isLoaner = isTrayLoaner, IsCanceled = tray.GCCase.IsCanceled, LapCount = tray.ActualLapCount, GCTrayId = tray.Id };
                        List<GCLifecycleStatusType> statusTypes = new List<GCLifecycleStatusType>();
                        if (isTrayLoaner) //get all lifecycles if tray is Loaner except Clean Staged
                        {
                            statusTypes = lifecycleStatusTypes.Where(x => x.Code != (int)LifecycleStatusType.CleanStaged).ToList();

                        }
                        else //exclude Loaner lifecycles
                        {
                            statusTypes = lifecycleStatusTypes.Where(x => x.IsLoaner == false).ToList();

                        }
                        foreach (var lifecycleStatus in statusTypes)
                        {
                            var trayStatus = trayLifecycleStatuses.Where(x => x.LifecycleStatusTypeId == lifecycleStatus.Id).FirstOrDefault(); //get tray status for specific lifecycleStatusType
                            var historyLifecycle = new TrayHistoryTrayLifecycle();
                            if (trayStatus != null) //if status exists, take its data
                            {
                                historyLifecycle = new TrayHistoryTrayLifecycle() 
                                { 
                                    LifecycleOrder = lifecycleStatus.Order, 
                                    LifecycleTitle = lifecycleStatus.Title, 
                                    Timestamp = trayStatus.Timestamp, 
                                    ShowTimestamp = true, 
                                    User = trayStatus.User, 
                                    StatusType = (LifecycleStatusType)Enum.Parse(typeof(LifecycleStatusType), lifecycleStatus.Code.ToString()), 
                                    LifecycleStatusId = lifecycleStatus.Id, 
                                    Id = trayStatus.Id, 
                                    IsCTOrigin = (!string.IsNullOrWhiteSpace(lifecycleStatus.Origin) && lifecycleStatus.Origin.Equals(Helpers.Constants.CT_ABBREVIATION_TWO_CHAR))
                                };
                            }
                            else //if not, just create placeholder for lifecycleStatus for UI
                            {
                                historyLifecycle = new TrayHistoryTrayLifecycle()
                                { 
                                    LifecycleOrder = lifecycleStatus.Order, 
                                    LifecycleTitle = lifecycleStatus.Title, 
                                    ShowTimestamp = false, 
                                    StatusType = (LifecycleStatusType)Enum.Parse(typeof(LifecycleStatusType), lifecycleStatus.Code.ToString()), 
                                    LifecycleStatusId = lifecycleStatus.Id, 
                                    IsCTOrigin = (!string.IsNullOrWhiteSpace(lifecycleStatus.Origin) && lifecycleStatus.Origin.Equals(Helpers.Constants.CT_ABBREVIATION_TWO_CHAR))
                                };
                            }
                            if (lifecycleStatus.Code == (int)LifecycleStatusType.ShippedOutbound) //if step is ShippedOutbound, get PODs
                            {
                                var podContainer = GetTrayHistoryPodContainer("pickup", tray.GCCase.OutgoingOrderId, tray.Barcode);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Delivered) //if step is Delivered, get PODs
                            {
                                var podContainer = GetTrayHistoryPodContainer("delivery", tray.GCCase.OutgoingOrderId, tray.Barcode);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.ShippedInbound) //if step is ShippedInbound, get PODs
                            {
                                var podContainer = GetTrayHistoryPodContainer("pickup", tray.GCCase.IncomingOrderId, tray.Barcode);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Received) //if step is Received, get PODs
                            {
                                var podContainer = GetTrayHistoryPodContainer("delivery", tray.GCCase.IncomingOrderId, tray.Barcode);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Picked) //if step is Prepped, get PODs
                            {
                                var podContainer = GetFullPodContainerForStatus(trayStatus?.Id ?? 0);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Verification)
                            {
                                TrayHistoryVerificationQualityFeed verificationQualityFeed = GetTrayHistoryVerificationQualityFeed(trayStatus?.Id ?? 0);
                                if (verificationQualityFeed != null)
                                {
                                    historyLifecycle.VerificationQualityFeed = verificationQualityFeed;
                                    historyLifecycle.ShowVerificationQualityFeed = true;
                                }
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Sterilize)
                            {
                                TrayHistorySterilizeBIResult sterilizeBIContainer = GetSterilizeBIResult(trayStatus?.Id ?? 0);
                                if (sterilizeBIContainer != null)
                                {
                                    historyLifecycle.SterilizeBIResult = sterilizeBIContainer;
                                    historyLifecycle.ShowSterilizeBI = true;
                                }
                            }
                            else if (lifecycleStatus.Code == (int)LifecycleStatusType.Assembly || lifecycleStatus.Code == (int)LifecycleStatusType.AssemblyLoaner)
                            {
                                var podContainer = GetFullPodContainerForAssemblyStatus(trayStatus?.Id ?? 0);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;

                                var countSheets = GetAssemblyCountSheet(trayStatus?.Id ?? 0);
                                historyLifecycle.AssemblyCountSheet = countSheets;
                                historyLifecycle.ShowCountSheets = countSheets.AssemblyCountSheets.Count > 0 ? true : false;
                            }
                            trayHistoryCase.TrayLifecycles.Add(historyLifecycle);
                        }
                        model.Cases.Add(trayHistoryCase);
                    }
                    else//else add to previous tray history (Tray Induction)
                    {
                        var trayInductionLifecycles = trayLifecycleStatuses.Where(x => x.LifecycleStatusType.Code >= (int)LifecycleStatusType.DeconStaged).ToList();
                        foreach (var lifecycleStatus in trayInductionLifecycles)
                        {

                            //var trayStatus = trayLifecycleStatuses.Where(x => x.Id == lifecycleStatus.Id).FirstOrDefault();
                            var historyLifecycle = new TrayHistoryTrayLifecycle() { LifecycleOrder = lifecycleStatus.LifecycleStatusType.Order, LifecycleTitle = lifecycleStatus.LifecycleStatusType.Title, Timestamp = lifecycleStatus.Timestamp, ShowTimestamp = true, User = lifecycleStatus?.User, Id = lifecycleStatus.Id, StatusType = (LifecycleStatusType)Enum.Parse(typeof(LifecycleStatusType), lifecycleStatus.LifecycleStatusType.Code.ToString()), LifecycleStatusId = lifecycleStatus.LifecycleStatusTypeId };
                            if (lifecycleStatus.LifecycleStatusType.Code == (int)LifecycleStatusType.Assembly || lifecycleStatus.LifecycleStatusType.Code == (int)LifecycleStatusType.AssemblyLoaner)
                            {
                                var podContainer = GetFullPodContainerForAssemblyStatus(lifecycleStatus?.Id ?? 0);
                                historyLifecycle.PodContainer = podContainer;
                                historyLifecycle.ShowPods = true;

                                var countSheets = GetAssemblyCountSheet(lifecycleStatus?.Id ?? 0);
                                historyLifecycle.AssemblyCountSheet = countSheets;
                                historyLifecycle.ShowCountSheets = countSheets.AssemblyCountSheets.Count > 0 ? true : false;
                            }
                            model.TrayPreviousHistory.Add(historyLifecycle);
                        }

                    }
                }
            }

            model.TrayPreviousHistory = model.TrayPreviousHistory.OrderBy(x => x.LifecycleOrder).ToList();
            model.Cases = model.Cases.DistinctBy(x => x.CaseId).OrderByDescending(x => x.DueDate).ToList();
            model.Cases = GetPreviousTrayHistory(model.Cases, (int)LifecycleStatusType.Prepped); //get everything from DeconStaged to CleanStaged as previous history
            if (model.Cases.Count == 1) //if there is only one tray, than previous tray history is from induction
            {
                model.Cases.First().PreviousLifecycles = model.TrayPreviousHistory;
            }
            //model.TrayAssemblyParts = GetTrayPartsWithActualCount(model.Cases);
            model.TrayAssemblyPhotos = GetLatestAssemblyPhotos(model.Cases);
            if(model.TrayAssemblyPhotos?.AssemblyPhotos==null || model.Cases.Count == 1)  //if we don't find in cases, try to find assembly photo from tray induction, or if there is just one case then take assembly photos from induction
            {
                model.TrayAssemblyPhotos = GetLatestAssemblyPhotosFromPreviousHistory(model.TrayPreviousHistory);
            }
            model.LatestBIResults = GetLatestBIResults(model.Cases);
            model.LatestDeliveryPods= GetLatestDeliveryResults(model.Cases);

            if(model.Cases.Count>0)
			{
                model.VendorHistoryLifecycles = GetVendorHistoryLifecycles(model.Cases.First());
            }
            else
			{
                model.VendorHistoryLifecycles = model.TrayPreviousHistory;
			}
            //model.TrayImages = GetTrayImages(trayId);
            model.NumOfPhotos = model.TrayAssemblyPhotos?.NumOfPhotos + model.LatestBIResults?.NumOfPhotos + model.LatestDeliveryPods?.NumOfPhotos;
            return model;
        }
        private List<TrayImage> GetTrayImages(int actualId)
        {
            var trayImages = new List<TrayImage>();

            var ctActual = _context.CTContainerTypeActuals.Find(actualId);
            if (ctActual != null)
            {
                //var ctContainerTypeGraphics = _context.CTContainerTypeGraphics.Include(x => x.GCImage).Where(x => x.CTContainerTypeId == ctActual.CTContainerTypeId && x.GCImageId.HasValue).ToList();
                var ctContainerTypeGraphics = _context.CTContainerTypeGraphics.Where(x => x.CTContainerTypeId == ctActual.CTContainerTypeId && x.GCImageId.HasValue).ToList();
                if (ctContainerTypeGraphics != null && ctContainerTypeGraphics.Count > 0)
                {
                    ctContainerTypeGraphics.ForEach(x =>
                    {
                        var gcImage = _context.GCImages.Where(y => y.ParentId == x.GCImageId.Value && y.Type == (int)Enums.GCImageSizeType.SizeMax200).FirstOrDefault();

                        if (gcImage != null)
                        {
                            trayImages.Add(new TrayImage()
                            {
                                IsAssembly = x.IsAssembly,
                                IsDecontam = x.IsDecontam,
                                IsSterilization = x.IsSterilization,
                                Title = x.ImageName,
                                ImageDataUrl = $"data:image/jpg;base64,{Convert.ToBase64String(gcImage.Data)}",
                                Id = (int)gcImage.ParentId
                            });
                        }
                    });
                }
            }

            return trayImages;
        }

        public List<TrayImage> GetTrayImagesByBarcode(string barcode)
        {
            var tray = _context.GCTrays.Where(x => x.Barcode == barcode).FirstOrDefault();

            List<TrayImage> images = new List<TrayImage>();
            if (tray != null && tray.CTContainerTypeActualId != null)
            {
                images = GetTrayImages((int)tray.CTContainerTypeActualId);
            }

            return images;
        }

        public List<TrayImage> GetTrayImagesById(int gcTrayId)
        {
            var tray = _context.GCTrays.Find(gcTrayId);

            List<TrayImage> images = new List<TrayImage>();
            if (tray != null && tray.CTContainerTypeActualId != null)
            {
                images = GetTrayImages((int)tray.CTContainerTypeActualId);
            }

            return images;
        }

        private List<TrayPart> GetTrayParts(int trayId)
        {
            var trayParts = new List<TrayPart>();

            var ctActual = _context.CTContainerTypeActuals.Find(trayId);
            if (ctActual != null)
            {
                var ctContainerItems = _context.CTContainerItems.Include(x => x.CTProduct).Where(x => x.CTContainerTypeId == ctActual.CTContainerTypeId).ToList();
                if (ctContainerItems != null && ctContainerItems.Count > 0)
                {
                    ctContainerItems.ForEach(x =>
                    {
                        trayParts.Add(new TrayPart()
                        {
                            RequiredCount = x.RequiredCount,
                            Description = x.CTProduct == null ? null : x.CTProduct.VendorProductName,
                            ModelNumber = x.CTProduct == null ? null : x.CTProduct.ModelNumber,
                            Placement = x.Placement,
                            Vendor = x.CTProduct == null ? null : x.CTProduct.VendorName,
                        });
                    });
                }
            }

            return trayParts;
        }
        /// <summary>
        /// Find latest assembly count sheet data if any
        /// </summary>
        /// <param name="cases">Order by descending by date list of cases</param>
        /// <returns></returns>
        private TrayAssemblyParts GetTrayPartsWithActualCount(List<TrayHistoryCase> cases)
        {
            var trayAssemblyParts = new TrayAssemblyParts() { TrayParts = new List<TrayPart>() };
            if (cases != null && cases.Count > 0)
            {
                foreach (var historyCase in cases) //foreach all cases and find first that have assembly data
                {
                    if (historyCase != null && historyCase.TrayLifecycles != null && historyCase.TrayLifecycles.Count > 0)
                    {
                        TrayHistoryTrayLifecycle assembly = historyCase.TrayLifecycles.Where(x => x.StatusType == LifecycleStatusType.Assembly).FirstOrDefault();
                        TrayHistoryTrayLifecycle loanerAssembly = historyCase.TrayLifecycles.Where(x => x.StatusType == LifecycleStatusType.AssemblyLoaner).FirstOrDefault();
                        var status = assembly != null ? assembly : loanerAssembly; //AssemblyLoaner is before Assembly step so if there is Assembly step use it, if there isnt check if there is AssemblyLoaner
                        if (status != null) //if both AssemblyLoaner nad Assembly are null
                        {
                            if (status.ShowCountSheets && status.AssemblyCountSheet != null && status.AssemblyCountSheet.AssemblyCountSheets != null)
                            {

                                status.AssemblyCountSheet.AssemblyCountSheets.ForEach(x =>
                                {
                                    trayAssemblyParts.TrayParts.Add(new TrayPart()
                                    {
                                        RequiredCount = x.RequiredCount,
                                        ActualCount = x.ActualCount,
                                        Description = x.Description,
                                        ModelNumber = x.ModelNumber,
                                        Vendor = x.VendorName,
                                        SetName = x.SetName
                                    });
                                });

                                trayAssemblyParts.StatusTimestamp = status.Timestamp;
                                trayAssemblyParts.CaseReference = historyCase.CaseReference;
                                //trayAssemblyParts.AssemblyPhotos = GetFullPodContainerForAssemblyStatus(status.LifecycleStatusId);
                                return trayAssemblyParts;
                            }
                        }
                    }
                }

            }
            return trayAssemblyParts;
        }

        public TrayAssemblyPhotos GetLatestAssemblyPhotos(List<TrayHistoryCase> cases)
        {
            var trayAssemblyPhotos = new TrayAssemblyPhotos();
            if (cases != null && cases.Count > 0)
            {
                foreach (var historyCase in cases) //foreach all cases and find first that have assembly data
                {
                    if (historyCase != null && historyCase.TrayLifecycles != null && historyCase.TrayLifecycles.Count > 0)
                    {
                        TrayHistoryTrayLifecycle assembly = historyCase.TrayLifecycles.Where(x => x.StatusType == LifecycleStatusType.Assembly).FirstOrDefault();
                        TrayHistoryTrayLifecycle loanerAssembly = historyCase.TrayLifecycles.Where(x => x.StatusType == LifecycleStatusType.AssemblyLoaner).FirstOrDefault();
                        var status = assembly != null ? assembly : loanerAssembly; //AssemblyLoaner is before Assembly step so if there is Assembly step use it, if there isnt check if there is AssemblyLoaner
                        if (status != null) //if both AssemblyLoaner nad Assembly are null
                        {
                            if (status.ShowPods && status.PodContainer != null && status.PodContainer.Pictures != null && status.PodContainer.Pictures.Count > 0)
                            {
                                trayAssemblyPhotos.AssemblyPhotos = status.PodContainer;
                                trayAssemblyPhotos.StatusTimestamp = status.Timestamp;
                                trayAssemblyPhotos.CaseReference = historyCase.CaseReference;
                                trayAssemblyPhotos.NumOfPhotos = status.PodContainer.Pictures.Count;
                                trayAssemblyPhotos.StatusId = status.Id;
                                return trayAssemblyPhotos;
                            }
                        }
                    }
                }

            }
            return trayAssemblyPhotos;
        }

        public TrayAssemblyPhotos GetLatestAssemblyPhotosFromPreviousHistory(List<TrayHistoryTrayLifecycle> lifecycles)
        {
            var trayAssemblyPhotos = new TrayAssemblyPhotos();
            if (lifecycles != null && lifecycles.Count > 0)
            {
                TrayHistoryTrayLifecycle assembly = lifecycles.Where(x => x.StatusType == LifecycleStatusType.Assembly).FirstOrDefault();
                TrayHistoryTrayLifecycle loanerAssembly = lifecycles.Where(x => x.StatusType == LifecycleStatusType.AssemblyLoaner).FirstOrDefault();
                var status = assembly != null ? assembly : loanerAssembly; //AssemblyLoaner is before Assembly step so if there is Assembly step use it, if there isnt check if there is AssemblyLoaner
                if (status != null) //if both AssemblyLoaner nad Assembly are null
                {
                    if (status.ShowPods && status.PodContainer != null && status.PodContainer.Pictures != null && status.PodContainer.Pictures.Count > 0)
                    {
                        trayAssemblyPhotos.AssemblyPhotos = status.PodContainer;
                        trayAssemblyPhotos.StatusTimestamp = status.Timestamp;
                        //trayAssemblyPhotos.CaseReference = historyCase.CaseReference;
                        trayAssemblyPhotos.NumOfPhotos = status.PodContainer.Pictures.Count;
                        trayAssemblyPhotos.StatusId = status.Id;
                        return trayAssemblyPhotos;
                    }
                }

            }
            return trayAssemblyPhotos;
        }

        public TrayHistorySterilizeBIResultPhotos GetLatestBIResults(List<TrayHistoryCase> cases)
        {
            var result = new TrayHistorySterilizeBIResultPhotos();
            if (cases != null && cases.Count > 0)
            {
                foreach (var historyCase in cases) //foreach all cases and find first that have assembly data
                {
                    if (historyCase != null && historyCase.TrayLifecycles != null && historyCase.TrayLifecycles.Count > 0)
                    {
                        TrayHistoryTrayLifecycle status = historyCase.TrayLifecycles.Where(x => x.StatusType == LifecycleStatusType.Sterilize).FirstOrDefault();
                        if (status != null)
                        {
                            if (status.ShowSterilizeBI && status.SterilizeBIResult!= null && status.SterilizeBIResult.ImagesData!= null && status.SterilizeBIResult.ImagesData.Count>0)
                            {
                                result.Timestamp = status.Timestamp;
                                result.ImagesData = status.SterilizeBIResult.ImagesData;
                                result.BIResult = status.SterilizeBIResult;
                                result.NumOfPhotos = status.SterilizeBIResult.ImagesData.Count;
                                return result;
                            }
                        }
                    }
                }

            }
            return result;
        }

        public TrayHistoryDeliveryPhotos GetLatestDeliveryResults(List<TrayHistoryCase> cases)
        {
            var result = new TrayHistoryDeliveryPhotos();
            if (cases != null && cases.Count > 0)
            {
                foreach (var historyCase in cases) //foreach all cases and find first that have assembly data
                {
                    if (historyCase != null && historyCase.TrayLifecycles != null && historyCase.TrayLifecycles.Count > 0)
                    {
                        TrayHistoryTrayLifecycle status = historyCase.TrayLifecycles.Where(x => x.StatusType == LifecycleStatusType.Delivered).FirstOrDefault();
                        if (status != null)
                        {
                            if (status.ShowPods && status.PodContainer != null && status.PodContainer.Pictures != null && status.PodContainer.Pictures.Count > 0)//I am assuming that if we have photos, than we have to have signature as POD
                            {
                                result.Timestamp = status.Timestamp;
                                result.PodContainer = status.PodContainer;
                                result.NumOfPhotos = status.PodContainer.Pictures.Count;
                                if(status.PodContainer.ShowSignature) //increase NumOfPhotos if signature exists
								{
                                    result.NumOfPhotos++;
								}
                                return result;
                            }
                        }
                    }
                }

            }
            return result;
        }

        public TrayHistoryPodContainer GetTrayHistoryPodContainer(string type, string orderId, string barcode = null)
        {
            var podContainer = new TrayHistoryPodContainer() { Pictures = new List<TrayHistoryPod>(), Barcodes = new List<TrayHistoryBarcode>() };
            var orderStep = _context.WWRouteSteps.Where(x => x.Type == type && x.WWOrderId == orderId)
                .Include(x => x.WWTrackingData)
                .Include(x => x.WWTrackingData.WWDriver)
                .Include(x => x.WWTrackingData.WWVehicle)
                .Include(x => x.WWTrackingData.WWPodContainer)
                .Include(x => x.WWTrackingData.WWPodContainer.Note)
                .Include(x => x.WWTrackingData.WWPodContainer.Pictures)
                .Include(x => x.WWTrackingData.WWPodContainer.Signatures)
                .Include(x => x.WWTrackingData.WWPodContainer.Barcodes)
                .FirstOrDefault();
            if (orderStep != null && orderStep.WWTrackingData != null)
            {
                if (orderStep.WWTrackingData.WWDriver != null) //get driver info
                {
                    podContainer.Driver = orderStep.WWTrackingData.WWDriver.Name;
                    podContainer.ShowVehicleAndDriver = true;
                }
                if (orderStep.WWTrackingData.WWVehicle != null) //get vehicle info
                {
                    podContainer.Vehicle = orderStep.WWTrackingData.WWVehicle.ExternalId;
                    podContainer.ShowVehicleAndDriver = true;
                }
                if (orderStep.WWTrackingData.WWPodContainer != null)
                {
                    if (orderStep.WWTrackingData.WWPodContainer.Note != null) //get note
                    {
                        podContainer.Note = orderStep.WWTrackingData.WWPodContainer.Note.Text;
                        podContainer.ShowNote = true;

                    }
                    if (orderStep.WWTrackingData.WWPodContainer.AdditionalNote != null)
                    {
                        podContainer.AdditionalNote = orderStep.WWTrackingData.WWPodContainer.AdditionalNote;
                        podContainer.ShowAdditionalNote = true;
                    }
                    if (orderStep.WWTrackingData.WWPodContainer.Signatures != null && orderStep.WWTrackingData.WWPodContainer.Signatures.Count > 0)//get signature
                    {
                        var signature = orderStep.WWTrackingData.WWPodContainer.Signatures.First();
                        var pod = _context.WWPods.Find(signature.WWPodId);
                        if (pod != null && pod.GCImageId.HasValue)
                        {
                            //var image = _context.GCImages.Where(x => x.ParentId == pod.GCImageId.Value && x.Type == (int)Enums.GCImageSizeType.SizeMax200).FirstOrDefault();
                            var image = _context.GCImages.Find(pod?.GCImageId);
                            if (image != null)
                            {
                                var signaturePod = new TrayHistoryPod() { Token = pod.Token, Url = wwImageUrl + pod.Token, Data = $"data:image/jpg;base64,{Convert.ToBase64String(image.Data)}", ImageId = image.Id };
                                podContainer.Signature = signaturePod;
                                podContainer.ShowSignature = true;
                            }
                        }

                    }
                    if (orderStep.WWTrackingData.WWPodContainer.Pictures != null && orderStep.WWTrackingData.WWPodContainer.Pictures.Count > 0) //get pictures
                    {
                        podContainer.ShowPictures = true;
                        foreach (var picture in orderStep.WWTrackingData.WWPodContainer.Pictures)
                        {
                            var pod = _context.WWPods.Find(picture.WWPodId);
                            if (pod != null && pod.GCImageId.HasValue)
                            {
                                var image = _context.GCImages.Where(x => x.ParentId == pod.GCImageId.Value && x.Type == (int)Enums.GCImageSizeType.SizeMax200).FirstOrDefault();
                                if (image != null)
                                {
                                    var picturePod = new TrayHistoryPod() { Token = pod.Token, Url = wwImageUrl + pod.Token, Data = $"data:image/jpg;base64,{Convert.ToBase64String(image.Data)}", ImageId = (int)image.ParentId };
                                    podContainer.Pictures.Add(picturePod);
                                }
                            }
                        }
                    }

                    if (orderStep.WWTrackingData.WWPodContainer.Barcodes != null && orderStep.WWTrackingData.WWPodContainer.Barcodes.Count > 0) //get barcodes
                    {

                        if (barcode != null)
                        {
                            var trayBarcode = orderStep.WWTrackingData.WWPodContainer.Barcodes.Where(x => x.Barcode == barcode).FirstOrDefault(); //find barcode for current tray
                            if (trayBarcode != null) //if that barcode exists in ww tracking data, add it to model
                            {
                                var brcd = new TrayHistoryBarcode() { Barcode = trayBarcode.Barcode, Status = trayBarcode.BarcodeStatus };
                                podContainer.Barcodes.Add(brcd);
                                podContainer.ShowBarcodes = true;
                            }
                        }

                        //podContainer.ShowBarcodes = true;
                        //foreach (var bc in orderStep.WWTrackingData.WWPodContainer.Barcodes)
                        //{
                        //    var brcd = new TrayHistoryBarcode() { Barcode = bc.Barcode, Status = bc.BarcodeStatus };
                        //    podContainer.Barcodes.Add(brcd);
                        //}
                    }

                }
            }
            return podContainer;
        }
        /// <summary>
        /// Get pod container with Pictures, Barcodes, Note and Additional Note
        /// </summary>
        /// <param name="statusId">Specifies status for which to get pod container</param>
        /// <returns></returns>
        public TrayHistoryPodContainer GetFullPodContainerForStatus(int statusId)
        {
            var trayHistoryPodContainer = new TrayHistoryPodContainer() { Pictures = new List<TrayHistoryPod>(), Barcodes = new List<TrayHistoryBarcode>() };
            var status = _context.GCTraysLifecycleStatusTypes.Find(statusId);
            if (status != null)
            {
                var podContainer = _context.WWPodContainers.Where(x => x.Id == status.WWPodContainerId).Include(x => x.Pictures).Include(x => x.Barcodes).FirstOrDefault();
                if (podContainer != null)
                {
                    if (podContainer.Pictures != null && podContainer.Pictures.Count > 0) //get pictures
                    {
                        trayHistoryPodContainer.ShowPictures = true;
                        foreach (var picture in podContainer.Pictures)
                        {
                            var pod = _context.WWPods.Find(picture.WWPodId);
                            if (pod != null && pod.GCImageId.HasValue)
                            {
                                var image = _context.GCImages.Where(x => x.ParentId == pod.GCImageId.Value && x.Type == (int)Enums.GCImageSizeType.SizeMax200).FirstOrDefault();
                                if (image != null)
                                {
                                    var picturePod = new TrayHistoryPod() { Token = pod.Token, Url = wwImageUrl + pod.Token, Data = $"data:image/png;base64,{Convert.ToBase64String(image.Data)}", ImageId = image.ParentId };
                                    trayHistoryPodContainer.Pictures.Add(picturePod);
                                }
                            }
                        }
                    }

                    if (podContainer.Barcodes != null && podContainer.Barcodes.Count > 0) //get barcodes
                    {
                        trayHistoryPodContainer.ShowBarcodes = true;
                        foreach (var barcode in podContainer.Barcodes)
                        {
                            var brcd = new TrayHistoryBarcode() { Barcode = barcode.Barcode, Status = barcode.BarcodeStatus };
                            trayHistoryPodContainer.Barcodes.Add(brcd);
                        }
                    }

                    if (podContainer.Note?.Text != null)
                    {
                        trayHistoryPodContainer.Note = podContainer.Note.Text;
                        trayHistoryPodContainer.ShowNote = true;
                    }
                    if (podContainer.AdditionalNote != null)
                    {
                        trayHistoryPodContainer.AdditionalNote = podContainer.AdditionalNote;
                        trayHistoryPodContainer.ShowAdditionalNote = true;
                    }


                }
            }
            return trayHistoryPodContainer;
        }

        public TrayHistoryPodContainer GetFullPodContainerForAssemblyStatus(int statusId)
        {
            var trayHistoryPodContainer = new TrayHistoryPodContainer() { Pictures = new List<TrayHistoryPod>() };
            var status = _context.GCTraysLifecycleStatusTypes.Find(statusId);
            if (status != null)
            {
                var assemblyGraphics = _context.CTAssemblyGraphics.Where(x => x.LifecycleStatusTypeId == statusId).ToList();
                if (assemblyGraphics != null && assemblyGraphics.Count > 0)
                {
                    trayHistoryPodContainer.ShowPictures = true;
                    foreach (var graphic in assemblyGraphics)
                    {
                        if (graphic.GCImageId != null)
                        {
                            var image = _context.GCImages.Where(x => x.ParentId == graphic.GCImageId && x.Type == (int)Enums.GCImageSizeType.SizeMax200).FirstOrDefault();
                            if (image != null)
                            {
                                var picturePod = new TrayHistoryPod() { Data = $"data:image/png;base64,{Convert.ToBase64String(image.Data)}", ImageId = image.ParentId };
                                trayHistoryPodContainer.Pictures.Add(picturePod);
                            }
                        }
                    }
                }
            }
            return trayHistoryPodContainer;
        }

        public AssemblyCountSheet GetAssemblyCountSheet(int statusId)
        {
            AssemblyCountSheet result = new AssemblyCountSheet() { AssemblyCountSheets = new List<CTAssemblyCountSheet>() };
            var countSheets = _context.CTAssemblyCountSheets.Where(x => x.TrayLifecycleStatusTypeId == statusId).ToList();
            if (countSheets != null && countSheets.Count > 0)
            {
                result.AssemblyCountSheets = countSheets;
            }
            return result;
        }

        private TrayHistoryVerificationQualityFeed GetTrayHistoryVerificationQualityFeed(int verificationLcId)
        {
            TrayHistoryVerificationQualityFeed result = null;

            var qualityFeed = _context.CTQualityFeedItems
                .Include(x => x.CTQualityFeedItemGraphics).ThenInclude(x => x.GCImage)
                .Where(x => x.GCTraysLifecycleStatusTypeId == verificationLcId).Include(x => x.GCTraysLifecycleStatusType).FirstOrDefault();
            if(qualityFeed != null)
            {
                result = new TrayHistoryVerificationQualityFeed()
                {
                    AssetName = qualityFeed.AssetName,
                    ReportedBy = qualityFeed.ReportedBy,
                    ResponsibleParty = qualityFeed.ResponsibleParty,
                    ReportType = qualityFeed.ReportType,
                    UpdatedDateUtc = qualityFeed.UpdatedDateUtc,
                    ImagesData = qualityFeed.CTQualityFeedItemGraphics == null ? null : qualityFeed.CTQualityFeedItemGraphics.Select(x => x.GCImage != null ? $"data:image/png;base64,{Convert.ToBase64String(x.GCImage.Data)}" : null).ToList()
                };
            }
            return result;
        }

        private TrayHistorySterilizeBIResult GetSterilizeBIResult(int statusId)
        {
            TrayHistorySterilizeBIResult result = null;

            var sterilizeStatus = _context.GCTraysLifecycleStatusTypes.Include(x => x.GCTray).ThenInclude(x => x.CTContainerTypeActual).Where(x => x.Id == statusId).FirstOrDefault();
            if (sterilizeStatus != null && sterilizeStatus.GCTray != null && sterilizeStatus.GCTray.CTContainerTypeActualId.HasValue && sterilizeStatus.GCTray.CTContainerTypeActualId.Value > 0)
            {
                bool actualHasBIResults = _context.CTBILoadResultContents.Any(x => x.CTContainerTypeActualId == sterilizeStatus.GCTray.CTContainerTypeActualId.Value);

                if (actualHasBIResults)
                {
                    //var all = _context.CTBILoadResults.ToList();

                    var ctBIResult = _context.CTBILoadResults
                        //.Include(x => x.CTBILoadResultContents)
                        .Include(x => x.CTBILoadResultIndicators)
                        .Include(x => x.CTBILoadResultGraphics).ThenInclude(x => x.GCImage)
                        .Where(x =>
                            x.LoadTimestamp < sterilizeStatus.Timestamp.AddSeconds(5) &&
                            x.LoadTimestamp > sterilizeStatus.Timestamp.AddSeconds(-5)).FirstOrDefault();

                    //var ctBILoadResultContent = _context.CTBILoadResultContents.Include(x => x.CTBILoadResultId).Where(x => 
                    //    x.CTContainerTypeActualId == sterilizeStatus.GCTray.CTContainerTypeActualId.Value &&
                    //    (x.CTBILoadResult.LoadTimestamp > sterilizeStatus.Timestamp.AddHours(-3)) && x.CTBILoadResult.LoadTimestamp < sterilizeStatus.Timestamp.AddHours(3))
                    //    .OrderByDescending(x => x.CreatedAtUtc).FirstOrDefault();


                    if (ctBIResult != null)
                    {
                        var ctBILoadResultContent = _context.CTBILoadResultContents.Where(x => x.CTContainerTypeActualId == sterilizeStatus.GCTray.CTContainerTypeActualId.Value).FirstOrDefault();

                        if (ctBILoadResultContent != null)
                        {
                            //var biLoadResultId = ctBILoadResultContent.CTBILoadResultId;

                            //var ctBIResult = _context.CTBILoadResults
                            //        .Include(x => x.CTBILoadResultContents)
                            //        .Include(x => x.CTBILoadResultIndicators)
                            //        .Include(x => x.CTBILoadResultGraphics).ThenInclude(x => x.GCImage)
                            //    .Where(x => x.Id == biLoadResultId).FirstOrDefault();


                            result = new TrayHistorySterilizeBIResult()
                            {
                                LoadNumber = ctBIResult.LoadBarcode,
                                ResultFlag = ctBIResult.ResultFlag,
                                Result = !string.IsNullOrWhiteSpace(ctBIResult.ResultFlag) && ctBIResult.ResultFlag.Equals("P") ? "Pass" : !string.IsNullOrWhiteSpace(ctBIResult.ResultFlag) && ctBIResult.ResultFlag.Equals("F") ? "Fail" : "Unknown",
                                CTContainerTypeActualId = sterilizeStatus.GCTray.CTContainerTypeActualId.Value,
                                CTContainerTypeActualName = $"{sterilizeStatus.GCTray.CTContainerTypeActual.ParentName} {sterilizeStatus.GCTray.CTContainerTypeActual.Name}",
                                Indicators = ctBIResult.CTBILoadResultIndicators == null ? null : ctBIResult.CTBILoadResultIndicators.OrderBy(x => x.DisplayOrder).Select(x =>
                                new TrayHistorySterilizeBIIndicator()
                                {
                                    Name = x.IndicatorName,
                                    Result = x.ResultFlag == null ?
                                        (string.Empty) :
                                        (x.ResultFlag == "P" ? x.PassLabel : x.FailLabel),
                                    Product = x.DefaultProduct,
                                    LotNumber = x.IndicatorLot,
                                    Order = x.DisplayOrder
                                }).ToList(),
                               
                                ImagesData = ctBIResult.CTBILoadResultGraphics == null ? null : ctBIResult.CTBILoadResultGraphics.Select(x => x.GCImage != null ? $"data:image/png;base64,{Convert.ToBase64String(x.GCImage.Data)}" : null).ToList()
                            };
                        }
                    }
                }
            }

            return result;
        }


        /// <summary>
        /// Gets only pod container with Note
        /// </summary>
        /// <param name="statusId">Specifies status for which to get pod container</param>
        /// <returns></returns>
        public WWPodContainer GetPodContainerForStatus(int statusId)
        {
            var status = _context.GCTraysLifecycleStatusTypes.Find(statusId);
            if (status != null)
            {
                var podContainer = _context.WWPodContainers.Where(x => x.Id == status.WWPodContainerId).Include(x => x.Note).FirstOrDefault();

                return podContainer;

            }

            return null;
        }

        public int? GetActualIdByTrayBarcode(string barcode)
        {
            var gcTray = _context.GCTrays.Where(x => x.Barcode == barcode).FirstOrDefault();

            if (gcTray != null)
                return gcTray.CTContainerTypeActualId;
            return null;
        }

        public int? GetTrayIdByBarcode(string barcode)
        {
            var gcTray = _context.GCTrays.Where(x => x.Barcode == barcode).FirstOrDefault();

            if (gcTray != null)
                return gcTray.Id;
            return null;
        }

        public List<GCTray> CheckTrayBarcodeForCustomer(string code, int customerId)
        {
            var cases = GetCasesByCustomer(customerId);
            var date = DateTime.Now;
            var todaysCases = cases.Where(x => x.CTCase.DueTimestamp.TrimHoursMinutesSeconds() == date.TrimHoursMinutesSeconds()).ToList();

            List<GCTray> trays = new List<GCTray>();
            foreach (var item in todaysCases)
            {
                var traysByCase = _context.GCTrays.Where(x => x.GCCaseId == item.Id).ToList();
                trays.AddRange(traysByCase);
            }
            var trayList = trays.Where(x => x.Barcode == code).ToList();

            return trayList;
        }


        public int? SetTrayStatus(string code, string lifecycleStatus, int? trayId, ClaimsPrincipal user = null)
        {
            GCTray tray = new GCTray();
            if (trayId != null)
            {
                tray = _context.GCTrays.Where(x => x.Id == trayId /*&& x.IsActiveStatus == true*/).FirstOrDefault();
            }
            else
            {
                tray = _context.GCTrays.Where(x => x.Barcode == code /*&& x.IsActiveStatus == true*/).FirstOrDefault();
            }

            if (tray != null)
            {
                var trayLifecycleStatusOld = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id && x.IsActiveStatus == true).FirstOrDefault();

                //var statusOld = _context.GCLifecycleStatusTypes.Where(x => x.Id == trayLifecycleStatusOld.LifecycleStatusTypeId).FirstOrDefault();
                var status = _context.GCLifecycleStatusTypes.Where(x => x.Title == lifecycleStatus).FirstOrDefault();

                var wasInStatus = CheckTrayWasInStatus("Pickup", tray.Id);

                //if (statusOld.Order < status.Order)
                if (!wasInStatus)
                {
                    if (trayLifecycleStatusOld != null)
                    {
                        trayLifecycleStatusOld.IsActiveStatus = false;
                        _context.GCTraysLifecycleStatusTypes.Update(trayLifecycleStatusOld);
                    }

                    GCTraysLifecycleStatusTypes trayLifecycleStatus = new GCTraysLifecycleStatusTypes
                    {
                        GCTrayId = tray.Id,
                        LifecycleStatusTypeId = status.Id,
                        IsActiveStatus = true,
                        Timestamp = DateTime.UtcNow,
                        User = user.Identity.Name,
                        TimestampOriginal = DateTime.MinValue
                    };

                    _context.GCTraysLifecycleStatusTypes.Add(trayLifecycleStatus);
                    tray.GCLifecycleStatusTypeId = status.Id;

                    _context.SaveChanges();

                    UpdateGCCaseLifecycleStatus((int)tray.GCCaseId);

                    return tray.Id;
                }
            }

            return null;
        }
        /// <summary>
        /// Set new tray status
        /// </summary>
        /// <param name="gcTrayId">tray for change</param>
        /// <param name="statusCode">code of new status</param>
        /// <param name="user"></param>
        public void SetTrayStatus(int gcTrayId, int statusCode, ClaimsPrincipal user)
		{
            var gcTray = _context.GCTrays.Where(x => x.Id == gcTrayId).FirstOrDefault();

            if (gcTray != null)
            {
                //get previous lifecycle status
                var trayLifecycleStatusOld = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == gcTrayId && x.IsActiveStatus == true).FirstOrDefault();
                var status = _context.GCLifecycleStatusTypes.Where(x => x.Code == statusCode).FirstOrDefault();

                //check if tray already has this new status
                bool alreadyInStatus = false ;
                var trayLifecyclestatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.LifecycleStatusTypeId == status.Id && x.GCTrayId == gcTrayId).FirstOrDefault();

                if (trayLifecyclestatus != null)
                {
                    alreadyInStatus = true;
                }
                else
                {
                    alreadyInStatus = false;
                }
                //if tray doesn't have this status, create it
                if (!alreadyInStatus)
                {
                    //invalidate previous status
                    if (trayLifecycleStatusOld != null)
                    {
                        trayLifecycleStatusOld.IsActiveStatus = false;
                        _context.GCTraysLifecycleStatusTypes.Update(trayLifecycleStatusOld);
                    }

                    GCTraysLifecycleStatusTypes trayLifecycleStatus = new GCTraysLifecycleStatusTypes
                    {
                        GCTrayId = gcTrayId,
                        LifecycleStatusTypeId = status.Id,
                        IsActiveStatus = true,
                        Timestamp = DateTime.UtcNow,
                        User = user.Identity.Name,
                        TimestampOriginal = DateTime.MinValue
                    };

                    _context.GCTraysLifecycleStatusTypes.Add(trayLifecycleStatus);
                    gcTray.GCLifecycleStatusTypeId = status.Id;

                    _context.SaveChanges();
                    //update status for entire case if necessary 
                    UpdateGCCaseLifecycleStatus((int)gcTray.GCCaseId);
                }
            }
        }

        public void Foamed(int trayId, string code, bool isYes)
        {
            var lifecycleStatus = _context.GCLifecycleStatusTypes.Where(x => x.Title == "Pickup").FirstOrDefault();

            //var tray = _context.GCTrays.Where(x => x.Barcode == code && x.IsActiveStatus == true).FirstOrDefault();
            var tray = _context.GCTrays.Find(trayId);

            if (tray != null)
            {
                var gcTrayLifecycleStatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id && x.LifecycleStatusTypeId == lifecycleStatus.Id && x.IsActiveStatus == true).FirstOrDefault();

                if (gcTrayLifecycleStatus != null)
                {
                    if (gcTrayLifecycleStatus.WWPodContainerId == null)
                    {
                        WWPod pod = new WWPod();
                        _context.WWPods.Add(pod);

                        WWPodContainer podContainer = new WWPodContainer();

                        podContainer.Note = pod;

                        if (isYes)
                        {
                            podContainer.AdditionalNote = "Yes";
                        }
                        else
                        {
                            podContainer.AdditionalNote = "No";
                        }

                        _context.WWPodContainers.Add(podContainer);

                        gcTrayLifecycleStatus.WWPodContainer = podContainer;
                        _context.GCTraysLifecycleStatusTypes.Update(gcTrayLifecycleStatus);

                        _context.SaveChanges();
                    }
                    else
                    {
                        var podContainer = _context.WWPodContainers.Find(gcTrayLifecycleStatus.WWPodContainerId);

                        if (isYes)
                        {
                            podContainer.AdditionalNote = "Yes";
                        }
                        else
                        {
                            podContainer.AdditionalNote = "No";
                        }

                        _context.WWPodContainers.Update(podContainer);

                        _context.SaveChanges();
                    }
                }
            }
        }

        public void StoreNote(string note, int trayId)
        {
            //code = "C1062";
            var lifecycleStatus = _context.GCLifecycleStatusTypes.Where(x => x.Title == "Pickup").FirstOrDefault();

            //var tray = _context.GCTrays.Where(x => x.Barcode == code).FirstOrDefault();
            var tray = _context.GCTrays.Find(trayId);

            var gcTrayLifecycleStatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id && x.LifecycleStatusTypeId == lifecycleStatus.Id && x.IsActiveStatus == true).FirstOrDefault();

            if (gcTrayLifecycleStatus != null)
            {
                if (gcTrayLifecycleStatus.WWPodContainerId == null)
                {
                    WWPod pod = new WWPod();
                    pod.Text = note;
                    _context.WWPods.Add(pod);

                    WWPodContainer podContainer = new WWPodContainer();
                    podContainer.Note = pod;

                    _context.WWPodContainers.Add(podContainer);

                    gcTrayLifecycleStatus.WWPodContainer = podContainer;
                    _context.GCTraysLifecycleStatusTypes.Update(gcTrayLifecycleStatus);

                    _context.SaveChanges();
                }
                else
                {
                    WWPod pod = new WWPod();
                    pod.Text = note;
                    _context.WWPods.Add(pod);

                    var podContainer = _context.WWPodContainers.Where(x => x.Id == gcTrayLifecycleStatus.WWPodContainerId).FirstOrDefault();

                    podContainer.Note = pod;
                    _context.WWPodContainers.Update(podContainer);

                    //var pod = _context.WWPods.Find(podContainer.NoteId);
                    //pod.Text = note;

                    //_context.WWPods.Update(pod);

                    _context.SaveChanges();
                }
            }
        }

        public void StorePictureInPodContainer(byte[] bytes, string fileName, int trayId)
        {
            var status = _context.GCLifecycleStatusTypes.Where(x => x.Title == "Pickup").FirstOrDefault();

            var tray = _context.GCTrays.Find(trayId);

            GCTraysLifecycleStatusTypes gcTrayLifecycleStatus = new GCTraysLifecycleStatusTypes();
            if (tray != null)
            {
                gcTrayLifecycleStatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id && x.LifecycleStatusTypeId == status.Id && x.IsActiveStatus == true).FirstOrDefault();
            }

            if (gcTrayLifecycleStatus != null)
            {
                if (gcTrayLifecycleStatus.WWPodContainerId == null)
                {
                    WWPod pod = new WWPod();
                    _context.WWPods.Add(pod);

                    WWPodContainer podContainer = new WWPodContainer();
                    //podContainer.Note = pod;

                    _context.WWPodContainers.Add(podContainer);

                    gcTrayLifecycleStatus.WWPodContainer = podContainer;
                    _context.GCTraysLifecycleStatusTypes.Update(gcTrayLifecycleStatus);

                    _context.SaveChanges();

                    //url
                    WWPicture picture = new WWPicture()
                    {
                        Url = fileName,
                        WWPodContainer = podContainer,
                        WWPod = pod
                    };

                    _context.WWPictures.Add(picture);

                    //bytes
                    GCImage image = new GCImage()
                    {
                        Data = bytes,
                        Origin = "TP",
                        CreatedAtUtc = DateTime.UtcNow
                    };

                    _context.GCImages.Add(image);
                    _groundControlService.CreateThumbnailsForGCImage(image);

                    pod.GCImage = image;
                    _context.WWPods.Update(pod);

                    _context.SaveChanges();
                }
                else
                {
                    var podContainer = _context.WWPodContainers.Where(x => x.Id == gcTrayLifecycleStatus.WWPodContainerId).FirstOrDefault();

                    WWPod pod = new WWPod();
                    _context.WWPods.Add(pod);

                    _context.SaveChanges();

                    //url
                    WWPicture picture = new WWPicture()
                    {
                        Url = fileName,
                        WWPodContainer = podContainer,
                        WWPod = pod
                    };

                    _context.WWPictures.Add(picture);

                    //bytes
                    GCImage image = new GCImage()
                    {
                        Data = bytes,
                        Origin = "TP",
                        CreatedAtUtc = DateTime.UtcNow
                    };

                    _context.GCImages.Add(image);
                    _groundControlService.CreateThumbnailsForGCImage(image);

                    pod.GCImage = image;
                    _context.WWPods.Update(pod);

                    _context.SaveChanges();
                }
            }
        }


        //public void StorePictureInDatabase(byte[] bytes, string code)
        //{
        //    var status = _context.GCLifecycleStatusTypes.Where(x => x.Title == "Prepped").FirstOrDefault();
        //    var tray = _context.GCTrays.Where(x => x.Barcode == code).FirstOrDefault();

        //    GCTraysLifecycleStatusTypes gcTrayLifecycleStatus = new GCTraysLifecycleStatusTypes();
        //    if (tray != null)
        //    {
        //        gcTrayLifecycleStatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id && x.LifecycleStatusTypeId == status.Id && x.IsActiveStatus == true).FirstOrDefault();
        //    }

        //    if (gcTrayLifecycleStatus != null)
        //    {
        //        if (gcTrayLifecycleStatus.WWPodContainerId == null)
        //        {
        //            WWPod pod = new WWPod();
        //            _context.WWPods.Add(pod);

        //            WWPodContainer podContainer = new WWPodContainer();
        //            podContainer.Note = pod;

        //            _context.WWPodContainers.Add(podContainer);

        //            gcTrayLifecycleStatus.WWPodContainer = podContainer;
        //            _context.GCTraysLifecycleStatusTypes.Update(gcTrayLifecycleStatus);

        //            GCImage image = new GCImage()
        //            {
        //                Data = bytes,
        //                Origin = "TP",
        //                CreatedAtUtc = DateTime.UtcNow
        //            };

        //            _context.GCImages.Add(image);

        //            pod.GCImage = image;

        //            _context.SaveChanges();
        //        }
        //        else
        //        {
        //            var podContainer = _context.WWPodContainers.Where(x => x.Id == gcTrayLifecycleStatus.WWPodContainerId).Include(x => x.Note).FirstOrDefault();

        //            GCImage image = new GCImage()
        //            {
        //                Data = bytes,
        //                Origin = "TP",
        //                CreatedAtUtc = DateTime.UtcNow
        //            };

        //            var pod = _context.WWPods.Find(podContainer.NoteId);
        //            pod.GCImage = image;

        //            _context.WWPods.Update(pod);

        //            _context.SaveChanges();
        //        }
        //    }
        //}

        /// <summary>
        /// Returns previous history for each case
        /// </summary>
        /// <param name="cases">Has to be descending ordered list of cases</param>
        /// <returns>Update list of cases</returns>
        public List<TrayHistoryCase> GetPreviousTrayHistory(List<TrayHistoryCase> cases, int orderToStartFrom)
        {
            if (cases.Count > 1)
            {
                for (int i = 0; i < cases.Count - 1; i++)
                {
                    if (!cases[i].isLoaner) //get previous tray history only for non-loaner trays
                    {
                        var currentCase = cases[i];
                        var previousCase = cases[i + 1];
                        cases[i].PreviousLifecycles = previousCase.TrayLifecycles.Where(x => x.LifecycleOrder >= orderToStartFrom).ToList();
                    }

                }

            }
            return cases;
        }

        public List<CTContainerTypeActual> GetAllActuals()
        {
            var result = _context.CTContainerTypeActuals.ToList();
            return result;
        }

        //This method can be only used to update Barcodes for Pickup step (on customer side)
        public async Task UpdateBarcodeToWWAsync(int gcTrayId)
        {
            var tray = _context.GCTrays.Find(gcTrayId);
            if (tray != null)
            {
                var gcCase = _context.GCCases.Find(tray.GCCaseId);

                if (gcCase != null)
                {
                    var route = _context.WWRoutes.Include(x => x.Vehicle).FirstOrDefault(x => x.Id == gcCase.WWPickupRouteId);
                    if (route != null)
                    {
                        BarcodeExecutionEvents barcodeEvents = new BarcodeExecutionEvents() { Events = new List<ExecutionEventBarcode>() };
                        ExecutionEventBarcode barcodeEvent = new ExecutionEventBarcode()
                        {
                            EventType = ExecutionEventType.podBarcodes,
                            OrderId = gcCase.IncomingOrderId,
                            StepType = OrderStepType.pickup,
                            VehicleId = route.Vehicle.WWVehicleId,
                            Date = route.Date.ToWorkWaveDate()
                        };
                        EEDataPodBarcodes dataPodBarcodes = new EEDataPodBarcodes() { Barcodes = new List<EEPodBarcodes>() };

                        TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                        DateTime dateTimeNow = TimeZoneInfo.ConvertTime(DateTime.Now, cstZone);

                        //DateTime dateTimeNow = ProfitOptics.Modules.Sox.Helpers.Helpers.GetLocalDateTime(DateTime.Now);
                        //EEPodBarcodes podBarcode = new EEPodBarcodes() { Barcode = tray.Barcode, BarcodeStatusType = BarcodeStatusType.SCANNED, Sec = (int)dateTimeNow.TimeOfDay.TotalSeconds };

                        var trays = GetTraysForGCCase((int)tray.GCCaseId);
                        //List<GCTray> scannedTrays = new List<GCTray>();
                        foreach (var item in trays)
                        {
                            if (CheckTrayWasInStatus("Pickup", item.Id))
                            {
                                //scannedTrays.Add(item);
                                EEPodBarcodes podBarcode = new EEPodBarcodes() { Barcode = item.Barcode, BarcodeStatusType = BarcodeStatusType.SCANNED, Sec = (int)dateTimeNow.TimeOfDay.TotalSeconds };
                                dataPodBarcodes.Barcodes.Add(podBarcode);
                            }
                        }

                        //dataPodBarcodes.Barcodes.Add(podBarcode);
                        barcodeEvent.Data = dataPodBarcodes;
                        barcodeEvents.Events.Add(barcodeEvent);
                        await WorkWaveClient.SendBarcodeExecutionEvent(barcodeEvents);
                    }
                }
            }
        }

        //This method can be only used to update order status for Pickup step (on customer side)
        public async Task SetOrderStatusInWWAsync(int caseId, RouteStepStatusType? stepStatus)
        {

            var gcCase = _context.GCCases.Find(caseId);

            if (gcCase != null)
            {
                var route = _context.WWRoutes.Include(x => x.Vehicle).FirstOrDefault(x => x.Id == gcCase.WWPickupRouteId);
                if (route != null)
                {
                    StatusUpdateExecutionEvents events = new StatusUpdateExecutionEvents() { Events = new List<ExecutionEventStatusUpdate>() };
                    ExecutionEventStatusUpdate singleEvent = new ExecutionEventStatusUpdate()
                    {
                        EventType = ExecutionEventType.statusUpdate,
                        OrderId = gcCase.IncomingOrderId,
                        StepType = OrderStepType.pickup,
                        VehicleId = route.Vehicle.WWVehicleId,
                        Date = route.Date.ToWorkWaveDate()
                    };

                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                    DateTime dateTimeNow = TimeZoneInfo.ConvertTime(DateTime.Now, cstZone);

                    //DateTime dateTimeNow = ProfitOptics.Modules.Sox.Helpers.Helpers.GetLocalDateTime(DateTime.Now);

                    EEDataStatusUpdate statusUpdate = new EEDataStatusUpdate() { StepStatus = stepStatus, Sec = (int)dateTimeNow.TimeOfDay.TotalSeconds };
                    singleEvent.Data = statusUpdate;
                    events.Events.Add(singleEvent);
                    await WorkWaveClient.SendStatusUpdateExecutionEvent(events);
                }

            }

        }

        public async Task SetOrderStatusInWWIfTraysArePickedUp(int trayId)
        {
            //var tray = _context.GCTrays.Where(x => x.Barcode == code).FirstOrDefault();
            var tray = _context.GCTrays.Find(trayId);

            var traysForCase = _context.GCTrays.Where(x => x.GCCaseId == tray.GCCaseId).ToList();

            var count = 0;

            foreach (var item in traysForCase)
            {
                var trayLifecycleStatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == item.Id && x.IsActiveStatus == true).FirstOrDefault();
                var status = _context.GCLifecycleStatusTypes.Where(x => x.Id == trayLifecycleStatus.LifecycleStatusTypeId).FirstOrDefault();

                var pickupStatus = _context.GCLifecycleStatusTypes.Where(x => x.Title == "Pickup").FirstOrDefault();
                if (status.Order >= pickupStatus.Order)
                {
                    count++;
                }
            }

            if (count == traysForCase.Count())
                await SetOrderStatusInWWAsync((int)tray.GCCaseId, RouteStepStatusType.done);
        }

        public bool CheckTrayWasInStatus(string lifecycleStatus, int trayId)
        {
            var status = _context.GCLifecycleStatusTypes.Where(x => x.Title == lifecycleStatus).FirstOrDefault();
            var trayLifecyclestatus = _context.GCTraysLifecycleStatusTypes.Where(x => x.LifecycleStatusTypeId == status.Id && x.GCTrayId == trayId).FirstOrDefault();

            if (trayLifecyclestatus != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckIfCaseWasInStatus(LifecycleStatusType lifecycleStatus, int caseId)
        {
            var status = _context.GCLifecycleStatusTypes.Where(x => x.Code == (int)lifecycleStatus).FirstOrDefault();
            var caseLifecyclestatus = _context.GCCasesLifecycleStatusTypes.Where(x => x.LifecycleStatusTypeId == status.Id && x.GCCaseId == caseId).FirstOrDefault();

            if (caseLifecyclestatus != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetVendorIdsForCases()
        {
            var cases = _context.GCCases.ToList();
            foreach (var item in cases)
            {
                GCVendor vendor = null;
                var tray = _context.GCTrays.Where(x => x.GCCaseId == item.Id).FirstOrDefault();
                if (tray != null)
                {
                    var ctContainerType = _context.CTContainerTypes.Where(x => x.Id == tray.CTContainerTypeId).FirstOrDefault();

                    if (ctContainerType != null)
                        vendor = _context.GCVendors.Where(x => x.Title == ctContainerType.VendorName).FirstOrDefault();
                }

                if (vendor != null)
                {
                    item.GCVendorId = vendor.Id;
                    _context.GCCases.Update(item);
                }
            }

            _context.SaveChanges();
        }

        public void SetDueTimeForGCCases()
        {
            var cases = _context.GCCases.ToList();
            foreach (var item in cases)
            {
                var ctCase = _context.CTCases.Find(item.CTCaseId);
                item.DueTimestamp = ctCase.DueTimestamp;

                _context.GCCases.Update(item);
            }

            _context.SaveChanges();
        }

        public ETLLogsModel ETLLogs()
        {
            ETLLogsModel logs = new ETLLogsModel();
            logs.ETLLogs = new List<ETLLogModel>();
            var etlLogs = _context.ETLLogs.OrderByDescending(p => p.Id).Take(50);

            foreach(var log in etlLogs)
            {
                var logModel = _mapper.Map<ETLLogModel>(log);

                logModel.Source = Extensions.MapSource(log.Source);
                logModel.ActionType = Extensions.MapActionType(log.Source, (int)log.ActionType);

                logs.ETLLogs.Add(logModel);
            }

            return logs;
        }

        public PagedList<GCLocationInfoModel> GetLocationInfosPaged(LocationInfoPagingModel pagingModel, int? locationId, DateTime? startDate, DateTime? endDate)
        {
            IQueryable<GCLocationInfo> locationInfosQuery;
            if (pagingModel.SearchValue != "")
            {
                locationInfosQuery = _context.GCLocationInfos.Where(x => x.Humidity.ToString().Contains(pagingModel.SearchValue)
                                                                                            || x.Timestamp.ToString().Contains(pagingModel.SearchValue)
                                                                                            || x.Temperature.ToString().Contains(pagingModel.SearchValue)
                                                                                            || x.Pressure.ToString().Contains(pagingModel.SearchValue))
                        .Include(x => x.GCLocation)
                        .AsNoTracking()
                        .AsQueryable();

                if (locationId != null)
                {
                    locationInfosQuery = locationInfosQuery.Where(x => (x.Humidity.ToString().Contains(pagingModel.SearchValue)
                                                                                              || x.Timestamp.ToString().Contains(pagingModel.SearchValue)
                                                                                              || x.Temperature.ToString().Contains(pagingModel.SearchValue)
                                                                                              || x.Pressure.ToString().Contains(pagingModel.SearchValue))
                                                                         && x.GCLocationId == locationId)
                         //.Include(x => x.GCLocation)
                         .AsNoTracking()
                         .AsQueryable();
                }

                if (startDate != null && endDate != null)
                {
                    locationInfosQuery = locationInfosQuery.Where(x => (x.Humidity.ToString().Contains(pagingModel.SearchValue)
                                                                                             || x.Timestamp.ToString().Contains(pagingModel.SearchValue)
                                                                                             || x.Temperature.ToString().Contains(pagingModel.SearchValue)
                                                                                             || x.Pressure.ToString().Contains(pagingModel.SearchValue))
                                                                        && x.Timestamp >= startDate
                                                                        && x.Timestamp <= endDate)
                         //.Include(x => x.GCLocation)
                         .AsNoTracking()
                         .AsQueryable();
                }
            }
            else
            {
                locationInfosQuery = _context.GCLocationInfos
                    .Include(x => x.GCLocation)
                    .AsNoTracking()
                    .AsQueryable();

                if (locationId != null)
                {
                    locationInfosQuery = _context.GCLocationInfos.Where(x => x.GCLocationId == locationId)
                    .AsNoTracking()
                    .AsQueryable();
                }

                if (startDate != null && endDate != null)
                {
                    locationInfosQuery = locationInfosQuery.Where(x => x.Timestamp >= startDate && x.Timestamp <= endDate)
                    .AsNoTracking()
                    .AsQueryable();
                }

            }

            locationInfosQuery = locationInfosQuery.OrderByDescending(x => x.Timestamp);

            var locationInfosList = locationInfosQuery
                   .Skip((pagingModel.PageNumber - 1) * pagingModel.PageSize)
                   .Take(pagingModel.PageSize)
                   .ToList();
           

            var totalCount = locationInfosQuery.Count();

            List<GCLocationInfoModel> list = new List<GCLocationInfoModel>();
            foreach (var locInfo in locationInfosList)
            {
                var locInfoModel = _mapper.Map<GCLocationInfoModel>(locInfo);
                list.Add(locInfoModel);
            }

            var pagedList = PagedList<GCLocationInfoModel>.ToPagedList(list,
                                                     pagingModel.PageNumber,
                                                     pagingModel.PageSize,
                                                     totalCount);

            return pagedList;
        }

        public GCLocation GetGCLocationById(int locationId)
        {
            var location = _context.GCLocations.Find(locationId);

            return location;
        }

        public GCLocationsModel GetGCLocationsModel()
        {
            GCLocationsModel locationsModel = new GCLocationsModel();
            locationsModel.Locations = new List<LocationModel>();
            var locations = _context.GCLocations.ToList();

            if(locations!=null)
			{
                foreach (var location in locations)
                {
                    var locationModel = new LocationModel();
                    locationModel.GCLocaton = location;

                    var locationInfos = _context.GCLocationInfos.Where(x => x.GCLocationId == location.Id).ToList();
                    if(locationInfos!=null && locationInfos.Count>0)
					{
                        GCLocationInfo locationInfoMax = new GCLocationInfo();
                        locationInfoMax.Timestamp = DateTime.MinValue;
                        foreach (var locInfo in locationInfos)
                        {
                            if (locInfo.Timestamp > locationInfoMax.Timestamp)
                            {
                                locationInfoMax = locInfo;
                            }
                        }

                        locationModel.GCLocationInfo = locationInfoMax;

                        locationsModel.Locations.Add(locationModel);
                    }
                  
                }
            }

            return locationsModel;
        }

        public ProcedureTypesModel GetProcedureTypes(bool? includeInactive)
        {
            var result = new ProcedureTypesModel();
            IQueryable<GCProcedureType> query = _context.GCProcedureTypes;
            if (includeInactive.HasValue && includeInactive.Value)
            {
                query = query.Where(x => x.IsActive == includeInactive.Value);
            }
            var procedureTypes = query.OrderBy(x => x.Title).ToList();
            result.ProcedureTypes = _mapper.Map<List<ProcedureTypeModel>>(procedureTypes);

            return result;
        }

        public ProcedureTypeModel GetProcedureType(int procedureId)
        {
            var procedureType = _context.GCProcedureTypes.Find(procedureId);
            return _mapper.Map<ProcedureTypeModel>(procedureType);
        }

        public void UpdateProcedureType(ProcedureTypeModel model)
        {
            var procedureType = _context.GCProcedureTypes.Find(model.Id);

            _mapper.Map(model, procedureType);
            procedureType.ModifiedAtUtc = DateTime.UtcNow;

            _context.GCProcedureTypes.Update(procedureType);
            _context.SaveChanges();
        }

        public ProceduresModel GetProcedures()
        {
            var result = new ProceduresModel();
            var procedures = _context.CTContainerServices.Include(x => x.GCProcedureType).OrderBy(x => x.Name).ToList();
            result.Procedures = _mapper.Map<List<ProcedureModel>>(procedures);

            return result;
        }

        public ProcedureModel GetProcedure(int procedureId)
        {
            var procedure = _context.CTContainerServices.Find(procedureId);
            return _mapper.Map<ProcedureModel>(procedure);
        }

        public void UpdateProcedure(ProcedureModel model)
        {
            var procedure = _context.CTContainerServices.Find(model.Id);

            procedure.ICD10 = model.ICD10;
            procedure.ICD9 = model.ICD9;
            procedure.GCProcedureTypeId = model.GCProcedureTypeId;
            procedure.ModifiedAtUtc = DateTime.UtcNow;

            _context.CTContainerServices.Update(procedure);
            _context.SaveChanges();
        }

        public async Task DeleteOrdersInWWAsync(int caseId)
		{
            var orderIds = new List<string>();
            var gcCase = _context.GCCases.Find(caseId);
            if(gcCase != null)
			{
                if(gcCase.IncomingOrderId != null)
				{
                    orderIds.Add(gcCase.IncomingOrderId);
                    gcCase.IncomingOrderId = null;
                }
                if (gcCase.OutgoingOrderId != null)
                {
                    orderIds.Add(gcCase.OutgoingOrderId);
                    gcCase.OutgoingOrderId = null;
                }
            }
            await _groundControlService.DeleteOrdersInWWAsync(orderIds);
            gcCase.IsWWCanceled = true;
            _context.SaveChanges();

		}
        public async Task UpdateCaseBarcodesAsync(int caseId)
		{
            await _groundControlService.UpdateCaseBarcodesAsync(caseId);
  
		}

        public void SyncCaseHistory(int gcCaseId)
        {
            int minsToAdd = 0;
            int.TryParse(GlobalOptions.CENTRAL_TIMEZONE_OFFSET_IN_MINS, out minsToAdd);
            minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(minsToAdd);
            _groundControlService.SyncCaseHistory(gcCaseId, minsToAdd);
        }

        public void TriggerJobs(int jobNumber)
        {
            var options = new CensiTrackModuleOptions();

            switch (jobNumber)
            {
                case (int)CTActionType.GetCTCasesContainersAndSubmitToWW:
                    options.Action = configuration["CensiTrackOptions:CTCasesContainers:Action"];
                    options.ClientId = Int32.Parse(configuration["CensiTrackOptions:ClientId"]);
                    options.ContainerId = Int32.Parse(configuration["CensiTrackOptions:ContainerId"]);
                    options.FacilityId = Int32.Parse(configuration["CensiTrackOptions:FacilityId"]);
                    options.Password = configuration["CensiTrackOptions:CTCasesContainers:Password"];
                    options.ServiceId = Int32.Parse(configuration["CensiTrackOptions:ServiceId"]);
                    options.StopHistoryFromLoading = configuration["CensiTrackOptions:CTCasesContainers:StopHistoryFromLoading"];
                    options.UserName = configuration["CensiTrackOptions:CTCasesContainers:UserName"];
                    break;
                case (int)CTActionType.GetCTContainerTypeGraphicMedia:
                    options.Action = configuration["CensiTrackOptions:CTContainerTypeGraphicMedia:Action"];
                    options.ClientId = Int32.Parse(configuration["CensiTrackOptions:ClientId"]);
                    options.ContainerId = Int32.Parse(configuration["CensiTrackOptions:ContainerId"]);
                    options.FacilityId = Int32.Parse(configuration["CensiTrackOptions:FacilityId"]);
                    options.Password = configuration["CensiTrackOptions:CTContainerTypeGraphicMedia:Password"];
                    options.ServiceId = Int32.Parse(configuration["CensiTrackOptions:ServiceId"]);
                    options.UserName = configuration["CensiTrackOptions:CTContainerTypeGraphicMedia:UserName"];
                    break;
                case (int)CTActionType.GetCTLocations:
                    options.Action = configuration["CensiTrackOptions:CTLocations:Action"];
                    options.ClientId = Int32.Parse(configuration["CensiTrackOptions:ClientId"]);
                    options.ContainerId = Int32.Parse(configuration["CensiTrackOptions:ContainerId"]);
                    options.FacilityId = Int32.Parse(configuration["CensiTrackOptions:FacilityId"]);
                    options.Password = configuration["CensiTrackOptions:CTLocations:Password"];
                    options.ServiceId = Int32.Parse(configuration["CensiTrackOptions:ServiceId"]);
                    options.UserName = configuration["CensiTrackOptions:CTLocations:UserName"];
                    break;
                case (int)CTActionType.GetCTBILoadResults:
                    options.Action = configuration["CensiTrackOptions:CTBILoadResults:Action"];
                    options.ClientId = Int32.Parse(configuration["CensiTrackOptions:ClientId"]);
                    options.ContainerId = Int32.Parse(configuration["CensiTrackOptions:ContainerId"]);
                    options.FacilityId = Int32.Parse(configuration["CensiTrackOptions:FacilityId"]);
                    options.Password = configuration["CensiTrackOptions:CTBILoadResults:Password"];
                    options.ServiceId = Int32.Parse(configuration["CensiTrackOptions:ServiceId"]);
                    options.UserName = configuration["CensiTrackOptions:CTBILoadResults:UserName"];
                    break;
                case (int)CTActionType.GetContainerAssemblyMediaForActuals:
                    options.Action = configuration["CensiTrackOptions:ContainerAssemblyMediaForActuals:Action"];
                    options.ClientId = Int32.Parse(configuration["CensiTrackOptions:ClientId"]);
                    options.ContainerId = Int32.Parse(configuration["CensiTrackOptions:ContainerId"]);
                    options.FacilityId = Int32.Parse(configuration["CensiTrackOptions:FacilityId"]);
                    options.Password = configuration["CensiTrackOptions:ContainerAssemblyMediaForActuals:Password"];
                    options.ServiceId = Int32.Parse(configuration["CensiTrackOptions:ServiceId"]);
                    options.UserName = configuration["CensiTrackOptions:ContainerAssemblyMediaForActuals:UserName"];
                    break;
                case (int)CTActionType.GetCTContainerServices:
                    options.Action = configuration["CensiTrackOptions:CTContainerServices:Action"];
                    options.ClientId = Int32.Parse(configuration["CensiTrackOptions:ClientId"]);
                    options.ContainerId = Int32.Parse(configuration["CensiTrackOptions:ContainerId"]);
                    options.FacilityId = Int32.Parse(configuration["CensiTrackOptions:FacilityId"]);
                    options.Password = configuration["CensiTrackOptions:CTContainerServices:Password"];
                    options.ServiceId = Int32.Parse(configuration["CensiTrackOptions:ServiceId"]);
                    options.StopHistoryFromLoading = configuration["CensiTrackOptions:CTContainerServices:StopHistoryFromLoading"];
                    options.FetchChildObjects = configuration["CensiTrackOptions:CTContainerServices:FetchChildObjects"];
                    options.UserName = configuration["CensiTrackOptions:CTContainerServices:UserName"];
                    break;
                case (int)CTActionType.CompleteBrokenTrayHistory:
                    options.Action = configuration["CensiTrackOptions:BrokenTrayHistory:Action"];
                    options.ClientId = Int32.Parse(configuration["CensiTrackOptions:ClientId"]);
                    options.ContainerId = Int32.Parse(configuration["CensiTrackOptions:ContainerId"]);
                    options.FacilityId = Int32.Parse(configuration["CensiTrackOptions:FacilityId"]);
                    options.Password = configuration["CensiTrackOptions:BrokenTrayHistory:Password"];
                    options.ServiceId = Int32.Parse(configuration["CensiTrackOptions:ServiceId"]);
                    options.UserName = configuration["CensiTrackOptions:BrokenTrayHistory:UserName"];
                    break;
                case (int)CTActionType.CreateGCImageThumbnails:
                    options.Action = configuration["CensiTrackOptions:GCImageThumbnails:Action"];
                    options.ClientId = Int32.Parse(configuration["CensiTrackOptions:ClientId"]);
                    options.ContainerId = Int32.Parse(configuration["CensiTrackOptions:ContainerId"]);
                    options.FacilityId = Int32.Parse(configuration["CensiTrackOptions:FacilityId"]);
                    options.Password = configuration["CensiTrackOptions:GCImageThumbnails:Password"];
                    options.ServiceId = Int32.Parse(configuration["CensiTrackOptions:ServiceId"]);
                    options.UserName = configuration["CensiTrackOptions:GCImageThumbnails:UserName"];
                    break;
            }

            _groundControlService.Execute(options);
        }

        public async Task<InventoryAlerStatusModel> GetInventoryAlertCount(ClaimsPrincipal user)
        {
            // enhance this to use cases of logged user

            var inventoryAlertStatus = new InventoryAlerStatusModel();
            inventoryAlertStatus.QuarantinedTrayCount = await _context.GCTrays.CountAsync(x => x.GCLifecycleStatusTypeId.HasValue && x.GCLifecycleStatusTypeId.Value == (int)LifecycleStatusType.QualityHold);
            inventoryAlertStatus.IncompleteCountSheetCount = await _context.CTAssemblyCountSheets.CountAsync(x => x.ActualCount < x.RequiredCount);
            return inventoryAlertStatus;

        }

        public List<TrayHistoryTrayLifecycle> GetVendorHistoryLifecycles(TrayHistoryCase historyCase)
		{
            List<TrayHistoryTrayLifecycle> result = new List<TrayHistoryTrayLifecycle>();
            List<LifecycleStatusType> previousSteps = new List<LifecycleStatusType>() { LifecycleStatusType.DeconStaged, LifecycleStatusType.Assembly };
            List<LifecycleStatusType> currentSteps = new List<LifecycleStatusType>() { LifecycleStatusType.Sterilize, LifecycleStatusType.Delivered, LifecycleStatusType.InSurgery, LifecycleStatusType.Picked, LifecycleStatusType.Received };
            var dbStatuses = _context.GCLifecycleStatusTypes.ToList();
            if(dbStatuses != null && dbStatuses.Count>0)
			{
                if(historyCase.PreviousLifecycles!=null && historyCase.PreviousLifecycles.Count>0)
				{
                    foreach (var step in previousSteps)
                    {
                        var dbStatus = dbStatuses.Where(x => x.Code == (int)step).FirstOrDefault();
                        if (dbStatus != null)
                        {
                            var status = historyCase.PreviousLifecycles.Where(x => x.LifecycleStatusId == dbStatus.Id).FirstOrDefault();
                            if (status != null)
                            {
                                result.Add(status);
                            }

                        }

                    }
                }
                if(historyCase.TrayLifecycles!= null && historyCase.TrayLifecycles.Count>0)
				{
                    foreach (var step in currentSteps)
                    {
                        var dbStatus = dbStatuses.Where(x => x.Code == (int)step).FirstOrDefault();
                        if (dbStatus != null)
                        {
                            var status = historyCase.TrayLifecycles.Where(x => x.LifecycleStatusId == dbStatus.Id).FirstOrDefault();
                            if (status != null)
                            {
                                result.Add(status);
                            }

                        }

                    }
                }

            }

            result = result.OrderBy(x => x.LifecycleOrder).ToList();
            return result;
		}

        public TrayPhisicalLocationType GetTraysPhisicalLocation(LifecycleStatusType currentStatusType)
		{
            var vehicleLocationStatuses = new List<LifecycleStatusType>() { LifecycleStatusType.Loaded, LifecycleStatusType.ShippedInbound, LifecycleStatusType.Picked, LifecycleStatusType.ShippedOutbound };
            var providerLocations = new List<LifecycleStatusType>() { LifecycleStatusType.Delivered, LifecycleStatusType.InSurgery, LifecycleStatusType.SurgeryComplete };
            if(vehicleLocationStatuses.Contains(currentStatusType))
			{
                return TrayPhisicalLocationType.Vehicle;
			}
            else if(providerLocations.Contains(currentStatusType))
            {
                return TrayPhisicalLocationType.Provider;
            }
            else
			{
                return TrayPhisicalLocationType.VM;
			}
        }

        public GCTraysLifecycleStatusTypes AddOrEditProposedHistoryItemForLC(ProposedTrayLCHistoryModel model)
        {
            GCTraysLifecycleStatusTypes lc = null;
            var historyItem = _context.CTContainerTypeActualHistoryItems.Find(model.SelectedHI);
            if(historyItem != null)
            {
                lc = _context.GCTraysLifecycleStatusTypes.Find(model.GCTrayLifeCycleId);

                if(lc == null)
                {
                    lc = new GCTraysLifecycleStatusTypes()
                    {
                        GCTrayId = model.GCTrayId,
                        LifecycleStatusTypeId = model.GCLifeCycleId,
                        User = historyItem.UpdateUserId
                    };
                }
                lc.Timestamp = historyItem.UpdateTimestamp;
                lc.TimestampOriginal = historyItem.UpdateTimestampOriginal;
                if (lc.Id == 0)
                {
                    _context.GCTraysLifecycleStatusTypes.Add(lc);
                }
                _context.SaveChanges();
            }
            return lc;
        }

        public bool RemoveGCTrayLifecycleStatud(int id)
        {
            var result = false;
            var lc = _context.GCTraysLifecycleStatusTypes.Find(id);
            if (lc != null)
            {
                result = _groundControlService.DeleteGCTraysLifecycleStatusType(id) > 0;
                UpdateGCCaseLifecycleStatus(lc.GCTrayId);
            }
            return result;
        }

        public List<GoogleMapMarker> GetWWMapMarkers()
		{
            var devices = _context.WWGPSDevices.ToList();
            var result = new List<GoogleMapMarker>();
            if(devices != null && devices.Count>0)
			{
				foreach (var device in devices)
				{
                    var vehicle = _context.WWVehicles.Where(x => x.WWGPSDeviceId == device.Id).FirstOrDefault();
                    if(vehicle!=null)
					{
                        var latestSample = _context.WWGPSDeviceSamples.Where(x => x.DeviceId == device.DeviceId).OrderByDescending(x => x.Timestamp).FirstOrDefault();
                        if(latestSample!=null)
						{
                            var marker = new GoogleMapMarker() //lat and loong need to be adjusted because WW is doing *1000000 multiplication
                            {
                                Title = vehicle.ExternalId[0].ToString(), //only getting first letter because pin on UI is too small for entire name
                                Lat = latestSample.Latitude / 1000000f,
                                Long = latestSample.Longitude / 1000000f
                            };
                            result.Add(marker);
                        }
                    }                        
                }
			}
            return result;
		}

        public List<GCSystemAlert> GetSystemAlerts(List<int> trayIds)
        {
            var result = new List<GCSystemAlert>();

            result = _context.GCSystemAlerts
                .Include(x => x.GCSystemAlertType)
                .Include(x => x.GCTray)
                    .ThenInclude(x => x.GCCase)
                .Include(x => x.GCTray)
                    .ThenInclude(x => x.CTContainerTypeActual)
                .Where(x => trayIds.Contains(x.GCTrayId.Value)).ToList();

            return result;
        }

        public List<GCTray> GetTraysForGCCaseIds(List<int> gcCaseIds)
        {
            var actualIds = _context.GCTrays.Where(x =>
                x.CTContainerTypeActualId.HasValue &&
                gcCaseIds.Contains(x.CTContainerTypeActualId.Value))
                .GroupBy(x => x.CTContainerTypeActualId).Select(x => x.Key.Value).ToList();

            if (actualIds != null && actualIds.Count > 0)
            {
                var trays = _context.GCTrays
                    .Include(x => x.CTContainerTypeActual)
                    .Include(x => x.GCLifecycleStatusType)
                    .Where(x => x.CTContainerTypeActualId.HasValue && actualIds.Contains(x.CTContainerTypeActualId.Value))
                    .ToList();
                return trays;
            }
            else
            {
                return null;
            }
        }

        public bool MarkGCSystemAlertRead(int id)
        {
            var result = false;
            var alert = _context.GCSystemAlerts.Find(id);
            if(alert != null)
            {
                alert.MarkedAsReadAtUtc = DateTime.UtcNow;
                result = _context.SaveChanges() > 0;
            }
            return result;
        }
    }
}