﻿using System.Collections.Generic;

namespace DataBrowser
{
    public class DataBrowserSettings
    {
        public string Databases { get; set; }

        public string ConnectionString { get; set; }

        public Dictionary<string, string> AllowedTables { get; set; }
    }
}
