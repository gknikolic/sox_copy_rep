﻿using ProfitOptics.Framework.Core.Email;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Services;

namespace ProfitOptics.Framework.Web.Areas.Account.Services
{
    public class EMailTwoFactorResolver : ITwoFactorResolver
    {
        private readonly IEmailer _emailer;

        public EMailTwoFactorResolver(IEmailer emailer)
        {
            this._emailer = emailer;
        }

        /// <summary>
        /// Implementation of ITwoFactorResolver method that sends email with the verification code to user
        /// </summary>
        /// <param name="code">Verification code</param>
        /// <param name="user">User</param>
        /// <returns></returns>
        public async Task<bool> SendCode(string code, ApplicationUser user)
        {
            try
            {
                var body = "Your ProfitOptics security code is " + code;
                return await _emailer.SendEmailAndWriteToDbAsync(new List<string> { user.Email }, "ProfitOptics Security Code", body);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string GetProviderName()
        {
            return TwoFactorProviderNames.Email;
        }
    }
}
