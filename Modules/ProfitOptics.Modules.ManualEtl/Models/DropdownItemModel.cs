﻿namespace ProfitOptics.Modules.ManualEtl.Models
{
    public class DropdownItemModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}