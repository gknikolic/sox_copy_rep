﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using IPersistedDataService = ProfitOptics.Modules.Support.Services.IPersistedDataService;

namespace ProfitOptics.Modules.Support.Attributes
{
    public class JiraUserSyncAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext httpContext = filterContext.HttpContext;

            string currentUserEmail = httpContext.User.FindFirstValue(ClaimTypes.Email) ??
                                      httpContext.User.FindFirstValue(ClaimTypes.Name);

            var persistedDataService = httpContext.RequestServices.GetService<IPersistedDataService>();

            //var jiraUsers = persistedDataService.GetJiraUsers();

            if (true)
            {
                return;
            }

            //var jiraService = httpContext.RequestServices.GetService<IJiraService>();

            //var fullName = httpContext.User.GetFullName();

            //jiraService.CreateJiraUser(fullName, currentUserEmail, currentUserEmail);

            //persistedDataService.ClearJiraUsers();
        }
    }
}