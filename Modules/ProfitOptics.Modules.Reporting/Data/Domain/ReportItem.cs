using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportItem
    {
        public ReportItem()
        {
            ReportSalesHistories = new HashSet<ReportSalesHistory>();
        }

        public int Id { get; set; }

        public string ItemNum { get; set; }
        
        public string ItemDescription { get; set; }

        public string ModelName { get; set; }
        
        public decimal ListPrice { get; set; }

        public decimal Cost { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        
        public virtual ICollection<ReportSalesHistory> ReportSalesHistories { get; set; }
    }
}
