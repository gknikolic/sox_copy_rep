﻿using System;
using System.Collections.Generic;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.TrendVariance;

namespace ProfitOptics.Modules.Reporting.Services.TrendVariance
{
    public interface ITrendVarianceDataService
    {
        /// <summary>
        /// Gets the sales history for months.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IEnumerable<TrendVarianceSalesForMonthModel> GetSalesHistoryForMonths(int userId, DateTime startDate,
            DateTime endDate);

        /// <summary>
        /// Gets the variances grouped by view by type.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="viewByType"></param>
        /// <param name="periods"></param>
        /// <returns></returns>
        PagedList<TrendVarianceVarianceByTypeModel> GetVariancesGroupedByViewByType(int userId, IDataTablesRequest request, 
            ViewByType viewByType, TrendVariancePeriodsModel periods);

        /// <summary>
        /// Gets the variances grouped by item category for the bill to parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="billToId"></param>
        /// <param name="request"></param>
        /// <param name="periods"></param>
        /// <returns></returns>
        PagedList<TrendVarianceVarianceByTypeModel> GetVariancesGroupedByItemCategoryForBillToId(int userId,
            int billToId, IDataTablesRequest request, TrendVariancePeriodsModel periods);

        /// <summary>
        /// Gets the variances grouped by item category for the ship to parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="shipToId"></param>
        /// <param name="request"></param>
        /// <param name="periods"></param>
        /// <returns></returns>
        PagedList<TrendVarianceVarianceByTypeModel> GetVariancesGroupedByItemCategoryForShipToId(int userId,
            int shipToId, IDataTablesRequest request, TrendVariancePeriodsModel periods);

        /// <summary>
        /// Gets the bill to with the specified identifier.
        /// </summary>
        /// <param name="billToId"></param>
        /// <returns></returns>
        ReportBillTo GetBillToById(int billToId);

        /// <summary>
        /// Gets the ship to with the specified identifier.
        /// </summary>
        /// <param name="shipToId"></param>
        /// <returns></returns>
        ReportShipTo GetShipToById(int shipToId);

        /// <summary>
        /// Gets the item category with the specified identifier.
        /// </summary>
        /// <param name="itemCategoryId"></param>
        /// <returns></returns>
        ReportItemCategory GetItemCategoryById(int itemCategoryId);

        /// <summary>
        /// Gets the variances grouped by item for the specified parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="billToId"></param>
        /// <param name="itemCategoryId"></param>
        /// <param name="request"></param>
        /// <param name="periods"></param>
        /// <returns></returns>
        PagedList<TrendVarianceVarianceForItemModel> GetVariancesGroupedByItemForItemCategoryIdAndBillToId(
            int userId, int billToId, int itemCategoryId, IDataTablesRequest request, TrendVariancePeriodsModel periods);

        /// <summary>
        /// Gets the variances grouped by item for the specified parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="shipToId"></param>
        /// <param name="itemCategoryId"></param>
        /// <param name="request"></param>
        /// <param name="periods"></param>
        /// <returns></returns>
        PagedList<TrendVarianceVarianceForItemModel> GetVariancesGroupedByItemForItemCategoryIdAndShipToId(
            int userId, int shipToId, int itemCategoryId, IDataTablesRequest request, TrendVariancePeriodsModel periods);

        /// <summary>
        /// Gets the sales histories for the specified date range.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        List<ReportSalesHistory> GetSalesHistoriesForDateRange(int userId, DateTime startDate, DateTime endDate);
    }
}
