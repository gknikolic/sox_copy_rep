﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
	[Table("WWBarcode")]
    public class WWBarcode
	{
        public int Id { get; set; }
        public int Sec { get; set; }
		public string Barcode { get; set; }
		public string BarcodeStatus { get; set; }
		public int WWPodContainerId { get; set; }

        public WWPodContainer WWPodContainer { get; set; }
    }
}
