﻿namespace ProfitOptics.Framework.Web.Framework.Modules
{
    public class FrameworkModuleDefaults
    {
        public static string Path => "~/Modules";
        public static string ShadowCopyPath => "~/Modules/bin";
        public static string ReserveShadowCopyPathName => "reserve_bin_";
        public static string DescriptionFileName => "module.json";
        public static string PathName => "Modules";
        public static string RefsPathName => "refs";
    }
}
