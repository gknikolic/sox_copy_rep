﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.Test
{
    public class TSkill
    {
        public TSkill()
        {
            EmployeeSkills = new HashSet<TEmployeeSkills>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TEmployeeSkills> EmployeeSkills { get; set; }
    }
}
