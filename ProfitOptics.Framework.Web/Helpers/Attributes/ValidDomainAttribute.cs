﻿using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Helpers.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;

namespace ProfitOptics.Framework.Web.Helpers.Attributes
{
    public class ValidDomainAttribute : ValidationAttribute
    {      

        public ValidDomainAttribute()
            : base()
        {

        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var domains = ((Common)validationContext.GetService(typeof(Common))).AllowedEmailDomains.Split(',').Select(x => x.ToLower());
            var emailValue = ((string)value)?.ToLower();//.GetType().GetProperty("Email").GetValue(value, null);

            if(emailValue == null)
            {
                return new ValidationResult("No email specified");
            }

            return domains.Contains(emailValue.GetDomainName()) ? ValidationResult.Success : new ValidationResult("Bad domain");
        }
    }
}
