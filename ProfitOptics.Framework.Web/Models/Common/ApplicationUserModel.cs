﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CsvHelper.Configuration.Attributes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;

namespace ProfitOptics.Framework.Web.Models.Common
{
    public class ApplicationUserModel
    {
        public ApplicationUserModel()
        {
            Roles = new List<RoleAssignmentModel>();
        }

        [Required]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Email/Username")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Title { get; set; }

        public List<RoleAssignmentModel> Roles { get; set; }

        [Display(Name = "Roles")]
        public List<SelectListItem> UserRoles { get; set; }
        [Required]
        public List<string> SelectedRoles { get; set; }

        public IEnumerable<ProfitOptics.Framework.Web.Framework.Identity.Domain.GCParentCompany> ParentCompanies { get; set; }
        [Display(Name = "Companies")]
        public IEnumerable<SelectListItem> AllParentCompanies { get; set; }

        [Ignore]
        public List<int> SelectedParentCompanies { get; set; }
        public IEnumerable<string> ChildCompanyPermissions { get; set; }
        public string ChildCompanyPermissionValues { get { return ChildCompanyPermissions == null ? null : string.Join(",", ChildCompanyPermissions.ToList()); } } 
    }
}
