﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class SendCodeModel
    {
        private IList<string> twoFactorProviders;

        public SendCodeModel()
        {
            Providers = new List<SelectListItem>();
        }

        public SendCodeModel(string returnUrl, bool rememberMe, IList<string> twoFactorProviders)
        {
            ReturnUrl = returnUrl;
            RememberMe = rememberMe;
            Providers = twoFactorProviders.Select(x => new SelectListItem { Text = x, Value = x }).ToList();
        }
        [Display(Name = "Select Two-Factor Authentication Provider")]
        [Required]
        public string Provider { get; set; }
        public ICollection<SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }
}
