﻿namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVarianceVarianceByTypeModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public string Code { get; set; }
        
        public decimal PriorSales { get; set; }

        public decimal PriorCost { get; set; }

        public decimal PriorQty { get; set; }

        public decimal PriorAsp => PriorQty == decimal.Zero ? decimal.Zero : PriorSales / PriorQty;

        public decimal PriorGrossProfit => PriorSales - PriorCost;

        public decimal PriorGrossProfitPercent =>
            PriorSales == decimal.Zero ? decimal.Zero : (PriorSales - PriorCost) / PriorSales;
        
        public decimal CurrentSales { get; set; }

        public decimal CurrentCost { get; set; }

        public decimal CurrentQty { get; set; }

        public decimal CurrentAsp => CurrentQty == decimal.Zero ? decimal.Zero : CurrentSales / CurrentQty;

        public decimal CurrentGrossProfit => CurrentSales - CurrentCost;
        
        public decimal CurrentGrossProfitPercent =>
            CurrentSales == decimal.Zero? decimal.Zero : (CurrentSales - CurrentCost) / CurrentSales;

        public decimal VarianceSales => CurrentSales - PriorSales;

        public decimal VarianceSalesPercent =>
            PriorSales == decimal.Zero ? decimal.Zero : (CurrentSales - PriorSales) / PriorSales;

        public decimal VarianceQty => CurrentQty - PriorQty;

        public decimal VarianceQtyPercent =>
            PriorQty == decimal.Zero ? decimal.Zero : (CurrentQty - PriorQty) / PriorQty;

        public decimal VarianceGrossProfit => CurrentGrossProfit - PriorGrossProfit;

        public decimal VarianceGrossProfitGrowthPercent =>
            PriorGrossProfit == decimal.Zero ? decimal.Zero : (CurrentGrossProfit - PriorGrossProfit) / PriorGrossProfit;

        public decimal VarianceGrossProfitPercent => CurrentGrossProfitPercent - PriorGrossProfitPercent;
    }
}
