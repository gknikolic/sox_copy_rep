﻿namespace ProfitOptics.Framework.Web.Framework.Models.Components
{
    public class HierarchyChartModel
    {
        public HierarchyChartNodeModel Data { get; set; }
        public string Button1Text { get; set; }
        public string Button2Text { get; set; }
        public string Button3Text { get; set; }
        public string OnNodeButton1Callback { get; set; }
        public string OnNodeButton2Callback { get; set; }
        public string OnNodeButton3Callback { get; set; }
    }
}