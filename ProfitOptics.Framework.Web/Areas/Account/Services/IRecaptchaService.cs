﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Services
{
    public interface IRecaptchaService
    {
        /// <summary>
        /// Validates the captcha
        /// </summary>
        /// <param name="value">Captcha value</param>
        /// <returns></returns>
        bool Validate(string value);
    }
}
