using System;
using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportHierarchyRegion : IFilterableEntity
    {
        public ReportHierarchyRegion()
        {
            ReportHierarchySalesReps = new HashSet<ReportHierarchySalesRep>();
        }

        [Select2Id]
        public int Id { get; set; }
        
        [Select2Text]
        public string Name { get; set; }

        public int? ZoneId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual ReportHierarchyZone ReportHierarchyZone { get; set; }
        
        public virtual ICollection<ReportHierarchySalesRep> ReportHierarchySalesReps { get; set; }
    }
}
