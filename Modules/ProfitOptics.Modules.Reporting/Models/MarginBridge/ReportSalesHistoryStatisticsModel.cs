﻿using System;
using ProfitOptics.Modules.Reporting.Attributes;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class ReportSalesHistoryStatisticsModel
    {
        public int Id { get; set; }

        [FieldTitle("InvoiceDate")]
        public DateTime InvoiceDate { get; set; }

        [FieldTitle("ZoneId")]
        public int? ZoneId { get; set; }

        [FieldTitle("RegionId")]
        public int? RegionId { get; set; }

        [FieldTitle("SalesRepId")]
        public int? SalesRepId { get; set; }

        [FieldTitle("BillToId")]
        public int BillToId { get; set; }

        [FieldTitle("ShipToId")]
        public int ShipToId { get; set; }

        [FieldTitle("VendorId")]
        public int? VendorId { get; set; }

        [FieldTitle("ItemId")]
        public int ItemId { get; set; }

        [FieldTitle("UnitPrice")]
        public decimal? UnitPrice { get; set; }

        [FieldTitle("UnitCost")]
        public decimal? UnitCost { get; set; }

        [FieldTitle("ExtPrice")]
        public decimal ExtPrice { get; set; }

        [FieldTitle("ExtCost")]
        public decimal ExtCost { get; set; }

        [FieldTitle("Qty")]
        public decimal Qty { get; set; }

        [FieldTitle("BuyGroupId")]
        public int? BuyGroupId { get; set; }

        [FieldTitle("CustomerClassId")]
        public int? CustomerClassId { get; set; }

        [FieldTitle("ItemCategoryId")]
        public int? ItemCategoryId { get; set; }

        [FieldTitle("RebateRate")]
        public decimal RebateRate { get; set; }

        [FieldTitle("ShipToName")]
        public string ShipTo { get; set; }

        [FieldTitle("BillToName")]
        public string BillTo { get; set; }

        [FieldTitle("Territory")]
        public string Territory { get; set; }

        [FieldTitle("Region")]
        public string Region { get; set; }

        [FieldTitle("Zone")]
        public string Zone { get; set; }

        [FieldTitle("Item")]
        public string Item { get; set; }

        [FieldTitle("ItemCategory")]
        public string ItemCategory { get; set; }
    }
}