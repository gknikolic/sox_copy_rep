﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.TokenizationSearch;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Quotes.Data.Domain;
using ProfitOptics.Modules.Quotes.Models;
using ProfitOptics.Modules.Quotes.Services;

namespace ProfitOptics.Modules.Quotes.Factories
{
    public class QuoteFactory : IQuoteFactory
    {
        #region Fields

        private readonly IQuoteService _quoteService;

        #endregion Fields

        #region Ctor

        public QuoteFactory(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }

        #endregion Ctor

        #region Methods

        public PagedList<CustomerModel> GetCustomerModels(IDataTablesRequest request, int userId)
        {
            var records = _quoteService.GetQtBillTos(userId)
                .Select(to => CreateCustomerModelPredicate(to)).AsQueryable();

            var paged = ProcessCustomerRequest(request, records);

            return paged;
        }

        private static CustomerModel CreateCustomerModelPredicate(QTBillTo qtBillTo)
        {
            return new CustomerModel
            {
                CustId = qtBillTo.CustomerId,
                CustNo = qtBillTo.BillToNum.ToString(),
                BillToAddress = qtBillTo.Address1,
                BillToId = qtBillTo.Id,
                CustName = qtBillTo.BillToName,
                City = qtBillTo.City,
                ZipCode = qtBillTo.Zip
            };
        }

        private PagedList<CustomerModel> ProcessCustomerRequest(IDataTablesRequest request, IQueryable<CustomerModel> records)
        {
            // filter
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                var searchColumns = request.Columns.Where(t => !string.IsNullOrEmpty(t.Data) && t.Searchable)
                    .ToDictionary(t => t.Data, t => true);

                var zipCode = searchColumns.ContainsKey("ZipCode");
                var city = searchColumns.ContainsKey("City");
                var custName = searchColumns.ContainsKey("CustName");
                var billToAddress = searchColumns.ContainsKey("BillToAddress");
                var custId = searchColumns.ContainsKey("CustId");

                var strings = Tokenizer.TokenizeToWords(request.Search.Value);

                foreach (var str in strings)
                {
                    records = records.Where(x =>
                        (zipCode && x.ZipCode != null && x.ZipCode.Contains(str)) ||
                        (city && x.City != null && x.City.Contains(str)) ||
                        (custName && x.CustName != null && x.CustName.Contains(str)) ||
                        (billToAddress && x.BillToAddress != null && x.BillToAddress.Contains(str)) ||
                        (custId && x.CustId != null && x.CustId.Contains(str))
                    );
                }
            }

            records = records.ApplySort(request);

            // page
            var paged = records.ApplyPagination(request).ToList();

            var totalCount = records.Count();
            return new PagedList<CustomerModel>(paged, totalCount);
        }

        public PagedList<ItemModel> GetItemModelsFromPriceListData(IDataTablesRequest request, int quoteId, string plcCode)
        {
            var items = _quoteService.GetQtItems(quoteId, true);

            var pricings = _quoteService.GetCustomerPriceList(plcCode);

            var records = (from item in items
                join price in pricings on item.ItemNum equals price.ItemNum 
                select new ItemModel
                {
                    ItemId = item.Id,
                    ItemNum = item.ItemNum,
                    ItemDescription = item.ItemDescription,
                    ItemUoM = price.UOM,
                    LastPrice = price.BasePrice
                }).Distinct();

            var paged = ProcessItemModelRequest(request, records);

            return paged;
        }

        public PagedList<ItemModel> GetItemsFromStandingOrderData(IDataTablesRequest request, int quoteId, int userId, int orderNum)
        {
            var records = _quoteService.GetStandingOrderQtItems(quoteId, orderNum);

            var paged = ProcessItemModelRequest(request, records, true);
            
            foreach (var p in paged)
            {
                p.LastPrice = _quoteService.GetBasePrice(quoteId, p.ItemId);
            }

            return paged;
        }
        
        public PagedList<ItemModel> GetQuoteItemsData(IDataTablesRequest request, int quoteId, int userId)
        {
            var records = _quoteService.GetItemsListForQuote(userId, quoteId);

            var quoteTotalWeight = records.Sum(z => z.WeightTotal);
            var quoteExtendedTotalCost = records.Sum(z => z.QuoteExtendedCost);
            var quoteExtendedTotalRevenue = records.Sum(z => z.QuoteExtendedRevenue);
            var quoteExtendedMDollarBeforeFreight = quoteExtendedTotalRevenue - quoteExtendedTotalCost;

            var quoteExtendedNetShippingCost = records.Select(z => z.QuoteAverageShippingCost)
                .FirstOrDefault() ?? 0m;
            var freightPaymentType = records.Select(z => z.FreightPaymentType)
                .FirstOrDefault() ?? "statlab";
            var quoteExtendedNetShippingRevenue = decimal.Zero;
            if (freightPaymentType == "statlab")
            {
            }
            else if (freightPaymentType == "customer")
            {
                quoteExtendedNetShippingRevenue = quoteTotalWeight != 0
                    ? 2.1m * quoteExtendedNetShippingCost : 0;
            }
            else if (freightPaymentType == "split")
            {
                var freightCap = records.Select(z => z.FreightCap)
                    .FirstOrDefault() ?? Decimal.Zero;
                var freightCapRevenue = Math.Min(freightCap, 2.1m * quoteExtendedNetShippingCost);
                quoteExtendedNetShippingRevenue = quoteTotalWeight != 0
                    ? freightCapRevenue : 0;
            }

            var quoteExtendedAdjustMarginAmt = quoteExtendedMDollarBeforeFreight
                + quoteExtendedNetShippingRevenue
                - quoteExtendedNetShippingCost;
            var quoteExtendedAdjustMarginPct = quoteExtendedTotalRevenue > 0 ?
                quoteExtendedAdjustMarginAmt / quoteExtendedTotalRevenue : 0;

            var floorTotalRevenue = records.Sum(z => z.FloorRevenue);
            var floorTotalCost = records.Sum(z => z.FloorCost);
            var mDollar = records.Sum(z => z.FloorMDollar);
            var mPercent = floorTotalRevenue > 0 ? mDollar / floorTotalRevenue : 0;

            IDictionary<string, decimal?> total = new Dictionary<string, decimal?>
                {
                    { "QuoteTotalWeight", quoteTotalWeight },
                    { "QuoteExtendedTotalCost", quoteExtendedTotalCost },
                    { "QuoteExtendedTotalRevenue", quoteExtendedTotalRevenue },
                    { "QuoteExtendedMDollarBeforeFreight", quoteExtendedMDollarBeforeFreight },
                    { "QuoteExtendedNetShippingCost", quoteExtendedNetShippingCost },
                    { "QuoteExtendedNetShippingRevenue", quoteExtendedNetShippingRevenue },
                    { "QuoteExtendedAdjustMarginAmt", quoteExtendedAdjustMarginAmt },
                    { "QuoteExtendedAdjustMarginPct", quoteExtendedAdjustMarginPct },
                    { "FloorTotalRevenue", floorTotalRevenue },
                    { "FloorTotalCost", floorTotalCost },
                    { "MDollar", mDollar },
                    { "MPercent", mPercent }
                };


            var paged = ProcessItemModelRequest(request, records.AsQueryable(), searchByFreightClass: true, total: total);

            return paged;
        }

        public PagedList<ItemModel> GetItemsData(IDataTablesRequest request, int quoteId, string filter)
        {
            var records = _quoteService.GetItemsData(request, quoteId, filter);

            var paged = ProcessItemModelRequest(request, records, true, true);
            
            return paged;
        }

        #endregion Methods

        #region Helper Methods
        
        private PagedList<ItemModel> ProcessItemModelRequest(IDataTablesRequest request,
            IQueryable<ItemModel> records, bool searchByItemVendor = false, bool searchByFreightClass = false, IDictionary<string, decimal?> total = null)
        {
            // filter
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                var strings = Tokenizer.TokenizeToWords(request.Search.Value);

                foreach (var str in strings)
                {
                    records = records.Where(x =>
                        (x.ItemNum != null && x.ItemNum.Contains(str)) ||
                        (x.ItemDescription != null && x.ItemDescription.Contains(str)) ||
                        (searchByItemVendor && x.ItemVendor != null && x.ItemVendor.Contains(str)) ||
                        (searchByFreightClass && x.FreightClass != null && x.FreightClass.ToString().Contains(str)) ||
                        (x.ItemUoM != null && x.ItemUoM.Contains(str))
                    );
                }
            }

            // sort
            records = records.ApplySort(request);

            // page
            var paged = records.ApplyPagination(request).ToList();

            var totalCount = records.Count();
            return new PagedList<ItemModel>(paged, totalCount, total);
        }

        #endregion
    }
}