﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCTrayColorStatusNoteConfig : IEntityTypeConfiguration<GCTrayColorStatusNote>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCTrayColorStatusNote> builder)
        {
            builder.ToTable("GCTrayColorStatusNote");
            builder.HasKey(entity => entity.Id);
        }
    }
}
