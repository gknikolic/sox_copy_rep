namespace ProfitOptics.Framework.Web.RestApi.Models.Distributor
{
    public class DistributorResponse : BaseResponse<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor>
    {
        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="distributor">Saved distributor.</param>
        /// <returns>Response.</returns>
        public DistributorResponse(ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor distributor) : base(distributor)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public DistributorResponse(string message) : base(message)
        { }
    }
}