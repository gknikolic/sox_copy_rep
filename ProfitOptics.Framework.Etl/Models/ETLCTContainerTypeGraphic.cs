﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Models
{
    public class ETLCTContainerTypeGraphic
    {
        [JsonProperty("imageName")]
        public string ImageName { get; set; }

        [JsonProperty("containerTypeId")]
        public long ContainerTypeId { get; set; }

        [JsonProperty("graphicKey")]
        public ETLCTGraphicKey GraphicKey { get; set; }

        [JsonProperty("usageFlags")]
        public long UsageFlags { get; set; }

        [JsonProperty("referenceId")]
        public long ReferenceId { get; set; }

        [JsonProperty("isAssembly")]
        public bool IsAssembly { get; set; }

        [JsonProperty("isDecontam")]
        public bool IsDecontam { get; set; }

        [JsonProperty("isSterilization")]
        public bool IsSterilization { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }
    }
}
