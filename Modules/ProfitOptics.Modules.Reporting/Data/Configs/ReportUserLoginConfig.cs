﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportUserLoginConfig : IEntityTypeConfiguration<ReportUserLogin>
    {
        public void Configure(EntityTypeBuilder<ReportUserLogin> builder)
        {
            builder.ToTable(nameof(ReportUserLogin));
            builder.HasKey(l => l.Id);

            builder.Property(l => l.LoginDateTime).IsRequired();
        }
    }
}
