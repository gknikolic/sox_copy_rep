﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Atlassian.Jira;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Modules.Support.Models.JiraRestModels;
using ProfitOptics.Modules.Support.Models.ViewModels;
using RestSharp;
using CreateIssueViewModel = ProfitOptics.Modules.Support.Models.ViewModels.CreateIssueViewModel;
using Issue = Atlassian.Jira.Issue;
using IssuePriority = Atlassian.Jira.IssuePriority;
using IssueType = Atlassian.Jira.IssueType;
using JiraUser = Atlassian.Jira.JiraUser;

namespace ProfitOptics.Modules.Support.Services
{
    public interface IJiraService
    {
        /// <summary>
        /// Creates a new jira issue
        /// </summary>
        /// <returns></returns>
        Task<Issue> CreateJiraIssueAsync(CreateIssueViewModel viewModel, string reporter);

        /// <summary>
        /// Returns list of all projects
        /// </summary>
        /// <returns></returns>
        List<Project> FetchProjects();

        /// <summary>
        /// Returns list of all components for the given project key
        /// </summary>
        /// <param name="projectKey"></param>
        /// <returns></returns>
        List<ComponentResult> FetchComponents(string projectKey);

        /// <summary>
        /// Returns list of all priorities
        /// </summary>
        /// <returns></returns>
        List<IssuePriority> FetchPriorities();

        /// <summary>
        /// Returns list of all issue types for the given project key
        /// </summary>
        /// <param name="projectKey"></param>
        /// <returns></returns>
        List<IssueType> FetchIssueTypes(string projectKey);

        /// <summary>
        /// Returns the list of all users in the JIRA server.
        /// </summary>
        /// <returns></returns>
        List<JiraUserFromApi> FetchUsersForProject(string projectKey);

        bool HasBackLog(BoardConfiguration config);

        /// <summary>
        /// Gets the issue with the provided issue Key.
        /// </summary>
        /// <param name="issueKey"></param>
        /// <returns></returns>
        Issue GetIssueById(string issueKey);

        /// <summary>
        /// Gets possible transitions for issue key
        /// </summary>
        /// <param name="issueKey"></param>
        /// <returns></returns>
        List<IssueTransition> GetPossibleTransitionsForIssue(string issueKey);

        /// <summary>
        /// Changes the status of the issue with the given key
        /// </summary>
        /// <param name="issueKey"></param>
        /// <param name="transitionName"></param>
        void ChangeIssueStatus(string issueKey, string transitionName);

        /// <summary>
        /// Update the issue from edit issue page
        /// </summary>
        /// <param name="viewModel"></param>
        void UpdateIssue(UpdateIssueViewModel viewModel);

        void GetAttachmentsAndCommentsForIssue(string issueKey, out IEnumerable<Attachment> attachments, out IEnumerable<Comment> comments);

        Attachment DownloadAttachment(string issueKey, string attachmentId);

        void DeleteAttachment(string issueKey, string attachmentId);

        Task<List<JiraUser>> GetJiraUsersAsync(string search);

        BoardConfiguration FetchBoardConfiguration(string projectKey);

        List<string> GetPossibleBoxTransitions(string destinationBox, BoardConfiguration config);

        IRestResponse RankBeforeIssue(string issueIdToRank, string rankBeforeIssueId, string customSortField);

        IRestResponse RankAfterIssue(string issueIdToRank, string rankAfterIssueId, string customSortField);

        PagedList<Modules.Support.Models.Issue> FindIssues(string project, string[] statuses, string[] components,
           string assignee, string reporter, int skip, int take, BoardConfiguration config);

        List<SelectListItem> FetchIssueKeys(string project, BoardConfiguration config);

        PagedList<Modules.Support.Models.Issue> FindBacklogIssues(string project, List<string> components,
           string assignee, string reporter, int start, int length, BoardConfiguration config);
    }
}