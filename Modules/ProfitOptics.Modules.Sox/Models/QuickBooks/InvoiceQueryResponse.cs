﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class InvoiceQueryResponse
    {
        public InvoiceResponse QueryResponse { get; set; }
    }
}
