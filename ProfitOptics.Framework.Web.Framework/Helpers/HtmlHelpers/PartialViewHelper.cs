﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Helpers.HtmlHelpers
{
    public static class PartialViewHelper
    {
        public static string BuildModulePartialViewPath(Assembly moduleAssembly, string path)
        {
            var assemblyName = moduleAssembly.FullName.Split(',')[0];
            return $"~/Modules/{assemblyName}/{path}";
        }
    }
}
