﻿using Flurl.Http;
using ProfitOptics.Framework.Core.Flurl;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Modules.Sox.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Infrastructure.Clients
{
	public class MonnitClient : IMonnitClient
	{
		private readonly Framework.DataLayer.Entities _context;
		public HttpRequestClient HttpRequestClient { get; set; }

		public MonnitOptions MonnitOptions { get; set; }

		public MonnitClient(Framework.DataLayer.Entities context, MonnitOptions monnitOptions)
		{
			_context = context;
			MonnitOptions = monnitOptions;
			this.HttpRequestClient = new HttpRequestClient(MonnitOptions.BaseUrl, string.Empty, string.Empty);
		}

		public async Task<List<MonnitSensor>> GetSensors()
		{
			var isLoggedIn = await MonnitLogonAsync();
			if (isLoggedIn)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments(MonnitOptions.ResponseType, "SensorList", MonnitOptions.AuthToken)
					.GetAsync<MonnitSensorList>();
				if(response!=null)
				{
					return response.Result;
				}
				else
				{
					return null;
				}
			}
			return null;
		}

		public async Task<bool> MonnitLogonAsync()
		{
			var response = await HttpRequestClient._Request.AppendPathSegments(MonnitOptions.ResponseType, "Logon", MonnitOptions.AuthToken)
					.GetAsync<MonnitResponse>();
			if (response != null)
			{
				if (response.Result == "Success")
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
}
