﻿using ProfitOptics.Modules.Reporting.Attributes;
using System;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class SalesHistory
    {
        public int ProductHierarchyID { get; set; }
        public int LocationHierarchyID { get; set; }

        [FieldTitle("CoreFocus")] public string CoreFocus { get; set; }

        [FieldTitle("PGSGBusinessUnit")] public string PGSGBusinessUnit { get; set; }

        [FieldTitle("PGSGCategory")] public string PGSGCategory { get; set; }

        [FieldTitle("ProductGroup")] public string ProductGroup { get; set; }

        [FieldTitle("PGSG")] public string PGSG { get; set; }

        [FieldTitle("ProductNumber")] public string ProductNumber { get; set; }

        [FieldTitle("BusinessUnitNew")] public string BusinessUnitNew { get; set; }

        [FieldTitle("BusinessUnitNew")] public string DivisionName { get; set; }

        [FieldTitle("DivisionName")] public string Area { get; set; }

        [FieldTitle("LocationNameEnglish")] public string LocationNameEnglish { get; set; }

        [FieldTitle("InvoiceCost")] public decimal? InvoiceCost { get; set; }

        [FieldTitle("QuantityShipped")] public decimal? QuantityShipped { get; set; }

        [FieldTitle("InvoiceAmount")] public decimal? InvoiceAmount { get; set; }

        [FieldTitle("RebatedCost")] public decimal RebatedCost { get; set; }

        [FieldTitle("InvoiceDT")] public DateTime? InvoiceDT { get; set; }

        [FieldTitle("OrderDT")] public DateTime? OrderDT { get; set; }

        [FieldTitle("CustomerNumber")] public string CustomerNumber { get; set; }

        [FieldTitle("CustomerName")] public string CustomerName { get; set; }

        [FieldTitle("Invoice#")] public string InvoiceNumber { get; set; }

        public string DivisionCode { get; set; }

        public string OrderType { get; set; }

        public bool IsPolitique { get; set; }

        public bool IsShipAndDebit { get; set; }
    }
}