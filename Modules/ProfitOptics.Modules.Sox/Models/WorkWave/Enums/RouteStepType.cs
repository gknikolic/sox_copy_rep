﻿namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public enum RouteStepType
	{
		departure, 
		arrival, 
		pickup, 
		delivery, 
		brk
	}
}
