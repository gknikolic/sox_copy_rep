﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.DataLayer.Test;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Employee.Models
{
    public class EmployeeModel
    {
        public EmployeeModel()
        {
            EmployeeSkills = new HashSet<TEmployeeSkills>();
            EmployeeSkillIds = new HashSet<int>();
        }
        public virtual ICollection<TEmployeeSkills> EmployeeSkills { get; set; }
        public ICollection<int> EmployeeSkillIds { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ManagerId { get; set; }
        public List<SelectListItem> AllManagers { get; set; }
        public List<SelectListItem> AllSkills { get; set; }
        public bool UpdateFlag { get; set; }
    }
}
