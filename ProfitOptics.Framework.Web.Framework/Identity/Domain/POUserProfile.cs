namespace ProfitOptics.Framework.Web.Framework.Identity.Domain
{
    public partial class POUserProfile
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public virtual ApplicationUser AspNetUser { get; set; }
    }
}