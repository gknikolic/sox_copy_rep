﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportBuyGroupConfig : IEntityTypeConfiguration<ReportBuyGroup>
    {
        public void Configure(EntityTypeBuilder<ReportBuyGroup> builder)
        {
            builder.ToTable(nameof(ReportBuyGroup));
            builder.HasKey(b => b.Id);

            builder.Property(b => b.Code).HasMaxLength(50);
            builder.Property(b => b.Name).HasMaxLength(50).IsRequired();
            builder.Property(b => b.StartDate).IsRequired();

            builder.HasMany(b => b.ReportSalesHistories)
                .WithOne(s => s.ReportBuyGroup)
                .HasForeignKey(s => s.BuyGroupId)
                .IsRequired(false);
        }
    }
}
