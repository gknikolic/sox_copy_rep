USE [Sox]

CREATE TABLE [dbo].[CTQualityFeedItem]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[GCTraysLifecycleStatusTypeId] INT NULL,
	[CTContainerTypeActualId] INT NULL,
	[ReportType] NVARCHAR(50) NULL,
	[UpdatedDate] [DATETIME] NOT NULL,
	[ReportedBy] NVARCHAR(100) NULL,
	[ResponsibleParty] NVARCHAR(100) NULL,
	[AssetName] NVARCHAR(100) NULL,
	[EventDate] [DATETIME] NOT NULL,
	[Comments] NVARCHAR(MAX) NULL,
	[LegacyId] BIGINT NOT NULL,
	[CreatedAtUtc] [DATETIME] NOT NULL,
	[ModifiedAtUtc] [DATETIME] NULL
CONSTRAINT [PK_CTQualityFeedItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTQualityFeedItem]  WITH CHECK ADD  CONSTRAINT [FK_CTQualityFeedItem_GCTraysLifecycleStatusTypes] FOREIGN KEY([GCTraysLifecycleStatusTypeId])
REFERENCES [dbo].[GCTraysLifecycleStatusTypes] ([Id])
GO
ALTER TABLE [dbo].[CTQualityFeedItem] CHECK CONSTRAINT [FK_CTQualityFeedItem_GCTraysLifecycleStatusTypes]
GO

ALTER TABLE [dbo].[CTQualityFeedItem]  WITH CHECK ADD  CONSTRAINT [FK_CTQualityFeedItem_CTContainerTypeActual] FOREIGN KEY([CTContainerTypeActualId])
REFERENCES [dbo].[CTContainerTypeActual] ([Id])
GO
ALTER TABLE [dbo].[CTQualityFeedItem] CHECK CONSTRAINT [FK_CTQualityFeedItem_CTContainerTypeActual]
GO