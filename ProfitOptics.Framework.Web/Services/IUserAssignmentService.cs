﻿using ProfitOptics.Framework.Web.Models.Common;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;

namespace ProfitOptics.Framework.Web.Services
{
    public interface IUserAssignmentService
    {
        Task<List<RoleAssignmentModel>> GetRoleAssignmentsByUser(ApplicationUser user, List<string> userRoles);

        Task<List<RoleAssignmentModel>> GetRoleAssignmentsByUserIdAsync(int userId);
        void SaveRoleAssignments(ApplicationUser user, ApplicationUserModel model);
    }
}