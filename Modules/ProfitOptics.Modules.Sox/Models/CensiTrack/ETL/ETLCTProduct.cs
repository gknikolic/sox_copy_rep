﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTProduct
    {
        [JsonProperty("id")]
        public decimal Id { get; set; }

        [JsonProperty("vendorName")]
        public string VendorName { get; set; }

        [JsonProperty("vendorProductName")]
        public string VendorProductName { get; set; }

        [JsonProperty("modelNumber")]
        public string ModelNumber { get; set; }



        [JsonProperty("vendorId")]
        public long? VendorId { get; set; }
        
        [JsonProperty("sterilizationMethod")]
        public string SterilizationMethod { get; set; }

        [JsonProperty("sterilizationMethodId")]
        public string SterilizationMethodId { get; set; }

        [JsonProperty("isGeneric")]
        public bool? IsGeneric { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
