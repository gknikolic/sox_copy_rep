﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Linq;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public class CTProductService : ICTProductService
    {
        private readonly Entities _entities;

        public CTProductService(Entities entities)
        {
            _entities = entities;
        }

        //public void AddOrEditCTProducts(List<CTProduct> ctProducts)
        //{
        //    ctProducts.ForEach(item =>
        //    {
        //        var existingProduct = _entities.CTProducts.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

        //        if (existingProduct == null)
        //        {
        //            item.CreatedAtUtc = DateTime.UtcNow;
        //            _entities.CTProducts.Add(item);
        //        }
        //        else
        //        {
        //            existingProduct.LegacyId = item.LegacyId;
        //            existingProduct.VendorName = item.VendorName;
        //            existingProduct.VendorProductName = item.VendorProductName;
        //            existingProduct.ModelNumber = item.ModelNumber;
        //            existingProduct.ModifiedAtUtc = DateTime.UtcNow;
        //        }
        //    });

        //    _entities.SaveChanges();
        //}

        public void AddOrEditCTVendors(List<CTVendor> ctVendors)
        {
            ctVendors.ForEach(item =>
            {
                var existingVendor = _entities.CTVendors.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existingVendor == null)
                {
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _entities.CTVendors.Add(item);
                }
                else
                {
                    existingVendor.LegacyId = item.LegacyId;
                    existingVendor.Name = item.Name;
                    existingVendor.ContactName = item.ContactName;
                    existingVendor.VendorTypes = item.VendorTypes;
                    existingVendor.FriendlyName = item.FriendlyName;
                    existingVendor.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _entities.SaveChanges();
        }
    }
}
