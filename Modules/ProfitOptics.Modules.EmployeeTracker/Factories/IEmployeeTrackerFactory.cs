﻿using ProfitOptics.Modules.EmployeeTracker.Models;

namespace ProfitOptics.Modules.EmployeeTracker.Factories
{
    public interface IEmployeeTrackerFactory
    {
        EmptyModel PrepareEmptyModel();
    }
}