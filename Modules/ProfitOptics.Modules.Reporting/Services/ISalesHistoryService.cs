﻿using System;
using System.Linq;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Services
{
    public interface ISalesHistoryService
    {
        /// <summary>
        /// Gets a queryable collection of sales histories with the specified report filter applied.
        /// </summary>
        /// <param name="reportFilter"></param>
        /// <returns></returns>
        IQueryable<ReportSalesHistory> GetFilteredSalesHistories(ReportFilterModel reportFilter);

        /// <summary>
        /// Gets the sales histories for the specified date range.
        /// </summary>
        /// <param name="reportFilter"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IQueryable<ReportSalesHistory> GetFilteredSalesHistoriesForDateRange(ReportFilterModel reportFilter, DateTime startDate,
            DateTime endDate);
    }
}
