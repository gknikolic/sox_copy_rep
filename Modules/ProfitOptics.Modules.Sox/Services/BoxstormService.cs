﻿using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Services
{
	public class BoxstormService : IBoxstormService
	{
		private readonly IBoxstormClient _boxstormClient;
		private readonly Framework.DataLayer.Entities _context;
		public BoxstormService(IBoxstormClient boxstormClient, Framework.DataLayer.Entities context)
		{
			_boxstormClient = boxstormClient;
			_context = context;
		}

		public Task<bool> AuthAsync()
		{
			var result = _boxstormClient.AuthAsync();
			return result;
		}
	}
}
