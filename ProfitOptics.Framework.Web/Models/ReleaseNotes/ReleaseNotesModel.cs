﻿using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ProfitOptics.Framework.Web.Models.ReleaseNotes
{
    public class ReleaseNotesModel
    {
        public string FileBody { get; set; }
        public DateTime Date { get; set; }
        public string Version { get; set; }

        public static List<ReleaseNotesModel> LoadReleaseNotes(HttpContext httpContext, string contentRootPath)
        {
            var path = contentRootPath + $"\\wwwroot\\Releases";

            var result = new List<ReleaseNotesModel>();
            if (Directory.Exists(path))
            {
                // We won't allow application to fail because release notes are messed up.
                try
                {
                    var fileList = Directory.GetFiles(path).ToList();
                    foreach (var file in fileList)
                    {
                        var date = GetVersionDate(Path.GetFileName(file));
                        var fileText = File.ReadAllText(file);
                        var version = fileText.Substring(0, fileText.IndexOf('\n'));
                        var body = fileText.Substring(fileText.IndexOf('\n') + 1);
                        body = HttpUtility.HtmlEncode(body);
                        result.Add(new ReleaseNotesModel
                        {
                            FileBody = body,
                            Date = date,
                            Version = version
                        });
                    }
                }
                catch (Exception ex)
                {
                    ex.Ship(httpContext);
                }
            }

            return result;
        }

        public static DateTime GetVersionDate(string fileName)
        {
            fileName = fileName.Insert(4, "-");
            fileName = fileName.Insert(7, "-");
            fileName = fileName.Substring(0, 10);
            return DateTime.Parse(fileName);
        }
    }

}