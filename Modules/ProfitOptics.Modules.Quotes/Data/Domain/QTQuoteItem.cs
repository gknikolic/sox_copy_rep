using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTQuoteItem")]
    public partial class QTQuoteItem
    {
        public int Id { get; set; }

        public int? QuoteId { get; set; }

        public int? ItemId { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? ExtPrice { get; set; }

        public decimal? ExtCost { get; set; }

        public int? Qty { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal? CompetitorPrice { get; set; }

        public string CompetitorUoM { get; set; }

        public decimal? CompetitorCF { get; set; }

        public decimal? UnitPMPct { get; set; }

        public int LineNumber { get; set; }
    }
}
