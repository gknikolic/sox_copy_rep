﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Core.Email;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Core.SMS;
using ProfitOptics.Framework.Core.System;
using ProfitOptics.Framework.Etl.Helpers;
using ProfitOptics.Framework.Web.Factories;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Framework.Web.Models.HelpArticle;
using ProfitOptics.Framework.Web.Services;

namespace ProfitOptics.Framework.Web.Infrastructure
{
    public sealed class CommonServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddScoped<ITwilio, Core.SMS.Twilio>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IHelpArticleService, HelpArticleService>();
            services.AddScoped<IEmailer, Emailer>();
            services.AddSingleton<IStringResourceRepository, StringResourcesRepository>();
            services.AddScoped<IIO, IO>();
            services.AddScoped<ITempFile, TempFile>();
            services.AddScoped<IUrlHelper>(x => {
                var actionContext = x.GetRequiredService<IActionContextAccessor>().ActionContext;
                var factory = x.GetRequiredService<IUrlHelperFactory>();
                return factory.GetUrlHelper(actionContext);
            });
            services.AddScoped<ISessionManager, SessionManager>();
            services.AddSingleton<POFrameworkDefaults, POFrameworkDefaults>();
            services.AddScoped<IPrinter, Printer>();
            services.AddScoped<ILayoutModelFactory, LayoutModelFactory>();
            services.AddScoped<IEtlHelper, EtlHelper>();
            services.AddScoped<IMemoryCacheHelper, MemoryCacheHelper>();
            services.AddScoped<IUserAssignmentService, UserAssignmentService>();
            services.AddTransient<IMenuFragmentRendererFactory, MenuFragmentRendererFactory>();
            services.AddScoped<IModuleManager, ModuleManager>();
        }
    }
}
