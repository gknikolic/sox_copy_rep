﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCTraysLifecycleStatusTypesConfig : IEntityTypeConfiguration<GCTraysLifecycleStatusTypes>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCTraysLifecycleStatusTypes> builder)
        {
            builder.ToTable("CTLocationTypesGCLifecycleStatusTypes");
            builder.HasKey(entity => entity.Id);
        }
    }
}
