﻿using Intuit.Ipp.OAuth2PlatformClient;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Models.CensiTrack.ETL;
using ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.Interfaces;
using ProfitOptics.Modules.Sox.Models.QuickBooks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static ProfitOptics.Modules.Sox.Services.GroundControlService;

namespace ProfitOptics.Modules.Sox.Services
{
    public interface IGroundControlService
    {
        public void UpdateGCCaseLifecycleStatus(int gcCaseId);
        void AddOrEditUserClaims(int userId, Dictionary<string, string> claimDictionary);

        public void UpdateGCCaseColorStatus(int gcCaseId);
        public void UpdateGCTrayColorStatus(int gcTrayId, int? gcColorStatusTypeId = null);
        public string GetQuickBooksAccessToken(int userId, List<AspNetUserClaims> userClaims, string userName = "");
        Task<TokenRevocationResponse> RevokeQBAccessTokenAsync(string token);
        Task<TokenResponse> RefreshQBAuthorizeToken(string refreshToken);
        QBUpdateTokenResponse UpdateQBToken(string code);
        Customer GetQBCustomer(int id, string token);
        List<Customer> GetQBCustomerList(string token);
        EditCustomerResponse EditQBCustomer(Customer customer, string token);
        EditCustomerResponse SetQBCustomerActiveStatus(int customerId, bool isActive, string token, string syncToken);
        QuickBooksOptions GetQuickBooksOptions();
        bool IsQuickBooksAuthorized();
        EditVendorResponse EditQBVendor(Vendor qbVendor, string v);
        Vendor GetQBVendor(int id, string token);
        string GetQBAuthorizeUrl(string scope = null);
        /// <summary>
        /// Adds or updates record in POSettings Table
        /// </summary>
        /// <param name="type">String value of enum: POSettingsType</param>
        /// <param name="value">String value</param>
        /// <param name="user">Currently logged user name</param>
        void AddOrUpdatePOSetting(POSettingsType type, string value, string user);
        POSetting GetPOSetting(POSettingsType type);
        bool RemovePOSetting(POSettingsType quickBooksRefreshToken);
        void CreateQBEstimate(GCCase gcCase, string token);
        Task PushTrayOrCaseInfoToQuickBooks(int gcCustomerId, string  yearAndMonth, string token);
        Task PushInvoicesToQB(string token, GCInvoice gcInvoice);
        CTLocation GetCTLocationForLocationName(string locationName, bool? isSterilize = null);
        GCLifecycleStatusType GetGCLifeCycleBasedOnLocationType(DateTime timestamp, int ctLocationTypeId, int ctContainerTypeActualId, bool? isCaseComplete = null, int? gcCaseId = null, DateTime? dueDate = null, int? localOffsetInMis = null, string user = null, bool? isActualCurrentlyAvailable = null);
        void UpdateLCForCompletedCases(int? localOffsetInMis = null);
        List<long> GetContainerAssemblyActualIds();
        void AddCTAssemblyGraphic(CTAssemblyGraphic assemblyGraphic);
        List<GCTraysLifecycleStatusTypes> GetAssemblyStatuses();
        Task<ETLResponseModel> CT_GetCTContainerTypeGraphicMediaAndTrayInductionSyncAndLCUpdate(int clientId, long facilityId, string userName, string password, bool syncAllActuals, bool updateAllTrays);
        void AddCTAssemblyCountSheet(CTAssemblyCountSheet countSheet);
        int? SetGCTraysLifeCycle(DateTime updateTimeStamp, string locationName, int ctContainerTypeActualId, bool isCaseComplete, int? gcCaseId, DateTime dueTimestamp, int minsToAdd, string updateUser, bool? isActualCurrentlyAvailable = null);
        void UpdateTrayInductionSynchronization(bool runAnyway);
        void CreateGCImageThumbnails();
        void CreateThumbnailsForGCImage(GCImage gcImage);
        GCVendor CreateGCVendor(long? vendorId, string vendorName, bool createChildCompany, int? gcCustomerId);
        GCChildCompany CreateGCChildCompany(string title, GCVendor gCVendor, int? gcCustomerId, int? gcParentCompanyId);
        void CancelOrDeleteCTCanceledCases(List<string> caseReferences, int minsToAdd);
        void DeleteGCCase(int gcCaseId);
        void DeleteGCTray(int gcTrayId);
        int DeleteGCTraysLifecycleStatusType(int id);
        Customer MapGcToQbCustomer(GCCustomer customer);
        Task UpdateCaseBarcodesAsync(int caseId);
        public Task DeleteOrdersInWWAsync(List<string> orderIds);
        void CompleteBrokenCTTrayHistory(List<int> gcTrayIds, int? minsToAdd = null, bool? scanAllTrays = false);
        CTLocation CorrectingNonExistingLocationForActualHistoryItem(CTContainerTypeActualHistoryItem hi);

        #region CT Services

        /// <summary>
        /// Logs in user to CensiTrack and returns result boolean
        /// </summary>
        /// <param name="model">CTLogin model</param>
        /// <returns>Returned result as a ETLCTLoginResponseModel</returns>
        /// 
        Task<ETLCTLoginResponseModel> CT_Login(ETLCTLoginModel model);
        int Execute(IOptions opts);
        void SyncCaseHistory(int gcCaseId, int minsToAdd);
        Task<List<ETLCTCase>> CT_GetCTCasesV2(int clientId);
        Task<ETLResponseModel> CT_GetCTCases(CensiTrackModuleOptions options);
        void CT_AddOrEditCTCases(List<CTCase> ctCases);
        Task<ETLResponseModel> CT_GetCTFacilities(int clientId, long facilityId, string userName, string password);
        void CT_AddOrEditCTFacilities(List<CTFacility> ctFacilities);
        Task<ETLResponseModel> CT_GetCTContainerServices(CensiTrackModuleOptions options);
        Task<List<ETLCTContainerService>> CT_GetCTContainerServices(int clientId);
        void CT_AddOrEditCTContainerServices(List<Framework.DataLayer.CTContainerService> ctContainerServices);
        Task<ETLResponseModel> CT_GetCTContainerTypesByService(CensiTrackModuleOptions options);
        Task<List<ETLCTContainerType>> CT_GetCTContainerTypesByService(int clientId, long facilityId, long serviceId);
        Task<ETLCTContainerType> CT_GetCTContainerTypeItem(int clientId, long legacyId);
        void CT_AddOrEditCTContainerTypes(List<CTContainerType> ctContainerTypes, long serviceId);
        Task<List<ETLCTProduct>> CT_GetCTProducts(int clientId);
        Task<ETLResponseModel> CT_GetCTProducts(int clientId, long facilityId, string userName, string password);
        void CT_AddOrEditCTProducts(List<CTProduct> ctProducts);
        Task<ETLResponseModel> CT_GetCTVendors(CensiTrackModuleOptions options);
        Task<List<ETLCTVendor>> CT_GetCTVendors(int clientId);
        void CT_AddOrEditCTVendors(List<CTVendor> ctVendors);
        Task<List<ETLCTContainerItem>> CT_GetCTContainerTypeItems(int clientId, long containerId);
        void CT_AddOrEditCTContainerItems(List<CTContainerItem> ctContainerItems, long containerId);
        Task<List<ETLCTCaseContainer>> CT_GetCTCaseContainers(int clientId, long facilityId, string caseReference, bool? showReprocessingOnly = true);
        Task<ETLResponseModel> CT_GetCTCaseContainers(CensiTrackModuleOptions options, int serverOffsetInMinsFromUTC);
        void CT_AddOrEditCTCaseContainers(List<CTCaseContainer> ctCaseContainers, int serverOffsetInMinsFromUTC, bool? isStopLifecycleHistory = false);
        Task<ETLResponseModel> CT_GetCTCasesContainersAndSubmitToWW(CensiTrackModuleOptions options, int serverOffsetInMinsFromUTC);
        CTContainerType CT_GetCTContainerByContainerTypeId(long containerTypeId);
        Task<ETLCTContainerType> CT_GetCTContainerTypesByService(int clientId, long containerTypeId);
        ProfitOptics.Framework.DataLayer.CTContainerService CT_GetCTContainerServiceByLegacyId(int serviceId);
        Task<ETLCTContainerService> CT_GetCTContainerServiceByLegacyId(int clientId, long serviceId);
        CTContainerTypeActual CT_GetCTContainerTypeActualByLegacyId(long containerId);
        Task<List<ETLCTContainerTypeActual>> CT_GetCTContainerTypeActualssByContainerTypes(int clientId, long containerTypeId);
        void CT_AddOrEditCTContainerTypeActuals(List<CTContainerTypeActual> ctContainerTypeActuals, long containerTypeId, int serverOffsetInMinsFromUTC);
        Task<ETLCTContainerTypeActualHistoryResponse> CT_GetCTContainerTypeActualHistoryItems(int clientId, long actualId);
        void CT_AddOrEditCTContainerTypeActualHistoryItemsAndGCTrays(List<Framework.DataLayer.CTContainerTypeActualHistoryItem> ctContainerTypeActualHistoryItems, long serviceId, int serverOffsetInMinsFromUTC, bool? isStopLifecycleHistory = false, bool? updateLapCountAndLocation = false);
        void CT_UpdateLCForCompletedCases(int serverOffsetInMinsFromUTC);
        void CT_SettingGCTrayActiveStatus(List<long?> allLegacyIds);
        void CT_UpdateColorStatusForGCCasesAndGCTrays(List<CTCase> ctCasesToUse);
        void CT_CancelOrDeleteCTCanceledCases(List<string> caseReferences, int serverOffsetInMinsFromUTC);
        Task<ETLResponseModel> CT_GetCTLocationTypes(int clientId, long facilityId, string userName, string password);
        Task<List<ETLCTLocationType>> CT_GetCTLocationTypes(int clientId);
        void CT_AddOrEditCTLocationTypes(List<CTLocationType> ctLocationTypes);
        Task<ETLResponseModel> CT_GetCTLocations(int clientId, long facilityId, string userName, string password);
        Task<ETLCTLocationsResponse> CT_GetCTLocations(int clientId);
        void CT_AddOrEditCTLocations(List<CTLocation> ctLocation);
        Task<List<ETLCTContainerTypeGraphic>> CT_GetCTContainerTypeGraphic(int clientId, long containerTypeId);
        void CT_AddOrEditCTContainerTypeGraphics(List<CTContainerTypeGraphic> ctContainerTypeGraphics, long containerTypeId);
        List<CTContainerTypeGraphic> CT_GetCTContainerTypeGraphic(bool? isWithoutImage);
        Task<byte[]> CT_GetCTGraphicImage(int clientId, long graphicId);
        void CT_UpdateCTContainerTypeGraphic(int ctContainerTypeGraphicId, byte[] ctContainerTypeImage);
        List<CTBILoadResultGraphic> CT_GetCTLoadResultGraphics(bool? isLoadMeadiaWithoutImage);
        void CT_UpdateCTLoadResultGraphic(int id, byte[] ctBILoadResultImage);
        List<CTAssemblyGraphic> CT_GetCTAssemblyGraphics(bool? isWithoutImage);
        void CT_UpdateCTAssemblyGraphicsGraphic(int id, byte[] imageBytes);
        Task<List<ETLCTBILoadResult>> CT_GetCTBILoadResults(int clientId);
        Task<ETLCTBILoadResultContentResponse> CT_GetCTBILoadResultContents(int clientId, long loadId);
        bool CT_AddOrEditCTBILoadResult(CTBILoadResult item, int serverOffsetInMinsFromUTC);
        void CT_AddOrEditCTBILoadResultContents(List<CTBILoadResultContent> contents, long loadId, int serverOffsetInMinsFromUTC);
        Task<ETLCTBILoadResultIndicatorResponse> CT_GetCTBILoadResultIndicators(int clientId, long loadId);
        void CT_AddOrEditCTBILoadResultIndicators(List<CTBILoadResultIndicator> indicators, long loadId, int serverOffsetInMinsFromUTC);
        Task<List<ETLCTBILoadResultGraphic>> CT_GetCTBILoadResultGraphics(int clientId, long loadId);
        void CT_AddOrEditCTBILoadResultGraphics(List<CTBILoadResultGraphic> graphics, long loadId, int serverOffsetInMinsFromUTC);
        void CT_UpdateCTLoadResultImportStatus(long loadId, bool isImportSuccessfull);
        void CT_SyncTimestamps(int serverOffsetInMinsFromUTC);
        List<long> CT_GetContainerAssemblyActualIds();
        List<GCTraysLifecycleStatusTypes> CT_GetAssemblyStatuses();
        Task<ETLCTCurrentAssemblyGraphics> CT_GetCTContainerAssemblyGraphics(int clientId, long containerTypeId, Guid censisSetHistoryId);
        Task<ETLCTContainerAssembly> CT_GetCTContainerAssemblyForActual(int clientId, long legacyId);
        void CT_AddCTAssemblyGraphic(ETLCTAssemblyGraphic assemblyGraphic, int statusTypeId, long legacyId);
        Task<ETLCTContainerAssemblyCountSheet> CT_GetCTContainerAssemblyCountSheets(int clientId, long containerTypeId);
        void CT_AddCTAssemblyCountSheet(ETLCTContainerAssemblyCountSheetItem countSheet, string setName, int statusTypeId);
        void CT_SyncLCForPastCases();
        void CT_SyncColorForPastCases();
        void LogProcessedAction(string username, string source, string fileName, string processingTime, string status, int rowCount, string message = null, int? actionType = null, string options = null, string description = "");
        public void AddGCTrayActualHistory(GCTrayActualHistory gcTrayActualHistory);
        void UpdateGCTraysLapCount(bool? allTrays = false);
        void UpdateHistoryItemAndLapCount(CTContainerTypeActualHistoryItem hi, int minsToAdd, bool isActualLoaner, bool? updateLapCountAndLocation = false);
        void UpdateAllHistoryItemsAndLapCount(int minsToAdd, bool? allHistoryItems = false);
        List<GCLifecycleStatusType> GetAllGCLifecycles();
        void UpdateGCTrayLapCount(GCTray gcTray, List<int> ctLifeCycleIds);
        List<CTContainerTypeActualHistoryItem> GetActualHistoryItems(int ctContainerTypeActualId, int? gcLifecycleId = null);
        GCTraysLifecycleStatusTypes GetGCTraysLifeCycleForId(int trayLifecycleId);
        int GetIncompleteSheetCountForTrayIds(List<int> trayIds);
        bool HasActualIncompleteCountSheet(int cTContainerTypeActualId);
        int GetSystemAlertCountForTrayIds(List<int> trayIds);
        void RaiseSkippedRequiredCTStep(int gcTrayId, int trayLifecycleId);
        void UpdateGCTrayLifecycles(bool runAnyway);
        Task<ETLResponseModel> CT_GetQualityFeedResults(int clientId, string userName, string password, int daysBehind, int serverOffsetInMinsFromUTC);
        Task<ETLCTQualityFeedResponse> CT_GetQualityFeedResponse(int clientId, ETLCTQualityFeedRequest request);
        Task<ETLResponseModel> CT_GetContainerTypeActualHistory(int clientId, long facilityId, string userName, string password, int serverOffsetInMinsFromUTC, bool isStopLifecycleHistory, string cycleId, long actualId);
        void CheckForCaseAssemblyAndTransportationStaging(int clientId, string caseReference, int serverOffsetInMinsFromUTC, bool? isStopLifecycleHistory = false);

        #endregion
    }
}