﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class DataBridgeRawData
    {
        public List<DataBridgeItem> DataBridge { get; set; }
        public List<SalesHistory> PricingCreaditsP1 { get; set; }
        public List<SalesHistory> PricingCreaditsP2 { get; set; }
        public List<SalesHistory> ShipDebitP1 { get; set; }
        public List<SalesHistory> ShipDebitP2 { get; set; }
        public List<SalesHistory> PolitiqueP1 { get; set; }
        public List<SalesHistory> PolitiqueP2 { get; set; }
        public List<SalesHistory> InvoicesNoSalesP1 { get; set; }
        public List<SalesHistory> InvoicesNoSalesP2 { get; set; }
        public List<SalesHistory> InvoicesNoCostP1 { get; set; }
        public List<SalesHistory> InvoicesNoCostP2 { get; set; }
    }
}