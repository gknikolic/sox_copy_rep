﻿namespace ProfitOptics.Framework.Web.Framework.Identity
{
    public static class ApplicationClaimTypes
    {
        public static string OriginalUserId => "OriginalUserId";
        public static string RegistrationDateUtc => "StartDate";
    }
}