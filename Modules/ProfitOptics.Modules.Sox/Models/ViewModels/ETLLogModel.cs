﻿using ProfitOptics.Modules.Sox.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class ETLLogModel
    {
        public int Id { get; set; }
        public string Source { get; set; }
        public string FileName { get; set; }
        public string Username { get; set; }
        public DateTime ProcessingTime { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public int RowCount { get; set; }
        public string ActionType { get; set; }
        public string Options { get; set; }
        public string Description { get; set; }
        public string ProcessingTimeLocal { get { return ProcessingTime.GetLocalDateTimeString(); } }
        public bool IsTimeExceeded { get { return ProfitOptics.Modules.Sox.Helpers.Helpers.IsETLCycleExceeded(ProcessingTime); } }
    }
}
