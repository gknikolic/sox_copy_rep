﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProfitOptics.Modules.BackgroundTasks.Data.Domain;

namespace ProfitOptics.Modules.BackgroundTasks.Services
{
    public interface IBackgroundTaskService
    {
        /// <summary>
        /// Gets the active tasks for the specified user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>The tasks for the specified user identifier.</returns>
        Task<List<BackgroundTask>> GetActiveTasksAsync(int userId);
    }
}