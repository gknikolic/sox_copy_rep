﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class TrayHistoryVerificationQualityFeed
    {
        public string ReportType { get; set; }
        public DateTime UpdatedDateUtc { get; set; }
        public string ReportedBy { get; set; }
        public string ResponsibleParty { get; set; }
        public string AssetName { get; set; }
        public List<string> ImagesData { get; set; }
    }
}
