USE [Sox]

CREATE TABLE CTContainerItem
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[LegacyId] [BIGINT] NOT NULL,
	[ProductId] [NUMERIC](18,2) NOT NULL,
	[CTProductId] INT NOT NULL,
	[CTContainerTypeId] INT NOT NULL,
	[RequiredCount] INT NOT NULL,
	[Placement] [NVARCHAR](200) NULL,
	[SubstitutionsAllowed] [BIT] NOT NULL,
	[CriticalItem] [BIT] NOT NULL,
	[ChangeState] [NVARCHAR](200) NULL,
	[SequenceNumber] INT NOT NULL,
	[CreatedAtUtc] DateTime NOT NULL,
	[ModifiedAtUtc] DateTime NULL
CONSTRAINT [PK_CTContainerItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTContainerItem]  WITH CHECK ADD  CONSTRAINT [FK_CTContainerItem_CTProduct] FOREIGN KEY([CTProductId])
REFERENCES [dbo].[CTProduct] ([Id])
GO
ALTER TABLE [dbo].[CTContainerItem] CHECK CONSTRAINT [FK_CTContainerItem_CTProduct]
GO

ALTER TABLE [dbo].[CTContainerItem]  WITH CHECK ADD  CONSTRAINT [FK_CTContainerItem_CTContainerType] FOREIGN KEY([CTContainerTypeId])
REFERENCES [dbo].[CTContainerType] ([Id])
GO
ALTER TABLE [dbo].[CTContainerItem] CHECK CONSTRAINT [FK_CTContainerItem_CTContainerType]
GO