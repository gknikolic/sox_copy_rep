﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Support.Data.Domain;

namespace ProfitOptics.Modules.Support.Data.Config
{
    public class JiraSupportNotificationConfig : IEntityTypeConfiguration<JiraSupportNotification>
    {
        public void Configure(EntityTypeBuilder<JiraSupportNotification> builder)
        {
            builder.ToTable("JiraSupportNotification");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Email).HasMaxLength(255).IsRequired();
            builder.Property(entity => entity.IssueId).HasMaxLength(255).IsRequired();
            builder.Property(entity => entity.ProjectId).HasMaxLength(150).IsRequired();
        }
    }
}
