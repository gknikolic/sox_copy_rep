namespace ProfitOptics.Framework.Web.RestApi.Resources.Product
{
    public class ProductQueryResource : QueryResource
    {
        public int? DistributorId { get; set; }
        public int? CustomerId { get; set; }
    }
}