﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTQuoteItemConfig : IEntityTypeConfiguration<QTQuoteItem>
    {
        public void Configure(EntityTypeBuilder<QTQuoteItem> builder)
        {
            builder.ToTable("QTQuoteItem");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.CompetitorUoM).HasMaxLength(20);
           
        }
    }
}
