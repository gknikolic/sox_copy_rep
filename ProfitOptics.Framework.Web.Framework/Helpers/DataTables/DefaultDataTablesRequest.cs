﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

namespace ProfitOptics.Framework.Web.Framework.Helpers.DataTables
{
    public class DefaultDataTablesRequest : IDataTablesRequest
    {
        /// <summary>
        /// Gets/Sets the draw counter from DataTables.
        /// </summary>
        public virtual int Draw { get; set; }
        
        /// <summary>
        /// Gets/Sets the start record number (jump) for paging.
        /// </summary>
        public virtual int Start { get; set; }
        
        /// <summary>
        /// Gets/Sets the length of the page (paging).
        /// </summary>
        public virtual int Length { get; set; }
        
        /// <summary>
        /// Gets/Sets the global search term.
        /// </summary>
        public virtual Search Search { get; set; }
        
        /// <summary>
        /// Gets/Sets the column collection.
        /// </summary>
        public virtual ColumnCollection Columns { get; set; }

        /// <summary>
        /// Gets or sets the value indicating if the returned data should by viewed by sales.
        /// </summary>
        public bool ViewBySales { get; set; }

        public static IDataTablesRequest MakeFlat(IDataTablesRequest request)
        {
            return new DefaultDataTablesRequest
            {
                Draw = 0,
                Start = 0,
                Length = int.MaxValue,
                Search = request.Search,
                Columns = request.Columns,
                ViewBySales = request.ViewBySales
            };
        }

        public static IDataTablesRequest Default => new DefaultDataTablesRequest
        {
            Draw = 0,
            Start = 0,
            Length = int.MaxValue
        };
    }
}
