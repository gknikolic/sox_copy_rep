﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace ProfitOptics.Framework.Web.Framework.Helpers.Extensions
{
    public static class SessionExtensions
    {
        public static T Get<T>(this ISession session, string key, Func<T> acquire = null)
        {
            if (!session.Keys.Contains(key) && acquire != null)
            {
                var data = acquire();
                session.SetString(key, JsonConvert.SerializeObject(data));
                return data;
            }

            if (!session.Keys.Contains(key) && acquire == null)
            {
                return default;
            }

            var serializedData = session.GetString(key);
            return JsonConvert.DeserializeObject<T>(serializedData);
        }

        public static void Set<T>(this ISession session, string key, T value)
        {
            var data = JsonConvert.SerializeObject(value);
            session.SetString(key, data);
        }

        public static double? GetDouble(this ISession session, SessionKeys key)
        {
            var data = session.Get(key.ToString());
            if (data == null)
            {
                return null;
            }
            return BitConverter.ToDouble(data, 0);
        }

        public static void SetDouble(this ISession session, SessionKeys key, double value)
        {
            session.Set(key.ToString(), BitConverter.GetBytes(value));
        }

        public static T GetObject<T>(this ISession session, SessionKeys key)
        {
            var data = session.GetString(key.ToString());

            if (data == null)
            {
                return default;
            }

            return JsonConvert.DeserializeObject<T>(data);
        }

        public static void SetObject(this ISession session, SessionKeys key, object value)
        {
            session.SetString(key.ToString(), JsonConvert.SerializeObject(value));
        }

        public static void SetByteArray(this ISession session, SessionKeys key, byte[] value)
        {
            session.Set(key.ToString(), value);
        }

        public static byte[] GetByteArray(this ISession session, SessionKeys key)
        {
            return session.Get(key.ToString());
        }

        public static void RemoveObject(this ISession session, SessionKeys key)
        {
            session.SetObject(key, null);
        }
    }
}