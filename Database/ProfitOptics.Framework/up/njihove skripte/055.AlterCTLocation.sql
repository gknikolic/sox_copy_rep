USE [Sox]

ALTER TABLE [dbo].[CTLocation] DROP CONSTRAINT CTLocation_CTLocationType;
ALTER TABLE [dbo].[CTLocation] DROP CONSTRAINT CTLocation_CTFacility;

ALTER TABLE [dbo].[CTLocation]  WITH CHECK ADD  CONSTRAINT [FK_CTLocation_CTLocationType] FOREIGN KEY([CTLocationTypeId])
REFERENCES [dbo].[CTLocationType] ([Id])
GO
ALTER TABLE [dbo].[CTLocation] CHECK CONSTRAINT [FK_CTLocation_CTLocationType]
GO

ALTER TABLE [dbo].[CTLocation]  WITH CHECK ADD  CONSTRAINT [FK_CTLocation_CTFacility] FOREIGN KEY([CTFacilityId])
REFERENCES [dbo].[CTFacility] ([Id])
GO
ALTER TABLE [dbo].[CTLocation] CHECK CONSTRAINT [FK_CTLocation_CTFacility]
GO