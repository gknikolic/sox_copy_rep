﻿using ProfitOptics.Modules.Sox.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Services
{
    public interface IWorkWaveService
    {
        Task<WorkWaveResponse> AddOrderToWWAsync(int CaseId, bool createOtugoingOrder = true, bool createIncomingOrder = true);

        Task<List<string>> CompareBarcodesWithWorkWaveAsync(int gcCaseId);

        Task<string> SyncOrdersWithWorkWaveAsync();

        Task GetGPSDevicesCurrentPos();
        Task SyncGPSDevices();
    }
}
