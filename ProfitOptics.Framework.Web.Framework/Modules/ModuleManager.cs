﻿using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Newtonsoft.Json;
using ProfitOptics.Framework.Core;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace ProfitOptics.Framework.Web.Framework.Modules
{
    public class ModuleManager
    {
        private static readonly ReaderWriterLockSlim Locker = new ReaderWriterLockSlim();
        private static readonly IApplicationFileProvider _fileProvider;
        private static readonly List<string> _baseAppLibraries;
        private static string _shadowCopyFolder;
        private static string _reserveShadowCopyFolder;

        public static IEnumerable<ModuleDescriptor> ReferencedModules { get; set; }

        static ModuleManager()
        {
            _fileProvider = CommonHelper.DefaultFileProvider;

            //get all libraries from /bin/{version}/ directory
            _baseAppLibraries = _fileProvider.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll").Select(fi => _fileProvider.GetFileName(fi)).ToList();

            //get all libraries from base site directory
            if (!AppDomain.CurrentDomain.BaseDirectory.Equals(Environment.CurrentDirectory, StringComparison.InvariantCultureIgnoreCase))
                _baseAppLibraries.AddRange(_fileProvider.GetFiles(Environment.CurrentDirectory, "*.dll").Select(fi => _fileProvider.GetFileName(fi)));

            //get all libraries from refs directory
            var refsPathName = _fileProvider.Combine(Environment.CurrentDirectory, FrameworkModuleDefaults.RefsPathName);
            if (_fileProvider.DirectoryExists(refsPathName))
                _baseAppLibraries.AddRange(_fileProvider.GetFiles(refsPathName, "*.dll").Select(fi => _fileProvider.GetFileName(fi)));
        }

        public static void Initialize(ApplicationPartManager applicationPartManager)
        {
            if (applicationPartManager == null)
                throw new ArgumentNullException(nameof(applicationPartManager));

            using (new WriteLockDisposable(Locker))
            {
                var moduleFolder = _fileProvider.MapPath(FrameworkModuleDefaults.Path);
                _shadowCopyFolder = _fileProvider.MapPath(FrameworkModuleDefaults.ShadowCopyPath);
                _reserveShadowCopyFolder = _fileProvider.Combine(_fileProvider.MapPath(FrameworkModuleDefaults.ShadowCopyPath), $"{FrameworkModuleDefaults.ReserveShadowCopyPathName}{DateTime.Now.ToFileTimeUtc()}");

                var referencedModules = new List<ModuleDescriptor>();
                _fileProvider.CreateDirectory(moduleFolder);
                _fileProvider.CreateDirectory(_shadowCopyFolder);

                var binFiles = _fileProvider.GetFiles(_shadowCopyFolder, "*", false);
                foreach (var file in binFiles)
                {
                    try
                    {
                        _fileProvider.DeleteFile(file);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Error deleting file " + file + ". Exception: " + ex);
                    }
                }

                foreach (var directory in _fileProvider.GetDirectories(_shadowCopyFolder, FrameworkModuleDefaults.ReserveShadowCopyPathName))
                {
                    try
                    {
                        _fileProvider.DeleteDirectory(directory);
                    }
                    catch
                    {
                    }
                }

                foreach (var moduleDescription in GetDescriptionFilesAndDescriptors(moduleFolder))
                {
                    var descriptionFile = moduleDescription.Key;
                    var moduleDescriptor = moduleDescription.Value;

                    if (string.IsNullOrWhiteSpace(moduleDescriptor.SystemName))
                        throw new Exception($"A module '{descriptionFile}' has no system name. Try assigning the module a unique name and recompiling.");
                    if (referencedModules.Contains(moduleDescriptor))
                        throw new Exception($"A module with '{moduleDescriptor.SystemName}' system name is already defined");

                    try
                    {
                        var directoryName = _fileProvider.GetDirectoryName(descriptionFile);
                        if (string.IsNullOrEmpty(directoryName))
                            throw new Exception(
                                $"Directory cannot be resolved for '{_fileProvider.GetFileName(descriptionFile)}' description file");

                        //get list of all DLLs in modules (not in bin!)
                        var moduleFiles = _fileProvider.GetFiles(directoryName, "*.dll", false)
                            //just make sure we're not registering shadow copied modules
                            .Where(x => !binFiles.Select(q => q).Contains(x))
                            .Where(x => IsPackageModuleFolder(_fileProvider.GetDirectoryName(x)))
                            .ToList();

                        //other plugin description info
                        var mainModuleFile = moduleFiles
                            .FirstOrDefault(x => _fileProvider.GetFileName(x).Equals(moduleDescriptor.AssemblyFileName,
                                StringComparison.InvariantCultureIgnoreCase));

                        if (mainModuleFile == null)
                        {
                            continue;
                        }

                        moduleDescriptor.OriginalAssemblyFile = mainModuleFile;

                        moduleDescriptor.ReferencedAssembly = PerformFileDeploy(mainModuleFile, applicationPartManager);

                        //load all other referenced assemblies now
                        foreach (var module in moduleFiles
                            .Where(x => !_fileProvider.GetFileName(x).Equals(_fileProvider.GetFileName(mainModuleFile),
                                StringComparison.InvariantCultureIgnoreCase))
                            .Where(x => !IsAlreadyLoaded(x)))
                            PerformFileDeploy(module, applicationPartManager);

                        /*foreach (var t in moduleDescriptor.ReferencedAssembly.GetTypes())
                            if (typeof(IModule).IsAssignableFrom(t))
                                if (!t.IsInterface)
                                    if (t.IsClass && !t.IsAbstract)
                                    {
                                        moduleDescriptor.ModuleType = t;
                                        break;
                                    }*/

                        referencedModules.Add(moduleDescriptor);
                    }
                    catch (ReflectionTypeLoadException ex)
                    {
                        //add a plugin name. this way we can easily identify a problematic plugin
                        var msg = $"Module '{moduleDescriptor.FriendlyName}'. ";
                        foreach (var e in ex.LoaderExceptions)
                            msg += e.Message + Environment.NewLine;

                        var fail = new Exception(msg, ex);
                        throw fail;
                    }
                    catch (Exception ex)
                    {
                        var msg = $"Module '{moduleDescriptor.FriendlyName}'. {ex.Message}";

                        var fail = new Exception(msg, ex);
                        throw fail;
                    }
                }

                ReferencedModules = referencedModules;
            }
        }

        private static bool IsAlreadyLoaded(string filePath)
        {
            if (_baseAppLibraries.Any(sli => sli.Equals(_fileProvider.GetFileName(filePath), StringComparison.InvariantCultureIgnoreCase)))
                return true;

            try
            {
                var fileNameWithoutExt = _fileProvider.GetFileNameWithoutExtension(filePath);
                if (string.IsNullOrEmpty(fileNameWithoutExt))
                    throw new Exception($"Cannot get file extension for {_fileProvider.GetFileName(filePath)}");

                foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                {
                    var assemblyName = a.FullName.Split(',').FirstOrDefault();
                    if (fileNameWithoutExt.Equals(assemblyName, StringComparison.InvariantCultureIgnoreCase))
                        return true;
                }
            }
            catch (Exception exc)
            {
                Debug.WriteLine("Cannot validate whether an assembly is already loaded. " + exc);
            }

            return false;
        }

        private static Assembly PerformFileDeploy(string file, ApplicationPartManager applicationPartManager, string shadowCopyPath = "")
        {
            var parent = string.IsNullOrEmpty(file) ? string.Empty : _fileProvider.GetParentDirectory(file);

            if (string.IsNullOrEmpty(parent))
                throw new InvalidOperationException($"The module directory for the {_fileProvider.GetFileName(file)} file exists in a folder outside of the allowed folder hierarchy");

            if (string.IsNullOrEmpty(shadowCopyPath))
                shadowCopyPath = _shadowCopyFolder;

            _fileProvider.CreateDirectory(shadowCopyPath);

            var shadowCopiedModule = ShadowCopyFile(file, shadowCopyPath);

            Assembly shadowCopiedAssembly = RegisterPluginDefinition(applicationPartManager, shadowCopiedModule);

            return shadowCopiedAssembly ?? PerformFileDeploy(file, applicationPartManager, _reserveShadowCopyFolder);
        }

        private static Assembly RegisterPluginDefinition(ApplicationPartManager applicationPartManager, string module)
        {
            Assembly pluginAssembly;
            try
            {
                pluginAssembly = Assembly.LoadFrom(module);
            }
            catch (FileLoadException)
            {
                pluginAssembly = Assembly.UnsafeLoadFrom(module);
            }

            Debug.WriteLine("Adding to ApplicationParts: '{0}'", pluginAssembly.FullName);
            applicationPartManager.ApplicationParts.Add(new AssemblyPart(pluginAssembly));

            return pluginAssembly;
        }

        private static string ShadowCopyFile(string file, string shadowCopyPath)
        {
            var shouldCopy = true;
            var shadowCopiedModule = _fileProvider.Combine(shadowCopyPath, _fileProvider.GetFileName(file));

            //check if a shadow copied file already exists and if it does, check if it's updated, if not don't copy
            if (_fileProvider.FileExists(shadowCopiedModule))
            {
                //it's better to use LastWriteTimeUTC, but not all file systems have this property
                //maybe it is better to compare file hash?
                var areFilesIdentical = _fileProvider.GetCreationTime(shadowCopiedModule).ToUniversalTime().Ticks >= _fileProvider.GetCreationTime(file).ToUniversalTime().Ticks;
                if (areFilesIdentical)
                {
                    Debug.WriteLine("Not copying; files appear identical: '{0}'", _fileProvider.GetFileName(shadowCopiedModule));
                    shouldCopy = false;
                }
                else
                {
                    //delete an existing file

                    //More info: https://www.nopcommerce.com/boards/t/11511/access-error-nopplugindiscountrulesbillingcountrydll.aspx?p=4#60838
                    Debug.WriteLine("New plugin found; Deleting the old file: '{0}'", _fileProvider.GetFileName(shadowCopiedModule));
                    _fileProvider.DeleteFile(shadowCopiedModule);
                }
            }

            if (!shouldCopy)
                return shadowCopiedModule;

            try
            {
                _fileProvider.FileCopy(file, shadowCopiedModule, true);
            }
            catch (IOException)
            {
                Debug.WriteLine(shadowCopiedModule + " is locked, attempting to rename");
                //this occurs when the files are locked,
                //for some reason devenv locks plugin files some times and for another crazy reason you are allowed to rename them
                //which releases the lock, so that it what we are doing here, once it's renamed, we can re-shadow copy
                try
                {
                    var oldFile = shadowCopiedModule + Guid.NewGuid().ToString("N") + ".old";
                    _fileProvider.FileMove(shadowCopiedModule, oldFile);
                }
                catch (IOException exc)
                {
                    throw new IOException(shadowCopiedModule + " rename failed, cannot initialize plugin", exc);
                }
                //OK, we've made it this far, now retry the shadow copy
                _fileProvider.FileCopy(file, shadowCopiedModule, true);
            }

            return shadowCopiedModule;
        }

        private static IEnumerable<KeyValuePair<string, ModuleDescriptor>> GetDescriptionFilesAndDescriptors(string moduleFolder)
        {
            if (moduleFolder == null)
                throw new ArgumentNullException(nameof(moduleFolder));

            //create list (<file info, parsed module descritor>)
            var result = new List<KeyValuePair<string, ModuleDescriptor>>();

            //add display order and path to list
            foreach (var descriptionFile in _fileProvider.GetFiles(moduleFolder, FrameworkModuleDefaults.DescriptionFileName, false))
            {
                if (!IsPackageModuleFolder(_fileProvider.GetDirectoryName(descriptionFile)))
                    continue;

                //parse file
                var moduleDescriptor = GetModuleDescriptorFromFile(descriptionFile);

                //populate list
                result.Add(new KeyValuePair<string, ModuleDescriptor>(descriptionFile, moduleDescriptor));
            }

            return result;
        }

        private static ModuleDescriptor GetModuleDescriptorFromFile(string filePath)
        {
            var text = _fileProvider.ReadAllText(filePath, Encoding.UTF8);

            return GetModuleDescriptorFromText(text);
        }

        private static ModuleDescriptor GetModuleDescriptorFromText(string text)
        {
            if (string.IsNullOrEmpty(text))
                return new ModuleDescriptor();

            //get plugin descriptor from the JSON file
            var descriptor = JsonConvert.DeserializeObject<ModuleDescriptor>(text);

            return descriptor;
        }

        private static bool IsPackageModuleFolder(string folder)
        {
            if (string.IsNullOrEmpty(folder))
                return false;

            var parent = _fileProvider.GetParentDirectory(folder);

            if (string.IsNullOrEmpty(parent))
                return false;

            if (!_fileProvider.GetDirectoryNameOnly(parent).Equals(FrameworkModuleDefaults.PathName, StringComparison.InvariantCultureIgnoreCase))
                return false;

            return true;
        }
    }
}
