﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueReporter
    {
        public string name { get; set; }

        public string emailAddress { get; set; }

        public string displayName { get; set; }
    }
}