﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class WebhookChangelogItem
    {
        public string field { get; set; }

        public string fromString { get; set; }

        public string toString { get; set; }
    }
}