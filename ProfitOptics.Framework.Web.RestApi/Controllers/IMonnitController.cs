﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Services;

namespace ProfitOptics.Framework.Web.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IMonnitController : ControllerBase
    {
        IMonnitService _monnitService;

        public IMonnitController(IMonnitService monnitService)
        {
            _monnitService = monnitService;
        }
        /// <summary>
        /// Test method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("test")]
        public async Task<StatusCodeResult> Test()
        {
            try
            {
                //await workWaveClient.GetOrdersFromWorWaveAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }
        [HttpPost]
        [Route("response")]
        public async Task<StatusCodeResult> IMonnitResponseAsync(object res)
        {
            try
            {
                var response = JsonConvert.DeserializeObject<MonnitWebhookResponse>(res.ToString());
                _monnitService.HandleMonnitWebhookResponse(response);
                return Ok();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
