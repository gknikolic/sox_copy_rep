﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Framework.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationUserDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly POFrameworkDefaults _poFrameworkDefaults;

        public UserService(UserManager<ApplicationUser> userManager,
            ApplicationUserDbContext dbContext,
            RoleManager<ApplicationRole> roleManager,
            POFrameworkDefaults poFrameworkDefaults)
        {
            this._userManager = userManager;
            this._dbContext = dbContext;
            this._roleManager = roleManager;
            this._poFrameworkDefaults = poFrameworkDefaults;
        }

        public async Task<ApplicationUser> FindUserByUsernameAsync(string username)
        {
            return await _userManager.FindByNameAsync(username);
        }

        public async Task<ApplicationUser> FindUserByIdAsync(int id)
        {
            var users = await _userManager.Users.Include(x => x.UserRoles).ThenInclude(x => x.Role).Include(x => x.GCChildCompanyUsers).Include(x => x.GCParentCompanyUsers).ToListAsync();
            var user = users.Where(x => x.Id == id).FirstOrDefault();
           
            //var user  = await _userManager.FindByIdAsync(id.ToString());         
            
            user.POUserProfile = await GetUserProfileAsync(id);
            return user;
        }

        public async Task<ApplicationUser> FindUserByEmailAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<IdentityResult> CreateUserAsync(string firstName, string lastName, string email, string password = null, string phone = null, byte[] profilePictureData = null)
        {
            var user = new ApplicationUser
            {
                UserName = email,
                NormalizedUserName = email.ToUpper(),
                Email = email,
                NormalizedEmail = email.ToUpper(),
                FirstName = firstName,
                LastName = lastName,
                PhoneNumber = phone,
                StartTime = DateTime.UtcNow,
            };
            if (profilePictureData != null)
            {
                user.ProfilePicture = new POUserProfilePicture
                {
                    User = user,
                    Data = profilePictureData
                };
            }

            if (string.IsNullOrEmpty(password))
            {
                return await _userManager.CreateAsync(user);
            }

            return await _userManager.CreateAsync(user, password);
        }

        public async Task<IdentityResult> AddExternalLoginAsync(ApplicationUser user, UserLoginInfo userLoginInfo)
        {
            return await _userManager.AddLoginAsync(user, userLoginInfo);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(ApplicationUser user, string confirmationToken)
        {
            return await _userManager.ConfirmEmailAsync(user, confirmationToken);
        }

        public async Task<IdentityResult> ResetAccessFailedCountAsync(ApplicationUser user)
        {
            return await _userManager.ResetAccessFailedCountAsync(user);
        }

        public async Task<IdentityResult> AddAccessFailedAttemptToUser(ApplicationUser user)
        {
            return await _userManager.AccessFailedAsync(user);
        }

        public async Task<IList<string>> GetValidTwoFactorProvidersForUserAsync(ApplicationUser user)
        {
            return await _userManager.GetValidTwoFactorProvidersAsync(user);
        }

        public async Task<string> GenerateTwoFactorTokenAsync(ApplicationUser user, string providerName)
        {
            return await _userManager.GenerateTwoFactorTokenAsync(user, providerName);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(user);
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(ApplicationUser user)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<IList<ApplicationUser>> GetUsersInRoleAsync(string roleName)
        {
            var usersInRole = await (from userInRole in _dbContext.UserRoles
                                     from role in _dbContext.Roles.Where(x => x.Id == userInRole.RoleId)
                                     from user in _dbContext.Users.Where(x => x.Id == userInRole.UserId)
                                     where role.Name == roleName
                                     select user).ToListAsync();

            return usersInRole;
        }

        public async Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string code, string password)
        {
            return await _userManager.ResetPasswordAsync(user, code, password);
        }

        public async Task<bool> IsLockedOutAsync(ApplicationUser user)
        {
            return await _userManager.IsLockedOutAsync(user);
        }

        public async Task<IdentityResult> SetLockoutEndDateAsync(ApplicationUser user, DateTimeOffset lockoutEndOffset)
        {
            return await _userManager.SetLockoutEndDateAsync(user, lockoutEndOffset);
        }

        public async Task<IList<ApplicationRole>> GetUserRolesAsync(ApplicationUser user)
        {
            var userInRoleNames = await _userManager.GetRolesAsync(user);
            List<ApplicationRole> roles = new List<ApplicationRole>();
            foreach (var roleName in userInRoleNames)
            {
                var role = await _roleManager.FindByNameAsync(roleName);
                roles.Add(role);
            }

            return roles;
        }

        public async Task<byte[]> GetProfilePictureAsync(int userId)
        {
            var result = (await _dbContext.POUserProfilePictures.FirstOrDefaultAsync(x => x.UserId == userId))?.Data;
           
            return result ?? File.ReadAllBytes(_poFrameworkDefaults.ProfilePictureAbsoluteFilePath);
        }

        public async Task<POUserProfile> GetUserProfileAsync(int userId)
        {
            return await _dbContext.POUserProfiles.FirstOrDefaultAsync(x => x.Id == userId);
        }

        public async Task<bool> IsEmailConfirmedAsync(ApplicationUser user)
        {
            return await _userManager.IsEmailConfirmedAsync(user);
        }

        public async Task<IdentityResult> UpdateUserAsync(ApplicationUser user)
        {
            return await _userManager.UpdateAsync(user);
        }

        public async Task<int> UpdateProfilePictureAsync(ApplicationUser user, byte[] profilePhotoBytes)
        {
            var profilePicture = _dbContext.POUserProfilePictures.FirstOrDefault(x => x.UserId == user.Id);

            if (profilePicture == null)
            {
                profilePicture = new POUserProfilePicture
                {
                    User = user,
                    Data = profilePhotoBytes
                };

                _dbContext.POUserProfilePictures.Add(profilePicture);
            }
            else
            {
                profilePicture.Data = profilePhotoBytes;
                _dbContext.POUserProfilePictures.Attach(profilePicture);
            }

            return await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> HasPasswordAsync(ApplicationUser user)
        {
            return await _userManager.HasPasswordAsync(user);
        }

        public async Task<string> GetPhoneNumberAsync(ApplicationUser user)
        {
            return await _userManager.GetPhoneNumberAsync(user);
        }

        public async Task<bool> GetTwoFactorEnabledAsync(ApplicationUser user)
        {
            return await _userManager.GetTwoFactorEnabledAsync(user);
        }

        public async Task<string> GenerateChangePhoneNumberTokenAsync(ApplicationUser user, string number)
        {
            return await _userManager.GenerateChangePhoneNumberTokenAsync(user, number);
        }

        public async Task<IdentityResult> ChangePhoneNumberAsync(ApplicationUser user, string phoneNumber, string code)
        {
            return await _userManager.ChangePhoneNumberAsync(user, phoneNumber, code);
        }

        public async Task<bool> IsTwoFactorAuthenticationEnabledAsync(ApplicationUser user)
        {
            return await _userManager.GetTwoFactorEnabledAsync(user);
        }

        public async Task<IdentityResult> SetTwoFactorAuthenticationEnabledAsync(ApplicationUser user, bool enabled)
        {
            return await _userManager.SetTwoFactorEnabledAsync(user, enabled);
        }

        public async Task<bool> IsGoogleAuthenticatorEnabledAsync(ApplicationUser user)
        {
            return await Task.FromResult<bool>(user.IsGoogleAuthenticatorEnabled);
        }

        public async Task<IdentityResult> SetAccountGoogleAuthenticatorAsync(ApplicationUser user, string key)
        {
            var result = await _userManager.SetTwoFactorEnabledAsync(user, true);
            if (!result.Succeeded)
            {
                return result;
            }

            user.IsGoogleAuthenticatorEnabled = true;
            user.GoogleAuthenticatorSecretKey = key;

            return await _userManager.UpdateAsync(user);
        }

        public async Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string currentPassword, string newPassword)
        {
            return await _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
        }

        public async Task<IList<IdentityUserLogin<int>>> GetExternalLoginsForUser(ApplicationUser user)
        {
            return await _dbContext.UserLogins.Where(x => x.UserId == user.Id).ToListAsync();
        }

        public async Task<IdentityResult> RemoveLoginAsync(ApplicationUser user, string loginProvider, string providerKey)
        {
            return await _userManager.RemoveLoginAsync(user, loginProvider, providerKey);
        }

        public async Task<IdentityResult> UpdateUserRolesAsync(ApplicationUser user, List<string> roles)
        {
            var currentRoles = await _userManager.GetRolesAsync(user);
            var removeResult = await _userManager.RemoveFromRolesAsync(user, currentRoles);
            if (!removeResult.Succeeded)
            {
                return removeResult;
            }

            return await _userManager.AddToRolesAsync(user, roles);
        }

        public async Task<IList<ApplicationRole>> GetRolesAsync()
        {
            return await _roleManager.Roles.ToListAsync();
        }

        public IQueryable<ApplicationUser> GetUsersAsQueryable(bool? isEnabled = true)
        {
            var query = _userManager.Users.Include("UserRoles").Include("UserRoles.Role");

            if(isEnabled.HasValue && isEnabled.Value)
            {
                query = query.Where(x => x.IsEnabled == isEnabled.Value);
            }

            return query;
        }

        public IQueryable<POUserProfilePicture> GetProfilePicturesQueryable()
        {
            return _dbContext.POUserProfilePictures;
        }

        public async Task<IdentityResult> RemovePhoneNumberAsync(ApplicationUser user)
        {
            return await _userManager.SetPhoneNumberAsync(user, null);
        }

        public async Task<IdentityResult> SetPasswordAsync(ApplicationUser user, string password)
        {
            return await _userManager.AddPasswordAsync(user, password);
        }

        public async Task<IdentityResult> AddUserToRolesAsync(ApplicationUser user, string[] roles)
        {
            return await _userManager.AddToRolesAsync(user, roles);
        }

        public async Task<int> EnableAndAddRolesToUser(string username, List<string> roles)
        {
            var appUser = _dbContext.Users.SingleOrDefault(user => user.UserName == username);

            if (appUser == null)
            {
                throw new Exception("User not found");
            }

            appUser.IsEnabled = true;
            appUser.Approved = true;

            await AddUserToRolesAsync(appUser, roles.ToArray());

            return await _dbContext.SaveChangesAsync();
        }

        public async Task<int> ConfirmEmail(string email, string token)
        {
            var appUser = FindUserByUsernameAsync(email).Result;

            await ConfirmEmailAsync(appUser, token);

            appUser.EmailConfirmed = true;

            return await _dbContext.SaveChangesAsync();
        }

        public IEnumerable<SelectListItem> GetGCParentCompanies()
        {
            var companies = _dbContext.GCParentCompany.ToList();

            var parentCompanies = companies.Select(company => new SelectListItem
            {
                Text = company.Title,
                Value = company.Id.ToString()
            });

            return parentCompanies;
        }

        public IEnumerable<GCParentCompany> GetParentCompanies()
        {
            var companies = _dbContext.GCParentCompany.ToList();

            return companies;
        }

        public GCChildCompany GetChildCompanyForCustomer(int customerId)
        {
            var childCompany = _dbContext.GCChildCompany.Where(x => x.GCCustomerId == customerId).FirstOrDefault();

            return childCompany;
        }

        public GCChildCompany GetChildCompanyForVendor(int vendorId)
		{
            var childCompany = _dbContext.GCChildCompany.Where(x => x.GCVendorId == vendorId).FirstOrDefault();

            return childCompany;
        }

        public IEnumerable<GCChildCompany> GetChildCompanies()
        {
            var companies = _dbContext.GCChildCompany.ToList();

            return companies;
        }

        public GCChildCompany GetChildCompanyById(int id)
        {
            var company = _dbContext.GCChildCompany.Find(id);

            return company;
        }

        public void AddUserToGCParentCompany(int userId, IEnumerable<int> parentCompanyIds)
        {

            IEnumerable<GCParentCompanyUsers> list = parentCompanyIds.Select(x => new GCParentCompanyUsers()
            {
                UserId = userId,

                GCParentCompanyId = x
            });

            _dbContext.GCParentCompanyUsers.AddRange(list);
            _dbContext.SaveChanges();
        }

        public ParentsWithChildCompaniesModel GetChildrenForGCParentCompany(List<int> parentCompanyIds)
        {
            //var childCompanies = parentCompanyIds.Select(x => _dbContext.GCChildCompany.Where(y => y.GCParentCompanyId == x).ToList());
            //var childCompanies = await _dbContext.GCChildCompany.Where(x => x.GCParentCompanyId == parentCompanyIds).ToListAsync();

            ParentsWithChildCompaniesModel companies = new ParentsWithChildCompaniesModel();

            companies.ParentCompanies = new List<ParentCompanyModel>();

            for (int i = 0; i < parentCompanyIds.Count; i++)
            {
                var parentCompanyData = _dbContext.GCParentCompany.Find(parentCompanyIds[i]);

                ParentCompanyModel parentCompany = new ParentCompanyModel();

                parentCompany.Id = parentCompanyData.Id;
                parentCompany.Title = parentCompanyData.Title;

                companies.ParentCompanies.Add(parentCompany);

                var childCompaniesData = _dbContext.GCChildCompany.Where(x => x.GCParentCompanyId == parentCompanyIds[i]).ToList();

                companies.ParentCompanies[i].ChildCompanies = new List<ChildCompanyModel>();

                for (int j = 0; j < childCompaniesData.Count; j++)
                {
                    ChildCompanyModel childCompany = new ChildCompanyModel();

                    childCompany.Id = childCompaniesData[j].Id;
                    childCompany.Title = childCompaniesData[j].Title;
                    childCompany.GCCustomerId = childCompaniesData[j].GCCustomerId;
                    childCompany.GCVendorId = childCompaniesData[j].GCVendorId;

                    companies.ParentCompanies[i].ChildCompanies.Add(childCompany);
                }
            }

            return companies;
        }

        public ParentCompanyModel GetParentCompanyWithChilder(int companyId, string childCompanyPermissionValues = null)
		{
            var parentCompanyData = _dbContext.GCParentCompany.Find(companyId);

            ParentCompanyModel parentCompany = new ParentCompanyModel();
            if(parentCompanyData==null)
			{
                return parentCompany;
			}
            parentCompany.Id = parentCompanyData.Id;
            parentCompany.Title = parentCompanyData.Title;


            var childCompaniesData = _dbContext.GCChildCompany.Where(x => x.GCParentCompanyId == parentCompanyData.Id).ToList();

            var childCompanyPermissions = new List<string>();
            if(childCompanyPermissionValues != null)
            {
                childCompanyPermissions = childCompanyPermissionValues.Split(',').ToList();
            }

            for (int j = 0; j < childCompaniesData.Count; j++)
            {
                ChildCompanyModel childCompany = new ChildCompanyModel();

                childCompany.Id = childCompaniesData[j].Id;
                childCompany.Title = childCompaniesData[j].Title;

                // initialy set checkboxes checked:
                childCompany.PermissionTypeViewSelected = true;
                childCompany.PermissionTypeEditSelected = true;
                childCompany.PermissionTypeAddSelected = true;
                childCompany.PermissionTypeDeleteSelected = true;

                // if company has permissions, set accordingly:
                if (childCompanyPermissions.Count > 0)
                {
                    childCompany.PermissionTypeViewSelected = childCompanyPermissions.Where(x => x == string.Format("{0}_{1}", Enums.CompanyPermissionType.CompanyPermissionTypeView.ToString(), childCompany.Id)).FirstOrDefault() != null;
                    childCompany.PermissionTypeEditSelected = childCompanyPermissions.Where(x => x == string.Format("{0}_{1}", Enums.CompanyPermissionType.CompanyPermissionTypeEdit.ToString(), childCompany.Id)).FirstOrDefault() != null;
                    childCompany.PermissionTypeAddSelected = childCompanyPermissions.Where(x => x == string.Format("{0}_{1}", Enums.CompanyPermissionType.CompanyPermissionTypeAdd.ToString(), childCompany.Id)).FirstOrDefault() != null;
                    childCompany.PermissionTypeDeleteSelected = childCompanyPermissions.Where(x => x == string.Format("{0}_{1}", Enums.CompanyPermissionType.CompanyPermissionTypeDelete.ToString(), childCompany.Id)).FirstOrDefault() != null;
                }

                parentCompany.ChildCompanies.Add(childCompany);
            }

            return parentCompany;
        }

        public IEnumerable<GCParentCompany> GetParentCompaniesByUserId(int userId)
        {
            var usersCompanies = _dbContext.GCParentCompanyUsers.Where(x => x.UserId == userId).ToList();

            var parentCompaniesIds = usersCompanies.Select(x => x.GCParentCompanyId).ToList();

            var parentCompanies = _dbContext.GCParentCompany.Where(x => parentCompaniesIds.Contains(x.Id)).ToList();

            return parentCompanies;
        }

        public void EditUsersParentCompanies(int userId, IEnumerable<int> parentCompaniesIds)
        {
            var parentCompaniesUsers = _dbContext.GCParentCompanyUsers.Where(x => x.UserId == userId).ToList();

            //Companies for delete
            var companiesIdsForDelete = parentCompaniesUsers.Select(x => x.GCParentCompanyId).Except(parentCompaniesIds).ToList();
            var companiesUsersForDelete = _dbContext.GCParentCompanyUsers.Where(x => companiesIdsForDelete.Contains(x.GCParentCompanyId) && x.UserId == userId).AsEnumerable();
            _dbContext.GCParentCompanyUsers.RemoveRange(companiesUsersForDelete);

            //Companies for add
            var companiesIdsForAdd = parentCompaniesIds.Except(parentCompaniesUsers.Select(x => x.GCParentCompanyId));
            var companiesUsersForAdd = companiesIdsForAdd.Select(x => new GCParentCompanyUsers { UserId = userId, GCParentCompanyId = x }).ToList();
            _dbContext.GCParentCompanyUsers.AddRange(companiesUsersForAdd);

            _dbContext.SaveChanges();
        }

        public void EditParentCompany(ParentCompanyModel company)
        {
            var parentCompany = _dbContext.GCParentCompany.Find(company.Id);

            parentCompany.Title = company.Title;

            _dbContext.GCParentCompany.Update(parentCompany);
            _dbContext.SaveChanges();
        }

        public void AddParentCompany(ParentCompanyModel company)
        {
            GCParentCompany parentCompany = new GCParentCompany();

            parentCompany.Title = company.Title;

            _dbContext.GCParentCompany.Add(parentCompany);
            _dbContext.SaveChanges();
        }

        public void DeleteParentCompany(int companyId)
        {
            var parentWithChildren = GetParentCompanyWithChilder(companyId);

            var parentCompanyUsers = _dbContext.GCParentCompanyUsers.Where(x => x.GCParentCompanyId == companyId).ToList();
            _dbContext.GCParentCompanyUsers.RemoveRange(parentCompanyUsers);

            foreach (var child in parentWithChildren.ChildCompanies)
            {
                var childCompnay = _dbContext.GCChildCompany.Find(child.Id);
                _dbContext.GCChildCompany.Remove(childCompnay);
            }

            var companny = _dbContext.GCParentCompany.Find(companyId);
            _dbContext.GCParentCompany.Remove(companny);
            _dbContext.SaveChanges();
        }

        public void EditChildCompany(ChildCompanyModel company)
        {
            var childCompany = _dbContext.GCChildCompany.Find(company.Id);

            childCompany.Title = company.Title;
            childCompany.GCParentCompanyId = company.ParentCompanyId;

            _dbContext.GCChildCompany.Update(childCompany);
            _dbContext.SaveChanges();
        }

        public void AddChildCompany(ChildCompanyModel company)
        {
            GCChildCompany childCompany = new GCChildCompany();

            childCompany.Title = company.Title;
            childCompany.GCParentCompanyId = company.ParentCompanyId;

            if (company.SelectedVendor != null)
            {
                childCompany.GCVendorId = Int32.Parse(company.SelectedVendor);
            }
            
            if (company.SelectedCustomer != null)
            {
                childCompany.GCCustomerId = Int32.Parse(company.SelectedCustomer);
            }
           

            _dbContext.GCChildCompany.Add(childCompany);
            _dbContext.SaveChanges();
        }

        public void DeleteChildCompany(int companyId)
        {
            var companny = _dbContext.GCChildCompany.Find(companyId);

            _dbContext.GCChildCompany.Remove(companny);
            _dbContext.SaveChanges();
        }

        public IEnumerable<IdentityUserClaim<int>> GetUserClaims(int userId, string startsWith = null)
        {
            var query = _dbContext.UserClaims.Where(x => x.UserId == userId);

            if(startsWith != null)
            {
                query = query.Where(x => x.ClaimType.StartsWith(startsWith));
            }

            return query.ToList();
        }

        public void RemoveClaim(string claimType, int? userId = null)
        {
            var query = _dbContext.UserClaims.Where(x => x.ClaimType == claimType);

            if(userId.HasValue)
            {
                query = query.Where(x => x.UserId == userId.Value);
            }

            var claimsToRemove = query.ToList();

            if(claimsToRemove != null && claimsToRemove.Count > 0)
            {
                _dbContext.UserClaims.RemoveRange(claimsToRemove);
                _dbContext.SaveChanges();
            }
        }
    }
}