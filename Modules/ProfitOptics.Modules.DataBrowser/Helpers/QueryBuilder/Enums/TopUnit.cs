﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
namespace ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Enums
{
    public enum TopUnit
    {
        Records,
        Percent
    }
}