﻿using Newtonsoft.Json;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using System;
using System.Reflection;

namespace ProfitOptics.Framework.Web.Framework.Modules
{
    public class ModuleDescriptor : IDescriptor, IComparable<ModuleDescriptor>
    {
        public ModuleDescriptor()
        {
        }

        public ModuleDescriptor(Assembly referencedAssembly) : this()
        {
            this.ReferencedAssembly = referencedAssembly;
        }

        public IModule Instance()
        {
            return Instance<IModule>();
        }

        public virtual T Instance<T>() where T : class, IModule
        {
            object instance = null;
            try
            {
                instance = EngineContext.Current.Resolve(ModuleType);
            }
            catch
            {
            }

            if (instance == null)
            {
                instance = EngineContext.Current.ResolveUnregistered(ModuleType);
            }

            var typedInstance = instance as T;
            if (typedInstance != null)
                typedInstance.ModuleDescriptor = this;

            return typedInstance;
        }

        public int CompareTo(ModuleDescriptor other)
        {
            return FriendlyName.CompareTo(other.FriendlyName);
        }

        [JsonProperty(PropertyName = "FriendlyName")]
        public virtual string FriendlyName { get; set; }

        [JsonProperty(PropertyName = "SystemName")]
        public virtual string SystemName { get; set; }

        [JsonProperty(PropertyName = "Version")]
        public virtual string Version { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public virtual string Description { get; set; }

        [JsonProperty(PropertyName = "FileName")]
        public virtual string AssemblyFileName { get; set; }

        [JsonIgnore]
        public virtual Type ModuleType { get; set; }

        [JsonIgnore]
        public virtual bool Installed { get; set; }

        [JsonIgnore]
        public virtual string OriginalAssemblyFile { get; internal set; }

        [JsonIgnore]
        public virtual Assembly ReferencedAssembly { get; internal set; }
    }
}
