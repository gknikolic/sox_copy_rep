﻿namespace ProfitOptics.Framework.Web.Framework.Identity
{
    public class ExternalUserDetails
    {
        public string Key { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageUrl { get; set; }
    }

    public class ExternalLoginDetails
    {
        public ExternalUserDetails User { get; set; }
        public string Provider { get; set; }
        public string ReturnUrl { get; set; }
    }
}