﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class GPSDeviceCurrentInfos
	{
        [JsonProperty("currentInfos")]
        public Dictionary<string,GPSDeviceSample> CurrentInfos{ get; set; }
	}
}
