﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProfitOptics.Framework.Core.Helper
{
    public static class Render
    {
        //Put in BaseController
        public static string RenderPartialViewToString(Controller controller, ICompositeViewEngine viewEngine, string viewName, object model)
        {
            viewName = viewName ?? controller.ControllerContext.ActionDescriptor.ActionName;
            controller.ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                IView view = viewEngine.FindView(controller.ControllerContext, viewName, false).View;
                ViewContext viewContext = new ViewContext(controller.ControllerContext, view, controller.ViewData, controller.TempData, sw, new HtmlHelperOptions());
                view.RenderAsync(viewContext).Wait();
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
