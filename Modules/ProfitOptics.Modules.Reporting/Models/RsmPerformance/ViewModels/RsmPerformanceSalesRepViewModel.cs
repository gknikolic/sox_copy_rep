﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.RegionTable;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels
{
    public class RsmPerformanceSalesRepViewModel : RsmPerformanceSalesRepModel
    {
        public List<SalesChannelQuotaPerformanceDataModel> SalesChannelPerformance { get; set; }
        public List<ProductFamilyQuotaPerformanceDataModel> ProductFamilyPerformance { get; set; }
        public List<ItemCategoryQuotaPerformanceDataModel> ItemCategoryPerformance { get; set; }
        public RegionTablePartialViewModel RegionTableViewModel { get; set; }

        public RsmPerformanceSalesRepViewModel()
        {
            SalesChannelPerformance = new List<SalesChannelQuotaPerformanceDataModel>();
            ProductFamilyPerformance = new List<ProductFamilyQuotaPerformanceDataModel>();
            ItemCategoryPerformance = new List<ItemCategoryQuotaPerformanceDataModel>();
        }
    }
}