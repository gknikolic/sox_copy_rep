﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Identity.Domain
{
    public class GCChildCompany
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? GCParentCompanyId { get; set; }
        public GCParentCompany GCParentCompany { get; set; }
        public int? GCVendorId { get; set; }
        public int? GCCustomerId { get; set; }
        public GCVendor Vendor { get; set; }
        public GCCustomer GCCustomer { get; set; }
        public virtual ICollection<GCChildCompanyUsers> GCChildCompanyUsers { get; set; }
    }
}
