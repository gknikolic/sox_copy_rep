﻿using AutoMapper;
using Flurl;
using Flurl.Http;
using Intuit.Ipp.OAuth2PlatformClient;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Helpers;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Models.CensiTrack.ETL;
using ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.Interfaces;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using ProfitOptics.Modules.Sox.Models.QuickBooks;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Services
{
    public class GroundControlService : IGroundControlService
    {
        private readonly Framework.DataLayer.Entities _context;
        private readonly ICaseManagementClient _caseManagementClient;
        private readonly IQuickBooksClient _quickBooksClient;
        private readonly IMapper _mapper;
        public IWorkWaveClient WorkWaveClient { get; set; }

        public GroundControlService(Framework.DataLayer.Entities context, ICaseManagementClient caseManagementClient, IQuickBooksClient quickBooksClient, IWorkWaveClient WorkWaveClient,
                                    IMapper mapper)
        {
            _context = context;
            _caseManagementClient = caseManagementClient;
            _quickBooksClient = quickBooksClient;
            this.WorkWaveClient = WorkWaveClient;
            _mapper = mapper;
        }

        public void AddOrEditUserClaims(int userId, Dictionary<string, string> claimDictionary)
        {
            foreach (var claim in claimDictionary)
            {
                var userClaim = _context.AspNetUserClaims.Where(x => x.UserId == userId && x.ClaimType == claim.Key).FirstOrDefault();
                if (userClaim == null)
                {
                    userClaim = new AspNetUserClaims()
                    {
                        UserId = userId,
                        ClaimType = claim.Key
                    };

                }
                userClaim.ClaimValue = claim.Value;

                if (userClaim.Id == 0)
                {
                    _context.AspNetUserClaims.Add(userClaim);
                }
            }
            _context.SaveChanges();
        }

        public async Task UpdateCaseBarcodesAsync(int caseId)
        {
            List<string> orderIds = new List<string>();
            AddOrdersModel model = new AddOrdersModel() { Orders = new List<Order>() };
            var gcCase = _context.GCCases.Find(caseId);
            if (gcCase != null)
            {
                if (gcCase.IncomingOrderId != null) //if we have incoming order, add it to list for update
                {
                    orderIds.Add(gcCase.IncomingOrderId);
                }
                if (gcCase.OutgoingOrderId != null)
                {
                    orderIds.Add(gcCase.OutgoingOrderId);
                }
                if (orderIds.Count > 0)
                {
                    var ordersList = await WorkWaveClient.GetOrdersByIdFromWWAsync(orderIds); //get orders from WW (it is faster to get order from ww and just change barcodes filed and return in that to create entire new model)

                    if (ordersList != null && ordersList.Orders != null && ordersList.Orders.Count > 0)
                    {
                        List<string> barcodesList = new List<string>();
                        var trays = _context.GCTrays.Where(x => x.GCCaseId == caseId).ToList();
                        if (trays != null && trays.Count > 0) //if we have trays for that case, then we have barcodes
                        {
                            foreach (var tray in trays)
                            {
                                barcodesList.Add(tray.Barcode);
                            }
                        }

                        foreach (var order in ordersList.Orders) //replace barcodes in model
                        {
                            var currentOrder = order.Value;
                            if (currentOrder.Delivery != null)
                            {
                                currentOrder.Delivery.Barcodes = barcodesList;
                            }
                            if (currentOrder.Pickup != null)
                            {
                                currentOrder.Pickup.Barcodes = barcodesList;
                            }
                            model.Orders.Add(currentOrder);
                        }
                        await WorkWaveClient.ReplaceOrders(model); //replace barcodes in WW
                    }
                }
            }
        }

        #region POSettings

        public POSetting GetPOSetting(POSettingsType type)
        {
            return _context.POSettings.Where(x => x.SettingKey == type.ToString()).FirstOrDefault();
        }

        public void AddOrUpdatePOSetting(POSettingsType type, string value, string user)
        {
            var setting = _context.POSettings.Where(x => x.SettingKey == type.ToString()).FirstOrDefault();
            var isNew = false;
            if (setting == null)
            {
                isNew = true;
                setting = new POSetting()
                {
                    SettingKey = type.ToString()
                };

            }
            setting.Value = value;
            setting.LastChangedTime = DateTime.UtcNow;
            setting.LastChangedBy = user;

            if (isNew)
            {
                _context.POSettings.Add(setting);
            }
            _context.SaveChanges();
        }

        public bool RemovePOSetting(POSettingsType type)
        {
            bool result = true;

            var setting = _context.POSettings.Where(x => x.SettingKey == type.ToString()).FirstOrDefault();
            if (setting != null)
            {
                try
                {
                    _context.POSettings.Remove(setting);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    result = false;
                }
            }

            return result;
        }

        #endregion POSettings

        #region QuickBooks

        public string GetQuickBooksAccessToken(int userId, List<AspNetUserClaims> claims, string userName = "")
        {
            string token = null;

            TokenResponse tokenResponse = null;
            var tokenUpdated = false;
            POSetting refreshTokenPOSetting = null;
            POSetting refreshTokenExpiresAtPOSetting = null;

            if (claims != null)
            {
                var access_token = claims.Where(x => x.ClaimType == Enums.QBTokenEnum.quickbooks_access_token.ToString()).FirstOrDefault();
                var access_token_expires_at = claims.Where(x => x.ClaimType == Enums.QBTokenEnum.quickbooks_access_token_expires_at.ToString()).FirstOrDefault();

                if (access_token != null && access_token_expires_at != null)
                {
                    DateTime expiresAt = DateTime.MinValue;

                    if (DateTime.TryParse(access_token_expires_at.ClaimValue, out expiresAt) && expiresAt > DateTime.MinValue)
                    {
                        if (expiresAt > DateTime.UtcNow)
                        {
                            token = access_token.ClaimValue;
                        }
                    }
                }

                if (token == null)
                {
                    string refresh_token = null;
                    refreshTokenPOSetting = GetPOSetting(Enums.POSettingsType.QuickBooksRefreshToken);
                    if (refreshTokenPOSetting != null)
                    {
                        refresh_token = refreshTokenPOSetting.Value;
                    }
                    refreshTokenExpiresAtPOSetting = GetPOSetting(Enums.POSettingsType.QuickBooksRefreshTokenExpiresAt);

                    if (refresh_token != null && refreshTokenExpiresAtPOSetting != null)
                    {
                        DateTime expiresAt = DateTime.MinValue;
                        if (DateTime.TryParse(refreshTokenExpiresAtPOSetting.Value, out expiresAt) && expiresAt > DateTime.MinValue)
                        {
                            if (expiresAt > DateTime.UtcNow)
                            {
                                tokenResponse = RefreshQBAuthorizeToken(refresh_token).Result;
                                tokenUpdated = true;
                            }
                        }
                    }
                }
            }

            if (tokenUpdated && tokenResponse != null)
            {
                var dict = new Dictionary<string, string>();
                dict.Add(Enums.QBTokenEnum.quickbooks_access_token.ToString(), tokenResponse.AccessToken ?? string.Empty);
                dict.Add(Enums.QBTokenEnum.quickbooks_access_token_expires_at.ToString(), DateTime.UtcNow.AddSeconds(tokenResponse.AccessTokenExpiresIn).ToString());
                AddOrEditUserClaims(userId, dict);
                token = tokenResponse.AccessToken;

                if (refreshTokenPOSetting != null &&
                    refreshTokenExpiresAtPOSetting != null &&
                    !refreshTokenPOSetting.Value.Equals(tokenResponse.RefreshToken))
                {
                    AddOrUpdatePOSetting(Enums.POSettingsType.QuickBooksRefreshToken, tokenResponse.RefreshToken, userName);
                    AddOrUpdatePOSetting(Enums.POSettingsType.QuickBooksRefreshTokenExpiresAt, DateTime.UtcNow.AddSeconds(tokenResponse.RefreshTokenExpiresIn).ToString(), userName);
                }
            }

            return token;
        }

        public async Task<TokenResponse> RefreshQBAuthorizeToken(string refreshToken)
        {
            return await _quickBooksClient.RefreshAuthorizeToken(refreshToken);
        }

        public async Task<TokenRevocationResponse> RevokeQBAccessTokenAsync(string token)
        {
            return await _quickBooksClient.RevokeAccessToken(token);
        }

        public QBUpdateTokenResponse UpdateQBToken(string code)
        {
            return _quickBooksClient.UpdateToken(code);
        }

        public string GetQBAuthorizeUrl(string scope = null)
        {
            if (scope == null)
            {
                scope = OidcScopes.Accounting.GetStringValue() + " " + OidcScopes.Payment.GetStringValue();
            }

            var url = _quickBooksClient.GetAuthorizeUrl(scope);

            return url;
        }

        public Customer GetQBCustomer(int id, string token)
        {
            Customer customer = null;

            try
            {
                customer = _quickBooksClient.GetCustomer(id, token);
            }
            catch (FlurlHttpException e)
            {

            }
            catch (Exception e)
            {

            }

            return customer;
        }

        public Vendor GetQBVendor(int id, string token)
        {
            Vendor vendor = null;

            try
            {
                vendor = _quickBooksClient.GetVendor(id, token);
            }
            catch (FlurlHttpException e)
            {

            }
            catch (Exception e)
            {

            }

            return vendor;
        }

        public List<Customer> GetQBCustomerList(string token)
        {
            List<Customer> customers = null;

            try
            {
                customers = _quickBooksClient.GetCustomerList(token);
            }
            catch (FlurlHttpException e)
            {

            }
            catch (Exception e)
            {

            }

            return customers;
        }

        public EditCustomerResponse EditQBCustomer(Customer customer, string token)
        {
            return _quickBooksClient.EditCustomer(customer, token);
        }

        public EditCustomerResponse SetQBCustomerActiveStatus(int customerId, bool isActive, string token, string syncToken = null)
        {
            return _quickBooksClient.SetCustomerActiveStatus(customerId, isActive, token, syncToken);
        }

        public EditVendorResponse EditQBVendor(Vendor qbVendor, string token)
        {
            return _quickBooksClient.EditVendor(qbVendor, token);
        }

        public ProfitOptics.Framework.Core.Settings.QuickBooksOptions GetQuickBooksOptions()
        {
            return _quickBooksClient.GetOptions();
        }

        public Customer MapGcToQbCustomer(GCCustomer customer)
        {
            var qbCustomer = new Models.QuickBooks.Customer()
            {
                DisplayName = customer.CustomerName,
                Notes = customer.DeliverNotes,
                BillAddr = new Models.QuickBooks.Address()
                {
                    Line1 = customer.DeliveryStreet,
                    City = customer.DeliveryCity,
                    Country = "USA",
                    CountrySubDivisionCode = customer.DeliveryState,
                    PostalCode = customer.DeliveryZip
                },
                PrimaryPhone = new Models.QuickBooks.Phone()
                {
                    FreeFormNumber = customer.APPhone
                },
                AlternatePhone = new Models.QuickBooks.Phone()
                {
                    FreeFormNumber = customer.DeliverPhoneNumber
                }
            };
            return qbCustomer;
        }

        public bool IsQuickBooksAuthorized()
        {
            var isAuthorized = false;

            var qbRefreshTokenSetting = GetPOSetting(POSettingsType.QuickBooksRefreshToken);
            var qbRefreshTokenExpiresAtSetting = GetPOSetting(POSettingsType.QuickBooksRefreshTokenExpiresAt);
            DateTime refreshTokenExpiresAt = DateTime.MinValue;

            if (qbRefreshTokenSetting != null &&
                qbRefreshTokenExpiresAtSetting != null &&
                DateTime.TryParse(qbRefreshTokenExpiresAtSetting.Value, out refreshTokenExpiresAt) &&
                refreshTokenExpiresAt > DateTime.UtcNow)
            {
                isAuthorized = true;
            }

            return isAuthorized;
        }

        public void CreateQBEstimate(GCCase gcCase, string token)
        {
            string description = string.Empty;
            var result = _quickBooksClient.CreateEstimate(gcCase, token, out description);
            if(result != null)
            {
                var estimateLines = new List<GCEstimateLine>();

                if(result.EstimateLines != null && result.EstimateLines.Count > 0)
                {
                    result.EstimateLines.ForEach(x =>
                    {
                        estimateLines.Add(new GCEstimateLine()
                        {
                            LineNum = x.LineNum,
                            Description = x.Description,
                            Amount = x.Amount,
                            DetailType = x.DetailType,
                            SalesItemValue = (x.SalesItemLineDetail != null && x.SalesItemLineDetail.ItemRef != null) ? x.SalesItemLineDetail.ItemRef.Value : null,
                            SalesItemName = (x.SalesItemLineDetail != null && x.SalesItemLineDetail.ItemRef != null) ? x.SalesItemLineDetail.ItemRef.Name : null,
                            SalesItemUnitPrice = (x.SalesItemLineDetail != null) ? x.SalesItemLineDetail.UnitPrice : 0,
                            SalesItemQty = (x.SalesItemLineDetail != null) ? x.SalesItemLineDetail.Qty : 0,
                            SalesItemTaxCodeValue = (x.SalesItemLineDetail != null && x.SalesItemLineDetail.TaxCodeRef != null) ? x.SalesItemLineDetail.TaxCodeRef.Value : null,
                            DiscountPercentBased = x.DiscountLineDetail != null && x.DiscountLineDetail.PercentBased,
                            DiscountAccountName = x.DiscountLineDetail != null && x.DiscountLineDetail.DiscountAccountRef != null ? x.DiscountLineDetail.DiscountAccountRef.Name : null,
                            DiscountAccountValue = x.DiscountLineDetail != null && x.DiscountLineDetail.DiscountAccountRef != null ? x.DiscountLineDetail.DiscountAccountRef.Value : null,
                            DiscountPercent = x.DiscountLineDetail != null ? x.DiscountLineDetail.DiscountPercent : 0,
                            CreatedAtUtc = DateTime.UtcNow
                        });
                    });
                }

                var gcEstimate = new GCEstimate()
                {
                    Domain = result.Domain,
                    Sparse = result.Sparse,
                    SyncToken = result.SyncToken,
                    MetaDataCreateTime = result.MetaData != null ? result.MetaData.CreateTime : DateTime.MinValue,
                    MetaDataLastUpdatedTime = result.MetaData != null ? result.MetaData.LastUpdatedTime : DateTime.MinValue,
                    TxnDate = result.TxnDate,
                    CurrencyValue = result.CurrencyRef != null ? result.CurrencyRef.Value : null,
                    CurrencyName = result.CurrencyRef != null ? result.CurrencyRef.Name : null,
                    TxnTaxDetailTotalTax = result.TxnTaxDetail != null ? result.TxnTaxDetail.TotalTax : 0,
                    CustomerValue = result.CustomerRef != null ? result.CustomerRef.Value : null,
                    CustomerName = result.CustomerRef != null ? result.CustomerRef.Name : null,
                    CustomerMemo = result.CustomerMemo != null ? result.CustomerMemo.Value : null,
                    TotalAmt = result.TotalAmt,
                    ApplyTaxAfterDiscount = result.ApplyTaxAfterDiscount,
                    PrintStatus = result.PrintStatus,
                    EmailStatus = result.EmailStatus,
                    CreatedAtUtc = DateTime.UtcNow,
                    GCCase = gcCase,
                    GCEstimateLines = estimateLines,
                };
                _context.GCEstimates.Add(gcEstimate);
            }
            _context.SaveChanges();
        }

        public async Task PushTrayOrCaseInfoToQuickBooks(int gcCustomerId, string yearAndMonth, string token)
        {
            GCInvoice gcInvoice = null;
            int invoiceMonth = DateTime.UtcNow.Month;
            int invoiceYear = DateTime.UtcNow.Year;
            var invoiceDate = new DateTime(invoiceYear, invoiceMonth, 1);
            if(!string.IsNullOrWhiteSpace(yearAndMonth))
            {
                var dateChunks = yearAndMonth.Split("-").ToList();
                if(dateChunks != null && dateChunks.Count == 2)
                {
                    int year = int.Parse(dateChunks[0]);
                    int month = int.Parse(dateChunks[1]);
                    invoiceDate = new DateTime(year, month, 1);
                    invoiceMonth = month;
                    invoiceYear = year;
                }
            }
            var invoiceDateString = invoiceDate.ToString("yyyy-MM-dd");
            var gcInvoices = await _context.GCInvoices.Include(x => x.GCInvoiceLines).Where(x => x.GCCustomerId == gcCustomerId && x.TxnDate == invoiceDate).ToListAsync();
            var shouldSubmitToQB = false;
            bool isTraysPush = true;

            if (gcInvoices != null && gcInvoices.Count > 0)
            {
                gcInvoice = gcInvoices.First();
                gcInvoice.ModifiedAtUtc = DateTime.UtcNow;
            }
            else
            {
                var gcCustomer = await _context.GCCustomers.FindAsync(gcCustomerId);
                if (gcCustomer != null)
                {
                    if(!gcCustomer.QBCustomerId.HasValue)
                    {
                        Models.QuickBooks.Customer qbCustomer = MapGcToQbCustomer(gcCustomer);
                        var qbCustomerResponse = _quickBooksClient.EditCustomer(qbCustomer, token);

                        if (qbCustomerResponse.Fault != null && qbCustomerResponse.Fault.Errors.Count > 0)
                        {
                            var success = false;
                            var message = qbCustomerResponse.Fault.Errors[0].Detail;
                        }
                        else if (qbCustomerResponse.Customer != null)
                        {
                            gcCustomer.QBCustomerId = qbCustomerResponse.Customer.Id;
                            _context.SaveChanges();
                        }
                    }

                    if(gcCustomer.QBBillingType.HasValue)
                    {
                        isTraysPush = gcCustomer.QBBillingType.Value == (int)QBBillingType.Tray;
                    }

                    gcInvoice = new GCInvoice()
                    {
                        GCCustomerId = gcCustomerId,
                        QBInvoiceId = null,
                        AllowIpnPayment = false,
                        AllowOnlinePayment = false,
                        AllowOnlineCreditCardPayment = false,
                        AllowOnlineAchPayment = false,
                        Domain = "QBO",
                        Sparse = false,
                        SyncToken = null,
                        MetaDataCreateTime = null,
                        MetaDataLastUpdatedTime = null,
                        DocNumber = $"{gcCustomer.QBCustomerId}-{invoiceDateString}-DRAFT",
                        TxnDate = invoiceDate,
                        CurrencyValue = Helpers.Constants.QBO_USD_SHORT,
                        CurrencyName = Helpers.Constants.QBO_USD_LONG,
                        QBCustomerId = gcCustomer.QBCustomerId.ToString() ?? string.Empty,
                        QBCustomerName = gcCustomer.CustomerName,
                        CustomerMemo = null,
                        DueDate = invoiceDate.AddMonths(1),
                        TotalAmt = 0,
                        ApplyTaxAfterDiscount = false,
                        PrintStatus = null,
                        EmailStatus = null,
                        Balance = 0,
                        CreatedAtUtc = DateTime.UtcNow
                    }; 
                }
            }

            if(gcInvoice.GCInvoiceLines == null)
            {
                gcInvoice.GCInvoiceLines = new List<GCInvoiceLine>();
            }

            var currentMonthCases = _context.GCCases
                .Include(x => x.GCTrays)
                .ThenInclude(x => x.CTContainerType)
                .Include(x => x.GCTrays)
                .ThenInclude(x => x.GCTraysLifecycleStatusTypes)
                .Where(x =>
                    x.GCInvoiceId == null &&
                    x.DueTimestamp.Month == invoiceMonth &&
                    x.DueTimestamp.Year == invoiceYear &&
                    x.GCCustomerId == gcCustomerId &&
                    x.GCTrays.Any(y => y.GCTraysLifecycleStatusTypes.Any(z => z.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Delivered)))
                .OrderBy(x => x.DueTimestamp).ToList();
            if (currentMonthCases != null && currentMonthCases.Count > 0)
            {
                int lineNum = (gcInvoice.GCInvoiceLines.Count > 0) ? gcInvoice.GCInvoiceLines.Max(x => x.LineNum) + 1 : 1;
                var salesItemTaxCodeValue = "TAX";
                int salesItemQty = 1;

                foreach (var gcCase in currentMonthCases)
                {
                    if (gcCase.GCTrays.Count > 0)
                    {
                        var firstTray = gcCase.GCTrays.FirstOrDefault();
                        if (firstTray != null && firstTray.CTContainerType != null)
                        {
                            var firstSku = firstTray.CTContainerType.ProcurementReferenceId;
                            if (!string.IsNullOrWhiteSpace(firstSku))
                            {
                                var skuChunks = firstSku.Split("-").ToList();
                                if (skuChunks != null && skuChunks.Count > 0)
                                {
                                    var procTypeAbb = skuChunks[0];
                                    var gcProcType = _context.GCProcedureTypes.Where(x => x.Abbreviation.Equals(procTypeAbb)).FirstOrDefault();

                                    if (gcProcType != null)
                                    {
                                        int trayCap = gcProcType.TrayCount;
                                        // ProcedureType 'BASE' is gcProcType.TrayCount, it can occur only once per case, if there are trays over the cap, add them as individual lines
                                        int qbLineItemCount =
                                                trayCap >= gcCase.GCTrays.Count ?
                                                    1 : 2; // (gcCase.GCTrays.Count - trayCap + 1);


                                        for (int i = 0; i < qbLineItemCount; i++)
                                        {
                                            var gcTray = gcCase.GCTrays.ToList()[i];
                                            var ctSKU = gcTray.CTContainerType.ProcurementReferenceId;
                                            //var qbSKU = $"{ctSKU}-{Helpers.Constants.QBO_SKU_SUFFIX_TRAY}";
                                            string qbSKU = $"{gcProcType.Abbreviation}-"; // to be filled based on PROC or TRAY

                                            string trayType = i == 0 ? "OEM-Tray (Base)" : "Tray-Above-Cap"; // OEM Tray (Base) or Tray Above Cap
                                            decimal unitPrice = gcProcType.Abbreviation.Equals(Helpers.Constants.QBO_PROVIDER_ABBREVIATION) ?
                                                (gcProcType.ProcedurePriceCustomer) : (gcProcType.TrayPriceAboveCap ?? 0);

                                            var icd = !string.IsNullOrWhiteSpace(gcProcType.ICD10) ?
                                                ($"ICD10: {gcProcType.ICD10}, ") :
                                                (!string.IsNullOrWhiteSpace(gcProcType.ICD9) ? $"ICD9: {gcProcType.ICD9}, " : string.Empty);

                                            if (i == 0) // ProcedureType 'BASE'
                                            {
                                                unitPrice = gcProcType.ProcedurePriceOEM;
                                                //qbSKU = $"{ctSKU}-{Helpers.Constants.QBO_SKU_SUFFIX_PROC}";
                                                salesItemQty = 1;
                                                qbSKU += Helpers.Constants.QBO_SKU_SUFFIX_PROC;
                                            }
                                            else // ProcedureType 'TRAY'
                                            {
                                                qbSKU += Helpers.Constants.QBO_SKU_SUFFIX_TRAY;
                                                salesItemQty = trayCap >= gcCase.GCTrays.Count ? 1 : (gcCase.GCTrays.Count - trayCap); // QTY of trays after the cap
                                            }

                                            //var qbItem = _quickBooksClient.GetItem(qbSKU, token);
                                            var qbItem = _quickBooksClient.GetItem(qbSKU, token);

                                            if (qbItem == null)
                                            {
                                                var error = $"No QBItem (service) found for SKU: {ctSKU}";
                                                Log.Error(error);
                                                throw new Exception(error);
                                            }
                                            else if(qbItem.UnitPrice != unitPrice)
                                            {
                                                qbItem.UnitPrice = unitPrice;
                                                EditItemResponse editItemResponse = _quickBooksClient.EditItem(qbItem, token);

                                                if (editItemResponse.Fault != null && editItemResponse.Fault.Errors.Count > 0)
                                                {
                                                    var success = false;
                                                    var message = editItemResponse.Fault.Errors[0].Detail;

                                                    Log.Error(message);
                                                    throw new Exception(message);
                                                }
                                            }

                                            gcInvoice.GCInvoiceLines.Add(new GCInvoiceLine()
                                            {
                                                Type = (int)GCInvoiceLineType.Tray,
                                                GCCaseId = gcCase.Id,
                                                LineNum = lineNum,
                                                SKU = ctSKU,
                                                Description = $"{trayType}, \nCase: {gcCase.Title}, {icd}\nSurgery date: {gcCase.DueTimestamp}",
                                                Amount = (salesItemQty * unitPrice),
                                                DetailType = Helpers.Constants.QBO_SALES_ITEM_LINE_DETAIL,
                                                SalesItemServiceDate = gcCase.DueTimestamp,
                                                SalesItemValue = qbItem.Id.ToString(),
                                                SalesItemName = qbItem.Name,
                                                ItemDisplayName = qbSKU,
                                                SalesItemUnitPrice = qbItem.UnitPrice,
                                                SalesItemQty = salesItemQty,
                                                SalesItemTaxCodeValue = salesItemTaxCodeValue,
                                                CreatedAtUtc = DateTime.UtcNow
                                            });
                                            lineNum++;
                                            shouldSubmitToQB = true;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    gcCase.GCInvoice = gcInvoice;
                }

                var totalAmount = gcInvoice.GCInvoiceLines.Sum(x => x.Amount);
                gcInvoice.TotalAmt = totalAmount;

                if (gcInvoice.Id == 0)
                {
                    await _context.GCInvoices.AddAsync(gcInvoice);

                }

                await _context.SaveChangesAsync();
            }
        }

        public async Task PushInvoicesToQB(string token, GCInvoice gcInvoice)
        {
            Invoice invoiceResult = null;

            Invoice invoice = new Invoice()
            {
                Id = gcInvoice.QBInvoiceId,
                SyncToken = gcInvoice.SyncToken,
                MetaData = null,
                DocNumber = gcInvoice.DocNumber,
                TxnDate = gcInvoice.TxnDate,
                CurrencyRef = new GeneralRef()
                {
                    Name = gcInvoice.CurrencyName,
                    Value = gcInvoice.CurrencyValue
                },
                InvoiceLines = gcInvoice.GCInvoiceLines == null ? null : gcInvoice.GCInvoiceLines.Select(gcInvoiceLine => new InvoiceLine()
                {
                    Id = gcInvoiceLine.QBInvoiceLineId,
                    LineNum = gcInvoiceLine.LineNum,
                    Description = gcInvoiceLine.Description,
                    Amount = gcInvoiceLine.Amount,
                    DetailType = gcInvoiceLine.DetailType,
                    SalesItemLineDetail = new EstimateLineDetail()
                    {
                        ItemRef = new GeneralRef()
                        {
                            Name = gcInvoiceLine.SalesItemName,
                            Value = gcInvoiceLine.SalesItemValue
                        },
                        Qty = gcInvoiceLine.SalesItemQty,
                        TaxCodeRef = new GeneralValue()
                        {
                            Value = gcInvoiceLine.SalesItemTaxCodeValue
                        },
                        UnitPrice = gcInvoiceLine.SalesItemUnitPrice,
                        ServiceDate = gcInvoiceLine.SalesItemServiceDate
                    },
                    SubTotalLineDetail = new object()
                }).ToList(),
                //TxnTaxDetail = 
                CustomerRef = new GeneralRef()
                {
                    Name = gcInvoice.QBCustomerName,
                    Value = gcInvoice.QBCustomerId
                },
                CustomerMemo = new GeneralValue()
                {
                    Value = gcInvoice.CustomerMemo
                },
                //BillAddr = 
                //SalesTermRef = 
                DueDate = gcInvoice.DueDate,
                TotalAmt = gcInvoice.TotalAmt,
                ApplyTaxAfterDiscount = gcInvoice.ApplyTaxAfterDiscount,
                PrintStatus = gcInvoice.PrintStatus,
                EmailStatus = gcInvoice.EmailStatus,
                Balance = gcInvoice.Balance
            };

            if (gcInvoice.QBInvoiceId.HasValue && gcInvoice.QBInvoiceId.Value > 0)
            {
                var txnDateString = gcInvoice.TxnDate.ToString("yyyy-MM-dd");

                var qbInvoice = _quickBooksClient.GetInvoice(int.Parse(gcInvoice.QBCustomerId), txnDateString, token);
                if(qbInvoice != null)
                {
                    invoice.SyncToken = qbInvoice.SyncToken;
                    invoiceResult = _quickBooksClient.EditInvoice(invoice, token).Invoice;
                }
                else
                {
                    var error = $"No QB invoice found for QBCustomerId: {gcInvoice.QBCustomerId}, and TransactionDate: {txnDateString}";
                    Log.Error(error);
                    throw new Exception(error);
                }
            }
            else
            {
                var result = _quickBooksClient.CreateInvoice(invoice, token);
                invoiceResult = result.Invoice;

                if(invoiceResult == null && result.Fault != null && result.Fault.Errors != null && result.Fault.Errors.Count > 0)
                {
                    var message = result.Fault.Errors[0].Detail;
                    Log.Error(message);
                    throw new Exception(message);
                }
            }

            if (invoiceResult != null)
            {
                gcInvoice.QBInvoiceId = invoiceResult.Id;
                if(invoiceResult.InvoiceLines != null && invoiceResult.InvoiceLines.Count > 0)
                {
                    foreach (var il in invoiceResult.InvoiceLines.Where(il => il.DetailType.Equals(Helpers.Constants.QBO_SALES_ITEM_LINE_DETAIL)).ToList())
                    {
                        var gcIl = gcInvoice.GCInvoiceLines.Where(x => x.SalesItemValue == il.SalesItemLineDetail.ItemRef.Value && x.LineNum == il.LineNum).FirstOrDefault();
                        if(gcIl != null)
                        {
                            gcIl.QBInvoiceLineId = il.Id;
                        }
                    }
                }
                await _context.SaveChangesAsync();
            }
        }

        private string GetCaseSkuFromTray(string sku)
        {
            var resultSku = string.Empty;

            var skuChunks = sku.Split('-').ToList();

            if(skuChunks.Count > 3 && skuChunks[3].Equals("T"))
            {
                skuChunks[3] = "C";
                resultSku = string.Join('-', skuChunks);
            }

            return resultSku;
        }

        #endregion QuickBooks

        #region WorkWave

        public async Task DeleteOrdersInWWAsync(List<string> orderIds)
        {
            await WorkWaveClient.DeleteOrdersInWW(orderIds);
        }

        #endregion

        #region GroundControl

        private void CreateFutureLCBasedOnActualHistory(GCTraysLifecycleStatusTypes trayLC)
        {
            if(trayLC.GCTray != null && trayLC.GCTray.CTContainerTypeActualId.HasValue && trayLC.GCTray.CTContainerTypeActualId.Value > 0)
            {
                var nextHistoryItem = _context.CTContainerTypeActualHistoryItems.Where(x =>
                    x.CTContainerTypeActualId == trayLC.GCTray.CTContainerTypeActualId.Value &&
                    (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG ||
                        x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER) &&
                    x.UpdateTimestamp > trayLC.Timestamp)
                    .Include(x => x.CTLocation)
                        .ThenInclude(x => x.CTLocationType)
                            .ThenInclude(x => x.CTLocationTypeGCLifecycleStatusTypes)
                    .FirstOrDefault();

                if(nextHistoryItem != null && 
                    nextHistoryItem.CTLocation != null && 
                        nextHistoryItem.CTLocation.CTLocationType != null &&
                            nextHistoryItem.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null &&
                                nextHistoryItem.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.Count == 1)
                {
                    var ctLocationTypeGCLifecycle = nextHistoryItem.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First();

                    if(ctLocationTypeGCLifecycle.GCLifecycleStatusTypeId == trayLC.LifecycleStatusTypeId + 1)
                    {
                        trayLC.IsActiveStatus = false;
                        _context.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                        {
                            LifecycleStatusTypeId = ctLocationTypeGCLifecycle.GCLifecycleStatusTypeId,
                            GCTrayId = trayLC.GCTrayId,
                            Timestamp = nextHistoryItem.UpdateTimestamp,
                            IsActiveStatus = true,
                            User = nextHistoryItem.UpdateUserId,
                            WWPodContainerId = null,
                            TimestampOriginal = nextHistoryItem.UpdateTimestampOriginal
                        });
                        trayLC.GCTray.GCLifecycleStatusTypeId = ctLocationTypeGCLifecycle.GCLifecycleStatusTypeId;
                        _context.SaveChanges();
                    }
                }
            }
        }

        public void UpdateGCCaseColorStatus(int gcCaseId)
        {
            var gcCase = _context.GCCases.Where(x => x.Id == gcCaseId).Include(x => x.GCTrays).ThenInclude(x => x.GCTrayColorStatuses).ThenInclude(x => x.GCColorStatusType).FirstOrDefault();

            if (gcCase != null)
            {
                var colorStatuses = new List<GCTrayColorStatus>();
                foreach (var gcTray in gcCase.GCTrays)
                {
                    colorStatuses.AddRange(gcTray.GCTrayColorStatuses.Where(x => x.IsActiveStatus == true).ToList());
                }

                var lowestColorStatus = colorStatuses.OrderBy(x => x.GCColorStatusType.Code).FirstOrDefault();

                if (lowestColorStatus != null)
                {
                    // getting previouslyActiveColorStatus:
                    var existingActiveColorStatus = _context.GCCaseColorStatuses
                        .Where(x => x.IsActiveStatus == true && x.GCCaseId == gcCaseId).FirstOrDefault();

                    bool shouldCreateNewActiveColorStatus = false;

                    if (existingActiveColorStatus == null)
                    {
                        shouldCreateNewActiveColorStatus = true;
                    }
                    else
                    {
                        if (existingActiveColorStatus.GCColorStatusTypeId != lowestColorStatus.GCColorStatusTypeId)
                        {
                            existingActiveColorStatus.IsActiveStatus = false;
                            shouldCreateNewActiveColorStatus = true;
                        }
                    }

                    if (shouldCreateNewActiveColorStatus)
                    {
                        _context.GCCaseColorStatuses.Add(new GCCaseColorStatus()
                        {
                            GCColorStatusTypeId = lowestColorStatus.GCColorStatusTypeId,
                            GCCaseId = gcCaseId,
                            Timestamp = DateTime.UtcNow,
                            IsActiveStatus = true,

                        });
                    }
                    _context.SaveChanges();
                }
            }
        }

        public void UpdateGCCaseLifecycleStatus(int gcCaseId)
        {
            _caseManagementClient.UpdateGCCaseLifecycleStatus(gcCaseId);
        }

        public void UpdateGCTrayColorStatus(int gcTrayId, int? gcColorStatusTypeId = null)
        {
            bool isDifferentColor = true;
            bool isLocked = false;

            var gcTrayColors = _context.GCTrayColorStatuses.Where(x => x.GCTrayId == gcTrayId && x.IsActiveStatus == true).ToList();
            if (gcTrayColors != null && gcTrayColors.Count > 0)
            {
                if (gcTrayColors.Count > 1)
                {
                    Log.Error($"Multiple Active statuses for GCTrayId: {gcTrayId}.");
                    throw new Exception();
                }

                var currentTrayColor = gcTrayColors.First();
                isLocked = currentTrayColor.IsLocked.HasValue && currentTrayColor.IsLocked.Value;

                if (gcColorStatusTypeId.HasValue && gcColorStatusTypeId.Value == currentTrayColor.GCColorStatusTypeId)
                {
                    isDifferentColor = false;
                }
                else
                {
                    currentTrayColor.IsActiveStatus = false;
                }
            }

            if (!isLocked && isDifferentColor)
            {
                var colorStatusToAdd = new GCTrayColorStatus()
                {
                    GCTrayId = gcTrayId,
                    IsActiveStatus = true,
                    Timestamp = DateTime.UtcNow
                };

                // Set known color
                if (gcColorStatusTypeId.HasValue && gcColorStatusTypeId.Value > 0)
                {
                    colorStatusToAdd.GCColorStatusTypeId = gcColorStatusTypeId.Value;
                }
                else // Color not known, needs to be calculated:
                {
                    // for now set it to yellow:
                    colorStatusToAdd.GCColorStatusTypeId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Yellow;
                }
                _context.GCTrayColorStatuses.Add(colorStatusToAdd);

                var colorStatusNote = new GCTrayColorStatusNote()
                {
                    GCTrayColorStatus = colorStatusToAdd,
                    Type = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCTrayColorStatusNoteType.SystemGenerated,
                    Title = ProfitOptics.Modules.Sox.Helpers.Constants.COLOR_STATUS_NOTE_SYSTEM_GENERATED_TITLE,
                    Description = null,
                    CreatedAtUtc = DateTime.UtcNow,
                    RequiresResolution = false,
                    ResolvedAtUtc = null,
                    CreatedBy = ProfitOptics.Modules.Sox.Helpers.Constants.GC_SYSTEM_USERNAME
                };
                _context.GCTrayColorStatusNotes.Add(colorStatusNote);

                _context.SaveChanges();
            }
        }

        public void CreateGCImageThumbnails()
        {
            Log.Information($"Getting existing thumbnails for deleting...");
            var thumbnails = _context.GCImages.Where(x => x.ParentId != null).ToList();
            if (thumbnails != null && thumbnails.Count > 0)
            {
                Log.Information($"Fetched {thumbnails.Count} thumbnails.");
                _context.GCImages.RemoveRange(thumbnails);
                _context.SaveChanges();
                Log.Information($"Deleted {thumbnails.Count} thumbnails successfully.");
            }
            else
            {
                Log.Information($"No thumbnail fetched.");
            }

            Log.Information($"Getting GCImages for thumbnail creation...");

            var originalImages = _context.GCImages.Where(x => x.ParentId == null).ToList();
            if (originalImages != null && originalImages.Count > 0)
            {
                Log.Information($"Fetched {originalImages.Count} original GCImages.");
                originalImages.ForEach(oi =>
                {
                    CreateThumbnailsForGCImage(oi);
                    _context.SaveChanges();
                });
            }
        }

        public List<CTContainerTypeActualHistoryItem> GetActualHistoryItems(int ctContainerTypeActualId, int? gcLifecycleId = null)
        {
            var query = _context.CTContainerTypeActualHistoryItems.Where(x =>
                (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG || 
                    x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER) &&
                x.CTContainerTypeActualId == ctContainerTypeActualId).AsQueryable();

            var list = query.ToList();

            if(gcLifecycleId.HasValue && gcLifecycleId.Value > 0)
            {
                switch ((LifecycleStatusType)gcLifecycleId.Value)
                {
                    case LifecycleStatusType.DeconStagedLoaner:
                        gcLifecycleId = (int)LifecycleStatusType.DeconStaged;
                        break;
                    case LifecycleStatusType.SinkLoaner:
                        gcLifecycleId = (int)LifecycleStatusType.Sink;
                        break;
                    case LifecycleStatusType.WasherLoaner:
                        gcLifecycleId = (int)LifecycleStatusType.DeconStaged;
                        break;
                    case LifecycleStatusType.AssemblyStagedLoaner:
                        gcLifecycleId = (int)LifecycleStatusType.AssemblyStaged;
                        break;
                    case LifecycleStatusType.AssemblyLoaner:
                        gcLifecycleId = (int)LifecycleStatusType.Assembly;
                        break;
                }

                var lifeCycleLocationType = _context.CTLocationTypeGCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeId == gcLifecycleId.Value).FirstOrDefault();
                if (lifeCycleLocationType == null)
                {
                    return null;
                }
                var ctLocations = _context.CTLocations.Where(x => x.CTLocationTypeId == lifeCycleLocationType.CTLocationTypeId).ToList();

                if (ctLocations == null)
                {
                    return null;
                }

                var locationIds = ctLocations.Select(y => y.Id).ToList();

                list.ForEach(hi =>
                {
                    if (hi.CTLocationId == null)

                        // Every item need to have LocationID, confirming, and correcting now:
                        CorrectingNonExistingLocationForActualHistoryItem(hi);
                });

                list = list.Where(x => x.CTLocationId.HasValue && locationIds.Contains(x.CTLocationId.Value)).ToList();
            }

            return list;
        }

        public void CreateThumbnailsForGCImage(GCImage gcImage)
        {
            WorkWaveClient.CreateThumbnailsForGCImage(gcImage);
        }

        private int CalculateOppositeSide(int biggerSide, int x, int y)
        {
            return (biggerSide * x) / y;
        }

        public void SyncCaseHistory(int gcCaseId, int minsToAdd)
        {
            var allTrayIds = new List<int>();
            int previousGCCaseId = 0;
            var trays = _context.GCTrays.Include(x => x.GCCase).Where(x => x.GCCaseId == gcCaseId).ToList();
            if (trays != null)
            {
                allTrayIds.AddRange(trays.Select(x => x.Id).ToList());

                trays.ForEach(x =>
                {
                    if (x.CTCaseContainerId.HasValue && x.GCCaseId.HasValue && (!x.IsLoaner.HasValue || x.IsLoaner.Value == false))
                    {
                        //var nextTray = FetchNextTrayOnNextCase(x.CTContainerTypeActualId.Value, x.GCCaseId.Value, x.GCCase.DueTimestamp, null);
                        //if(nextTray != null)
                        //{
                        //    allTrayIds.Add(nextTray.Id);
                        //    nextGCCaseId = nextTray.GCCaseId.Value;
                        //}

                        if (x.CTContainerTypeActualId.HasValue)
                        {
                            var prevTray = FetchPreviousTrayOnPreviousCase(x.CTContainerTypeActualId.Value, x.GCCaseId.Value, x.GCCase.DueTimestamp, null);
                            if (prevTray == null) // no case prior, so look into Induction:
                            {
                                prevTray = FetchInductionTrayForActual(x.CTContainerTypeActualId.Value);
                            }
                            if (prevTray != null)
                            {
                                allTrayIds.Add(prevTray.Id);
                                if (prevTray.GCCaseId.HasValue)
                                {
                                    previousGCCaseId = prevTray.GCCaseId.Value;
                                }
                            }
                        }
                    }
                });
            }

            if (trays != null && trays.Count > 0)
            {
                CompleteBrokenCTTrayHistory(allTrayIds, minsToAdd, null);
            }

            UpdateGCCaseLifecycleStatus(gcCaseId);
            if (previousGCCaseId > 0)
            {
                UpdateGCCaseLifecycleStatus(previousGCCaseId);
            }
        }

        public void UpdateHistoryItemAndLapCount(CTContainerTypeActualHistoryItem hi, int minsToAdd, bool isActualLoaner, bool? updateLapCountAndLocation = false)
        {
            // Used for making a reference with CTLocation into CTContainerTypeActualHistoryItem
            int? ctLocationId = null;
            CTLocation ctLocation = null;
            int? actualLapCount = null;

            if (hi.Status.Equals(Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG) || hi.Status.Equals(Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER))
            {
                Log.Information($"Looking for CTContainerTypeActualHistoryItem: CTContainerTypeActualId:{hi.CTContainerTypeActualId}, UpdateTimestamp:{hi.UpdateTimestamp}");
                var existing = _context.CTContainerTypeActualHistoryItems.Where(x =>
                    (x.Status.Equals(Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG) ||
                        x.Status.Equals(Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)) &&
                    x.CTContainerTypeActualId == hi.CTContainerTypeActualId &&
                    x.UpdateTimestampOriginal == hi.UpdateTimestampOriginal
                ).FirstOrDefault();

                if (updateLapCountAndLocation.HasValue && updateLapCountAndLocation.Value)
                {
                    if (hi.CTLocationId == null)
                    {
                        CorrectingNonExistingLocationForActualHistoryItem(hi);
                    }

                    // Silly Hack because of CT
                    ctLocation = GetCTLocationForLocationName(hi.LocationElapsedCase);

                    if (ctLocation == null && hi.CTLocationId.HasValue)
                    {
                        ctLocation = _context.CTLocations.Where(x => x.Id == hi.CTLocationId.Value).FirstOrDefault();
                    }

                    if (ctLocation != null)
                    {
                        Log.Information($"1st HI Iter - CTLocation: {ctLocation.LocationName}, for LocName: {hi.LocationElapsedCase}");
                        ctLocationId = ctLocation.Id;
                    }
                    else
                    {
                        if (hi.LocationElapsedCase != null &&
                            !hi.LocationElapsedCase.ToLower().Contains(Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION.ToLower()) &&
                            !hi.LocationElapsedCase.ToLower().Contains(Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION.ToLower()))
                        {
                            Log.Error($"1st HI Iter - NO CTLocation for provided LocName: {hi.LocationElapsedCase}");
                        }
                        else
                        {
                            Log.Information($"1st HI Iter - NO CTLocation since provided LocName is: {hi.LocationElapsedCase}");
                        }
                    }

                    bool isLoanerCheckin = false;

                    if (ctLocation == null)
                    {
                        if (existing != null &&
                            (existing.LocationElapsedCase.Equals(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION) ||
                            existing.LocationElapsedCase.Equals(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION)))
                        {
                            isLoanerCheckin = true;
                        }
                    }

                    // Setting Actual's History Item Lap count here:
                    #region Actuals HI Lap Count
                    if (isActualLoaner)
                    {
                        actualLapCount = 0;
                    }
                    else
                    {
                        if (ctLocation != null || isLoanerCheckin)
                        {
                            var previous = _context.CTContainerTypeActualHistoryItems.Where(x =>
                                (x.Status.Equals(Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG) ||
                                x.Status.Equals(Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)) &&
                                x.CTContainerTypeActualId == hi.CTContainerTypeActualId &&
                                x.UpdateTimestampOriginal < hi.UpdateTimestampOriginal
                            ).OrderByDescending(x => x.UpdateTimestampOriginal).FirstOrDefault();
                            if (previous == null)
                            {
                                actualLapCount = 0;
                            }
                            else
                            {
                                actualLapCount = previous.ActualLapCount;
                                bool isPreviousLocationLoanerCheckIn = previous.LocationElapsedCase.Equals(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION) || previous.LocationElapsedCase.Equals(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION);

                                var previousCTLocation = GetCTLocationForLocationName(previous.LocationElapsedCase);
                                if (previousCTLocation != null || isPreviousLocationLoanerCheckIn)
                                {
                                    GCLifecycleStatusType previousLC = new GCLifecycleStatusType();
                                    if (previousCTLocation != null)
                                    {
                                        previousLC = GetGCLifeCycleBasedOnLocationType(previous.UpdateTimestamp, previousCTLocation.CTLocationTypeId, hi.CTContainerTypeActualId, null, null, null, null, null, null);
                                    }
                                    else if (isPreviousLocationLoanerCheckIn)
                                    {
                                        previousLC.Id = (int)LifecycleStatusType.LoanerInbound;
                                    }

                                    if (ctLocation != null)
                                    {
                                        var currentLC = GetGCLifeCycleBasedOnLocationType(hi.UpdateTimestamp, ctLocation.CTLocationTypeId, hi.CTContainerTypeActualId, null, null, null, null, null, null);

                                        var currentLCNotInduction = !Helpers.Helpers.CurrentStatusIsInduction(currentLC.Id);
                                        var previousLCInduction = Helpers.Helpers.CurrentStatusIsInduction(previousLC.Id);

                                        if (currentLCNotInduction && previousLCInduction && actualLapCount.HasValue)
                                        {
                                            actualLapCount++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }

                hi.ActualLapCount = actualLapCount;
                hi.UpdateTimestamp = hi.UpdateTimestampOriginal.AddMinutes(minsToAdd);

                if (existing == null)
                {
                    Log.Information($"NOT Found, Creating new, CTLocationId:{ctLocationId}");
                    hi.CTContainerTypeActualId = hi.CTContainerTypeActualId;
                    hi.CreatedAtUtc = DateTime.UtcNow;
                    hi.CTLocationId = ctLocationId;
                    hi.ActualLapCount = actualLapCount;
                    _context.CTContainerTypeActualHistoryItems.Add(hi);
                }
                else
                {
                    Log.Information($"Found CTContainerTypeActualHistoryItem with ID:{existing.Id}");
                    existing.StatusId = hi.StatusId;
                    existing.Status = hi.Status;
                    existing.LoadId = hi.LoadId;
                    existing.UpdateTimestamp = hi.UpdateTimestampOriginal.AddMinutes(minsToAdd);
                    existing.UpdateUserId = hi.UpdateUserId;
                    existing.ModifiedAtUtc = DateTime.UtcNow;

                    if (ctLocationId.HasValue)
                    {
                        existing.CTLocationId = ctLocationId;
                    }

                    if (actualLapCount.HasValue)
                    {
                        existing.ActualLapCount = actualLapCount;
                    }
                }

                _context.SaveChanges();
            }
        }

        public CTLocation GetCTLocationForLocationName(string locationName, bool? isSterilize = false)
        {
            CTLocation ctLocation = null;
            var devLocationPrefix = "(VME) ";

            if (locationName != null)
            {
                if (locationName == "(NCDH) Container Assembly")
                {
                    locationName = "Container Assembly";
                }

                if (locationName == "Cart Assembly")
                {
                    var prodLocationName = $"VM ELMHURST {locationName}";
                    ctLocation = _context.CTLocations.Where(x => x.LocationName.ToUpper() == prodLocationName.ToUpper()).FirstOrDefault();

                    if (ctLocation == null)
                    {
                        var devLocationName = $"{devLocationPrefix}{locationName}";
                        ctLocation = _context.CTLocations.Where(x => x.LocationName.ToUpper() == devLocationName.ToUpper()).FirstOrDefault();
                    }
                }
                else if (locationName == "Container Assembly")
                {
                    var prodLocationName = $"VM {locationName}";
                    ctLocation = _context.CTLocations.Where(x => x.LocationName.ToUpper() == prodLocationName.ToUpper()).FirstOrDefault();

                    if (ctLocation == null)
                    {
                        var devLocationName = $"{devLocationPrefix}{locationName}";
                        ctLocation = _context.CTLocations.Where(x => x.LocationName.ToUpper() == devLocationName.ToUpper()).FirstOrDefault();
                    }
                }
                else if (locationName == "Decontamination")
                {
                    var prodLocationName = $"VM {locationName}";
                    ctLocation = _context.CTLocations.Where(x => x.LocationName.ToUpper() == prodLocationName.ToUpper()).FirstOrDefault();

                    if (ctLocation == null)
                    {
                        var devLocationName = $"{devLocationPrefix}{locationName}";
                        ctLocation = _context.CTLocations.Where(x => x.LocationName.ToUpper() == devLocationName.ToUpper()).FirstOrDefault();
                    }
                }
                else
                {
                    ctLocation = _context.CTLocations.Where(x => x.LocationName.ToUpper() == locationName.ToUpper()).FirstOrDefault();
                }

                if (ctLocation == null && isSterilize.HasValue && isSterilize.Value)
                {
                    var locationNameParts = locationName.Split("-").ToList();
                    if (locationNameParts != null && locationNameParts.Count > 1 && locationNameParts[1].Length > 1)
                    {
                        var sterilizeLocationName = locationNameParts[1].Substring(0, 2);
                        ctLocation = _context.CTLocations.Where(x => x.LocationName.ToUpper() == sterilizeLocationName.ToUpper()).FirstOrDefault();
                    }

                    // Need to handle this!!!
                    if (locationName == "test123")
                    {
                        var sterilizeLocationType = _context.CTLocationTypeGCLifecycleStatusTypes.Include(x => x.CTLocationType)
                            .Where(x => x.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Sterilize).FirstOrDefault();

                        if (sterilizeLocationType != null)
                        {
                            ctLocation = _context.CTLocations.Where(x => x.CTLocationTypeId == sterilizeLocationType.CTLocationTypeId).FirstOrDefault();
                        }
                    }
                }
            }

            return ctLocation;
        }

        public GCLifecycleStatusType GetGCLifeCycleBasedOnLocationType(DateTime timestamp, int ctLocationTypeId, int ctContainerTypeActualId, bool? isCaseComplete, int? gcCaseId = null, DateTime? dueDate = null, int? localOffsetInMis = null, string user = null, bool? isActualCurrentlyAvailable = null)
        {
            bool isLoaner = false;
            bool isLoanerInductionPassed = false;
            List<CTContainerTypeActualHistoryItem> itemsAfterLoanerCheckIn = null;

            var loanerInboundLC = _context.CTContainerTypeActualHistoryItems
                .Where(x => x.CTContainerTypeActualId == ctContainerTypeActualId &&
                    x.LocationElapsedCase.ToLower().Contains(Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION.ToLower()))
                .OrderByDescending(x => x.UpdateTimestamp).FirstOrDefault();

            if (loanerInboundLC != null)
            {
                isLoaner = true;

                itemsAfterLoanerCheckIn = _context.CTContainerTypeActualHistoryItems
                    .Include(x => x.CTLocation)
                    .Where(x =>
                    x.CTContainerTypeActualId == ctContainerTypeActualId &&
                    x.UpdateTimestamp > loanerInboundLC.UpdateTimestamp).ToList();

                if (itemsAfterLoanerCheckIn != null && itemsAfterLoanerCheckIn.Count > 0)
                {
                    //var verificationLifeCycleLocationType = _context.CTLocationTypeGCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Verification).FirstOrDefault();
                    var sterilizeLifeCycleLocationType = _context.CTLocationTypeGCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Sterilize).FirstOrDefault();
                    var sterileStagedLifeCycleLocationType = _context.CTLocationTypeGCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.SterileStaged).FirstOrDefault();

                    if (sterilizeLifeCycleLocationType != null || sterileStagedLifeCycleLocationType != null)
                    {
                        isLoanerInductionPassed =
                            itemsAfterLoanerCheckIn.Any(x =>
                                x.CTLocation != null &&
                                    ((sterilizeLifeCycleLocationType != null && x.CTLocation.CTLocationTypeId == sterilizeLifeCycleLocationType.CTLocationTypeId) ||
                                    (sterileStagedLifeCycleLocationType != null && x.CTLocation.CTLocationTypeId == sterileStagedLifeCycleLocationType.CTLocationTypeId)));
                    }
                }
            }

            GCLifecycleStatusType gcLifeCycleStatusType = null;
            var ctLocationTypeGCLifecycleStatusType = _context.CTLocationTypeGCLifecycleStatusTypes.Include(x => x.GCLifecycleStatusType).Where(x => x.CTLocationTypeId == ctLocationTypeId).FirstOrDefault();

            // if type is ether of those two, ask for previous Lifecycle, 
            // if it was: 'Assembly - Statged' then: Assembly, it it was: 'Clean Stage' then: Verification
            if (ctLocationTypeGCLifecycleStatusType != null &&
                (ctLocationTypeGCLifecycleStatusType.GCLifecycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Verification ||
                ctLocationTypeGCLifecycleStatusType.GCLifecycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly))
            {
                Log.Information($"Processing LC: {((ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType)ctLocationTypeGCLifecycleStatusType.GCLifecycleStatusType.Id)}, For ActualId: {ctContainerTypeActualId}");
                var lifeCycleStatusType = ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly;

                var previousHistoryItem = _context.CTContainerTypeActualHistoryItems.Include(x => x.CTLocation).Where(x =>
                    x.CTContainerTypeActualId == ctContainerTypeActualId &&
                    x.UpdateTimestamp < timestamp &&
                    (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG ||
                    x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER))
                    .OrderByDescending(x => x.UpdateTimestamp).FirstOrDefault();

                if (previousHistoryItem != null)
                {
                    var ctLocationTypeGcLifeCycleCleanStage = _context.CTLocationTypeGCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.CleanStaged).FirstOrDefault();

                    if (isLoaner) // Loaners can but don't have to have 'Clean Stage', so if we see 'AssemblyLoaner' 2 times (with different timestamps) one after the other, second should be Verification
                    {
                        var ctLocationTypeGcLifeCycleAssemblyLoaner = _context.CTLocationTypeGCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.AssemblyLoaner).FirstOrDefault();
                        if ((previousHistoryItem.CTLocation != null && previousHistoryItem.CTLocation.CTLocationTypeId == ctLocationTypeGcLifeCycleCleanStage.CTLocationTypeId) ||
                            (ctLocationTypeGcLifeCycleAssemblyLoaner != null && previousHistoryItem.CTLocation != null && previousHistoryItem.CTLocation.CTLocationTypeId == ctLocationTypeGcLifeCycleAssemblyLoaner.CTLocationTypeId))
                        {
                            lifeCycleStatusType = Models.GroundControl.LifecycleStatusType.Verification;
                        }
                    }
                    else
                    {
                        //var ctLocationTypeGcLifeCycleAssembly = _context.CTLocationTypeGCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly).FirstOrDefault();
                        if (previousHistoryItem.CTLocation != null && previousHistoryItem.CTLocation.CTLocationTypeId == ctLocationTypeGcLifeCycleCleanStage.CTLocationTypeId)
                        {
                            lifeCycleStatusType = Models.GroundControl.LifecycleStatusType.Verification;
                        }
                    }
                }

                if ((int)lifeCycleStatusType != ctLocationTypeGCLifecycleStatusType.GCLifecycleStatusTypeId)
                {
                    ctLocationTypeGCLifecycleStatusType = _context.CTLocationTypeGCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeId == (int)lifeCycleStatusType).Include(x => x.GCLifecycleStatusType).FirstOrDefault();
                    Log.Information($"Found CTLocationTypeGCLifecycleStatusType: {ctLocationTypeGCLifecycleStatusType.Id}; for GCLifeCycleStatusType: {lifeCycleStatusType}");
                }
            }
            else if (false)// this cycle is now back to CT - Cart Assembly
            {
                // Once CT puts a container in Location that maps to LifecycleStatusType.SterileStaged (or LifecycleStatusType.Sterilize), it marks case Completed and no longer keeps track of it, so GC tracks this and move the container to LifecycleStatusType.ShipmentStaged
                //else // this cycle is now back to CT - Cart Assembly
                if (isCaseComplete.HasValue && isCaseComplete.Value &&
                    (ctLocationTypeGCLifecycleStatusType.GCLifecycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Sterilize ||
                    ctLocationTypeGCLifecycleStatusType.GCLifecycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.SterileStaged))
                {
                    ctLocationTypeGCLifecycleStatusType = _context.CTLocationTypeGCLifecycleStatusTypes.Where(x =>
                        x.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.CaseAssembly)
                        .Include(x => x.GCLifecycleStatusType).FirstOrDefault();
                }
            }
            else if (gcCaseId.HasValue && ctLocationTypeGCLifecycleStatusType != null &&
                ctLocationTypeGCLifecycleStatusType.GCLifecycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.SterileStaged)
            {
                UpdateCaseBarcodesAsync(gcCaseId.Value).GetAwaiter().GetResult();
            }

            if (ctLocationTypeGCLifecycleStatusType != null && ctLocationTypeGCLifecycleStatusType.GCLifecycleStatusType != null)
            {
                gcLifeCycleStatusType = ctLocationTypeGCLifecycleStatusType.GCLifecycleStatusType;
            }

            if (isLoaner && !isLoanerInductionPassed)
            {
                if (gcLifeCycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.DeconStaged)
                {
                    gcLifeCycleStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.DeconStagedLoaner).FirstOrDefault();
                }
                else if (gcLifeCycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Sink)
                {
                    gcLifeCycleStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.SinkLoaner).FirstOrDefault();
                }
                else if (gcLifeCycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Washer)
                {
                    gcLifeCycleStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.WasherLoaner).FirstOrDefault();
                }
                else if (gcLifeCycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.AssemblyStaged)
                {
                    gcLifeCycleStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.AssemblyStagedLoaner).FirstOrDefault();
                }
                else if (gcLifeCycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly)
                {
                    gcLifeCycleStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.AssemblyLoaner).FirstOrDefault();
                }
            }

            // If LC is Induction, check the previous case, and if is in one LC lower, append to that (prevoius) case as well:
            if (gcCaseId.HasValue && gcCaseId.Value > 0 && dueDate.HasValue && dueDate.Value > DateTime.MinValue &&
                Helpers.Helpers.CurrentStatusIsInduction(gcLifeCycleStatusType.Id))
            {
                int lifecycleId = gcLifeCycleStatusType.Id - 1;
                if (gcLifeCycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.CleanStaged)
                {
                    lifecycleId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly;
                }

                GCTray previousTrayOnPreviousCase = FetchPreviousTrayOnPreviousCase(ctContainerTypeActualId, gcCaseId, dueDate, lifecycleId);

                if (previousTrayOnPreviousCase != null)
                {
                    int? lapCountForPreviousTray = null;
                    var historyItem = _context.CTContainerTypeActualHistoryItems.Where(x => x.UpdateTimestamp == timestamp && x.CTContainerTypeActualId == ctContainerTypeActualId).FirstOrDefault();
                    if (historyItem != null)
                    {
                        lapCountForPreviousTray = historyItem.ActualLapCount;
                    }

                    var activeLC = previousTrayOnPreviousCase.GCTraysLifecycleStatusTypes.Where(x => x.IsActiveStatus == true).OrderByDescending(x => x.Timestamp).FirstOrDefault();
                    bool isPreviousLCInSync = activeLC != null &&
                        ((gcLifeCycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.DeconStaged &&
                            activeLC.LifecycleStatusTypeId < (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.DeconStaged &&
                            activeLC.LifecycleStatusTypeId > (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.TransportationStaging) ||
                         activeLC.LifecycleStatusTypeId == gcLifeCycleStatusType.Id - 1);
                    if (activeLC != null)
                    {
                        if (!isPreviousLCInSync &&
                                activeLC.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly &&
                                gcLifeCycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.CleanStaged)
                        {
                            isPreviousLCInSync = true;
                        }


                        if (!isPreviousLCInSync)
                        {
                            Log.Error($"Wrong matching for previous case, and previous Lifecycle! AcutalId: {ctContainerTypeActualId}");
                        }
                        else
                        {
                            activeLC.IsActiveStatus = false;

                            var timestampOriginal = localOffsetInMis.HasValue ? timestamp.AddMinutes(localOffsetInMis.Value * (-1)) : timestamp;
                            _context.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                            {
                                LifecycleStatusTypeId = gcLifeCycleStatusType.Id,
                                GCTrayId = previousTrayOnPreviousCase.Id,
                                Timestamp = timestamp,
                                IsActiveStatus = true,
                                WWPodContainerId = null,
                                User = !string.IsNullOrWhiteSpace(user) ? user : Helpers.Constants.GC_SYSTEM_USERNAME,
                                TimestampOriginal = timestampOriginal,

                            });
                            previousTrayOnPreviousCase.GCLifecycleStatusTypeId = gcLifeCycleStatusType.Id;
                            if (lapCountForPreviousTray.HasValue)
                            {
                                previousTrayOnPreviousCase.ActualLapCount = lapCountForPreviousTray;
                            }
                            _context.SaveChanges();
                        }
                    }
                }
            }

            if (!Helpers.Helpers.CurrentStatusIsInduction(gcLifeCycleStatusType.Id) && isActualCurrentlyAvailable.HasValue && isActualCurrentlyAvailable.Value == false)
            {
                return null;
            }

            return gcLifeCycleStatusType;
        }

        /// <summary>
        /// Returns GCTray for Previous case and provided ActualID
        /// </summary>
        /// <param name="ctContainerTypeActualId"></param>
        /// <param name="gcCaseId"></param>
        /// <param name="dueDate"></param>
        /// <param name="lifecycleId"></param>
        /// <returns>GCTray</returns>
        private GCTray FetchPreviousTrayOnPreviousCase(int ctContainerTypeActualId, int? gcCaseId, DateTime? dueDate, int? lifecycleId)
        {
            var previousTrayOnPreviousCase = _context.GCTrays.Include(x => x.GCCase).Include(x => x.GCTraysLifecycleStatusTypes)
                                .Where(x =>
                                    x.GCCase.DueTimestamp < dueDate.Value &&
                                    ((lifecycleId.HasValue &&
                                        x.GCTraysLifecycleStatusTypes.Any(y =>
                                            // Previous Case's Tray may be stuck in any NON-CT LC, that's why has to be checked:
                                            y.IsActiveStatus == true &&
                                                (y.LifecycleStatusTypeId == lifecycleId ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Loaded ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.ShippedOutbound ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Delivered ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.InSurgery ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.SurgeryComplete ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Prepped ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Picked ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.ShippedInbound ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Received)))
                                        || 1 == 1 // using this to skip execution of this if lifecycleId IS NULL
                                    ) &&
                                    x.GCCaseId != gcCaseId.Value &&
                                    x.CTContainerTypeActualId.HasValue && x.CTContainerTypeActualId.Value == ctContainerTypeActualId
                                    ).OrderByDescending(x => x.GCCase.DueTimestamp).FirstOrDefault();
            return previousTrayOnPreviousCase;
        }

        private GCTray FetchInductionTrayForActual(int ctContainerTypeActualId)
        {
            var inductionTray = _context.GCTrays.Include(x => x.GCCase).Include(x => x.GCTraysLifecycleStatusTypes)
                                .Where(x =>
                                    x.GCCaseId == null &&
                                    x.CTContainerTypeActualId.HasValue && x.CTContainerTypeActualId.Value == ctContainerTypeActualId
                                    ).FirstOrDefault();
            return inductionTray;
        }

        /// <summary>
        /// Returns GCTray for Next case and provided ActualID
        /// </summary>
        /// <param name="ctContainerTypeActualId"></param>
        /// <param name="gcCaseId"></param>
        /// <param name="dueDate"></param>
        /// <param name="lifecycleId"></param>
        /// <returns>GCTray</returns>
        private GCTray FetchNextTrayOnNextCase(int ctContainerTypeActualId, int? gcCaseId, DateTime? dueDate, int? lifecycleId)
        {
            var nextTrayOnNextCase = _context.GCTrays.Include(x => x.GCCase).Include(x => x.GCTraysLifecycleStatusTypes)
                                .Where(x =>
                                    x.GCCase.DueTimestamp > dueDate.Value &&
                                    ((lifecycleId.HasValue &&
                                        x.GCTraysLifecycleStatusTypes.Any(y =>
                                            y.IsActiveStatus == true &&
                                                (y.LifecycleStatusTypeId == lifecycleId ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Loaded ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.ShippedOutbound ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Delivered ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.InSurgery ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.SurgeryComplete ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Prepped ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Picked ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.ShippedInbound ||
                                                y.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Received)))
                                        || 1 == 1 // using this to skip execution of this if lifecycleId IS NULL
                                    ) &&
                                    x.GCCaseId != gcCaseId.Value &&
                                    x.CTContainerTypeActualId.HasValue && x.CTContainerTypeActualId.Value == ctContainerTypeActualId
                                    ).OrderBy(x => x.GCCase.DueTimestamp).FirstOrDefault();
            return nextTrayOnNextCase;
        }

        public void CancelOrDeleteCTCanceledCases(List<string> caseReferences, int minsToAdd)
        {
            var casesToCancelOrDelete = _context.GCCases
                    .Include(x => x.CTCase)
                    .Include(x => x.GCCasesLifecycleStatusTypes)
                    .Include(x => x.GCTrays)
                .Where(x =>
                    x.CanceledAtGCLifecycleStatusTypeId == null &&
                    !caseReferences.Any(y => y == x.CTCase.CaseReference) &&
                    x.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE &&
                    x.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID &&
                    x.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE &&
                    x.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID &&
                    x.CTCase.DueTimestamp.AddMinutes(minsToAdd) > DateTime.UtcNow).ToList();

            Log.Information($"Found {casesToCancelOrDelete.Count} GCCases for CANCEL or DELETE");

            List<string> orderIds = new List<string>();

            if (casesToCancelOrDelete != null && casesToCancelOrDelete.Count > 0)
            {
                casesToCancelOrDelete.ForEach(gcCase =>
                {
                    if (gcCase.GCCasesLifecycleStatusTypes != null && gcCase.GCCasesLifecycleStatusTypes.Count > 0)
                    {
                        var activeLC = gcCase.GCCasesLifecycleStatusTypes.Where(x => x.IsActiveStatus).OrderByDescending(x => x.Timestamp).FirstOrDefault();
                        if (activeLC != null)
                        {
                            LifecycleStatusType lcType = (LifecycleStatusType)activeLC.LifecycleStatusTypeId;

                            switch (lcType)
                            {
                                case LifecycleStatusType.LoanerInbound:
                                case LifecycleStatusType.DeconStaged:
                                case LifecycleStatusType.Sink:
                                case LifecycleStatusType.Washer:
                                case LifecycleStatusType.AssemblyStaged:
                                case LifecycleStatusType.Assembly:
                                case LifecycleStatusType.CleanIncomplete:
                                case LifecycleStatusType.QualityHold:
                                case LifecycleStatusType.CleanStaged:
                                    Log.Information($"DELETING - GCCase Title: {gcCase.Title}, LC: {lcType}");
                                    DeleteGCCase(gcCase.Id);
                                    break;
                                case LifecycleStatusType.Verification:
                                case LifecycleStatusType.Sterilize:
                                case LifecycleStatusType.SterileStaged:
                                case LifecycleStatusType.CaseAssembly:
                                case LifecycleStatusType.TransportationStaging:
                                case LifecycleStatusType.Loaded:
                                case LifecycleStatusType.ShippedOutbound:
                                case LifecycleStatusType.Delivered:
                                case LifecycleStatusType.InSurgery:
                                case LifecycleStatusType.SurgeryComplete:
                                case LifecycleStatusType.Prepped:
                                case LifecycleStatusType.Picked:
                                case LifecycleStatusType.ShippedInbound:
                                case LifecycleStatusType.Received:
                                case LifecycleStatusType.DeconStagedLoaner:
                                case LifecycleStatusType.SinkLoaner:
                                case LifecycleStatusType.WasherLoaner:
                                case LifecycleStatusType.AssemblyStagedLoaner:
                                case LifecycleStatusType.AssemblyLoaner:
                                case LifecycleStatusType.LoanerOutbound:
                                    Log.Information($"CANCELING - GCCase Title: {gcCase.Title}, LC: {lcType}");
                                    gcCase.CanceledAtGCLifecycleStatusTypeId = activeLC.LifecycleStatusTypeId;
                                    _context.SaveChanges();
                                    Log.Information($"Setting color to GRAY for GCCase Title: {gcCase.Title}");
                                    if (gcCase.GCTrays != null && gcCase.GCTrays.Count > 0)
                                    {
                                        gcCase.GCTrays.ToList().ForEach(gcTray =>
                                        {
                                            UpdateGCTrayColorStatus(gcTray.Id, (int)GCColorStatusType.Gray);
                                        });
                                    }
                                    UpdateGCCaseColorStatus(gcCase.Id);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        Log.Information($"DELETING - GCCase Title: {gcCase.Title}, Has NO LC");
                        DeleteGCCase(gcCase.Id);
                    }

                    // Cancel in WW
                    if (gcCase.OutgoingOrderId != null)
                    {
                        orderIds.Add(gcCase.OutgoingOrderId.ToString());
                    }
                    if (gcCase.IncomingOrderId != null)
                    {
                        orderIds.Add(gcCase.IncomingOrderId.ToString());
                    }
                });

                WorkWaveClient.DeleteOrdersInWW(orderIds).GetAwaiter().GetResult();
            }
        }

        public void DeleteGCCase(int gcCaseId)
        {
            var traysForCase = _context.GCTrays.Where(x => x.GCCaseId == gcCaseId).ToList();
            foreach (var tray in traysForCase)
            {
                DeleteGCTray(tray.Id);
            }
            _context.GCTrays.RemoveRange(traysForCase);

            var caseColorStatuses = _context.GCCaseColorStatuses.Where(x => x.GCCaseId == gcCaseId).ToList();
            _context.GCCaseColorStatuses.RemoveRange(caseColorStatuses);

            var casesLifecycleStatuses = _context.GCCasesLifecycleStatusTypes.Where(x => x.GCCaseId == gcCaseId).ToList();
            _context.GCCasesLifecycleStatusTypes.RemoveRange(casesLifecycleStatuses);

            var gcEstimates = _context.GCEstimates.Include(x => x.GCEstimateLines).Where(x => x.GCCaseId == gcCaseId).ToList();
            if (gcEstimates != null && gcEstimates.Count > 0)
            {
                gcEstimates.ForEach(gcEstimate =>
                {
                    if (gcEstimate.GCEstimateLines != null && gcEstimate.GCEstimateLines.Count > 0)
                    {
                        _context.GCEstimateLines.RemoveRange(gcEstimate.GCEstimateLines);
                    }
                });
                _context.GCEstimates.RemoveRange(gcEstimates);
            }
            var gcCase = _context.GCCases.Find(gcCaseId);
            var gcInvoice = _context.GCInvoices.Include(x => x.GCInvoiceLines).Where(x => x.Id == gcCase.GCInvoiceId).FirstOrDefault();
            if (gcInvoice != null)
            {

                if (gcInvoice.GCInvoiceLines != null && gcInvoice.GCInvoiceLines.Count > 0)
                {
                    _context.GCInvoiceLines.RemoveRange(gcInvoice.GCInvoiceLines);
                }
                //_context.GCInvoices.Remove(gcInvoice);
            }

   
            var ctCase = _context.CTCases.Find(gcCase.CTCaseId);
            var ctCaseContainers = _context.CTCaseContainers.Where(x => x.CTCaseId == ctCase.Id);

            if (ctCaseContainers != null)
            {
                foreach (var ctCaseContainer in ctCaseContainers)
                {
                    var gcTrays = _context.GCTrays.Where(x => x.CTCaseContainerId == ctCaseContainer.Id).ToList();
                }
            }


            _context.CTCaseContainers.RemoveRange(ctCaseContainers);
            _context.GCCases.Remove(gcCase);
            _context.CTCases.Remove(ctCase);

			_context.SaveChanges();
        }

        public void DeleteGCTray(int gcTrayId)
        {
            var tray = _context.GCTrays.Find(gcTrayId);
            if (tray != null)
            {
                var traysLifecycleStatuses = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == tray.Id).ToList();
                if (traysLifecycleStatuses != null)
                {
                    traysLifecycleStatuses.ForEach(traysLifecycleStatus =>
                    {
                        DeleteGCTraysLifecycleStatusType(traysLifecycleStatus.Id);
                    });
                }
                _context.GCTraysLifecycleStatusTypes.RemoveRange(traysLifecycleStatuses);

                var traysColorStatuses = _context.GCTrayColorStatuses.Where(x => x.GCTrayId == tray.Id).ToList();
                if (traysColorStatuses != null)
                {
                    traysColorStatuses.ForEach(trayColorStatus =>
                    {
                        var trayColorStatusNotes = _context.GCTrayColorStatusNotes.Where(x => x.GCTrayColorStatusId == trayColorStatus.Id).ToList();
                        _context.GCTrayColorStatusNotes.RemoveRange(trayColorStatusNotes);
                    });
                }
                _context.GCTrayColorStatuses.RemoveRange(traysColorStatuses);
                _context.GCTrays.Remove(tray);
            }
        }

        public int DeleteGCTraysLifecycleStatusType(int id)
        {
            int result = 0;
            var ctCountSheets = _context.CTAssemblyCountSheets.Where(x => x.TrayLifecycleStatusTypeId == id).ToList();
            _context.CTAssemblyCountSheets.RemoveRange(ctCountSheets);
            var ctAssemblyGraphics = _context.CTAssemblyGraphics.Where(x => x.LifecycleStatusTypeId == id).ToList();
            _context.CTAssemblyGraphics.RemoveRange(ctAssemblyGraphics);

            var ctQualityFeed = _context.CTQualityFeedItems.Where(x => x.GCTraysLifecycleStatusTypeId == id).ToList();
            if (ctQualityFeed != null && ctQualityFeed.Count > 0)
            {
                ctQualityFeed.ForEach(qFeed =>
                {
                    var qFeedGraphics = _context.CTQualityFeedItemGraphics.Where(x => x.CTQualityFeedItemId == qFeed.Id).ToList();
                    if (qFeedGraphics != null && qFeedGraphics.Count > 0)
                    {
                        qFeedGraphics.ForEach(qFeedGraphic =>
                        {
                            var qFeedGcImages = _context.GCImages.Where(x => x.Id == qFeedGraphic.GCImageId || x.ParentId == qFeedGraphic.GCImageId).ToList();
                            _context.GCImages.RemoveRange(qFeedGcImages);
                        });
                        _context.CTQualityFeedItemGraphics.RemoveRange(qFeedGraphics);
                    }
                });
                _context.CTQualityFeedItems.RemoveRange(ctQualityFeed);
            }
            result = _context.SaveChanges();
            return result;
        }

        public void UpdateLCForCompletedCases(int? localOffsetInMis = null)
        {
            List<int> casesThatNeedColorUpdate = new List<int>();
            List<GCTraysLifecycleStatusTypes> traysThatNeedLCUpdate = new List<GCTraysLifecycleStatusTypes>();

            var completedCases = _context.GCCases.Include(x => x.CTCase)
                .Where(x =>
                    x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE ||
                    x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID ||
                    x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE ||
                    x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID)
                .Include(x => x.GCTrays).ThenInclude(x => x.GCTraysLifecycleStatusTypes).ToList();

            if(completedCases != null && completedCases.Count > 0)
            {
                completedCases.ForEach(gcCase =>
                {
                    if(gcCase.GCTrays != null && gcCase.GCTrays.Count > 0)
                    {
                        gcCase.GCTrays.ToList().ForEach(gcTray =>
                        {
                            if(gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
                            {
                                ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType? lifecycleStatusType = null;

                                var activeLC = gcTray.GCTraysLifecycleStatusTypes.Where(x => x.IsActiveStatus == true).OrderByDescending(x => x.Timestamp).FirstOrDefault();
                                if (activeLC != null)
                                {
                                    if (false)// this cycle is now back to CT - Cart Assembly
                                    {
                                        if (activeLC.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Sterilize ||
                                          activeLC.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.SterileStaged)
                                        //For completed cases, if in LC: 'Sterilize' or 'SterileStaged' manualy move to: ShipmentStaged
                                        {
                                            lifecycleStatusType = ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.CaseAssembly;
                                        }
                                    }
                                    else if (activeLC.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Received ||
                                        ProfitOptics.Modules.Sox.Helpers.Helpers.CurrentStatusIsInduction(activeLC.LifecycleStatusTypeId))
                                    // For completed cases, if LC in Induction phase, or LifecycleStatusType.Received, add to list: traysThatNeedLCUpdate
                                    {
                                        traysThatNeedLCUpdate.Add(activeLC);
                                    }
                                    //For completed LOANER cases, if in LC: 'Assembly' manualy move to: LoanerOutbound
                                    else if (gcTray.IsLoaner.HasValue && gcTray.IsLoaner.Value && activeLC.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly)
                                    {
                                        lifecycleStatusType = ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerOutbound;

                                        // For this LC, trays go to Green color:
                                        UpdateGCTrayColorStatus(gcTray.Id, (int)GCColorStatusType.Green);
                                        if (gcTray.GCCaseId.HasValue && !casesThatNeedColorUpdate.Any(x => x == gcTray.GCCaseId.Value))
                                        {
                                            casesThatNeedColorUpdate.Add(gcTray.GCCaseId.Value);
                                        }
                                    }
                                }

                                if(lifecycleStatusType.HasValue)
                                {
                                    activeLC.IsActiveStatus = false;
                                    var currentUtcTime = DateTime.UtcNow;
                                    var currentUtcOriginalTime = localOffsetInMis.HasValue ? currentUtcTime.AddMinutes(localOffsetInMis.Value) : currentUtcTime;

                                    gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes
                                    {
                                        LifecycleStatusTypeId = (int)lifecycleStatusType.Value,
                                        GCTrayId = gcTray.Id,
                                        Timestamp = currentUtcTime,
                                        IsActiveStatus = true,
                                        WWPodContainerId = null,
                                        User = Helpers.Constants.GC_SYSTEM_USERNAME,
                                        TimestampOriginal = currentUtcOriginalTime
                                    });
                                    gcTray.GCLifecycleStatusTypeId = (int)lifecycleStatusType.Value;
                                }
                            }
                        });
                    }
                });
                _context.SaveChanges();
            }

            if(casesThatNeedColorUpdate.Count > 0)
            {
                casesThatNeedColorUpdate.ForEach(x =>
                {
                    UpdateGCCaseColorStatus(x);
                });
            }

            if(traysThatNeedLCUpdate.Count > 0)
            {
                traysThatNeedLCUpdate.ForEach(x =>
                {
                    CreateFutureLCBasedOnActualHistory(x);
                });
            }
        }

        public List<long> GetContainerAssemblyActualIds()
        {
            List<long> actualLegacyIds = new List<long>();
            var assemblyStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Code == (int)LifecycleStatusType.Assembly).FirstOrDefault();
            var assemblyLoanerStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Code == (int)LifecycleStatusType.AssemblyLoaner).FirstOrDefault();
            if (assemblyStatusType!=null && assemblyLoanerStatusType != null)
			{
                var gcTrayIds = _context.GCTraysLifecycleStatusTypes.Where(x => x.LifecycleStatusTypeId == assemblyStatusType.Id || x.LifecycleStatusTypeId == assemblyLoanerStatusType.Id).Select(x => x.GCTrayId).ToList();
                actualLegacyIds = _context.GCTrays.Where(x => gcTrayIds.Contains(x.Id)).Include(x => x.CTContainerTypeActual).Select(x => x.CTContainerTypeActual.LegacyId).ToList();
            }
           
            return actualLegacyIds;
        }

        public List<GCTraysLifecycleStatusTypes> GetAssemblyStatuses()
		{
            var assemblyStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Code == (int)LifecycleStatusType.Assembly).FirstOrDefault();
            var assemblyLoanerStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Code == (int)LifecycleStatusType.AssemblyLoaner).FirstOrDefault();
            var statusTypes = _context.GCTraysLifecycleStatusTypes.Where(x => x.LifecycleStatusTypeId == assemblyStatusType.Id || x.LifecycleStatusTypeId == assemblyLoanerStatusType.Id).Include(x=>x.GCTray).Include(x => x.GCTray.CTContainerTypeActual).ToList();
            return statusTypes;
        }

        public void AddCTAssemblyGraphic(CTAssemblyGraphic assemblyGraphic)
		{
            var ag = _context.CTAssemblyGraphics.Where(x => x.LifecycleStatusTypeId == assemblyGraphic.LifecycleStatusTypeId && x.GraphicId == assemblyGraphic.GraphicId).FirstOrDefault();
            if(ag == null) //if it doenst exist in our db, create it
			{
                _context.CTAssemblyGraphics.Add(assemblyGraphic);
                _context.SaveChanges();
            }

		}

        public void AddCTAssemblyCountSheet(CTAssemblyCountSheet countSheet)
        {
            var cs = _context.CTAssemblyCountSheets.Where(x => x.TrayLifecycleStatusTypeId == countSheet.TrayLifecycleStatusTypeId && x.VendorName == countSheet.VendorName && x.ModelNumber == countSheet.ModelNumber).FirstOrDefault();
            if (cs == null) //if it doenst exist in our db, create it
            {
                _context.CTAssemblyCountSheets.Add(countSheet);
                _context.SaveChanges();
            }

        }

        public int? SetGCTraysLifeCycle(DateTime updateTimeStamp, string locationName, int ctContainerTypeActualId, bool isCaseComplete, int? gcCaseId, DateTime dueTimestamp, int minsToAdd, string updateUser, bool? isActualCurrentlyAvailable)
        {
            int? result = null;

            var previousLocation = GetCTLocationForLocationName(locationName);
            int previousLocationTypeId = 0;
            if (previousLocation != null)
            {
                previousLocationTypeId = previousLocation.CTLocationTypeId;
            }
            var lcOfPrevious = GetGCLifeCycleBasedOnLocationType(updateTimeStamp, previousLocationTypeId, ctContainerTypeActualId, isCaseComplete, gcCaseId, dueTimestamp, minsToAdd, updateUser);

            if(lcOfPrevious != null)
            {
                result = lcOfPrevious.Id;
            }

            return result;
        }

        public void UpdateTrayInductionSynchronization(bool syncAllActuals)
        {
            var inventoryActualIds = _context.GCTrays.Where(x =>
                x.GCCaseId == null && x.CTContainerTypeActualId.HasValue).Select(x => x.CTContainerTypeActualId.Value).Distinct().ToList();

            var timestampHistoryPoint = syncAllActuals ? DateTime.UtcNow.AddYears(-100) : DateTime.UtcNow.AddDays(-2);

            var actualIds = _context.CTContainerTypeActuals
                .Where(x => x.CreatedAtUtc > timestampHistoryPoint)
                .Select(x => x.Id).ToList();

            if (actualIds != null && actualIds.Count > 0)
            {
                UpdateTrayInductionForActuals(inventoryActualIds, actualIds);
            }
        }

        public void UpdateGCTrayLifecycles(bool updateAllTrays)
        {
            List<GCTray> trays = null;

            if(updateAllTrays)
            {
                Log.Information("Preparing to fetch all trays and update its LifeCycle Status");
                trays = _context.GCTrays
                    .Include(x => x.CTContainerTypeActual)
                    .Include(x => x.GCTraysLifecycleStatusTypes)
                .Where(x => x.CTContainerTypeActualId.HasValue && x.GCCaseId == null && (x.IsLoaner == null || x.IsLoaner.Value == false)).ToList();
            }
            else
            {
                // get actuals historitems changed or created in last 30 mins:
                int timeSpanInMins = -30;
                Log.Information($"Preparing to fetch trays whose actuals updated {timeSpanInMins} mins ago");
                var latestHistorItems = _context.CTContainerTypeActualHistoryItems.Where(x =>
                    (x.ModifiedAtUtc.HasValue && x.ModifiedAtUtc.Value > DateTime.UtcNow.AddMinutes(timeSpanInMins)) ||
                    (x.ModifiedAtUtc == null && x.CreatedAtUtc > DateTime.UtcNow.AddMinutes(timeSpanInMins))).ToList();

                if(latestHistorItems != null && latestHistorItems.Count > 0)
                {
                    var actualIds = latestHistorItems.Select(x => x.CTContainerTypeActualId).Distinct().ToList();
                    trays = _context.GCTrays
                        .Include(x => x.CTContainerTypeActual)
                        .Include(x => x.GCTraysLifecycleStatusTypes)
                    .Where(x =>
                        x.CTContainerTypeActualId.HasValue &&
                        actualIds.Contains(x.CTContainerTypeActualId.Value) &&
                        x.GCCaseId == null && (x.IsLoaner == null || x.IsLoaner.Value == false)).ToList();
                }
            }

            if(trays != null && trays.Count > 0)
            {
                Log.Information($"Found {trays.Count} GCTrays for LC update");
                trays.ForEach(gcTray =>
                {
                    Log.Information($"-Processing GCTrayId {gcTray.Id}, ActualId: {gcTray.CTContainerTypeActualId}, GCCaseId: {gcTray.GCCaseId}");
                    var latestTrayForActual = _context.GCTrays.Include(x => x.GCCase).Include(x => x.GCTraysLifecycleStatusTypes)
                        .Where(x => x.CTContainerTypeActualId.HasValue && x.CTContainerTypeActualId.Value == gcTray.CTContainerTypeActualId.Value)
                        .OrderByDescending(x => x.GCCase.DueTimestamp).FirstOrDefault();

                    if(latestTrayForActual != null && latestTrayForActual.GCTraysLifecycleStatusTypes != null && latestTrayForActual.GCTraysLifecycleStatusTypes.Count > 0)
                    { 
                        var latestLC = latestTrayForActual.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).FirstOrDefault();
                        Log.Information($"--LatestTray for ActualID: {gcTray.CTContainerTypeActualId}, has lifecycle: {(LifecycleStatusType)latestLC.LifecycleStatusTypeId}");
                        if (gcTray.GCLifecycleStatusTypeId == null || gcTray.GCLifecycleStatusTypeId.Value != latestLC.LifecycleStatusTypeId)
                        {
                            var lcValue = gcTray.GCLifecycleStatusTypeId == null ? "NULL" : ((LifecycleStatusType)gcTray.GCLifecycleStatusTypeId).ToString();
                            Log.Information($"---Current GCTray has LC {lcValue}, latest is: {(LifecycleStatusType)latestLC.LifecycleStatusTypeId}, updating now...");
                            gcTray.GCLifecycleStatusTypeId = latestLC.LifecycleStatusTypeId;
                            _context.SaveChanges();
                        }
                    }
                });
            }
            else
            {
                Log.Information($"Found NO GCTrays for LC update");
            }
        }

        public void RaiseSkippedRequiredCTStep(int gcTrayId, int trayLifecycleId)
        {
            GCTray gcTray = _context.GCTrays.Include(x => x.GCCase).Include(x => x.GCTraysLifecycleStatusTypes).Where(x => x.Id == gcTrayId).FirstOrDefault();

            var requiredCTLifeCycles = _context.GCLifecycleStatusTypes.Where(x =>
                x.Origin == Helpers.Constants.CT_ABBREVIATION_TWO_CHAR &&
                x.IsRequired == true).OrderByDescending(x => x.Order).ToList();

            var trayLifecycle = _context.GCLifecycleStatusTypes.Where(x => x.Id == trayLifecycleId).FirstOrDefault();

            if (gcTray != null && trayLifecycle != null)
            {
                bool checkInductionForPreviousTray = false;

                // if post induction, should check previous tray as well:
                switch ((LifecycleStatusType)trayLifecycle.Id)
                {
                    case LifecycleStatusType.Verification:
                    case LifecycleStatusType.Sterilize:
                    case LifecycleStatusType.SterileStaged:
                    case LifecycleStatusType.CaseAssembly:
                    case LifecycleStatusType.TransportationStaging:
                        checkInductionForPreviousTray = true;
                        break;
                }

                // if not a loaner, ditch loaner lifecycles
                List<GCLifecycleStatusType> lifecyclesToUse = new List<GCLifecycleStatusType>();
                if(gcTray.IsLoaner.HasValue && gcTray.IsLoaner.Value == true)
                {
                    lifecyclesToUse = requiredCTLifeCycles;
                }
                else
                {
                    lifecyclesToUse = requiredCTLifeCycles.Where(x => x.IsLoaner == false).ToList();
                }

                var postInductionRequiredLifeCycleIds = lifecyclesToUse.Where(x => x.Order < trayLifecycle.Order).Select(x => x.Id).ToList();

                postInductionRequiredLifeCycleIds.ForEach(lifecycleId =>
                {
                    AddGCSystemAlertIfRequired(gcTray, lifecycleId);
                });

                if (checkInductionForPreviousTray && gcTray.CTContainerTypeActualId.HasValue && gcTray.GCCaseId.HasValue && gcTray.GCCase != null)
                {
                    var previousTray = FetchPreviousTrayOnPreviousCase(gcTray.CTContainerTypeActualId.Value, gcTray.GCCaseId.Value, gcTray.GCCase.DueTimestamp, null);

                    if(previousTray != null)
                    {
                        var requiredNonLoanerLifeCycles = requiredCTLifeCycles.Where(x => x.IsLoaner == false).ToList();

                        requiredNonLoanerLifeCycles.ForEach(lifecycleId =>
                        {
                            AddGCSystemAlertIfRequired(previousTray, lifecycleId.Id);
                        });
                    }
                }
            }
        }

        private void AddGCSystemAlertIfRequired(GCTray gcTray, int lifecycleId)
        {
            if (!gcTray.GCTraysLifecycleStatusTypes.Any(x => x.LifecycleStatusTypeId == lifecycleId))
            {
                Log.Information($"-GCTray Id:{gcTray.Id}, ActualId: {gcTray.CTContainerTypeActualId}, GCCaseId: {gcTray.GCCaseId}, skipped required step: {(LifecycleStatusType)lifecycleId}, checking if exist already...");
                int alertTypeId = Helpers.Helpers.MapSystemAlertTypeForMissingLifeCycleId((LifecycleStatusType)lifecycleId);

                // look for that notification that is no older than 6 hrs
                if (!_context.GCSystemAlerts.Any(x => x.CreatedAtUtc > DateTime.UtcNow.AddHours(Helpers.Constants.GC_SYSTEM_ALERT_TIME_TO_LOOK_BACK) &&
                    x.GCTrayId == gcTray.Id &&
                    x.GCSystemAlertTypeId == alertTypeId))
                {
                    Log.Information($"--No Alert exists ({(Models.GroundControl.GCSystemAlertType)alertTypeId}), adding now...");
                    _context.GCSystemAlerts.Add(new GCSystemAlert()
                    {
                        GCTrayId = gcTray.Id,
                        GCSystemAlertTypeId = alertTypeId,
                        CreatedAtUtc = DateTime.UtcNow,
                        MarkedAsReadAtUtc = null
                    });
                    _context.SaveChanges();
                }
                else
                {
                    Log.Information($"--Alert already exists, no action...");
                }
            }
        }

        private void UpdateTrayInductionForActuals(List<int> inventoryActualIds, List<int> actualIds)
        {
            var inductionLifeCycleLocationTypes = _context.CTLocationTypeGCLifecycleStatusTypes.Include(x => x.GCLifecycleStatusType).Where(x =>
                                    x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.DeconStaged ||
                                    x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Sink ||
                                    x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Washer ||
                                    x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.AssemblyStaged ||
                                    x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Assembly ||
                                    x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.CleanIncomplete ||
                                    x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.QualityHold ||
                                    x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged).ToList();

            var deconLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.DeconStaged).FirstOrDefault();
            var sinkLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Sink).FirstOrDefault();
            var washerLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Washer).FirstOrDefault();
            var assemblyStagedLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.AssemblyStaged).FirstOrDefault();
            var assemblyLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Assembly).FirstOrDefault();
            var cleanInclompleteLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.CleanIncomplete).FirstOrDefault();
            var quarantineLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.QualityHold).FirstOrDefault();
            var cleanStagedLCLT = inductionLifeCycleLocationTypes.Where(x => x.GCLifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged).FirstOrDefault();

            actualIds.ForEach(actualId =>
            {
                Log.Information($"Processing ActualId: {actualId}...");

                var allHistoryItems = _context.CTContainerTypeActualHistoryItems.Include(x => x.CTLocation).Where(x =>
                    x.CTContainerTypeActualId == actualId &&
                    (x.Status == Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG || x.Status == Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)).ToList();

                if (allHistoryItems != null && allHistoryItems.Count > 0)
                {
                    var isLoaner = allHistoryItems.Any(x =>
                                    x.LocationElapsedCase != null &&
                                        (x.LocationElapsedCase.ToLower().Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION.ToLower()) ||
                                        x.LocationElapsedCase.ToLower().Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION.ToLower())));

                    if (isLoaner)
                    {
                        allHistoryItems.ForEach(hi =>
                        {
                            hi.ActualLapCount = 0;
                        });
                        _context.SaveChanges();
                    }
                    else
                    {
                        var trayHistoryItems = allHistoryItems.Where(x =>
                            x.CTContainerTypeActualId == actualId &&
                            //(earliestLC == null || x.UpdateTimestamp < earliestLC.Timestamp) &&
                            (x.Status == Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG || x.Status == Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER) &&
                            (x.CTLocationId == null ||
                                (deconLCLT != null && x.CTLocation.CTLocationTypeId == deconLCLT.CTLocationTypeId) ||
                                (sinkLCLT != null && x.CTLocation.CTLocationTypeId == sinkLCLT.CTLocationTypeId) ||
                                (washerLCLT != null && x.CTLocation.CTLocationTypeId == washerLCLT.CTLocationTypeId) ||
                                (assemblyStagedLCLT != null && x.CTLocation.CTLocationTypeId == assemblyStagedLCLT.CTLocationTypeId) ||
                                (assemblyLCLT != null && x.CTLocation.CTLocationTypeId == assemblyLCLT.CTLocationTypeId) ||
                                (cleanInclompleteLCLT != null && x.CTLocation.CTLocationTypeId == cleanInclompleteLCLT.CTLocationTypeId) ||
                                (quarantineLCLT != null && x.CTLocation.CTLocationTypeId == quarantineLCLT.CTLocationTypeId) ||
                                (cleanStagedLCLT != null && x.CTLocation.CTLocationTypeId == cleanStagedLCLT.CTLocationTypeId))
                        ).OrderBy(x => x.UpdateTimestamp)
                        //.Take(8) // There are 8 Induction Steps (LifeCycles)
                        .ToList();

                        var actual = _context.CTContainerTypeActuals.Find(actualId);

                        if (actual != null)
                        {
                            GCTray gcTray = null;

                            if (inventoryActualIds.Any(x => x == actualId)) // GCTray Exists, fetch it
                            {
                                gcTray = _context.GCTrays.Include(x => x.GCTraysLifecycleStatusTypes).Where(x => x.GCCaseId == null && x.CTContainerTypeActualId == actualId).OrderBy(x => x.Id).FirstOrDefault();
                            }

                            if (gcTray == null)// GCTray doesn't exist, create it
                            {
                                gcTray = new GCTray()
                                {
                                    CTContainerTypeId = actual.CTContainerTypeId,
                                    GCCaseId = null,
                                    CTCaseContainerId = null,
                                    CTContainerTypeActualId = actualId,
                                    Barcode = Helpers.Helpers.GenerateGCTrayBarcode(actual.LegacyId),
                                    IsActiveStatus = false,
                                    IsLoaner = false,
                                    GCLifecycleStatusTypeId = null,
                                    IsActualCurrentlyAvailable = null,
                                    ActualLapCount = 0,
                                    GCTraysLifecycleStatusTypes = new List<GCTraysLifecycleStatusTypes>()
                                };
                                _context.GCTrays.Add(gcTray);
                                Log.Information($"-GCTray doesn't exist, creating it now...");
                            }

                            if (gcTray.GCTraysLifecycleStatusTypes == null)
                            {
                                gcTray.GCTraysLifecycleStatusTypes = new List<GCTraysLifecycleStatusTypes>();
                            }

                            if (trayHistoryItems != null && trayHistoryItems.Count > 0)
                            {
                                Log.Information($"--Found {trayHistoryItems.Count} TrayHistoryItems");
                                for (int i = 0; i < trayHistoryItems.Count; i++)
                                {
                                    var x = trayHistoryItems[i];

                                    if (x.CTLocationId == null)
                                    {
                                        var location = CorrectingNonExistingLocationForActualHistoryItem(x);
                                        if(location != null)
                                        {
                                            x.CTLocationId = location.Id;
                                            x.CTLocation = location;
                                        }
                                    }


                                    if (x.CTLocation != null)
                                    {
                                        var lifecycleStatus = inductionLifeCycleLocationTypes.Where(y => y.CTLocationTypeId == x.CTLocation.CTLocationTypeId).FirstOrDefault();
                                        if (lifecycleStatus != null)
                                        {
                                            if (//!gcTray.GCTraysLifecycleStatusTypes.Any(lc => lc.Timestamp == x.UpdateTimestamp) &&
                                                !gcTray.GCTraysLifecycleStatusTypes.Any(lc => lc.LifecycleStatusTypeId == lifecycleStatus.GCLifecycleStatusTypeId))
                                            {
                                                int lastLC = 0;
                                                // Not using this anymore, cause LCs can repeat
                                                if (gcTray.GCTraysLifecycleStatusTypes.Count > 0)
                                                {
                                                    lastLC = gcTray.GCTraysLifecycleStatusTypes.Max(x => x.LifecycleStatusTypeId);
                                                }
                                                if (lifecycleStatus.GCLifecycleStatusTypeId > lastLC)
                                                {
                                                    gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                                    {
                                                        LifecycleStatusTypeId = lifecycleStatus.GCLifecycleStatusTypeId,
                                                        Timestamp = x.UpdateTimestamp,
                                                        IsActiveStatus = false, // (i == (trayHistoryItems.Count - 1)),
                                                        WWPodContainerId = null,
                                                        User = x.UpdateUserId,
                                                        TimestampOriginal = x.UpdateTimestampOriginal
                                                    });
                                                    Log.Information($"----Added TrayInduction LC: {(LifecycleStatusType)lifecycleStatus.GCLifecycleStatusTypeId}");
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // Setting latest LC to gcTray:
                            // This bellow is moved to this function: _GroundControlService.UpdateGCTrayLifecycles()
                            if (false)
                            {
                                #region Setting latest LC to gcTray
                                int latestLC = 0;
                                var dateOfLatestHistoryTimestamp = DateTime.MinValue;
                                var dateOfLatestTrayLC = DateTime.MinValue;
                                var last2ActualHistoryItems = _context.CTContainerTypeActualHistoryItems
                                .Include(x => x.CTLocation).ThenInclude(x => x.CTLocationType).ThenInclude(x => x.CTLocationTypeGCLifecycleStatusTypes)
                                .Where(x =>
                                    x.CTContainerTypeActualId == actualId &&
                                    (x.Status == Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG || x.Status == Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)
                                ).OrderByDescending(x => x.UpdateTimestamp).Take(2).ToList();

                                // correncting location ID if null
                                last2ActualHistoryItems.ForEach(hi =>
                                {
                                    if (hi.CTLocationId == null)
                                    {
                                    // Every item need to have LocationID, confirming, and correcting now:
                                    CorrectingNonExistingLocationForActualHistoryItem(hi);
                                    }
                                });
                                if (last2ActualHistoryItems != null && last2ActualHistoryItems.Count > 0)
                                {
                                    // first of last 2 means this one is the last:
                                    var firstHI = last2ActualHistoryItems.FirstOrDefault();
                                    dateOfLatestHistoryTimestamp = firstHI.UpdateTimestamp;
                                    if (firstHI.CTLocation != null && firstHI.CTLocation.CTLocationType != null && firstHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null)
                                    {
                                        var lifeCycle = firstHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.FirstOrDefault();
                                        if (lifeCycle != null)
                                        {
                                            if (lifeCycle.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Assembly || lifeCycle.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Verification)
                                            {
                                                var nextHI = last2ActualHistoryItems.LastOrDefault();
                                                if (nextHI != null && nextHI.Id != firstHI.Id && nextHI.CTLocation.CTLocationType != null && nextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null) // <-- means it's not the same, hence there were 2
                                                {
                                                    var lcOfNext = nextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.FirstOrDefault();

                                                    if (lcOfNext != null)
                                                    {
                                                        latestLC = lcOfNext.GCLifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged ?
                                                            (int)LifecycleStatusType.Verification :
                                                            (int)LifecycleStatusType.Assembly;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                latestLC = lifeCycle.GCLifecycleStatusTypeId;
                                            }
                                        }
                                    }
                                }

                                var latestTrayOfActual = _context.GCTrays.Include(x => x.GCCase).Include(x => x.GCTraysLifecycleStatusTypes).Where(x => x.GCCaseId.HasValue && x.CTContainerTypeActualId == actualId).OrderByDescending(x => x.GCCase.DueTimestamp).FirstOrDefault();
                                if (latestTrayOfActual != null && latestTrayOfActual.GCTraysLifecycleStatusTypes != null && latestTrayOfActual.GCTraysLifecycleStatusTypes.Count > 0)
                                {
                                    var latestTraysLC = latestTrayOfActual.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).FirstOrDefault();
                                    dateOfLatestTrayLC = latestTraysLC.Timestamp;

                                    if (dateOfLatestTrayLC > dateOfLatestHistoryTimestamp)
                                    {
                                        latestLC = latestTraysLC.LifecycleStatusTypeId;
                                    }
                                }

                                if (latestLC > 0 && gcTray.GCLifecycleStatusTypeId != latestLC)
                                {
                                    string previousCycle = gcTray.GCLifecycleStatusTypeId != null ? ((LifecycleStatusType)gcTray.GCLifecycleStatusTypeId).ToString() : "none";
                                    Log.Information($"-GCTray's LifeCycle was: {previousCycle}, setting now to {(LifecycleStatusType)latestLC}");
                                    gcTray.GCLifecycleStatusTypeId = latestLC;
                                }
                                #endregion
                            }

                            _context.SaveChanges();
                        }
                    }
                }
            });
        }

        public GCVendor CreateGCVendor(long? vendorId, string vendorName, bool createChildCompany, int? gcCustomerId)
        {
            GCVendor gcVendor = new GCVendor()
            {
                LegacyId = vendorId,
                Title = vendorName
            };

            if(createChildCompany)
            {
                var othersParentCompany = _context.GCParentCompanies.Where(x => x.Title == Helpers.Constants.GC_PARENT_COMPANY_OTHERS).FirstOrDefault();
                if(othersParentCompany == null)
                {
                    othersParentCompany = new GCParentCompany()
                    {
                        Title = Helpers.Constants.GC_PARENT_COMPANY_OTHERS
                    };
                }

                _context.GCChildCompanies.Add(new GCChildCompany()
                {
                    GCParentCompany = othersParentCompany,
                    Title = vendorName,
                    GCCustomerId = gcCustomerId,
                    GCVendor = gcVendor
                });
            }

            _context.GCVendors.Add(gcVendor);
            _context.SaveChanges();

            return gcVendor;
        }

        public void UpdateAllHistoryItemsAndLapCount(int minsToAdd, bool? allHistoryItems = false)
        {
            var hiQuery = _context.CTContainerTypeActualHistoryItems.Where(x =>
                x.Status.Equals(Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG) ||
                x.Status.Equals(Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)
            ).AsQueryable();

            if (!allHistoryItems.HasValue || allHistoryItems.Value == false)
            {
                hiQuery = hiQuery.Where(x => x.ActualLapCount == null).AsQueryable();
            }

            var hiList = hiQuery.ToList();

            if (hiList != null && hiList.Count > 0)
            {
                Log.Information($"Startng to update History and LapCount for: {hiList.Count} items...");

                var actualIds = hiList.Select(x => x.CTContainerTypeActualId).Distinct().ToList();
                var actualList = _context.CTContainerTypeActuals.Where(x => actualIds.Contains(x.Id)).ToList();

                if(actualList != null  && actualList.Count > 0)
                {
                    actualList.ForEach(actual =>
                    {
                        var actualsHiList = hiList.Where(x => x.CTContainerTypeActualId == actual.Id).ToList();

                        if(actualsHiList != null && actualsHiList.Count > 0)
                        {
                            bool isLoaner = actualsHiList.Any(x =>
                                x.LocationElapsedCase != null &&
                                    (x.LocationElapsedCase.ToLower().Contains(Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION.ToLower()) ||
                                    x.LocationElapsedCase.ToLower().Contains(Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION.ToLower())));
                            Log.Information($"-Processing Actual Id: {actual.Id}, Name: {actual.ParentName} {actual.Name}, IsLoaner: {isLoaner}");
                            Log.Information($"--Processing History Items now...");

                            actualsHiList.ForEach(hi =>
                            {
                                UpdateHistoryItemAndLapCount(hi, minsToAdd, isLoaner, true);
                            });
                        }
                        else
                        {
                            Log.Information($"-NO HI found for Actual: {actual.Id}, Name: {actual.ParentName} {actual.Name}");
                        }
                    });
                }
            }
            else
            {
                Log.Information($"No itmes found to update.");
            }
        }

        public List<GCLifecycleStatusType> GetAllGCLifecycles()
        {
            return _context.GCLifecycleStatusTypes.ToList();
        }

        public GCChildCompany CreateGCChildCompany(string title, GCVendor gcVendor, int? gcCustomerId, int? gcParentCompanyId)
        {
            var childCompany = new GCChildCompany()
            {
                GCParentCompanyId = gcParentCompanyId,
                GCCustomerId = gcCustomerId,
                Title = title,
                GCVendor = gcVendor
            };
            _context.GCChildCompanies.Add(childCompany);
            _context.SaveChanges();
            return childCompany;
        }

        public void UpdateGCTrayLapCount(GCTray gcTray, List<int> ctLifeCycleIds)
        {
            var hiForActual = _context.CTContainerTypeActualHistoryItems.Where(x =>
                                        (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG ||
                                            x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER) &&
                                        x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId).ToList();

            if (hiForActual != null && hiForActual.Count > 0)
            {
                var isLoaner = hiForActual.Any(x =>
                            x.LocationElapsedCase != null &&
                                (x.LocationElapsedCase.ToLower().Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION.ToLower()) ||
                                x.LocationElapsedCase.ToLower().Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION.ToLower())));

                int? lapCount = null;
                Log.Information($"Processing GCTrayID: {gcTray.Id}, GCCaseId: {gcTray.GCCaseId}...");
                if (isLoaner)
                {
                    lapCount = 0;
                    Log.Information($"IS a Loaner");
                    gcTray.ActualLapCount = lapCount;
                    _context.SaveChanges();
                    Log.Information($"Updated GCTrayID: {gcTray.Id}, new ActualLapCount: {lapCount}");
                }
                else
                {
                    Log.Information($"NOT a Loaner");

                    if (gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
                    {
                        Log.Information($"GCTrayID: {gcTray.Id} has {gcTray.GCTraysLifecycleStatusTypes.Count} current LCs");
                        var lcList = gcTray.GCTraysLifecycleStatusTypes.Where(x => ctLifeCycleIds.Any(y => y == x.LifecycleStatusTypeId)).ToList();
                        int? totalLCCountSum = null;
                        int lcCount = lcList.Count;
                        int? lcActualLap = null;

                        for (int i = 0; i < lcCount; i++)
                        {
                            var lc = lcList[i];

                            Log.Information($"Fetching HI for timestamp: {lc.Timestamp}");
                            var hiForLC = hiForActual.Where(x => x.UpdateTimestamp == lc.Timestamp).FirstOrDefault();
                            string hiInfo = hiForLC == null ? "-no data-" : hiForLC.Id.ToString();
                            Log.Information($"HI found: {hiForLC != null}, ID: {hiInfo}");
                            Log.Information($"LcActualLap: {lcActualLap}");

                            if (hiForLC == null && lcActualLap.HasValue)
                            {
                                Log.Information($"HI not found, but LapCount:{lcActualLap.Value}");
                                hiForLC = hiForActual.Where(x => x.ActualLapCount.HasValue && x.ActualLapCount == lcActualLap).FirstOrDefault();
                                if (hiForLC != null)
                                {
                                    Log.Information($"Found HI with different timestamp, updating LC from: {lc.Timestamp} to: {hiForLC.UpdateTimestamp}");
                                    lc.Timestamp = hiForLC.UpdateTimestamp;
                                    lc.TimestampOriginal = hiForLC.UpdateTimestampOriginal;
                                    _context.SaveChanges();
                                }
                            }

                            if (hiForLC != null && hiForLC.ActualLapCount.HasValue)
                            {
                                if (!lcActualLap.HasValue)
                                {
                                    lcActualLap = hiForLC.ActualLapCount.Value;
                                    Log.Information($"Setting LapCount: {lcActualLap}.");
                                }

                                if (!totalLCCountSum.HasValue && hiForLC.ActualLapCount.HasValue)
                                {
                                    totalLCCountSum = 0;
                                }

                                totalLCCountSum += hiForLC.ActualLapCount ?? 0;
                                Log.Information($"Processing HI Item Id {hiForLC.Id}, LapCount is: {hiForLC.ActualLapCount}, Timestamp: {hiForLC.UpdateTimestamp}.");
                            }
                            else // no History item or it has no lap count, reset everything, skip for now!
                            {
                                totalLCCountSum = null;
                                lcActualLap = null;
                                lapCount = null;
                                Log.Information($"NO History item found ({(LifecycleStatusType)lc.LifecycleStatusTypeId}) or it has no lap count, timestamp is: {lc.Timestamp}, reseting everything, skiping for now...");
                            }
                        }

                        if (totalLCCountSum.HasValue && lcActualLap.HasValue)
                        {
                            if (totalLCCountSum.Value == 0 && lcActualLap.Value == 0)
                            {
                                lcActualLap = 0;
                                lapCount = lcActualLap;
                            }
                            //else if(lcActualLap.Value == 0)
                            //{
                            //    lapCount = -1;
                            //}
                            else
                            {
                                var divisionRes = (float)totalLCCountSum / (float)lcActualLap;

                                Log.Information($"---LCActualLap: {lcActualLap}.");
                                Log.Information($"---DivisionRes: {divisionRes}.");
                                Log.Information($"---LCCount: {lcCount}.");

                                if (lcActualLap > 0 && divisionRes == lcCount)
                                {
                                    lapCount = lcActualLap;
                                }
                                else
                                {
                                    lapCount = -1;
                                }
                            }
                        }
                    }
                    else
                    {
                        Log.Information($"NO LCs for GCTrayID: {gcTray.Id} ");
                    }

                    if (lapCount.HasValue)
                    {
                        gcTray.ActualLapCount = lapCount;
                        _context.SaveChanges();
                        Log.Information($"Updated GCTrayID: {gcTray.Id}, new ActualLapCount: {lapCount}");
                    }
                }
            }
        }

        public void UpdateGCTraysLapCount(bool? allTrays = false)
        {
            var ctLifeCycleIds = _context.GCLifecycleStatusTypes.Where(x => x.Origin == Helpers.Constants.CT_ABBREVIATION_TWO_CHAR).Select(x => x.Id).ToList();

            if (false)// this cycle is now back to CT - Cart Assembly
            {
                ctLifeCycleIds.Remove((int)LifecycleStatusType.CaseAssembly); // removing for now...
            }

            if(ctLifeCycleIds == null || ctLifeCycleIds.Count == 0)
            {
                var message = "No CT-based Lifecycles available!";
                Log.Error(message);
                throw new Exception(message);
            }

            var gcTrayQuery = _context.GCTrays.Include(x => x.GCTraysLifecycleStatusTypes).AsQueryable();

            if (!allTrays.HasValue || allTrays.Value == false)
            {
                gcTrayQuery = gcTrayQuery.Where(x => x.ActualLapCount == null).AsQueryable();
            }

            var gcTrays = gcTrayQuery.ToList();

            if(gcTrays != null && gcTrays.Count > 0)
            {
                Log.Information($"Found {gcTrays.Count} GCTrays to update ActualLapCount.");
                gcTrays.ForEach(gcTray =>
                {
                    UpdateGCTrayLapCount(gcTray, ctLifeCycleIds);
                });
            }
        }

        public void CompleteBrokenCTTrayHistory(List<int> gcTrayIds, int? minsToAdd = null, bool? scanAllTrays = false)
        {
            // TODO: Loaners!!!

            List<GCTray> notCompletedGCTrays = null;

            if (gcTrayIds != null)
            {
                notCompletedGCTrays = _context.GCTrays
                        .Include(x => x.GCCase)
                        .Include(x => x.GCTraysLifecycleStatusTypes).ThenInclude(x => x.LifecycleStatusType)
                    .Where(x => gcTrayIds.Contains(x.Id)).ToList();
            }
            else
            {
                DateTime scanTime = DateTime.UtcNow.AddDays(-3);
                if (scanAllTrays.HasValue && scanAllTrays.Value)
                {
                    scanTime = DateTime.MinValue;
                }

                var query = _context.GCTrays
                        .Include(x => x.GCCase)
                        .Include(x => x.GCTraysLifecycleStatusTypes).ThenInclude(x => x.LifecycleStatusType)
                    .Where(x =>
                    x.GCCaseId.HasValue &&
                    x.CTContainerTypeActualId.HasValue &&
                    x.GCCase.DueTimestamp > scanTime);

                if (scanAllTrays.Value == false)
                {
                    query = query.Where(x => x.IsTrayHistoryComplete == null || x.IsTrayHistoryComplete.Value == false);
                }
                notCompletedGCTrays = query.ToList();
            }

            if (notCompletedGCTrays != null && notCompletedGCTrays.Count > 0)
            {
                notCompletedGCTrays.ForEach(gcTray =>
                {
                    //bool keepGoing = true;
                    //while (keepGoing)
                    //{
                    //    keepGoing = CompleteBrokenHistoryForGCTray_V2(gcTray);
                    //}
                    ////CompleteBrokenHistoryForGCTray(gcTray);
                    ///

                    CompleteBrokenHistoryForGCTray_V3(gcTray, minsToAdd ?? 0);
                });
            }
        }

        private void CompleteBrokenHistoryForGCTray(GCTray gcTray)
        {
            var expectedGCTrayLifeCycles = ProfitOptics.Modules.Sox.Helpers.Helpers.LifeCycleCTIds(gcTray.IsLoaner.HasValue && gcTray.IsLoaner.Value);

            // Removing Assembly & Verification - to be handled latter
            expectedGCTrayLifeCycles.Remove((int)LifecycleStatusType.Assembly);
            expectedGCTrayLifeCycles.Remove((int)LifecycleStatusType.Verification);

            if (!gcTray.IsLoaner.HasValue || gcTray.IsLoaner == false)
            {
                var previousGCTrayOnPreviousCase = FetchPreviousTrayOnPreviousCase(gcTray.CTContainerTypeActualId.Value, gcTray.GCCaseId, gcTray.GCCase.DueTimestamp, null);

                var historyItemsForActual = _context.CTContainerTypeActualHistoryItems
                .Include(x => x.CTLocation)
                    .ThenInclude(x => x.CTLocationType)
                        .ThenInclude(x => x.CTLocationTypeGCLifecycleStatusTypes)
                .Where(x => x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value &&
                    (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG ||
                        x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER))
                .OrderBy(x => x.UpdateTimestamp)
                .ToList();

                if (historyItemsForActual != null && historyItemsForActual.Count > 0)
                {
                    // Every item need to have LocationID, confirming, and correcting now:
                    historyItemsForActual.ForEach(hi =>
                    {
                        if (hi.CTLocationId == null)
                        {
                            CorrectingNonExistingLocationForActualHistoryItem(hi);
                        }
                    });

                    GCTraysLifecycleStatusTypes firstLC = gcTray.GCTraysLifecycleStatusTypes.Where(x =>
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.TransportationStaging ||
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.CaseAssembly ||
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.SterileStaged ||
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.Sterilize
                                ).OrderBy(x => x.Timestamp).FirstOrDefault();
                    GCTraysLifecycleStatusTypes lastLC = null;
                    GCTraysLifecycleStatusTypes previousTrayFirstLC = null;
                    GCTraysLifecycleStatusTypes previousTrayLastLC = null;

                    if (previousGCTrayOnPreviousCase != null && previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes != null && previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes.Count > 0)
                    {
                        previousTrayFirstLC = previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes.Where(x =>
                                Helpers.Helpers.CurrentStatusIsInduction(x.LifecycleStatusTypeId)
                            ).OrderBy(x => x.Timestamp).FirstOrDefault();
                        previousTrayLastLC = previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes.Where(x =>
                                Helpers.Helpers.CurrentStatusIsInduction(x.LifecycleStatusTypeId)
                            ).OrderByDescending(x => x.Timestamp).FirstOrDefault();

                        if (firstLC == null && previousTrayLastLC != null)
                        {
                            firstLC = previousTrayLastLC;
                            //firstLC.GCTrayId = gcTray.Id;
                        }
                    }

                    if (gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
                    {
                        if (firstLC == null)
                        {
                            firstLC = gcTray.GCTraysLifecycleStatusTypes.Where(x =>
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.TransportationStaging ||
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.CaseAssembly ||
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.SterileStaged ||
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.Sterilize
                                ).OrderBy(x => x.Timestamp).FirstOrDefault();
                        }

                        lastLC = gcTray.GCTraysLifecycleStatusTypes.Where(x =>
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.TransportationStaging ||
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.CaseAssembly ||
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.SterileStaged ||
                                    x.LifecycleStatusTypeId == (int)LifecycleStatusType.Sterilize
                                ).OrderByDescending(x => x.Timestamp).FirstOrDefault();
                    }

                    if (lastLC == null && previousGCTrayOnPreviousCase != null && previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes != null && previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes.Count > 0)
                    {
                        if (previousTrayLastLC != null)
                        {
                            lastLC = previousTrayLastLC;
                        }
                    }

                    #region Fill in between for both cases
                    GCTraysLifecycleStatusTypes lastOfCurrent = null;
                    GCTraysLifecycleStatusTypes firstOfCurrent = null;
                    GCTraysLifecycleStatusTypes firstOfPrevious = null;
                    GCTraysLifecycleStatusTypes lastOfPrevious = null;
                    DateTime latestTimestamp = DateTime.MinValue;
                    DateTime earliestTimestamp = DateTime.MinValue;
                    PopulateEarliestAndSatestTimestamp(gcTray, previousGCTrayOnPreviousCase, out lastOfCurrent, out firstOfCurrent, ref firstOfPrevious, ref lastOfPrevious, ref latestTimestamp, ref earliestTimestamp);

                    if (latestTimestamp != DateTime.MinValue && earliestTimestamp != DateTime.MinValue)
                    {
                        var hiBetween = _context.CTContainerTypeActualHistoryItems
                                .Include(x => x.CTLocation)
                                    .ThenInclude(x => x.CTLocationType)
                                        .ThenInclude(x => x.CTLocationTypeGCLifecycleStatusTypes)
                                .Where(x => x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value &&
                                    (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG ||
                                    x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER) &&
                                    x.UpdateTimestamp > earliestTimestamp && x.UpdateTimestamp < latestTimestamp)
                                .ToList();

                        hiBetween.ForEach(hi =>
                        {
                            // need to recalculate first & last timestamp after each iteration:
                            PopulateEarliestAndSatestTimestamp(gcTray, previousGCTrayOnPreviousCase, out lastOfCurrent, out firstOfCurrent, ref firstOfPrevious, ref lastOfPrevious, ref latestTimestamp, ref earliestTimestamp);

                            var hilcList = hi.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes;
                            var hilcId = hilcList.First().GCLifecycleStatusTypeId;

                            GCTraysLifecycleStatusTypes lcToAdd = null;
                            bool isAllCasesTested = false;

                            switch ((LifecycleStatusType)hilcId)
                            {
                                case LifecycleStatusType.DeconStaged:
                                case LifecycleStatusType.Sink:
                                case LifecycleStatusType.Washer:
                                case LifecycleStatusType.AssemblyStaged:
                                case LifecycleStatusType.CleanIncomplete:
                                case LifecycleStatusType.QualityHold:
                                case LifecycleStatusType.CleanStaged:
                                    if (previousGCTrayOnPreviousCase != null)
                                    {
                                        lcToAdd = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                        y.GCTrayId == previousGCTrayOnPreviousCase.Id &&
                                                        y.LifecycleStatusTypeId == hilcId
                                                        &&
                                                        y.Timestamp > earliestTimestamp &&
                                                        y.Timestamp < latestTimestamp
                                                        ).FirstOrDefault();

                                        if (lcToAdd == null)
                                        {
                                            if (!TrayLifeCycleAlreadyExists(previousGCTrayOnPreviousCase.Id, hilcId))
                                            {
                                                previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                                {
                                                    GCTrayId = previousGCTrayOnPreviousCase.Id,
                                                    IsActiveStatus = false,
                                                    LifecycleStatusTypeId = hilcId,
                                                    Timestamp = hi.UpdateTimestamp,
                                                    TimestampOriginal = hi.UpdateTimestampOriginal,
                                                    User = hi.UpdateUserId
                                                });
                                                _context.SaveChanges();
                                            }
                                        }
                                    }
                                    break;
                                case LifecycleStatusType.Sterilize:
                                case LifecycleStatusType.SterileStaged:
                                case LifecycleStatusType.CaseAssembly:
                                case LifecycleStatusType.TransportationStaging:
                                    lcToAdd = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                        y.GCTrayId == gcTray.Id &&
                                                        y.LifecycleStatusTypeId == hilcId
                                                        &&
                                                        y.Timestamp > earliestTimestamp &&
                                                        y.Timestamp < latestTimestamp
                                                        ).FirstOrDefault();
                                    if (lcToAdd == null)
                                    {
                                        if (!TrayLifeCycleAlreadyExists(gcTray.Id, hilcId))
                                        {
                                            gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                            {
                                                GCTrayId = gcTray.Id,
                                                IsActiveStatus = false,
                                                LifecycleStatusTypeId = hilcId,
                                                Timestamp = hi.UpdateTimestamp,
                                                TimestampOriginal = hi.UpdateTimestampOriginal,
                                                User = hi.UpdateUserId
                                            });
                                            _context.SaveChanges();
                                        }
                                    }
                                    break;
                            }
                        });
                    }

                    #endregion

                    #region Previous Tray - look for previous
                    if (firstLC != null && firstLC.LifecycleStatusTypeId != (int)LifecycleStatusType.DeconStaged)
                    {
                        var historyItemsBeforeFirst = historyItemsForActual.Where(x =>
                            x.UpdateTimestamp < firstLC.Timestamp).OrderByDescending(x => x.UpdateTimestamp).Take(8).ToList();

                        if (historyItemsBeforeFirst != null && historyItemsBeforeFirst.Count > 0)
                        {
                            historyItemsBeforeFirst.ForEach(x =>
                            {
                                var previousHIItemLifeCycles = x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes;
                                var previousHIItemLifeCycleId = previousHIItemLifeCycles.First().GCLifecycleStatusTypeId;

                                if (previousHIItemLifeCycleId != firstLC.LifecycleStatusTypeId)
                                {
                                    GCTraysLifecycleStatusTypes itemAlreadyCreatedInPreviousCase = null;
                                    bool isAllCasesTested = false;

                                    switch ((LifecycleStatusType)previousHIItemLifeCycleId)
                                    {
                                        case LifecycleStatusType.DeconStaged:
                                        case LifecycleStatusType.Sink:
                                        case LifecycleStatusType.Washer:
                                        case LifecycleStatusType.AssemblyStaged:
                                        case LifecycleStatusType.CleanIncomplete:
                                        case LifecycleStatusType.QualityHold:
                                        case LifecycleStatusType.CleanStaged:
                                            if (x.UpdateTimestamp > firstLC.Timestamp && previousGCTrayOnPreviousCase != null)
                                            {
                                                var timestampToUse = firstLC.Timestamp;
                                                if (previousTrayFirstLC != null && previousTrayFirstLC.Timestamp > firstLC.Timestamp)
                                                {
                                                    timestampToUse = previousTrayFirstLC.Timestamp;
                                                }
                                                itemAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                        y.GCTrayId == previousGCTrayOnPreviousCase.Id &&
                                                        y.LifecycleStatusTypeId == previousHIItemLifeCycleId &&
                                                        y.Timestamp >= timestampToUse
                                                        ).FirstOrDefault();
                                                isAllCasesTested = true;
                                            }
                                            break;
                                        case LifecycleStatusType.Assembly:
                                        case LifecycleStatusType.Verification:
                                            if (previousGCTrayOnPreviousCase != null)
                                            {
                                                var previousItemOfAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                    y.GCTrayId == previousGCTrayOnPreviousCase.Id
                                                    && y.Timestamp < x.UpdateTimestamp
                                                    ).OrderByDescending(x => x.Timestamp).FirstOrDefault();

                                                if (previousItemOfAlreadyCreatedInPreviousCase != null && previousItemOfAlreadyCreatedInPreviousCase.LifecycleStatusTypeId != (int)LifecycleStatusType.CleanStaged)
                                                {
                                                    previousHIItemLifeCycleId = (int)LifecycleStatusType.Assembly;

                                                    itemAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                    y.GCTrayId == previousGCTrayOnPreviousCase.Id &&
                                                    y.LifecycleStatusTypeId == previousHIItemLifeCycleId
                                                    ).FirstOrDefault();
                                                    isAllCasesTested = true;
                                                }
                                            }
                                            break;
                                    }

                                    if (isAllCasesTested && itemAlreadyCreatedInPreviousCase == null)
                                    {
                                        if (!TrayLifeCycleAlreadyExists(previousGCTrayOnPreviousCase.Id, previousHIItemLifeCycleId))
                                        {
                                            previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                            {
                                                GCTrayId = previousGCTrayOnPreviousCase.Id,
                                                IsActiveStatus = false,
                                                LifecycleStatusTypeId = previousHIItemLifeCycleId,
                                                Timestamp = x.UpdateTimestamp,
                                                TimestampOriginal = x.UpdateTimestampOriginal,
                                                User = x.UpdateUserId
                                            });
                                            _context.SaveChanges();
                                        }
                                    }
                                }
                            });
                        }
                    }
                    #endregion

                    #region Current Tray - look for previous
                    //if (gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
                    //{
                    if (firstLC != null)
                    {
                        var historyItemsBeforeFirst = historyItemsForActual.Where(x =>
                        x.UpdateTimestamp < firstLC.Timestamp).OrderByDescending(x => x.UpdateTimestamp).Take(4).ToList();

                        if (historyItemsBeforeFirst != null && historyItemsBeforeFirst.Count > 0)
                        {
                            historyItemsBeforeFirst.ForEach(x =>
                            {
                                var previousHIItemLifeCycles = x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes;
                                var previousHIItemLifeCycleId = previousHIItemLifeCycles.First().GCLifecycleStatusTypeId;

                                if (previousHIItemLifeCycleId != firstLC.LifecycleStatusTypeId)
                                {
                                    GCTraysLifecycleStatusTypes itemAlreadyCreatedInPreviousCase = null;
                                    bool isAllCasesTested = false;

                                    switch ((LifecycleStatusType)previousHIItemLifeCycleId)
                                    {
                                        case LifecycleStatusType.SterileStaged:
                                        case LifecycleStatusType.Sterilize:
                                            if (previousGCTrayOnPreviousCase != null)
                                            {
                                                itemAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                    y.GCTrayId == previousGCTrayOnPreviousCase.Id &&
                                                    y.LifecycleStatusTypeId == previousHIItemLifeCycleId
                                                    //&& y.Timestamp == x.UpdateTimestamp
                                                    ).FirstOrDefault();
                                                isAllCasesTested = true;
                                            }
                                            break;
                                        case LifecycleStatusType.Verification:
                                        case LifecycleStatusType.Assembly:
                                            if (previousGCTrayOnPreviousCase != null)
                                            {
                                                var previousItemOfAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                y.GCTrayId == previousGCTrayOnPreviousCase.Id
                                                // && y.LifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged
                                                && y.Timestamp < x.UpdateTimestamp
                                                ).OrderByDescending(x => x.Timestamp).FirstOrDefault();

                                                if (previousItemOfAlreadyCreatedInPreviousCase != null && previousItemOfAlreadyCreatedInPreviousCase.LifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged)
                                                {
                                                    previousHIItemLifeCycleId = (int)LifecycleStatusType.Verification;

                                                    itemAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                    y.GCTrayId == gcTray.Id &&
                                                    y.LifecycleStatusTypeId == previousHIItemLifeCycleId
                                                    //&& y.Timestamp == x.UpdateTimestamp
                                                    ).FirstOrDefault();
                                                    isAllCasesTested = true;
                                                }
                                            }
                                            else // is it in induction
                                            {
                                                var inductionTray = _context.GCTrays.Include(x => x.GCTraysLifecycleStatusTypes).Where(y =>
                                                    y.GCCaseId == null &&
                                                    y.CTContainerTypeActualId == gcTray.CTContainerTypeActualId
                                                ).FirstOrDefault();
                                                if(inductionTray != null && inductionTray.GCTraysLifecycleStatusTypes != null && inductionTray.GCTraysLifecycleStatusTypes.Count > 0)
                                                {
                                                    var inductionLastLC = inductionTray.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).FirstOrDefault();
                                                    if(inductionLastLC != null && inductionLastLC.LifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged)
                                                    {
                                                        previousHIItemLifeCycleId = (int)LifecycleStatusType.Verification;
                                                        itemAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                        y.GCTrayId == gcTray.Id &&
                                                        y.LifecycleStatusTypeId == previousHIItemLifeCycleId
                                                        ).FirstOrDefault();
                                                        isAllCasesTested = true;
                                                    }
                                                }
                                            }
                                            break;
                                    }

                                    if (isAllCasesTested && itemAlreadyCreatedInPreviousCase == null)
                                    {
                                        if (!TrayLifeCycleAlreadyExists(gcTray.Id, previousHIItemLifeCycleId))
                                        {
                                            gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                            {
                                                GCTrayId = gcTray.Id,
                                                IsActiveStatus = false,
                                                LifecycleStatusTypeId = previousHIItemLifeCycleId,
                                                Timestamp = x.UpdateTimestamp,
                                                TimestampOriginal = x.UpdateTimestampOriginal,
                                                User = x.UpdateUserId
                                            });
                                            _context.SaveChanges();
                                        }
                                    }
                                }
                            });
                        }
                    }
                    //}
                    #endregion

                    #region Previous Tray - look for latter
                    if (lastLC != null && lastLC.LifecycleStatusTypeId != (int)LifecycleStatusType.DeconStaged)
                    {
                        var historyItemsAfter = historyItemsForActual.Where(x =>
                            x.UpdateTimestamp > lastLC.Timestamp).OrderBy(x => x.UpdateTimestamp).Take(8).ToList();

                        if (historyItemsAfter != null && historyItemsAfter.Count > 0)
                        {
                            historyItemsAfter.ForEach(x =>
                            {
                                var nextHIItemLifeCycles = x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes;
                                var nextHIItemLifeCycleId = nextHIItemLifeCycles.First().GCLifecycleStatusTypeId;

                                if (nextHIItemLifeCycleId != lastLC.LifecycleStatusTypeId)
                                {
                                    GCTraysLifecycleStatusTypes itemAlreadyCreatedInPreviousCase = null;
                                    bool isAllCasesTested = false;

                                    switch ((LifecycleStatusType)nextHIItemLifeCycleId)
                                    {
                                        //case LifecycleStatusType.DeconStaged:
                                        case LifecycleStatusType.Sink:
                                        case LifecycleStatusType.Washer:
                                        case LifecycleStatusType.AssemblyStaged:
                                        case LifecycleStatusType.CleanIncomplete:
                                        case LifecycleStatusType.QualityHold:
                                        case LifecycleStatusType.CleanStaged:
                                            if (previousGCTrayOnPreviousCase != null && previousTrayFirstLC != null)
                                            {
                                                itemAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                    y.GCTrayId == previousGCTrayOnPreviousCase.Id &&
                                                    y.LifecycleStatusTypeId == nextHIItemLifeCycleId
                                                    && y.Timestamp >= previousTrayFirstLC.Timestamp
                                                    ).FirstOrDefault();
                                                isAllCasesTested = true;
                                            }
                                            break;
                                        case LifecycleStatusType.Assembly:
                                        case LifecycleStatusType.Verification:
                                            if (previousGCTrayOnPreviousCase != null && previousTrayFirstLC != null)
                                            {
                                                var previousItemOfAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                    y.GCTrayId == previousGCTrayOnPreviousCase.Id
                                                    // && y.LifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged
                                                    && y.Timestamp < x.UpdateTimestamp
                                                    ).OrderByDescending(x => x.Timestamp).FirstOrDefault();

                                                if (previousItemOfAlreadyCreatedInPreviousCase != null && previousItemOfAlreadyCreatedInPreviousCase.LifecycleStatusTypeId != (int)LifecycleStatusType.CleanStaged)
                                                {
                                                    nextHIItemLifeCycleId = (int)LifecycleStatusType.Assembly;

                                                    itemAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                    y.GCTrayId == previousGCTrayOnPreviousCase.Id &&
                                                    y.LifecycleStatusTypeId == nextHIItemLifeCycleId
                                                    && y.Timestamp >= previousTrayFirstLC.Timestamp
                                                    ).FirstOrDefault();
                                                    isAllCasesTested = true;
                                                }
                                            }
                                            break;
                                    }

                                    if (isAllCasesTested && itemAlreadyCreatedInPreviousCase == null)
                                    {
                                        if (!TrayLifeCycleAlreadyExists(previousGCTrayOnPreviousCase.Id, nextHIItemLifeCycleId))
                                        {
                                            previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                            {
                                                GCTrayId = previousGCTrayOnPreviousCase.Id,
                                                IsActiveStatus = false,
                                                LifecycleStatusTypeId = nextHIItemLifeCycleId,
                                                Timestamp = x.UpdateTimestamp,
                                                TimestampOriginal = x.UpdateTimestampOriginal,
                                                User = x.UpdateUserId
                                            });
                                            _context.SaveChanges();
                                        }
                                    }
                                }
                            });
                        }
                    }
                    #endregion

                    #region Current Tray - look for latter
                    //if (gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
                    //{
                    if (lastLC != null)
                    {
                        var historyItemsAfterLast = historyItemsForActual.Where(x =>
                        x.UpdateTimestamp > lastLC.Timestamp).OrderBy(x => x.UpdateTimestamp).Take(5).ToList();

                        if (historyItemsAfterLast != null && historyItemsAfterLast.Count > 0)
                        {
                            historyItemsAfterLast.ForEach(x =>
                            {
                                var nextHIItemLifeCycles = x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes;
                                var nextHIItemLifeCycleId = nextHIItemLifeCycles.First().GCLifecycleStatusTypeId;

                                if (nextHIItemLifeCycleId != lastLC.LifecycleStatusTypeId)
                                {
                                    GCTraysLifecycleStatusTypes itemAlreadyCreatedInPreviousCase = null;
                                    bool isAllCasesTested = false;

                                    switch ((LifecycleStatusType)nextHIItemLifeCycleId)
                                    {
                                        case LifecycleStatusType.Sterilize:
                                        case LifecycleStatusType.SterileStaged:
                                        case LifecycleStatusType.CaseAssembly:
                                        case LifecycleStatusType.TransportationStaging:
                                            if (gcTray != null)
                                            {
                                                itemAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                    y.GCTrayId == gcTray.Id &&
                                                    y.LifecycleStatusTypeId == nextHIItemLifeCycleId
                                                    ).FirstOrDefault();
                                                isAllCasesTested = true;
                                            }
                                            break;
                                        case LifecycleStatusType.Verification:
                                        case LifecycleStatusType.Assembly:
                                            if (gcTray != null)
                                            {
                                                var previousItemOfAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                y.GCTrayId == gcTray.Id
                                                && y.Timestamp < x.UpdateTimestamp
                                                ).OrderByDescending(x => x.Timestamp).FirstOrDefault();

                                                if (previousItemOfAlreadyCreatedInPreviousCase != null && previousItemOfAlreadyCreatedInPreviousCase.LifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged)
                                                {
                                                    nextHIItemLifeCycleId = (int)LifecycleStatusType.Verification;

                                                    itemAlreadyCreatedInPreviousCase = _context.GCTraysLifecycleStatusTypes.Where(y =>
                                                    y.GCTrayId == previousGCTrayOnPreviousCase.Id &&
                                                    y.LifecycleStatusTypeId == nextHIItemLifeCycleId
                                                    ).FirstOrDefault();
                                                    isAllCasesTested = true;
                                                }
                                            }
                                            break;
                                    }

                                    if (isAllCasesTested && itemAlreadyCreatedInPreviousCase == null)
                                    {
                                        if (!TrayLifeCycleAlreadyExists(gcTray.Id, nextHIItemLifeCycleId))
                                        {
                                            gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                            {
                                                GCTrayId = gcTray.Id,
                                                IsActiveStatus = false,
                                                LifecycleStatusTypeId = nextHIItemLifeCycleId,
                                                Timestamp = x.UpdateTimestamp,
                                                TimestampOriginal = x.UpdateTimestampOriginal,
                                                User = x.UpdateUserId
                                            });
                                            _context.SaveChanges();
                                        }
                                    }
                                }
                            });
                        }
                    }
                    //}
                    #endregion
                }

                if (previousGCTrayOnPreviousCase != null && previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes != null && previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes.Count > 0)
                {
                    var previousGCTrayLCs = previousGCTrayOnPreviousCase.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).ToList();
                    for (int i = 0; i < previousGCTrayLCs.Count; i++)
                    {
                        var currentLC = previousGCTrayLCs[i];
                        currentLC.IsActiveStatus = i == 0;
                    }
                    _context.SaveChanges();
                }

                if (gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
                {
                    var gcTrayLCs = gcTray.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).ToList();
                    for (int i = 0; i < gcTrayLCs.Count; i++)
                    {
                        var currentLC = gcTrayLCs[i];
                        currentLC.IsActiveStatus = i == 0;
                    }
                    _context.SaveChanges();
                }

                // Check if Tray now has all posible CT Statuses:
                var isTrayHistoryComplete = ProfitOptics.Modules.Sox.Helpers.Helpers.DoesGCTrayHaveAllCTStatuses(gcTray);
                if (isTrayHistoryComplete)
                {
                    gcTray.IsTrayHistoryComplete = isTrayHistoryComplete;
                    _context.SaveChanges();
                }
            }
        }

        private void CompleteBrokenHistoryForGCTray_V3(GCTray gcTray, int minsToAdd)
        {
            var historyItemsForActual = _context.CTContainerTypeActualHistoryItems
                   .Include(x => x.CTLocation)
                       .ThenInclude(x => x.CTLocationType)
                           .ThenInclude(x => x.CTLocationTypeGCLifecycleStatusTypes)
                   .Where(x => gcTray.CTContainerTypeActualId.HasValue && x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value &&
                        (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG ||
                           x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER))
                   .OrderBy(x => x.UpdateTimestamp)
                   .ToList();

            if(historyItemsForActual != null && historyItemsForActual.Count > 0)
            {
                historyItemsForActual.ForEach(hi =>
                {
                    if (hi.CTContainerTypeActualId == 0)
                    {
                        hi.CTContainerTypeActualId = gcTray.CTContainerTypeActualId.Value;
                    }
                    var isLoaner = gcTray.IsLoaner.HasValue && gcTray.IsLoaner.Value;
                    UpdateHistoryItemAndLapCount(hi, minsToAdd, isLoaner, true);
                });
            }


            var historyItemsForActualAndLapCount = _context.CTContainerTypeActualHistoryItems
                   .Include(x => x.CTLocation)
                       .ThenInclude(x => x.CTLocationType)
                           .ThenInclude(x => x.CTLocationTypeGCLifecycleStatusTypes)
                   .Where(x => gcTray.CTContainerTypeActualId.HasValue && x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value &&
                        x.ActualLapCount.HasValue && gcTray.ActualLapCount.HasValue && x.ActualLapCount.Value == gcTray.ActualLapCount.Value &&
                        (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG ||
                           x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER))
                   .OrderBy(x => x.UpdateTimestamp)
                   .ToList();

            if (historyItemsForActualAndLapCount != null && historyItemsForActualAndLapCount.Count > 0)
            {
                if (gcTray.GCTraysLifecycleStatusTypes == null)
                {
                    gcTray.GCTraysLifecycleStatusTypes = new List<GCTraysLifecycleStatusTypes>();
                }

                // Every item need to have LocationID, confirming, and correcting now:
                historyItemsForActualAndLapCount.ForEach(hi =>
                {
                    bool isCurrentLoanerCheckinLocation = hi.LocationElapsedCase != null && hi.LocationElapsedCase.Equals(Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION);
                    bool isCurrentLoanerCheckoutLocation = hi.LocationElapsedCase != null && hi.LocationElapsedCase.Equals(Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION);

                    CTLocation ctLocation = null;

                    if (hi.CTLocationId == null)
                    {
                        ctLocation = CorrectingNonExistingLocationForActualHistoryItem(hi);
                        if (ctLocation != null)
                        {
                            hi.CTLocationId = ctLocation.Id;
                            hi.CTLocation = ctLocation;
                        }
                    }

                    if (isCurrentLoanerCheckinLocation ||
                        isCurrentLoanerCheckoutLocation ||
                        (hi.CTLocationId != null && hi.CTLocation != null && hi.CTLocation.CTLocationType != null &&
                        hi.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null && hi.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.Count > 0))
                    {
                        int lifecycleId = 0;

                        if (isCurrentLoanerCheckinLocation)
                        {
                            lifecycleId = (int)LifecycleStatusType.LoanerInbound;
                        }
                        else if (isCurrentLoanerCheckoutLocation)
                        {
                            lifecycleId = (int)LifecycleStatusType.LoanerOutbound;
                        }
                        else
                        {
                            lifecycleId = hi.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First().GCLifecycleStatusTypeId;
                        }

                        if (lifecycleId > 0)
                        {
                            if (lifecycleId == (int)LifecycleStatusType.Assembly || lifecycleId == (int)LifecycleStatusType.Verification)
                            {
                                var previousHI = historyItemsForActualAndLapCount.Where(x => x.UpdateTimestamp < hi.UpdateTimestamp).OrderByDescending(x => x.UpdateTimestamp).FirstOrDefault();
                                var nextHI = historyItemsForActualAndLapCount.Where(x => x.UpdateTimestamp > hi.UpdateTimestamp).OrderBy(x => x.UpdateTimestamp).FirstOrDefault();
                                if (previousHI != null && previousHI.CTLocation != null && previousHI.CTLocation.CTLocationType != null &&
                                        previousHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null && previousHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.Count > 0)
                                {
                                    var prevLifecycleId = previousHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First().GCLifecycleStatusTypeId;
                                    if (prevLifecycleId == (int)LifecycleStatusType.CleanStaged)
                                    {
                                        lifecycleId = (int)LifecycleStatusType.Verification;
                                    }
                                    else if (nextHI != null && nextHI.CTLocation != null && nextHI.CTLocation.CTLocationType != null &&
                                        nextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null &&
                                        nextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.Count > 0 &&
                                        nextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First().GCLifecycleStatusTypeId == (int)LifecycleStatusType.Sterilize)
                                    {
                                        lifecycleId = (int)LifecycleStatusType.Verification;
                                    }
                                    else
                                    {
                                        lifecycleId = (int)LifecycleStatusType.Assembly;
                                    }
                                }
                            }

                            if (gcTray.IsLoaner.HasValue && gcTray.IsLoaner.Value)
                            {
                                bool isTrayPassedInduction = false;

                                switch ((LifecycleStatusType)lifecycleId)
                                {
                                    case LifecycleStatusType.DeconStaged:
                                    case LifecycleStatusType.Sink:
                                    case LifecycleStatusType.Washer:
                                    case LifecycleStatusType.AssemblyStaged:
                                    case LifecycleStatusType.Assembly:
                                        var cleanStageHI = historyItemsForActualAndLapCount.Where(x =>
                                            x.CTLocation != null &&
                                            x.CTLocation.CTLocationType != null &&
                                            x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null &&
                                            x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First().GCLifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged).FirstOrDefault();
                                        if (cleanStageHI != null)
                                        {
                                            isTrayPassedInduction = hi.UpdateTimestamp > cleanStageHI.UpdateTimestamp;
                                        }
                                        else
                                        {
                                            var sterilizeHI = historyItemsForActualAndLapCount.Where(x =>
                                                x.CTLocation != null &&
                                                x.CTLocation.CTLocationType != null &&
                                                x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null &&
                                                x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First().GCLifecycleStatusTypeId == (int)LifecycleStatusType.Sterilize).FirstOrDefault();
                                            if (sterilizeHI != null)
                                            {
                                                isTrayPassedInduction = hi.UpdateTimestamp > sterilizeHI.UpdateTimestamp;
                                            }
                                            else
                                            {
                                                var sterileStageHI = historyItemsForActualAndLapCount.Where(x =>
                                                    x.CTLocation != null &&
                                                    x.CTLocation.CTLocationType != null &&
                                                    x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null &&
                                                    x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First().GCLifecycleStatusTypeId == (int)LifecycleStatusType.SterileStaged).FirstOrDefault();
                                                if (sterileStageHI != null)
                                                {
                                                    isTrayPassedInduction = hi.UpdateTimestamp > sterileStageHI.UpdateTimestamp;
                                                }
                                                else
                                                {
                                                    var deliveredHI = historyItemsForActualAndLapCount.Where(x =>
                                                        x.CTLocation != null &&
                                                        x.CTLocation.CTLocationType != null &&
                                                        x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null &&
                                                        x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First().GCLifecycleStatusTypeId == (int)LifecycleStatusType.Delivered).FirstOrDefault();
                                                    if (deliveredHI != null)
                                                    {
                                                        isTrayPassedInduction = hi.UpdateTimestamp > deliveredHI.UpdateTimestamp;
                                                    }
                                                    else
                                                    {
                                                        var receivedHI = historyItemsForActualAndLapCount.Where(x =>
                                                            x.CTLocation != null &&
                                                            x.CTLocation.CTLocationType != null &&
                                                            x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null &&
                                                            x.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First().GCLifecycleStatusTypeId == (int)LifecycleStatusType.Received).FirstOrDefault();
                                                        if (receivedHI != null)
                                                        {
                                                            isTrayPassedInduction = hi.UpdateTimestamp > receivedHI.UpdateTimestamp;
                                                        }
                                                        else
                                                        {
                                                            var message = "Cannot find any HistoryItems";
                                                            Log.Error(message);
                                                            throw new Exception(message);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                }

                                if (!isTrayPassedInduction)
                                {
                                    switch ((LifecycleStatusType)lifecycleId)
                                    {
                                        case LifecycleStatusType.DeconStaged:
                                            lifecycleId = (int)LifecycleStatusType.DeconStagedLoaner;
                                            break;
                                        case LifecycleStatusType.Sink:
                                            lifecycleId = (int)LifecycleStatusType.SinkLoaner;
                                            break;
                                        case LifecycleStatusType.Washer:
                                            lifecycleId = (int)LifecycleStatusType.WasherLoaner;
                                            break;
                                        case LifecycleStatusType.AssemblyStaged:
                                            lifecycleId = (int)LifecycleStatusType.AssemblyStagedLoaner;
                                            break;
                                        case LifecycleStatusType.Assembly:
                                            lifecycleId = (int)LifecycleStatusType.AssemblyLoaner;
                                            break;
                                    }
                                }
                            }

                            var traysLifecycle = gcTray.GCTraysLifecycleStatusTypes.Where(x => x.LifecycleStatusTypeId == lifecycleId).FirstOrDefault();
                            if (traysLifecycle == null)
                            {
                                gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                {
                                    GCTrayId = gcTray.Id,
                                    IsActiveStatus = false,
                                    LifecycleStatusTypeId = lifecycleId,
                                    Timestamp = hi.UpdateTimestamp,
                                    TimestampOriginal = hi.UpdateTimestampOriginal,
                                    User = hi.UpdateUserId
                                });
                                _context.SaveChanges();
                            }
                        }
                    }
                });
            }
        }

        public GCTraysLifecycleStatusTypes GetGCTraysLifeCycleForId(int trayLifecycleId)
        {
            return _context.GCTraysLifecycleStatusTypes.Find(trayLifecycleId);
        }

        private bool CompleteBrokenHistoryForGCTray_V2(GCTray gcTray)
        {
            if(gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
            {
                var historyItemsForActual = _context.CTContainerTypeActualHistoryItems
                   .Include(x => x.CTLocation)
                       .ThenInclude(x => x.CTLocationType)
                           .ThenInclude(x => x.CTLocationTypeGCLifecycleStatusTypes)
                   .Where(x => gcTray.CTContainerTypeActualId.HasValue && x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value &&
                       (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG ||
                           x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER))
                   .ToList();

                if (historyItemsForActual != null && historyItemsForActual.Count > 0)
                {
                    // Every item need to have LocationID, confirming, and correcting now:
                    historyItemsForActual.ForEach(hi =>
                    {
                        if (hi.CTLocationId == null)
                        {
                            CorrectingNonExistingLocationForActualHistoryItem(hi);
                        }
                    });

                    if (gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
                    {
                        foreach (GCTraysLifecycleStatusTypes lc in gcTray.GCTraysLifecycleStatusTypes)
                        {
                            bool isNextLCMissing = false;
                            bool isNextHICorrect = false;

                            bool isPreviousLCMissing = false;
                            bool isPreviousHICorrect = false;

                            bool lookIntoPreviousTray = false;
                            bool lookIntoNextTray = false;

                            var currentHI = historyItemsForActual.Where(x => x.UpdateTimestamp == lc.Timestamp).FirstOrDefault();
                            if (currentHI != null)
                            {
                                var nextHI = historyItemsForActual.Where(x => x.UpdateTimestamp > currentHI.UpdateTimestamp).OrderBy(x => x.UpdateTimestamp).FirstOrDefault();
                                var nextExpectedLC = 0;

                                var previousHI = historyItemsForActual.Where(x => x.UpdateTimestamp < currentHI.UpdateTimestamp).OrderByDescending(x => x.UpdateTimestamp).FirstOrDefault();
                                var previousExpectedLC = 0;

                                GCTraysLifecycleStatusTypes nextLC = null;
                                GCTraysLifecycleStatusTypes previousLC = null;

                                switch ((LifecycleStatusType)lc.LifecycleStatusTypeId)
                                {
                                    case LifecycleStatusType.Verification:
                                        nextExpectedLC = (int)LifecycleStatusType.Sterilize;
                                        previousExpectedLC = (int)LifecycleStatusType.CleanStaged;
                                        lookIntoPreviousTray = true;
                                        break;
                                    case LifecycleStatusType.Sterilize:
                                        nextExpectedLC = (int)LifecycleStatusType.SterileStaged;
                                        previousExpectedLC = (int)LifecycleStatusType.Verification;
                                        break;
                                    case LifecycleStatusType.SterileStaged:
                                        nextExpectedLC = (int)LifecycleStatusType.CaseAssembly;
                                        previousExpectedLC = (int)LifecycleStatusType.Sterilize;
                                        break;
                                    case LifecycleStatusType.CaseAssembly:
                                        nextExpectedLC = (int)LifecycleStatusType.TransportationStaging;
                                        previousExpectedLC = (int)LifecycleStatusType.SterileStaged;
                                        break;
                                    //case LifecycleStatusType.ShipmentStaged:
                                    //    break;
                                    //case LifecycleStatusType.Loaded:
                                    //    break;
                                    //case LifecycleStatusType.ShippedOutbound:
                                    //    break;
                                    //case LifecycleStatusType.Delivered:
                                    //    break;
                                    //case LifecycleStatusType.InSurgery:
                                    //    break;
                                    //case LifecycleStatusType.SurgeryComplete:
                                    //    break;
                                    //case LifecycleStatusType.Prepped:
                                    //    break;
                                    //case LifecycleStatusType.Picked:
                                    //    break;
                                    //case LifecycleStatusType.ShippedInbound:
                                    //    break;
                                    //case LifecycleStatusType.Received:
                                    //    break;
                                    case LifecycleStatusType.DeconStaged:
                                        nextExpectedLC = (int)LifecycleStatusType.Sink;
                                        break;
                                    case LifecycleStatusType.Sink:
                                        nextExpectedLC = (int)LifecycleStatusType.Washer;
                                        previousExpectedLC = (int)LifecycleStatusType.DeconStaged;
                                        break;
                                    case LifecycleStatusType.Washer:
                                        nextExpectedLC = (int)LifecycleStatusType.AssemblyStaged;
                                        previousExpectedLC = (int)LifecycleStatusType.Sink;
                                        break;
                                    case LifecycleStatusType.AssemblyStaged:
                                        nextExpectedLC = (int)LifecycleStatusType.Assembly;
                                        previousExpectedLC = (int)LifecycleStatusType.Washer;
                                        break;
                                    case LifecycleStatusType.Assembly:
                                        nextExpectedLC = (int)LifecycleStatusType.CleanStaged;
                                        previousExpectedLC = (int)LifecycleStatusType.AssemblyStaged;
                                        break;
                                    //case LifecycleStatusType.CleanIncomplete:
                                    //    break;
                                    //case LifecycleStatusType.Quarantine:
                                    //    break;
                                    case LifecycleStatusType.CleanStaged:
                                        nextExpectedLC = (int)LifecycleStatusType.Verification;
                                        lookIntoNextTray = true;
                                        previousExpectedLC = (int)LifecycleStatusType.Assembly;
                                        break;
                                        //case LifecycleStatusType.LoanerInbound:
                                        //    break;
                                        //case LifecycleStatusType.DeconStagedLoaner:
                                        //    break;
                                        //case LifecycleStatusType.SinkLoaner:
                                        //    break;
                                        //case LifecycleStatusType.WasherLoaner:
                                        //    break;
                                        //case LifecycleStatusType.AssemblyStagedLoaner:
                                        //    break;
                                        //case LifecycleStatusType.AssemblyLoaner:
                                        //    break;
                                        //case LifecycleStatusType.LoanerOutbound:
                                        //    break;
                                        //default:
                                        //    break;
                                }

                                if (nextHI != null)
                                {
                                    int? gcCaseId = null;
                                    DateTime dueTimestamp = DateTime.MinValue;
                                    if (gcTray.GCCaseId.HasValue && gcTray.GCCase != null)
                                    {
                                        gcCaseId = gcTray.GCCaseId.Value;
                                        dueTimestamp = gcTray.GCCase.DueTimestamp;
                                    }
                                    else
                                    {
                                        gcCaseId = -1;
                                    }

                                    if (lookIntoNextTray)
                                    {
                                        var nextTray = FetchNextTrayOnNextCase(gcTray.CTContainerTypeActualId.Value, gcCaseId.Value, dueTimestamp, null);
                                        if (nextTray != null)
                                        {
                                            if (nextTray.GCTraysLifecycleStatusTypes != null && nextTray.GCTraysLifecycleStatusTypes.Count > 0)
                                            {
                                                var nextLCOfNextTray = nextTray.GCTraysLifecycleStatusTypes.OrderBy(x => x.Timestamp).FirstOrDefault();
                                                if (nextLCOfNextTray != null && nextLCOfNextTray.LifecycleStatusTypeId != nextExpectedLC)
                                                {
                                                    isNextLCMissing = true;
                                                }
                                            }
                                            else
                                            {
                                                nextTray.GCTraysLifecycleStatusTypes = new List<GCTraysLifecycleStatusTypes>();
                                                isNextLCMissing = true;
                                            }

                                            if (nextHI.CTLocation != null && nextHI.CTLocation.CTLocationType != null && nextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null)
                                            {
                                                var lclt = nextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First();
                                                if (lclt.GCLifecycleStatusTypeId == nextExpectedLC)
                                                {
                                                    isNextHICorrect = true;
                                                }
                                                else if ((nextExpectedLC == (int)LifecycleStatusType.Assembly || nextExpectedLC == (int)LifecycleStatusType.Verification) &&
                                                    (lclt.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Assembly || lclt.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Verification))
                                                {

                                                }
                                            }

                                            if (nextExpectedLC > 0 && isNextLCMissing && isNextHICorrect)
                                            {
                                                var isLCAAlreadyAddedToTray = _context.GCTraysLifecycleStatusTypes.Any(x =>
                                                        x.GCTray.Id == nextTray.Id &&
                                                        x.LifecycleStatusTypeId == nextExpectedLC);

                                                if (!isLCAAlreadyAddedToTray)
                                                {
                                                    nextTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                                    {
                                                        GCTrayId = nextTray.Id,
                                                        IsActiveStatus = false,
                                                        LifecycleStatusTypeId = nextExpectedLC,
                                                        Timestamp = nextHI.UpdateTimestamp,
                                                        TimestampOriginal = nextHI.UpdateTimestampOriginal,
                                                        User = nextHI.UpdateUserId
                                                    });
                                                    _context.SaveChanges();
                                                    return true;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        nextLC = gcTray.GCTraysLifecycleStatusTypes.Where(x => x.Timestamp > lc.Timestamp && lc.LifecycleStatusTypeId == nextExpectedLC).FirstOrDefault();
                                        if (nextLC == null)
                                        {
                                            isNextLCMissing = true;
                                            if (nextHI.CTLocation != null && nextHI.CTLocation.CTLocationType != null && nextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null)
                                            {
                                                var lclt = nextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First();
                                                if (lclt.GCLifecycleStatusTypeId == nextExpectedLC)
                                                {
                                                    isNextHICorrect = true;
                                                }
                                                else if ((nextExpectedLC == (int)LifecycleStatusType.Assembly || nextExpectedLC == (int)LifecycleStatusType.Verification) &&
                                                    (lclt.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Assembly || lclt.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Verification))
                                                {

                                                }
                                            }
                                        }

                                        if (nextExpectedLC > 0 && isNextLCMissing && isNextHICorrect)
                                        {
                                            var isLCAAlreadyAddedToTray = _context.GCTraysLifecycleStatusTypes.Any(x =>
                                                    x.GCTray.Id == gcTray.Id &&
                                                    x.LifecycleStatusTypeId == nextExpectedLC);

                                            if (!isLCAAlreadyAddedToTray)
                                            {
                                                gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                                {
                                                    GCTrayId = gcTray.Id,
                                                    IsActiveStatus = false,
                                                    LifecycleStatusTypeId = nextExpectedLC,
                                                    Timestamp = nextHI.UpdateTimestamp,
                                                    TimestampOriginal = nextHI.UpdateTimestampOriginal,
                                                    User = nextHI.UpdateUserId
                                                });
                                                _context.SaveChanges();
                                                return true;
                                            }
                                        }
                                    }
                                }

                                if (previousHI != null)
                                {
                                    if (lookIntoPreviousTray)
                                    {
                                        var prevTray = FetchPreviousTrayOnPreviousCase(gcTray.CTContainerTypeActualId.Value, gcTray.GCCaseId.Value, gcTray.GCCase.DueTimestamp, null);
                                        if (prevTray != null)
                                        {
                                            if (prevTray.GCTraysLifecycleStatusTypes != null && prevTray.GCTraysLifecycleStatusTypes.Count > 0)
                                            {
                                                var prevLCOfPrevTray = prevTray.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).FirstOrDefault();
                                                if (prevLCOfPrevTray != null && prevLCOfPrevTray.LifecycleStatusTypeId != previousExpectedLC)
                                                {
                                                    isPreviousLCMissing = true;
                                                }
                                            }
                                            else
                                            {
                                                prevTray.GCTraysLifecycleStatusTypes = new List<GCTraysLifecycleStatusTypes>();
                                                isPreviousLCMissing = true;
                                            }

                                            if (previousHI.CTLocation != null && previousHI.CTLocation.CTLocationType != null && previousHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null)
                                            {
                                                var lclt = previousHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First();
                                                if (lclt.GCLifecycleStatusTypeId == previousExpectedLC)
                                                //|| (((LifecycleStatusType)lc.LifecycleStatusTypeId == LifecycleStatusType.Sterilize || (LifecycleStatusType)lc.LifecycleStatusTypeId == LifecycleStatusType.CleanStaged) &&
                                                //((LifecycleStatusType)previousExpectedLC == LifecycleStatusType.Assembly || (LifecycleStatusType)previousExpectedLC == LifecycleStatusType.Verification)))
                                                {
                                                    isPreviousHICorrect = true;
                                                }
                                                else if ((previousExpectedLC == (int)LifecycleStatusType.Assembly || previousExpectedLC == (int)LifecycleStatusType.Verification) &&
                                                    (lclt.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Assembly || lclt.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Verification))
                                                {
                                                    if ((LifecycleStatusType)lc.LifecycleStatusTypeId == LifecycleStatusType.Sterilize)
                                                    {
                                                        isPreviousHICorrect = true;
                                                    }
                                                    else if ((LifecycleStatusType)lc.LifecycleStatusTypeId == LifecycleStatusType.CleanStaged)
                                                    {
                                                        isPreviousHICorrect = true;
                                                    }
                                                }
                                            }

                                            if (previousExpectedLC > 0 && isPreviousLCMissing && isPreviousHICorrect)
                                            {
                                                var isLCAAlreadyAddedToTray = _context.GCTraysLifecycleStatusTypes.Any(x =>
                                                        x.GCTray.Id == prevTray.Id &&
                                                        x.LifecycleStatusTypeId == previousExpectedLC);

                                                if (!isLCAAlreadyAddedToTray)
                                                {
                                                    prevTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                                    {
                                                        GCTrayId = prevTray.Id,
                                                        IsActiveStatus = false,
                                                        LifecycleStatusTypeId = previousExpectedLC,
                                                        Timestamp = nextHI.UpdateTimestamp,
                                                        TimestampOriginal = nextHI.UpdateTimestampOriginal,
                                                        User = nextHI.UpdateUserId
                                                    });
                                                    _context.SaveChanges();
                                                    return true;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        previousLC = gcTray.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).Where(x => x.Timestamp < lc.Timestamp && lc.LifecycleStatusTypeId == previousExpectedLC).FirstOrDefault();
                                        if (previousLC == null)
                                        {
                                            isPreviousLCMissing = true;
                                            if (previousHI.CTLocation != null && previousHI.CTLocation.CTLocationType != null && previousHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null)
                                            {
                                                var lclt = previousHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First();
                                                if (lclt.GCLifecycleStatusTypeId == previousExpectedLC)
                                                //|| (((LifecycleStatusType)lc.LifecycleStatusTypeId == LifecycleStatusType.Sterilize || (LifecycleStatusType)lc.LifecycleStatusTypeId == LifecycleStatusType.CleanStaged) &&
                                                //((LifecycleStatusType)previousExpectedLC == LifecycleStatusType.Assembly || (LifecycleStatusType)previousExpectedLC == LifecycleStatusType.Verification)))
                                                {
                                                    isPreviousHICorrect = true;
                                                }
                                                else if ((previousExpectedLC == (int)LifecycleStatusType.Assembly || previousExpectedLC == (int)LifecycleStatusType.Verification) &&
                                                    (lclt.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Assembly || lclt.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Verification))
                                                {
                                                    if ((LifecycleStatusType)lc.LifecycleStatusTypeId == LifecycleStatusType.Sterilize)
                                                    {
                                                        isPreviousHICorrect = true;
                                                    }
                                                    else if ((LifecycleStatusType)lc.LifecycleStatusTypeId == LifecycleStatusType.CleanStaged)
                                                    {
                                                        isPreviousHICorrect = true;
                                                    }
                                                }
                                            }
                                        }

                                        if (previousExpectedLC > 0 && isPreviousLCMissing && isPreviousHICorrect)
                                        {
                                            var isLCAAlreadyAddedToTray = _context.GCTraysLifecycleStatusTypes.Any(x =>
                                                    x.GCTray.Id == gcTray.Id &&
                                                    x.LifecycleStatusTypeId == previousExpectedLC);

                                            if (!isLCAAlreadyAddedToTray)
                                            {
                                                gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                                {
                                                    GCTrayId = gcTray.Id,
                                                    IsActiveStatus = false,
                                                    LifecycleStatusTypeId = previousExpectedLC,
                                                    Timestamp = previousHI.UpdateTimestamp,
                                                    TimestampOriginal = previousHI.UpdateTimestampOriginal,
                                                    User = previousHI.UpdateUserId
                                                });
                                                _context.SaveChanges();
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else // no LCs trying to re-create from previous tray
                    {
                        if (gcTray.CTContainerTypeActualId.HasValue)
                        {
                            var previousTray = FetchPreviousTrayOnPreviousCase(gcTray.CTContainerTypeActualId.Value, gcTray.GCCaseId, gcTray.GCCase.DueTimestamp, null);
                            if(previousTray != null && previousTray.GCTraysLifecycleStatusTypes != null && previousTray.GCTraysLifecycleStatusTypes.Count > 0)
                            {
                                var lastLCOfPreviousTray = previousTray.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).FirstOrDefault();
                                if(lastLCOfPreviousTray.LifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged)
                                {
                                    var hiAfterCleanStage = historyItemsForActual.Where(x => x.UpdateTimestamp > lastLCOfPreviousTray.Timestamp).OrderBy(x => x.UpdateTimestamp).FirstOrDefault();
                                    if(hiAfterCleanStage != null && hiAfterCleanStage.CTLocation != null && hiAfterCleanStage.CTLocation.CTLocationType != null && hiAfterCleanStage.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null)
                                    {
                                        var nextHILcType = hiAfterCleanStage.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.FirstOrDefault();
                                        if(nextHILcType != null)
                                        {
                                            int lcIdToAdd = 0;

                                            if(nextHILcType.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Sterilize)
                                            {
                                                lcIdToAdd = (int)LifecycleStatusType.Sterilize;
                                            }
                                            else if (nextHILcType.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Assembly || nextHILcType.GCLifecycleStatusTypeId == (int)LifecycleStatusType.Verification)
                                            {
                                                lcIdToAdd = (int)LifecycleStatusType.Verification;
                                            }

                                            if (lcIdToAdd > 0)
                                            {
                                                gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                                {
                                                    GCTrayId = gcTray.Id,
                                                    IsActiveStatus = false,
                                                    LifecycleStatusTypeId = lcIdToAdd,
                                                    Timestamp = hiAfterCleanStage.UpdateTimestamp,
                                                    TimestampOriginal = hiAfterCleanStage.UpdateTimestampOriginal,
                                                    User = hiAfterCleanStage.UpdateUserId
                                                });
                                                _context.SaveChanges();
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        private bool TrayLifeCycleAlreadyExists(int gcTrayId, int gcLifeCycleTypeId)
        {
            return _context.GCTraysLifecycleStatusTypes.Any(x => x.GCTrayId == gcTrayId && x.LifecycleStatusTypeId == gcLifeCycleTypeId);
        }

        public void AddGCTrayActualHistory(GCTrayActualHistory gcTrayActualHistory)
        {
            _context.GCTrayActualHistorys.Add(gcTrayActualHistory);
            _context.SaveChanges();
        }

        private void PopulateEarliestAndSatestTimestamp(GCTray gcTray, GCTray previousGCTrayOnPreviousCase, out GCTraysLifecycleStatusTypes lastOfCurrent, out GCTraysLifecycleStatusTypes firstOfCurrent, ref GCTraysLifecycleStatusTypes firstOfPrevious, ref GCTraysLifecycleStatusTypes lastOfPrevious, ref DateTime latestTimestamp, ref DateTime earliestTimestamp)
        {
            lastOfCurrent = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == gcTray.Id).OrderByDescending(x => x.Timestamp).FirstOrDefault();
            firstOfCurrent = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == gcTray.Id).OrderBy(x => x.Timestamp).FirstOrDefault();

            if (firstOfCurrent != null)
            {
                earliestTimestamp = firstOfCurrent.Timestamp;
            }

            if (previousGCTrayOnPreviousCase != null)
            {
                firstOfPrevious = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == previousGCTrayOnPreviousCase.Id).OrderBy(x => x.Timestamp).FirstOrDefault();
                lastOfPrevious = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == previousGCTrayOnPreviousCase.Id).OrderByDescending(x => x.Timestamp).FirstOrDefault();

                if (firstOfPrevious != null)
                {
                    earliestTimestamp = firstOfPrevious.Timestamp;
                }

                if (lastOfPrevious != null)
                {
                    latestTimestamp = lastOfPrevious.Timestamp;
                }
            }

            if (lastOfCurrent != null)
            {
                latestTimestamp = lastOfCurrent.Timestamp;
            }
        }

        public CTLocation CorrectingNonExistingLocationForActualHistoryItem(CTContainerTypeActualHistoryItem hi)
        {
            Log.Information($"---NO CTLocationId for HistoryItem ID: {hi.Id}, current location: {hi.LocationElapsedCase}, ActualID: {hi.CTContainerTypeActualId}");
            Log.Information($"----Trying to correct now...");

            var ctLocation = _context.CTLocations
                                    .Include(x => x.CTLocationType).ThenInclude(x => x.CTLocationTypeGCLifecycleStatusTypes)
                                    .Where(x => x.LocationName == hi.LocationElapsedCase).FirstOrDefault();

            // Hack for Strerilization location:
            if (ctLocation == null &&
                hi.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)
            {
                var locationNameChunks = hi.LocationElapsedCase.Split("-").ToList();
                // this indicates that the hi.LocationElapsedCase is in format: '02092021-4202'
                if (locationNameChunks != null && locationNameChunks.Count >= 2)
                {
                    string locationPartName = null;
                    if (locationNameChunks.Count == 2 && locationNameChunks[1].Length == 4)
                    {
                        locationPartName = locationNameChunks[1].Substring(0, 2);
                    }
                    else if (locationNameChunks.Count == 3 && locationNameChunks[1].Length == 2)
                    {
                        locationPartName = locationNameChunks[1];
                    }
                    ctLocation = _context.CTLocations.Include(x => x.CTLocationType).ThenInclude(x => x.CTLocationTypeGCLifecycleStatusTypes)
                        .Where(x => x.LocationName == $"{ProfitOptics.Modules.Sox.Helpers.Constants.CT_STERILIZE_LOCATION_BASE_NAME}{locationPartName}").FirstOrDefault();
                }
            }

            if(ctLocation == null)
            {
                ctLocation = GetCTLocationForLocationName(hi.LocationElapsedCase, hi.Status == Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER);
            }

            if (ctLocation != null)
            {
                Log.Information($"---Found CTLocation {ctLocation.LocationName}, data patched!");
                hi.CTLocationId = ctLocation.Id;
                hi.CTLocation = ctLocation;
                _context.SaveChanges();
            }
            else
            {
                Log.Information($"---Still no CTLocation found...");
            }

            return ctLocation;
        }

        private void MatchHistoryItems(int gcCaseId)
        {
            var gcTrays = _context.GCTrays.Where(x => x.GCCaseId == gcCaseId).ToList();
            if (gcTrays != null && gcTrays.Count > 0)
            {
                var ctLocationTypeGCLifecycleStatusTypes = _context.CTLocationTypeGCLifecycleStatusTypes.ToList();
                var deconStageMappedLocationType = ctLocationTypeGCLifecycleStatusTypes.Where(x => x.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.DeconStaged).FirstOrDefault();

                if (deconStageMappedLocationType != null)
                {
                    gcTrays.ForEach(gcTray =>
                    {
                        if (gcTray.CTContainerTypeActualId.HasValue && gcTray.CTContainerTypeActualId.Value > 0)
                        {
                            int newLCsFound = 0;

                            var historyItems = _context.CTContainerTypeActualHistoryItems.Include(x => x.CTLocation).Where(x =>
                                x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value &&
                                (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG || x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER))
                                    .OrderByDescending(x => x.UpdateTimestamp).ToList();

                            var trayLCs = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == gcTray.Id).ToList();
                            var isLoaner = _context.GCTraysLifecycleStatusTypes.Include(x => x.LifecycleStatusType).Any(x => x.LifecycleStatusType.Code == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerInbound);
                            gcTray.IsLoaner = isLoaner;

                            if (historyItems != null && historyItems.Count > 0)
                            {
                                Log.Information($"Found {historyItems.Count} history items for GCTrayId: {gcTray.Id}, CTContainerTypeActualId: {gcTray.CTContainerTypeActualId.Value}");
                                var statusesToCheck = new List<CTContainerTypeActualHistoryItem>();
                                bool hasRachedDeconStaged = false;
                                foreach (var hi in historyItems)
                                {
                                    if (hi.CTLocation != null)
                                    {
                                        if (!hasRachedDeconStaged)
                                            statusesToCheck.Add(hi);
                                        if (hi.CTLocation.CTLocationTypeId == deconStageMappedLocationType.CTLocationTypeId)
                                        {
                                            hasRachedDeconStaged = true;
                                        }
                                    }
                                }

                                if (statusesToCheck.Count > 0)
                                {
                                    statusesToCheck.Reverse(); // reversing to start from earliest Timestamp

                                    for (int i = 0; i < statusesToCheck.Count; i++)
                                    {
                                        var statusToCheck = statusesToCheck[i];

                                        if (!trayLCs.Any(x => x.Timestamp == statusToCheck.UpdateTimestamp))
                                        {
                                            var lcStatuType = ctLocationTypeGCLifecycleStatusTypes.Where(x => x.CTLocationTypeId == statusToCheck.CTLocation.CTLocationTypeId).FirstOrDefault();
                                            int lcStatuTypeId = lcStatuType.GCLifecycleStatusTypeId;

                                            if (lcStatuType != null)
                                            {
                                                if (lcStatuType.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly ||
                                                lcStatuType.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Verification)
                                                {
                                                    if (i > 0)
                                                    {
                                                        var previousStatusToCheck = statusesToCheck[i - 1];
                                                        var previousLC = ctLocationTypeGCLifecycleStatusTypes.Where(x => x.CTLocationTypeId == previousStatusToCheck.CTLocation.CTLocationTypeId).FirstOrDefault();
                                                        if (previousLC != null)
                                                        {
                                                            if (previousLC.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.AssemblyStaged ||
                                                                previousLC.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Washer)
                                                            {
                                                                lcStatuTypeId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Assembly;
                                                            }
                                                            else if (previousLC.GCLifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.CleanStaged)
                                                            {
                                                                lcStatuTypeId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Verification;
                                                            }
                                                        }
                                                    }
                                                }

                                                _context.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                                {
                                                    LifecycleStatusTypeId = lcStatuTypeId,
                                                    GCTrayId = gcTray.Id,
                                                    IsActiveStatus = false,
                                                    Timestamp = statusToCheck.UpdateTimestamp,
                                                    User = statusToCheck.UpdateUserId,
                                                    TimestampOriginal = statusToCheck.UpdateTimestampOriginal
                                                });
                                                newLCsFound++;
                                            }
                                        }
                                    }

                                    Log.Information($"Found {newLCsFound} unrecorded (old) LC Statuses");
                                    _context.SaveChanges();
                                }
                            }
                        }
                    });
                }
            }
        }

        public void CT_SyncLCForPastCases()
        {
            var completedOrPastCases = _context.GCCases.Include(x => x.CTCase).Where(x =>
                x.CTCase.DueTimestamp < DateTime.Now ||
                x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE ||
                x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID ||
                x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE ||
                x.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID
            ).Include(x => x.GCTrays).ThenInclude(x => x.GCTraysLifecycleStatusTypes).ToList();
            if (completedOrPastCases != null || completedOrPastCases.Count > 0)
            {
                Log.Information($"Found {completedOrPastCases.Count} GCCases");
                completedOrPastCases.ForEach(gcCase =>
                {
                    Log.Information($"Processing GCCase: {gcCase.Title}");
                    bool isCaseComplete = gcCase.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE ||
                                gcCase.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID ||
                                gcCase.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE ||
                                gcCase.CTCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID;

                    if (gcCase.GCTrays != null && gcCase.GCTrays.Count > 0)
                    {
                        Log.Information($"Found {gcCase.GCTrays.Count} GCTrays");
                        gcCase.GCTrays.ToList().ForEach(gcTray =>
                        {
                            if (gcTray.CTContainerTypeActualId.HasValue)
                            {
                                Log.Information($"Processing GCTrayID: {gcTray.Id}, ActualID: {gcTray.CTContainerTypeActualId.Value}");

                                var historyItems = _context.CTContainerTypeActualHistoryItems.Where(x =>
                                    x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value &&
                                    (x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG || x.Status == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)).ToList();

                                Log.Information($"Found {historyItems.Count} HistoryItems");
                                if (historyItems != null && historyItems.Count > 0)
                                {
                                    if (gcTray.GCTraysLifecycleStatusTypes == null)
                                    {
                                        gcTray.GCTraysLifecycleStatusTypes = new List<GCTraysLifecycleStatusTypes>();
                                    }

                                    foreach (var hi in historyItems)
                                    {
                                        if (!gcTray.GCTraysLifecycleStatusTypes.Any(x => x.Timestamp == hi.UpdateTimestamp))
                                        {
                                            Log.Information($"No GCTrayLC for timestamp: {hi.UpdateTimestamp}");
                                            bool isLoaner = false;
                                            GCLifecycleStatusType gcLifecycleStatusType = null;

                                            if (string.IsNullOrWhiteSpace(hi.LocationElapsedCase) ||
                                                hi.LocationElapsedCase.Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION) ||
                                                hi.LocationElapsedCase.Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION))
                                            {
                                                isLoaner = true;
                                                gcLifecycleStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Code == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerInbound).FirstOrDefault();
                                                Log.Information($"Tray is a loaner!");
                                            }
                                            else
                                            {
                                                // Silly Hack because of CT
                                                var ctLocation = GetCTLocationForLocationName(hi.LocationElapsedCase, hi.Status == Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER);
                                                gcLifecycleStatusType = GetGCLifeCycleBasedOnLocationType(hi.UpdateTimestamp, ctLocation.CTLocationTypeId, gcTray.CTContainerTypeActualId.Value, null);
                                            }

                                            Log.Information($"Lifecycle: {gcLifecycleStatusType.Title}");


                                            gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes
                                            {
                                                IsActiveStatus = false,
                                                GCTrayId = gcTray.Id,
                                                User = hi.UpdateUserId,
                                                Timestamp = hi.UpdateTimestamp,
                                                TimestampOriginal = hi.UpdateTimestampOriginal,
                                                LifecycleStatusTypeId = gcLifecycleStatusType.Id
                                            });
                                        }
                                        _context.SaveChanges();
                                    }

                                    gcTray.GCTraysLifecycleStatusTypes = gcTray.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).ToList();

                                    for (int i = 0; i < gcTray.GCTraysLifecycleStatusTypes.ToList().Count(); i++)
                                    {
                                        var lc = gcTray.GCTraysLifecycleStatusTypes.ToList()[i];
                                        lc.IsActiveStatus = i == 0;
                                    }

                                }
                            }
                            _context.SaveChanges();
                        });
                    }
                });
            }
        }

        public int GetIncompleteSheetCountForTrayIds(List<int> trayIds)
        {
            //int result = _context.CTAssemblyCountSheets.Include(x => x.TrayLifecycleStatusType).Count(x =>
            //    x.ActualCount < x.RequiredCount &&
            //    x.TrayLifecycleStatusType != null &&
            //    trayIds.Contains(x.TrayLifecycleStatusType.GCTrayId));

            var result = _context.CTAssemblyCountSheets.Include(x => x.TrayLifecycleStatusType).ThenInclude(x => x.GCTray).Where(x =>
                x.ActualCount < x.RequiredCount &&
                x.TrayLifecycleStatusType != null &&
                x.TrayLifecycleStatusType.GCTray != null &&
                x.TrayLifecycleStatusType.GCTray.CTContainerTypeActualId.HasValue &&
                trayIds.Contains(x.TrayLifecycleStatusType.GCTray.CTContainerTypeActualId.Value)).Count();

            return result;
        }

        public int GetSystemAlertCountForTrayIds(List<int> trayIds)
        {
            var result = _context.GCSystemAlerts.Where(x =>
                x.MarkedAsReadAtUtc == null &&
                x.GCTrayId.HasValue &&
                trayIds.Contains(x.GCTrayId.Value)).Count();

            return result;
        }

        public bool HasActualIncompleteCountSheet(int cTContainerTypeActualId)
        {
            return _context.CTAssemblyCountSheets.Include(x => x.TrayLifecycleStatusType).ThenInclude(x => x.GCTray).Any(x =>
                x.ActualCount < x.RequiredCount &&
                x.TrayLifecycleStatusType != null &&
                x.TrayLifecycleStatusType.GCTray != null &&
                x.TrayLifecycleStatusType.GCTray.CTContainerTypeActualId.HasValue &&
                x.TrayLifecycleStatusType.GCTray.CTContainerTypeActualId == cTContainerTypeActualId);
        }

        public void CT_SyncColorForPastCases()
        {
            var allCases = _context.CTCases.Include(x => x.GCCase).Include(x => x.GCCase.GCTrays).ThenInclude(x => x.GCTraysLifecycleStatusTypes)
                .Where(x => x.DueTimestamp < DateTime.Now || x.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID).ToList();
            if (allCases != null && allCases.Count > 0)
            {
                CT_UpdateColorStatusForGCCasesAndGCTrays(allCases);
            }
        }

        #endregion GroundControl

        #region CensiTrack

        #region CT ETL Services
        public class CensiTrackModuleOptions : IOptions
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public int ClientId { get; set; }
            public long FacilityId { get; set; }
            public string Action { get; set; }
            public long ServiceId { get; set; }
            public long ContainerId { get; set; }
            public string CaseReference { get; set; }
            public string FetchChildObjects { get; set; }
            public string CycleId { get; set; }
            public string BypassContainerServices { get; set; }
            public string StopHistoryFromLoading { get; set; }
            public string RunAnyway { get; set; }
        }

        public int Execute(IOptions opts)
        {
            var ctOpts = opts as CensiTrackModuleOptions;
            CTActionType ctActionType = (CTActionType)(int.Parse(ctOpts.Action));

            ETLLogs etlLog = new ETLLogs();
            etlLog.Source = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_CT_MODULE_NAME;
            etlLog.FileName = "no file";
            etlLog.Username = "no username";
            etlLog.Status = ETLStatusType.OK.ToString();
            etlLog.Message = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE;
            etlLog.RowCount = 0;
            etlLog.ActionType = (int)ctActionType;
            etlLog.Options = $@"UserName: {ctOpts.UserName}; Password: ******; ClientId: {ctOpts.ClientId}; FacilityId: {ctOpts.FacilityId}; Action: {ctOpts.Action}; ServiceId: {ctOpts.ServiceId}; ContainerId: {ctOpts.ContainerId}; CaseReference: {ctOpts.CaseReference}; FetchChildObjects: {ctOpts.FetchChildObjects};";
            etlLog.Description = string.Empty;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            // TODO needs refacotring ASAP:
            const int CENTRAL_TIMEZONE_OFFSET_IN_MINS = -360;
            int clientId = 1906;
            string userName = "dragan";
            string password = "Vested123!";
            long facilityId = 0;
            bool syncAllActuals = true;
            bool updateAllTrays = true;

            try
            {
                switch (ctActionType)
                {
                    case CTActionType.GetCTCases:
                        var getCTCasesResponse = CT_GetCTCases(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTCasesResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTCasesResponse.Message;
                        }
                        etlLog.RowCount = getCTCasesResponse.RowCount;
                        break;
                    case CTActionType.GetCTFacilities:
                        var getCTFacilitiesResponse = CT_GetCTFacilities(clientId, facilityId, userName, password).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTFacilitiesResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTFacilitiesResponse.Message;
                        }
                        etlLog.RowCount = getCTFacilitiesResponse.RowCount;
                        break;
                    case CTActionType.GetCTContainerServices:
                        var getCTContainerServicesResponse = CT_GetCTContainerServices(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTContainerServicesResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTContainerServicesResponse.Message;
                        }
                        etlLog.RowCount = getCTContainerServicesResponse.RowCount;
                        etlLog.Description = getCTContainerServicesResponse.Description;
                        break;
                    case CTActionType.GetCTContainersByService:
                        var getCTContainersByServiceResponse = CT_GetCTContainerTypesByService(ctOpts);
                        var getCTContainersByServiceResponseResult = getCTContainersByServiceResponse.GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTContainersByServiceResponseResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTContainersByServiceResponseResult.Message;
                        }
                        etlLog.RowCount = getCTContainersByServiceResponseResult.RowCount;
                        etlLog.Description = getCTContainersByServiceResponseResult.Description;
                        break;
                    case CTActionType.GetCTProducts:
                        //var getCTProductsResponse = CT_GetCTProducts(ctOpts).GetAwaiter().GetResult();
                        var getCTProductsResponse = CT_GetCTProducts(clientId, facilityId, userName, password).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTProductsResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTProductsResponse.Message;
                        }
                        etlLog.RowCount = getCTProductsResponse.RowCount;
                        break;
                    case CTActionType.GetCTVendors:
                        var getCTVendorsResponse = CT_GetCTVendors(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTVendorsResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTVendorsResponse.Message;
                        }
                        etlLog.RowCount = getCTVendorsResponse.RowCount;
                        break;
                    case CTActionType.GetCTContainerItems:
                        var getCTContainerItemsResponse = CT_GetCTContainerItems(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTContainerItemsResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTContainerItemsResponse.Message;
                        }
                        etlLog.RowCount = getCTContainerItemsResponse.RowCount;
                        break;
                    case CTActionType.GetCTCaseContainers:
                        var getCTCaseContainersResponse = CT_GetCTCaseContainers(ctOpts, CENTRAL_TIMEZONE_OFFSET_IN_MINS).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTCaseContainersResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTCaseContainersResponse.Message;
                        }
                        etlLog.RowCount = getCTCaseContainersResponse.RowCount;
                        break;
                    case CTActionType.GetCTCasesContainersAndSubmitToWW:
                        if (ctOpts.BypassContainerServices == null || !ctOpts.BypassContainerServices.Equals("2"))
                        {
                            var getCTCasesContainersAndSubmitToWWResponse = CT_GetCTCasesContainersAndSubmitToWW(ctOpts, CENTRAL_TIMEZONE_OFFSET_IN_MINS).GetAwaiter().GetResult();
                            if (!string.IsNullOrWhiteSpace(getCTCasesContainersAndSubmitToWWResponse.Message))
                            {
                                etlLog.Status = ETLStatusType.Error.ToString();
                                etlLog.Message = getCTCasesContainersAndSubmitToWWResponse.Message;
                            }
                            etlLog.RowCount = getCTCasesContainersAndSubmitToWWResponse.RowCount;
                            etlLog.Description = getCTCasesContainersAndSubmitToWWResponse.Description;
                        }
                        break;
                    case CTActionType.GetCTContainerActualsByContainerType:
                        var getCTContainerActualsByContainerTypeResponse = CT_GetCTContainerActualsByContainerType(ctOpts, CENTRAL_TIMEZONE_OFFSET_IN_MINS).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTContainerActualsByContainerTypeResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTContainerActualsByContainerTypeResponse.Message;
                        }
                        etlLog.RowCount = getCTContainerActualsByContainerTypeResponse.RowCount;
                        etlLog.Description = getCTContainerActualsByContainerTypeResponse.Description;
                        break;
                    case CTActionType.GetCTLocationTypes:
                        var getCTLocationTypesResponse = CT_GetCTLocationTypes(clientId, facilityId, userName, password).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTLocationTypesResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTLocationTypesResponse.Message;
                        }
                        etlLog.RowCount = getCTLocationTypesResponse.RowCount;
                        etlLog.Description = getCTLocationTypesResponse.Description;
                        break;
                    case CTActionType.GetCTLocations:
                        var getCTLocationsResponse = CT_GetCTLocations(clientId, facilityId, userName, password).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTLocationsResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTLocationsResponse.Message;
                        }
                        etlLog.RowCount = getCTLocationsResponse.RowCount;
                        etlLog.Description = getCTLocationsResponse.Description;
                        break;
                    case CTActionType.GetContainerTypeActualHistory:
                        //var getContainerTypeActualHistoryResponse = CT_GetContainerTypeActualHistory(ctOpts, CENTRAL_TIMEZONE_OFFSET_IN_MINS).GetAwaiter().GetResult();
                        bool isStopLifecycleHistory = true;
                        string cycleId = string.Empty;
                        long actualId = 0;
                        var getContainerTypeActualHistoryResponse = CT_GetContainerTypeActualHistory(clientId, facilityId, userName, password, CENTRAL_TIMEZONE_OFFSET_IN_MINS, isStopLifecycleHistory, cycleId, actualId).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getContainerTypeActualHistoryResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getContainerTypeActualHistoryResponse.Message;
                        }
                        etlLog.RowCount = getContainerTypeActualHistoryResponse.RowCount;
                        etlLog.Description = getContainerTypeActualHistoryResponse.Description;
                        break;
                    case CTActionType.GetCTContainerTypeGraphic:
                        //var getCTContainerTypeGraphicResponse = CT_GetCTContainerTypeGraphic(ctOpts).GetAwaiter().GetResult();
                        //if (!string.IsNullOrWhiteSpace(getCTContainerTypeGraphicResponse.Message))
                        //{
                        //    etlLog.Status = ETLStatusType.Error.ToString();
                        //    etlLog.Message = getCTContainerTypeGraphicResponse.Message;
                        //}
                        //etlLog.RowCount = getCTContainerTypeGraphicResponse.RowCount;
                        //etlLog.Description = getCTContainerTypeGraphicResponse.Description;
                        break;
                    case CTActionType.GetCTContainerTypeGraphicMedia:
                        var getCTContainerTypeGraphicMediaResult = CT_GetCTContainerTypeGraphicMediaAndTrayInductionSyncAndLCUpdate(clientId, facilityId, userName, password, syncAllActuals, updateAllTrays).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTContainerTypeGraphicMediaResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTContainerTypeGraphicMediaResult.Message;
                        }
                        etlLog.RowCount = getCTContainerTypeGraphicMediaResult.RowCount;
                        etlLog.Description = getCTContainerTypeGraphicMediaResult.Description;
                        break;
                    case CTActionType.GetCTBILoadResults:
                        var getCTBILoadResultsResult = CT_GetCTBILoadResultsAndTrayInductionSync(ctOpts, CENTRAL_TIMEZONE_OFFSET_IN_MINS).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getCTBILoadResultsResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getCTBILoadResultsResult.Message;
                        }
                        etlLog.RowCount = getCTBILoadResultsResult.RowCount;
                        etlLog.Description = getCTBILoadResultsResult.Description;
                        break;
                    case CTActionType.SyncTimestamps:
                        var testingResult = CT_SyncTimestampResults(ctOpts, CENTRAL_TIMEZONE_OFFSET_IN_MINS).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(testingResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = testingResult.Message;
                        }
                        etlLog.RowCount = testingResult.RowCount;
                        etlLog.Description = testingResult.Description;
                        break;
                    case CTActionType.GetContainerAssemblyMediaForActuals:
                        var getContainerAssemblyMediaForActualsResult = CT_GetContainerAssemblyMediaForActuals(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(getContainerAssemblyMediaForActualsResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = getContainerAssemblyMediaForActualsResult.Message;
                        }
                        etlLog.RowCount = getContainerAssemblyMediaForActualsResult.RowCount;
                        etlLog.Description = getContainerAssemblyMediaForActualsResult.Description;
                        break;
                    case CTActionType.SyncLCForPastCases:
                        var syncLCForPastCasesResult = CT_SyncLCForPastCases(ctOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(syncLCForPastCasesResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = syncLCForPastCasesResult.Message;
                        }
                        etlLog.RowCount = syncLCForPastCasesResult.RowCount;
                        etlLog.Description = syncLCForPastCasesResult.Description;
                        break;
                    case CTActionType.GetServerTime:
                        Log.Information($"Server time: {DateTime.Now}");
                        break;
                    case CTActionType.CreateGCImageThumbnails:
                        var createGCImageThumbnailsResult = CT_CreateGCImageThumbnails().GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(createGCImageThumbnailsResult.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = createGCImageThumbnailsResult.Message;
                        }
                        etlLog.RowCount = createGCImageThumbnailsResult.RowCount;
                        etlLog.Description = createGCImageThumbnailsResult.Description;
                        break;
                    case CTActionType.CompleteBrokenTrayHistory:
                        //var completeBrokenTrayHistoryResult = CT_CompleteBrokenTrayHistory().GetAwaiter().GetResult();
                        //if (!string.IsNullOrWhiteSpace(completeBrokenTrayHistoryResult.Message))
                        //{
                        //    etlLog.Status = ETLStatusType.Error.ToString();
                        //    etlLog.Message = completeBrokenTrayHistoryResult.Message;
                        //}
                        //etlLog.RowCount = completeBrokenTrayHistoryResult.RowCount;
                        //etlLog.Description = completeBrokenTrayHistoryResult.Description;
                        break;
                    default:
                        break;
                }

                //etlLog.Description = string.Empty;
            }
            catch (Exception e)
            {
                etlLog.Status = ETLStatusType.Error.ToString();
                etlLog.Message = e.Message;
                etlLog.Description = e.StackTrace;
                if (e.InnerException != null)
                {
                    etlLog.Description += $" InnerException: {e.InnerException.Message}";
                }
                Log.Error(e.Message);
            }
            finally
            {
                etlLog.ProcessingTime = DateTime.UtcNow;
                stopwatch.Stop();
                etlLog.Username = stopwatch.Elapsed.ToString();
                LogProcessedAction(etlLog.Username, etlLog.Source, etlLog.FileName, etlLog.ProcessingTime.ToString(), etlLog.Status, etlLog.RowCount, etlLog.Message, etlLog.ActionType, etlLog.Options, etlLog.Description);
            }


            return 0;
        }

        public void LogProcessedAction(string username, string source, string fileName, string processingTime, string status, int rowCount, string message = null, int? actionType = null, string options = null, string description = "")
        {
            //using (var connection = new SqlConnection(_settings.DefaultConnection))
            //{
            //    connection.Open();
            //    using (var command = connection.CreateCommand())
            //    {
            //        command.CommandText = @"
            //        INSERT INTO [ETLLog] ([Source],[FileName],[Username],[ProcessingTime],[Status],[Message],[RowCount],[ActionType],[Options],[Description]) 
            //        VALUES (@source, @fileName, @username, @processingTime, @status, @message, @rowCount, @actionType, @options, @description)";

            //        command.Parameters.Add(new SqlParameter("source", source));
            //        command.Parameters.Add(new SqlParameter("fileName", fileName));
            //        command.Parameters.Add(new SqlParameter("username", username));
            //        command.Parameters.Add(new SqlParameter("processingTime", processingTime));
            //        command.Parameters.Add(new SqlParameter("status", status));
            //        command.Parameters.Add(new SqlParameter("message", string.IsNullOrEmpty(message) ? "OK" : message));
            //        command.Parameters.Add(new SqlParameter("rowCount", rowCount));
            //        command.Parameters.Add(new SqlParameter("actionType", actionType));
            //        command.Parameters.Add(new SqlParameter("options", options));
            //        command.Parameters.Add(new SqlParameter("description", description));

            //        command.ExecuteNonQuery();
            //    }
            //}

            var etlLog = new ETLLogs();

            etlLog.Source = source;
            etlLog.FileName = fileName;
            etlLog.Username = username;
            etlLog.ProcessingTime = Convert.ToDateTime(processingTime);
            etlLog.Status = status;
            etlLog.Message = message;
            etlLog.RowCount = rowCount;
            etlLog.ActionType = actionType;
            etlLog.Options = options;
            etlLog.Description = description;

            _context.ETLLogs.Add(etlLog);
            _context.SaveChanges();
        }

        public async Task<ETLCTLoginResponseModel> CT_Login(ETLCTLoginModel model)
        {
            var responseModel = new ETLCTLoginResponseModel()
            {
                LoginSuccessful = false,
                StatusCode = 204,
            };

            Log.Information($"Loging user: { model.UserId }");
            try
            {
                var response = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, model.ClientId).AppendPathSegment(Helpers.Constants.CT_CENSITRACK_LOGIN_URL).PostJsonAsync(model);
                responseModel.LoginSuccessful = responseModel.StatusCode == response.StatusCode;
                Log.Information($"Response: { response.ResponseMessage }");
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                responseModel.Message = e.Message;
            }

            return responseModel;
        }

        public async Task<List<ETLCTCase>> CT_GetCTCasesV2(int clientId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/external/distribution-center/all-case-statuses

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("external", "distribution-center", "all-case-statuses")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTCase>>();

            Log.Information($"Fetched {result.Count} CT cases.");

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTCases(CensiTrackModuleOptions options)
        {
            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTCases - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });

            if (loginResponse.LoginSuccessful)
            {
                var ctCases = await CT_GetCTCasesV2(options.ClientId);
                if (ctCases.Count > 0)
                {
                    CT_AddOrEditCTCases(ctCases.Select(x => _mapper.Map<Framework.DataLayer.CTCase>(x)).ToList());
                }
                result.RowCount = ctCases.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<List<ETLCTFacility>> CT_GetCTFacilities(int clientId)
        {
            // Example
            // http://1906.censis.net/mvc/api/schedules/facilities

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("schedules", "facilities")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTFacilitiesResponse>();

            Log.Information($"Fetched {result.ScheduleFacilities.Count} CT facilities.");

            return result.ScheduleFacilities;
        }

        public async Task<ETLCTQualityFeedResponse> CT_GetQualityFeedResponse(int clientId, ETLCTQualityFeedRequest request)
        {
            // Example:
            // http://1906.censis.net/mvc/api/reports/execute-report

            var response = new ETLCTQualityFeedResponse();
            response.Message = "";

            try
            {
                response = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
                    .AppendPathSegments("reports", "execute-report")
                    .PostJsonAsync(request)
                    .ReceiveJson<ETLCTQualityFeedResponse>();
            }
            catch (Exception e)
            {
                response.Message = e.Message;
                throw;
            }

            return response;
        }

        private async Task<ETLResponseModel> CT_CreateGCImageThumbnails()
        {
            Log.Information("CreateGCImageThumbnails - Fetch process started.");
            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"CreateGCImageThumbnails - CycleId: {cycleId} | ";

            Log.Information($"CreateGCImageThumbnails STARTED...");
            CreateGCImageThumbnails();
            Log.Information($"CreateGCImageThumbnails FINISHED");

            result.RowCount = 0;
            result.Description = description;

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTFacilities(int clientId, long facilityId, string userName, string password)
        {
            Log.Information("GetCTFacilities = Fetch process started.");

            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTFacilities - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = clientId, FacilityId = facilityId, UserId = userName, UserPassword = password });
            if (loginResponse.LoginSuccessful)
            {
                var ctFacilities = await CT_GetCTFacilities(clientId);
                if (ctFacilities.Count > 0)
                {
                    CT_AddOrEditCTFacilities(ctFacilities.Select(x => _mapper.Map<Framework.DataLayer.CTFacility>(x)).ToList());
                }
                result.RowCount = ctFacilities.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<List<ETLCTContainerService>> CT_GetCTContainerServices(int clientId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/services

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "services")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTContainerService>>();

            Log.Information($"Fetched {result.Count} CT Container Services.");

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTContainerServices(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTContainerServices - Fetch process started.");
            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTContainerServices - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctContainerServices = await CT_GetCTContainerServices(options.ClientId);
                if (ctContainerServices.Count > 0)
                {
                    //_ctContainerService.AddOrEditGCVendors(ctContainerServices.Select(x => _mapper.Map<Framework.DataLayer.GCVendor>(x)).ToList());
                    CT_AddOrEditCTContainerServices(ctContainerServices.Select(x => _mapper.Map<Framework.DataLayer.CTContainerService>(x)).ToList());
                }
                result.RowCount = ctContainerServices.Count;
                description += $"Fetched {ctContainerServices.Count} CTContainerServices.";

                // fetching ContainerTypes
                if (!string.IsNullOrWhiteSpace(options.FetchChildObjects) && (options.FetchChildObjects.Equals("1") || options.FetchChildObjects.Equals("2")))
                {
                    ctContainerServices.ForEach(containerService =>
                    {
                        options.Action = ((int)CTActionType.GetCTContainersByService).ToString();
                        options.CycleId = cycleId;
                        options.ServiceId = containerService.Id;
                        Execute(options);
                    });
                }

                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<List<ETLCTContainerType>> CT_GetCTContainerTypesByService(int clientId, long facilityId, long serviceId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/types/by-service/10000002?facilityId=10000000&includeDeleted=false&includeMedia=true

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "types", "by-service", serviceId)
            .SetQueryParams(new { facilityId, includeDeleted = false, includeMedia = true })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTContainerType>>();

            Log.Information($"Fetched {result.Count} CT Container Types for FacilityId: {facilityId} & ServiceId: {serviceId}.");

            return result;
        }

        public async Task<ETLCTContainerType> CT_GetCTContainerTypeItem(int clientId, long legacyId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/types/1045

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "types", legacyId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerType>();

            Log.Information($"Fetched CT ETLCTContainerType.");

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTContainerTypesByService(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTContainerTypesByService - Fetch process started.");
            var result = new ETLResponseModel();
            var description = $"GetCTContainerTypesByService - CycleId: {options.CycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                // TODO: Handle case when ServiceId is not provided - Get for all existing services
                var etlCTContainerTypes = await CT_GetCTContainerTypesByService(options.ClientId, options.FacilityId, options.ServiceId);
                if (etlCTContainerTypes.Count > 0)
                {
                    // Getting more info from a ContainerType:
                    var ctCTContainerTypes = etlCTContainerTypes.Select(x => _mapper.Map<Framework.DataLayer.CTContainerType>(x)).ToList();

                    foreach (var ctContainerType in ctCTContainerTypes)
                    {
                        ETLCTContainerType etlCTContainerType = await CT_GetCTContainerTypeItem(options.ClientId, ctContainerType.LegacyId);
                        if (etlCTContainerType != null)
                        {
                            ctContainerType.FacilityId = etlCTContainerType.FacilityId;
                            ctContainerType.SterilizationMethodId = etlCTContainerType.SterilizationMethodId;
                            ctContainerType.FirstAlternateMethodId = etlCTContainerType.FirstAlternateMethodId;
                            ctContainerType.SecondAlternateMethodId = etlCTContainerType.SecondAlternateMethodId;
                            ctContainerType.ThirdAlternateMethodId = etlCTContainerType.ThirdAlternateMethodId;
                            ctContainerType.HomeLocationId = etlCTContainerType.HomeLocationId;
                            ctContainerType.ProcessingLocationId = etlCTContainerType.ProcessingLocationId;
                            ctContainerType.PhysicianName = etlCTContainerType.PhysicianName;
                            ctContainerType.UsesBeforeService = etlCTContainerType.UsesBeforeService;
                            ctContainerType.UsageInterval = etlCTContainerType.UsageInterval;
                            ctContainerType.UsageIntervalUnitOfMeasure = etlCTContainerType.UsageIntervalUnitOfMeasure;
                            ctContainerType.StandardAssemblyTime = etlCTContainerType.StandardAssemblyTime;
                            ctContainerType.Weight = etlCTContainerType.Weight;
                            ctContainerType.ProcessingCost = etlCTContainerType.ProcessingCost;
                            ctContainerType.ProcurementReferenceId = etlCTContainerType.ProcurementReferenceId;
                            ctContainerType.DecontaminationInstructions = etlCTContainerType.DecontaminationInstructions;
                            ctContainerType.AssemblyInstructions = etlCTContainerType.AssemblyInstructions;
                            ctContainerType.SterilizationInstructions = etlCTContainerType.SterilizationInstructions;
                            ctContainerType.CountSheetComments = etlCTContainerType.CountSheetComments;
                            ctContainerType.PriorityId = etlCTContainerType.PriorityId;
                            ctContainerType.BiologicalTestRequired = etlCTContainerType.BiologicalTestRequired;
                            ctContainerType.ClassSixTestRequired = etlCTContainerType.ClassSixTestRequired;
                            ctContainerType.AssemblyByException = etlCTContainerType.AssemblyByException;
                            ctContainerType.SoftWrap = etlCTContainerType.SoftWrap;
                            ctContainerType.ComplexityLevelId = etlCTContainerType.ComplexityLevelId;
                            ctContainerType.OriginalContainerTypeId = etlCTContainerType.OriginalContainerTypeId;
                            ctContainerType.VendorId = etlCTContainerType.VendorId;
                            ctContainerType.VendorName = etlCTContainerType.VendorName;
                        }
                    }

                    CT_AddOrEditCTContainerTypes(ctCTContainerTypes, options.ServiceId);
                }
                result.RowCount = etlCTContainerTypes.Count;
                description += $"Feched {etlCTContainerTypes.Count} ContainerTypes";

                // Fetching actuals for inventory:
                if (!string.IsNullOrWhiteSpace(options.FetchChildObjects) && (options.FetchChildObjects.Equals("1") || options.FetchChildObjects.Equals("2")))
                {
                    etlCTContainerTypes.ForEach(ctContainerType =>
                    {
                        options.Action = ((int)CTActionType.GetCTContainerActualsByContainerType).ToString();
                        options.ServiceId = ctContainerType.Id;
                        Execute(options);
                    });

                    //etlCTContainerTypes.ForEach(ctContainerType =>
                    //{
                    //    options.Action = ((int)CTActionType.GetCTContainerItems).ToString();
                    //    options.ServiceId = ctContainerType.Id;
                    //    Execute(options);
                    //});

                    etlCTContainerTypes.ForEach(ctContainerType =>
                    {
                        options.Action = ((int)CTActionType.GetCTContainerTypeGraphic).ToString();
                        options.ServiceId = ctContainerType.Id;
                        Execute(options);
                    });
                }
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<List<ETLCTProduct>> CT_GetCTProducts(int clientId)
        {
            // Example
            // http://1906.censis.net/mvc/api/products/search?$count=true

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("products", "search")
            //.SetQueryParams(new { count = true })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTProductsResponse>();

            Log.Information($"Fetched {result.Items.Count} CT Products.");

            return result.Items;
        }

        //public async Task<ETLResponseModel> CT_GetCTProducts(CensiTrackModuleOptions options)
        public async Task<ETLResponseModel> CT_GetCTProducts(int clientId, long facilityId, string userName, string password)
        {
            Log.Information("GetCTProducts - Fetch process started.");
            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTProducts - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = clientId, FacilityId = facilityId, UserId = userName, UserPassword = password });
            if (loginResponse.LoginSuccessful)
            {
                var ctProducts = await CT_GetCTProducts(clientId);
                if (ctProducts.Count > 0)
                {
                    CT_AddOrEditCTProducts(ctProducts.Select(x => _mapper.Map<Framework.DataLayer.CTProduct>(x)).ToList());
                }
                result.RowCount = ctProducts.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<List<ETLCTVendor>> CT_GetCTVendors(int clientId)
        {
            // Example
            // http://1906.censis.net/mvc/api/vendor/

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegment("vendor")
            //.SetQueryParams(new { $count, includeDeleted = false, includeMedia = true })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTVendor>>();

            Log.Information($"Fetched {result.Count} CT Vendors.");

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTVendors(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTVendors - Fetch process started.");
            var result = new ETLResponseModel();
            var description = $"GetCTVendors - CycleId: {options.CycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctVendors = await CT_GetCTVendors(options.ClientId);
                if (ctVendors.Count > 0)
                {
                    CT_AddOrEditCTVendors(ctVendors.Select(x => _mapper.Map<Framework.DataLayer.CTVendor>(x)).ToList());
                }
                result.RowCount = ctVendors.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<List<ETLCTContainerItem>> CT_GetCTContainerTypeItems(int clientId, long containerId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/types/10000056/count-sheet

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "types", containerId, "count-sheet")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTContainerItem>>();

            Log.Information($"Fetched {result.Count} CT ContainerItems.");

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTContainerItems(CensiTrackModuleOptions options)
        {
            Log.Information("GetCTContainerItems - Fetch process started.");
            var result = new ETLResponseModel();
            var description = $"GetCTContainerItems - CycleId: {options.CycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctContainerItems = await CT_GetCTContainerTypeItems(options.ClientId, options.ServiceId);
                if (ctContainerItems.Count > 0)
                {
                    CT_AddOrEditCTContainerItems(ctContainerItems.Select(x => _mapper.Map<Framework.DataLayer.CTContainerItem>(x)).ToList(), options.ServiceId);
                }
                result.RowCount = ctContainerItems.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<List<ETLCTCaseContainer>> CT_GetCTCaseContainers(int clientId, long facilityId, string caseReference, bool? showReprocessingOnly = true)
        {
            // Example:
            //http://1906.censis.net/mvc/api/schedules/case-containers?caseReference=11111111&facilityId=0&showReprocessingOnly=false

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("schedules", "case-containers")
            //.SetQueryParams(new { caseReference, facilityId, showReprocessingOnly })
            .SetQueryParams(new { caseReference })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTCaseContainer>>();

            Log.Information($"Fetched {result.Count} CT Containers for CaseReference: { caseReference }.");

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTCaseContainers(CensiTrackModuleOptions options, int serverOffsetInMinsFromUTC)
        {
            Log.Information("GetCTCaseContainers - Fetch process started.");
            var result = new ETLResponseModel();
            var description = $"GetCTCaseContainers - CycleId: {options.CycleId} | ";
            bool isStopLifecycleHistory = options.StopHistoryFromLoading != null && options.StopHistoryFromLoading == "1";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctCaseContainers = await CT_GetCTCaseContainers(options.ClientId, options.FacilityId, options.CaseReference);
                if (ctCaseContainers.Count > 0)
                {
                    CT_AddOrEditCTCaseContainers(ctCaseContainers.Select(x => _mapper.Map<Framework.DataLayer.CTCaseContainer>(x)).ToList(), serverOffsetInMinsFromUTC, isStopLifecycleHistory);
                }
                result.RowCount = ctCaseContainers.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<ETLCTContainerType> CT_GetCTContainerTypesByService(int clientId, long containerTypeId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/types/1249

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "types", containerTypeId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerType>();

            Log.Information($"Fetched CT ETLCTContainerType.");

            return result;
        }

        public async Task<ETLCTContainerService> CT_GetCTContainerServiceByLegacyId(int clientId, long serviceId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/services/1002

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "services", serviceId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerService>();

            Log.Information($"Fetched CT ETLCTContainerService.");

            return result;
        }

        public async Task<List<ETLCTContainerTypeActual>> CT_GetCTContainerTypeActualssByContainerTypes(int clientId, long containerTypeId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/by-type/10000069?includeDeleted=false

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "by-type", containerTypeId)
            .SetQueryParams(new { includeDeleted = false })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTContainerTypeActual>>();

            Log.Information($"Fetched {result.Count} CT ContainerTypeActuals for ContainerTypeID: { containerTypeId }.");

            return result;
        }

        public async Task<ETLCTContainerTypeActualHistoryResponse> CT_GetCTContainerTypeActualHistoryItems(int clientId, long actualId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/1010/history?pageSize=30

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", actualId, "history")
            .SetQueryParams(new { pageSize = 100000000 })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerTypeActualHistoryResponse>();

            Log.Information($"Fetched {(result.ETLCTContainerTypeActualHistoryItems == null ? 0 : result.ETLCTContainerTypeActualHistoryItems.Count)} CT ETLCTContainerTypeActualHistoryItems.");

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTCasesContainersAndSubmitToWW(CensiTrackModuleOptions options, int serverOffsetInMinsFromUTC)
        {
            Log.Information("GetCTCasesContainersAndSubmitToWW - Fetch process started.");
            var result = new ETLResponseModel();
            bool isStopLifecycleHistory = options.StopHistoryFromLoading != null && options.StopHistoryFromLoading == "1";

            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTCasesContainersAndSubmitToWW - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                Log.Information("Fetching CTCases...");
                var ctCases = await CT_GetCTCasesV2(options.ClientId);
                var ctContainerTypeActualLegacyIds = new List<long?>();
                if (ctCases.Count > 0)
                {
                    var ctCasesToUse = ctCases.ToList();

                    CT_AddOrEditCTCases(ctCasesToUse.Select(x => _mapper.Map<Framework.DataLayer.CTCase>(x)).ToList());

                    ctCasesToUse.ForEach(ctCase =>
                    {
                        Log.Information("Fetching CTCaseContainers...");
                        var ctCaseContainers = CT_GetCTCaseContainers(options.ClientId, options.FacilityId, ctCase.CaseReference).Result;
                        if (ctCaseContainers.Count > 0)
                        {
                            try
                            {
                                // HistoryItems for Actuals need to be updated, since skipped in previous step
                                if (true)//options.BypassContainerServices != null && options.BypassContainerServices.Equals("1"))
                                {
                                    ctCaseContainers.ForEach(x =>
                                    {
                                        if (x.ContainerTypeId.HasValue)
                                        {
                                            var ctContainerType = CT_GetCTContainerByContainerTypeId(x.ContainerTypeId.Value);
                                            if (ctContainerType == null)
                                            {
                                                Log.Information($"No ContainerType named: {x.ContainerName} found for legacyID: {x.ContainerTypeId.Value}, Fetching now...");
                                                ETLCTContainerType etlCTContainerType = CT_GetCTContainerTypesByService(options.ClientId, x.ContainerTypeId.Value).Result;

                                                if (etlCTContainerType != null)
                                                {
                                                    var ctContainerService = CT_GetCTContainerServiceByLegacyId((int)etlCTContainerType.ServiceId);
                                                    if (ctContainerService == null)
                                                    {
                                                        Log.Information($"No ContainerService named: {etlCTContainerType.ServiceName} found for legacyID: {etlCTContainerType.ServiceId}, Fetching now...");
                                                        ETLCTContainerService etlCTContainerService = CT_GetCTContainerServiceByLegacyId(options.ClientId, etlCTContainerType.ServiceId).Result;

                                                        if (etlCTContainerService != null)
                                                        {
                                                            var etlCTContainerServices = new List<ETLCTContainerService>();
                                                            etlCTContainerServices.Add(etlCTContainerService);
                                                            CT_AddOrEditCTContainerServices(etlCTContainerServices.Select(x => _mapper.Map<Framework.DataLayer.CTContainerService>(x)).ToList());
                                                        }
                                                    }

                                                    var etlCTContainerTypes = new List<ETLCTContainerType>();
                                                    etlCTContainerTypes.Add(etlCTContainerType);
                                                    CT_AddOrEditCTContainerTypes(etlCTContainerTypes.Select(x => _mapper.Map<Framework.DataLayer.CTContainerType>(x)).ToList(), etlCTContainerType.ServiceId);
                                                }
                                            }
                                        }

                                        if (x.ContainerId.HasValue && x.ContainerId.Value > 0)
                                        {
                                            var ctContainerTypeActual = CT_GetCTContainerTypeActualByLegacyId(x.ContainerId.Value);
                                            if (ctContainerTypeActual == null)
                                            {
                                                Log.Information($"No ContainerTypeActual named: {x.ContainerName} found for legacyID: {x.ContainerId.Value}, Fetching now...");
                                                ETLCTContainerTypeActual etlCTContainerTypeActual = null;

                                                var actuals = CT_GetCTContainerTypeActualssByContainerTypes(options.ClientId, x.ContainerTypeId.Value).Result;
                                                if (actuals != null)
                                                {
                                                    etlCTContainerTypeActual = actuals.Where(y => y.Id == x.ContainerId.Value).FirstOrDefault();
                                                }

                                                if (etlCTContainerTypeActual != null)
                                                {
                                                    var etlCTContainerTypeActuals = new List<ETLCTContainerTypeActual>();
                                                    etlCTContainerTypeActuals.Add(etlCTContainerTypeActual);
                                                    CT_AddOrEditCTContainerTypeActuals(etlCTContainerTypeActuals.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeActual>(x)).ToList(), x.ContainerTypeId.Value, serverOffsetInMinsFromUTC);
                                                }
                                            }


                                            var historyItemsResult = CT_GetCTContainerTypeActualHistoryItems(options.ClientId, x.ContainerId.Value);
                                            if (historyItemsResult != null && historyItemsResult.Result.ETLCTContainerTypeActualHistoryItems != null)
                                            {
                                                var historyItems = historyItemsResult.Result.ETLCTContainerTypeActualHistoryItems;
                                                CT_AddOrEditCTContainerTypeActualHistoryItemsAndGCTrays(historyItems.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeActualHistoryItem>(x)).ToList(), x.ContainerId.Value, serverOffsetInMinsFromUTC, isStopLifecycleHistory);
                                            }
                                        }
                                    });
                                }

                                CT_AddOrEditCTCaseContainers(ctCaseContainers.Select(x => _mapper.Map<Framework.DataLayer.CTCaseContainer>(x)).ToList(), serverOffsetInMinsFromUTC, isStopLifecycleHistory);
                                //For testing only:
                                //var dummy = _ctCaseService.CaseTesting();
                                //_ctCaseService.AddOrEditCTCaseContainers(dummy, isStopLifecycleHistory);
                                ctContainerTypeActualLegacyIds.AddRange(ctCaseContainers.Select(x => x.ContainerId).ToList());
                            }
                            catch (Exception e)
                            {
                                var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                                var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                                Log.Error(error);
                                description += error;
                            }
                        }
                        description += $"Feched CaseReference: {ctCase.CaseReference}, containing {ctCaseContainers.Count} Containers | ";
                    });

                    try
                    {
                        Log.Information("Update LC for Completed Cases STARTED");
                        CT_UpdateLCForCompletedCases(serverOffsetInMinsFromUTC);
                        Log.Information("Update LC for Completed Cases FINISHED");

                        // First set GCTrayActiveStatus:
                        Log.Information("Setting GCTray Active Status STARTED");
                        CT_SettingGCTrayActiveStatus(ctContainerTypeActualLegacyIds);
                        Log.Information("Setting GCTray Active Status FINISHED");
                        // Then do this:
                        // Only pulling cases that are in future and CartStatus is not Completed
                        Log.Information("Update Color Status for GCCases and GCTrays STARTED");
                        var localTime = ProfitOptics.Modules.Sox.Helpers.Helpers.GetLocalDateTime(DateTime.UtcNow);
                        Log.Information($"Local time is: {localTime}");
                        var gcCases = ctCasesToUse.Select(x => _mapper.Map<CTCase>(x)).ToList().Where(x =>
                            x.DueTimestamp > localTime &&
                            x.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE &&
                            x.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID &&
                            x.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE &&
                            x.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID
                        ).ToList();
                        if (gcCases != null && gcCases.Count > 0)
                        {
                            Log.Information($"Found: {gcCases.Count} GCCases for color update");
                            CT_UpdateColorStatusForGCCasesAndGCTrays(gcCases);
                        }
                        Log.Information("Update Color Status for GCCases and GCTrays FINISHED");

                        Log.Information("DELETE or CANCEL Cases STARTED");
                        CT_CancelOrDeleteCTCanceledCases(ctCasesToUse.Select(x => x.CaseReference).ToList(), serverOffsetInMinsFromUTC);
                        Log.Information("DELETE or CANCEL Cases FINISHED");
                    }
                    catch (Exception e)
                    {
                        var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                        var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                        Log.Error(error);
                        description += error;
                    }

                    //Log.Information("Create WorkWave Orders...");
                    //_ctCaseService.CreateWorkWaveOrdersForCTCases();
                }
                result.RowCount = ctCases.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTContainerActualsByContainerType(CensiTrackModuleOptions options, int serverOffsetInMinsFromUTC)
        {
            Log.Information("GetCTContainerActualsByContainerType - Fetch process started.");
            var result = new ETLResponseModel();
            var description = $"GetCTContainerActualsByContainerType - CycleId: {options.CycleId} | ";

            // Using options.ServiceId as a parameter for ContainerTypeId
            long containerTypeId = options.ServiceId;

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                var ctContainerTypeActuals = await CT_GetCTContainerTypeActualssByContainerTypes(options.ClientId, containerTypeId);
                if (ctContainerTypeActuals.Count > 0)
                {
                    CT_AddOrEditCTContainerTypeActuals(ctContainerTypeActuals.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeActual>(x)).ToList(), options.ServiceId, serverOffsetInMinsFromUTC);
                }
                result.RowCount = ctContainerTypeActuals.Count;
                result.Description = $"Feched {ctContainerTypeActuals.Count} ContainerTypeActuals for ContainerTypeID (parentId): {containerTypeId}";

                // Fetching actual history:
                if (!string.IsNullOrWhiteSpace(options.FetchChildObjects) && (options.FetchChildObjects.Equals("1") || options.FetchChildObjects.Equals("2")))
                {
                    ctContainerTypeActuals.ForEach(ctContainerTypeActual =>
                    {
                        options.Action = ((int)CTActionType.GetContainerTypeActualHistory).ToString();
                        options.ServiceId = ctContainerTypeActual.Id;
                        Execute(options);
                    });
                }
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<List<ETLCTLocationType>> CT_GetCTLocationTypes(int clientId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/admin/locations/location-types

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("admin", "locations", "location-types")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTLocationType>>();

            Log.Information($"Fetched {result.Count} CT LocationTypes.");

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTLocationTypes(int clientId, long facilityId, string userName, string password)
        {
            Log.Information("GetCTLocationTypes - Fetch process started.");
            var result = new ETLResponseModel();
            var description = "GetCTLocationTypes - ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = clientId, FacilityId = facilityId, UserId = userName, UserPassword = password });
            if (loginResponse.LoginSuccessful)
            {
                var ctLocationTypes = await CT_GetCTLocationTypes(clientId);
                if (ctLocationTypes.Count > 0)
                {
                    CT_AddOrEditCTLocationTypes(ctLocationTypes.Select(x => _mapper.Map<Framework.DataLayer.CTLocationType>(x)).ToList());
                }
                result.RowCount = ctLocationTypes.Count;
                description += $"Feched {ctLocationTypes.Count} LocationTypes";
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<ETLCTLocationsResponse> CT_GetCTLocations(int clientId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/admin/locations/location-initial-data

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("admin", "locations", "location-initial-data")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTLocationsResponse>();

            Log.Information($"Fetched {result.Locations.Count} CT Locations.");

            return result;
        }

        private async Task<ETLResponseModel> CT_GetContainerAssemblyMediaForActuals(CensiTrackModuleOptions options)
        {
            Log.Information("GetContainerAssemblyMediaForActuals - Fetch process started.");
            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetContainerAssemblyMediaForActuals - CycleId: {cycleId} | ";

            //TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                List<long> containerAssemblyActualLegacyIds = CT_GetContainerAssemblyActualIds();
                var statuses = CT_GetAssemblyStatuses();
                if (statuses != null && statuses.Count > 0)
                {
                    statuses.ForEach(assemblyStatus =>
                    {
                        Log.Information($"Fetching Container Found: {containerAssemblyActualLegacyIds.Count} CTContainerTypeActual items for CA media fetching");
                        var legacyId = assemblyStatus?.GCTray?.CTContainerTypeActual?.LegacyId ?? 0;
                        try
                        {
                            //legacyId = 1124; //FOR TESTING
                            //Graphics
                            ETLCTContainerAssembly etlCTContainerAssembly = CT_GetCTContainerAssemblyForActual(options.ClientId, legacyId).Result;

                            if (etlCTContainerAssembly != null && etlCTContainerAssembly.ContainerDetails != null && etlCTContainerAssembly.ContainerDetails.CensisSetHistoryId != null)
                            {
                                var containerAssemblyGraphics = CT_GetCTContainerAssemblyGraphics(options.ClientId, etlCTContainerAssembly.ContainerDetails.Id, (Guid)etlCTContainerAssembly.ContainerDetails.CensisSetHistoryId).Result;
                                if (containerAssemblyGraphics != null && containerAssemblyGraphics.AssemblyGraphics != null && containerAssemblyGraphics.AssemblyGraphics.Count > 0)
                                {
                                    containerAssemblyGraphics.AssemblyGraphics.ForEach(assemblyGraphic => {
                                        CT_AddCTAssemblyGraphic(assemblyGraphic, assemblyStatus.Id, legacyId);
                                    });

                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Error($"{e.Message}, InnerExc: {e.InnerException}");
                        }

                        try
                        {
                            //Count Sheets
                            ETLCTContainerAssemblyCountSheet countSheets = CT_GetCTContainerAssemblyCountSheets(options.ClientId, legacyId).Result;
                            if (countSheets.Sets != null && countSheets.Sets.Count() > 0 && countSheets.Items != null && countSheets.Items.Count() > 0)
                            {
                                countSheets.Items.ForEach(countSheet =>
                                {
                                    CT_AddCTAssemblyCountSheet(countSheet, countSheets.Sets[0]?.SetName, assemblyStatus.Id);
                                });
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Error($"{e.Message}, InnerExc: {e.InnerException}");
                        }

                    });
                }


                //if (containerAssemblyActualLegacyIds != null && containerAssemblyActualLegacyIds.Count > 0)
                //{
                //    Log.Information($"Found: {containerAssemblyActualLegacyIds.Count} CTContainerTypeActual items for CA media fetching");
                //    var test = 1124;
                //    containerAssemblyActualLegacyIds.ForEach(actualLegacyId =>
                //    {
                //         Log.Information($"Fetching Container Found: {containerAssemblyActualLegacyIds.Count} CTContainerTypeActual items for CA media fetching");

                //         ETLCTContainerAssembly etlCTContainerAssembly = _service.GetCTContainerAssemblyForActual(options.ClientId, test/*actualLegacyId*/).Result;
                //         var containerAssemblyGraphics = _service.GetCTContainerAssemblyGraphics(options.ClientId, etlCTContainerAssembly.ContainerDetails.Id, etlCTContainerAssembly.ContainerDetails.CensisSetHistoryId).Result;
                //         if (containerAssemblyGraphics != null && containerAssemblyGraphics.AssemblyGraphics != null && containerAssemblyGraphics.AssemblyGraphics.Count > 0)
                //         {
                //             containerAssemblyGraphics.AssemblyGraphics.ForEach(assemblyGraphic => {
                //                 _ctContainerService.AddCTAssemblyGraphic(assemblyGraphic);
                //             });

                //         }
                //    });
                //}
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            result.Description = description;
            Log.Information("Fetch process ended.");
            return result;
        }

        private async Task<ETLResponseModel> CT_SyncLCForPastCases(CensiTrackModuleOptions ctOpts)
        {
            Log.Information("SyncLCForPastCases - Fetch process started.");
            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"SyncLCForPastCases - CycleId: {cycleId} | ";

            if (ctOpts.BypassContainerServices == null || !ctOpts.BypassContainerServices.Equals("1"))
            {
                CT_SyncLCForPastCases();
            }

            if (ctOpts.BypassContainerServices == null || !ctOpts.BypassContainerServices.Equals("2"))
            {
                CT_SyncColorForPastCases();
            }

            result.RowCount = 0;
            result.Description = description;

            return result;
        }

        public async Task<ETLResponseModel> CT_GetQualityFeedResults(int clientId, string userName, string password, int daysCount, int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            Log.Information("GetQualityFeedResults - Fetch process started.");
            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetQualityFeedResults - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = clientId, FacilityId = 0, UserId = userName, UserPassword = password });
            if (loginResponse.LoginSuccessful)
            {
                Log.Information("Fetching GetQualityFeedResults...");
                var request = new ETLCTQualityFeedRequest()
                {
                    Id = 1241,
                    Fields = new List<ETLQualityFeedFieldsItem>()
                    {
                        new ETLQualityFeedFieldsItem()
                        {
                            FieldId = 1161,
                            ReportId = 0,
                            DisplayName = "Quality Event",
                            DefaultOrder = 1,
                            DisplayFlag = "Y",
                            SortOrder = 1,
                            ValueList = string.Empty,
                            DataType = 0
                        }
                    },
                    ReportParameters = new List<ETLQualityFeedReportParameterItem>()
                    {
                        new ETLQualityFeedReportParameterItem()
                        {
                            ParameterId = 2003,
                            Value = 1
                        },
                        new ETLQualityFeedReportParameterItem()
                        {
                            ParameterId = 1030,
                            DateQuantity = daysCount,
                            DateType = "dd",
                            IsRuntime = true
                        }
                    }
                };

                var qualityFeedReports = Enum.GetValues(typeof(CTQualityFeedReportType)).Cast<CTQualityFeedReportType>().ToList();
                foreach (var qFeed in qualityFeedReports)
                {
                    var qFeedReportList = new List<CTQualityFeedItem>();
                    var qFeedItemsGraphicsList = new List<CTQualityFeedItemGraphic>();
                    Log.Information($"-Fetching {qFeed.GetDescription()} report type...");
                    request.Fields[0].ValueList = qFeed.GetDescription();
                    ETLCTQualityFeedResponse response = await CT_GetQualityFeedResponse(clientId, request);
                    if (response.Data.Count > 0)
                    {
                        Log.Information($"--Fetched {response.Data.Count}  ETLCTQualityFeedResults");

                        response.Data.ForEach(dataItem =>
                        {
                            if (dataItem.Metadata != null && !dataItem.Metadata.IsGrandTotalRecord && !dataItem.Metadata.IsSubtotalRecord)
                            {
                                var ctQualityFeedItem = new CTQualityFeedItem();
                                if (dataItem.Fields != null && dataItem.Fields.Count > 0)
                                {
                                    ctQualityFeedItem.ReportType = dataItem.Fields[0].Data;

                                    if (dataItem.Fields.Count > 1)
                                    {
                                        DateTime updatedDate = DateTime.MinValue;
                                        if (DateTime.TryParse(dataItem.Fields[1].Data, out updatedDate) && updatedDate > DateTime.MinValue)
                                        {
                                            ctQualityFeedItem.UpdatedDate = updatedDate;
                                            ctQualityFeedItem.UpdatedDateUtc = updatedDate.AddMinutes(minsToAdd);
                                        }

                                        if (dataItem.Fields.Count > 2)
                                        {
                                            ctQualityFeedItem.ReportedBy = dataItem.Fields[2].Data;

                                            if (dataItem.Fields.Count > 3)
                                            {
                                                ctQualityFeedItem.ResponsibleParty = dataItem.Fields[3].Data;

                                                if (dataItem.Fields.Count > 4)
                                                {
                                                    ctQualityFeedItem.AssetName = dataItem.Fields[4].Data;

                                                    if (dataItem.Fields.Count > 5)
                                                    {
                                                        DateTime eventDate = DateTime.MinValue;
                                                        if (DateTime.TryParse(dataItem.Fields[5].Data, out eventDate) && eventDate > DateTime.MinValue)
                                                        {
                                                            ctQualityFeedItem.EventDate = eventDate;
                                                            ctQualityFeedItem.EventDateUtc = eventDate.AddMinutes(minsToAdd);
                                                        }

                                                        if (dataItem.Fields.Count > 6)
                                                        {
                                                            ctQualityFeedItem.Comments = dataItem.Fields[6].Data;

                                                            if (dataItem.Fields.Count > 7)
                                                            {
                                                                long legacyId = 0;
                                                                if (long.TryParse(dataItem.Fields[7].Data, out legacyId) && legacyId > 0)
                                                                {
                                                                    ctQualityFeedItem.LegacyId = legacyId;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (ctQualityFeedItem.LegacyId > 0)
                                {
                                    Log.Information($"---Successfully parsed item with legacyId: {ctQualityFeedItem.LegacyId}");
                                    qFeedReportList.Add(ctQualityFeedItem);
                                    Log.Information($"----Preparing to fech Graphic info...");
                                    List<ETLCTQualityFeedItemGraphic> qFeedItemGraphics = CT_GetCTBIQualityFeedItemGraphics(clientId, ctQualityFeedItem.LegacyId).Result;
                                    if (qFeedItemGraphics != null)
                                    {
                                        Log.Information($"Fetched QualityFeed Graphics for LegacyId: {ctQualityFeedItem.LegacyId}");
                                        var graphics = qFeedItemGraphics.ToList().Select(x => _mapper.Map<Framework.DataLayer.CTQualityFeedItemGraphic>(x)).ToList();
                                        graphics.ForEach(x =>
                                        {
                                            x.LoadId = ctQualityFeedItem.LegacyId;
                                        });
                                        qFeedItemsGraphicsList.AddRange(graphics);
                                    }
                                    description += $"Fetched QualityFeed Graphics for LegacyId: {ctQualityFeedItem.LegacyId} | ";
                                }
                                else
                                {
                                    Log.Error($"---Unable to parse item: {JsonConvert.SerializeObject(dataItem)}");
                                }
                            }
                        });

                        if (qFeedReportList.Count > 0)
                        {
                            CT_AddOrEditCTQualityFeedReport(qFeedReportList);
                        }
                        if(qFeedItemsGraphicsList.Count > 0)
                        {
                            CT_AddOrEditCTQualityFeedItemGraphics(qFeedItemsGraphicsList);
                        }
                    }
                    else
                    {
                        Log.Information($"--Fetched NO ETLCTQualityFeedResults");
                    }
                }

                //if (ctBILoadResults.Count > 0)
                //{
                //    //_ctBIService.AddOrEditCTBILoadResults(ctBILoadResults.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResult>(x)).ToList());
                //    Log.Information($"Fetched {ctBILoadResults.Count} CTBILoadResults");

                //    ctBILoadResults.ForEach(ctBILoadResult =>
                //    {
                //        Log.Information($"-Processing CTBILoadResult, LoadId: {ctBILoadResult.LoadId}, Barcode: {ctBILoadResult.LoadBarcode}...");
                //        bool isImportSuccessfull = false;
                //        try
                //        {
                //            // CTBILoadResult Details & CTBILoadResultContent:
                //            ETLCTBILoadResultContentResponse ctBILoadResultContentResponse = _service.GetCTBILoadResultContents(options.ClientId, ctBILoadResult.LoadId).Result;
                //            Log.Information($"-Fetching CTBILoadResult Contents for loadId: {ctBILoadResult.LoadId}...");
                //            if (ctBILoadResultContentResponse != null)
                //            {
                //                Log.Information($"-Updating CTBILoadResult Items for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}...");
                //                isImportSuccessfull = _ctBIService.AddOrEditCTBILoadResult(_mapper.Map<Framework.DataLayer.CTBILoadResult>(ctBILoadResultContentResponse.LoadHeader), _settings.ServerOffsetInMinsFromUTC);

                //                if (!isImportSuccessfull)
                //                {
                //                    Log.Information($"-Import of CTBILoadResult NOT YET Successfull LoadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}...");
                //                    if (ctBILoadResultContentResponse.LoadHeader.LoadId > 0)
                //                    {
                //                        _ctBIService.AddOrEditCTBILoadResultContents(ctBILoadResultContentResponse.LoadItems.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResultContent>(x)).ToList(), ctBILoadResultContentResponse.LoadHeader.LoadId, _settings.ServerOffsetInMinsFromUTC);
                //                    }
                //                    else
                //                    {
                //                        Log.Information($"-LoadId is still 0! Skipping AddOrEditCTBILoadResultContents for now...");
                //                    }
                //                }
                //            }
                //            if (!isImportSuccessfull)
                //            {
                //                description += $"Fetched CTBILoadResult Items for loadId: { ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultContentResponse.LoadItems.Count} Contents | ";

                //                // CTBILoadResult Indicators:
                //                Log.Information($"Fetching CTBILoadResultIndicator Items for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}...");
                //                ETLCTBILoadResultIndicatorResponse ctBILoadResultIndicatorResponse = _service.GetCTBILoadResultIndicators(options.ClientId, ctBILoadResultContentResponse.LoadHeader.LoadId).Result;
                //                if (ctBILoadResultIndicatorResponse != null)
                //                {
                //                    Log.Information($"Fetched CTBILoadResult Indicators for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultIndicatorResponse.RequiredIndicators.Count} Indicators");
                //                    _ctBIService.AddOrEditCTBILoadResultIndicators(ctBILoadResultIndicatorResponse.RequiredIndicators.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResultIndicator>(x)).ToList(), ctBILoadResultContentResponse.LoadHeader.LoadId, _settings.ServerOffsetInMinsFromUTC);
                //                }
                //                description += $"Fetched CTBILoadResult Indicators for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultIndicatorResponse.RequiredIndicators.Count} Indicators | ";

                //                // CTBILoadResult Graphics:
                //                Log.Information($"Fetching CTBILoadResultGraphic Items for loadId: {ctBILoadResult.LoadId}...");
                //                List<ETLCTBILoadResultGraphic> ctBILoadResultGraphics = _service.GetCTBILoadResultGraphics(options.ClientId, ctBILoadResultContentResponse.LoadHeader.LoadId).Result;
                //                if (ctBILoadResultGraphics != null)
                //                {
                //                    Log.Information($"Fetched CTBILoadResult Graphics for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultGraphics.Count} Graphics");
                //                    _ctBIService.AddOrEditCTBILoadResultGraphics(ctBILoadResultGraphics.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResultGraphic>(x)).ToList(), ctBILoadResultContentResponse.LoadHeader.LoadId, _settings.ServerOffsetInMinsFromUTC);
                //                }
                //                description += $"Fetched CTBILoadResult Graphics for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultGraphics.Count} Graphics | ";
                //                isImportSuccessfull = true;
                //                _ctBIService.UpdateCTLoadResultImportStatus(ctBILoadResult.LoadId, isImportSuccessfull);
                //            }
                //            else
                //            {
                //                var msg = $"CTBILoadResult with LoadId: {ctBILoadResult.LoadId} already importet into GC!";
                //                Log.Information(msg);
                //                description += msg;
                //            }
                //        }
                //        catch (Exception e)
                //        {
                //            var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                //            var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                //            Log.Error(error);
                //            description += error;
                //        }
                //    });
                //}
                //result.RowCount = ctBILoadResults.Count;
                //result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            return result;
        }

        public async Task<ETLCTContainerAssembly> CT_GetCTContainerAssemblyForActual(int clientId, long legacyId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers-assembly/1198

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers-assembly", legacyId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerAssembly>();

            Log.Information($"Fetched CT ETLCTContainerType.");

            return result;
        }

        public async Task<ETLCTCurrentAssemblyGraphics> CT_GetCTContainerAssemblyGraphics(int clientId, long containerTypeId, Guid censisSetHistoryId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers-assembly/graphics?censisSetHistoryId=2125c490-7987-40b8-8cea-8c84dc566340&containerTypeId=1124

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers-assembly", "graphics")
            //.WithOAuthBearerToken("my_oauth_token")
            .SetQueryParams(new { censisSetHistoryId, containerTypeId })
            .GetAsync()
            .ReceiveJson<ETLCTCurrentAssemblyGraphics>();

            Log.Information($"Fetched CT ETLCTAssemblyGraphics.");

            return result;
        }

        private async Task CT_GetCTAssemblyGraphicMedia(int clientId, ETLResponseModel result, List<CTAssemblyGraphic> assemblyGraphics)
        {
            if (assemblyGraphics != null && assemblyGraphics.Count > 0)
            {
                foreach (var graphic in assemblyGraphics)
                {
                    Log.Information($"- Fetching Bytes for graphic ID: {graphic.GraphicId}.");
                    byte[] graphicBytes = null;
                    try
                    {
                        graphicBytes = await CT_GetCTGraphicImage(clientId, graphic.GraphicId);
                    }
                    catch (Exception)
                    {

                    }

                    if (graphicBytes != null)
                    {
                        Log.Information($"-- Bytes found for graphic ID: {graphic.GraphicId}.");
                        CT_UpdateCTAssemblyGraphicsGraphic(graphic.Id, graphicBytes);
                        Log.Information($"-- Updated CTAssemblyGraphicsGraphic for graphic ID: {graphic.GraphicId}.");
                    }
                    else
                    {
                        Log.Information($"-- NO Bytes found for graphic ID: {graphic.GraphicId}.");
                    }
                    result.RowCount = 1;

                }
            }
        }

        private async Task CT_QualityFeedItemGraphicMedia(int clientId, ETLResponseModel result, List<CTQualityFeedItemGraphic> qualityFeedItemGraphics)
        {
            if (qualityFeedItemGraphics != null && qualityFeedItemGraphics.Count > 0)
            {
                foreach (var graphic in qualityFeedItemGraphics)
                {
                    Log.Information($"- Fetching Bytes for graphic ID: {graphic.GraphicId}.");
                    byte[] graphicBytes = null;
                    try
                    {
                        graphicBytes = await CT_GetCTGraphicImage(clientId, graphic.GraphicId);
                    }
                    catch (Exception)
                    {

                    }

                    if (graphicBytes != null)
                    {
                        Log.Information($"-- Bytes found for graphic ID: {graphic.GraphicId}.");
                        CT_UpdateCTQualityFeedItemGraphic(graphic.Id, graphicBytes);
                        Log.Information($"-- Updated CTQualityFeedItemGraphic for graphic ID: {graphic.GraphicId}.");
                    }
                    else
                    {
                        Log.Information($"-- NO Bytes found for graphic ID: {graphic.GraphicId}.");
                    }
                    result.RowCount = 1;

                }
            }
        }

        public async Task<ETLResponseModel> CT_GetCTContainerTypeGraphicMediaAndTrayInductionSyncAndLCUpdate(int clientId, long facilityId, string userName, string password, bool syncAllActuals, bool updateAllTrays)
        {
            Log.Information("GetCTContainerTypeGraphicMedia - Fetch process started.");
            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTContainerTypeGraphicMedia - CycleId: {cycleId} | ";
            bool isWithoutImage = false;

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = clientId, FacilityId = facilityId, UserId = userName, UserPassword = password });
            if (loginResponse.LoginSuccessful)
            {
                try
                {
                    // Part 1 - CTContainerTypeGraphics
                    isWithoutImage = true;
                    Log.Information("Preparing to fetch bytes for CTContainerTypeGraphics.");
                    var graphicsWithoutMedia = CT_GetCTContainerTypeGraphic(isWithoutImage);
                    Log.Information($"{graphicsWithoutMedia.Count} CTContainerTypeGraphics found in GC DB.");
                    await CT_GetCTContainerTypeGraphicMedia(clientId, result, graphicsWithoutMedia);
                }
                catch (Exception e)
                {
                    var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                    var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                    Log.Error(error);
                    description += error;
                }

                try
                {
                    // Part 2 - CTLoadIndicatorGraphics
                    bool isLoadMeadiaWithoutImage = true;
                    Log.Information("Preparing to fetch bytes for CTLoadResultGraphics.");
                    var loadResultGraphicsWithoutMedia = CT_GetCTLoadResultGraphics(isLoadMeadiaWithoutImage);
                    Log.Information($"{loadResultGraphicsWithoutMedia.Count} CTLoadResultGraphics found in GC DB.");
                    await CT_GetCTLoadResultGraphicMedia(clientId, result, loadResultGraphicsWithoutMedia);
                }
                catch (Exception e)
                {
                    var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                    var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                    Log.Error(error);
                    description += error;
                }

                try
                {
                    // Part 3 - AssemblyGraphics
                    isWithoutImage = true;
                    Log.Information("Preparing to fetch bytes for CTAssemblyGraphics.");
                    var assemblyGraphics = CT_GetCTAssemblyGraphics(isWithoutImage);
                    Log.Information($"{assemblyGraphics.Count} CTAssemblyGraphics found in GC DB.");
                    await CT_GetCTAssemblyGraphicMedia(clientId, result, assemblyGraphics);
                }
                catch (Exception e)
                {
                    var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                    var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                    Log.Error(error);
                    description += error;
                }

                try
                {
                    // Part 4 - CTQualityFeedItemGrapthics
                    isWithoutImage = true;
                    Log.Information("Preparing to fetch bytes for CTQualityFeedItemGrapthics.");
                    var qualityFeedItemGrapthics = CT_GetCTQualityFeedItemGraphic(isWithoutImage);
                    Log.Information($"{qualityFeedItemGrapthics.Count} CTQualityFeedItemGrapthics found in GC DB.");
                    await CT_QualityFeedItemGraphicMedia(clientId, result, qualityFeedItemGrapthics);
                }
                catch (Exception e)
                {
                    var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                    var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                    Log.Error(error);
                    description += error;
                }
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            try
            {
                Log.Information("Tray Induction synchronization STARTED");
                UpdateTrayInductionSynchronization(syncAllActuals);
                Log.Information("Tray Induction synchronization FINISHED");
            }
            catch (Exception e)
            {
                var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                Log.Error(error);
                description += error;
            }

            try
            {
                Log.Information("Tray LifeCycle update STARTED");
                UpdateGCTrayLifecycles(updateAllTrays);
                Log.Information("Tray LifeCycle update FINISHED");
            }
            catch (Exception e)
            {
                var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                Log.Error(error);
                description += error;
            }

            result.Description = description;
            Log.Information("Fetch process ended.");
            return result;
        }

        public async Task<List<ETLCTBILoadResult>> CT_GetCTBILoadResults(int clientId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/load-results/filter

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("load-results", "filter")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTBILoadResult>>();

            Log.Information($"Fetched {result.Count} CT BI LoadResults");

            return result;
        }

        public async Task<ETLCTBILoadResultContentResponse> CT_GetCTBILoadResultContents(int clientId, long loadId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/sterilization/load-contents/1035

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("sterilization", "load-contents", loadId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTBILoadResultContentResponse>();

            Log.Information($"Fetched {result.LoadItems.Count} CT BI LoadResult Items for LoadId: {loadId}");

            return result;
        }

        public async Task<ETLResponseModel> CT_GetCTLocations(int clientId, long facilityId, string userName, string password)
        {
            Log.Information("GetCTLocations - Fetch process started.");
            var result = new ETLResponseModel();
            var description = "GetCTLocations - ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = clientId, FacilityId = facilityId, UserId = userName, UserPassword = password });
            if (loginResponse.LoginSuccessful)
            {
                var ctLocations = CT_GetCTLocations(clientId).Result.Locations;
                if (ctLocations.Count > 0)
                {
                    CT_AddOrEditCTLocations(ctLocations.Select(x => _mapper.Map<Framework.DataLayer.CTLocation>(x)).ToList());
                }
                result.RowCount = ctLocations.Count;
                description += $"Feched {ctLocations.Count} Locations";
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<ETLResponseModel> CT_GetContainerTypeActualHistory(int clientId, long facilityId, string userName, string password, int serverOffsetInMinsFromUTC, bool isStopLifecycleHistory, string cycleId, long actualId)
        {
            Log.Information("GetContainerTypeActualHistory - Fetch process started.");
            var result = new ETLResponseModel();
            var description = $"GetContainerTypeActualHistory - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = clientId, FacilityId = facilityId, UserId = userName, UserPassword = password });
            if (loginResponse.LoginSuccessful)
            {
                var response = await CT_GetCTContainerTypeActualHistoryItems(clientId, actualId);
                List<ETLCTContainerTypeActualHistoryItem> ctActualHistoryItems = response.ETLCTContainerTypeActualHistoryItems;
                if (ctActualHistoryItems != null)
                {
                    if (ctActualHistoryItems.Count > 0)
                    {
                        CT_AddOrEditCTContainerTypeActualHistoryItemsAndGCTrays(ctActualHistoryItems.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeActualHistoryItem>(x)).ToList(), actualId, serverOffsetInMinsFromUTC, isStopLifecycleHistory);
                    }
                    result.RowCount = ctActualHistoryItems.Count;
                    result.Description = $"Feched {ctActualHistoryItems.Count} CTContainerTypeActualHistoryItems for ActualId (parentId): {actualId}";
                }

                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            Log.Information("Fetch process ended.");

            return result;
        }

        public async Task<List<ETLCTContainerTypeGraphic>> CT_GetCTContainerTypeGraphic(int clientId, long containerTypeId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/graphics/1033

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "graphics", containerTypeId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTContainerTypeGraphic>>();

            Log.Information($"Fetched {result.Count} CT ContainerTypeGraphics.");

            return result;
        }

        public async Task<byte[]> CT_GetCTGraphicImage(int clientId, long graphicId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/graphics/product-image?GraphicId=1013&IsGlobal=false

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("graphics", "product-image")
            .SetQueryParams(new { GraphicId = graphicId, IsGlobal = false })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveBytes();

            Log.Information($"Fetched CT ContainerTypeGraphicImage.");

            return result;
        }


        //public async Task<ETLResponseModel> CT_GetCTContainerTypeGraphic(CensiTrackModuleOptions options)
        //{
        //    Log.Information("GetCTContainerTypeGraphic - Fetch process started.");
        //    var result = new ETLResponseModel();
        //    var description = $"GetCTContainerTypeGraphic - CycleId: {options.CycleId} | ";

        //    // TODO: Adapt this once CensiTrack changes Authentication:
        //    var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
        //    if (loginResponse.LoginSuccessful)
        //    {
        //        var ctContainerTypeGraphics = await CT_GetCTContainerTypeGraphic(options.ClientId, options.ServiceId);
        //        if (ctContainerTypeGraphics.Count > 0)
        //        {
        //            CT_AddOrEditCTContainerTypeGraphics(ctContainerTypeGraphics.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeGraphic>(x)).ToList(), options.ServiceId);
        //        }
        //        result.RowCount = ctContainerTypeGraphics.Count;
        //    }
        //    else
        //    {
        //        result.Message = loginResponse.Message;
        //    }

        //    Log.Information("Fetch process ended.");

        //    return result;
        //}

        public async Task<ETLCTContainerAssemblyCountSheet> CT_GetCTContainerAssemblyCountSheets(int clientId, long containerTypeId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/count-sheet/set/1124

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("count-sheet", "set", containerTypeId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerAssemblyCountSheet>();

            Log.Information($"Fetched CT ETLCTAssemblyGraphics.");

            return result;
        }

        private async Task<ETLResponseModel> CT_GetCTBILoadResultsAndTrayInductionSync(CensiTrackModuleOptions options, int serverOffsetInMinsFromUTC)
        {
            Log.Information("GetCTBILoadResultsAndTrayInductionSync - Fetch process started.");
            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"GetCTBILoadResultsAndTrayInductionSync - CycleId: {cycleId} | ";

            // TODO: Adapt this once CensiTrack changes Authentication:
            var loginResponse = await CT_Login(new ETLCTLoginModel() { ClientId = options.ClientId, FacilityId = options.FacilityId, UserId = options.UserName, UserPassword = options.Password });
            if (loginResponse.LoginSuccessful)
            {
                Log.Information("Fetching CTBILoadResults...");
                var ctBILoadResults = await CT_GetCTBILoadResults(options.ClientId);

                if (ctBILoadResults.Count > 0)
                {
                    //_ctBIService.AddOrEditCTBILoadResults(ctBILoadResults.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResult>(x)).ToList());
                    Log.Information($"Fetched {ctBILoadResults.Count} CTBILoadResults");

                    ctBILoadResults.ForEach(ctBILoadResult =>
                    {
                        Log.Information($"-Processing CTBILoadResult, LoadId: {ctBILoadResult.LoadId}, Barcode: {ctBILoadResult.LoadBarcode}...");
                        bool isImportSuccessfull = false;
                        try
                        {
                            // CTBILoadResult Details & CTBILoadResultContent:
                            ETLCTBILoadResultContentResponse ctBILoadResultContentResponse = CT_GetCTBILoadResultContents(options.ClientId, ctBILoadResult.LoadId).Result;
                            Log.Information($"-Fetching CTBILoadResult Contents for loadId: {ctBILoadResult.LoadId}...");
                            if (ctBILoadResultContentResponse != null)
                            {
                                Log.Information($"-Updating CTBILoadResult Items for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}...");
                                isImportSuccessfull = CT_AddOrEditCTBILoadResult(_mapper.Map<Framework.DataLayer.CTBILoadResult>(ctBILoadResultContentResponse.LoadHeader), serverOffsetInMinsFromUTC);

                                if (!isImportSuccessfull)
                                {
                                    Log.Information($"-Import of CTBILoadResult NOT YET Successfull LoadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}...");
                                    if (ctBILoadResultContentResponse.LoadHeader.LoadId > 0)
                                    {
                                        CT_AddOrEditCTBILoadResultContents(ctBILoadResultContentResponse.LoadItems.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResultContent>(x)).ToList(), ctBILoadResultContentResponse.LoadHeader.LoadId, serverOffsetInMinsFromUTC);
                                    }
                                    else
                                    {
                                        Log.Information($"-LoadId is still 0! Skipping AddOrEditCTBILoadResultContents for now...");
                                    }
                                }
                            }
                            if (!isImportSuccessfull)
                            {
                                description += $"Fetched CTBILoadResult Items for loadId: { ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultContentResponse.LoadItems.Count} Contents | ";

                                // CTBILoadResult Indicators:
                                Log.Information($"Fetching CTBILoadResultIndicator Items for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}...");
                                ETLCTBILoadResultIndicatorResponse ctBILoadResultIndicatorResponse = CT_GetCTBILoadResultIndicators(options.ClientId, ctBILoadResultContentResponse.LoadHeader.LoadId).Result;
                                if (ctBILoadResultIndicatorResponse != null)
                                {
                                    Log.Information($"Fetched CTBILoadResult Indicators for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultIndicatorResponse.RequiredIndicators.Count} Indicators");
                                    CT_AddOrEditCTBILoadResultIndicators(ctBILoadResultIndicatorResponse.RequiredIndicators.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResultIndicator>(x)).ToList(), ctBILoadResultContentResponse.LoadHeader.LoadId, serverOffsetInMinsFromUTC);
                                }
                                description += $"Fetched CTBILoadResult Indicators for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultIndicatorResponse.RequiredIndicators.Count} Indicators | ";

                                // CTBILoadResult Graphics:
                                Log.Information($"Fetching CTBILoadResultGraphic Items for loadId: {ctBILoadResult.LoadId}...");
                                List<ETLCTBILoadResultGraphic> ctBILoadResultGraphics = CT_GetCTBILoadResultGraphics(options.ClientId, ctBILoadResultContentResponse.LoadHeader.LoadId).Result;
                                if (ctBILoadResultGraphics != null)
                                {
                                    Log.Information($"Fetched CTBILoadResult Graphics for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultGraphics.Count} Graphics");
                                    CT_AddOrEditCTBILoadResultGraphics(ctBILoadResultGraphics.Select(x => _mapper.Map<Framework.DataLayer.CTBILoadResultGraphic>(x)).ToList(), ctBILoadResultContentResponse.LoadHeader.LoadId, serverOffsetInMinsFromUTC);
                                }
                                description += $"Fetched CTBILoadResult Graphics for loadId: {ctBILoadResultContentResponse.LoadHeader.LoadId}, containing {ctBILoadResultGraphics.Count} Graphics | ";
                                isImportSuccessfull = true;
                                CT_UpdateCTLoadResultImportStatus(ctBILoadResult.LoadId, isImportSuccessfull);
                            }
                            else
                            {
                                var msg = $"CTBILoadResult with LoadId: {ctBILoadResult.LoadId} already importet into GC!";
                                Log.Information(msg);
                                description += msg;
                            }
                        }
                        catch (Exception e)
                        {
                            var innerExc = e.InnerException != null ? e.InnerException.Message : "-no info-";
                            var error = $"|| ERROR: {e.Message}, INNER EXCEPTION: {innerExc}, STACK TRACE: {e.StackTrace} ||";
                            Log.Error(error);
                            description += error;
                        }
                    });
                }
                result.RowCount = ctBILoadResults.Count;
                result.Description = description;
            }
            else
            {
                result.Message = loginResponse.Message;
            }

            //try
            //{
            //    Log.Information("Tray Induction synchronization STARTED");
            //    CT_UpdateTrayInductionSynchronization(options.RunAnyway);
            //    Log.Information("Tray Induction synchronization FINISHED");
            //}
            //catch (Exception e)
            //{
            //    var error = $"|| ERROR: {e.Message} ||";
            //    Log.Error(error);
            //    description += error;
            //}

            return result;
        }

        private async Task<ETLResponseModel> CT_SyncTimestampResults(CensiTrackModuleOptions options, int serverOffsetInMinsFromUTC)
        {
            Log.Information("SyncTimestampResults - Fetch process started.");
            var result = new ETLResponseModel();
            string cycleId = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            var description = $"SyncTimestampResults - CycleId: {cycleId} | ";

            CT_SyncTimestamps(serverOffsetInMinsFromUTC);

            result.RowCount = 0;
            result.Description = description;

            return result;
        }

        private async Task CT_GetCTContainerTypeGraphicMedia(int clientId, ETLResponseModel result, List<CTContainerTypeGraphic> graphicsWithoutMedia)
        {
            if (graphicsWithoutMedia != null && graphicsWithoutMedia.Count > 0)
            {
                foreach (var ctContainerTypeGraphic in graphicsWithoutMedia)
                {
                    Log.Information($"- Fetching Bytes for graphic ID: {ctContainerTypeGraphic.GraphicId}.");
                    byte[] ctContainerTypeImage = null;
                    try
                    {
                        ctContainerTypeImage = await CT_GetCTGraphicImage(clientId, ctContainerTypeGraphic.GraphicId);
                    }
                    catch (Exception e)
                    {

                    }

                    if (ctContainerTypeImage != null)
                    {
                        Log.Information($"-- Bytes found for graphic ID: {ctContainerTypeGraphic.GraphicId}.");
                        CT_UpdateCTContainerTypeGraphic(ctContainerTypeGraphic.Id, ctContainerTypeImage);
                        Log.Information($"-- Updated CTContainerTypeGraphic for graphic ID: {ctContainerTypeGraphic.GraphicId}.");
                    }
                    else
                    {
                        Log.Information($"-- NO Bytes found for graphic ID: {ctContainerTypeGraphic.GraphicId}.");
                    }
                    result.RowCount = 1;

                }
            }
        }

        public async Task<byte[]> CT_GetCTLoadResultGraphicImage(int clientId, long graphicId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/graphics/indicators/graphics/1001

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("graphics", "indicators", "graphics", graphicId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveBytes();

            Log.Information($"Fetched CT LoadResultGraphicImage.");

            return result;
        }

        private async Task CT_GetCTLoadResultGraphicMedia(int clientId, ETLResponseModel result, List<CTBILoadResultGraphic> loadResultGraphicsWithoutMedia)
        {
            if (loadResultGraphicsWithoutMedia != null && loadResultGraphicsWithoutMedia.Count > 0)
            {
                foreach (var graphic in loadResultGraphicsWithoutMedia)
                {
                    Log.Information($"- Fetching Bytes for graphic ID: {graphic.GraphicId}.");
                    byte[] ctBILoadResultImage = null;
                    try
                    {
                        ctBILoadResultImage = await CT_GetCTLoadResultGraphicImage(clientId, graphic.GraphicId);
                    }
                    catch (Exception e)
                    {

                    }

                    if (ctBILoadResultImage != null)
                    {
                        Log.Information($"-- Bytes found for graphic ID: {graphic.GraphicId}.");
                        CT_UpdateCTLoadResultGraphic(graphic.Id, ctBILoadResultImage);
                        Log.Information($"-- Updated CTLoadResultGraphic for graphic ID: {graphic.GraphicId}.");
                    }
                    else
                    {
                        Log.Information($"-- NO Bytes found for graphic ID: {graphic.GraphicId}.");
                    }
                    result.RowCount = 1;

                }
            }
        }

        public async Task<ETLCTBILoadResultIndicatorResponse> CT_GetCTBILoadResultIndicators(int clientId, long loadId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/load-results/indicators/1035

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("load-results", "indicators", loadId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTBILoadResultIndicatorResponse>();

            Log.Information($"Fetched {result.RequiredIndicators.Count} CT BI LoadResult Indicators for LoadId: {loadId}");

            return result;
        }

        public async Task<List<ETLCTBILoadResultGraphic>> CT_GetCTBILoadResultGraphics(int clientId, long loadId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/graphics/indicators/1035/graphics

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("graphics", "indicators", loadId, "graphics")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTBILoadResultGraphic>>();

            Log.Information($"Fetched {result.Count} CT BI LoadResult Graphics for LoadId: {loadId}");

            return result;
        }

        public async Task<List<ETLCTQualityFeedItemGraphic>> CT_GetCTBIQualityFeedItemGraphics(int clientId, long legacyId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/qualityfeedback/graphics/10000258

            var result = await string.Format(Helpers.Constants.CT_CensitrackApiEndpoint, clientId)
            .AppendPathSegments("qualityfeedback", "graphics", legacyId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTQualityFeedItemGraphic>>();

            Log.Information($"Fetched {result.Count} CT QualityFeed Graphics for LoadId: {legacyId}");

            return result;
        }

        #endregion CT ETL Services

        #region CT GC Services

        public void CT_AddOrEditCTCases(List<CTCase> ctCases)
        {
            var newCases = new List<string>();

            // hardcoding it until cases start having this!
            long facilityId = 1001;

            ctCases.ForEach(ctCase =>
            {
                if (ctCase.DestinationFacilityId.HasValue)
                {
                    facilityId = ctCase.DestinationFacilityId.Value;
                }

                Log.Information($"Processing CaseReference: {ctCase.CaseReference}, DestinationFacilityId: {ctCase.DestinationFacilityId}");

                var gcCustomer = _context.GCCustomers.Where(x => x.FacilityId == facilityId.ToString()).FirstOrDefault();

                if (gcCustomer == null)
                {
                    Log.Error($"No GCCustomer found for facilityId: { facilityId }");
                }
                else
                {
                    Log.Information($"Fetched GCCustomer: {gcCustomer.CustomerName}, for ctCase.DestinationFacilityId: {ctCase.DestinationFacilityId}");
                }

                var existingCase = _context.CTCases.Where(x => x.CaseReference == ctCase.CaseReference).Include("GCCase").FirstOrDefault();

                if (existingCase == null)
                {
                    ctCase.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTCases.Add(ctCase);
                    _context.GCCases.Add(new GCCase
                    {
                        CTCase = ctCase,
                        Title = $"GC_{ctCase.CaseReference}",
                        GCCustomerId = gcCustomer?.Id,
                        DueTimestamp = ctCase.DueTimestamp
                    });

                    newCases.Add(ctCase.CaseReference);
                    //var wwResponse = _soxService.AddOrderToWWAsync(newCTCase.Entity.Id);
                }
                else
                {
                    existingCase.DueTimestamp = ctCase.DueTimestamp;
                    existingCase.DestinationName = ctCase.DestinationName;
                    existingCase.CaseDoctor = ctCase.CaseDoctor;
                    existingCase.CaseCartId = ctCase.CaseCartId;
                    existingCase.CaseTime = ctCase.CaseTime;
                    existingCase.CartStatusId = ctCase.CartStatusId;
                    existingCase.CaseCartName = ctCase.CaseCartName;
                    existingCase.CaseCartLocationName = ctCase.CaseCartLocationName;
                    existingCase.CaseStatus = ctCase.CaseStatus;
                    existingCase.IsCartComplete = ctCase.IsCartComplete;
                    existingCase.CartStatus = ctCase.CartStatus;
                    existingCase.IsCanceled = ctCase.IsCanceled;
                    existingCase.Shipped = ctCase.Shipped;
                    existingCase.PreviousDueTimestamp = ctCase.PreviousDueTimestamp;
                    existingCase.PreviousDestinationName = ctCase.PreviousDestinationName;
                    existingCase.ScheduleId = ctCase.ScheduleId;
                    existingCase.ExpediteTime = ctCase.ExpediteTime;
                    existingCase.DueInMinutes = ctCase.DueInMinutes;
                    existingCase.DueInHoursAndMinutes = ctCase.DueInHoursAndMinutes;
                    existingCase.PreviousDueInMinutes = ctCase.PreviousDueInMinutes;
                    existingCase.PreviousDueInHoursAndMinutes = ctCase.PreviousDueInHoursAndMinutes;
                    existingCase.Changed = ctCase.Changed;
                    existingCase.Priority = ctCase.Priority;
                    existingCase.DateTimeNow = ctCase.DateTimeNow;
                    existingCase.EstimatedTime = ctCase.EstimatedTime;
                    existingCase.ModifiedAtUtc = DateTime.UtcNow;

                    if (gcCustomer != null && existingCase.GCCase != null)
                    {
                        existingCase.GCCase.GCCustomerId = gcCustomer.Id;
                    }
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTFacilities(List<CTFacility> ctFacilities)
        {
            ctFacilities.ForEach(item =>
            {
                var existingFacility = _context.CTFacilities.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existingFacility == null)
                {
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTFacilities.Add(item);
                }
                else
                {
                    existingFacility.LegacyId = item.LegacyId;
                    existingFacility.ProcessingFacilityId = item.ProcessingFacilityId;
                    existingFacility.Name = item.Name;
                    existingFacility.CompositeFacilityKey = item.CompositeFacilityKey;
                    existingFacility.IsProcessingFacility = item.IsProcessingFacility;
                    existingFacility.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTContainerServices(List<Framework.DataLayer.CTContainerService> ctContainerServices)
        {
            ctContainerServices.ForEach(item =>
            {
                var existing = _context.CTContainerServices.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existing == null)
                {
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTContainerServices.Add(item);
                }
                else
                {
                    existing.LegacyId = item.LegacyId;
                    existing.Name = item.Name;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTContainerTypes(List<CTContainerType> ctContainerTypes, long serviceId)
        {
            //var gcVendor = _entities.GCVendors.Where(x => x.LegacyId == serviceId).FirstOrDefault();
            var ctContainerService = _context.CTContainerServices.Where(x => x.LegacyId == serviceId).FirstOrDefault();

            if (ctContainerService == null)
            {
                Log.Error($"No CTContainerService found for LegacyId: {serviceId}.");
                throw new Exception();
            }

            ctContainerTypes.ForEach(item =>
            {
                var existing = _context.CTContainerTypes.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existing == null)
                {
                    //item.GCVendor.Id = gcVendor.Id;
                    //item.GCVendor = gcVendor;
                    item.CTContainerService = ctContainerService;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTContainerTypes.Add(item);
                }
                else
                {
                    existing.LegacyId = item.LegacyId;
                    existing.ServiceId = item.ServiceId;
                    existing.ServiceName = item.ServiceName;
                    existing.HomeLocationName = item.HomeLocationName;
                    existing.ProcessingLocationName = item.ProcessingLocationName;
                    existing.Name = item.Name;
                    existing.SterilizationMethodName = item.SterilizationMethodName;
                    existing.Modified = item.Modified;
                    existing.IsDiscarded = item.IsDiscarded;
                    existing.AutomaticActuals = item.AutomaticActuals;

                    existing.FacilityId = item.FacilityId;
                    existing.SterilizationMethodId = item.SterilizationMethodId;
                    existing.FirstAlternateMethodId = item.FirstAlternateMethodId;
                    existing.SecondAlternateMethodId = item.SecondAlternateMethodId;
                    existing.ThirdAlternateMethodId = item.ThirdAlternateMethodId;
                    existing.HomeLocationId = item.HomeLocationId;
                    existing.ProcessingLocationId = item.ProcessingLocationId;
                    existing.PhysicianName = item.PhysicianName;
                    existing.UsesBeforeService = item.UsesBeforeService;
                    existing.UsageInterval = item.UsageInterval;
                    existing.UsageIntervalUnitOfMeasure = item.UsageIntervalUnitOfMeasure;
                    existing.StandardAssemblyTime = item.StandardAssemblyTime;
                    existing.Weight = item.Weight;
                    existing.ProcessingCost = item.ProcessingCost;
                    existing.ProcurementReferenceId = item.ProcurementReferenceId;
                    existing.DecontaminationInstructions = item.DecontaminationInstructions;
                    existing.AssemblyInstructions = item.AssemblyInstructions;
                    existing.SterilizationInstructions = item.SterilizationInstructions;
                    existing.CountSheetComments = item.CountSheetComments;
                    existing.PriorityId = item.PriorityId;
                    existing.BiologicalTestRequired = item.BiologicalTestRequired;
                    existing.ClassSixTestRequired = item.ClassSixTestRequired;
                    existing.AssemblyByException = item.AssemblyByException;
                    existing.SoftWrap = item.SoftWrap;
                    existing.ComplexityLevelId = item.ComplexityLevelId;
                    existing.OriginalContainerTypeId = item.OriginalContainerTypeId;
                    existing.VendorId = item.VendorId;
                    existing.VendorName = item.VendorName;

                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTProducts(List<CTProduct> ctProducts)
        {
            ctProducts.ForEach(item =>
            {
                var existingProduct = _context.CTProducts.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existingProduct == null)
                {
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTProducts.Add(item);
                }
                else
                {
                    existingProduct.LegacyId = item.LegacyId;
                    existingProduct.VendorName = item.VendorName;
                    existingProduct.VendorProductName = item.VendorProductName;
                    existingProduct.ModelNumber = item.ModelNumber;
                    existingProduct.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTVendors(List<CTVendor> ctVendors)
        {
            ctVendors.ForEach(item =>
            {
                var existingVendor = _context.CTVendors.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existingVendor == null)
                {
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTVendors.Add(item);
                }
                else
                {
                    existingVendor.LegacyId = item.LegacyId;
                    existingVendor.Name = item.Name;
                    existingVendor.ContactName = item.ContactName;
                    existingVendor.VendorTypes = item.VendorTypes;
                    existingVendor.FriendlyName = item.FriendlyName;
                    existingVendor.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTContainerItems(List<CTContainerItem> ctContainerItems, long containerId)
        {
            var ctContainer = _context.CTContainerTypes.Where(x => x.LegacyId == containerId).FirstOrDefault();

            if (ctContainer == null)
            {
                Log.Error($"No CTContainerType found for LegacyId: {containerId}.");
                throw new Exception();
            }

            ctContainerItems.ForEach(item =>
            {
                var existing = _context.CTContainerItems.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existing == null)
                {
                    var product = _context.CTProducts.Where(x => x.LegacyId == item.ProductId).FirstOrDefault();

                    if (product == null)
                    {
                        Log.Information($"No CTProduct found for LegacyId: {item.ProductId}.");
                        //throw new Exception();
                        product = new CTProduct()
                        {
                            CreatedAtUtc = DateTime.UtcNow
                        };
                    }
                    product.LegacyId = item.CTProduct.LegacyId;
                    product.ModelNumber = item.CTProduct.ModelNumber;
                    product.VendorName = item.CTProduct.VendorName;
                    product.VendorProductName = item.CTProduct.VendorProductName;
                    product.ModifiedAtUtc = DateTime.UtcNow;
                    if (product.Id == 0)
                    {
                        _context.CTProducts.Add(product);
                    }

                    item.CTContainerTypeId = ctContainer.Id;
                    item.CTProduct = product;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTContainerItems.Add(item);
                }
                else
                {
                    existing.LegacyId = item.LegacyId;
                    existing.RequiredCount = item.RequiredCount;
                    existing.Placement = item.Placement;
                    existing.SubstitutionsAllowed = item.SubstitutionsAllowed;
                    existing.CriticalItem = item.CriticalItem;
                    existing.ChangeState = item.ChangeState;
                    existing.SequenceNumber = item.SequenceNumber;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        private bool CT_CurrentStatusIsInCTDomain(GCLifecycleStatusType gCLifecycleStatusType)
        {
            var result = false;
            var ctLifeCycles = _context.GCLifecycleStatusTypes.Where(x => x.Origin == ProfitOptics.Modules.Sox.Helpers.Constants.CT_ABBREVIATION_TWO_CHAR).ToList();
            if (ctLifeCycles != null && ctLifeCycles.Count > 0)
            {
                result = ctLifeCycles.Any(x => x.Id == gCLifecycleStatusType.Id);

                if (!result) // make an exception for Received
                {
                    result = gCLifecycleStatusType.Id == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.Received;
                }
            }
            return result;
        }

        public void CT_AddOrEditCTCaseContainers(List<CTCaseContainer> ctCaseContainers, int serverOffsetInMinsFromUTC, bool? isStopLifecycleHistory = false)
        {
            var ctLocations = _context.CTLocations.Include(x => x.CTLocationType).ToList();
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            CTCase ctCase = null;
            bool isCaseComplete = false;
            int? gcCaseId = null;
            bool caseBarocdeChanged = false;
            int? gcCustomerId = null;

            if (ctCaseContainers != null && ctCaseContainers.Count > 0)
            {
                var firstCTCaseContainer = ctCaseContainers.FirstOrDefault();

                ctCase = _context.CTCases.Where(x => x.CaseReference == firstCTCaseContainer.CaseReference).Include(x => x.GCCase).ThenInclude(x => x.GCTrays).FirstOrDefault();
                if (ctCase == null)
                {
                    Log.Error($"No CTCase found for CaseReference: {ctCaseContainers.First().CaseReference}.");
                    throw new Exception();
                }

                if (ctCase.DestinationFacilityId.HasValue)
                {
                    var gcCustomer = _context.GCCustomers.Where(x => x.FacilityId == ctCase.DestinationFacilityId.ToString()).FirstOrDefault();
                    if (gcCustomer != null)
                    {
                        gcCustomerId = gcCustomer.Id;
                    }
                }

                var ctContainerType = _context.CTContainerTypes.Where(x => x.LegacyId == firstCTCaseContainer.ContainerTypeId).FirstOrDefault();
                GCVendor vendor = null;
                if (ctContainerType != null)
                {
                    vendor = _context.GCVendors.Where(x => x.Title == ctContainerType.VendorName).FirstOrDefault();

                    if (vendor == null)
                    {
                        vendor = CreateGCVendor(ctContainerType.VendorId, ctContainerType.VendorName, true, gcCustomerId);
                    }
                    else
                    {
                        var gcChildCompany = _context.GCChildCompanies.Where(x => x.GCVendorId == vendor.Id).FirstOrDefault();
                        if (gcChildCompany == null)
                        {
                            _context.GCChildCompanies.Add(new GCChildCompany()
                            {
                                GCParentCompanyId = null,
                                GCCustomerId = gcCustomerId,
                                GCVendorId = vendor.Id,
                                Title = vendor.Title
                            });
                            _context.SaveChanges();
                        }
                    }
                }

                //var gcCase = _entities.GCCases.Include(x => x.GCTrays).Where(x => x.Id == ctCase.GCCase.Id).FirstOrDefault();
                if (ctCase.GCCase != null)
                {
                    gcCaseId = ctCase.GCCase.Id;
                    if (vendor != null)
                    {
                        ctCase.GCCase.GCVendorId = vendor.Id;
                    }
                }
                ctCase.GCCase.DueTimestamp = ctCase.DueTimestamp;
                _context.GCCases.Update(ctCase.GCCase);


                isCaseComplete = ctCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE ||
                                ctCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID ||
                                ctCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE ||
                                ctCase.CartStatus == ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID;


                //ctCaseContainers.ForEach(ctCaseContainer =>
                foreach (var ctCaseContainer in ctCaseContainers)
                {
                    Log.Information($"Processing CTCaseContainer: {JsonConvert.SerializeObject(ctCaseContainer)}.");

                    // Offsetting Timestamps to make them UTC:
                    ctCaseContainer.DueTimestamp = ctCaseContainer.DueTimestampOriginal.AddMinutes(minsToAdd);
                    ctCaseContainer.EndTimestamp = (ctCaseContainer.EndTimestampOriginal.HasValue && ctCaseContainer.EndTimestampOriginal.Value > DateTime.MinValue) ? ctCaseContainer.EndTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    ctCaseContainer.UpdateTimestamp = (ctCaseContainer.UpdateTimestampOriginal.HasValue && ctCaseContainer.UpdateTimestampOriginal.Value > DateTime.MinValue) ? ctCaseContainer.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    ctCaseContainer.MultipleBasis = (ctCaseContainer.MultipleBasisOriginal.HasValue && ctCaseContainer.MultipleBasisOriginal.Value > DateTime.MinValue) ? ctCaseContainer.MultipleBasisOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;

                    // creating historical record:
                    var ctCaseContainerHistorical = _mapper.Map<Framework.DataLayer.CTCaseContainerHistorical>(ctCaseContainer);
                    ctCaseContainerHistorical.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTCaseContainerHistoricals.Add(ctCaseContainerHistorical);
                    _context.SaveChanges();

                    string updateUser = null;
                    bool isCurrentLocationLoaner = false;
                    bool isLoaner = false;
                    string locationNameIfNotYetAvailable = null;
                    bool isActualCurrentlyAvailable = true;
                    int? currentActualLapCount = null;

                    string barcode = ProfitOptics.Modules.Sox.Helpers.Constants.NO_BARCODE_DATA;
                    if (ctCaseContainer.ContainerId.HasValue)
                    {
                        barcode = ProfitOptics.Modules.Sox.Helpers.Helpers.GenerateGCTrayBarcode(ctCaseContainer.ContainerId);
                    }

                    long containerTypeId = ctCaseContainer.ContainerTypeId;
                    var ctCurrentContainerType = _context.CTContainerTypes.Where(x => x.LegacyId == containerTypeId).FirstOrDefault();
                    if (ctCurrentContainerType == null)
                    {
                        Log.Error($"No CTContainerType found for LegacyId: {ctCaseContainer.ContainerTypeId}, moving on to next item...");
                    }
                    else
                    {
                        if (ctCaseContainer.CurrentLocationName != null && ctCaseContainer.CurrentLocationName.StartsWith("Available"))
                        {
                            locationNameIfNotYetAvailable = ctCaseContainer.LocationName;
                            ctCaseContainer.LocationName = "";
                            isActualCurrentlyAvailable = false;
                        }
                        // Quarantine candidate:
                        else if (ctCaseContainer.CurrentLocationName != null && ctCaseContainer.CurrentLocationName.Equals(ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASECONTAINER_LOCATION_NAME_NONE_AVAILABLE))
                        {
                            locationNameIfNotYetAvailable = null;
                            ctCaseContainer.LocationName = null;
                            isActualCurrentlyAvailable = false;
                        }

                        long? containerTypeActualId = ctCaseContainer.ContainerId;
                        CTContainerTypeActual ctContainerTypeActual = null;

                        if (ctLocations.Count == 0)
                        {
                            Log.Error($"No CTLocations found DB");
                            throw new Exception();
                        }
                        int ctLocationTypeId = 0;

                        if (containerTypeActualId.HasValue)
                        {
                            ctContainerTypeActual = _context.CTContainerTypeActuals.Where(x => x.LegacyId == containerTypeActualId.Value).FirstOrDefault();
                            if (ctContainerTypeActual == null)
                            {
                                Log.Information($"No CTContainerTypeActual found for LegacyId: {ctCaseContainer.ContainerId}.");
                            }
                        }
                        else
                        {
                            Log.Information($"No CTContainerTypeActual allocated (ctCaseContainer.ContainerId = NULL).");
                        }

                        if (!string.IsNullOrWhiteSpace(ctCaseContainer.LocationName) &&  // Now even loaners have location
                            (ctCaseContainer.LocationName.Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION) ||
                            ctCaseContainer.LocationName.Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION)))
                        {
                            isCurrentLocationLoaner = true;
                        }
                        else
                        {
                            // Silly Hack because of CT
                            var ctLocation = GetCTLocationForLocationName(ctCaseContainer.LocationName);
                            if (ctLocation != null)
                            {
                                ctLocationTypeId = ctLocation.CTLocationTypeId;
                            }
                        }

                        //CTLocationTypeGCLifecycleStatusType ctLocationTypeGCLifecycleStatusType = null;
                        GCLifecycleStatusType gcLifeCycleStatusType = null;

                        // Tray must have an 'actual' (be allocated) in order to have Location/LocationType
                        if (ctContainerTypeActual != null)
                        {
                            var currentHistoryItem = _context.CTContainerTypeActualHistoryItems.Where(x =>
                                x.CTContainerTypeActualId == ctContainerTypeActual.Id &&
                                ctCaseContainer.UpdateTimestamp.HasValue &&
                                x.UpdateTimestamp.Date == ctCaseContainer.UpdateTimestamp.Value.Date &&
                                x.UpdateTimestamp.Hour == ctCaseContainer.UpdateTimestamp.Value.Hour &&
                                x.UpdateTimestamp.Minute == ctCaseContainer.UpdateTimestamp.Value.Minute &&
                                x.UpdateTimestamp.Second == ctCaseContainer.UpdateTimestamp.Value.Second).FirstOrDefault(); // comparing without miliseconds

                            isLoaner = _context.CTContainerTypeActualHistoryItems.Any(x =>
                                x.CTContainerTypeActualId == ctContainerTypeActual.Id &&
                                x.LocationElapsedCase != null &&
                                    (x.LocationElapsedCase.ToLower().Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_IN_LOCATION.ToLower()) ||
                                    x.LocationElapsedCase.ToLower().Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION.ToLower())));

                            if (currentHistoryItem != null)
                            {
                                updateUser = currentHistoryItem.UpdateUserId;
                                currentActualLapCount = currentHistoryItem.ActualLapCount;
                            }
                            else
                            {
                                updateUser = ctContainerTypeActual.UpdateUserId;
                            }

                            if (isCurrentLocationLoaner)
                            {
                                int loanerCode = (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerInbound;
                                if (ctCaseContainer.LocationName.Contains(ProfitOptics.Modules.Sox.Helpers.Constants.CT_LOANERS_CHECK_OUT_LOCATION))
                                {
                                    loanerCode = (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerOutbound;
                                }
                                gcLifeCycleStatusType = _context.GCLifecycleStatusTypes.Where(x => x.Code == loanerCode).FirstOrDefault();
                            }
                            else
                            {
                                if (ctLocationTypeId == 0)
                                {
                                    //Log.Error($"No CTLocation found for LocationName: {ctCaseContainer.LocationName}.");
                                    Log.Information($"WAS an ERROR, now it's info: No CTLocation found for LocationName: {ctCaseContainer.LocationName}.");
                                    //throw new Exception();
                                }

                                if (ctLocationTypeId > 0)
                                {
                                    // Silly Hack because of CT:
                                    var updateTimeStamp = ctCaseContainer.UpdateTimestamp.HasValue ? ctCaseContainer.UpdateTimestamp.Value : DateTime.MinValue;
                                    gcLifeCycleStatusType = GetGCLifeCycleBasedOnLocationType(updateTimeStamp, ctLocationTypeId, ctContainerTypeActual.Id, isCaseComplete, gcCaseId, ctCase.DueTimestamp, minsToAdd, updateUser);

                                    if (gcLifeCycleStatusType == null)
                                    {
                                        Log.Error($"No GCLifecycleStatusType found for CTLocationTypeId: {ctLocationTypeId}.");
                                        //throw new Exception();
                                    }
                                }
                            }
                        }

                        var existingCaseContainers = _context.CTCaseContainers.Where(x =>
                            x.CaseReference == ctCase.CaseReference &&
                            x.ContainerTypeId == ctCaseContainer.ContainerTypeId &&
                            //ctCaseContainer.ContainerName != null && ctCaseContainer.ContainerName.Contains(x.ContainerName) &&
                            x.ItemName == ctCaseContainer.ItemName &&
                            x.CaseItemNumber == ctCaseContainer.CaseItemNumber &&
                                (x.ContainerId == ctCaseContainer.ContainerId ||
                                x.ContainerId == null ||
                                x.ContainerId != ctCaseContainer.ContainerId))
                            //  ^ Ambiguous, but this exact order, means, matching first by ActualId, then match if none existing, then take whichever exists (cause ActualId has probably changed)
                            .OrderByDescending(x => x.ContainerId)
                            .ToList();

                        var matchedCaseContainer = existingCaseContainers.FirstOrDefault();

                        // creatig historical record:
                        var matchedCaseContainerHistorical = _mapper.Map<Framework.DataLayer.CTCaseContainerHistorical>(matchedCaseContainer);
                        if (matchedCaseContainer != null && matchedCaseContainerHistorical != null)
                        {
                            matchedCaseContainerHistorical.ScheduleStatusCode = ProfitOptics.Modules.Sox.Helpers.Constants.CT_MATCHED_CASE_CONTAINER;
                            matchedCaseContainerHistorical.CreatedAtUtc = DateTime.UtcNow;
                            _context.CTCaseContainerHistoricals.Add(matchedCaseContainerHistorical);
                            _context.SaveChanges();
                        }

                        GCTray gcTray = null;

                        if (matchedCaseContainer == null)
                        {
                            ctCaseContainer.CreatedAtUtc = DateTime.UtcNow;
                            ctCaseContainer.ContainerId = containerTypeActualId;
                            ctCaseContainer.CTCaseId = ctCase.Id;
                            ctCaseContainer.ContainerTypeId = ctCurrentContainerType.LegacyId;
                            ctCaseContainer.CTContainerTypeId = ctCurrentContainerType.Id;

                            if (ctCaseContainer.VendorName == null && vendor != null)
                            {
                                ctCaseContainer.VendorName = vendor.Title;
                            }

                            _context.CTCaseContainers.Add(ctCaseContainer);

                            gcTray = new Framework.DataLayer.GCTray()
                            {
                                CTContainerTypeId = ctCurrentContainerType.Id,
                                GCCaseId = ctCase.GCCase.Id,
                                CTCaseContainer = ctCaseContainer,
                                Barcode = barcode,
                                CTContainerTypeActualId = ctContainerTypeActual != null ? ctContainerTypeActual.Id : (int?)null,
                                IsActiveStatus = false,
                                IsLoaner = isLoaner,
                                IsActualCurrentlyAvailable = isActualCurrentlyAvailable,
                                GCLifecycleStatusTypeId = gcLifeCycleStatusType != null ? gcLifeCycleStatusType.Id : (int?)null,
                                ActualLapCount = currentActualLapCount
                            };

                            _context.GCTrays.Add(gcTray);

                            // Adding only LCs if now Induction (inductions are added to previous case), or loaner (Loaner Check In & Loaner Check Out)
                            if (gcLifeCycleStatusType != null &&
                                    //(isCurrentLocationLoaner || 
                                    (isLoaner ||
                                    !ProfitOptics.Modules.Sox.Helpers.Helpers.CurrentStatusIsInduction(gcLifeCycleStatusType.Id)))
                            {
                                var gcTraysLifecycleStatusTypeToAdd = new GCTraysLifecycleStatusTypes()
                                {
                                    GCTray = gcTray,
                                    LifecycleStatusType = gcLifeCycleStatusType,
                                    Timestamp = (ctCaseContainer.UpdateTimestamp.HasValue && ctCaseContainer.UpdateTimestamp.Value > DateTime.MinValue) ? ctCaseContainer.UpdateTimestamp.Value : DateTime.UtcNow,
                                    IsActiveStatus = true,
                                    User = updateUser,
                                    TimestampOriginal = (ctCaseContainer.UpdateTimestampOriginal.HasValue && ctCaseContainer.UpdateTimestampOriginal.Value > DateTime.MinValue) ? ctCaseContainer.UpdateTimestampOriginal.Value : DateTime.MinValue.AddYears(1000)
                                };
                                gcTray.GCLifecycleStatusTypeId = gcLifeCycleStatusType.Id;
                                gcTray.IsActualCurrentlyAvailable = isActualCurrentlyAvailable;
                                _context.GCTraysLifecycleStatusTypes.Add(gcTraysLifecycleStatusTypeToAdd);
                            }
                        }
                        else
                        {
                            matchedCaseContainer.CTCaseId = ctCase.Id;
                            matchedCaseContainer.ContainerId = containerTypeActualId;
                            matchedCaseContainer.ContainerTypeId = ctCurrentContainerType.LegacyId;
                            matchedCaseContainer.CTContainerTypeId = ctCurrentContainerType.Id;
                            matchedCaseContainer.CaseReference = ctCaseContainer.CaseReference;
                            matchedCaseContainer.CaseCartId = ctCaseContainer.CaseCartId;
                            matchedCaseContainer.CaseCartName = ctCaseContainer.CaseCartName;
                            matchedCaseContainer.DueMinutes = ctCaseContainer.DueMinutes;
                            matchedCaseContainer.CaseTime = ctCaseContainer.CaseTime;
                            matchedCaseContainer.DueTimestampOriginal = ctCaseContainer.DueTimestampOriginal;
                            matchedCaseContainer.DueTimestamp = ctCaseContainer.DueTimestampOriginal.AddMinutes(minsToAdd);
                            matchedCaseContainer.DestinationName = ctCaseContainer.DestinationName;
                            matchedCaseContainer.EndTimestampOriginal = ctCaseContainer.EndTimestampOriginal;
                            matchedCaseContainer.EndTimestamp = (ctCaseContainer.EndTimestampOriginal.HasValue && ctCaseContainer.EndTimestampOriginal.Value > DateTime.MinValue) ? matchedCaseContainer.EndTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                            matchedCaseContainer.CaseDoctor = ctCaseContainer.CaseDoctor;
                            matchedCaseContainer.ContainerName = ctCaseContainer.ContainerName;
                            matchedCaseContainer.ItemName = ctCaseContainer.ItemName;
                            matchedCaseContainer.Status = ctCaseContainer.Status;
                            matchedCaseContainer.ScheduleStatusCode = ctCaseContainer.ScheduleStatusCode;
                            matchedCaseContainer.ScheduleStatus = ctCaseContainer.ScheduleStatus;
                            matchedCaseContainer.StandardTime = ctCaseContainer.StandardTime;
                            matchedCaseContainer.ExpediteTime = ctCaseContainer.ExpediteTime;
                            matchedCaseContainer.ItemCode = ctCaseContainer.ItemCode;
                            matchedCaseContainer.ItemType = ctCaseContainer.ItemType;
                            matchedCaseContainer.ItemTypeName = ctCaseContainer.ItemTypeName;
                            matchedCaseContainer.StatTime = ctCaseContainer.StatTime;
                            matchedCaseContainer.MultipleBasisOriginal = ctCaseContainer.MultipleBasisOriginal;
                            matchedCaseContainer.MultipleBasis = (ctCaseContainer.MultipleBasisOriginal.HasValue && ctCaseContainer.MultipleBasisOriginal.Value > DateTime.MinValue) ? matchedCaseContainer.MultipleBasisOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                            matchedCaseContainer.LocationName = ctCaseContainer.LocationName;
                            matchedCaseContainer.UpdateTimestampOriginal = ctCaseContainer.UpdateTimestampOriginal;
                            matchedCaseContainer.UpdateTimestamp = (ctCaseContainer.UpdateTimestampOriginal.HasValue && ctCaseContainer.UpdateTimestampOriginal.Value > DateTime.MinValue) ? matchedCaseContainer.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                            matchedCaseContainer.ScheduleCartChanged = ctCaseContainer.ScheduleCartChanged;
                            matchedCaseContainer.IsModified = ctCaseContainer.IsModified;
                            matchedCaseContainer.HoldQuantity = ctCaseContainer.HoldQuantity;
                            matchedCaseContainer.ItemQuantity = ctCaseContainer.ItemQuantity;
                            matchedCaseContainer.IsCanceled = ctCaseContainer.IsCanceled;
                            matchedCaseContainer.IsCartComplete = ctCaseContainer.IsCartComplete;
                            matchedCaseContainer.Shipped = ctCaseContainer.Shipped;
                            matchedCaseContainer.CaseStatus = ctCaseContainer.CaseStatus;
                            matchedCaseContainer.StandardTimeInHoursAndMinutes = ctCaseContainer.StandardTimeInHoursAndMinutes;
                            matchedCaseContainer.ExpediteTimeInHoursAndMinutes = ctCaseContainer.ExpediteTimeInHoursAndMinutes;
                            matchedCaseContainer.StatTimeInHoursAndMinutes = ctCaseContainer.StatTimeInHoursAndMinutes;
                            matchedCaseContainer.DueInMinutes = ctCaseContainer.DueInMinutes;
                            matchedCaseContainer.DueInHoursAndMinutes = ctCaseContainer.DueInHoursAndMinutes;
                            matchedCaseContainer.ProcessingFacilityId = ctCaseContainer.ProcessingFacilityId;
                            matchedCaseContainer.LocationFacilityId = ctCaseContainer.LocationFacilityId;
                            matchedCaseContainer.CaseItemNumber = ctCaseContainer.CaseItemNumber;
                            matchedCaseContainer.CaseContainerStatus = ctCaseContainer.CaseContainerStatus;
                            matchedCaseContainer.CurrentLocationName = ctCaseContainer.CurrentLocationName;
                            matchedCaseContainer.VendorName = ctCaseContainer.VendorName;
                            matchedCaseContainer.SerialNumber = ctCaseContainer.SerialNumber;
                            matchedCaseContainer.ModifiedAtUtc = DateTime.UtcNow;

                            if (matchedCaseContainer.VendorName == null && vendor != null)
                            {
                                matchedCaseContainer.VendorName = vendor.Title;
                            }

                            gcTray = _context.GCTrays.Where(x => x.CTCaseContainerId.HasValue && x.CTCaseContainerId == matchedCaseContainer.Id).FirstOrDefault();
                            //if(existingGCTray == null)
                            //{
                            //    Log.Error($"No GCTray found for CTCaseContainerId: {existingCaseContainer.Id}.");
                            //    throw new Exception();
                            //}
                            if (gcTray == null)
                            {
                                gcTray = new Framework.DataLayer.GCTray()
                                {
                                    CTContainerTypeId = ctCurrentContainerType.Id,
                                    GCCaseId = ctCase.GCCase.Id,
                                    CTCaseContainer = ctCaseContainer,
                                    CTContainerTypeActualId = ctContainerTypeActual == null ? (int?)null : ctContainerTypeActual.Id,
                                    IsActualCurrentlyAvailable = isActualCurrentlyAvailable,
                                    IsActiveStatus = false,
                                    ActualLapCount = currentActualLapCount
                                };
                            }
                            else
                            {
                                // Actual has changed, need to update lifecycles as well!!
                                if (gcTray.CTContainerTypeActualId.HasValue &&
                                    ctContainerTypeActual != null &&
                                        gcTray.CTContainerTypeActualId.Value != ctContainerTypeActual.Id)
                                {
                                    Log.Warning($"Actual has changed for GCTrayID: {gcTray.Id}, was: {gcTray.CTContainerTypeActualId}, now: {ctContainerTypeActual.Id}");
                                    var gcTrayActualHistory = new GCTrayActualHistory()
                                    {
                                        GCTrayId = gcTray.Id,
                                        CTContainerTypeActualId = gcTray.CTContainerTypeActualId.Value,
                                        CreatedAtUtc = DateTime.UtcNow
                                    };
                                    AddGCTrayActualHistory(gcTrayActualHistory);
                                }

                                if (ctContainerTypeActual != null)
                                {
                                    gcTray.CTContainerTypeActualId = ctContainerTypeActual.Id;
                                }
                                else if (gcTray.CTContainerTypeActualId != null && ctContainerTypeActual == null) // lost the Actual
                                {
                                    gcTray.CTContainerTypeActualId = null;
                                    isActualCurrentlyAvailable = false;
                                }
                            }

                            if (gcTray.Barcode == null || gcTray.Barcode == ProfitOptics.Modules.Sox.Helpers.Constants.NO_BARCODE_DATA || gcTray.Barcode != barcode)
                            {
                                if (gcTray.Barcode != barcode && barcode != ProfitOptics.Modules.Sox.Helpers.Constants.NO_BARCODE_DATA)
                                {
                                    // update barcode in WW
                                    caseBarocdeChanged = true;
                                }
                                gcTray.Barcode = barcode;
                            }

                            if (gcLifeCycleStatusType != null)
                            {
                                var previousActiveLifecycleStatuses = _context.GCTraysLifecycleStatusTypes.Include(x => x.LifecycleStatusType).Where(x => x.IsActiveStatus == true && x.GCTrayId == gcTray.Id).OrderByDescending(x => x.Timestamp).ToList();

                                GCTraysLifecycleStatusTypes gcTraysLifecycleStatusType = new GCTraysLifecycleStatusTypes()
                                {
                                    GCTray = gcTray,
                                    LifecycleStatusType = gcLifeCycleStatusType,
                                    //Timestamp = DateTime.UtcNow,
                                    Timestamp = (ctCaseContainer.UpdateTimestamp.HasValue && ctCaseContainer.UpdateTimestamp.Value > DateTime.MinValue) ? ctCaseContainer.UpdateTimestamp.Value : DateTime.UtcNow,
                                    IsActiveStatus = true,
                                    User = updateUser,
                                    TimestampOriginal = (ctCaseContainer.UpdateTimestampOriginal.HasValue && ctCaseContainer.UpdateTimestampOriginal.Value > DateTime.MinValue) ? ctCaseContainer.UpdateTimestampOriginal.Value : DateTime.UtcNow.AddMinutes(minsToAdd),
                                };

                                if (previousActiveLifecycleStatuses != null)
                                {
                                    if (previousActiveLifecycleStatuses.Count > 1)
                                    {
                                        Log.Information($"Multiple Active statuses for GCTrayId: {gcTray.Id}, count: {previousActiveLifecycleStatuses.Count}.");
                                        //throw new Exception();
                                    }

                                    previousActiveLifecycleStatuses.ForEach(x =>
                                    {
                                        if (x.LifecycleStatusType.Origin != null && x.LifecycleStatusType.Origin.Equals(ProfitOptics.Modules.Sox.Helpers.Constants.CT_ABBREVIATION_TWO_CHAR))
                                        {
                                            x.IsActiveStatus = false;
                                        }
                                    });

                                    if (gcTray != null && gcTray.Id > 0)
                                    {
                                        isCurrentLocationLoaner = _context.GCTraysLifecycleStatusTypes.Include(x => x.LifecycleStatusType).Where(x => x.GCTrayId == gcTray.Id).Any(x => x.LifecycleStatusType.Code == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.LoanerInbound);
                                    }

                                    if (isCurrentLocationLoaner || !ProfitOptics.Modules.Sox.Helpers.Helpers.CurrentStatusIsInduction(gcLifeCycleStatusType.Id))
                                    {
                                        // Getting latest status:
                                        GCTraysLifecycleStatusTypes previousActiveLifecycleStatus = null;
                                        if (previousActiveLifecycleStatuses.Count > 0)
                                        {
                                            previousActiveLifecycleStatus = previousActiveLifecycleStatuses[previousActiveLifecycleStatuses.Count - 1];

                                            if (previousActiveLifecycleStatus != null)
                                            {
                                                if (previousActiveLifecycleStatus.LifecycleStatusTypeId != gcLifeCycleStatusType.Id)
                                                {
                                                    if (CT_CurrentStatusIsInCTDomain(previousActiveLifecycleStatus.LifecycleStatusType))
                                                    {
                                                        _context.GCTraysLifecycleStatusTypes.Add(gcTraysLifecycleStatusType);
                                                    }
                                                }
                                                else
                                                {
                                                    previousActiveLifecycleStatus.IsActiveStatus = true;
                                                    previousActiveLifecycleStatus.User = updateUser;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            _context.GCTraysLifecycleStatusTypes.Add(gcTraysLifecycleStatusType);
                                        }
                                    }
                                }
                                else
                                {
                                    _context.GCTraysLifecycleStatusTypes.Add(gcTraysLifecycleStatusType);
                                }

                                gcTray.GCLifecycleStatusTypeId = gcLifeCycleStatusType.Id;
                                gcTray.IsActualCurrentlyAvailable = isActualCurrentlyAvailable;
                            }
                            else if (!string.IsNullOrWhiteSpace(locationNameIfNotYetAvailable))
                            {
                                var updateTimeStamp = ctCaseContainer.UpdateTimestamp.HasValue ? ctCaseContainer.UpdateTimestamp.Value : DateTime.MinValue;

                                //gcTray.GCLifecycleStatusTypeId = _groundControlService.SetGCTraysLifeCycle(updateTimeStamp, locationNameIfNotYetAvailable, ctContainerTypeActual.Id, isCaseComplete, gcCaseId, ctCase.DueTimestamp, minsToAdd, updateUser, isActualCurrentlyAvailable);
                                // Setting this to NULL to prevent ORANGE trays from getting LC Value (not sure why was set in the first place???)
                                gcTray.GCLifecycleStatusTypeId = null;
                                gcTray.IsActualCurrentlyAvailable = isActualCurrentlyAvailable;
                            }

                            //if (isCurrentLocationLoaner)
                            //{
                            //    gcTray.IsLoaner = isCurrentLocationLoaner;
                            //}
                            gcTray.IsLoaner = isLoaner;
                        }
                    }
                }

                _context.SaveChanges();

                if (caseBarocdeChanged)
                {
                    try
                    {
                        Log.Information($"Update Barcodes for GCCase: {ctCase.GCCase.Title} has changed barcodes, updating WW now...");
                        UpdateCaseBarcodesAsync(ctCase.GCCase.Id).GetAwaiter().GetResult();
                        Log.Information($"Update Case Barcodes COMPLETED.");
                    }
                    catch (Exception e)
                    {
                    }
                }

                if (ctCase.GCCase != null)
                {
                    if (!isStopLifecycleHistory.HasValue || !isStopLifecycleHistory.Value)
                    {
                        Log.Information($"STARTED Fetching History Items for Case Reference: {ctCase.GCCase.Title}");
                        // Matching history items:
                        MatchHistoryItems(ctCase.GCCase.Id);
                        Log.Information($"FINISHED Fetching History Items for Case Reference: {ctCase.GCCase.Title}");
                    }

                    UpdateGCCaseLifecycleStatus(ctCase.GCCase.Id);
                }
            }
        }

        public ProfitOptics.Framework.DataLayer.CTContainerService CT_GetCTContainerServiceByLegacyId(int serviceId)
        {
            return _context.CTContainerServices.Where(x => x.LegacyId == serviceId).FirstOrDefault();
        }

        public CTContainerTypeActual CT_GetCTContainerTypeActualByLegacyId(long containerId)
        {
            return _context.CTContainerTypeActuals.Where(x => x.LegacyId == containerId).FirstOrDefault();
        }

        public CTContainerType CT_GetCTContainerByContainerTypeId(long containerTypeId)
        {
            return _context.CTContainerTypes.Where(x => x.LegacyId == containerTypeId).FirstOrDefault();
        }

        public void CT_AddOrEditCTLocationTypes(List<CTLocationType> ctLocationTypes)
        {
            ctLocationTypes.ForEach(item =>
            {
                var existingLocationType = _context.CTLocationTypes.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                if (existingLocationType == null)
                {
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTLocationTypes.Add(item);
                }
                else
                {
                    existingLocationType.LegacyId = item.LegacyId;
                    existingLocationType.Name = item.Name;
                    existingLocationType.Level = item.Level;
                    existingLocationType.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTContainerTypeActuals(List<CTContainerTypeActual> ctContainerTypeActuals, long containerTypeId, int serverOffsetInMinsFromUTC)
        {
            var ctContainerType = _context.CTContainerTypes.Where(x => x.LegacyId == containerTypeId).FirstOrDefault();
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            if (ctContainerType == null)
            {
                Log.Error($"No CTContainerType found for LegacyId: {containerTypeId}.");
                throw new Exception();
            }

            ctContainerTypeActuals.ForEach(item =>
            {
                var existing = _context.CTContainerTypeActuals.Where(x => x.LegacyId == item.LegacyId).FirstOrDefault();

                item.UpdateTimestamp = item.UpdateTimestampOriginal.AddMinutes(minsToAdd);

                if (existing == null)
                {
                    item.CTContainerTypeId = ctContainerType.Id;
                    item.VendorName = ctContainerType.VendorName;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTContainerTypeActuals.Add(item);
                }
                else
                {
                    existing.CTContainerTypeId = ctContainerType.Id;
                    existing.LegacyId = item.LegacyId;
                    existing.ParentId = item.ParentId;
                    existing.Weight = item.Weight;
                    existing.PhysicianName = item.PhysicianName;
                    existing.SterilizationMethodName = item.SterilizationMethodName;
                    existing.ServiceId = item.ServiceId;
                    existing.ServiceName = item.ServiceName;
                    //existing.ProcurementReferenceId = item.ProcurementReferenceId;
                    existing.Name = item.Name;
                    //existing.ParentName = item.ParentName;
                    existing.Status = item.Status;
                    existing.UpdateTimestamp = item.UpdateTimestamp;
                    existing.UpdateUserId = item.UpdateUserId;
                    existing.LocationElapsed = item.LocationElapsed;
                    existing.CaseCart = item.CaseCart;
                    existing.ProcessingLocationName = item.ProcessingLocationName;
                    existing.ParentProcessingLocationName = item.ParentProcessingLocationName;
                    existing.ParentUsageIntervalUnitOfMeasure = item.ParentUsageIntervalUnitOfMeasure;
                    existing.HomeLocationName = item.HomeLocationName;
                    existing.ParentHomeLocationName = item.ParentHomeLocationName;
                    existing.ParentIntervalUsageCount = item.ParentIntervalUsageCount;
                    existing.IntervalUsageCount = item.IntervalUsageCount;
                    existing.Usage = item.Usage;
                    existing.CensisSetLastRequestDate = item.CensisSetLastRequestDate;
                    existing.LastMaintenance = item.LastMaintenance;
                    existing.Assembly = item.Assembly;
                    existing.Storage = item.Storage;
                    existing.RtlsLocationName = item.RtlsLocationName;
                    existing.RtlsLocationUpdateTimestamp = item.RtlsLocationUpdateTimestamp;
                    existing.IsDiscarded = item.IsDiscarded;
                    existing.Priority = item.Priority;
                    existing.ShelfLife = item.ShelfLife;
                    existing.NotificationCount = item.NotificationCount;
                    existing.Expedite = item.Expedite;
                    existing.Rigidity = item.Rigidity;
                    existing.ParentExpedite = item.ParentExpedite;
                    existing.ParentRigidity = item.ParentRigidity;
                    existing.CensisSetHistoryBuildStartDate = item.CensisSetHistoryBuildStartDate;
                    existing.FirstAlternateMethodId = item.FirstAlternateMethodId;
                    existing.FirstAlternateMethodName = item.FirstAlternateMethodName;
                    existing.SecondAlternateMethodId = item.SecondAlternateMethodId;
                    existing.SecondAlternateMethodName = item.SecondAlternateMethodName;
                    existing.ThirdAlternateMethodId = item.ThirdAlternateMethodId;
                    existing.ThirdAlternateMethodName = item.ThirdAlternateMethodName;
                    existing.ComplexityLevelDescription = item.ComplexityLevelDescription;
                    existing.DisposableFlag = item.DisposableFlag;
                    existing.SerialNumber = item.SerialNumber;
                    //existing.VendorName = item.VendorName;
                    existing.VendorName = ctContainerType.VendorName;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                    existing.UpdateTimestampOriginal = item.UpdateTimestampOriginal;
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTContainerTypeActualHistoryItemsAndGCTrays(List<Framework.DataLayer.CTContainerTypeActualHistoryItem> ctContainerTypeActualHistoryItems, long serviceId, int serverOffsetInMinsFromUTC, bool? isStopLifecycleHistory = false, bool? updateLapCountAndLocation = false)
        {
            var ctContainerTypeActual = _context.CTContainerTypeActuals.Include(x => x.CTContainerType).Where(x => x.LegacyId == serviceId).FirstOrDefault();
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            if (ctContainerTypeActual == null)
            {
                Log.Error($"No CTContainerTypeActual found for LegacyId: {serviceId}.");
                throw new Exception();
            }

            bool isActualLoaner = ctContainerTypeActualHistoryItems.Any(x => x.LocationElapsedCase.Equals(Constants.CT_LOANERS_CHECK_IN_LOCATION));

            ctContainerTypeActualHistoryItems = ctContainerTypeActualHistoryItems.OrderBy(x => x.UpdateTimestampOriginal).ToList();

            // First iteration (updating HistoryItems)
            ctContainerTypeActualHistoryItems.ForEach(hi =>
            {
                if (hi.CTContainerTypeActualId == 0)
                {
                    hi.CTContainerTypeActualId = ctContainerTypeActual.Id;
                }
                UpdateHistoryItemAndLapCount(hi, minsToAdd, isActualLoaner, updateLapCountAndLocation);
            });

            if (!isStopLifecycleHistory.HasValue || !isStopLifecycleHistory.Value)
            {
                GCTray gcTray = _context.GCTrays.Include(x => x.GCTraysLifecycleStatusTypes)
                   .Where(x => x.CTContainerTypeActualId == ctContainerTypeActual.Id
                   && x.GCCaseId == null
                   ).FirstOrDefault();
                if (gcTray == null)
                {
                    gcTray = new GCTray()
                    {
                        CTContainerTypeActualId = ctContainerTypeActual.Id,
                        CTContainerTypeId = ctContainerTypeActual.CTContainerType.Id,
                        GCTraysLifecycleStatusTypes = new List<GCTraysLifecycleStatusTypes>(),
                        GCCaseId = null,
                        IsActiveStatus = false
                    };
                }

                // Second iteration (updating LCs)
                ctContainerTypeActualHistoryItems.ForEach(item =>
                {
                    if (item.CTContainerTypeActualId == 0)
                    {
                        item.CTContainerTypeActualId = ctContainerTypeActual.Id;
                    }

                    // Used for making a reference with CTLocation into CTContainerTypeActualHistoryItem
                    int? ctLocationId = null;
                    CTLocation ctLocation = null;

                    if (item.Status.Equals(Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG) || item.Status.Equals(Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER))
                    {
                        // Silly Hack because of CT
                        ctLocation = GetCTLocationForLocationName(item.LocationElapsedCase);

                        if (ctLocation != null)
                        {
                            Log.Information($"(2nd iteration HI) - Found CTLocation: {ctLocation.LocationName}, for LocationName: {item.LocationElapsedCase}");

                            ctLocationId = ctLocation.Id;
                        }
                        else
                        {
                            Log.Error($"(2nd iteration HI) - NOT Found CTLocation for provided LocationName: {item.LocationElapsedCase}");
                        }
                    }

                    if ((item.Status.Equals(Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG) || item.Status.Equals(Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)) && ctLocation != null)
                    {
                        //var ctLocationTypesGCLifecycleStatusType = _entities.CTLocationTypeGCLifecycleStatusTypes.Where(x => x.CTLocationTypeId == ctLocation.CTLocationTypeId).FirstOrDefault();

                        // Silly Hack because of CT
                        var gcLifecycleStatusType = GetGCLifeCycleBasedOnLocationType(item.UpdateTimestamp, ctLocation.CTLocationTypeId, ctContainerTypeActual.Id, null);
                        if (gcLifecycleStatusType != null)
                        {
                            var gcTrayLifeCycle = gcTray.GCTraysLifecycleStatusTypes.Where(x => x.Timestamp == item.UpdateTimestamp).FirstOrDefault();
                            if (gcTrayLifeCycle == null)
                            {
                                gcTrayLifeCycle = new GCTraysLifecycleStatusTypes()
                                {
                                    LifecycleStatusTypeId = gcLifecycleStatusType.Id,
                                    Timestamp = item.UpdateTimestamp,
                                    IsActiveStatus = false,
                                    User = item.UpdateUserId,
                                    TimestampOriginal = item.UpdateTimestampOriginal
                                };
                                gcTray.GCTraysLifecycleStatusTypes.Add(gcTrayLifeCycle);
                                Log.Information($"-----------------Created LC with timestamp: {item.UpdateTimestamp} and original timestamp: {item.UpdateTimestampOriginal}");
                            }
                            else
                            {
                                gcTrayLifeCycle.User = item.UpdateUserId;
                                gcTrayLifeCycle.LifecycleStatusTypeId = gcLifecycleStatusType.Id;
                            }
                        }
                    }

                    _context.SaveChanges();
                });

                if (gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
                {
                    gcTray.GCTraysLifecycleStatusTypes = gcTray.GCTraysLifecycleStatusTypes.OrderBy(x => x.Timestamp).ToList();
                    for (int i = 0; i < gcTray.GCTraysLifecycleStatusTypes.Count; i++)
                    {
                        gcTray.GCTraysLifecycleStatusTypes.ToList()[i].IsActiveStatus = (i == (gcTray.GCTraysLifecycleStatusTypes.Count - 1));
                    }
                }

                if (gcTray.Id == 0)
                {
                    _context.GCTrays.Add(gcTray);
                }
            }

            _context.SaveChanges();
        }

        public void CT_UpdateLCForCompletedCases(int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);
            UpdateLCForCompletedCases(minsToAdd);
        }

        public void CT_SettingGCTrayActiveStatus(List<long?> allLegacyIds)
        {
            var idsToIterate = allLegacyIds.Where(x => x.HasValue).Distinct().ToList();

            idsToIterate.ForEach(ctActualsLegacyId =>
            {
                var ctContainerTypeActual = _context.CTContainerTypeActuals.Where(x => x.LegacyId == ctActualsLegacyId).FirstOrDefault();

                if (ctContainerTypeActual != null)
                {
                    var gcTrays = _context.GCTrays.Include(x => x.GCCase).ThenInclude(x => x.CTCase).Where(x => x.CTContainerTypeActualId == ctContainerTypeActual.Id).ToList();
                    if (gcTrays != null)
                    {
                        gcTrays.ForEach(gcTray =>
                        {
                            if (gcTray.GCCaseId.HasValue)
                            {
                                gcTray.IsActiveStatus = false;
                            }
                        });
                    }

                    var nextGCTray = gcTrays.Where(x =>
                        x.CTContainerTypeActualId == ctContainerTypeActual.Id &&
                        x.GCCase != null && x.GCCase.CTCase != null &&
                        x.GCCase.CTCase.DueTimestamp > DateTime.UtcNow
                    ).OrderBy(x => x.GCCase.CTCase.DueTimestamp).FirstOrDefault();
                    if (nextGCTray != null)
                    {
                        nextGCTray.IsActiveStatus = true;
                    }
                }
            });

            _context.SaveChanges();
        }

        public void CT_UpdateColorStatusForGCCasesAndGCTrays(List<CTCase> ctCasesToUse)
        {
            Log.Information("Running UpdateColorStatusForGCCasesAndGCTrays...");
            Log.Information($"{ctCasesToUse.Count} cases to color...");
            ctCasesToUse.ForEach(ctCase =>
            {
                var gcCase = _context.GCCases.Where(x => x.CTCase.CaseReference == ctCase.CaseReference).Include(x => x.GCTrays).FirstOrDefault();
                if (gcCase != null)
                {
                    Log.Information($"-Processing GCCase: {gcCase.Title}...");
                    if (gcCase.GCTrays != null)
                    {
                        gcCase.GCTrays.ToList().ForEach(gcTray =>
                        {
                            Log.Information($"--Processing GCTrayId: {gcTray.Id}, with actualID: {gcTray.CTContainerTypeActualId}");

                            //GCTray is Active: GCTray.CTCaseContainerId != NULL && GCTray.CTContainerTypeActualId != NULL && GCTray.GCCase.CTCase.CartStatus != "Completed"

                            // by default, setting to RED, because some trays will have an Actual, but not a Location, hence no LifeCycle, hence no Color,
                            // which in theory won't happen, but reality hits us strong
                            int colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Red;

                            bool isAcutalAvailable = gcTray.CTContainerTypeActualId.HasValue;
                            Log.Information($"--IsAcutalAvailable: {isAcutalAvailable}");

                            GCTraysLifecycleStatusTypes currentLifeCycleStatus = null;
                            var activeLifeCycleStatustes = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == gcTray.Id && x.IsActiveStatus == true).OrderByDescending(x => x.Timestamp).ToList();
                            if (activeLifeCycleStatustes.Count > 1)
                            {
                                var error = $"More than one active LifeCycle statuses detected for GCTrayId = ${gcTray.Id}";
                                Log.Error(error);
                                // do not throw, handle for now:
                                //throw new Exception(error);
                                // instead, set only latest to active:
                                for (int i = 0; i < activeLifeCycleStatustes.Count; i++)
                                {
                                    if (i == 0)
                                    {
                                        currentLifeCycleStatus = activeLifeCycleStatustes[i];
                                    }
                                    else
                                    {
                                        activeLifeCycleStatustes[i].IsActiveStatus = false;
                                    }
                                }
                                _context.SaveChanges();
                            }
                            else if (activeLifeCycleStatustes.Count > 0)
                            {
                                currentLifeCycleStatus = activeLifeCycleStatustes.First();
                            }
                            else // activeLifeCycleStatustes.Count == 0;
                            {
                                if (isAcutalAvailable)
                                {
                                    if (gcTray.IsActualCurrentlyAvailable.HasValue && gcTray.IsActualCurrentlyAvailable.Value)
                                    {
                                        colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Yellow;
                                        Log.Information($"--Acutal is Available, setting to YELLOW");
                                    }
                                    else
                                    {
                                        colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Orange;
                                        Log.Information($"--Acutal is NOT Available, setting to ORANGE");
                                    }
                                }
                            }

                            if (isAcutalAvailable)
                            {
                                if (currentLifeCycleStatus != null)
                                {
                                    if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.LoanerInbound)
                                    {
                                        colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Orange;
                                        Log.Information($"--Current Status is LoanerInbound, setting to Orange");
                                    }
                                    else if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.LoanerOutbound ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Loaded ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.ShippedOutbound ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Delivered ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.InSurgery ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.SurgeryComplete ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Prepped ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Picked ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.ShippedInbound ||
                                        currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Received)
                                    {
                                        colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Green;
                                    }
                                    else
                                    {
                                        if (!isAcutalAvailable)
                                        {
                                            colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Red;
                                        }
                                        else
                                        {
                                            // If in Induction, it's RED
                                            if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.DeconStaged ||
                                                    currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.DeconStagedLoaner ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Sink ||
                                                    currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.SinkLoaner ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Washer ||
                                                    currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.WasherLoaner ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.AssemblyStaged ||
                                                    currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.AssemblyStagedLoaner ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Assembly ||
                                                    currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.AssemblyLoaner ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.CleanIncomplete)
                                            {
                                                colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Yellow;
                                            }
                                            else if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.QualityHold)
                                            {
                                                colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Purple;
                                            }
                                            else if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.CleanStaged)
                                            {
                                                colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Yellow;
                                            }
                                            else if (currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Verification ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.Sterilize ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.SterileStaged ||
                                                currentLifeCycleStatus.LifecycleStatusTypeId == (int)LifecycleStatusType.CaseAssembly)
                                            {
                                                var caseDate = ctCase.DueTimestamp.Date;
                                                List<GCTray> traysForActual = new List<GCTray>();
                                                bool isOrange = false;

                                                // Checking if Actual is allocated to a more recent case, if Yes, current tray is ORANGE
                                                if (gcTray.CTContainerTypeActualId.HasValue)
                                                {
                                                    traysForActual = _context.GCTrays.Include(x => x.GCCase).ThenInclude(x => x.CTCase).Where(x =>
                                                        x.CTContainerTypeActualId == gcTray.CTContainerTypeActualId.Value &&
                                                        x.GCCase.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE &&
                                                        x.GCCase.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_COMPLETE_ID &&
                                                        x.GCCase.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE &&
                                                        x.GCCase.CTCase.CartStatus != ProfitOptics.Modules.Sox.Helpers.Constants.CT_CASE_CART_INCOMPLETE_ID &&
                                                        x.GCCase.CTCase.DueTimestamp > DateTime.UtcNow).OrderBy(x => x.GCCase.CTCase.DueTimestamp).ToList();
                                                    if (traysForActual != null && traysForActual.Count > 1)
                                                    {
                                                        var nextCase = traysForActual.First().GCCase;
                                                        if (nextCase.Id != gcCase.Id)
                                                        {
                                                            isOrange = true;
                                                        }
                                                    }
                                                }

                                                if (isOrange)
                                                {
                                                    colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Orange;
                                                }
                                                else
                                                {
                                                    colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Yellow;

                                                    //if (caseDate == DateTime.UtcNow.Date)
                                                    //{
                                                    var gcTrayActiveLifecycleStatuses = _context.GCTraysLifecycleStatusTypes.Where(x => x.GCTrayId == gcTray.Id && x.IsActiveStatus == true).ToList();
                                                    if (gcTrayActiveLifecycleStatuses != null && gcTrayActiveLifecycleStatuses.Count > 0)
                                                    {
                                                        if (gcTrayActiveLifecycleStatuses.Count > 1)
                                                        {
                                                            var error = $"More than one active LifeCycle statuses detected for GCTrayId = ${gcTray.Id}";
                                                            Log.Error(error);
                                                            throw new Exception(error);
                                                        }
                                                        var activeLifeCycleStatus = gcTrayActiveLifecycleStatuses.First();
                                                        if (activeLifeCycleStatus.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.SterileStaged ||
                                                            activeLifeCycleStatus.LifecycleStatusTypeId == (int)ProfitOptics.Modules.Sox.Models.GroundControl.LifecycleStatusType.CaseAssembly)
                                                        {
                                                            colorId = (int)ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Green;
                                                        }
                                                    }
                                                    //}
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            UpdateGCTrayColorStatus(gcTray.Id, colorId);
                        });
                    }
                    UpdateGCCaseColorStatus(gcCase.Id);
                }
            });
        }

        public void CT_CancelOrDeleteCTCanceledCases(List<string> caseReferences, int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);
            CancelOrDeleteCTCanceledCases(caseReferences, minsToAdd);
        }

        public void CT_AddOrEditCTLocations(List<CTLocation> ctLocation)
        {
            ctLocation.ForEach(item =>
            {
                var existingLocation = _context.CTLocations.Where(x => x.LocationId == item.LocationId).FirstOrDefault();

                var ctLocationType = _context.CTLocationTypes.Where(x => x.Name == item.LocationType).FirstOrDefault();
                if (ctLocationType == null)
                {
                    Log.Error($"No CTLocationType found for Name: {item.LocationType}.");
                    throw new Exception();
                }

                int gcCustomerId = 0;
                var gcCustomer = _context.GCCustomers.Where(x => x.FacilityId == item.FacilityId.ToString()).FirstOrDefault();
                if (gcCustomer == null)
                {
                    Log.Error($"No GCCustomer found for FacilityId: {item.FacilityId}.");
                }
                else
                {
                    gcCustomerId = gcCustomer.Id;
                }

                if (existingLocation == null)
                {
                    item.CreatedAtUtc = DateTime.UtcNow;
                    item.CTLocationTypeId = ctLocationType.Id;
                    if (gcCustomerId > 0)
                    {
                        item.GCCustomerId = gcCustomer.Id;
                    }
                    _context.CTLocations.Add(item);
                }
                else
                {
                    existingLocation.LocationName = item.LocationName;
                    existingLocation.CTLocationTypeId = ctLocationType.Id;
                    existingLocation.AreaId = item.AreaId;
                    existingLocation.BuildingId = item.BuildingId;
                    existingLocation.SterilizationMethod = item.SterilizationMethod;
                    existingLocation.LocationInventoryDate = item.LocationInventoryDate;
                    existingLocation.LocationInventoryUser = item.LocationInventoryUser;
                    existingLocation.ModifiedAtUtc = DateTime.UtcNow;

                    if (gcCustomerId > 0)
                    {
                        existingLocation.GCCustomerId = gcCustomerId;
                    }
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTContainerTypeGraphics(List<CTContainerTypeGraphic> ctContainerTypeGraphics, long containerTypeId)
        {
            var ctContainer = _context.CTContainerTypes.Where(x => x.LegacyId == containerTypeId).FirstOrDefault();

            if (ctContainer == null)
            {
                Log.Error($"No CTContainerType found for LegacyId: {containerTypeId}.");
                throw new Exception();
            }

            ctContainerTypeGraphics.ForEach(item =>
            {
                var existing = _context.CTContainerTypeGraphics.Where(x => x.GraphicId == item.GraphicId).FirstOrDefault();

                if (existing == null)
                {
                    item.CTContainerTypeId = ctContainer.Id;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    item.GCImageId = null;
                    _context.CTContainerTypeGraphics.Add(item);
                }
                else
                {
                    existing.ContainerTypeId = item.ContainerTypeId;
                    existing.ImageName = item.ImageName;
                    existing.IsGlobal = item.IsGlobal;
                    existing.UsageFlags = item.UsageFlags;
                    existing.ReferenceId = item.ReferenceId;
                    existing.IsAssembly = item.IsAssembly;
                    existing.IsDecontam = item.IsDecontam;
                    existing.IsSterilization = item.IsSterilization;
                    existing.Path = item.Path;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public List<CTContainerTypeGraphic> CT_GetCTContainerTypeGraphic(bool? isWithoutImage)
        {
            var result = new List<CTContainerTypeGraphic>();

            var query = _context.CTContainerTypeGraphics.AsQueryable();

            if (isWithoutImage.HasValue)
            {
                if (isWithoutImage.Value == true)
                {
                    query = query.Where(x => !x.GCImageId.HasValue);
                }
                else
                {
                    query = query.Where(x => x.GCImageId.HasValue);
                }
            }

            result = query.ToList();

            return result;
        }

        public void CT_UpdateCTContainerTypeGraphic(int ctContainerTypeGraphicId, byte[] ctContainerTypeImage)
        {
            var graphic = _context.CTContainerTypeGraphics.Find(ctContainerTypeGraphicId);

            if (graphic != null)
            {
                var gcImage = new GCImage()
                {
                    Data = ctContainerTypeImage,
                    Origin = Constants.CT_ABBREVIATION_TWO_CHAR,
                    Title = graphic.ImageName,
                    CreatedAtUtc = DateTime.UtcNow
                };

                graphic.GCImage = gcImage;
                CreateThumbnailsForGCImage(gcImage);

                _context.SaveChanges();
            }
        }

        public List<CTBILoadResultGraphic> CT_GetCTLoadResultGraphics(bool? isLoadMeadiaWithoutImage)
        {
            var result = new List<CTBILoadResultGraphic>();

            var query = _context.CTBILoadResultGraphics.AsQueryable();

            if (isLoadMeadiaWithoutImage.HasValue)
            {
                if (isLoadMeadiaWithoutImage.Value == true)
                {
                    query = query.Where(x => !x.GCImageId.HasValue);
                }
                else
                {
                    query = query.Where(x => x.GCImageId.HasValue);
                }
            }

            result = query.ToList();

            return result;
        }

        public void CT_UpdateCTLoadResultGraphic(int id, byte[] ctBILoadResultImage)
        {
            var graphic = _context.CTBILoadResultGraphics.Find(id);

            if (graphic != null)
            {
                var gcImage = new GCImage()
                {
                    Data = ctBILoadResultImage,
                    Origin = Constants.CT_ABBREVIATION_TWO_CHAR,
                    Title = graphic.FileName,
                    CreatedAtUtc = DateTime.UtcNow
                };

                graphic.GCImage = gcImage;
                CreateThumbnailsForGCImage(gcImage);

                _context.SaveChanges();
            }
        }

        public List<CTAssemblyGraphic> CT_GetCTAssemblyGraphics(bool? isWithoutImage)
        {
            var result = new List<CTAssemblyGraphic>();

            var query = _context.CTAssemblyGraphics.AsQueryable();

            if (isWithoutImage.HasValue)
            {
                if (isWithoutImage.Value == true)
                {
                    query = query.Where(x => !x.GCImageId.HasValue);
                }
                else
                {
                    query = query.Where(x => x.GCImageId.HasValue);
                }
            }

            result = query.ToList();

            return result;
        }

        public List<CTQualityFeedItemGraphic> CT_GetCTQualityFeedItemGraphic(bool? isWithoutImage)
        {
            var result = new List<CTQualityFeedItemGraphic>();

            var query = _context.CTQualityFeedItemGraphics.AsQueryable();

            if (isWithoutImage.HasValue)
            {
                if (isWithoutImage.Value == true)
                {
                    query = query.Where(x => !x.GCImageId.HasValue);
                }
                else
                {
                    query = query.Where(x => x.GCImageId.HasValue);
                }
            }

            result = query.ToList();

            return result;
        }

        public void CT_UpdateCTAssemblyGraphicsGraphic(int id, byte[] imageBytes)
        {
            var graphic = _context.CTAssemblyGraphics.Find(id);

            if (graphic != null)
            {
                var gcImage = new GCImage()
                {
                    Data = imageBytes,
                    Origin = Constants.CT_ABBREVIATION_TWO_CHAR,
                    Title = graphic.Description,
                    CreatedAtUtc = DateTime.UtcNow
                };

                graphic.GCImage = gcImage;
                CreateThumbnailsForGCImage(gcImage);

                _context.SaveChanges();
            }
        }

        public void CT_UpdateCTQualityFeedItemGraphic(int id, byte[] imageBytes)
        {
            var graphic = _context.CTQualityFeedItemGraphics.Find(id);

            if (graphic != null)
            {
                var gcImage = new GCImage()
                {
                    Data = imageBytes,
                    Origin = Constants.CT_ABBREVIATION_TWO_CHAR,
                    Title = graphic.Description,
                    CreatedAtUtc = DateTime.UtcNow
                };

                graphic.GCImage = gcImage;
                CreateThumbnailsForGCImage(gcImage);

                _context.SaveChanges();
            }
        }

        public bool CT_AddOrEditCTBILoadResult(CTBILoadResult item, int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            bool isImportSuccessfull = true;

            var existingBILoadResult = _context.CTBILoadResults.Where(x => x.LoadId == item.LoadId).FirstOrDefault();

            int hrsBack = -6;

            if (existingBILoadResult != null &&
                existingBILoadResult.IsImportSuccessfull.HasValue &&
                existingBILoadResult.IsImportSuccessfull.Value &&
                existingBILoadResult.CreatedAtUtc < DateTime.UtcNow.AddHours(hrsBack)) // Load Results in past 'hrsBack' hrs just in case
            {
                Log.Information($"-Found CTBILoadResult Items for loadId: {item.LoadId}, older than {hrsBack * -1}hrs, import is COMPLETE");
                isImportSuccessfull = true;
            }
            else
            {
                if (existingBILoadResult == null)
                {
                    Log.Information($"-NOT Found CTBILoadResult Items for loadId: {item.LoadId}, older than {hrsBack * -1}hrs, import Continues");
                    item.IsImportSuccessfull = false;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    //item.LocationName = item.Location;
                    item.LoadDate = (item.LoadDateOriginal.HasValue && item.LoadDateOriginal.Value > DateTime.MinValue) ? item.LoadDateOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    item.InsertTimestamp = (item.InsertTimestampOriginal.HasValue && item.InsertTimestampOriginal.Value > DateTime.MinValue) ? item.InsertTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    item.UpdateTimestamp = (item.UpdateTimestampOriginal.HasValue && item.UpdateTimestampOriginal.Value > DateTime.MinValue) ? item.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    item.LoadTimestamp = (item.LoadTimestampOriginal.HasValue && item.LoadTimestampOriginal.Value > DateTime.MinValue) ? item.LoadTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    _context.CTBILoadResults.Add(item);
                }
                else
                {
                    Log.Information($"-Found CTBILoadResult Items for loadId: {item.LoadId}, NOT older than {hrsBack * -1}hrs, import Continues");
                    existingBILoadResult.LoadBarcode = item.LoadBarcode;
                    existingBILoadResult.Location = item.Location;
                    existingBILoadResult.LocationId = item.LocationId;
                    existingBILoadResult.LocationName = item.LocationName;
                    existingBILoadResult.LocationMethodId = item.LocationMethodId;
                    existingBILoadResult.LocationMethod = item.LocationMethod;
                    existingBILoadResult.LocationType = item.LocationType;
                    existingBILoadResult.LoadDateOriginal = item.LoadDateOriginal;
                    existingBILoadResult.LoadDate = (item.LoadDateOriginal.HasValue && item.LoadDateOriginal.Value > DateTime.MinValue) ? item.LoadDateOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existingBILoadResult.InsertTimestampOriginal = item.InsertTimestampOriginal;
                    existingBILoadResult.InsertTimestamp = (item.InsertTimestampOriginal.HasValue && item.InsertTimestampOriginal.Value > DateTime.MinValue) ? item.InsertTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existingBILoadResult.UpdateTimestampOriginal = item.UpdateTimestampOriginal;
                    existingBILoadResult.UpdateTimestamp = (item.UpdateTimestampOriginal.HasValue && item.UpdateTimestampOriginal.Value > DateTime.MinValue) ? item.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existingBILoadResult.LoadTimestampOriginal = item.LoadTimestampOriginal;
                    existingBILoadResult.LoadTimestamp = (item.LoadTimestampOriginal.HasValue && item.LoadTimestampOriginal.Value > DateTime.MinValue) ? item.LoadTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existingBILoadResult.UpdateUser = item.UpdateUser;
                    existingBILoadResult.Operator = item.Operator;
                    existingBILoadResult.SterilizationCycle = item.SterilizationCycle;
                    existingBILoadResult.CaseReference = item.CaseReference;
                    existingBILoadResult.Reason = item.Reason;
                    existingBILoadResult.Notes = item.Notes;
                    existingBILoadResult.ResultFlag = item.ResultFlag;
                    existingBILoadResult.Alarm = item.Alarm;
                    existingBILoadResult.IsLastLoad = item.IsLastLoad;
                    existingBILoadResult.SterilantBatch = item.SterilantBatch;
                    existingBILoadResult.MaxTemperature = item.MaxTemperature;
                    existingBILoadResult.ExposureTime = item.ExposureTime;
                    existingBILoadResult.ModifiedAtUtc = DateTime.UtcNow;
                }

                _context.SaveChanges();

                isImportSuccessfull = false;
            }

            return isImportSuccessfull;
        }

        public void CT_AddOrEditCTBILoadResultContents(List<CTBILoadResultContent> contents, long loadId, int serverOffsetInMinsFromUTC)
        {
            Log.Information($"--Editing {contents.Count} CTBILoadResultContents LoadId: {loadId}...");

            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            contents.ForEach(item =>
            {
                Log.Information($"---Editing Content with AssetID (LegacyID): {item.AssetId}, Id: {item.CTContainerTypeActualId} CTBILoadResultContents LoadId: {loadId}...");

                var ctBILoadResult = _context.CTBILoadResults.Where(x => x.LoadId == loadId).FirstOrDefault();

                if (ctBILoadResult == null)
                {
                    Log.Error($"No CTBILoadResult found for LoadId: {loadId}.");
                    throw new Exception();
                }

                var ctContainerTypeActual = _context.CTContainerTypeActuals.Where(x => x.LegacyId == item.AssetId).FirstOrDefault();

                if (ctContainerTypeActual == null)
                {
                    Log.Error($"No CTContainerTypeActual found for LegacyId: {item.AssetId}.");
                    throw new Exception();
                }
                else
                {
                    Log.Information($"---Found CTContainerTypeActual for LegacyId: {item.AssetId}, Id: {item.CTContainerTypeActualId}");
                }

                var existing = _context.CTBILoadResultContents.Where(x => x.LoadId == loadId && x.AssetId == item.AssetId).FirstOrDefault();

                if (existing == null)
                {
                    Log.Information($"---NO CTBILoadResultContent Found for LegacyId: {item.AssetId}, Id: {item.CTContainerTypeActualId}, Creating now!");
                    item.CTBILoadResultId = ctBILoadResult.Id;
                    item.CTContainerTypeActualId = ctContainerTypeActual.Id;
                    item.LoadId = loadId;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    item.LastUpdate = item.LastUpdateOriginal.AddMinutes(minsToAdd);
                    _context.CTBILoadResultContents.Add(item);
                }
                else
                {
                    Log.Information($"---Found CTBILoadResultContent for LegacyId: {item.AssetId}, Id: {item.CTContainerTypeActualId}, Updating now!");
                    existing.CTBILoadResultId = ctBILoadResult.Id;
                    existing.CTContainerTypeActualId = ctContainerTypeActual.Id;
                    existing.LoadId = item.LoadId;
                    existing.Quantity = item.Quantity;
                    existing.Description = item.Description;
                    existing.Container = item.Container;
                    existing.UpdatedBy = item.UpdatedBy;
                    existing.LastUpdateOriginal = item.LastUpdateOriginal;
                    existing.LastUpdate = item.LastUpdateOriginal.AddMinutes(minsToAdd);
                    existing.LastUpdateOriginal = item.LastUpdateOriginal;
                    existing.LastContainer = item.LastContainer;
                    existing.LastLocation = item.LastLocation;
                    existing.Sort = item.Sort;
                    existing.BiotestFlag = item.BiotestFlag;
                    existing.AssetId = item.AssetId;
                    existing.AssetType = item.AssetType;
                    existing.CaseReference = item.CaseReference;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTBILoadResultIndicators(List<CTBILoadResultIndicator> indicators, long loadId, int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            var ctBILoadResult = _context.CTBILoadResults.Where(x => x.LoadId == loadId).FirstOrDefault();

            if (ctBILoadResult == null)
            {
                Log.Error($"No CTBILoadResult found for LoadId: {loadId}.");
                throw new Exception();
            }

            Log.Information($"--Preparing to edit CTBILoadResultIndicators Items for loadId: {loadId}...");

            var existingIndicators = _context.CTBILoadResultIndicators.Where(x => x.LoadId == loadId).ToList();
            if (existingIndicators != null && existingIndicators.Count > 0)
            {
                Log.Information($"--Found {existingIndicators.Count} CTBILoadResultIndicators Items for loadId: {loadId}...");
                Log.Information($"--REMOVING {existingIndicators.Count} CTBILoadResultIndicators Items for loadId: {loadId}...");
                _context.CTBILoadResultIndicators.RemoveRange(existingIndicators);
                _context.SaveChanges();
            }

            indicators.ForEach(item =>
            {
                Log.Information($"--Processing LoadIndicatorId: {item.LoadIndicatorId}...");

                var existing = _context.CTBILoadResultIndicators.Where(x => x.LoadId == loadId && x.LoadIndicatorId == item.LoadIndicatorId).FirstOrDefault();

                if (existing == null)
                {
                    Log.Information($"--NOT found for LoadIndicatorId: {item.LoadIndicatorId}, adding now...");
                    item.CTBILoadResultId = ctBILoadResult.Id;
                    item.LoadId = loadId;
                    if (item.InsertTimestampOriginal.HasValue && item.InsertTimestampOriginal.Value > DateTime.MinValue) item.InsertTimestamp = item.InsertTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (item.UpdateTimestampOriginal.HasValue && item.UpdateTimestampOriginal.Value > DateTime.MinValue) item.UpdateTimestamp = item.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd);
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTBILoadResultIndicators.Add(item);
                }
                else
                {
                    Log.Information($"--FOUND for LoadIndicatorId: {item.LoadIndicatorId}, updating now...");
                    existing.CTBILoadResultId = ctBILoadResult.Id;
                    existing.IndicatorType = item.IndicatorType;
                    existing.IndicatorName = item.IndicatorName;
                    existing.PassLabel = item.PassLabel;
                    existing.IncubationFlag = item.IncubationFlag;
                    existing.DisplayOrder = item.DisplayOrder;
                    existing.DefaultProduct = item.DefaultProduct;
                    existing.SterilizerGroupId = item.SterilizerGroupId;
                    existing.IndicatorProduct = item.IndicatorProduct;
                    existing.IndicatorLot = item.IndicatorLot;
                    existing.ResultFlag = item.ResultFlag;
                    existing.InsertTimestampOriginal = item.InsertTimestampOriginal;
                    existing.InsertTimestamp = (item.InsertTimestampOriginal.HasValue && item.InsertTimestampOriginal.Value > DateTime.MinValue) ? item.InsertTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existing.UpdateTimestampOriginal = item.UpdateTimestampOriginal;
                    existing.UpdateTimestamp = (item.UpdateTimestampOriginal.HasValue && item.UpdateTimestampOriginal.Value > DateTime.MinValue) ? item.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existing.StartUser = item.StartUser;
                    existing.UserId = item.UserId;
                    existing.Quantity = item.Quantity;
                    existing.Exclude = item.Exclude;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void CT_AddOrEditCTBILoadResultGraphics(List<CTBILoadResultGraphic> graphics, long loadId, int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            Log.Information($"--Preparing to edit CTBILoadResultGraphics Items for loadId: {loadId}...");
            graphics.ForEach(item =>
            {
                Log.Information($"--Processing GraphicsID: {item.GraphicId}...");
                var ctBILoadResult = _context.CTBILoadResults.Where(x => x.LoadId == loadId).FirstOrDefault();

                if (ctBILoadResult == null)
                {
                    Log.Error($"No CTBILoadResult found for LoadId: {loadId}.");
                    throw new Exception();
                }

                var existing = _context.CTBILoadResultGraphics.Where(x => x.LoadId == loadId && x.GraphicId == item.GraphicId).FirstOrDefault();

                if (existing == null)
                {
                    Log.Information($"--NOT found for GraphicId: {item.GraphicId}, adding now...");
                    item.CTBILoadResultId = ctBILoadResult.Id;
                    item.LoadId = loadId;
                    item.GCImageId = null;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    item.UpdateTimestamp = item.UpdateTimestampOriginal.AddMinutes(minsToAdd);
                    _context.CTBILoadResultGraphics.Add(item);
                }
                else
                {
                    Log.Information($"--FOUND for GraphicId: {item.GraphicId}, updating now...");
                    existing.CTBILoadResultId = ctBILoadResult.Id;
                    existing.IsGlobal = item.IsGlobal;
                    existing.FileName = item.FileName;
                    existing.Path = item.Path;
                    existing.UserName = item.UserName;
                    existing.UpdateTimestamp = item.UpdateTimestampOriginal.AddMinutes(minsToAdd);
                    existing.UseLocalImage = item.UseLocalImage;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void CT_UpdateCTLoadResultImportStatus(long loadId, bool isImportSuccessfull)
        {
            var loadResult = _context.CTBILoadResults.Where(x => x.LoadId == loadId).FirstOrDefault();
            if (loadResult != null)
            {
                loadResult.IsImportSuccessfull = isImportSuccessfull;
                _context.SaveChanges();
            }
        }

        public void CT_SyncTimestamps(int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            var gcTraysLifecycleStatusTypes = _context.GCTraysLifecycleStatusTypes.ToList();
            if (gcTraysLifecycleStatusTypes != null && gcTraysLifecycleStatusTypes.Count > 0)
            {
                gcTraysLifecycleStatusTypes.ForEach(x =>
                {
                    x.Timestamp = x.TimestampOriginal.AddMinutes(minsToAdd);
                });
            }

            var ctContainerTypeActualHistoryItems = _context.CTContainerTypeActualHistoryItems.ToList();
            if (ctContainerTypeActualHistoryItems != null && ctContainerTypeActualHistoryItems.Count > 0)
            {
                ctContainerTypeActualHistoryItems.ForEach(x =>
                {
                    x.UpdateTimestamp = x.UpdateTimestampOriginal.AddMinutes(minsToAdd);
                });
            }

            var ctCaseContainers = _context.CTCaseContainers.ToList();
            if (ctCaseContainers != null && ctCaseContainers.Count > 0)
            {
                ctCaseContainers.ForEach(x =>
                {
                    x.DueTimestamp = x.DueTimestampOriginal.AddMinutes(minsToAdd);
                    if (x.EndTimestampOriginal.HasValue) x.EndTimestamp = x.EndTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (x.UpdateTimestampOriginal.HasValue) x.UpdateTimestamp = x.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (x.MultipleBasisOriginal.HasValue) x.MultipleBasis = x.MultipleBasisOriginal.Value.AddMinutes(minsToAdd);
                });
            }

            var ctBILoadResults = _context.CTBILoadResults.ToList();
            if (ctBILoadResults != null && ctBILoadResults.Count > 0)
            {
                ctBILoadResults.ForEach(x =>
                {
                    if (x.LoadDateOriginal.HasValue) x.LoadDate = x.LoadDateOriginal.Value.AddMinutes(minsToAdd);
                    if (x.InsertTimestampOriginal.HasValue) x.InsertTimestamp = x.InsertTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (x.UpdateTimestampOriginal.HasValue) x.UpdateTimestamp = x.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (x.LoadTimestampOriginal.HasValue) x.LoadTimestamp = x.LoadTimestampOriginal.Value.AddMinutes(minsToAdd);
                });
            }

            var ctBILoadResultContents = _context.CTBILoadResultContents.ToList();
            if (ctBILoadResultContents != null && ctBILoadResultContents.Count > 0)
            {
                ctBILoadResultContents.ForEach(x =>
                {
                    x.LastUpdate = x.LastUpdateOriginal.AddMinutes(minsToAdd);
                });
            }

            var ctBILoadResultIndicators = _context.CTBILoadResultIndicators.ToList();
            if (ctBILoadResultIndicators != null && ctBILoadResultIndicators.Count > 0)
            {
                ctBILoadResultIndicators.ForEach(x =>
                {
                    if (x.InsertTimestampOriginal.HasValue) x.InsertTimestamp = x.InsertTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (x.UpdateTimestampOriginal.HasValue) x.UpdateTimestamp = x.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd);
                });
            }

            var ctBILoadResultGraphics = _context.CTBILoadResultGraphics.ToList();
            if (ctBILoadResultGraphics != null && ctBILoadResultGraphics.Count > 0)
            {
                ctBILoadResultGraphics.ForEach(x =>
                {
                    x.UpdateTimestamp = x.UpdateTimestampOriginal.AddMinutes(minsToAdd);
                });
            }

            _context.SaveChanges();
        }

        public List<GCTraysLifecycleStatusTypes> CT_GetAssemblyStatuses()
        {
            return GetAssemblyStatuses();
        }

        public List<long> CT_GetContainerAssemblyActualIds()
        {
            return CT_GetContainerAssemblyActualIds();
        }

        public void CT_AddCTAssemblyGraphic(ETLCTAssemblyGraphic assemblyGraphic, int statusTypeId, long legacyId)
        {
            CTAssemblyGraphic graphic = new CTAssemblyGraphic()
            {
                CensisSetHistoryId = assemblyGraphic.CensisSetHistoryId.ToString(),
                ContainerTypeId = assemblyGraphic.ContainerTypeId,
                Description = assemblyGraphic.Description,
                GraphicId = assemblyGraphic.GraphicId,
                LifecycleStatusTypeId = statusTypeId,
                ActualLegacyId = legacyId
            };
            AddCTAssemblyGraphic(graphic);
        }

        public void CT_AddCTAssemblyCountSheet(ETLCTContainerAssemblyCountSheetItem countSheet, string setName, int statusTypeId)
        {
            CTAssemblyCountSheet cs = new CTAssemblyCountSheet()
            {
                ContainerTypeId = countSheet.SetId,
                Description = countSheet.ItemName,
                VendorName = countSheet.VendorName,
                ModelNumber = countSheet.ModelNumber,
                RequiredCount = countSheet.RequiredCount,
                ActualCount = countSheet.ActualCount,
                SetName = setName,
                TrayLifecycleStatusTypeId = statusTypeId
            };
            AddCTAssemblyCountSheet(cs);
        }

        public void CT_AddOrEditCTQualityFeedReport(List<CTQualityFeedItem> qFeedReportList)
        {
            qFeedReportList.ForEach(qfeedItem =>
            {
                Log.Information($"----Processing QFeedItem, Actual: {qfeedItem.AssetName}, LegacyId: {qfeedItem.LegacyId}");
                var existing = _context.CTQualityFeedItems.Where(x =>
                    x.ReportType == qfeedItem.ReportType && x.LegacyId == qfeedItem.LegacyId).FirstOrDefault();

                if (existing == null)
                {
                    existing = qfeedItem;
                    existing.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTQualityFeedItems.Add(existing);
                }

                if (!existing.CTContainerTypeActualId.HasValue)
                {
                    // Try fo find actual
                    string parentName = null;
                    string actualName = null;

                    if (existing.AssetName != null)
                    {
                        var qFeedNameChunks = existing.AssetName.Split(" ").ToList();

                        if (qFeedNameChunks == null || qFeedNameChunks.Count < 2)
                        {
                            var error = $"Invalid AssetName: {existing.AssetName}, for LegacyId {existing.LegacyId}";
                            Log.Error(error);
                            throw new Exception(error);
                        }
                        else
                        {
                            actualName = qFeedNameChunks.Last();
                            parentName = existing.AssetName.Substring(0, (existing.AssetName.Length - qFeedNameChunks.Last().Length - 1));
                        }

                        if (parentName != null && actualName != null)
                        {
                            var actual = _context.CTContainerTypeActuals.Where(x => x.ParentName == parentName && x.Name == actualName).FirstOrDefault();
                            if (actual != null)
                            {
                                qfeedItem.CTContainerTypeActualId = actual.Id;
                                _context.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        var error = $"NO AssetName for LegacyId {existing.LegacyId}, date: {existing.UpdatedDate}, Report type: {existing.ReportType}, skipping...";
                        Log.Error(error);
                    }
                }

                if (existing.CTContainerTypeActualId.HasValue &&
                    !existing.GCTraysLifecycleStatusTypeId.HasValue &&
                        existing.ReportType == CTQualityFeedReportType.QAPass.GetDescription())
                {
                    var gcTray = _context.GCTrays
                            .Include(x => x.GCCase)
                            .Include(x => x.GCTraysLifecycleStatusTypes)
                        .Where(x =>
                            x.CTContainerTypeActualId == existing.CTContainerTypeActualId.Value &&
                            x.GCCaseId.HasValue && x.GCCase != null && x.GCCase.DueTimestamp > existing.UpdatedDate)
                        .OrderByDescending(x => x.GCCase.DueTimestamp).FirstOrDefault();

                    if (gcTray != null)
                    {
                        bool shouldCreateVerification = false;

                        if (gcTray.GCTraysLifecycleStatusTypes != null)
                        {
                            var verificationLC = gcTray.GCTraysLifecycleStatusTypes.Where(x => x.LifecycleStatusTypeId == (int)LifecycleStatusType.Verification).FirstOrDefault();
                            if (verificationLC != null)
                            {
                                if (verificationLC.TimestampOriginal == existing.UpdatedDate)
                                {
                                    existing.GCTraysLifecycleStatusTypeId = verificationLC.Id;
                                }
                                else
                                {
                                    int alertTypeId = (int)Models.GroundControl.GCSystemAlertType.GCTrayVerificationQualityFeedDataMismatch;

                                    if (!_context.GCSystemAlerts.Any(x => x.CreatedAtUtc > DateTime.UtcNow.AddHours(Helpers.Constants.GC_SYSTEM_ALERT_TIME_TO_LOOK_BACK) &&
                                        x.GCTrayId == gcTray.Id &&
                                        x.GCSystemAlertTypeId == alertTypeId))
                                    {
                                        Log.Information($"--No Alert exists ({(Models.GroundControl.GCSystemAlertType)alertTypeId}), adding now...");
                                        _context.GCSystemAlerts.Add(new GCSystemAlert()
                                        {
                                            GCTrayId = gcTray.Id,
                                            GCSystemAlertTypeId = alertTypeId,
                                            CreatedAtUtc = DateTime.UtcNow,
                                            MarkedAsReadAtUtc = null
                                        });
                                        _context.SaveChanges();
                                    }
                                    else
                                    {
                                        Log.Information($"--Alert already exists, no action...");
                                    }
                                }
                            }
                            else
                            {
                                shouldCreateVerification = true;
                            }
                        }
                        else
                        {
                            gcTray.GCTraysLifecycleStatusTypes = new List<GCTraysLifecycleStatusTypes>();
                            shouldCreateVerification = true;
                        }

                        if (shouldCreateVerification)
                        {
                            var verificationLC = new GCTraysLifecycleStatusTypes()
                            {
                                GCTrayId = gcTray.Id,
                                IsActiveStatus = false,
                                LifecycleStatusTypeId = (int)LifecycleStatusType.Verification,
                                Timestamp = qfeedItem.UpdatedDateUtc,
                                TimestampOriginal = qfeedItem.UpdatedDate,
                                User = qfeedItem.ResponsibleParty,
                                WWPodContainerId = null
                            };

                            gcTray.GCTraysLifecycleStatusTypes.Add(verificationLC);
                            existing.GCTraysLifecycleStatusType = verificationLC;
                            _context.SaveChanges();
                        }
                    }
                }
            });
        }

        public void CT_AddOrEditCTQualityFeedItemGraphics(List<CTQualityFeedItemGraphic> qFeedItemsGraphics)
        {
            Log.Information($"--Preparing to edit CTQualityFeedItemGraphic");
            qFeedItemsGraphics.ForEach(item =>
            {
                Log.Information($"--Processing GraphicsID: {item.GraphicId}...");
                var qFeedItem = _context.CTQualityFeedItems.Where(x => x.LegacyId == item.LoadId).FirstOrDefault();
                if(qFeedItem == null)
                {
                    Log.Error($"No CTQualityFeedItem found for LegacyId: {item.LoadId}.");
                    throw new Exception();
                }

                var existing = _context.CTQualityFeedItemGraphics.Where(x => x.LoadId == item.LoadId && x.GraphicId == item.GraphicId).FirstOrDefault();

                if (existing == null)
                {
                    Log.Information($"--NOT found for GraphicId: {item.GraphicId}, adding now...");
                    item.CTQualityFeedItemId = qFeedItem.Id;
                    item.GCImageId = null;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTQualityFeedItemGraphics.Add(item);
                }
                else
                {
                    Log.Information($"--FOUND for GraphicId: {item.GraphicId}, updating now...");
                    existing.CTQualityFeedItemId = qFeedItem.Id;
                    existing.Description = item.Description;
                    existing.ReferenceId = item.ReferenceId;
                    existing.GraphicId = item.GraphicId; 
                    existing.IsGlobal = item.IsGlobal;
                    existing.LoadId = item.LoadId;
                    existing.UpdateTimestamp = item.UpdateTimestamp;
                    existing.UpdateUserId = item.UpdateUserId;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void CheckForCaseAssemblyAndTransportationStaging(int clientId, string caseReference, int serverOffsetInMinsFromUTC, bool? isStopLifecycleHistory = false)
        {
            var gcCase = _context.GCCases.Where(x => x.Title == caseReference).FirstOrDefault();

            if (gcCase != null)
            {
                int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

                var caseContainers = _context.CTCaseContainers.Where(x => x.CaseReference == caseReference).ToList();

                if (caseContainers != null && caseContainers.Count > 0)
                {
                    caseContainers.ForEach(caseContainer =>
                    {
                        if (!caseContainer.ContainerId.HasValue)
                        {
                            Log.Warning($"No Actual found for CaseReference: {caseContainer.CaseReference}, ContainerName: {caseContainer.ContainerName}");
                        }
                        else
                        {
                            var historyItemsResult = CT_GetCTContainerTypeActualHistoryItems(clientId, caseContainer.ContainerId.Value);
                            if (historyItemsResult != null && historyItemsResult.Result.ETLCTContainerTypeActualHistoryItems != null)
                            {
                                var historyItems = historyItemsResult.Result.ETLCTContainerTypeActualHistoryItems;
                                CT_AddOrEditCTContainerTypeActualHistoryItemsAndGCTrays(historyItems.Select(x => _mapper.Map<Framework.DataLayer.CTContainerTypeActualHistoryItem>(x)).ToList(), caseContainer.ContainerId.Value, serverOffsetInMinsFromUTC, isStopLifecycleHistory, false);
                            }
                        }

                        var gcTray = _context.GCTrays.Include(x => x.GCTraysLifecycleStatusTypes).Where(x => x.GCCaseId == gcCase.Id && x.CTCaseContainerId == caseContainer.Id).FirstOrDefault();
                        if (gcTray != null && gcTray.CTContainerTypeActualId.HasValue && gcTray.GCTraysLifecycleStatusTypes != null && gcTray.GCTraysLifecycleStatusTypes.Count > 0)
                        {
                            var latestLC = gcTray.GCTraysLifecycleStatusTypes.OrderByDescending(x => x.Timestamp).FirstOrDefault();

                            var nextHIsAfterSterileStaged = _context.CTContainerTypeActualHistoryItems
                            .Include(x => x.CTLocation).ThenInclude(x => x.CTLocationType).ThenInclude(x => x.CTLocationTypeGCLifecycleStatusTypes)
                            .Where(hi =>
                                (hi.Status.Equals(Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_LOCATION_CHG) ||
                                    hi.Status.Equals(Helpers.Constants.CT_CONTAINER_TYPE_ACTUAL_HISTORY_STATUS_CHECK_INTO_STERILIZER)) &&
                                hi.CTContainerTypeActualId == gcTray.CTContainerTypeActualId &&
                                hi.UpdateTimestamp > latestLC.Timestamp
                            ).OrderBy(x => x.UpdateTimestamp).Take(2).ToList();

                            CTContainerTypeActualHistoryItem transportationStagedHI = null;

                            if (latestLC.LifecycleStatusTypeId == (int)LifecycleStatusType.SterileStaged)
                            {
                                if (nextHIsAfterSterileStaged != null && nextHIsAfterSterileStaged.Count > 0)
                                {
                                    var firstNextHI = nextHIsAfterSterileStaged.First();
                                    if (firstNextHI.CTLocation != null &&
                                        firstNextHI.CTLocation.CTLocationType != null &&
                                            firstNextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null &&
                                                firstNextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.Count > 0)
                                    {
                                        var firstLifeCycleLocationType = firstNextHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First();
                                        if (firstLifeCycleLocationType.GCLifecycleStatusTypeId != (int)LifecycleStatusType.CaseAssembly)
                                        {
                                            var message = $"Next HI should be: {LifecycleStatusType.CaseAssembly}, but is: {(LifecycleStatusType)firstLifeCycleLocationType.GCLifecycleStatusTypeId}";
                                            Log.Error(message);
                                        }
                                        else
                                        {
                                            gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                            {
                                                GCTrayId = gcTray.Id,
                                                IsActiveStatus = false,
                                                LifecycleStatusTypeId = (int)LifecycleStatusType.CaseAssembly,
                                                User = firstNextHI.UpdateUserId,
                                                Timestamp = firstNextHI.UpdateTimestamp,
                                                TimestampOriginal = firstNextHI.UpdateTimestampOriginal,
                                                WWPodContainerId = null
                                            });
                                            _context.SaveChanges();
                                        }
                                    }

                                    if (nextHIsAfterSterileStaged.Count > 1)
                                    {
                                        transportationStagedHI = nextHIsAfterSterileStaged[1];
                                    }
                                }
                            }

                            if (latestLC.LifecycleStatusTypeId == (int)LifecycleStatusType.CaseAssembly)
                            {
                                transportationStagedHI = nextHIsAfterSterileStaged.First();
                            }

                            if (transportationStagedHI != null &&
                                    transportationStagedHI.CTLocation != null &&
                                        transportationStagedHI.CTLocation.CTLocationType != null &&
                                            transportationStagedHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes != null &&
                                                transportationStagedHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.Count > 0)
                            {
                                var firstLifeCycleLocationType = transportationStagedHI.CTLocation.CTLocationType.CTLocationTypeGCLifecycleStatusTypes.First();
                                if (firstLifeCycleLocationType.GCLifecycleStatusTypeId != (int)LifecycleStatusType.CaseAssembly)
                                {
                                    var message = $"Next HI should be: {LifecycleStatusType.CaseAssembly}, but is: {(LifecycleStatusType)firstLifeCycleLocationType.GCLifecycleStatusTypeId}";
                                    Log.Error(message);
                                }
                                else
                                {
                                    gcTray.GCTraysLifecycleStatusTypes.Add(new GCTraysLifecycleStatusTypes()
                                    {
                                        GCTrayId = gcTray.Id,
                                        IsActiveStatus = false,
                                        LifecycleStatusTypeId = (int)LifecycleStatusType.CaseAssembly,
                                        User = transportationStagedHI.UpdateUserId,
                                        Timestamp = transportationStagedHI.UpdateTimestamp,
                                        TimestampOriginal = transportationStagedHI.UpdateTimestampOriginal,
                                        WWPodContainerId = null
                                    });
                                    _context.SaveChanges();
                                }
                            }
                        }
                    });
                }
            }
        }

        #endregion CT GC Services

        #endregion CensiTrack
    }
}

