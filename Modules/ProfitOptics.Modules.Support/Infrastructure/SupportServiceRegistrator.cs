﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.Support.Data;
using ProfitOptics.Modules.Support.Factories;
using ProfitOptics.Modules.Support.Services;

namespace ProfitOptics.Modules.Support.Infrastructure
{
    public sealed class SupportServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<ISupportFactory, SupportFactory>();
            services.AddScoped<ISupportService, SupportService>();
            services.AddScoped<IJiraService, JiraService>();
            services.AddScoped<IPersistedDataService, PersistedDataService>();
            services.AddScoped<ISupportMemoryCacheService, SupportMemoryCacheService>();

            StartupConfiguration.ConfigureStartupConfig<SupportSettings>(services, configuration); 
        }
    }
}