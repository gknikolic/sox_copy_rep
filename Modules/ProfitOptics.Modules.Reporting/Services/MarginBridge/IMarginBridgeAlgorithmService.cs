﻿using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Services.MarginBridge
{
    public interface IMarginBridgeAlgorithmService
    {
        OutputData Process1(InputData period1, InputData period2, MarginBridgeType marginBridgeType);
    }
}