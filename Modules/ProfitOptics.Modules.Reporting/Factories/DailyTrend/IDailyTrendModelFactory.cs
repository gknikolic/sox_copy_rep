﻿using System;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.DailyTrend;

namespace ProfitOptics.Modules.Reporting.Factories.DailyTrend
{
    public interface IDailyTrendModelFactory
    {
        /// <summary>
        /// Creates the daily trend view model for the specified user identifier.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        DailyTrendViewModel CreateDailyTrendViewModel(int userId);

        /// <summary>
        /// Creates the daily trend models for the specified parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        PagedList<DailyTrendModel> CreateDailyTrendModels(int userId,DateRangeDto dateRange);

        /// <summary>
        /// Creates the daily trend drill through all view model for the specified parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="drillThroughDate"></param>
        /// <returns></returns>
        DailyTrendDrillThroughAllViewModel CreateDailyTrendDrillThroughAllViewModel(int userId, DateTime drillThroughDate);

        /// <summary>
        /// Creates the daily trend details models.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="drillThroughDate"></param>
        /// <returns></returns>
        PagedList<DailyTrendDetailsModel> CreateDailyTrendDetailsModels(int userId,
            IDataTablesRequest request, DateTime drillThroughDate);

        /// <summary>
        /// Creates the daily trend drill through by type view model for the specified paramteres.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="drillThroughType"></param>
        /// <param name="drillThroughDate"></param>
        /// <returns></returns>
        DailyTrendDrillThroughByTypeViewModel CreateDailyTrendDrillThroughByTypeViewModel(int userId, DailyTrendDrillThroughType drillThroughType, DateTime drillThroughDate);

        /// <summary>
        /// Creates the daily trend grouped models.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="drillThroughType"></param>
        /// <param name="drillThroughDate"></param>
        PagedList<DailyTrendGroupedModel> CreateDailyTrendGroupedModels(int userId, IDataTablesRequest request, DailyTrendDrillThroughType drillThroughType, DateTime drillThroughDate);

        /// <summary>
        /// Creates the daily trend drill through invoice view model.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="billToNum"></param>
        /// <param name="drillThroughDate"></param>
        /// <returns></returns>
        DailyTrendDrillThroughInvoiceViewModel DailyTrendDrillThroughInvoiceViewModel(int userId, string billToNum, DateTime drillThroughDate);

        /// <summary>
        /// Creates the daily trend invoice models.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="billToId"></param>
        /// <param name="drillThroughDate"></param>
        /// <returns></returns>
        PagedList<DailyTrendInvoiceItemModel> CreateDailyTrendInvoiceItemModels(int userId, IDataTablesRequest request, int billToId, DateTime drillThroughDate);
    }
}
