﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Announcements.Data.Domain;

namespace ProfitOptics.Modules.Announcements.Data.Config
{
    public class POAnnouncementConfig : IEntityTypeConfiguration<POAnnouncement>
    {
        public void Configure(EntityTypeBuilder<POAnnouncement> builder)
        {
            builder.ToTable("POAnnouncement");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.UserId);
            builder.Property(entity => entity.CreateTime).HasColumnType("datetime");
            builder.Property(entity => entity.Title).HasMaxLength(50);
            builder.Property(entity => entity.Text);
            builder.Property(entity => entity.ValidUntil).HasColumnType("date");
            builder.Property(entity => entity.IsDeleted);

            builder.HasOne(entity => entity.AspNetUser)
                .WithMany(p => p.POAnnouncements)
                .HasForeignKey(entity => entity.UserId);

        }

    }
}
