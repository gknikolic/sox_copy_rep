﻿using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Areas.Account.Services;
using ProfitOptics.Framework.Web.Factories;
using ProfitOptics.Framework.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.ViewComponents
{
    public class HeaderAccountMenuViewComponent : ViewComponent
    {
        public HeaderAccountMenuViewComponent(ILayoutModelFactory layoutModelFactory)
        {
            this._layoutModelFactory = layoutModelFactory;
        }

        public ILayoutModelFactory _layoutModelFactory { get; }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            HeaderAccountMenuModel model = await _layoutModelFactory.PrepareHeaderAccountMenuModelAsync();
            return View(model);
        }
    }
}
