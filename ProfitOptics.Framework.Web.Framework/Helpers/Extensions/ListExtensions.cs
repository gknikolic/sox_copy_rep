﻿using System.Collections.Generic;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;

namespace ProfitOptics.Framework.Web.Framework.Helpers.Extensions
{
    public static class ListExtensions
    {
        /// <summary>
        /// Converts the paged list to a data tables response.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pagedList"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static DataTablesResponse<T> ToDataTablesResponse<T>(this PagedList<T> pagedList,
            IDataTablesRequest request)
        {
            return new DataTablesResponse<T>(request.Draw, pagedList, pagedList.TotalCount, pagedList.TotalCount, pagedList.Total);
        }

        /// <summary>
        /// Converts the list to a data tables response.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static DataTablesResponse<T> ToDataTablesResponse<T>(this List<T> list,
            IDataTablesRequest request)
        {
            return new DataTablesResponse<T>(request.Draw, list, list.Count, list.Count);
        }
    }
}
