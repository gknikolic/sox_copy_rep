﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerTypeConfig : IEntityTypeConfiguration<CTContainerType>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTContainerType> builder)
        {
            builder.ToTable("CTContainerType");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.GCVendorId);
            builder.Property(e => e.LegacyId).IsRequired();
            builder.Property(e => e.ServiceId).IsRequired();
            builder.Property(e => e.ServiceName).IsRequired();
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.SterilizationMethodName).IsRequired();
            builder.Property(e => e.IsDiscarded).IsRequired();
            builder.Property(e => e.AutomaticActuals).IsRequired();
            builder.Property(e => e.CreatedAtUtc).IsRequired();

            //builder.HasOne(d => d.GCVendor)
            //    .WithMany(p => p.CTContainerTypes)
            //    .HasForeignKey(d => d.GCVendorId)
            //    .HasConstraintName("CTContainerType_GCVendor");
        }
    }
}
