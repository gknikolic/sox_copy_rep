﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.DataLayer
{
	public class GCProductTypeConfig : IEntityTypeConfiguration<GCProductType>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCProductType> builder)
        {
            builder.ToTable("GCProductType");
            builder.HasKey(entity => entity.Id);
        }
    }


}
