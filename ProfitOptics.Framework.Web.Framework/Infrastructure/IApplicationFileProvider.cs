﻿using System;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    public interface IApplicationFileProvider
    {
        string MapPath(string path);
        string Combine(params string[] paths);
        void CreateDirectory(string directoryPath);
        string[] GetFiles(string directoryPath, string searchPattern = "", bool topDirectoryOnly = true);
        string GetFileName(string file);
        void DeleteFile(string file);
        string[] GetDirectories(string path, string searchPattern = "", bool topDirectoryOnly = true);
        void DeleteDirectory(string path);
        string GetParentDirectory(string folder);
        string GetDirectoryNameOnly(string path);
        string GetDirectoryName(string path);
        string ReadAllText(string path, Encoding encoding);
        bool FileExists(string file);
        DateTime GetCreationTime(string path);
        void FileCopy(string sourceFileName, string destFileName, bool overwrite = false);
        void FileMove(string sourceFileName, string destFileName);
        bool DirectoryExists(string path);
        string GetFileNameWithoutExtension(string filePath);
    }
}
