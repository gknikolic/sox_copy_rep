﻿var enableDialog = true;

//btnShowAllManager.onclick(getAllManagers)

//function deleteEmployee(employeeId) {
//    //(title, message, callback, cancelCallback, yesButtonText, noButtonText)
//    if (Confirm("Are you shure?") == true) {
//        $.ajax({
//            type: "GET",
//            url: "Employee/OnDelete",
//            contentType: 'application/json',
//            dataType: "json",
//            data: {
//                id: employeeId
//            },
//            success: res => { app.showAlert(res.message); },
//            error: function (response) {
//                var error = 400;
//            }
//        });
//    }
//}

//function deleteEmployee(employeeId) {
//    console.log("Uso sam u deleteEmployee js")
//    console.log(app)
//    app.showConfirmModal(
//        "Delete manager",
//        "Are you sure?",
//        () => {
//            $.ajax({
//                type: "GET",
//                url: "Employee/OnDelete",
//                contentType: 'application/json',
//                dataType: "json",
//                data: {
//                    id: employeeId
//                },
//                success: function (res) {
//                    app.showAlert(res.message);
//                    app.showAlert()
//                    location.reload()
//                    //location.href = 
//                },
//                error: function (response) {
//                    var error = 400;
//                }
//            });
//        },
//        null,
//        "Delete",
//        "Cancel"
//    );
//}


function getAllManagers() {
    $.ajax({
        type: "GET",
        url: "Employee/ShowAllManagers",
        contentType: 'application/json',
        dataType: "json",
        success: res => {
            var divManager = document.getElementById("divManagers");
            //divManager.innerHTML = "Uso sam"
            //console.log("Uso sam!!")
            var text = ""
            res.forEach(manager => {
                text += "<div>" + manager.Id + " " + manager.FirstName + " " + manager.LastName + "<div>";
            })
            divManager.innerHTML = text;


        },
        error: function (response) {
            var error = 400;
        }
    });
}

function addManager() {

    var url = $("#addManagerUrl").val();
    console.log(url);
    var model = {
        FirstName: $("#txtFirstName").val(),
        LastName: $("#txtLastName").val(),
        Title: $("#txtTitle")[0].value
    }

    $.ajax({
        type: "POST",
        url: url,
        //contentType: 'application/json',
        dataType: "json",
        data: model,
        success: res => { app.showAlert(res.message); },
        error: function (response) {
            var error = 400;
        }
    });
}

//function addManager() {
//    console.log($("#txtFirstName")[0].value)
//    console.log(document.getElementById("txtFirstName").value)

//    var data = {
//        firstName: $("#txtFirstName").val(),
//        lastName: $("#txtLastName").val(),
//        title: $("#txtTitle")[0].value
//    }

//    $.ajax({
//        type: "POST",
//        url: "Employee/AddManager",
//        //contentType: 'application/json',
//        dataType: "json",
//        data: data,
//        success: res => { app.alert(res.message); },
//        error: function (response) {
//            var error = 400;
//        }
//    });
//}

//dialog
function updateEmployee(employeeId) {
    if (enableDialog == false) {
        return;
    }

    //var url = $("#UpdateEmployeeDialogUrl")

    $.ajax({
        type: "GET",
        url: $("#UpdateEmployeeDialogUrl").val(),
        data: { id: employeeId },
        contentType: 'application/json',
        dataType: "html",
        success: response => {
            $("#CreateOrUpdateEmployeeDialogHolder").html(response);
            $("#CreateOrUpdateEmployeeDialog").modal({ show: true });
            enableDialog = true;
        },
        error: function (response) {
            var error = 400;
            enableDialog = true;
        }
    });
}

//create employee - open dialog
function createEmployee() {
    if (enableDialog == false) {
        return;
    }

    $.ajax({
        type: "GET",
        url: $("#CreateEmployeeDialogUrl").val(),
        contentType: 'application/json', //ono sto saljem
        dataType: "html", //ono sto primam
        success: response => {
            $("#CreateOrUpdateEmployeeDialogHolder").html(response);
            $("#CreateOrUpdateEmployeeDialog").modal({ show: true });
            enableDialog = true;
        },
        error: function (response) {
            var error = 400;
            enableDialog = true;
        }
    });
}

//show delete confirmation dialog
function deleteemployee(employeeid) {
    if (enabledialog == false) {
        return;
    }

    console.log(employeeid)

    $.ajax({
        type: "get",
        url: $("#deleteemployeeconfirmationurl").val(),
        data: { id: employeeid },
        contenttype: 'application/json', //ono sto saljem
        datatype: "html", //ono sto primam
        success: response => {
            $("#deleteemployeeconfirmationdialogholder").html(response);
            $("#deleteemployeeconfirmationdialog").modal({ show: true }); //id modala
            enabledialog = true;
        },
        error: function (response) {
            var error = 400;
            enabledialog = true;
        }
    });
}