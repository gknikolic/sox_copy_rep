﻿namespace ProfitOptics.Framework.Etl.Models
{
    public static class SqlConstraintType
    {
        public const string NullConstraint = "There are some NULL values in this table.";
        public const string DuplicateKeyConstraint = "There are some duplicate keys in this table.";
        public const string SqlException = "SQL Exception. Please, reach out to developers to check this out.";
    }
}
