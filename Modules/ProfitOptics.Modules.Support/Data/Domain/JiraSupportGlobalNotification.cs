using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Support.Data.Domain
{
    [Table("JiraSupportGlobalNotification")]
    public class JiraSupportGlobalNotification
    {
        public int Id { get; set; }

        public string Email { get; set; }
    }
}