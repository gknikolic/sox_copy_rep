﻿using System;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVarianceShipToItemCategoryDrillThroughViewModel : TrendVarianceItemCategoryDrillThroughViewModel
    {
        private readonly IUrlHelper _urlHelper;

        public TrendVarianceShipToItemCategoryDrillThroughViewModel(int customerId,
            string customerName,
            string customerNum,
            int itemCategoryId,
            string itemCategoryName,
            ComparisonType comparisonType,
            DateTime? comparisonStartDate,
            DateTime? comparisonEndDate,
            bool isCorporate,
            IUrlHelper urlHelper)
            : base(customerId,
                customerName,
                customerNum,
                itemCategoryId,
                itemCategoryName,
                comparisonType,
                comparisonStartDate,
                comparisonEndDate,
                isCorporate)
        {
            _urlHelper = urlHelper;
        }

        public override bool IsBillTo => false;

        public override string DataUrl =>
            _urlHelper.Action("GetItemVariancesForItemCategoryAndShipTo", "TrendVariance", new { area = ReportingSettings.AreaName });

        public override string ChartDataUrl =>
            _urlHelper.Action("GetItemVariancesForChartForItemCategoryAndShipTo", "TrendVariance", new { area = ReportingSettings.AreaName });
    }
}