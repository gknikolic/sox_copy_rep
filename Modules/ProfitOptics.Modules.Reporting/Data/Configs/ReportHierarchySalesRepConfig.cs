﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportHierarchySalesRepConfig : IEntityTypeConfiguration<ReportHierarchySalesRep>
    {
        public void Configure(EntityTypeBuilder<ReportHierarchySalesRep> builder)
        {
            builder.ToTable(nameof(ReportHierarchySalesRep));
            builder.HasKey(s => s.Id);

            builder.Property(s => s.Name).HasMaxLength(50).IsRequired();
            builder.Property(s => s.StartDate).IsRequired();

            builder.HasMany(s => s.ReportBillTos)
                .WithOne(b => b.ReportHierarchySalesRep)
                .HasForeignKey(b => b.SalesRepId)
                .IsRequired(false);

            builder.HasMany(s => s.ReportSalesHistories)
                .WithOne(b => b.ReportHierarchySalesRep)
                .HasForeignKey(b => b.SalesRepId)
                .IsRequired(false);

            builder.HasMany(s => s.ReportShipTos)
                .WithOne(b => b.ReportHierarchySalesRep)
                .HasForeignKey(b => b.SalesRepId)
                .IsRequired(false);

            builder.HasOne(s => s.ReportHierarchyRegion)
                .WithMany(r => r.ReportHierarchySalesReps)
                .HasForeignKey(s => s.RegionId)
                .IsRequired(false);

            builder.HasMany(v => v.ReportQuotas)
                .WithOne(s => s.ReportHierarchySalesRep)
                .HasForeignKey(s => s.SalesRepId);
        }
    }
}
