﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.WorkWave.Enums
{
	public enum OrderCustomFiledType
	{
		[Display(Name = "Pick-up Surgery")]
		PickUpSurgery,
		[Display(Name = "Pick-up Surgeon")]
		PickUpSurgeon,
		[Display(Name = "Delivery Surgery")]
		DeliverySurgery,
		[Display(Name = "Delivery Surgeon")]
		DeliverySurgeon,
		[Display(Name = "Pickup Tray #")]
		PickupTray,
		[Display(Name = "Deliver Tray #")]
		DeliverTray,
		[Display(Name = "Deliver Notes")]
		DeliverNotes,
		[Display(Name = "Deliver Access Code")]
		DeliverAccessCode,
		[Display(Name = "Deliver Phone Number")]
		DeliverPhoneNumber,
		[Display(Name = "Deliver Name")]
		DeliverName,
		[Display(Name = "Outgoing Order")]
		OutgoingOrder
	}
}
