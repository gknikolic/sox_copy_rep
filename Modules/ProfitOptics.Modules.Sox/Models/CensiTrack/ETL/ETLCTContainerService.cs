﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTContainerService
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
