﻿(function($) {
    var paginatePrevious = '<li class="paginate_button previous disabled" id="tbl_previous"><a href= "#" aria-controls="tbl" data-dt-idx="0" tabindex="0">Previous</a></li>';
    var paginateButton = '<li class="paginate_button number IsActive"><a href="#" aria-controls="tbl" data-dt-idx="X" tabindex="0">X</a></li>';
    var paginateNext = '<li class="paginate_button next disabled" id="tbl_next"><a href="#" aria-controls="tbl" data-dt-idx="3" tabindex= "0">Next</a></li>';
    var resizetimeout;

    $.fn.profitopticsTable = function (options) {

        // This is the easiest way to have default options.
        var settings = $.extend({
            curSort1: null,
            curSort1Order: null,
            curSort2: null,
            curSort2Order: null,
            pageIndex: 0,
            length: 10,
            useScroll: true,
            maxHeight: 550, // only needed if useScroll = true
            url: "", // ajax path
            totals: null, // 'header' or 'footer'
            ajaxForLevels: false,
            data: new Array()
        }, options);

        setClasses(this, settings);
        setSortIcons();
        getTable(this, settings, true);

        if (settings.useScroll === true) {
            $(window).resize(function() {
                console.log("resized");
                if (resizetimeout) {
                    clearTimeout(resizetimeout);
                }

                resizetimeout = setTimeout(function() {
                    TableSizeChanged($("table.multiRowTable"));
                }, 150);
            });
        }
        else {
            MakeSorting(this, settings);
        }
    };

    function setLevelIcons(table, settings) {
        var spans = $(table).children('tbody').children('tr').children('td').children('span.sign');
        $(spans).unbind("click");
        $(spans).on("click", function() {
            expandOrCloseBySpan(settings, this);
        });
    }

    function expandOrCloseBySpan(settings, span) {
        if ($(span).parent().parent().hasClass("closed")) {
            // EXPAND
            expand(settings, span);
        }
        else {
            // COLAPSE
            colapse(settings, span);
        }
    }

    function expand(settings, span) {
        $(span).parent().parent().removeClass("closed");
        $(span).parent().parent().addClass("open");
        $("tr[master='" + $(span).parent().parent().attr("id") + "']").show();
        $(span).html("-");
    }
    function colapse(settings, span) {
        var tr = $(span).parent().parent();
        $(tr).removeClass("open");
        $(tr).addClass("closed");
        CloseByParent(tr);
        $(span).html("+");
    }

    function CloseByParent(tr) {
        var childrenBySpan = $("tr[master='" + $(tr).attr("id") + "']");

        $.each(childrenBySpan, function (i, trChild) {
            $(trChild).removeClass("open").removeClass("closed");
            $(trChild).addClass("closed");
            $(trChild).children("td").children("span.sign").html("+");

            var childrens = $("tr[master='" + $(trChild).attr("id") + "']");
            $(childrens).hide();

            CloseByParent(trChild);
        });

        $(childrenBySpan).hide();
    }

    function setClasses(table, settings) {
        $(table).removeClass("dataTable");
        $(table).removeClass("table");
        $(table).removeClass("table-bordered");        
        $(table).removeClass("table-hover");
        $(table).removeClass("font-condensed");
        $(table).removeClass("fixedHeader");

        $(table).addClass("dataTable");
        $(table).addClass("table");
        $(table).addClass("table-bordered");
        $(table).addClass("table-hover");
        $(table).addClass("font-condensed");
        if (settings.useScroll === true) {
            $(table).addClass("fixedHeader");
        }
    }
    
    function TableSizeChanged(table) {
        console.log("refreshing header");
        RefreshColumnsWidth(table);
    }

    function getTable(table, settings) {

        settings.data = settings.data.filter(function(obj) {
            return obj.name !== 'pageIndex' &&
                obj.name !== "curSort1" &&
                obj.name !== "curSort1Order" &&
                obj.name !== "curSort2" &&
                obj.name !== "curSort2Order" &&
                obj.name !== "length";
        });

        addOrUpdateArray(settings.data, "level", null);
        addOrUpdateArray(settings.data, "masterId", null);

        settings.data.push({ "name": "curSort1", "value": settings.curSort1 });
        settings.data.push({ "name": "curSort1Order", "value": settings.curSort1Order });
        settings.data.push({ "name": "curSort2", "value": settings.curSort2 });
        settings.data.push({ "name": "curSort2Order", "value": settings.curSort2Order });
        settings.data.push({ "name": "pageIndex", "value": settings.pageIndex });
        settings.data.push({ "name": "length", "value": settings.length });

        //app.blockUI("One moment please..");        
        $.ajax({
            url: settings.url,
            type: "post",
            dataType: "json",
            data: settings.data,
            success: function (result) {

                if (settings.totals === "header") {
                    $(table).children("thead").children("tr.totals").remove();
                    $(table).children("thead").children("tr.totals").prepend(result.Totals);
                }
                else if (settings.totals === "footer") {
                    $(table).children("tfoot").children("tr.totals").remove();
                    $(table).children("tfoot").children("tr.totals").append(result.Totals);
                }

                $(table).children("tbody").html(result.Html);

                if (settings.useScroll === true && $('#' + getTableId(table) + 'Cloned').length === 0) {
                    scrolify($('#' + getTableId(table)), table, settings);
                }
                else if (settings.useScroll === true) {
                    RefreshColumnsWidth(table);
                }

                makeSpans(table, settings);
                setTimeout(function() { setLevelIcons(table, settings); }, 100);
                makePaging(table, settings, result.PageCount);
                //app.unblockUI();
            },
            error: function(result) {
                //app.unblockUI();
            }
        });
    }

    function makeSpans(table, settings) {
        var masterTRs = $(table).children("tbody").children("tr.topLevel[master='']");

        if (settings.ajaxForLevels === true) {
            putSpanAjax(table, settings, masterTRs, 0);
        }
        else {
            putSpan(table, settings, masterTRs, 0);
        }
    }

    function putSpanAjax(table, settings, masterTRs, level) {
        $.each(masterTRs, function (i, tr) {
            if ($(tr).hasClass("topLevel")) {
                $(tr).addClass("closed");
                $(tr).children("td:first").prepend("<span class='level" + level + "'></span><span class='sign'>+</span>");
            }
        });

        setTimeout(function () {
            $(masterTRs).find(".sign").unbind("click");
            $(masterTRs).find('.sign').on('click', function () {
                if ($(this).parent().parent().hasClass("closed")) {
                    getChildren(table, settings, this, $(this).parent().parent(), level);
                }
                else {
                    colapse(settings, this);
                }
            });
        }, 1000);
    }

    function findIndexByProperty(data, key, value) {
        for (var i = 0; i < data.length; i++) {
            if (data[i][key] === value) {
                return i;
            }
        }
        return -1;
    }

    function addOrUpdateArray(array, key, value) {
        var index = findIndexByProperty(array, 'name', key);
        if (index > -1) {
            array[index].value = value;
        }
        else {
            array.push({ "name": key, "value": value });
        }
    }

    function getChildren(table, settings, span, trMaster, level) {
        var masterId = $(trMaster).attr("id");

        addOrUpdateArray(settings.data, "level", level);
        addOrUpdateArray(settings.data, "masterId", masterId);

        $.ajax({
            url: settings.url,
            type: "post",
            dataType: "json",
            data: settings.data,
            success: function (result) {

                expandOrCloseBySpan(settings, span);
                $(trMaster).after(result.Html);

                if (settings.useScroll === true) {
                    RefreshColumnsWidth(table);
                }

                var masterTrs = $(table).children("tbody").children("tr.topLevel[master='" + masterId + "']");
                putSpanAjax(table, settings, masterTrs, level + 1);
                //app.unblockUI();
            },
            error: function (result) {
                //app.unblockUI();
            }
        });
    }

    function putSpan(table, settings, masterTRs, level) {
        level += 1;
        $.each(masterTRs, function (i, tr) {

            $(tr).addClass("closed");
            $(tr).children("td:first").prepend("<span class='level" + level + "'></span><span class='sign'>+</span>");
            
            if ($(tr).not["[master]"] || level > 1) {
                $(tr).hide();
            }

            var trChildren = $("tr.topLevel[master='" + $(tr).attr("id") + "']");
            putSpan(table, settings, trChildren, level);

            var trChildrenWithoutChildren = $("tr[master='" + $(tr).attr("id") + "']:not('.topLevel')");
            putLevelOnly(table, settings, trChildrenWithoutChildren, level);
        });
    }

    function putLevelOnly(table, settings, trs, level) {
        level += 1;
        $.each(trs, function (i, tr) {
            $(tr).children("td:first").prepend("<span class='level" + level + "'></span><span>&nbsp;&nbsp;&nbsp;</span>");
            $(tr).hide();
        });
    }

    function RefreshColumnsWidth(oTbl) {
        // var oTbl = $("table.fixedHeader");
        var tableId = oTbl.attr("id");
        var newTbl = $("#" + tableId + "Cloned");

        var trId = oTbl.find('thead tr').length - 2;

        newTbl.width(oTbl.width());
        var columnCount = oTbl.find('thead tr:eq(' + trId + ') th').length;

        $(oTbl.find('thead tr:eq(' + trId + ') th').get().reverse()).each(function (i) {
            var x = columnCount - i - 1;

            var tdWidth = $(this).width();

            var th = newTbl.find("thead tr:eq(" + trId + ") th:eq(" + x + ")");
            var thWidth = th.width();

            th.width(tdWidth);
        });

        var h = oTbl.find('thead').height() + 2;
        oTbl.css("cssText", "margin-top: -" + h + "px !important;");
    }

    function scrolify(tblAsJQueryObject, table, settings) {

        var oTbl = tblAsJQueryObject;
        var tableId = oTbl.attr("id");        

        var oTblDiv = $("<div/>");
        oTblDiv.css('height', settings.maxHeight);
        oTblDiv.css('overflow-y', 'auto');
        oTbl.wrap(oTblDiv);

        // clone the original table
        var newTbl = oTbl.clone();

        newTbl.attr("id", tableId + "Cloned");
        newTbl.css("z-index", 1);
        newTbl.removeClass("fixedHeader");
        newTbl.addClass("clonedTable");

        // remove table body from new table
        newTbl.find('tbody tr').remove();

        oTbl.parent().parent().prepend(newTbl);
        newTbl.wrap("<div/>");

        MakeSorting(table, settings);

        setTimeout(function () {
            RefreshColumnsWidth(table);
        }, 250);
    }

    function MakeSorting(table, settings) {
        var sortingTable = settings.useScroll === true ? getClonedId(table) : getTableId(table);
        var ths = $('#' + sortingTable + ' tr th.sortable');
        $(ths).on("click", function () {
            var id = $(this).attr("id");

            if (event.shiftKey) {
                if (settings.curSort2 === id) {
                    if (settings.curSort2Order === "asc") {
                        settings.curSort2Order = "desc";
                    }
                    else {
                        settings.curSort2Order = "asc";
                    }
                }
                else {
                    settings.curSort2 = id;
                    settings.curSort2Order = "asc";
                }
            }
            else {
                settings.curSort2 = null;
                settings.curSort2Order = null;

                if (settings.curSort1 === id) {
                    if (settings.curSort1Order === "asc") {
                        settings.curSort1Order = "desc";
                    }
                    else {
                        settings.curSort1Order = "asc";
                    }
                }
                else {
                    settings.curSort1 = id;
                    settings.curSort1Order = "asc";
                }
            }

            setSortIcons(table, settings);
            getTable(table, settings);
        });
        setSortIcons(table, settings);
    }

    function getTableId(table) {
        var oTbl = $(table);
        return oTbl.attr("id");
    }

    function getClonedId(table) {
        var id = getTableId(table);
        if ($("#" + id + "Cloned").length > 0) {
            return id + "Cloned";
        }
        else {
            return id;
        }
    }

    var setSortIcons = function(table, settings) {
        var clonedId = getClonedId(table);
        var ths = $('#' + clonedId + ' tr th.sortable');
        if (ths.length === 0) {
            ths = $('#' + getTableId(table) + ' tr th.sortable');
        }

        $.each(ths, function(i, item) {
            $(this).removeClass('sorting_asc');
            $(this).removeClass('sorting_desc');
            $(this).removeClass('sorting');
            $(this).addClass('sorting');

            var id = $(this).attr("id");

            if (id !== null && id !== "" && id === settings.curSort1) {
                $(this).removeClass('sorting');
                $(this).addClass("sorting_" + settings.curSort1Order);
            }
            if (id !== null && id !== "" && id === settings.curSort2) {
                $(this).removeClass('sorting');
                $(this).addClass("sorting_" + settings.curSort2Order);
            }
        });
    };    

    function makePaging(table, settings, count) {
        var i;
        if (count > 10) {
            count = 10;
        }

        var text = "";

        text += (settings.pageIndex === 0 ? paginatePrevious : paginatePrevious.replace("disabled", ""));

        var ipageIndex = 1;
        var iEnd = 9;

        if (settings.pageIndex - 4 > 0) {
            ipageIndex = settings.pageIndex - 3;
            iEnd = settings.pageIndex + 5;
        }

        if (count + 1 < iEnd) {
            iEnd = count + 1;
        }

        for (i = ipageIndex; i <= iEnd; i++) {
            var button = paginateButton.replace(/X/g, i);
            if (i === settings.pageIndex + 1) {
                button = button.replace("IsActive", "active");
            }
            else {
                button = button.replace("IsActive", "");
            }
            text += button;
        }

        text += (settings.pageIndex + 1 >= iEnd ? paginateNext : paginateNext.replace("disabled", ""));

        var paginationHolder = $(table).parent();
        if (settings.useScroll === true) {
            paginationHolder = $(table).parent().parent();
        }

        $(paginationHolder).find(".pagination").html(text);

        $(paginationHolder).find(".paginate_button.number").on("click", function () {
            var x = $(this).children("a").attr("data-dt-idx");
            settings.pageIndex = x - 1;
            getTable(table, settings);
        });

        $(paginationHolder).find(".paginate_button.previous").not(".disabled").on("click", function () {
            settings.pageIndex -= 1;
            getTable(table, settings);
        });
        $(paginationHolder).find(".paginate_button.next").not(".disabled").on("click", function () {
            settings.pageIndex += 1;
            getTable(table, settings);
        });
    }

}(jQuery));
 