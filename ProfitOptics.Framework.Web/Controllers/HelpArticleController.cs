﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Models.HelpArticle;
using Rotativa.NetCore;

namespace ProfitOptics.Framework.Web.Controllers
{
    [Authorize]
    public class HelpArticleController : Controller
    {
        private readonly IHelpArticleService _helpArticleService;

        public HelpArticleController(IHelpArticleService helpArticleService)
        {
            _helpArticleService = helpArticleService;
        }

        public IActionResult Index()
        {
            var model = _helpArticleService.GetHelpArticlesGroupedByTag();
            return View(model);
        }

        [HttpPost]
        public JsonResult GetHelpArticles(string currentUrl = "", string searchValue = "", int skip = 0, int take = 5)
        {
            var filter = new HelpArticleFilterModel(currentUrl, searchValue, skip, take);

            var result = _helpArticleService.GetHelpArticleListModel(filter);

            return Json(result);
        }

        [HttpGet]
        public JsonResult GetHelpArticleNames()
        {
            var result = _helpArticleService.GetHelpArticleNames();
            return Json(result);
        }

        [HttpGet]
        public JsonResult GetHelpArticleTags()
        {
            var result = _helpArticleService.GetHelpArticleTags();
            return Json(result);
        }

        [HttpPost]
        // Validate input has to be false because we send HTML content through the model.
        public JsonResult AddHelpArticle(HelpArticleModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json("Error");
            }
            _helpArticleService.AddHelpArticle(model);
            return Json("Ok");
        }

        [HttpPost]
        // Validate input has to be false because we send HTML content through the model.
        public JsonResult EditHelpArticle(HelpArticleModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json("Error");
            }
            _helpArticleService.EditHelpArticle(model);
            return Json("Ok");
        }

        [HttpPost]
        public JsonResult DeleteHelpArticle(int id)
        {
            _helpArticleService.DeleteHelpArticle(id);
            return Json("Ok");
        }

        public ActionResult Test()
        {
            return new ActionAsPdf("Index") { FileName = "Test.pdf", CustomSwitches = "--load-error-handling ignore" };
        }

        public ActionResult Test2()
        {
            return new ActionAsImage("Index") { FileName = "Test.png", CustomSwitches = "--load-error-handling ignore" };
        }
    }
}
