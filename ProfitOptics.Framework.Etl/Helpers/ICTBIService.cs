﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public interface ICTBIService
    {
        void AddOrEditCTBILoadResults(List<CTBILoadResult> ctBILoadResults, int serverOffsetInMinsFromUTC);
        bool AddOrEditCTBILoadResult(CTBILoadResult ctBILoadResult, int serverOffsetInMinsFromUTC);
        void AddOrEditCTBILoadResultContents(List<CTBILoadResultContent> contents, long loadId, int serverOffsetInMinsFromUTC);
        void AddOrEditCTBILoadResultIndicators(List<CTBILoadResultIndicator> indicators, long loadId, int serverOffsetInMinsFromUTC);
        void AddOrEditCTBILoadResultGraphics(List<CTBILoadResultGraphic> graphics, long loadId, int serverOffsetInMinsFromUTC);
        //List<CTBILoadResultGraphic> GetCTLoadResultGraphics(bool? isLoadMeadiaWithoutImage);
        void UpdateCTLoadResultGraphic(int id, byte[] ctBILoadResultImage);
        void UpdateCTLoadResultImportStatus(long loadId, bool isImportSuccessfull);
    }
}
