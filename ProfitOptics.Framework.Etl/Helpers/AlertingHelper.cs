﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Etl.Models;
using Serilog;

namespace ProfitOptics.Framework.Etl.Helpers
{
    /// <summary>
    /// Provides methods for querying the database with custom SQL checks and generating email reports.
    /// </summary>
    public class AlertingHelper
    {
        private readonly Settings _settings;
        private readonly string _alertingReportUrl;
        private readonly Entities _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="AlertingHelper"/> class.
        /// </summary>
        /// <param name="alertingUrl"></param>
        /// <param name="loggingService"></param>
        /// <param name="settings"></param>
        /// <param name="context"></param>
        public AlertingHelper(string alertingUrl, Settings settings, Entities context)
        {
            if (string.IsNullOrWhiteSpace(alertingUrl))
            {
                throw new ArgumentException("Value for URL can not be null or empty", nameof(alertingUrl));
            }

            _alertingReportUrl = alertingUrl;

            _settings = settings;
            _context = context;


            var listener = context.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }
        
        /// <summary>
        /// Execeutes the list of specified <see cref="CustomSqlCheckModel"/> and generates an email report.
        /// </summary>
        /// <param name="customChecks"></param>
        /// <returns></returns>
        public bool RunChecksAndSendReport(List<CustomSqlCheckModel> customChecks)
        {
            var model = GetDbCheckModel(customChecks);
            var json = JsonConvert.SerializeObject(model);
            var result = SendReport(_alertingReportUrl, json);

            return result;
        }

        /// <summary>
        /// Creates a new <see cref="DbStatusInfoModel"/> filled with status information.
        /// </summary>
        /// <param name="customChecks"></param>
        /// <returns></returns>
        private DbStatusInfoModel GetDbCheckModel(List<CustomSqlCheckModel> customChecks)
        {
            var etlStatus = CheckEtlLogTableStatus();
            var dbTableStatus = ExecuteCustomSqlChecks(customChecks);

            var model = new DbStatusInfoModel
            {
                EtlStatuses = etlStatus.ToList(),
                DbTableStatuses = dbTableStatus.ToList()
            };

            return model;
        }

        /// <summary>
        /// Sends the specified JSON data to the specified URL, where an email report should be generated.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public bool SendReport(string url, string json)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Timeout = 600000;

            var encoding = new System.Text.UTF8Encoding();
            var byteArray = encoding.GetBytes(json);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (var dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var reader = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException("The response stream is null."), System.Text.Encoding.GetEncoding("utf-8"));
                var responseString = reader.ReadToEnd();

                bool result;
                if (bool.TryParse(responseString, out result))
                {
                    return result;
                }
            }

            return false;
        }

        /// <summary>
        /// Generates the key mismatch query string with the specified parameters.
        /// </summary>
        /// <param name="table1"></param>
        /// <param name="key1"></param>
        /// <param name="table2"></param>
        /// <param name="key2"></param>
        /// <returns></returns>
        public string GetKeyMismatchCheckQuery(string table1, string key1, string table2, string key2)
        {
            var commandText = $@"SELECT COUNT(*)
                                FROM {table1} AS A
                                FULL OUTER JOIN {table2} AS B ON A.{key1} = B.{key2}
                                WHERE A.{key1} IS NULL OR B.{key2} IS NULL";

            return commandText;
        }

        /// <summary>
        /// Builds a query that returns the number of records from table table1 that don't exist in table table2 (join established through key1 and key2 columns)
        /// </summary>
        /// <param name="table1"></param>
        /// <param name="key1"></param>
        /// <param name="table2"></param>
        /// <param name="key2"></param>
        /// <returns></returns>
        public string GetKeyDoesntExistQuery(string table1, string key1, string table2, string key2)
        {
            var commandText = $@"SELECT DISTINCT A.{key1}
                                FROM {table1} AS A
                                LEFT JOIN {table2} AS B ON A.{key1} = B.{key2}
                                WHERE B.{key2} IS NULL";

            return commandText;
        }

        /// <summary>
        /// Generates the duplicate key query string with the specified parameters.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="keyColumns"></param>
        /// <returns></returns>
        public string GetDuplicateKeyCheckQuery(string table, string keyColumns)
        {
            var cmdText = $@"SELECT COUNT(cnt) FROM (
                            SELECT COUNT(*) as cnt
                            FROM {table} tbl
                            GROUP BY {keyColumns}
                            HAVING COUNT(*) > 1) cnt";

            return cmdText;
        }

        /// <summary>
        /// Generates the null check query string with the specified parameters.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="ignoreColumns"></param>
        /// <param name="onlyTheseColumns"></param>
        /// <returns></returns>
        public string GetNullCheckQuery(string tableName, List<string> ignoreColumns = null, List<string> onlyTheseColumns = null)
        {
            const int startIndexInOrderToSkipSchema = 4;
            var columnNames = GetTableColumnNames(tableName.Substring(startIndexInOrderToSkipSchema)).ToList();

            if (!columnNames.Any())
            {
                return string.Empty;
            }

            if (ignoreColumns != null && ignoreColumns.Count > 0)
                columnNames = columnNames.Where(p => !ignoreColumns.Contains(p)).ToList();
            if (onlyTheseColumns != null && onlyTheseColumns.Count > 0)
                columnNames = columnNames.Where(onlyTheseColumns.Contains).ToList();

            var commandText = $@"SELECT COUNT(*)
                                FROM {tableName}
                                WHERE {string.Join(" or ", columnNames.Select(x => $"{x} is null"))}";

            return commandText;
        }

        /// <summary>
        /// Gets a list of column names for the specified table name.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private IEnumerable<string> GetTableColumnNames(string tableName)
        {
            var result = new List<string>();
            using (var connection = new SqlConnection(_settings.DefaultConnection))
            {
                connection.Open();

                var commandText = $@"SELECT COLUMN_NAME
                FROM {connection.Database}.INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_NAME = N'{tableName}'";

                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = 0;
                    command.CommandText = commandText;

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            return result.ToArray();
                        }

                        while (reader.Read())
                        {
                            result.Add(reader["COLUMN_NAME"].ToString());
                        }
                    }
                }

                connection.Close();
            }

            return result;
        }

        #region Custom Check

        /// <summary>
        /// Executes custom SQL queries from the specified list of <see cref="CustomSqlCheckModel"/> objects.
        /// </summary>
        /// <param name="checksToExecute"></param>
        /// <returns>A list of <see cref="DbTableStatus"/> objects</returns>
        public IEnumerable<DbTableStatus> ExecuteCustomSqlChecks(List<CustomSqlCheckModel> checksToExecute)
        {
            var result = new List<DbTableStatus>();
            foreach (var model in checksToExecute)
            {
                try
                {
                    int count = 0;
                    List<string> badValues = new List<string>();
                    switch (model.Type)
                    {
                        case CustomCheckModelReturnType.ListOfBadValues:
                            count = ExecuteCustomScalarSqlQuery(model.Sql);
                            break;
                        case CustomCheckModelReturnType.NumberOfBadRows:
                            badValues = ExecuteCustomSqlQuery(model.Sql);
                            count = badValues.Count;
                            break;
                    }

                    if (count > 0)
                    {
                        result.Add(new DbTableStatus
                        {
                            TableName = model.TableName,
                            ConstraintBroken = model.Constraint + (badValues.Any() ? $" {string.Join(", ", badValues)}" : string.Empty),
                            RecordsThatBrokeConstraintCount = count
                        });
                    }
                }
                catch (Exception e)
                {
                    result.Add(GetErrorDbTableStatus(model.TableName));
                    Log.Error($"Error executing query {model.Sql}", e);
                    throw;
                }
            }

            return result.OrderByDescending(p => p.RecordsThatBrokeConstraintCount).ToList();
        }

        /// <summary>
        /// Executes the specified SQL query and returns a list of results.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        private List<string> ExecuteCustomSqlQuery(string sql)
        {
            var result = new List<string>();
            
            if (string.IsNullOrWhiteSpace(sql)) return result;

            try
            {
                using (var connection = new SqlConnection(_settings.DefaultConnection))
                {
                    connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandTimeout = 120;
                        command.CommandText = sql;

                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result.Add(reader[0].ToString());
                            }
                        }
                    }

                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Log.Error("Error while executing custom query", e);
                throw;
            }

            return result;
        }

        /// <summary>
        /// Executes the specified SQL query and returns a scalar result.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        private int ExecuteCustomScalarSqlQuery(string sql)
        {
            if (string.IsNullOrWhiteSpace(sql)) return 0;

            int result;
            try
            {
                using (var connection = new SqlConnection(_settings.DefaultConnection))
                {
                    connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandTimeout = 120;
                        command.CommandText = sql;

                        result = Convert.ToInt32(command.ExecuteScalar());
                    }

                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Log.Error("Error while executing custom query", e);
                throw;
            }

            return result;
        }

        /// <summary>
        /// Creates a new error <see cref="DbTableStatus"/> with the specified table name. 
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        private DbTableStatus GetErrorDbTableStatus(string table)
        {
            var result = new DbTableStatus
            {
                TableName = table,
                ConstraintBroken = SqlConstraintType.SqlException,
                RecordsThatBrokeConstraintCount = 0
            };

            return result;
        }

        #endregion

        #region ETL Log Check

        /// <summary>
        /// Checks the ETL Log table status in the previous 24 hour window.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<EtlLogStatus> CheckEtlLogTableStatus()
        {
            var timeWindow = DateTime.UtcNow.AddDays(-1);
            var result = (from log in _context.ETLLogs
                where log.ProcessingTime >= timeWindow
                group log by log.Source
                into g
                select g.OrderByDescending(x => x.ProcessingTime).FirstOrDefault() into latest
                select new EtlLogStatus
                {
                    Status = latest.Status,
                    ProcessingTime = latest.ProcessingTime,
                    TableName = latest.Source
                }).ToList();

            return result;
        }

        #endregion
    }
}
