﻿using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Modules.Employee.Factories;
using ProfitOptics.Modules.Employee.Models;
using ProfitOptics.Modules.Employee.Services;
using System.Threading.Tasks;
using System.Linq;

namespace ProfitOptics.Modules.Employee.Controllers
{
    [Area("Employee")]
    public class EmployeeController : BaseController
    {
        private readonly IEmployeeFactory _EmployeeFactory;
        private readonly IEmployeeService _EmployeeService;
        //private static int lastEmployeeId;

        public EmployeeController(IEmployeeFactory EmployeeFactory, IEmployeeService EmployeeService)
        {
            _EmployeeFactory = EmployeeFactory;
            _EmployeeService = EmployeeService;
        }

        //dodaj get atribute
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var model = await _EmployeeFactory.PrepareAllEmployeesModelAsync();
            //lastEmployeeId = model.Employees.Last().Id++; //Needs OnCreate method for tables TEmployeeSkills relationship (reseno sa vise SaveChanges() )
            return View(model); 
        }

        //[HttpGet]
        //public async Task<IActionResult> CreateEmployee()
        //{
        //    //var model = await _EmployeeFactory.PrepareEmptyModelAsync(lastEmployeeId + 1);
        //    var model = await _EmployeeFactory.PrepareEmptyModelAsync();
        //    return View("CreateOrUpdate", model);
        //}

        [HttpGet]
        public async Task<IActionResult> CreateEmployeeDialog()
        {
            //var model = await _EmployeeFactory.PrepareEmptyModelAsync(lastEmployeeId + 1);
            var model = await _EmployeeFactory.PrepareEmptyModelAsync();
            return PartialView("_CreateOrUpdateEmployeeModal", model);
        }

        //EditEmployee/id=1
        //[HttpGet]
        //public async Task<IActionResult> EditEmployee(int id)
        //{
        //    var model = await _EmployeeFactory.PrepareEmployeeModelAsync(id);
        //    return View("CreateOrUpdate", model);
        //}

        [HttpGet]
        public async Task<IActionResult> EditEmployeeDialog(int id)
        {
            var model = await _EmployeeFactory.PrepareEmployeeModelAsync(id);
            return PartialView("_CreateOrUpdateEmployeeModal", model);
        }

        [HttpGet]
        public async Task<IActionResult> DeleteEmployeeDialog(int id)
        {
            var model = await _EmployeeFactory.PrepareEmployeeModelAsync(id);
            return PartialView("_DeleteEmployeeConfirmationModal", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> OnUpdate(EmployeeModel model)
        {
            if (ModelState.IsValid)
            {
                await _EmployeeService.UpdateEmployeeAsync(model);
            }
            //model = await _EmployeeFactory.PrepareEmployeeModelAsync(model.Id, true);
            //return View("CreateOrUpdate", model);
            //return RedirectToAction("Index", "EmployeeController");
            return RefreshIndexPage();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> OnCreate(EmployeeModel model)
        {
            if (ModelState.IsValid)
            {
                await _EmployeeService.AddEmployeeAsync(model);
            }
            return RefreshIndexPage();
        }
        //TODO: probaj da vratis AJAX umesto id da nebi opet vadio iz baze employee-a
        [HttpGet]
        public async Task<IActionResult> OnDelete(EmployeeModel model)
        {
            await _EmployeeService.DeleteEmployeeAsync(model);
            //model = await _EmployeeFactory.PrepareEmployeeModelAsync(model.Id, true);
            //return View("CreateOrUpdate", model);
            // return RedirectToAction("Index", "EmployeeController");
            return RefreshIndexPage();
        }

        private IActionResult RefreshIndexPage()
        {
            //var temp = await _EmployeeFactory.PrepareAllEmployeesModelAsync();
            //return View("Index", temp);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> ShowAllManagers()
        {
            var res = await _EmployeeService.GetAllManagersAsync();
            return Json(res);
        }

        [HttpPost]
        public async Task<IActionResult> AddManager(ManagerModel model)
        {
            if (await _EmployeeService.AddManagerAsync(model) == true)
                return Json(new { message = "Manager added sucessfuly!" });
            else
                return Json(new { message = "Something went wrong!" });
        }

        //[HttpPost]
        //public async Task<IActionResult> AddManager(string firstName, string lastName, string title)
        //{
        //    if (await _EmployeeService.AddManagerAsync(firstName, lastName, title) == true)
        //        return Json(new { message = "Manager added sucessfuly!" });
        //    else
        //        return Json(new { message = "Something went wrong!" });
        //}
    }


}