﻿namespace ProfitOptics.Framework.Web.Framework.Models.UserAssignments
{
    public class RoleHierarchyModel
    {
        public string ZoneName { get; set; }
        public string RegionName { get; set; }
        public string SalesRepName { get; set; }
    }
}
