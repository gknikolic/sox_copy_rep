﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer
{
	[Table("GCCustomer")]
    public partial class GCCustomer
    {
        public int Id { get; set; }
        public string FacilityId { get; set; }
        public string FacilityCode { get; set; }
		public string DeliveryState { get; set; }
        public string DeliveryStreet { get; set; }
        public string DeliveryCity { get; set; }
        public string DeliveryZip { get; set; }
        public int ServiceTime { get; set; }
        public string DeliveryStart { get; set; }
        public string DeliveryEnd { get; set; }
        public string DeliverNotes { get; set; }
        public string DeliverAccessCode { get; set; }
        public string DeliverPhoneNumber { get; set; }
        public string DeliverName { get; set; }
        public string CustomerName { get; set; }
        public int? QBCustomerId { get; set; }
        public int? WWDepoId { get; set; }
        public bool Active { get; set; }
        public string PickupInstructions { get; set; }
        public string DropoffInstructions { get; set; }
        public string PickupName { get; set; }
        public int? QBBillingType { get; set; }
        public string APEmail { get; set; }
        public string APPhone { get; set; }
        public string APContactName { get; set; }
    }
}
