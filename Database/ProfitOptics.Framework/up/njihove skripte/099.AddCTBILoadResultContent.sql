USE [Sox]

CREATE TABLE [dbo].[CTBILoadResultContent]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[CTBILoadResultId] INT NOT NULL,
	[CTContainerTypeActualId] INT NOT NULL,
	[LoadId] BIGINT NOT NULL,
	[Quantity] INT NOT NULL,
	[Description] NVARCHAR(200) NULL,
	[Container] NVARCHAR(200) NULL,
	[UpdatedBy] NVARCHAR(100) NULL,
	[LastUpdate] DATETIME NOT NULL,
	[LastContainer] NVARCHAR(200) NULL,
	[LastLocation] NVARCHAR(200) NULL,
	[Sort] NVARCHAR(10) NULL,
	[BiotestFlag] NVARCHAR(10) NULL,
	[AssetId] BIGINT NOT NULL,
	[AssetType] NVARCHAR(10) NULL,
	[CaseReference] NVARCHAR(100) NULL,
	[CreatedAtUtc] DATETIME NOT NULL,
	[ModifiedAtUtc] DATETIME NULL
CONSTRAINT [PK_CTBILoadResultContent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTBILoadResultContent]  WITH CHECK ADD  CONSTRAINT [FK_CTBILoadResultContent_CTBILoadResult] FOREIGN KEY([CTBILoadResultId])
REFERENCES [dbo].[CTBILoadResult] ([Id])
GO
ALTER TABLE [dbo].[CTBILoadResultContent] CHECK CONSTRAINT [FK_CTBILoadResultContent_CTBILoadResult]
GO

ALTER TABLE [dbo].[CTBILoadResultContent]  WITH CHECK ADD  CONSTRAINT [FK_CTBILoadResultContent_CTContainerTypeActual] FOREIGN KEY([CTContainerTypeActualId])
REFERENCES [dbo].[CTContainerTypeActual] ([Id])
GO
ALTER TABLE [dbo].[CTBILoadResultContent] CHECK CONSTRAINT [FK_CTBILoadResultContent_CTContainerTypeActual]
GO