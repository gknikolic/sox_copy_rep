﻿using Newtonsoft.Json;
using System;

namespace ProfitOptics.Modules.Sox.Models
{
	public class GatewayMessage
    {
        [JsonProperty("gatewayID")]
        public long GatewayId { get; set; }

        [JsonProperty("gatewayName")]
        public string GatewayName { get; set; }

        [JsonProperty("accountID")]
        public long AccountId { get; set; }

        [JsonProperty("networkID")]
        public long NetworkId { get; set; }

        [JsonProperty("messageType")]
        public long MessageType { get; set; }

        [JsonProperty("power")]
        public float Power { get; set; }

        [JsonProperty("batteryLevel")]
        public float BatteryLevel { get; set; }

        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("signalStrength")]
        public float SignalStrength { get; set; }

        [JsonProperty("pendingChange")]
        public string PendingChange { get; set; }
    }
}
