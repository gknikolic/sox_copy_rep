﻿using Microsoft.AspNetCore.Identity;

namespace ProfitOptics.Framework.Web.Framework.Helpers.Extensions
{
    public static class SignInResultExtensions
    {
        public static bool Failed(this SignInResult result)
        {
            return !(result.Succeeded || result.IsLockedOut || result.IsNotAllowed || result.RequiresTwoFactor);
        }
    }
}
