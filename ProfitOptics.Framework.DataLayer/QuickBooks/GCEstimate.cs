﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCEstimate
    {
        public int Id { get; set; }
        public int GCCaseId { get; set; }
        public string Domain { get; set; }
        public bool? Sparse { get; set; }
        public string SyncToken { get; set; }
        public DateTime MetaDataCreateTime { get; set; }
        public DateTime? MetaDataLastUpdatedTime { get; set; }
        public DateTime? TxnDate { get; set; }
        public string CurrencyValue { get; set; }
        public string CurrencyName { get; set; }
        //public List<GCEstimateLine> EstimateLines { get; set; }
        public decimal TxnTaxDetailTotalTax { get; set; }
        public string CustomerValue { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMemo { get; set; }
        public decimal TotalAmt { get; set; }
        public bool ApplyTaxAfterDiscount { get; set; }
        public string PrintStatus { get; set; }
        public string EmailStatus { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual GCCase GCCase { get; set; }
        public virtual ICollection<GCEstimateLine> GCEstimateLines { get; set; }
    }
}
