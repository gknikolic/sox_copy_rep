﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Factories;
using ProfitOptics.Modules.Reporting.Models.DailyTrend;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Services.DailyTrend
{
    public class DailyTrendExportService : IDailyTrendExportService
    {
        private readonly IReportFilterModelFactory _reportFilterModelFactory;
        private readonly ISalesHistoryService _salesHistoryService;

        public DailyTrendExportService(IReportFilterModelFactory reportFilterModelFactory, ISalesHistoryService salesHistoryService)
        {
            _reportFilterModelFactory = reportFilterModelFactory;
            _salesHistoryService = salesHistoryService;
        }

        /// <inheritdoc />
        public MemoryStream ExportDailyTrendsExcel(int userId, DateTime startDate, DateTime endDate)
        {
            if (userId <= 0)
            {
                throw new ArgumentException(nameof(userId));
            }

            ReportFilterModel reportFilter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            IQueryable<ReportSalesHistory> salesHistories = _salesHistoryService
                .GetFilteredSalesHistoriesForDateRange(reportFilter, startDate, endDate);

            List<DailyTrendExportToExcelModel> exportToExcelModels = 
                ConvertSalesHistoriesToDailyTrendExportToExcelModels(salesHistories);

            MemoryStream excelFile = ExcelFile.ExportToExcelOverStream("Daily Trend",
                exportToExcelModels,
                true,
                true);

            excelFile = ExcelFile.ApplyFilterAndFreezePanes(excelFile, 2, 4, 1);

            return excelFile;
        }

        private static List<DailyTrendExportToExcelModel> ConvertSalesHistoriesToDailyTrendExportToExcelModels(
            IEnumerable<ReportSalesHistory> salesHistories)
        {
            return salesHistories.Select(s => new DailyTrendExportToExcelModel
            {
                SalesRep = s.ReportHierarchySalesRep.Name,
                BillToNum = s.ReportBillTo.BillToNum,
                BillToName = s.ReportBillTo.BillToName,
                ShipToNum = s.ReportShipTo.ShipToNum,
                ShipToName = s.ReportShipTo.ShipToName,
                VendorName = s.ReportVendor.Name,
                ItemNum = s.ReportItem.ItemNum,
                Date = s.InvoiceDate.ToString("MM/dd/yyyy"),
                CustomerClass = s.ReportCustomerClass.Name,
                ItemCategory = s.ReportItemCategory.Name,
                Quantity = s.Qty,
                ExtPrice = s.ExtPrice,
                ShippingStreet = s.ReportShipTo.Address,
                ShippingCity = s.ReportShipTo.City,
                ShippingStateProvince = s.ReportShipTo.State,
                SOPType = s.SOPType,
                SOPNumber = s.SOPNumber,
                Comment1 = s.Comment1,
                Comment2 = s.Comment2,
                Comment3 = s.Comment3,
                Comment4 = s.Comment4
            }).ToList();
        }
    }
}