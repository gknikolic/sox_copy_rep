﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.Anotations;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.Announcements.Factories;
using ProfitOptics.Modules.Announcements.Models;
using ProfitOptics.Modules.Announcements.Services;

namespace ProfitOptics.Modules.Announcements.Controllers
{
    [Area("Announcements")]
    [Authorize]
    public class AnnouncementController : BaseController
    {
        private readonly IAnnouncementService _announcementService;
        private readonly IAnnouncementModelFactory _announcementModelFactory;

        public AnnouncementController(IAnnouncementService announcementService,
            IAnnouncementModelFactory announcementModelFactory)
        {
            _announcementService = announcementService;
            _announcementModelFactory = announcementModelFactory;
        }

        [Authorize(Roles = "admin,superadmin")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetAnnouncements([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request, bool currentUserOnly)
        {
            try
            {
                int? userId = null;

                if (currentUserOnly || !User.IsInRole(ApplicationRoleNames.Admin) || !User.IsInRole(ApplicationRoleNames.SuperAdmin))
                {
                    userId = HttpContext.User.GetUserId();
                }

                PagedList<AnnouncementModel> data = await _announcementModelFactory.PrepareAnnouncementListModelAsync(request, userId);

                return new LargeJsonResult(data.ToDataTablesResponse(request));
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        [Authorize(Roles = "admin,superadmin")]
        public async Task<IActionResult> Announcement(int id)
        {
            AnnouncementModel model = await _announcementModelFactory.GetAnnouncementModelByIdAsync(id);

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AnnouncementPartial(int id)
        {
            AnnouncementModel model = await _announcementModelFactory.GetAnnouncementModelByIdAsync(id);

            return PartialView("_Announcement", model);
        }

        [HttpPost]
        public IActionResult MarkAnnouncementAsRead(int id)
        {
            int userId = GetCurrentUserId();

            _announcementService.MarkAnnouncementAsRead(userId, id);

            var announcementsList = HttpContext.Session
                .GetObject<List<AnnouncementNotificationModel>>(SessionKeys.AnnouncementList);
            
            AnnouncementNotificationModel announcement = announcementsList?.FirstOrDefault(x => x.Id == id);
            
            if (announcement != null)
            {
                announcementsList.Remove(announcement);
            }

            HttpContext.Session.SetObject(SessionKeys.AnnouncementList, announcementsList);

            return ViewComponent("Announcement");
        }

        [HttpGet]
        public IActionResult All()
        {
            bool isAdminOrSuperAdmin = HttpContext.User.IsInRole(ApplicationRoleNames.Admin) || HttpContext.User.IsInRole(ApplicationRoleNames.SuperAdmin);

            AnnouncementsPageModel model = _announcementModelFactory.PrepareAnnouncementsPageModel(isAdminOrSuperAdmin);

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin,superadmin")]
        public IActionResult Save(IFormCollection form)
        {
            try
            {
                int userId = GetCurrentUserId();

                _announcementService.Save(userId, form);

                return Json(new
                {
                    Status = "ok"
                });
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        [AjaxCall]
        [Authorize(Roles = "admin,superadmin")]
        public IActionResult AjaxDelete(int id)
        {
            try
            {
                _announcementService.Delete(id);

                return Json(new
                {
                    Status = "ok"
                });
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> GetAllAnnouncementsForCurrentUser([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request)
        {
            try
            {
                PagedList<AnnouncementModel> data = await _announcementModelFactory.PrepareAnnouncementModelsForUserIdAsync(request, GetCurrentUserId());

                return new LargeJsonResult(data.ToDataTablesResponse(request));
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }
    }
}