USE [Sox]

ALTER TABLE [Sox].[dbo].[GCTray] ADD [ActualLapCount] INT NULL;
ALTER TABLE [Sox].[dbo].[GCLifecycleStatusType] ADD [IsRequired] BIT NULL;

GO