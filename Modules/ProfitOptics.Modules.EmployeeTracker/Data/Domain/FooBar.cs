
namespace ProfitOptics.Modules.EmployeeTracker.Data.Domain
{
    public class FooBar
    {
        public FooBar()
        {
        }

        public int Id { get; set; }

        public string Foo { get; set; }

        public string Bar { get; set; }
    }
}
