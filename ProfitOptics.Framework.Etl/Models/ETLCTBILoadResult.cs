﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Models
{
    public class ETLCTBILoadResult
    {
        [JsonProperty("loadId")]
        public long LoadId { get; set; }

        [JsonProperty("loadBarcode")]
        public string LoadBarcode { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("locationId")]
        public long? LocationId { get; set; }

        [JsonProperty("locationName")]
        public string LocationName { get; set; }

        [JsonProperty("locationMethodId")]
        public long? LocationMethodId { get; set; }

        [JsonProperty("locationMethod")]
        public string LocationMethod { get; set; }

        [JsonProperty("locationType")]
        public long? LocationType { get; set; }

        [JsonProperty("loadDate")]
        public DateTime? LoadDate { get; set; }

        [JsonProperty("insertTimestamp")]
        public DateTime? InsertTimestamp { get; set; }

        [JsonProperty("updateTimestamp")]
        public DateTime? UpdateTimestamp { get; set; }

        [JsonProperty("loadTimestamp")]
        public DateTime? LoadTimestamp { get; set; }

        [JsonProperty("updateUser")]
        public string UpdateUser { get; set; }

        [JsonProperty("operator")]
        public string Operator { get; set; }

        [JsonProperty("sterilizationCycle")]
        public string SterilizationCycle { get; set; }

        [JsonProperty("caseReference")]
        public string CaseReference { get; set; }

        [JsonProperty("reason")]
        public string Reason { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("resultFlag")]
        public string ResultFlag { get; set; }

        [JsonProperty("alarm")]
        public string Alarm { get; set; }

        [JsonProperty("isLastLoad")]
        public bool? IsLastLoad { get; set; }

        [JsonProperty("sterilantBatch")]
        public string SterilantBatch { get; set; }

        [JsonProperty("maxTemperature")]
        public decimal? MaxTemperature { get; set; }

        [JsonProperty("exposureTime")]
        public string ExposureTime { get; set; }
    }
}
