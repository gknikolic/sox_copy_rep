﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Services
{
    public interface IUserHierarchyService
    {
        /// <summary>
        /// Checks if the user with the specified identifier is a corporate user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool IsCorporateUser(int userId);

        /// <summary>
        /// Checks if the user with the specified identifier is a zone user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool IsZoneUser(int userId);

        /// <summary>
        /// Checks if the user with the specified identifier is a region user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool IsRegionUser(int userId);

        /// <summary>
        /// Checks if the user with the specified identifier is a sales rep user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool IsSalesRepUser(int userId);

        /// <summary>
        /// Gets the zone identifier for the user with the specified identifier, or returns null if not found.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int? GetUserZoneId(int userId);

        /// <summary>
        /// Gets the region identifier for the user with the specified identifier, or returns null if not found.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int? GetUserRegionId(int userId);

        /// <summary>
        /// Gets the sales rep identifier for the user with the specified identifier, or returns null if not found.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int? GetUserSalesRepId(int userId);

        /// <summary>
        /// Gets all billTos for user with the specified identifier. If the userId does not match any user, return all billTos.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        List<ReportBillTo> GetUserBillTos(int userId, string search, out int totalCount, int skip = 0, int take = int.MaxValue);

        /// <summary>
        /// Gets all shipTos for user with the specified identifier. If the userId does not match any user, return all shipTos.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        List<ReportShipTo> GetUserShipTos(int userId, string search, out int totalCount, int skip = 0, int take = int.MaxValue);

        /// <summary>
        /// Checks if the user with the specified identifier can view data for the specified sales rep identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="salesRepId">The sales rep identifier.</param>
        /// <returns>true if the user can view data for the sales rep.</returns>
        bool CanUserViewDataForSalesRep(int userId, int salesRepId);

        int? GetUserRegionIdIfExistsForCurrentUser();

        string GetSalesRepNameById(int id);

    }
}
