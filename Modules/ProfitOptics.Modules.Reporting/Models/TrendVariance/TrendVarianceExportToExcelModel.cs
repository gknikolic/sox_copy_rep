﻿using ProfitOptics.Framework.Core.Data;

namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVarianceExportToExcelModel : IDataRecord
    {
        public int Id { get; set; }

        [DataField("Name", "", 1)]
        public string Name { get; set; }

        [DataField("Customer #", "", 2)]
        public string Code { get; set; }

        [DataField("Prior Sales", "$#,##0.00", 3, true)]
        public decimal PriorSales { get; set; }

        [DataField("Prior Cost", "$#,##0.00", 4, true)]
        public decimal PriorCost { get; set; }

        [DataField("Prior Quantity", "#,##0", 5, true)]
        public decimal PriorQty { get; set; }

        [DataField("Prior ASP", "$#,##0.00", 6)]
        public decimal PriorAsp => PriorQty == decimal.Zero ? decimal.Zero : PriorSales / PriorQty;

        [DataField("Prior GP", "$#,##0.00", 7, true)]
        public decimal PriorGrossProfit => PriorSales - PriorCost;

        [DataField("Prior GP Percent", "#,##0.0_)%;[Red](#,##0.0%)", 8)]
        public decimal PriorGrossProfitPercent =>
            PriorSales == decimal.Zero ? decimal.Zero : (PriorSales - PriorCost) / PriorSales;

        [DataField("Current Sales", "$#,##0.00", 9, true)]
        public decimal CurrentSales { get; set; }

        [DataField("Current Cost", "$#,##0.00", 10, true)]
        public decimal CurrentCost { get; set; }

        [DataField("Current Quantity", "#,##0", 11, true)]
        public decimal CurrentQty { get; set; }

        [DataField("Current ASP", "$#,##0.00", 12)]
        public decimal CurrentAsp => CurrentQty == decimal.Zero ? decimal.Zero : CurrentSales / CurrentQty;

        [DataField("Current GP", "$#,##0.00", 13, true)]
        public decimal CurrentGrossProfit => CurrentSales - CurrentCost;

        [DataField("Current GP Percent", "#,##0.0_)%;[Red](#,##0.0%)", 14)]
        public decimal CurrentGrossProfitPercent =>
            CurrentSales == decimal.Zero ? decimal.Zero : (CurrentSales - CurrentCost) / CurrentSales;

        [DataField("VarianceSales", "$#,##0.00_);[Red]($#,##0.00)", 15, true)]
        public decimal VarianceSales => CurrentSales - PriorSales;

        [DataField("Variance Sales Percent", "#,##0.0_)%;[Red](#,##0.0%)", 16)]
        public decimal VarianceSalesPercent =>
            PriorSales == decimal.Zero ? decimal.Zero : (CurrentSales - PriorSales) / PriorSales;

        public decimal VarianceQty => CurrentQty - PriorQty;

        public decimal VarianceQtyPercent =>
            PriorQty == decimal.Zero ? decimal.Zero : (CurrentQty - PriorQty) / PriorQty;

        [DataField("VarianceGP", "$#,##0.00_);[Red]($#,##0.00)", 17, true)]
        public decimal VarianceGrossProfit => CurrentGrossProfit - PriorGrossProfit;

        [DataField("Variance GP Percent Dollars", "#,##0.0_)%;[Red](#,##0.0%)", 18)]
        public decimal VarianceGrossProfitPercent =>
            PriorGrossProfit == decimal.Zero ? decimal.Zero : (CurrentGrossProfit - PriorGrossProfit) / PriorGrossProfit;

        [DataField("Variance GP Percent", "#,##0.0_)%;[Red](#,##0.0%)", 19)]
        public decimal VarianceGrossProfitGrowthPercent => CurrentGrossProfitPercent - PriorGrossProfitPercent;
    }
}
