﻿namespace ProfitOptics.Modules.CrossReference.Models
{
    public class ItemLookupSelect2DataModel
    {
        public string id { get; set; }

        public string itemDescription { get; set; }

        public string alternateItemNumberMedco { get; set; }

        public string mpn { get; set; }

        public string slug { get; set; }

        public string text { get; set; }
    }
}