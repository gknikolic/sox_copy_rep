using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTHierarchySalesRep")]
    public partial class QTHierarchySalesRep
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? RegionId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
