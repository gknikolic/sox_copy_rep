﻿using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Web.Areas.Account.Data.Configs;
using ProfitOptics.Framework.Web.Areas.Account.Data.Domain;

namespace ProfitOptics.Framework.Web.Areas.Account.Data
{
    public partial class Entities : DbContext
    {
        public Entities(DbContextOptions<Entities> options) : base(options)
        {
            var listener = this.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }

        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<POUserProfilePicture> POUserProfilePictures { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new AspNetRolesConfig());
            builder.ApplyConfiguration(new AspNetUsersConfig());
            builder.ApplyConfiguration(new AspNetUserRolesConfig());
            builder.ApplyConfiguration(new POUserProfilePictureConfig());
        }
    }
}
