﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class OrderStepInput
	{
		[JsonProperty("location")]
		public Location Location { get; set; }

		[JsonProperty("notes")]
		public string Notes { get; set; }
		[JsonProperty("serviceTimeSec")]
		public int ServiceTimeSec { get; set; }
	}
}
