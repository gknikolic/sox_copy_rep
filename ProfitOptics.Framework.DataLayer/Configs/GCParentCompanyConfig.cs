﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCParentCompanyConfig : IEntityTypeConfiguration<GCParentCompany>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCParentCompany> builder)
        {
            builder.ToTable("GCParentCompany");
            builder.HasKey(entity => entity.Id);
        }
    }
}
