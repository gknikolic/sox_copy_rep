﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class SearchResult
    {
        public string Expand { get; set; }

        public int? StartAt { get; set; }

        public int? Total { get; set; }

        public List<IssuesResult> Issues { get; set; }
    }
}