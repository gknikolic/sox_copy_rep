﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc.Razor;

namespace ProfitOptics.Framework.Web.Framework.Modules
{
    public class ModuleViewLocationExpander : IViewLocationExpander
    {
        private const string moduleKey = "module";

        public void PopulateValues(ViewLocationExpanderContext context)
        {
            var controller = context.ActionContext.ActionDescriptor.DisplayName;
            if (controller.Split('.').ElementAtOrDefault(1) == "Modules")
            {
                context.Values[moduleKey] = $"ProfitOptics.Modules.{controller.Split('.')[2]}";
            }
        }

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            if (context.Values.ContainsKey(moduleKey))
            {
                var module = context.Values[moduleKey];
                if (!string.IsNullOrWhiteSpace(module))
                {
                    var moduleViewLocations = new string[]
                    {
                        "/Modules/" + module + "/Views/{1}/{0}.cshtml",
                        "/Modules/" + module + "/Views/Shared/{0}.cshtml",
                    };
                    viewLocations = moduleViewLocations.Concat(viewLocations);
                }
            }

            return viewLocations;
        }
    }
}
