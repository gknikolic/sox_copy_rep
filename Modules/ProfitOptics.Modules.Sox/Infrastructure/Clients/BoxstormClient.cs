﻿using ProfitOptics.Framework.Core.Flurl;
using ProfitOptics.Framework.Core.Settings;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Infrastructure.Clients
{
	public class BoxstormClient : IBoxstormClient
	{
		private readonly Framework.DataLayer.Entities _context;
		public HttpRequestClient HttpRequestClient { get; set; }

		public BoxstormOptions BoxstormOptions { get; set; }

		public BoxstormClient(Framework.DataLayer.Entities context, BoxstormOptions boxstormOptions)
		{
			_context = context;
			BoxstormOptions = boxstormOptions;
			this.HttpRequestClient = new HttpRequestClient(BoxstormOptions.BaseUrl, string.Empty, string.Empty);
		}
		public async Task<bool> AuthAsync()
		{
			var result = false;
			return result;
		}
	}
}
