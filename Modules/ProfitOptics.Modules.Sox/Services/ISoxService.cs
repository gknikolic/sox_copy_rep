﻿using Intuit.Ipp.OAuth2PlatformClient;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Primitives;
using ProfitOptics.Framework;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.DataLayer.GroundControl;
using ProfitOptics.Framework.DataLayer.WorkWave;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using ProfitOptics.Modules.Sox.Models.QuickBooks;
using ProfitOptics.Modules.Sox.Models.ViewModels;
using ProfitOptics.Modules.Sox.Models.ViewModels.Paging;
using ProfitOptics.Modules.Sox.Models.WorkWave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GCColorStatusType = ProfitOptics.Framework.GCColorStatusType;

namespace ProfitOptics.Modules.Sox.Services
{
    public interface ISoxService
    {
        List<GCCase> GetGCCases();
        GCCase GetGCCaseById(int id, ClaimsPrincipal user);
        IEnumerable<SelectListItem> GetGCLifecycleStatusesList();
        IEnumerable<SelectListItem> GetGCCasesList();
        void ChangeGCCaseLifecycleStatus(ManageGCCaseLifecycleStatuses model);
        void DeleteGCCase(int gcCaseId);
        void DeleteAllGCCases();
        GCLifecycleStatusType GetLifecycleStatusById(int id);
        bool IsTrayInLifecycleStatus(int trayId, int statusId);
        List<CTCaseContainer> GetTraysByCTCaseId(int ctCaseId);
        List<GCTray> GetTraysForGCCase(int gcCaseId, bool? includeCTContainerType = null);
        List<GCTray> GetAllGCTrays();
        List<GCTray> GetAllInventoryTrays(List<int> actualIds = null, bool? includeLoaners = false);
        List<GCCase> GetCasesByCustomer(int customerId);
        List<GCLifecycleStatusType> GetLifecycleStatusTypes();
        CTContainerType GetCTContainerType(int id);
        void AddSKUToCTContainerTypes(bool? overwrite);
        LifecycleStatusWithTimestamp GetActiveLifecycleStatusAndTimestampForTray(int trayId);
        List<CTCaseContainer> GetCTCaseContainers();
        CTCaseContainer GetCTCaseContainerById(int id);
        List<CTCaseContainer> GetCTCaseContainersByCaseId(int id);
        List<GCCase> FilterCases(DashboardModel filterModel);
        IQueryable<GCCase> FilterCasesByUserRole(ClaimsPrincipal user);
        GCLifecycleStatusType GetActiveLifecycleStatusForCase(int caseId);
        List<GCCase> FilterCasesDashboard(DashboardModel filterModel);
        List<GCCase> FilterCasesList(GCCasesModel filterModel);
        CaseScheduleColorType GetCaseScheduleColorType(int caseId);
        List<GCCustomer> GetAllGCCustomers(bool? activeOnly = false);
        IEnumerable<SelectListItem> GetGCCustomersSelectList();
        IEnumerable<SelectListItem> GetGCCustomersFilteredSelectList(ClaimsPrincipal user);
        IEnumerable<SelectListItem> GetGCVendorsFilteredSelectList(ClaimsPrincipal user);
        List<GCVendor> GetVendors();
        void UpdateGCCaseLifecycleStatus(int gcCaseId);
        IEnumerable<SelectListItem> GetGCCustomersSelectListByDate(DateTime date);
        IEnumerable<SelectListItem> GetGCVendorsSelectList();
        void UpdateCustomer(UpdateCustomerModel model, bool? editChildCompany = true);
        GCCustomer GetCustomer(int customerId);
        void UpdateVendor(UpdateVendorModel model, bool? editChildCompany = true);
        void AddCustomer(AddCustomerModel model);
        void AddVendor(AddVendorModel model);
        void DeactivateCustomer(GCCustomerModel model);
        List<GCInvoice> GetGCInvoicesForCustomer(int value, bool? activeLinesOnly = false, bool? getUnSubmittedToQBOOnly = false);
        void DeactivateVendor(GCVendorModel model);
        ETLLogs GetLatestCTETLLogCycle(bool? lastSuccessful = null);
        void GetLatestAndLatestSuccessfulETLLogs();
        ETLLogs GetLatestSuccessfulllETLLogsWWRoute();
        ETLLogs GetLatestSuccessfulllETLLogsWWOrder();
        GCCase GetCaseById(int id);
        POSetting GetPOSetting(POSettingsType type);
        /// <summary>
        /// Adds or updates record in POSettings Table
        /// </summary>
        /// <param name="type">String value of enum: POSettingsType</param>
        /// <param name="value">String value</param>
        /// <param name="user">Currently logged user name</param>
        void AddOrUpdatePOSetting(POSettingsType type, string value, string user);
        bool RemovePOSetting(POSettingsType quickBooksRefreshToken);
        void UpdateGCCaseColorStatus(int id);

        Task SyncRoutesWithWorkWaveAsync(DateTime? date = null);

        Task<string> SyncOrdersWithWorkWaveAsync();
        GCVendor GetGCVendor(int vendorId);
        public List<GCTray> CheckTrayBarcodeForCustomer(string code, int customerId);
        List<LifecycleCategory> GetCaseLifecyceCategories(int caseId, bool getForPreviousHistory = false, bool isLoaner = false);
        string GetCaseColorStatus(int id);
        string GetTrayColorStatus(int id);
        Models.GroundControl.GCColorStatusType GetCaseColorStatusType(int gcCaseId);
        List<NoteGroup> GetCaseNotes(int caseId);
        Task ResolveGCTrayColorStatusNote(int noteId);
        void UpdateProcedureType(ProcedureTypeModel model);
        Task UnlockGCTrayColorStatus(int statusId);
        Task AddColorComment(AddColorCommentModal model, string userName);
        GCTray GetTrayById(int gCTrayId);
        GCTray GetTrayByActualId(int actualId);
        Task SetEndDateToGCInvoiceLine(int gcInvoiceLineId);
        public string GetGCImageSource(int gcImageId, int type);
        List<GCColorStatusType> GetCaseColorStatusTypes();
        public TrayHistoryCase GetTrayHistoryForSingleCase(int caseId, int trayId);
        TrayHistoryModel GetTrayHistory(int trayId);
        TrayHistoryModel GetTrayHistoryForVendors(int trayId);
        int? GetActualIdByTrayBarcode(string barcode);
        int? GetTrayIdByBarcode(string barcode);
        IEnumerable<SelectListItem> GetGCCasesListByDate(DateTime date);
        int? SetTrayStatus(string code, string lifecycleStatus, int? trayId, ClaimsPrincipal user = null);
        void SetTrayStatus(int gcTrayId, int statusCode, ClaimsPrincipal user);
        void Foamed(int trayId, string code, bool isYes);
        void StoreNote(string note, int trayId);
        void StorePictureInPodContainer(byte[] bytes, string fileName, int trayId);
        void UpdateProcedure(ProcedureModel model);

        //void StorePictureInDatabase(byte[] bytes, string code);
        List<CTContainerTypeActual> GetAllActuals();
        List<TrayImage> GetTrayImagesByBarcode(string barcode);
        List<TrayImage> GetTrayImagesById(int gcTrayId);
        TrayHistoryPodContainer GetFullPodContainerForStatus(int statusId);
        WWPodContainer GetPodContainerForStatus(int statusId);

        Task UpdateBarcodeToWWAsync(int gcTrayId);

        Task SetOrderStatusInWWAsync(int caseId, RouteStepStatusType? stepStatus);
        Task SetOrderStatusInWWIfTraysArePickedUp(int trayId);
        IEnumerable<SelectListItem> GetWWDriverSelectList();
        IEnumerable<SelectListItem> GetWWVehicleSelectList();
        List<GCCase> FilterCasesForCasePlanning(IEnumerable<GCCase> cases, string caseRef, int? vehicleId, int? driverId);
        GCLifecycleStatusType GetLifecycleStatusByTitle(string title);
        CTContainerTypeActual GetCTContainerTypeActualById(int id);
        bool CheckTrayWasInStatus(string lifecycleStatus, int trayId);
        bool CheckIfCaseWasInStatus(LifecycleStatusType lifecycleStatus, int caseId);
        void SetVendorIdsForCases();
        void SetDueTimeForGCCases();
        IEnumerable<GCTray> GetLoanerTrays(int caseId);
        List<CTContainerType> GetAllCTConntainerTypes(string vendorName = null);
        string GetImageSource(int imageId);
        byte[] GetImageBytes(int imageId);
        ProfitOptics.Modules.Sox.Models.ViewModels.Paging.PagedList<GCLocationInfoModel> GetLocationInfosPaged(LocationInfoPagingModel pagingModel, int? locationId, DateTime? startDate, DateTime? endDate);
        GCLocation GetGCLocationById(int locationId);
        GCLocationsModel GetGCLocationsModel();
        ProcedureTypesModel GetProcedureTypes(bool? includeInactive);
        ProcedureTypeModel GetProcedureType(int procedureId);
        ProceduresModel GetProcedures();
        ProcedureModel GetProcedure(int procedureId);
        public Task UpdateCaseBarcodesAsync(int caseId);
        ETLLogsModel ETLLogs();
        public void SyncCaseHistory(int gcCaseId);
        void TriggerJobs(int jobNumber);
        DashboardHourPreviewModel GetDashboardHourPreviewModel(DashboardModel filterModel);
        DashboardDetailedHourPreviewModel GetDashboardDetailedHourPreviewModel(DashboardModel filterModel);
        GCCaseModel GetDashboardCaseDetailsModel(int caseId);
        List<GCTray> GetAllGCTraysByCaseId(int caseId);
        Task DeleteOrdersInWWAsync(int caseId);
        Task<InventoryAlerStatusModel> GetInventoryAlertCount(ClaimsPrincipal user);
        PhotoWithCountSheet GetPhotoWithCountSheet(int imageId, bool isAsseblyPhoto, int statusId);
        GCTraysLifecycleStatusTypes AddOrEditProposedHistoryItemForLC(ProposedTrayLCHistoryModel model);
        TrayPhisicalLocationType GetTraysPhisicalLocation(LifecycleStatusType currentStatusType);
        bool RemoveGCTrayLifecycleStatud(int id);
        List<GoogleMapMarker> GetWWMapMarkers();
        List<GCSystemAlert> GetSystemAlerts(List<int> trayIds);
        List<GCTray> GetTraysForGCCaseIds(List<int> gcCaseIds);
        bool MarkGCSystemAlertRead(int id);
    }
}