﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Core.Data
{
    public class DataImportError : Exception
    {
        public DataImportError(int rowNumber, int columnNumber, string message)
        {
            RowNumber = rowNumber;
            ColumnNumber = columnNumber;
        }

        public int RowNumber { get; set; }
        public int ColumnNumber { get; set; }
    }

    public class DataImportResult<T> where T:IDataRecord
    {
        public DataImportResult()
        {
            Errors = new List<DataImportError>();
            Data = new List<T>();
        }

        public List<DataImportError> Errors { get; set; }
        public List<T> Data { get; set; }
    }
}
