﻿using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class AccountingsMainInvoiceVM
    {
        public GCInvoice GCInvoice { get; set; }
    }
}
