﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models
{
    public class CTContainerTypeModel
    {
        public string LifecycleStatus { get; set; }
        public int Id { get; set; }
        public int CTContainerServiceId { get; set; }
        public long LegacyId { get; set; }
        public long ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string HomeLocationName { get; set; }
        public string ProcessingLocationName { get; set; }
        public string Name { get; set; }
        public string SterilizationMethodName { get; set; }
        public string Modified { get; set; }
        public bool IsDiscarded { get; set; }
        public bool AutomaticActuals { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
		public int CTContainerTypeActualId { get; set; }
        public bool IsGCTrayActiveStatus { get; set; }
		public string SKU { get; set; }
        public string VendorName { get; set; }
        public string ColorStatus { get; set; }
		public bool IsLoaner { get; set; }
        public int? ActualLapCount { get; set; }
        public string ProcurementReferenceId { get; set; }
        public DateTime? Timestamp { get; set; }
		public string LifecycleCategory { get; set; }
        public string Location { get; set; }
    }
}
