﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Announcements.Data.Domain;

namespace ProfitOptics.Modules.Announcements.Data.Config
{
    public class AspNetRolesConfig : IEntityTypeConfiguration<AspNetRole>
    {
        public void Configure(EntityTypeBuilder<AspNetRole> builder)
        {
            builder.ToTable("AspNetRoles");
            builder.HasKey(role => role.Id);

            builder.HasIndex(role => role.Name).HasName("RoleNameIndex").IsUnique();

            builder.Property(role => role.Name).HasMaxLength(256).IsRequired();
        }
    }
}
