﻿using System;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public abstract class TrendVarianceCustomerDrillThroughViewModel
    {
        protected TrendVarianceCustomerDrillThroughViewModel(int customerId,
            string customerName, 
            string customerNum, 
            ComparisonType comparisonType,
            DateTime? comparisonStartDate, 
            DateTime? comparisonEndDate, 
            bool isCorporate)
        {
            CustomerId = customerId;
            CustomerName = customerName;
            CustomerNum = customerNum;
            ComparisonType = comparisonType;
            ComparisonStartDate = comparisonStartDate;
            ComparisonEndDate = comparisonEndDate;
            IsCorporate = isCorporate;
        }

        public int CustomerId { get; }

        public string CustomerName { get; }

        public string CustomerNum { get; }

        public ComparisonType ComparisonType { get; }

        public DateTime? ComparisonStartDate { get; }

        public DateTime? ComparisonEndDate { get; }

        public string StartDateDefault => ComparisonType == ComparisonType.CustomDateRangeYearOverYear
            ? ComparisonStartDate?.ToString("MM-dd-yyyy")
            : new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("MM-dd-yyyy");

        public string EndDateDefault => ComparisonType == ComparisonType.CustomDateRangeYearOverYear
            ? ComparisonEndDate?.ToString("MM-dd-yyyy")
            : DateTime.Now.ToString("MM-dd-yyyy");

        public bool IsCorporate { get; }

        public abstract bool IsBillTo { get; }

        public abstract string DataUrl { get; }
    }
}
