﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Identity.Domain
{
    public class GCChildCompanyUsers
    {
        public int Id { get; set; }
        public int GCChildCompanyId { get; set; }
        public int UserId { get; set; }
        public virtual GCChildCompany GCChildCompany { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
