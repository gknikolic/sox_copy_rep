﻿using AutoMapper;

namespace ProfitOptics.Framework.Etl.AutoMapperProfiles
{
    public class CensiTrackProfile : Profile
    {
        public CensiTrackProfile()
        {
            CreateMap<Etl.Models.ETLCTCase, Framework.DataLayer.CTCase>();

            CreateMap<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTFacility, Framework.DataLayer.CTFacility>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            //CreateMap<Etl.Models.ETLCTContainerService, Framework.DataLayer.GCVendor>()
            //    .ForMember(x => x.Id, opt => opt.Ignore())
            //    .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Name))
            //    .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<Etl.Models.ETLCTContainerService, Framework.DataLayer.CTContainerService>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<Etl.Models.ETLCTContainerType, Framework.DataLayer.CTContainerType>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<Etl.Models.ETLCTContainerTypeActual, Framework.DataLayer.CTContainerTypeActual>()
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<Etl.Models.ETLCTVendor, Framework.DataLayer.CTVendor>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<Etl.Models.ETLCTContainerItem, Framework.DataLayer.CTContainerItem>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CTProduct, opt => opt.MapFrom(src => src.Product));

            CreateMap<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTProduct, Framework.DataLayer.CTProduct>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<Etl.Models.ETLCTContainerTypeGraphic, Framework.DataLayer.CTContainerTypeGraphic>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.GraphicId, opt => opt.MapFrom(src => src.GraphicKey.GraphicId))
                .ForMember(dest => dest.IsGlobal, opt => opt.MapFrom(src => src.GraphicKey.IsGlobal));

            CreateMap<Etl.Models.ETLCTCaseContainer, Framework.DataLayer.CTCaseContainer>()
                .ForMember(dest => dest.DueTimestampOriginal, opt => opt.MapFrom(src => src.DueTimestamp))
                .ForMember(dest => dest.EndTimestampOriginal, opt => opt.MapFrom(src => src.EndTimestamp))
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(dest => dest.MultipleBasisOriginal, opt => opt.MapFrom(src => src.MultipleBasis))
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLocationType, Framework.DataLayer.CTLocationType>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTLocation, Framework.DataLayer.CTLocation>()
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTContainerTypeActualHistoryItem, Framework.DataLayer.CTContainerTypeActualHistoryItem>()
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<Etl.Models.ETLCTBILoadResult, Framework.DataLayer.CTBILoadResult>()
                .ForMember(dest => dest.LoadDateOriginal, opt => opt.MapFrom(src => src.LoadDate))
                .ForMember(dest => dest.InsertTimestampOriginal, opt => opt.MapFrom(src => src.InsertTimestamp))
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(dest => dest.LoadTimestampOriginal, opt => opt.MapFrom(src => src.LoadTimestamp))
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<Etl.Models.ETLCTBILoadResultContent, Framework.DataLayer.CTBILoadResultContent>()
                .ForMember(dest => dest.LastUpdateOriginal, opt => opt.MapFrom(src => src.LastUpdate))
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<Etl.Models.ETLCTBILoadResultIndicator, Framework.DataLayer.CTBILoadResultIndicator>()
                .ForMember(dest => dest.InsertTimestampOriginal, opt => opt.MapFrom(src => src.InsertTimestamp))
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<Etl.Models.ETLCTBILoadResultGraphic, Framework.DataLayer.CTBILoadResultGraphic>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(dest => dest.GraphicId, opt => opt.MapFrom(src => src.GraphicKey.GraphicId))
                .ForMember(dest => dest.IsGlobal, opt => opt.MapFrom(src => src.GraphicKey.IsGlobal));

            CreateMap<Framework.DataLayer.CTCaseContainer, Framework.DataLayer.CTCaseContainerHistorical>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CTCaseContainerId, opt => opt.MapFrom(src => src.Id));

            CreateMap<Framework.DataLayer.CTCase, Framework.DataLayer.CTCaseHistorical>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CTCaseId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTQualityFeedItem, Framework.DataLayer.CTQualityFeedItem>();

            CreateMap<ProfitOptics.Modules.Sox.Models.CensiTrack.ETL.ETLCTQualityFeedItemGraphic, Framework.DataLayer.CTQualityFeedItemGraphic>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.GraphicId, opt => opt.MapFrom(src => src.GraphicKey.GraphicId))
                .ForMember(dest => dest.IsGlobal, opt => opt.MapFrom(src => src.GraphicKey.IsGlobal));
        }
    }
}
