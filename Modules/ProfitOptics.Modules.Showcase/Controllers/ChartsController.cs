﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;

namespace ProfitOptics.Modules.Showcase.Controllers
{
    [Authorize]
    [Area("Showcase")]
    public class ChartsController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}