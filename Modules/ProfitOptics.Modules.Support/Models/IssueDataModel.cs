﻿namespace ProfitOptics.Modules.Support.Models
{
    public class IssueDataModel
    {
        public string Key { get; set; }

        public string Summary { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }

        public string FixVersion { get; set; }

        public string Assignee { get; set; }

        public string Reporter { get; set; }

        public string LocalCreator { get; set; }

        public string ProjectName { get; set; }

        public string Component { get; set; }

        public string Updated { get; set; }

        public string Description { get; set; }

        public string Priority { get; set; }

        public string DueDate { get; set; }

        public bool DisplayNotification { get; set; }

        public bool CanEdit { get; set; }

        public bool IsWatching { get; set; }

        public bool IsGlobalWatcher { get; set; }
    }
}