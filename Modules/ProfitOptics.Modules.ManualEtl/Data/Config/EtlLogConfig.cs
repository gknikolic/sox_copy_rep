﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.ManualEtl.Data.Domain;

namespace ProfitOptics.Modules.ManualEtl.Data.Config
{
    public class EtlLogConfig : IEntityTypeConfiguration<ETLLog>
    {
        public void Configure(EntityTypeBuilder<ETLLog> builder)
        {
            builder.ToTable("ETLLog");
            builder.HasKey(log => log.Id);

            builder.Property(log => log.Source).HasMaxLength(50).IsRequired();
            builder.Property(log => log.FileName).HasMaxLength(50).IsRequired();
            builder.Property(log => log.Username).HasMaxLength(50).IsRequired();
            builder.Property(log => log.Status).HasMaxLength(50).IsRequired();

        }
    }
}
