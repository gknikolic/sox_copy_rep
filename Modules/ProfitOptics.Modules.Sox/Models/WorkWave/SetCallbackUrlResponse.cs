﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class SetCallbackUrlResponse
	{
		[JsonProperty("previousUrl")]
		public string PreviousUrl { get; set; }
		[JsonProperty("url")]
		public string Url { get; set; }
	}
}
