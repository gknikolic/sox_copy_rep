﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ProfitOptics.Framework.Web.RestApiInfrastructure
{
    public class StartupConfiguration
    {
        public static TConfig ConfigureStartupConfig<TConfig>(IServiceCollection services, IConfiguration configuration) where TConfig : class, new()
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            //create instance of config
            var config = new TConfig();
            var classType = typeof(TConfig);
            //bind it to the appropriate section of configuration
            configuration.Bind(classType.Name, config);

            //and register it as a service
            services.AddSingleton(config);

            return config;
        }
    }
}
