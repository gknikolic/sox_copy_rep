﻿namespace ProfitOptics.Framework.Web.Framework.ServiceResults
{
    public class EmailConfirmationUrlResult : AuthenticationServiceResult
    {
        public string Url;

        public static EmailConfirmationUrlResult Success(string url) => new EmailConfirmationUrlResult()
        {
            Status = AuthenticationServiceResultStatusCode.Success,
            Error = null,
            Url = url
        };
    }
}
