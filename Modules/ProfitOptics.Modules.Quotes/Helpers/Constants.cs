﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Quotes.Helpers
{
    public class Constants
    {
        private static readonly List<string> ClassesToExcludeFromItems = new List<string> { "XXXX", "SAMP", "MKTG", "SPAR" };

        public static List<string> GetClassesToExclude()
        {
            return ClassesToExcludeFromItems;
        }
    }
}