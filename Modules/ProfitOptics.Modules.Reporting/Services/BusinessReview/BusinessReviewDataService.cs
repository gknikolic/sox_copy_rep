﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Services.BusinessReview
{
    public class BusinessReviewDataService : IBusinessReviewDataService
    {
        #region Field

        private readonly Entities _context;

        #endregion

        #region Ctor

        public BusinessReviewDataService(Entities context)
        {
            this._context = context;
        }

        #endregion

        #region Methods

        public string GetCustomerName(int customerId, BusinessReviewReportType reportType)
        {
            if (customerId < 0)
            {
                throw new Exception("Invalid customer Id.");
            }

            var result = "";

            switch (reportType)
            {
                case BusinessReviewReportType.BillTo:
                    {
                        result = _context.ReportBillTo.Where(item => item.Id == customerId).Select(item => item.BillToName).SingleOrDefault();
                    }
                    break;
                case BusinessReviewReportType.ShipTo:
                    {
                        result = _context.ReportShipTo.Where(item => item.Id == customerId).Select(item => item.ShipToName).SingleOrDefault();
                    }
                    break;
            }

            return result;
        }

        public List<ReportSalesHistory> GetReportSalesHistoryData(int customerId, BusinessReviewReportType reportType)
        {
            if(customerId < 0)
            {
                throw new ArgumentException(nameof(customerId));
            }

            IQueryable<ReportSalesHistory> result = _context.ReportSalesHistory.AsQueryable();

            switch (reportType)
            {
                case BusinessReviewReportType.BillTo:
                    result = result.Where(item => item.BillToId == customerId);
                    break;
                case BusinessReviewReportType.ShipTo:
                    result = result.Where(item => item.ShipToId == customerId);
                    break;
                default:
                    throw new ArgumentException(@"Unsupported report type specified", nameof(reportType));
            }

            return result.ToList();
        }

        #endregion


    }
}
