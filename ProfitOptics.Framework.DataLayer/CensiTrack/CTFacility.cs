﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTFacility
    {
        public int Id { get; set; }
        public long LegacyId { get; set; }
        public long? ProcessingFacilityId { get; set; }
        public string Name { get; set; }
        public string CompositeFacilityKey { get; set; }
        public bool? IsProcessingFacility { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
    }
}
