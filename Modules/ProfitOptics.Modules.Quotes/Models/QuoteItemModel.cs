﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class QuoteItemModel
    {
        public int Id { get; set; }
        public int LineNumber { get; set; }
        public int QuoteId { get; set; }
        public string QuoteName { get; set; }
        public int? CustomerNum { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int? FromZip { get; set; }
        public int? ShipToId { get; set; }

        //public string ToZip { get; set; }
        public decimal? AverageShippingCost { get; set; }

        public decimal? TotAdjMarginPerc { get; set; }
        public decimal? TotFloorMarginPerc { get; set; }
        public decimal? QuoteTotalWeight { get; set; }
        public decimal? QuoteExtendedTotalCost { get; set; }
        public decimal? QuoteExtendedTotalRevenue { get; set; }
        public decimal? QuoteExtendedMDollarBeforeFreight { get; set; }
        public decimal? QuoteExtendedNetShippingCost { get; set; }
        public decimal? QuoteExtendedNetShippingRevenue { get; set; }
        public decimal? QuoteExtendedAdjustMarginAmt { get; set; }
        public decimal? QuoteExtendedAdjustMarginPct { get; set; }
        public decimal? FloorTotalRevenue { get; set; }
        public decimal? FloorTotalCost { get; set; }
        public decimal? MDollar { get; set; }
        public decimal? MPercent { get; set; }
        public string Notes { get; set; }
        public List<ItemModel> QuoteItems { get; set; }
        public bool IsArchived { get; set; }
        public string OrderFrequency { get; set; }

        public class LookupSelect2Data
        {
            public string id { get; set; }
            public string slug { get; set; }
            public string text { get; set; }
        }
    }
}