using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTQuote")]
    public partial class QTQuote
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int CustomerBillToId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime? PresentedToCustomer { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public DateTime? ValidThroughDate { get; set; }

        public string Status { get; set; }

        public string HeaderComments { get; set; }

        public string FooterComments { get; set; }

        public string ContactEmail { get; set; }

        public string CustomerReference { get; set; }

        public bool? NewBusiness { get; set; }

        public DateTime? EstimatedFirstOrderDate { get; set; }

        public decimal? Probability { get; set; }

        public decimal? TaxAmount { get; set; }

        public bool? Freight { get; set; }

        public string OrderFrequency { get; set; }

        public decimal? FreightCapAmount { get; set; }

        public decimal? AverageShippingCost { get; set; }

        public int? CustomerShipToId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? OriginZipCode { get; set; }

        public int? UnitTimeFrame { get; set; }

        public int? FuelSurchargeFactor { get; set; }

        public decimal? ShippingCost { get; set; }

        public string FreightPaymentType { get; set; }

        public string ShipToZipOverride { get; set; }

        public string ShippingMethod { get; set; }
    }
}
