﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTContainerTypeActualHistoryItem
    {
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string LoadId { get; set; }
        public DateTime UpdateTimestamp { get; set; }
        public string UpdateUserId { get; set; }
        public string LocationElapsedCase { get; set; }
    }
}
