﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.ManualEtl.Models
{
    public class ManualUploadViewModel
    {
        /// <summary>
        /// List of file types of the model.
        /// </summary>
        public List<ManualUploadFileTypeModel> FileTypes { get; set; }

        /// <summary>
        /// A static list, that contains pre-configured information needed for fetching file type data.
        /// </summary>
        public static List<ManualUploadFileConfigModel> FileTypeConfigs { get; set; }

        public ManualUploadViewModel()
        {
            FileTypes = new List<ManualUploadFileTypeModel>();
            FileTypeConfigs = new List<ManualUploadFileConfigModel>();
        }
    }
}