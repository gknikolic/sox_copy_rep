﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class LoginModel
    {
        public LoginModel()
        {

        }

        public LoginModel(VerifyCodeModel model)
        {
            RememberMe = model.RememberMe;
            ReturnUrl = model.ReturnUrl;
        }

        [Required]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
        public string FullName { get; set; }
    }
}
