﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    public class MenuFragmentRendererFactory : IMenuFragmentRendererFactory
    {
        /// <inheritdoc />
        public IEnumerable<IMenuFragmentRenderer> CreateMenuFragmentRenderers()
        {
            List<IMenuFragmentRenderer> menuFragmentRenderers = Assembly.GetExecutingAssembly().GetTypes()
                .Where(type => !type.IsInterface && !type.IsAbstract && typeof(IMenuFragmentRenderer).IsAssignableFrom(type))
                .Select(type => (IMenuFragmentRenderer)Activator.CreateInstance(type))
                .OrderBy(mfr => mfr.Order)
                .ToList();

            return menuFragmentRenderers;
        }
    }
}