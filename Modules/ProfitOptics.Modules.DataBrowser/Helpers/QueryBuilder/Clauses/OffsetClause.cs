﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
namespace ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Clauses
{
    public class OffsetClause
    {
        public OffsetClause(long offset, long take)
        {
            Offset = offset;
            Take = take;
        }

        public long Offset { get; set; }
        public long Take { get; set; }
    }
}