namespace ProfitOptics.Framework.Web.RestApi.Models.Customer
{
    public class CustomerResponse : BaseResponse<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer>
    {
        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="category">Saved category.</param>
        /// <returns>Response.</returns>
        public CustomerResponse(ProfitOptics.Framework.Web.RestApi.DataLayer.Customer customer) : base(customer)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public CustomerResponse(string message) : base(message)
        { }
    }
}