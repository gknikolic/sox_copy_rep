﻿using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Services
{
	public interface IBoxstormService
	{
		public Task<bool> AuthAsync();
	}
}
