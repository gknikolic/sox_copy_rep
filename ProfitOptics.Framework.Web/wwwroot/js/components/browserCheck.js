﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

let BrowserCheck = (function() {
    // Opera 8.0+
    let isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    let isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

    // Internet Explorer 6-11
    let isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    let isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    let isChrome = !!window.chrome;

    // Blink engine detection
    let isBlink = (isChrome || isOpera) && !!window.CSS;

    let init = function() {
        if (!isChrome) {
            $("body").prepend($("<div id='browsercheck' />")
                .css({
                    "width": "100%",
                    "position": "relative",
                    "top": "0",
                    "left": "0",
                    "color":"white",
                    "background-color": "#017397",
                    "z-index": "999",
                    "text-align": "center",
                    "padding-top": "9px"
                })
                .append($("<div />")
                    .css({
                        "display": "inline-block",
                    })
                    .html("We noticed You are not using Chrome browser. We recommend You switch to Chrome as the application was designed for and will run most efficiently on Chrome. Please <a style='color:white' href='https://www.google.com/chrome/browser/desktop/index.html' target='_blank'><u>click here to download Chrome</u></a> if you haven't already."))
                .append($("<div />")
                    .css({
                        "width": "30px",
                        "height": "30px",
                        "display": "inline-block",
                        "cursor": "pointer",
                        "margin-left": "20px"
                    })
                    .html("&times;")
                    .on("click", function (event) {
                        $this = $(event.target);
                        $this.parents("div").hide();
                    }))
            );
        }
    };
    return {
        Init: init
    };
})();

(function() {
    BrowserCheck.Init();
})();