﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class ProcedureTypeModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Procedure Type")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Abbreviation")]
        public string Abbreviation { get; set; }

        [Required]
        [Display(Name = "Tray Count")]
        public int TrayCount { get; set; }

        [Required]
        [Display(Name = "Procedure Price OEM")]
        public decimal ProcedurePriceOEM { get; set; }

        [Display(Name = "Capped Tray Number")]
        public int? CappedTrayNumber { get; set; }

        [Display(Name = "Tray Price Above Cap")]
        public decimal? TrayPriceAboveCap { get; set; }

        [Required]
        [Display(Name = "Procedure Price Customer")]
        public decimal ProcedurePriceCustomer { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        [Display(Name = "ICD9")]
        public string ICD9 { get; set; }

        [Display(Name = "ICD10")]
        public string ICD10 { get; set; }
    }
}
