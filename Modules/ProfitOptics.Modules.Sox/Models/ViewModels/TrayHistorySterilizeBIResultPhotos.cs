﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayHistorySterilizeBIResultPhotos
	{
		public TrayHistorySterilizeBIResult BIResult { get; set; }
		public DateTime Timestamp { get; set; }
		public List<string> ImagesData { get; set; }
		public string Title { get; set; }
		public int NumOfPhotos { get; set; }
	}
}
