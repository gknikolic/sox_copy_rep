USE [Sox]

ALTER TABLE [dbo].[GCTraysLifecycleStatusTypes] ADD TimestampUtc DATETIME NULL;
ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem] ADD UpdateTimestampUtc DATETIME NULL;
ALTER TABLE [dbo].[CTCaseContainer] ADD DueTimestampUtc DATETIME NULL;
ALTER TABLE [dbo].[CTCaseContainer] ADD EndTimestampUtc DATETIME NULL;
ALTER TABLE [dbo].[CTCaseContainer] ADD UpdateTimestampUtc DATETIME NULL;
ALTER TABLE [dbo].[CTCaseContainer] ADD MultipleBasisUtc DATETIME NULL;
ALTER TABLE [dbo].[CTBILoadResult] ADD LoadDateUtc DATETIME NULL;
ALTER TABLE [dbo].[CTBILoadResult] ADD InsertTimestampUtc DATETIME NULL;
ALTER TABLE [dbo].[CTBILoadResult] ADD UpdateTimestampUtc DATETIME NULL;
ALTER TABLE [dbo].[CTBILoadResult] ADD LoadTimestampUtc DATETIME NULL;
ALTER TABLE [dbo].[CTBILoadResultContent] ADD LastUpdateUtc DATETIME NULL;
ALTER TABLE [dbo].[CTBILoadResultIndicator] ADD InsertTimestampUtc DATETIME NULL;
ALTER TABLE [dbo].[CTBILoadResultIndicator] ADD UpdateTimestampUtc DATETIME NULL;
ALTER TABLE [dbo].[CTBILoadResultGraphic] ADD UpdateTimestampUtc DATETIME NULL;

GO