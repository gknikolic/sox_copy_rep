﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System.Linq;
using System.Linq.Dynamic;

namespace ProfitOptics.Framework.Web.Framework.Helpers.DataTables
{
    public static class Extensions
    {
        public static IQueryable<T> ApplySort<T>(this IQueryable<T> source, IDataTablesRequest request)
        {
            if (request.Columns == null)
                return source;

            var sortedColumns = request.Columns.GetSortedColumns().OrderBy(x => x.OrderNumber)
                    .Select(x => $"{x.Data} {(x.SortDirection == Column.OrderDirection.Ascendant ? "asc" : "desc")}").ToArray();

            return sortedColumns.Length > 0 ? source.OrderBy(string.Join(",", sortedColumns)) : source;
        }

        public static IQueryable<T> ApplyPagination<T>(this IQueryable<T> source, IDataTablesRequest request)
        {
            return source.Skip(request.Start).Take(request.Length);
        }
    }
}
