﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTQualityFeedRequest
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("fields")]
        public List<ETLQualityFeedFieldsItem> Fields { get; set; }

        [JsonProperty("reportParameters")]
        public List<ETLQualityFeedReportParameterItem> ReportParameters { get; set; }
    }

    public class ETLQualityFeedFieldsItem
    {
        [JsonProperty("fieldId")]
        public int FieldId { get; set; }

        [JsonProperty("reportId")]
        public int ReportId { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("defaultOrder")]
        public int DefaultOrder { get; set; }

        [JsonProperty("displayFlag")]
        public string DisplayFlag { get; set; }

        [JsonProperty("sortOrder")]
        public int SortOrder { get; set; }

        [JsonProperty("valueList")]
        public string ValueList { get; set; }

        [JsonProperty("dataType")]
        public int DataType { get; set; }

    }

    public class ETLQualityFeedReportParameterItem
    {
        [JsonProperty("parameterId")]
        public int ParameterId { get; set; }

        [JsonProperty("value")]
        public int? Value { get; set; }

        [JsonProperty("dateQuantity")]
        public int? DateQuantity { get; set; }

        [JsonProperty("dateType")]
        public string DateType { get; set; }

        [JsonProperty("isRuntime")]
        public bool? IsRuntime { get; set; }
    }
}
