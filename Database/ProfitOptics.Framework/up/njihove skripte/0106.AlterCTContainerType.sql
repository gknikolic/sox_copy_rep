USE [Sox]

ALTER TABLE [dbo].[CTContainerType] ADD FacilityId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD SterilizationMethodId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD FirstAlternateMethodId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD SecondAlternateMethodId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD ThirdAlternateMethodId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD HomeLocationId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD ProcessingLocationId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD PhysicianName NVARCHAR(100) NULL;
ALTER TABLE [dbo].[CTContainerType] ADD UsesBeforeService BIT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD UsageInterval NVARCHAR(10) NULL;
ALTER TABLE [dbo].[CTContainerType] ADD UsageIntervalUnitOfMeasure NVARCHAR(20) NULL;
ALTER TABLE [dbo].[CTContainerType] ADD StandardAssemblyTime NVARCHAR(50) NULL;
ALTER TABLE [dbo].[CTContainerType] ADD Weight NVARCHAR(20) NULL;
ALTER TABLE [dbo].[CTContainerType] ADD ProcessingCost NVARCHAR(20) NULL;
ALTER TABLE [dbo].[CTContainerType] ADD ProcurementReferenceId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD DecontaminationInstructions NVARCHAR(MAX) NULL;
ALTER TABLE [dbo].[CTContainerType] ADD AssemblyInstructions NVARCHAR(MAX) NULL;
ALTER TABLE [dbo].[CTContainerType] ADD SterilizationInstructions NVARCHAR(MAX) NULL;
ALTER TABLE [dbo].[CTContainerType] ADD CountSheetComments NVARCHAR(MAX) NULL;
ALTER TABLE [dbo].[CTContainerType] ADD PriorityId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD BiologicalTestRequired BIT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD ClassSixTestRequired BIT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD AssemblyByException BIT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD SoftWrap BIT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD ComplexityLevelId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD OriginalContainerTypeId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD VendorId BIGINT NULL;
ALTER TABLE [dbo].[CTContainerType] ADD VendorName NVARCHAR(200) NULL;

GO