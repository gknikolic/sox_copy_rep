USE [Sox]

BEGIN

 	ALTER TABLE [Sox].[dbo].[CTCaseContainer] DROP COLUMN MultipleBasis;
	ALTER TABLE [Sox].[dbo].[CTCaseContainer] ADD MultipleBasis DATETIME NULL;
	
END