﻿using Microsoft.AspNetCore.Http;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.Reporting.Session.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Services.MarginBridge
{
    public class MarginBridgeSessionService : IMarginBridgeSessionService
    {
        private readonly ISession _session;
        private readonly IMemoryCacheHelper _memoryCache;
        private SessionKeys MBSessionKey = SessionKeys.MarginBridge;

        public MarginBridgeSessionService(IHttpContextAccessor httpContextAccessor, IMemoryCacheHelper memoryCache)
        {
            _memoryCache = memoryCache;
            _session = httpContextAccessor.HttpContext.Session;
        }

        private MarginBridgeSession _currentSession;

        public MarginBridgeSession Current =>
            _currentSession ?? (_currentSession = _memoryCache.Get($"{_session.Id}{MBSessionKey.ToString()}", () => new MarginBridgeSession(), 30));

        public bool DownloadFinished
        {
            get => _memoryCache.Get($"{_session.Id}DownloadFinished", () => false, 1);
            set => _memoryCache.Set($"{_session.Id}DownloadFinished", value, 1);
        }

        public void Save(MarginBridgeSession session)
        {
            _currentSession = session;
            _memoryCache.Set($"{_session.Id}{MBSessionKey.ToString()}", session, 30);
        }
    }
}