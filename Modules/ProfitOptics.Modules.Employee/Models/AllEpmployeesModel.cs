﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Employee.Models
{
    public class AllEmployeesModel
    {
        public List<EmployeeModel> Employees { get; set; }

        public AllEmployeesModel()
        {
            Employees = new List<EmployeeModel>();
        }
    }
}
