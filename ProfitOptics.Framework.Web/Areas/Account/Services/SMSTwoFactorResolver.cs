﻿using Microsoft.AspNetCore.Identity;
using ProfitOptics.Framework.Core.SMS;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Services;
using System;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Services
{
    public class SMSTwoFactorResolver : ITwoFactorResolver
    {
        private readonly ITwilio _twilio;
        private readonly UserManager<ApplicationUser> _userManager;

        public SMSTwoFactorResolver(ITwilio twilio,
            UserManager<ApplicationUser> userManager)
        {
            this._twilio = twilio;
            this._userManager = userManager;
        }

        /// <summary>
        /// Implementation of ITwoFactorResolver method that sends SMS with the verification code to user
        /// </summary>
        /// <param name="code">Verification code</param>
        /// <param name="user">User</param>
        /// <returns></returns>
        public async Task<bool> SendCode(string code, ApplicationUser user)
        {
            try
            {
                if (!await _userManager.IsPhoneNumberConfirmedAsync(user))
                {
                    return false;
                }
                _twilio.Send(user.PhoneNumber, "Your ProfitOptics security code is " + code);
                return true;
            }
            catch (Exception ex) 
            {
                return false;
            }
        }

        public string GetProviderName()
        {
            return TwoFactorProviderNames.Phone;
        }
    }
}
