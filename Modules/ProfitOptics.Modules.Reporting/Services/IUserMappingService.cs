﻿using System.Linq;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Services
{
    public interface IUserMappingService
    {
        /// <summary>
        /// Gets the report user mapping for the specified user identifier.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ReportUserMapping GetReportUserMappingForUserId(int userId);

        /// <summary>
        /// Gets all user mappings
        /// </summary>
        /// <returns></returns>
        IQueryable<ReportUserMapping> GetAllUserMappings();
    }
}
