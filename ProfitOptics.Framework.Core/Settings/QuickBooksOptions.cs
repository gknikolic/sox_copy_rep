﻿namespace ProfitOptics.Framework.Core.Settings
{
	public class QuickBooksOptions
    {
        public string BaseUrl { get; set; }
        public string InstanceUrl { get; set; }
        public string CompanyId { get; set; }
        public string MinorVersion { get; set; }
        public string RedirectUrl { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string DiscoveryUrl { get; set; }

        public string GetUrl(string urlPart) 
        {
            return $"https://{ BaseUrl }/v3/company/{CompanyId}/{urlPart}?minorversion={MinorVersion}";
        }
    }
}
