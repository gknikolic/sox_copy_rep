﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class InvoiceLine
    {
        public int? Id { get; set; }
        public int? LineNum { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        //public LinkedTxn[] LinkedTxn { get; set; }
        public string DetailType { get; set; }
        public EstimateLineDetail SalesItemLineDetail { get; set; }

        public object SubTotalLineDetail { get; set; }
    }
}
