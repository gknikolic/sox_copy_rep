﻿using Microsoft.AspNetCore.Routing;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    public interface IRouteRegistrator
    {
        void RegisterRoutes(IRouteBuilder routeBuilder);
    }
}
