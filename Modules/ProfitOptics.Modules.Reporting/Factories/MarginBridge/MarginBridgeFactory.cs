﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Extensions;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;
using ProfitOptics.Modules.Reporting.Services.MarginBridge;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;
using ProfitOptics.Modules.Reporting.Session.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Factories.MarginBridge
{
    public class MarginBridgeFactory : IMarginBridgeFactory
    {
        #region Fields

        private readonly IMarginBridgeService _marginBridgeService;
        private readonly IMarginBridgeAlgorithmService _marginBridgeAlgorithmService;
        private readonly IEPPlusExtensionService _epPlusExtensionService;
        private readonly IMarginBridgeSessionService _sessionService;
        private readonly IReportFilterModelFactory _reportFilterModelFactory;
        private readonly IMarginPeriodRangeProvider _periodRangeProvider;

        #endregion Fields

        #region Ctor

        public MarginBridgeFactory(IMarginBridgeService marginBridgeService,
            IMarginBridgeAlgorithmService marginBridgeAlgorithmService,
            IEPPlusExtensionService epPlusExtensionService,
            IMarginBridgeSessionService sessionService,
            IReportFilterModelFactory reportFilterModelFactory,
            IMarginPeriodRangeProvider periodRangeProvider)
        {
            _marginBridgeService = marginBridgeService;
            _marginBridgeAlgorithmService = marginBridgeAlgorithmService;
            _epPlusExtensionService = epPlusExtensionService;
            _sessionService = sessionService;
            _reportFilterModelFactory = reportFilterModelFactory;
            _periodRangeProvider = periodRangeProvider;
        }

        #endregion Ctor

        #region Methods
        
        public MargineBridgeResponseDataModel GetMarginBridgeStatistics(MarginBridgeDataModel model, int userId)
        {
            var filter = PrepareMarginBridgeFilterModel(model, userId);

            var productDbColumnName = model.ProductSelectionLevel.GetProductKeyColumnName();
            var locationDbColumnName = model.LocationSelectionLevel.GetLocationKeyColumnName();

            var inputData1 = _marginBridgeService.CreateProcessInputData(filter, model.FirstPeriod, PeriodType.First, model.ProductSelectionLevel, model.LocationSelectionLevel);
            var inputData2 = _marginBridgeService.CreateProcessInputData(filter, model.SecondPeriod, PeriodType.Second, model.ProductSelectionLevel, model.LocationSelectionLevel);

            var outputData = _marginBridgeAlgorithmService.Process1(inputData1, inputData2, model.MarginBridgeType);

            var fullOutput = _marginBridgeService.GetFullOutput(inputData1.DataPoints.Values, inputData2.DataPoints.Values, outputData.DataPoints, model.ProductSelectionLevel,
                model.LocationSelectionLevel, productDbColumnName, locationDbColumnName);

            var result = new MargineBridgeResponseDataModel
            {
                FirstPeriod = model.FirstPeriod,
                SecondPeriod = model.SecondPeriod,
                FullOutputData = fullOutput,
                MaxLocationLevel = model.LocationSelectionLevel,
                MaxProductLevel = model.ProductSelectionLevel,
                GroupedByMax = false,
                MaxLocationLevelString = model.LocationSelectionLevel.GetLocationLevelTypeButtonText(),
                MaxProductLevelString = model.ProductSelectionLevel.GetProductLevelTypeButtonText(),
                AlgorithmOutput = outputData.DataPoints,
                MarginBridgeType = model.MarginBridgeType
            };

            return result;
        }

        public string CreateFileName(string fileNameBase)
        {
            var today = DateTime.Now;
            var todayString = today.ToString("yyyy-MM-dd-HH-mm");
            var fileName = fileNameBase + todayString + ".xlsx";

            return fileName;
        }

        public MarginBridgeStatisticsModel CreateMarginBridgeStatisticsModel()
        {
            var model = new MarginBridgeStatisticsModel
            {
                FirstPeriod = _sessionService.Current.StatisticsDataModel.FirstPeriod,
                SecondPeriod = _sessionService.Current.StatisticsDataModel.SecondPeriod,
                LocationLevel = _sessionService.Current.StatisticsDataModel.LocationLevel,
                ProductLevel = _sessionService.Current.StatisticsDataModel.ProductLevel,
                GroupedBy = _sessionService.Current.StatisticsDataModel.GroupedBy,
                CanDrillThrough = _sessionService.Current.StatisticsDataModel.CanDrillThrough,
                LocationFilterMaxLevelString = _sessionService.Current.StatisticsDataModel.MaxLocationLevelString,
                ProductFilterMaxLevelString = _sessionService.Current.StatisticsDataModel.MaxProductLevelString,
                TotalsPermanent = _sessionService.Current.StatisticsDataModel.TotalsPermanent,
                GroupedByMax = _sessionService.Current.StatisticsDataModel.GroupedByMax,
                NumberOfRows = _sessionService.Current.StatisticsDataModel.NumberOfRows,
                MaxProductLevel = _sessionService.Current.StatisticsDataModel.MaxProductLevel,
                MaxLocationLevel = _sessionService.Current.StatisticsDataModel.MaxLocationLevel,
                MarginBridgeType = _sessionService.Current.MarginBridgeType
            };

            return model;
        }

        public List<HierarchySelectionLevel> GetProductHierarchySelectionLevels()
        {
            var result = new List<HierarchySelectionLevel>();

            var productHierarchyLocations = Enum.GetValues(typeof(ProductLevelType))
                .Cast<ProductLevelType>()
                .Except(new[] { ProductLevelType.AllProducts });

            foreach (var product in productHierarchyLocations)
            {
                result.Add(new HierarchySelectionLevel
                {
                    Value =(int)product,
                    Caption = GetEnumDescription(product)
                });
            }

            return result;
        }

        public List<HierarchySelectionLevel> GetProductHierarchySelectionLevelsWithAll()
        {
            var result = new List<HierarchySelectionLevel>();

            result = GetProductHierarchySelectionLevels();

            result.Insert(0, new HierarchySelectionLevel
            {
                Value = (int)ProductLevelType.AllProducts,
                Caption = _marginBridgeService.GetEnumDescription(ProductLevelType.AllProducts)
            });

            return result;
        }

        public List<HierarchySelectionLevel> GetLocationHierarchySelectionLevels()
        {
            var result = new List<HierarchySelectionLevel>();

            var locationHierarchyLocations = Enum.GetValues(typeof(LocationLevelType))
                .Cast<LocationLevelType>()
                .Except(new[] { LocationLevelType.AllLocations });

            foreach (var location in locationHierarchyLocations)
            {
                result.Add(new HierarchySelectionLevel
                {
                    Value = (int)location,
                    Caption = GetEnumDescription(location)
                });
            }

            return result;
        }

        public List<HierarchySelectionLevel> GetLocationHierarchySelectionLevelsWithAll()
        {
            var result = new List<HierarchySelectionLevel>();

            result = GetLocationHierarchySelectionLevels();

            result.Insert(0, new HierarchySelectionLevel
            {
                Value = (int)ProductLevelType.AllProducts,
                Caption = _marginBridgeService.GetEnumDescription(ProductLevelType.AllProducts)
            });

            return result;
        }

        public MarginBridgeIndexModel PrepareMarginBridgeIndexModel(MarginBridgeSession session)
        {
            var indexModel = new MarginBridgeIndexModel
            {
                LocationSelection = GetLocationHierarchySelectionLevels(),
                ProductSelection = GetProductHierarchySelectionLevels(),
                PeriodRanges = _periodRangeProvider.GetAll()
            };

            if (session.StatisticsDataModel != null)
            {
                indexModel.LocationLevel = session.StatisticsDataModel.MaxLocationLevel;
                indexModel.ProductLevel = session.StatisticsDataModel.MaxProductLevel;
                indexModel.PeriodId = session.StatisticsDataModel.PeriodId;
                indexModel.FirstPeriod = session.StatisticsDataModel.FirstPeriod;
                indexModel.SecondPeriod = session.StatisticsDataModel.SecondPeriod;
                indexModel.CheckedNodes = session.StatisticsDataModel.CheckedNodes;
                indexModel.IncludeNLItems = session.StatisticsDataModel.IncludeNLItems;
                indexModel.IncludeSDItems = session.StatisticsDataModel.IncludeSDItems;
                indexModel.IncludeListedItems = session.StatisticsDataModel.IncludeListedItems;
                indexModel.PricingLevels = session.StatisticsDataModel.PricingLevels;
                indexModel.MarginBridgeType = session.MarginBridgeType;
            }
            else
            {
                var defaultPeriodRange = _periodRangeProvider.GetDefault();
                indexModel.PeriodId = defaultPeriodRange.Id;
                indexModel.FirstPeriod = defaultPeriodRange.FirstPeriod;
                indexModel.SecondPeriod = defaultPeriodRange.SecondPeriod;
            }

            return indexModel;
        }

        public static string GetEnumDescription(Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        public List<DataBridgeItem> PrepareDataBridgeData(MarginBridgeDataModel model, int userId)
        {
            var filter = PrepareMarginBridgeFilterModel(model, userId);

            var result = _marginBridgeService.GetDataBridgeData(filter, model.FirstPeriod, model.SecondPeriod);

            return result;
        }

        public DataBridgeRawData PrepareDataBridgeRawData(MarginBridgeDataModel model, int userId)
        {
            var filter = PrepareMarginBridgeFilterModel(model, userId);

            var result = _marginBridgeService.GetDataBridgeRawData(filter, model.FirstPeriod, model.SecondPeriod);

            return result;
        }

        public List<ReportSalesHistoryModel> PrepareRawSalesHistory(int userId, MarginBridgePeriod period)
        {
            var model = new MarginBridgeDataModel
            {
                MarginBridgeType = _sessionService.Current.MarginBridgeType,
                LocationSelectionLevel = _sessionService.Current.StatisticsDataModel.LocationLevel,
                ProductSelectionLevel = _sessionService.Current.StatisticsDataModel.ProductLevel
            };

            var filter = PrepareMarginBridgeFilterModel(model, userId);

            var result = _marginBridgeService.GetRawSalesHistory(filter, period);

            return result;
        }

        #endregion Methods

        #region Util

        public MarginBridgeFilterModel PrepareMarginBridgeFilterModel(MarginBridgeDataModel dataModel, int userId)
        {
            ReportFilterModel filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            var model = new MarginBridgeFilterModel
            {
                ReportFitlerModel = filter,
                UserId = userId,
                LocationSelectionLevel = dataModel.LocationSelectionLevel,
                ProductSelectionLevel = dataModel.ProductSelectionLevel,
                MarginBridgeType = dataModel.MarginBridgeType
            };

            return model;
        }


        #endregion
    }
}