﻿using ProfitOptics.Modules.Reporting.Session.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Services.MarginBridge
{
    public interface IMarginBridgeSessionService
    {
        MarginBridgeSession Current { get; }

        bool DownloadFinished { get; set; }

        void Save(MarginBridgeSession session);
    }
}