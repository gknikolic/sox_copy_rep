﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

let MenuFilter = (function() {
    //.sidebar-menu - the ul containing the entire menu
    //#menu-filter-input - id of the input used for filtering

    let filterFunc = function(e) {

        let filter = $("#menu-filter-input").val();

        //exclude the "MAIN NAVIGATION" entry
        let menuEntryList = $("ul.sidebar-menu li").not(":first");

        for (let i = 0; i < menuEntryList.length; i++) {
            //no filter -> display top level nodes, attach bottom level nodes
            if (filter === "" || filter === undefined) {
                if (isTopLevelNode(menuEntryList[i])) {
                    showNode(menuEntryList[i]);
                    hideTopLevelNodeChildren(menuEntryList[i]);
                }
                if (isBottomLevelNode(menuEntryList[i])) {
                    showNode(menuEntryList[i]);
                }
                continue;
            }
            //filter top level nodes, with their children
            if (isTopLevelNode(menuEntryList[i])) {
                if (includesFilter(menuEntryList[i], filter)) {
                    showNode(menuEntryList[i]);
                }
                else {
                    hideNode(menuEntryList[i]);
                }
            }
            //filter bottom level nodes, display parent also
            if (isBottomLevelNode(menuEntryList[i])) {
                if (includesFilter(menuEntryList[i], filter))
                {
                    showTopLevelParent(menuEntryList[i].parentNode.parentNode);
                    showParent(menuEntryList[i].parentNode);
                    showNode(menuEntryList[i]);
                }
                else {
                    hideNode(menuEntryList[i]);
                }
                if (topLevelParentIncludesFilter(menuEntryList[i], filter)) {
                    showNode(menuEntryList[i]);
                }
            }
        }
    };

    let includesFilter = function(node, filter) {
        return node.children[0].innerText.toLowerCase().indexOf(filter.toLowerCase()) !== -1 ? true : false;
    };

    let topLevelParentIncludesFilter = function(node, filter) {
        return includesFilter(node.parentElement.parentElement, filter) ? true : false;
    };

    let isTopLevelNode = function(node) {
        return node.parentElement.className === "sidebar-menu" ? true : false;
    };

    let isBottomLevelNode = function(node) {
        return node.parentElement.className.indexOf("treeview-menu") !== -1 ? true : false;
    };

    let hideNode = function(node) {
        node.style.display = "none";
    };

    let showNode = function(node) {
        node.style.display = "list-item";
        if (node.className.indexOf("treeview active") !== -1) {
            node.className = "treeview";
        }
        if (node.className.indexOf("treeview-menu menu-open") !== -1) {
            node.className = "treeview-menu";
        }
    };

    let hideTopLevelNodeChildren = function(node) {
        for (let i = 0; i < node.children.length; i++) {
            if (node.children[i].className.indexOf("treeview-menu") !== -1) {
                node.children[i].style.display = "none";
                node.children[i].className = "treeview-menu";
            }
        }
    };

    let showTopLevelParent = function(node) {
        node.style.display = "list-item";
        node.className = "treeview active";
    };

    let showParent = function(node) {
        node.className = "treeview-menu menu-open";
        node.style.display = "block";
    };

    let autoCompleteInit = function() {
        let menuEntries = $("ul.sidebar-menu li > a");
        let autoCompleteEntries = [];

        $.each(menuEntries, function(index, val) {
            autoCompleteEntries.push(val.innerText.trim());
        });

        $("#menu-filter-input").autoComplete({
            minchars: 1,
            source: function (term, suggest) {
                term = term.toLowerCase();
                var choices = autoCompleteEntries;
                var matches = [];
                for (i = 0; i < choices.length; i++)
                    if (~choices[i].toLowerCase().indexOf(term)) {
                        matches.push(choices[i]);
                    }
                suggest(matches);
            }
        });
    };

    
    let init = function() {
        //we have to use keyup event, because we first need the key that was pressed to be added to the value of the input field. 
        //Keydown works as well, but does not get triggered by control keys.
        //So the keyup event was the best option to use.
        $("#menu-filter-input").keyup(filterFunc);
        autoCompleteInit();
    };
    return {
        Init: init
    };
})();