﻿namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportVendorQuota
    {
        public int Id { get; set; }

        public int SalesRepId { get; set; }

        public int VendorId { get; set; }

        public string Month { get; set; }

        public int Year { get; set; }

        public decimal Revenue { get; set; }

        public virtual ReportHierarchySalesRep ReportHierarchySalesRep { get; set; }

        public virtual ReportVendor ReportVendor { get; set; }
    }
}
