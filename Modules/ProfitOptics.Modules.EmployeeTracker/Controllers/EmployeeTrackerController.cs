﻿using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Modules.EmployeeTracker.Factories;

namespace ProfitOptics.Modules.EmployeeTracker.Controllers
{
    [Area("EmployeeTracker")]
    public class EmployeeTrackerController : BaseController
    {
        private readonly IEmployeeTrackerFactory _EmployeeTrackerFactory;

        public EmployeeTrackerController(IEmployeeTrackerFactory EmployeeTrackerFactory)
        {
            _EmployeeTrackerFactory = EmployeeTrackerFactory;
        }

        public IActionResult Index()
        {
            var model = _EmployeeTrackerFactory.PrepareEmptyModel();
            return View(model);
        }
    }
}