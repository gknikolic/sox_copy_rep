﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTInventoryConfig : IEntityTypeConfiguration<QTInventory>
    {
        public void Configure(EntityTypeBuilder<QTInventory> builder)
        {
            builder.ToTable("QTInventory");
            builder.HasKey(entity => new
            {
                entity.Company,
                entity.ItemNum,
                entity.Plant,
                entity.MinQty,
                entity.MaxQty,
                entity.ProdCode
            });

            builder.Property(entity => entity.Company).HasMaxLength(8);
            builder.Property(entity => entity.ItemNum).HasMaxLength(50);
            builder.Property(entity => entity.Plant).HasMaxLength(8);
            builder.Property(entity => entity.ProdCode).HasMaxLength(8);
        }
    }
}
