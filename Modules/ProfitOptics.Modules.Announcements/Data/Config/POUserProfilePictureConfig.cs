﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Announcements.Data.Domain;

namespace ProfitOptics.Modules.Announcements.Data.Config
{
    public class POUserProfilePictureConfig : IEntityTypeConfiguration<POUserProfilePicture>
    {
        public void Configure(EntityTypeBuilder<POUserProfilePicture> builder)
        {
            builder.HasKey(e => e.UserId);
            builder.ToTable("POUserProfilePicture");

            builder.Property(e => e.UserId).ValueGeneratedNever();

            builder.Property(e => e.Data).IsRequired();
        }
    }
}