﻿namespace ProfitOptics.Framework.Web.Framework.ServiceResults
{
    public class AuthenticationServiceResult : ServiceActionResult<AuthenticationServiceResultStatusCode>
    {
        public override bool Succeeded => Status == AuthenticationServiceResultStatusCode.Success;

        public static AuthenticationServiceResult Success() => new AuthenticationServiceResult
        {
            Error = null,
            Status = AuthenticationServiceResultStatusCode.Success
        };

        public static AuthenticationServiceResult UserNotFound() => new AuthenticationServiceResult
        {
            Error = null,
            Status = AuthenticationServiceResultStatusCode.UserNotFound
        };

        public static AuthenticationServiceResult EmailNotConfirmed() => new AuthenticationServiceResult
        {
            Error = null,
            Status = AuthenticationServiceResultStatusCode.EmailNotConfirmed
        };

        public static AuthenticationServiceResult Failed(string error) => new AuthenticationServiceResult
        {
            Error = error,
            Status = AuthenticationServiceResultStatusCode.EmailNotConfirmed
        };

        public static TClass UserNotFound<TClass>() where TClass : AuthenticationServiceResult, new() => new TClass
        {
            Error = null,
            Status = AuthenticationServiceResultStatusCode.UserNotFound
        };

        public static TClass EmailNotConfirmed<TClass>() where TClass: AuthenticationServiceResult, new() => new TClass
        {
            Error = null,
            Status = AuthenticationServiceResultStatusCode.EmailNotConfirmed
        };

        public static TClass Failed<TClass>(string error) where TClass : AuthenticationServiceResult, new() => new TClass
        {
            Error = error,
            Status = AuthenticationServiceResultStatusCode.Failed
        };
    }
}
