﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using ProfitOptics.Framework.Web.Models;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Services
{
    public class BreadcrumbService : IBreadcrumbService, IViewContextAware
    {
        public static bool enabled;
        IList<BreadcrumbModel> breadcrumbs;
        public BreadcrumbService(IConfiguration configuration)
        {
            var breadcrumbConfig = configuration.GetSection("Breadcrumbs");
            enabled = Convert.ToBoolean(breadcrumbConfig["Enabled"]);
        }

        public void Contextualize(ViewContext viewContext)
        {
            breadcrumbs = new List<BreadcrumbModel>();

            string area = $"{viewContext.RouteData.Values["area"]}";
            string controller = $"{viewContext.RouteData.Values["controller"]}";
            string action = $"{viewContext.RouteData.Values["action"]}";
            object id = viewContext.RouteData.Values["id"];
            string title = viewContext.ViewBag.Title;

            breadcrumbs.Add(new BreadcrumbModel(area, controller, action, title, id));
        }

        public IList<BreadcrumbModel> GetBreadcrumbs()
        {
            return breadcrumbs;
        }
    }
}
