﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.Web.Areas.Account.Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Data.Configs
{
    public class AspNetUsersConfig : IEntityTypeConfiguration<AspNetUsers>
    {
        public void Configure(EntityTypeBuilder<AspNetUsers> builder)
        {
            builder.ToTable(nameof(AspNetUsers));
            builder.HasKey(user => user.Id);

            builder.Property(user => user.FirstName).HasMaxLength(100);
            builder.Property(user => user.LastName).HasMaxLength(100);
            builder.Property(user => user.StartTime);
            builder.Property(user => user.EndTime);
            builder.Property(user => user.Approved);
            builder.Property(user => user.Email).HasMaxLength(256);
            builder.Property(user => user.EmailConfirmed);
            builder.Property(user => user.PasswordHash).HasMaxLength(255);
            builder.Property(user => user.SecurityStamp).HasMaxLength(255);
            builder.Property(user => user.PhoneNumber).HasMaxLength(50);
            builder.Property(user => user.PhoneNumberConfirmed);
            builder.Property(user => user.TwoFactorEnabled);
            builder.Property(user => user.LockoutEndDateUtc);
            builder.Property(user => user.LockoutEnabled);
            builder.Property(user => user.AccessFailedCount);
            builder.Property(user => user.UserName).HasMaxLength(256).IsRequired();
            builder.Property(user => user.IsGoogleAuthenticatorEnabled);
            builder.Property(user => user.GoogleAuthenticatorSecretKey);
            builder.Property(user => user.IsEnabled);
        }
    }
}
