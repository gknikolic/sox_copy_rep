﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportQuotaConfig : IEntityTypeConfiguration<ReportQuota>
    {
        public void Configure(EntityTypeBuilder<ReportQuota> builder)
        {
            builder.ToTable(nameof(ReportQuota));
            builder.HasKey(i => i.Id);

            builder.Property(x => x.Month).HasMaxLength(50).IsRequired();
            builder.Property(x => x.ItemCategoryName).HasMaxLength(50);
        }
    }
}
