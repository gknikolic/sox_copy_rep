﻿namespace ProfitOptics.Modules.Reporting.Models.CustomerReport
{
    public class CustomerReportTableModel
    {
        public string TableName { get; set; }
        public string TableId => TableName.ToLower().Replace(" ", "-");
        public string[] ColumnNames { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string AreaName { get; set; }
    }
}