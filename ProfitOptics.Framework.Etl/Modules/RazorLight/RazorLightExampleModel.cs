﻿using System;

namespace ProfitOptics.Framework.Etl.Modules.RazorLight
{
    public class RazorLightExampleModel
    {
        public string Vendor { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string FileName { get; set; }
        public string Exception { get; set; }
        public decimal? OrderAmount { get; set; }
        public string Reviewer { get; set; }
        public string Action { get; set; }
    }
}
