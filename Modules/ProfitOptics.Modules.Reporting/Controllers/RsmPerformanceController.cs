﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Modules.Reporting.Factories.RsmPerformance;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.RegionTable;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.SalesChannel;
using ProfitOptics.Modules.Reporting.Services;
using ProfitOptics.Modules.Reporting.Services.RsmPerformance;

namespace ProfitOptics.Modules.Reporting.Controllers
{
    [Authorize(Roles = "admin, reporting")]
    public class RsmPerformanceController : BaseReportingController
    {
        private const int CacheDurationInSeconds = 60 * 30; // seconds * minutes
        private readonly IUserHierarchyService _userHierarchyService;
        private readonly IUserMappingService _userMappingService;
        private readonly IRsmPerformanceModelFactory _rsmPerformanceModelFactory;
        private readonly IDaysOffService _daysOffService;
        private readonly IRsmPerformanceService _rsmPerformanceService;

        public RsmPerformanceController(IUserHierarchyService userHierarchyService,
            IUserMappingService userMappingService,
            IRsmPerformanceModelFactory rsmPerformanceModelFactory,
            IDaysOffService daysOffService,
            IRsmPerformanceService rsmPerformanceService)
        {
            _userHierarchyService = userHierarchyService;
            _userMappingService = userMappingService;
            _rsmPerformanceModelFactory = rsmPerformanceModelFactory;
            _daysOffService = daysOffService;
            _rsmPerformanceService = rsmPerformanceService;
        }

        public IActionResult Index(int year = 0, int month = 0)
        {
            _rsmPerformanceService.CheckYearAndMonth(ref year, ref month);

            var userId = GetCurrentUserId();
            if (_userHierarchyService.IsSalesRepUser(userId))
            {
                return RedirectToAction("SalesRepPerformance",
                    new { salesRepId = _userMappingService.GetReportUserMappingForUserId(userId).SalesRepId ?? 0 });
            }

            var model = new RsmPerformanceViewModel
            {
                Year = year,
                Month = month,
                UserRegionId = _userHierarchyService.GetUserRegionIdIfExistsForCurrentUser()
            };

            return View("Index", model);
        }

        public IActionResult SalesRepPerformance(int salesRepId, int year = 0, int month = 0)
        {
            _rsmPerformanceService.CheckYearAndMonth(ref year, ref month);

            if (!_userHierarchyService.CanUserViewDataForSalesRep(GetCurrentUserId(), salesRepId))
            {
                return RedirectToAction(nameof(Index));
            }

            RsmPerformanceSalesRepDataModel dataModel = _rsmPerformanceModelFactory.GetSalesRepPerformanceData(salesRepId, year, month);

            var viewModel = new RsmPerformanceSalesRepViewModel
            {
                Year = year,
                Month = month,
                SalesRepId = salesRepId,
                SalesRepName = _userHierarchyService.GetSalesRepNameById(salesRepId),
                ItemCategoryPerformance = dataModel.ItemCategoryPerformance,
                ProductFamilyPerformance = dataModel.ProductFamilyPerformance,
                SalesChannelPerformance = dataModel.SalesChannelPerformance,
                RegionTableViewModel = new RegionTablePartialViewModel
                {
                    RegionQuotaPerformanceData = dataModel.RegionQuotaPerformance,
                    CurrentMonth = CultureInfo.InvariantCulture.DateTimeFormat.GetMonthName(month),
                    CurrentQuarter = _daysOffService.GetQuarterNumber(month),
                    CurrentYear = year,
                    PreviousYear = year - 1
                },
                MonthName = _daysOffService.GetMonthName(month),
                RemainingWorkDaysCount = _daysOffService.GetRemainingWorkDays(year, month).Count()
            };

            return View(viewModel);
        }

        #region Partial Views

        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = CacheDurationInSeconds, VaryByQueryKeys = new[] { "*" })]
        public async Task<IActionResult> RegionTablePartial(int year = 0, int month = 0)
        {
            _rsmPerformanceService.CheckYearAndMonth(ref year, ref month);
            RegionTablePartialViewModel model;
            try
            {
                model = new RegionTablePartialViewModel
                {
                    RegionQuotaPerformanceData = await _rsmPerformanceModelFactory.GetRegionTableDataAsync(year, month),
                    CurrentMonth = CultureInfo.InvariantCulture.DateTimeFormat.GetMonthName(month),
                    CurrentQuarter = _daysOffService.GetQuarterNumber(month),
                    CurrentYear = year,
                    PreviousYear = year - 1
                };
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);
                throw;
            }

            return PartialView("_RegionPerformanceTablePartial", model);
        }

        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = CacheDurationInSeconds, VaryByQueryKeys = new[] { "*" })]
        public async Task<IActionResult> ChartsPartial(int year = 0, int month = 0, int? regionId = null)
        {
            _rsmPerformanceService.CheckYearAndMonth(ref year, ref month);
            ChartsPartialViewModel model;
            try
            {
                model = new ChartsPartialViewModel
                {
                    Year = year,
                    SalesChannelPerformance =
                        await _rsmPerformanceModelFactory.GetSalesChannelChartDataAsync(year, month, regionId: regionId),
                    ProductFamilyPerformance =
                        await _rsmPerformanceModelFactory.GetProductFamilyChartDataAsync(year, month, regionId: regionId)
                };
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);
                throw;
            }

            return PartialView("_ChartsPartial", model);
        }

        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = CacheDurationInSeconds, VaryByQueryKeys = new[] { "*" })]
        public async Task<IActionResult> AggregateSalesChannelTablePartial(int year = 0, int month = 0, int? regionId = null)
        {
            _rsmPerformanceService.CheckYearAndMonth(ref year, ref month);
            AggregateSalesChannelTablePartialViewModel model;
            try
            {
                model = new AggregateSalesChannelTablePartialViewModel
                {
                    Year = year,
                    Month = month,
                    MonthName = _daysOffService.GetMonthName(month),
                    RemainingWorkDaysCount = _daysOffService.GetRemainingWorkDays(year, month).Count(),
                    SalesChannels =
                        await _rsmPerformanceModelFactory.GetAggregatePerformanceForSalesChannelAsync(year, month,
                            regionId: regionId)
                };
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);
                throw;
            }

            return PartialView("_AggregateSalesChannelTablePartial", model);
        }

        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = CacheDurationInSeconds, VaryByQueryKeys = new[] { "*" })]
        public async Task<IActionResult> AggregateProductFamilyTablePartial(int year = 0, int month = 0, int? regionId = null)
        {
            _rsmPerformanceService.CheckYearAndMonth(ref year, ref month);
            AggregateProductFamilyTablePartialViewModel model;
            try
            {
                model = new AggregateProductFamilyTablePartialViewModel
                {
                    Year = year,
                    Month = month,
                    ProductFamilies = await _rsmPerformanceModelFactory.GetAggregatePerformanceForProductFamilyAsync(year, month, regionId: regionId)
                };
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);
                throw;
            }

            return PartialView("_AggregateProductFamilyTablePartial", model);
        }

        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = CacheDurationInSeconds, VaryByQueryKeys = new[] { "*" })]
        public async Task<IActionResult> AggregateItemCategoryTablePartial(int year = 0, int month = 0, int? regionId = null)
        {
            _rsmPerformanceService.CheckYearAndMonth(ref year, ref month);
            AggregateItemCategoryTablePartialViewModel model;
            try
            {
                model = new AggregateItemCategoryTablePartialViewModel
                {
                    Year = year,
                    Month = month,
                    ItemCategories = await _rsmPerformanceModelFactory.GetAggregatePerformanceForItemCategoryAsync(year, month, regionId: regionId)
                };
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);
                throw;
            }

            return PartialView("_AggregateItemCategoryTablePartial", model);
        }

        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = CacheDurationInSeconds, VaryByQueryKeys = new[] { "*" })]
        public async Task<IActionResult> SalesRepSalesChannelPartial(int year = 0, int month = 0, int? regionId = null)
        {
            _rsmPerformanceService.CheckYearAndMonth(ref year, ref month);
            SalesRepSalesChannelTablePartialViewModel model;
            try
            {
                model = new SalesRepSalesChannelTablePartialViewModel
                {
                    Year = year,
                    Month = month,
                    MonthName = _daysOffService.GetMonthName(month),
                    RemainingWorkDaysCount = _daysOffService.GetRemainingWorkDays(year, month).Count(),
                    SalesChannels = await _rsmPerformanceModelFactory.GetSalesRepSalesChannelPerformanceAsync(year, month, regionId: regionId)
                };
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);
                throw;
            }

            return PartialView("_SalesRepSalesChannelTablePartial", model);
        }

        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = CacheDurationInSeconds, VaryByQueryKeys = new[] { "*" })]
        public async Task<IActionResult> SalesRepProductFamilyPartial(int year = 0, int month = 0, int? regionId = null)
        {
            _rsmPerformanceService.CheckYearAndMonth(ref year, ref month);
            SalesRepProductFamilyTablePartialViewModel model;
            try
            {
                model = new SalesRepProductFamilyTablePartialViewModel
                {
                    Year = year,
                    Month = month,
                    ProductFamilies = await _rsmPerformanceModelFactory.GetSalesRepProductFamilyPerformanceAsync(year, month, regionId: regionId)
                };
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);
                throw;
            }

            return PartialView("_SalesRepProductFamilyTablePartial", model);
        }

        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = CacheDurationInSeconds,
            VaryByQueryKeys = new[] { "*" })]
        public async Task<IActionResult> SalesRepItemCategoryPartial(int year = 0, int month = 0, int? regionId = null)
        {
            _rsmPerformanceService.CheckYearAndMonth(ref year, ref month);
            SalesRepItemCategoryTablePartialViewModel model;
            try
            {
                model = new SalesRepItemCategoryTablePartialViewModel
                {
                    Year = year,
                    Month = month,
                    ItemCategories =
                        await _rsmPerformanceModelFactory.GetSalesRepItemCategoryPerformanceAsync(year, month,
                            regionId: regionId)
                };
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);
                throw;
            }

            return PartialView("_SalesRepItemCategoryTablePartial", model);
        }

        #endregion Partial Views
    }
}