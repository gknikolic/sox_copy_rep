﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.BackgroundTasks.Components;
using ProfitOptics.Modules.BackgroundTasks.Data;
using ProfitOptics.Modules.BackgroundTasks.Factories;
using ProfitOptics.Modules.BackgroundTasks.Services;

namespace ProfitOptics.Modules.BackgroundTasks.Infrastructure
{
    public sealed class BackgroundTaskServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options =>
                options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<IBackgroundTaskService, BackgroundTaskService>();
            services.AddScoped<IBackgroundTaskFactory, BackgroundTaskFactory>();
            services.AddScoped<IHeaderWidget, TasksViewComponent>();
        }
    }
}