﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.Announcements.Components;
using ProfitOptics.Modules.Announcements.Data;
using ProfitOptics.Modules.Announcements.Factories;
using ProfitOptics.Modules.Announcements.Services;

namespace ProfitOptics.Modules.Announcements.Infrastructure
{
    public sealed class AnnouncementServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<IAnnouncementService, AnnouncementService>();
            services.AddScoped<IAnnouncementModelFactory, AnnouncementModelFactory>();
            services.AddScoped<IHeaderWidget, AnnouncementViewComponent>();
        }
    }
}