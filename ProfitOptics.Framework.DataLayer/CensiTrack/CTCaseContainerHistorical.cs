﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTCaseContainerHistorical
    {
        public int Id { get; set; }
        public int CTCaseContainerId { get; set; }
        public int CTCaseId { get; set; }
        public int? CTContainerTypeId { get; set; }
        public long? ContainerId { get; set; }
        public long ContainerTypeId { get; set; }
        public string CaseReference { get; set; }
        public long CaseCartId { get; set; }
        public string CaseCartName { get; set; }
        public long DueMinutes { get; set; }
        public int? CaseTime { get; set; }
        public DateTime DueTimestamp { get; set; }
        public string DestinationName { get; set; }
        public DateTime? EndTimestamp { get; set; }
        public string CaseDoctor { get; set; }
        public string ContainerName { get; set; }
        public string ItemName { get; set; }
        public string Status { get; set; }
        public string ScheduleStatusCode { get; set; }
        public string ScheduleStatus { get; set; }
        public int? StandardTime { get; set; }
        public int? ExpediteTime { get; set; }
        public string ItemCode { get; set; }
        public string ItemType { get; set; }
        public string ItemTypeName { get; set; }
        public int? StatTime { get; set; }
        public DateTime? MultipleBasis { get; set; }
        public string LocationName { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public string ScheduleCartChanged { get; set; }
        public bool IsModified { get; set; }
        public decimal HoldQuantity { get; set; }
        public long ItemQuantity { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsCartComplete { get; set; }
        public bool Shipped { get; set; }
        public string CaseStatus { get; set; }
        public string StandardTimeInHoursAndMinutes { get; set; }
        public string ExpediteTimeInHoursAndMinutes { get; set; }
        public string StatTimeInHoursAndMinutes { get; set; }
        public decimal DueInMinutes { get; set; }
        public string DueInHoursAndMinutes { get; set; }
        public long? ProcessingFacilityId { get; set; }
        public long? LocationFacilityId { get; set; }
        public long CaseItemNumber { get; set; }
        public long CaseContainerStatus { get; set; }
        public string CurrentLocationName { get; set; }
        public string SerialNumber { get; set; }
        public string VendorName { get; set; }
        public DateTime CreatedAtUtc { get; set; }

        public DateTime DueTimestampOriginal { get; set; }
        public DateTime? EndTimestampOriginal { get; set; }
        public DateTime? UpdateTimestampOriginal { get; set; }
        public DateTime? MultipleBasisOriginal { get; set; }
    }
}
