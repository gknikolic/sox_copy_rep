﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCVendorConfig : IEntityTypeConfiguration<GCVendor>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCVendor> builder)
        {
            builder.ToTable("GCVendor");
            builder.HasKey(entity => entity.Id);
        }
    }
}
