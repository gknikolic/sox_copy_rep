﻿using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.DailyTrend
{
    public class DailyTrendDrillThroughByTypeViewModel
    {
        public bool IsCorporate { get; set; }

        public string DrillThroughDate { get; set; }

        public DailyTrendDrillThroughType DrillThroughType { get; set; }
    }
}
