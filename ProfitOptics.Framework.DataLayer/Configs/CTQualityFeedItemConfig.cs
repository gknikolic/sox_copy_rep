﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTQualityFeedItemConfig : IEntityTypeConfiguration<CTQualityFeedItem>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTQualityFeedItem> builder)
        {
            builder.ToTable("CTQualityFeedItem");
            builder.HasKey(entity => entity.Id);
        }
    }
}
