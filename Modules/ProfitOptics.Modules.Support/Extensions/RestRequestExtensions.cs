﻿using RestSharp;

namespace ProfitOptics.Modules.Support.Extensions
{
    public static class RestRequestExtensions
    {
        private const string ApplicationJson = "application/json";

        public static IRestRequest ContentTypeApplicationJson(this IRestRequest restRequest)
        {
            return restRequest.AddHeader("Content-Type", ApplicationJson);
        }

        public static IRestRequest AcceptApplicationJson(this IRestRequest restRequest)
        {
            return restRequest.AddHeader("Accept", ApplicationJson);
        }

        public static IRestRequest AddJsonParameter(this IRestRequest restRequest, string jsonString, ParameterType parameterType)
        {
            return restRequest.AddParameter(ApplicationJson, jsonString, parameterType);
        }
    }
}
