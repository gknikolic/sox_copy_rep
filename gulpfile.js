/// <binding ProjectOpened='default' />
const gulp = require('gulp');
const del = require('del');
const shadowCopy = '.\\ProfitOptics.Framework.Web\\'

function logEntry(message) {
    let current_datetime = new Date();
    let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds();
    console.log(`[\x1b[32m${formatted_date}\x1b[0m] ${message}`);
}

function gulpCopy(srcPath) {
    let destinationPath = `${shadowCopy}${srcPath}`;    
    gulp.src("./" + srcPath)
        .pipe(gulp.dest(shadowCopy));   
    logEntry(`File copied to ${destinationPath}`);
}


function gulpDelete(srcPath) {
    let destinationPath = `${shadowCopy}${srcPath}`;
    del(destinationPath);
    logEntry(`File removed from ${destinationPath}`);
}

gulp.task('watch', () => {
    var viewWatcher = gulp.watch(["./Modules/**/*.cshtml"]);
    viewWatcher.on('change', gulpCopy);
    viewWatcher.on('add', gulpCopy);
    viewWatcher.on('unlink', gulpDelete);

    var contentWatcher = gulp.watch(["./Modules/*/Content/**"]);
    contentWatcher.on('change', gulpCopy);
    contentWatcher.on('add', gulpCopy);
    contentWatcher.on('unlink', gulpDelete);    
})

gulp.task('default', gulp.series(['watch']));