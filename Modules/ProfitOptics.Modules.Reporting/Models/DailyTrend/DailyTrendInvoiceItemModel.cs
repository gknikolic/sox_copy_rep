﻿namespace ProfitOptics.Modules.Reporting.Models.DailyTrend
{
    public class DailyTrendInvoiceItemModel
    {
        public string ItemNumber { get; set; }

        public string ItemDescription { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal UnitCost { get; set; }

        public decimal ExtPrice { get; set; }

        public decimal ExtCost { get; set; }

        public decimal Qty { get; set; }

        public decimal GrossProfitPercent { get; set; }
    }
}
