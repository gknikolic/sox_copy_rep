﻿(function (upsellItem, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    // Public properties
    upsellItem.somePublicProperty = {};

    // Public Methods
    upsellItem.somePublicMethod = function () {
    };

    // Private Methods
    function somePrivateMethod(item) {
    }
}(window.upsellItem = window.upsellItem || {}, jQuery));