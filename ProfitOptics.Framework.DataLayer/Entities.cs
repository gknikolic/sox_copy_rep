﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer.Configs;
using ProfitOptics.Framework.DataLayer.GroundControl;
using ProfitOptics.Framework.DataLayer.Test;
using ProfitOptics.Framework.DataLayer.WorkWave;
using System.Reflection.Metadata;

namespace ProfitOptics.Framework.DataLayer
{
    public class Entities : DbContext
    {
        public Entities(DbContextOptions<Entities> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<ETLLogs> ETLLogs { get; set; }
        public virtual DbSet<POActionAudit> POActionAudits { get; set; }
        public virtual DbSet<POBackgroundTask> POBackgroundTasks { get; set; }
        public virtual DbSet<POEmail> POEmails { get; set; }
        public virtual DbSet<POEmailAttachment> POEmailAttachment { get; set; }
        public virtual DbSet<POHelpArticle> POHelpArticles { get; set; }
        public virtual DbSet<POSetting> POSettings { get; set; }

        public virtual DbSet<CTCase> CTCases { get; set; }
        public virtual DbSet<CTCaseContainer> CTCaseContainers { get; set; }
        public virtual DbSet<CTFacility> CTFacilities { get; set; }
        public virtual DbSet<CTContainerType> CTContainerTypes { get; set; }
        public virtual DbSet<CTContainerTypeActual> CTContainerTypeActuals { get; set; }
        public virtual DbSet<CTVendor> CTVendors { get; set; }
        public virtual DbSet<CTProduct> CTProducts { get; set; }
        public virtual DbSet<CTContainerItem> CTContainerItems { get; set; }
        public virtual DbSet<CTLocationType> CTLocationTypes { get; set; }
        public virtual DbSet<CTLocation> CTLocations { get; set; }
        public virtual DbSet<CTContainerService> CTContainerServices { get; set; }
        public virtual DbSet<CTContainerTypeActualHistoryItem> CTContainerTypeActualHistoryItems { get; set; }
        public virtual DbSet<CTContainerTypeGraphic> CTContainerTypeGraphics { get; set; }
        public virtual DbSet<CTBILoadResult> CTBILoadResults { get; set; }
        public virtual DbSet<CTBILoadResultContent> CTBILoadResultContents { get; set; }
        public virtual DbSet<CTBILoadResultIndicator> CTBILoadResultIndicators { get; set; }
        public virtual DbSet<CTBILoadResultGraphic> CTBILoadResultGraphics { get; set; }
        public virtual DbSet<CTCaseContainerHistorical> CTCaseContainerHistoricals { get; set; }
        public virtual DbSet<CTCaseHistorical> CTCaseHistoricals { get; set; }
        public virtual DbSet<CTQualityFeedItem> CTQualityFeedItems { get; set; }
        public virtual DbSet<CTQualityFeedItemGraphic> CTQualityFeedItemGraphics { get; set; }

        public virtual DbSet<GCCase> GCCases { get; set; }
        public virtual DbSet<GCSet> GCSets { get; set; }
        public virtual DbSet<GCContainerType> GCContainerTypes { get; set; }
        public virtual DbSet<GCContainer> GCContainers { get; set; }
        public virtual DbSet<GCProductType> GCProductTypes { get; set; }
        public virtual DbSet<GCProduct> GCProducts { get; set; }
        public virtual DbSet<GCCustomer> GCCustomers { get; set; }
        public virtual DbSet<GCSettings> GCSettings { get; set; }
        public virtual DbSet<GCTray> GCTrays { get; set; }
        public virtual DbSet<GCRequiredActionType> GCRequiredActionTypes { get; set; }
        public virtual DbSet<GCTraysGCRequiredActionTypes> GCTrayGCRequiredActionTypes { get; set; }
        public virtual DbSet<GCVendor> GCVendors { get; set; }
        public virtual DbSet<GCCaseStatus> GCCaseStatuses { get; set; }
        public virtual DbSet<GCLifecycleStatusType> GCLifecycleStatusTypes { get; set; }
        public virtual DbSet<GCCasesLifecycleStatusTypes> GCCasesLifecycleStatusTypes { get; set; }
        public virtual DbSet<GCTraysLifecycleStatusTypes> GCTraysLifecycleStatusTypes { get; set; }
        public virtual DbSet<CTLocationTypeGCLifecycleStatusType> CTLocationTypeGCLifecycleStatusTypes { get; set; }
        public virtual DbSet<GCCasesGCTrays> GCCasesGCTrays { get; set; }
        public virtual DbSet<GCColorStatusType> GCColorStatusTypes { get; set; }
        public virtual DbSet<GCTrayColorStatus> GCTrayColorStatuses { get; set; }
        public virtual DbSet<GCCaseColorStatus> GCCaseColorStatuses { get; set; }
        public virtual DbSet<GCTrayColorStatusNote> GCTrayColorStatusNotes { get; set; }
        public virtual DbSet<GCEstimate> GCEstimates { get; set; }
        public virtual DbSet<GCEstimateLine> GCEstimateLines { get; set; }
        public virtual DbSet<GCInvoice> GCInvoices { get; set; }
        public virtual DbSet<GCInvoiceLine> GCInvoiceLines { get; set; }
        public virtual DbSet<GCImage> GCImages { get; set; }
        public virtual DbSet<GCLocation> GCLocations { get; set; }
        public virtual DbSet<GCLocationInfo> GCLocationInfos { get; set; }
        public virtual DbSet<GCProcedureType> GCProcedureTypes { get; set; }
        public virtual DbSet<GCTrayActualHistory> GCTrayActualHistorys { get; set; }
        public virtual DbSet<GCParentCompany> GCParentCompanies { get; set; }
        public virtual DbSet<GCChildCompany> GCChildCompanies { get; set; }
        public virtual DbSet<GCSystemAlertType> GCSystemAlertTypes { get; set; }
        public virtual DbSet<GCSystemAlert> GCSystemAlerts { get; set; }

        public virtual DbSet<WWRoute> WWRoutes { get; set; }
        public virtual DbSet<WWSettings> WWSettings { get; set; }
        public virtual DbSet<WWDriver> WWDrivers { get; set; }
        public virtual DbSet<WWVehicle> WWVehicles { get; set; }
        public virtual DbSet<WWRouteStep> WWRouteSteps { get; set; }
        public virtual DbSet<WWTrackingData> WWTrackingData { get; set; }
        public virtual DbSet<WWPodContainer> WWPodContainers { get; set; }
        public virtual DbSet<WWPod> WWPods { get; set; }
        public virtual DbSet<WWPicture> WWPictures { get; set; }
        public virtual DbSet<WWSignature> WWSignatures { get; set; }
        public virtual DbSet<GCLifecycleStatusTypeCategory> GCLifecycleStatusTypeCategories { get; set; }

        public virtual DbSet<WWBarcode> WWBarcodes { get; set; }
        public virtual DbSet<CTAssemblyGraphic> CTAssemblyGraphics { get; set; }
        public virtual DbSet<CTAssemblyCountSheet> CTAssemblyCountSheets { get; set; }
        public virtual DbSet<WWGPSDevice> WWGPSDevices { get; set; }
        public virtual DbSet<WWGPSDeviceSample> WWGPSDeviceSamples { get; set; }
        public virtual DbSet<TManager> TManagers { get; set; }
        public virtual DbSet<TEmployee> TEmployees { get; set; }
        public virtual DbSet<TSkill> TSkills { get; set; }
        public virtual DbSet<TEmployeeSkills> TEmployeeSkills { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.ApplyConfiguration(new GCCaseConfig());
            modelBuilder.ApplyConfiguration(new GCSetConfig());
            modelBuilder.ApplyConfiguration(new GCContainerTypeConfig());
            modelBuilder.ApplyConfiguration(new GCContainerConfig());
            modelBuilder.ApplyConfiguration(new GCProductTypeConfig());
            modelBuilder.ApplyConfiguration(new GCProductConfig());
            modelBuilder.ApplyConfiguration(new GCCustomerConfig());
            modelBuilder.ApplyConfiguration(new GCSettingsConfig());
            modelBuilder.ApplyConfiguration(new CTCaseConfig());
            modelBuilder.ApplyConfiguration(new CTCaseContainerConfig());
            modelBuilder.ApplyConfiguration(new CTContainerItemConfig());
            modelBuilder.ApplyConfiguration(new CTContainerTypeConfig());
            modelBuilder.ApplyConfiguration(new CTContainerTypeActualConfig());
            modelBuilder.ApplyConfiguration(new CTFacilityConfig());
            modelBuilder.ApplyConfiguration(new CTLocationTypeConfig());
            modelBuilder.ApplyConfiguration(new CTLocationConfig());
            modelBuilder.ApplyConfiguration(new CTProductConfig());
            modelBuilder.ApplyConfiguration(new CTVendorConfig());
            modelBuilder.ApplyConfiguration(new CTContainerServiceConfig());
            modelBuilder.ApplyConfiguration(new GCRequiredActionTypeConfig());
            modelBuilder.ApplyConfiguration(new GCTraysGCRequiredActionTypesConfig());
            modelBuilder.ApplyConfiguration(new GCVendorConfig());
            modelBuilder.ApplyConfiguration(new GCCaseConfig());
            modelBuilder.ApplyConfiguration(new GCTrayConfig());
            modelBuilder.ApplyConfiguration(new GCCaseStatusConfig());
            modelBuilder.ApplyConfiguration(new LifecycleStatusTypeConfig());
            modelBuilder.ApplyConfiguration(new CTLocationTypeGCLifecycleStatusTypeConfig());
            modelBuilder.ApplyConfiguration(new LifecycleStatusTypeCategoryConfig());
            modelBuilder.ApplyConfiguration(new GCColorStatusTypeConfig());
            modelBuilder.ApplyConfiguration(new GCTrayColorStatusConfig());
            modelBuilder.ApplyConfiguration(new GCCaseColorStatusConfig());
            modelBuilder.ApplyConfiguration(new GCTrayColorStatusNoteConfig());
            modelBuilder.ApplyConfiguration(new GCEstimateConfig());
            modelBuilder.ApplyConfiguration(new GCEstimateLineConfig());
            modelBuilder.ApplyConfiguration(new GCInvoiceConfig());
            modelBuilder.ApplyConfiguration(new GCInvoiceLineConfig());
            modelBuilder.ApplyConfiguration(new CTContainerTypeActualHistoryItemConfig());
            modelBuilder.ApplyConfiguration(new CTContainerTypeGraphicConfig());
            modelBuilder.ApplyConfiguration(new GCImageConfig());
            modelBuilder.ApplyConfiguration(new CTBILoadResultConfig());
            modelBuilder.ApplyConfiguration(new CTBILoadResultContentConfig());
            modelBuilder.ApplyConfiguration(new CTBILoadResultIndicatorConfig());
            modelBuilder.ApplyConfiguration(new CTBILoadResultGraphicConfig());
            modelBuilder.ApplyConfiguration(new GCProcedureTypeConfig());
            modelBuilder.ApplyConfiguration(new CTCaseContainerHistoricalConfig());
            modelBuilder.ApplyConfiguration(new CTCaseHistoricalConfig());
            modelBuilder.ApplyConfiguration(new GCTrayActualHistoryConfig());
            modelBuilder.ApplyConfiguration(new GCParentCompanyConfig());
            modelBuilder.ApplyConfiguration(new GCChildCompanyConfig());
            modelBuilder.ApplyConfiguration(new GCSystemAlertTypeConfig());
            modelBuilder.ApplyConfiguration(new GCSystemAlertConfig());
            modelBuilder.ApplyConfiguration(new CTQualityFeedItemConfig());
            modelBuilder.ApplyConfiguration(new CTQualityFeedItemGraphicConfig());
            modelBuilder.ApplyConfiguration(new TManagerConfig());
            modelBuilder.ApplyConfiguration(new TEmployeeConfig());
            modelBuilder.ApplyConfiguration(new TSkillConfig());
            modelBuilder.ApplyConfiguration(new TEmployeeSkillsConfig());

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.UserName)
                    .HasName("UserNameIndex")
                    .IsUnique();

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.LockoutEndDateUtc).HasColumnType("datetime");

                entity.Property(e => e.PasswordHash).HasMaxLength(255);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.SecurityStamp).HasMaxLength(255);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            modelBuilder.Entity<ETLLogs>(entity =>
            {
                entity.ToTable("ETLLog");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ProcessingTime).HasColumnType("datetime");

                entity.Property(e => e.Source)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);
            });


            modelBuilder.Entity<POActionAudit>(entity =>
            {
                entity.ToTable("POActionAudit");

                entity.Property(e => e.Action)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Area)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Browser)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Controller)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.IpAddress)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReflectedActionName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReflectedType)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            
            modelBuilder.Entity<POBackgroundTask>(entity =>
            {
                entity.ToTable("POBackgroundTask");

                entity.Property(e => e.DateEnd).HasColumnType("datetime");

                entity.Property(e => e.DateStart).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Progress).HasColumnType("decimal(19, 4)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PobackgroundTask)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_POBackgroundTask_AspNetUsers");
            });

            modelBuilder.Entity<POEmail>(entity =>
            {
                entity.ToTable("POEmail");

                entity.Property(e => e.Body).IsRequired();

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.DontSendBefore).HasColumnType("datetime");

                entity.Property(e => e.SentTime).HasColumnType("datetime");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.To)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<POEmailAttachment>(entity =>
            {
                entity.ToTable("POEmailAttachment");

                entity.Property(e => e.Disposition)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceFileName)
                    .IsRequired()
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Email)
                    .WithMany(p => p.POEmailAttachments)
                    .HasForeignKey(d => d.EmailId)
                    .HasConstraintName("FK_POEmailAttachment_POEmail");
            });

            modelBuilder.Entity<POHelpArticle>(entity =>
            {
                entity.ToTable("POHelpArticle");

                entity.Property(e => e.Content).IsRequired();

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RelatedUrls).IsRequired();

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<POSetting>(entity =>
            {
                entity.ToTable("POSettings");
                entity.HasKey(e => e.SettingKey);
                entity.Property(e => e.SettingKey).IsRequired().HasMaxLength(50);
                entity.Property(e => e.Value).IsRequired();
                entity.Property(e => e.LastChangedBy).IsRequired().HasMaxLength(50);
                entity.Property(e => e.LastChangedTime).IsRequired().HasColumnType("datetime");
            });
        }
    }
}
