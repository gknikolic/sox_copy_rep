﻿using Flurl.Http;
using Newtonsoft.Json;
using ProfitOptics.Framework.Core.Flurl;
using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Models.WorkWave;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ProfitOptics.Framework.DataLayer.WorkWave;
using System.Globalization;
using Flurl;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Models.WorkWave.Enums;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Modules.Sox.Services;
using Serilog;
using System.IO;
using System.Drawing;

namespace ProfitOptics.Modules.Sox.Infrastructure.Clients
{
	public class WorkWaveClient : IWorkWaveClient
	{
		private readonly Framework.DataLayer.Entities _context;
		public string Url { get; set; } = "";
		public string Token { get; set; } = "";
		public string ApiKey { get; set; }
		public HttpRequestClient HttpRequestClient { get; set; }
		private readonly ICaseManagementClient _caseManagementClient;
		private readonly WorkWaveOptions _workWaveOptions;
		//private readonly IGroundControlService _groundControlService;

		public WorkWaveClient(Framework.DataLayer.Entities context, ICaseManagementClient caseManagementClient, WorkWaveOptions workWaveOptions)
		{
			_context = context;
			_caseManagementClient = caseManagementClient;
			_workWaveOptions = workWaveOptions;
			//_groundControlService = groundControlService;
			this.Url = _workWaveOptions.BaseUrl;
			this.Token = _workWaveOptions.Token;
			this.ApiKey = _workWaveOptions.ApiKey;
			this.HttpRequestClient = new HttpRequestClient(Url, Token, ApiKey);
		}

		public async Task<WorkWaveResponse> AddOrder(AddOrdersModel order)
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "orders")
					.PostAsync<WorkWaveResponse>(order);
				return response;
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}

		}

		public async Task<ListOrders> GetOrdersFromWWAsync()
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "orders")
					.GetAsync<ListOrders>();
				return response;
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}

		}

		public async Task<ListOrders> GetOrderByIdFromWWAsync(string orderId)
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "orders").SetQueryParams(new { ids = orderId })
					.GetAsync<ListOrders>();
				return response;
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}

		}

		public async Task<ListOrders> GetOrdersByIdFromWWAsync(List<string> orderIds)
		{
			var ids = String.Join(",", orderIds);
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "orders").SetQueryParams(new { ids = ids })
					.GetAsync<ListOrders>();
				return response;
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}

		}

		public async Task<WorkWaveResponse> ReplaceOrders(AddOrdersModel orders)
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "orders")
					.PutAsync<WorkWaveResponse>(orders);
				return response;
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}

		}

		public async Task DeleteOrdersInWW(List<string> orderIds)
		{
			string ids = String.Join(',', orderIds);
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				//var pathSetments = ["api", "v1", "territories", settings.TerritoryId, "orders"];
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "orders").SetQueryParams(new { ids = ids })
					.DeleteAsync();
				
				//_groundControlService.DeleteOrdersInWW()
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}

		}

		public async Task<WorkWaveResponse> BuildRoutes(string teritoryId, DateTime from, DateTime to)
		{
			var model = new BuildRoutesModel()
			{
				From = from.ToString("yyyyMMdd"),
				To = to.ToString("yyyyMMdd")
			};
			var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "teritories", teritoryId, "scheduling", "buildroutes")
				.PostAsync<WorkWaveResponse>(model);
			return response;
		}

		public async Task HandleWorkwaweResponseAsync(CallbackResponse response)
		{
			switch (response.Event)
			{
				case "ordersChanged":
					HandleAddOrderResponse(response);
					break;
				case "	VehiclesChanged":
					await HandleRouteCreatedResponseAsync(response);
					break;
				case "test":
					break;
				default:
					//var result = JsonConvert.DeserializeObject<object>(response.Data.ToString()); //this is just for checking content of response
					break;
			}
		}

		public async Task HandleRouteCreatedResponseAsync(CallbackResponse response)
		{
			var vehiclesChanged = JsonConvert.DeserializeObject<VehiclesChanged>(response.Data.ToString());
			if (vehiclesChanged.Updated.Count > 0)
			{
				foreach (var vehicleId in vehiclesChanged.Updated)
				{
					var ListRoutes = await GetRoutesFromWWByVehicleIdAsync(vehicleId);
				}

			}
		}

		public void HandleAddOrderResponse(CallbackResponse response)
		{
			//attach order id to newly created orders
			var ordersChanged = JsonConvert.DeserializeObject<OrdersChanged>(response.Data.ToString());
			if (ordersChanged.Created != null && ordersChanged.Created.Count > 0)
			{
				Task.Delay(1000).Wait(); //timeout not to overlap multiple requests
				var gcCaseOutgoing = _context.GCCases.Where(m => m.OutgoingRequestId == response.RequestId).FirstOrDefault();
				if (gcCaseOutgoing != null)
				{
					if(ordersChanged?.Created[0]?.Id != null)
					{
						gcCaseOutgoing.OutgoingOrderId = ordersChanged?.Created[0]?.Id?.ToString();
						_context.SaveChanges();
					}
					

				}
				var gcCaseIncoming = _context.GCCases.Where(m => m.IncomingRequestId == response.RequestId).FirstOrDefault();
				if (gcCaseIncoming != null)
				{
					if (ordersChanged?.Created[0]?.Id != null)
					{
						gcCaseIncoming.IncomingOrderId = ordersChanged?.Created[0]?.Id?.ToString();
						_context.SaveChanges();
					}

				}
			}

			//Handle deleted orders 
			if (ordersChanged.Deleted != null && ordersChanged.Deleted.Count > 0)
			{
				foreach (var item in ordersChanged.Deleted)
				{
					var changedOutgoingOrder = _context.GCCases.Where(m => m.OutgoingOrderId == item.ToString()).FirstOrDefault();
					if (changedOutgoingOrder != null)
					{
						changedOutgoingOrder.OutgoingOrderId = null;
						_context.SaveChanges();
					}
					var changedIncomingOrder = _context.GCCases.Where(m => m.IncomingOrderId == item.ToString()).FirstOrDefault();
					if (changedIncomingOrder != null)
					{
						changedIncomingOrder.IncomingOrderId = null;
						_context.SaveChanges();
					}
				}
			}
			//_context.SaveChanges();
		}

		public async Task<SetCallbackUrlResponse> SetCallbackUrl(SetCallbackUrl model)
		{
			var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "callback")
				.PostAsync<SetCallbackUrlResponse>(model);
			return response;
		}

		public async Task<TerritoriesResponse> GetTerritories()
		{
			var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories")
				.GetAsync<TerritoriesResponse>();
			return response;
		}

		public async Task<ListRoutes> GetRoutesFromWWByVehicleIdAsync(string VehicleId)
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "toa", "routes", "?vehicle=" + VehicleId)
					.GetAsync<ListRoutes>();
				return response;
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}

		}

		public async Task SendBarcodeExecutionEvent(BarcodeExecutionEvents data)
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "toa", "executionevents")
					.PostAsync<WorkWaveResponse>(data);
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}
		}

		public async Task SendStatusUpdateExecutionEvent(StatusUpdateExecutionEvents data)
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "toa", "executionevents")
					.PostAsync<WorkWaveResponse>(data);
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}
		}



		public async Task<ListRoutes> GetRoutesFromWWAsync(DateTime? date = null)
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				if (date == null)
				{
					var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "toa", "routes")
						.GetAsync<ListRoutes>();
					return response;
				}
				else
				{
					var formattedDate = ((DateTime)date).ToWorkWaveDate();
					var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "territories", settings.TerritoryId, "toa", "routes")
						.SetQueryParam("date", formattedDate)
						.GetAsync<ListRoutes>();
					return response;
				}

			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}

		}

		public async Task SyncRoutesWithWorkWaveAsync(DateTime? date = null)
		{
			Log.Information($"SyncRoutesWithWorkWaveAsync for date {date?.ToString()}");
			var listRoutes = await GetRoutesFromWWAsync(date);
			if (listRoutes.Routes != null && listRoutes.Routes.Count > 0)
			{
				using (var dbContextTransaction = _context.Database.BeginTransaction())
				{
					try
					{
						CultureInfo provider = CultureInfo.InvariantCulture; //for parsing date
						foreach (var item in listRoutes.Routes)
						{
							var route = item.Value;
							var driver = _context.WWDrivers.Where(x => x.WWDriverId == route.DriverId.ToString()).FirstOrDefault();
							if (driver == null) //If driver doesnt exist, create it in DB
							{
								var wwdriver = listRoutes.Drivers[route.DriverId];
								driver = new WWDriver()
								{
									Email = wwdriver.Email,
									Name = wwdriver.Name,
									WWDriverId = wwdriver.Id.ToString()
								};
								_context.WWDrivers.Add(driver);
								_context.SaveChanges();
							}
							else if (driver.Name != listRoutes.Drivers[route.DriverId].Name || driver.Email != listRoutes.Drivers[route.DriverId].Email) //if driver exists check if his name or email has changed and update id
							{
								driver.Name = listRoutes.Drivers[route.DriverId].Name;
								driver.Email = listRoutes.Drivers[route.DriverId].Email;
								_context.SaveChanges();
							}

							var vehicle = _context.WWVehicles.Where(x => x.WWVehicleId == route.VehicleId.ToString()).FirstOrDefault();
							if (vehicle == null) //If vehicle doesnt exist, create it in DB
							{

								var wwvehicle = listRoutes.Vehicles[route.VehicleId];
								var gpsDevice = _context.WWGPSDevices.Where(x => x.DeviceId == wwvehicle.GPSDeviceId).FirstOrDefault();
								int dbDeviceId = 0;
								if (gpsDevice != null)
								{
									dbDeviceId = gpsDevice.Id;
								}
								else
								{
									var newGpsDevice = new WWGPSDevice()
									{
										DeviceId = wwvehicle.GPSDeviceId,
									};
									_context.WWGPSDevices.Add(newGpsDevice);
									_context.SaveChanges();
									dbDeviceId = newGpsDevice.Id;
								}
								vehicle = new WWVehicle()
								{
									WWVehicleId = wwvehicle.Id.ToString(),
									ExternalId = wwvehicle.ExternalId,
									Available = wwvehicle.Settings.Available,
									Notes = wwvehicle.Settings.Notes,
									WWGPSDeviceId = dbDeviceId,
									DeviceId = wwvehicle.GPSDeviceId
								};
								_context.WWVehicles.Add(vehicle);
								_context.SaveChanges();
							}
							else if (vehicle.ExternalId != listRoutes.Vehicles[route.VehicleId].ExternalId || vehicle.DeviceId != listRoutes.Vehicles[route.VehicleId].GPSDeviceId) //if vehicle exists check its external Id and deviceid and if it has chaged update it
							{
								vehicle.ExternalId = listRoutes.Vehicles[route.VehicleId].ExternalId;
								vehicle.DeviceId = listRoutes.Vehicles[route.VehicleId].GPSDeviceId;
								_context.SaveChanges();
							}

							var wwroute = _context.WWRoutes.Where(x => x.WWRouteId == route.Id.ToString()).FirstOrDefault();
							if (wwroute == null) //if route doesnt exist create it in DB
							{

								wwroute = new WWRoute()
								{
									Date = DateTime.ParseExact(route.Date, "yyyyMMdd", CultureInfo.InvariantCulture),
									DriverId = driver.Id,
									Revision = route.Revision,
									VehicleId = vehicle.Id,
									WWRouteId = route.Id.ToString()
								};
								_context.WWRoutes.Add(wwroute);
								_context.SaveChanges();

								var routeSteps = route.Steps;
								if (routeSteps != null && routeSteps.Count > 0)
								{
									foreach (var routeStep in routeSteps)
									{
										RouteStepStatusType? stepStatus = null; //for checking case status
										int stepTimeIn = -1; //for checking case status
										if (routeStep.TrackingData != null)
										{
											if (routeStep.Type == "pickup" || routeStep.Type == "delivery") //only count pickup or delivery steps
											{
												stepStatus = routeStep.TrackingData.Status;
												stepTimeIn = routeStep.TrackingData.TimeInSec;

												var wwtrackingData = new WWTrackingData()
												{
													WWDriverId = driver.Id,
													WWVehicleId = vehicle.Id,
													TImeInSec = routeStep.TrackingData.TimeInSec,
													Status = routeStep.TrackingData.Status != null ? (int)routeStep.TrackingData.Status : -1,
													StatusSec = routeStep.TrackingData.StatusSec
												};
												_context.WWTrackingData.Add(wwtrackingData);
												_context.SaveChanges();

												var wwrouteStep = new WWRouteStep()
												{
													Type = routeStep.Type, //TODO: Should be string type
													WWOrderId = routeStep.OrderId,
													WWRouteId = wwroute.Id,
													WWTrackingDataId = wwtrackingData.Id ///added
												};
												_context.WWRouteSteps.Add(wwrouteStep);
												_context.SaveChanges();
												/////PODs
												if (routeStep.TrackingData.Pods != null)
												{
													//add note
													var wwnote = new WWPod()
													{
														Sec = routeStep.TrackingData.Pods.Note?.Sec ?? 0,
														Text = routeStep.TrackingData.Pods.Note?.Text,
														Token = routeStep.TrackingData.Pods.Note?.Token
													};
													_context.WWPods.Add(wwnote);
													_context.SaveChanges();

													//add pod container
													var wwpodContainer = new WWPodContainer()
													{
														NoteId = wwnote.Id
													};
													_context.WWPodContainers.Add(wwpodContainer);
													_context.SaveChanges();
													wwtrackingData.WWPodContainerId = wwpodContainer.Id;
													_context.SaveChanges();

													//add signatures
													if (routeStep.TrackingData.Pods.Signatures != null)
													{
														foreach (var signature in routeStep.TrackingData.Pods.Signatures)
														{
															var wwpod = new WWPod()
															{
																Sec = signature.Value.Sec,
																Text = signature.Value.Text,
																Token = signature.Value.Token
															};
															_context.WWPods.Add(wwpod);
															_context.SaveChanges();
															var wwsignature = new WWSignature()
															{
																SignatureType = (int)signature.Key,
																WWPodContainerId = wwpodContainer.Id,
																WWPodId = wwpod.Id
															};
															_context.WWSignatures.Add(wwsignature);
															_context.SaveChanges();
														}
													}


													//add pictures
													if (routeStep.TrackingData.Pods.Pictures != null)
													{
														foreach (var picture in routeStep.TrackingData.Pods.Pictures)
														{
															var wwpod = new WWPod()
															{
																Sec = picture.Value.Sec,
																Text = picture.Value.Text,
																Token = picture.Value.Token
															};
															_context.WWPods.Add(wwpod);
															_context.SaveChanges();
															var wwpicture = new WWPicture()
															{
																Url = picture.Key,
																WWPodContainerId = wwpodContainer.Id,
																WWPodId = wwpod.Id
															};
															_context.WWPictures.Add(wwpicture);
															_context.SaveChanges();
														}
													}

													if (routeStep.TrackingData.Pods.Barcodes != null && routeStep.TrackingData.Pods.Barcodes.Count > 0)
													{
														foreach (var barcode in routeStep.TrackingData.Pods.Barcodes)
														{
															var wwbarcode = new WWBarcode()
															{
																Barcode = barcode.BarcodeValue,
																BarcodeStatus = barcode.BarcodeStatus,
																Sec = barcode.Sec,
																WWPodContainerId = wwpodContainer.Id
															};
															_context.WWBarcodes.Add(wwbarcode);
														}
														_context.SaveChanges();
													}


													//ADD BARCODES
												}

												var gcCase = _context.GCCases.Where(x => x.OutgoingOrderId == routeStep.OrderId || x.IncomingOrderId == routeStep.OrderId).Include(x => x.GCCasesLifecycleStatusTypes).FirstOrDefault();
												if (gcCase != null)
												{
													if (routeStep.Type == "pickup")
													{
														gcCase.WWPickupRouteId = wwroute.Id;
													}
													else
													{
														gcCase.WWDeliveryRouteId = wwroute.Id;
													}

													_context.SaveChanges();

													//check case status
													CheckWorkWaveCaseStatus(gcCase, routeStep, stepStatus, listRoutes.Drivers[route.DriverId]?.Name ?? "");
												}
											}
										}
										else
										{
											if (routeStep.Type == "pickup" || routeStep.Type == "delivery")
											{
												var gcCase = _context.GCCases.Where(x => x.OutgoingOrderId == routeStep.OrderId || x.IncomingOrderId == routeStep.OrderId).Include(x => x.GCCasesLifecycleStatusTypes).FirstOrDefault();
												if (gcCase != null)
												{
													if (routeStep.Type == "pickup")
													{
														gcCase.WWPickupRouteId = wwroute.Id;
													}
													else
													{
														gcCase.WWDeliveryRouteId = wwroute.Id;
													}

													_context.SaveChanges();
												}
											}
										}
									}
								}
							}
							else //if route exists
							{
								var routeSteps = route.Steps;
								if (routeSteps != null && routeSteps.Count > 0) //if route from ww has septs
								{
									foreach (var routeStep in routeSteps) //go through all ww route steps
									{
										if (routeStep.Type == "pickup" || routeStep.Type == "delivery")
										{
											var step = _context.WWRouteSteps.Where(x => x.WWOrderId == routeStep.OrderId && x.Type == routeStep.Type) //get step from db that coresponds to ww step
											.Include(x => x.WWTrackingData)
											.Include(x => x.WWTrackingData.WWDriver)
											.Include(x => x.WWTrackingData.WWVehicle)
											.Include(x => x.WWTrackingData.WWPodContainer)
											.Include(x => x.WWTrackingData.WWPodContainer.Signatures)
											.Include(x => x.WWTrackingData.WWPodContainer.Pictures)
											.Include(x => x.WWTrackingData.WWPodContainer.Note)
											.Include(x => x.WWTrackingData.WWPodContainer.Barcodes)
											.FirstOrDefault();
											if (step != null && step.WWTrackingData != null) //if step exists, update its tracking data
											{
												if (routeStep.TrackingData != null) //check if step from ww has tracking data (this should always be true)
												{
													//if driver changed
													var wwdriver = listRoutes.Drivers[route.DriverId];
													if (step.WWTrackingData.WWDriver.WWDriverId != wwdriver.Id.ToString())
													{
														//get driver from db and update reference
														var dbdriver = _context.WWDrivers.Where(x => x.WWDriverId == wwdriver.Id.ToString()).FirstOrDefault();
														if (dbdriver != null)
														{
															step.WWTrackingData.WWDriverId = dbdriver.Id;
															_context.SaveChanges();
														}
														else //or if driver deosnt exists in db, create new and update reference
														{

															
															driver = new WWDriver()
															{
																Email = wwdriver.Email,
																Name = wwdriver.Name,
																WWDriverId = wwdriver.Id.ToString()
															};
															_context.WWDrivers.Add(driver);
															_context.SaveChanges();
															step.WWTrackingData.WWDriverId = driver.Id;
															_context.SaveChanges();
														}
													}

													//if vehicle changed
													if (step.WWTrackingData.WWVehicle.WWVehicleId != routeStep.TrackingData.VehicleId)
													{
														//get vehicle from db and update reference
														var dbvehicle = _context.WWVehicles.Where(x => x.WWVehicleId == routeStep.TrackingData.VehicleId).FirstOrDefault();
														if (dbvehicle != null)
														{
															step.WWTrackingData.WWVehicleId = dbvehicle.Id;
															_context.SaveChanges();
														}
														else //or if vehicle deosnt exists in db, create new and update reference
														{

															var wwvehicle = listRoutes.Vehicles[route.VehicleId];
															var gpsDevice = _context.WWGPSDevices.Where(x => x.DeviceId == wwvehicle.GPSDeviceId).FirstOrDefault();
															int dbDeviceId = 0;
															if (gpsDevice != null)
															{
																dbDeviceId = gpsDevice.Id;
															}
															else
															{
																var newGpsDevice = new WWGPSDevice()
																{
																	DeviceId = wwvehicle.GPSDeviceId,
																};
																_context.WWGPSDevices.Add(newGpsDevice);
																_context.SaveChanges();
																dbDeviceId = newGpsDevice.Id;
															}
															vehicle = new WWVehicle()
															{
																WWVehicleId = wwvehicle.Id.ToString(),
																ExternalId = wwvehicle.ExternalId,
																Available = wwvehicle.Settings.Available,
																Notes = wwvehicle.Settings.Notes,
																WWGPSDeviceId = dbDeviceId,
																DeviceId = wwvehicle.GPSDeviceId
															};
															_context.WWVehicles.Add(vehicle);
															_context.SaveChanges();
															step.WWTrackingData.WWVehicleId = vehicle.Id;
															_context.SaveChanges();
														}
													}

													//update basic tracking data info
													step.WWTrackingData.TImeInSec = routeStep.TrackingData.TimeInSec;
													if (routeStep.TrackingData.Status != null)
													{
														step.WWTrackingData.Status = (int)routeStep.TrackingData.Status;
													}
													else
													{
														step.WWTrackingData.Status = null;
													}
													step.WWTrackingData.StatusSec = routeStep.TrackingData.StatusSec;
													_context.SaveChanges();

													//PODs
													if (routeStep.TrackingData.Pods != null) //check if there are PODs
													{
														//if podContainer doenst exist in our db and create one 
														if (step.WWTrackingData.WWPodContainer == null)
														{
															var wwpodContainer = new WWPodContainer();
															_context.WWPodContainers.Add(wwpodContainer);
															_context.SaveChanges();
															step.WWTrackingData.WWPodContainer = wwpodContainer;
														}

														//check if note exists in ww
														if (routeStep.TrackingData.Pods.Note != null)
														{
															//if note exists in db, just update existing data
															if (step.WWTrackingData.WWPodContainer.Note != null)
															{
																step.WWTrackingData.WWPodContainer.Note.Sec = routeStep.TrackingData.Pods.Note.Sec;
																step.WWTrackingData.WWPodContainer.Note.Text = routeStep.TrackingData.Pods.Note.Text;
																step.WWTrackingData.WWPodContainer.Note.Token = routeStep.TrackingData.Pods.Note.Token;
																_context.SaveChanges();
															}
															else //if note doesnt exists, create new
															{
																var wwnote = new WWPod()
																{
																	Sec = routeStep.TrackingData.Pods.Note?.Sec ?? 0,
																	Text = routeStep.TrackingData.Pods.Note?.Text,
																	Token = routeStep.TrackingData.Pods.Note?.Token
																};
																_context.WWPods.Add(wwnote);
																_context.SaveChanges();
																step.WWTrackingData.WWPodContainer.NoteId = wwnote.Id;
																_context.SaveChanges();
															}
														}
														else
														{
															if (step.WWTrackingData.WWPodContainer.Note != null) //if note doenst exist in ww but exists in our db, delete it
															{
																_context.Remove(step.WWTrackingData.WWPodContainer.Note);
																_context.SaveChanges();
															}
														}
														//check if there are pictures in ww
														if (routeStep.TrackingData.Pods.Pictures != null)
														{
															//foreach pictures and sync with our db
															foreach (var picture in routeStep.TrackingData.Pods.Pictures)
															{
																var pictureinDB = step.WWTrackingData?.WWPodContainer?.Pictures?.Where(x => x.Url == picture.Key).FirstOrDefault();
																if (pictureinDB == null)
																{
																	var wwpod = new WWPod()
																	{
																		Sec = picture.Value.Sec,
																		Text = picture.Value.Text,
																		Token = picture.Value.Token
																	};
																	_context.WWPods.Add(wwpod);
																	_context.SaveChanges();
																	var wwpicture = new WWPicture()
																	{
																		Url = picture.Key,
																		WWPodContainerId = step.WWTrackingData.WWPodContainer.Id,
																		WWPodId = wwpod.Id
																	};
																	_context.WWPictures.Add(wwpicture);
																	_context.SaveChanges();
																}
															}

														}
														//delete pictures that are in our db but not in ww
														if (step.WWTrackingData?.WWPodContainer?.Pictures != null)
														{
															foreach (var picture in step.WWTrackingData.WWPodContainer.Pictures.ToList())
															{
																if (routeStep.TrackingData.Pods.Pictures == null) //if there are no pictures in ww, delete it anyway
																{
																	_context.Remove(picture);
																	_context.SaveChanges();
																}
																else if (!routeStep.TrackingData.Pods.Pictures.ContainsKey(picture.Url)) //if picture doesnt exist, delete it
																{
																	_context.Remove(picture);
																	_context.SaveChanges();
																}
															}
														}


														//check if there are signarures in ww
														if (routeStep.TrackingData.Pods.Signatures != null)
														{
															//foreach signatures and sync with our db
															foreach (var signature in routeStep.TrackingData.Pods.Signatures)
															{
																var signatureinDB = step.WWTrackingData?.WWPodContainer?.Signatures?.Where(x => x.SignatureType == (int)signature.Key).FirstOrDefault();
																if (signatureinDB == null)
																{
																	var wwpod = new WWPod()
																	{
																		Sec = signature.Value.Sec,
																		Text = signature.Value.Text,
																		Token = signature.Value.Token
																	};
																	_context.WWPods.Add(wwpod);
																	_context.SaveChanges();
																	var wwsignature = new WWSignature()
																	{
																		SignatureType = (int)signature.Key,
																		WWPodContainerId = step.WWTrackingData.WWPodContainer.Id,
																		WWPodId = wwpod.Id
																	};
																	_context.WWSignatures.Add(wwsignature);
																	_context.SaveChanges();
																}
															}

														}
														//delete signatures that are in our db but not in ww
														if (step.WWTrackingData?.WWPodContainer?.Signatures != null)
														{
															foreach (var signature in step.WWTrackingData.WWPodContainer.Signatures.ToList())
															{
																SignatureType signatureType = (SignatureType)Enum.ToObject(typeof(SignatureType), signature.SignatureType);
																if (routeStep.TrackingData.Pods.Signatures == null) //if there are no signatures in ww, delete it anyway
																{
																	_context.Remove(signature);
																	_context.SaveChanges();
																}
																else if (!routeStep.TrackingData.Pods.Signatures.ContainsKey(signatureType)) //if signature doesnt exist, delete it
																{
																	_context.Remove(signature);
																	_context.SaveChanges();
																}
															}
														}
														//check if there are barcodes in WW
														if (routeStep.TrackingData.Pods.Barcodes != null)
														{
															foreach (var barcode in routeStep.TrackingData.Pods.Barcodes) //foreach them
															{
																var dbBarcode = step.WWTrackingData?.WWPodContainer?.Barcodes?.Where(x => x.Barcode == barcode.BarcodeValue).FirstOrDefault();
																if (dbBarcode == null) //if barcode doesnt exist in our db, create it
																{
																	var wwBarcode = new WWBarcode()
																	{
																		Barcode = barcode.BarcodeValue,
																		BarcodeStatus = barcode.BarcodeStatus,
																		Sec = barcode.Sec,
																		WWPodContainerId = step.WWTrackingData.WWPodContainer.Id
																	};
																	_context.WWBarcodes.Add(wwBarcode);
																	_context.SaveChanges();
																}
																else //if barcode exists in our db, check if its status has changed and update it
																{
																	if (dbBarcode.BarcodeStatus != barcode.BarcodeStatus)
																	{
																		dbBarcode.BarcodeStatus = barcode.BarcodeStatus;
																		_context.SaveChanges();
																	}

																}
															}
														}

														////delete barcode that are in our db but not in ww
														//if (step.WWTrackingData?.WWPodContainer?.Barcodes != null)
														//{
														//	foreach (var barcode in step.WWTrackingData.WWPodContainer.Barcodes.ToList())
														//	{
														//		if (routeStep.TrackingData.Pods.Barcodes == null) //if there are no barcodes in ww, delete it anyway
														//		{
														//			_context.Remove(barcode);
														//			_context.SaveChanges();
														//		}
														//		else if (!routeStep.TrackingData.Pods.Barcodes.Select(x=>x.BarcodeValue).Contains(barcode.Barcode)) //if barcode doesnt exist, delete it
														//		{
														//			_context.Remove(barcode);
														//			_context.SaveChanges();
														//		}
														//	}
														//}
														//CHECK BARCODES
													}
													var gcCase = _context.GCCases.Where(x => x.OutgoingOrderId == routeStep.OrderId || x.IncomingOrderId == routeStep.OrderId).Include(x => x.GCCasesLifecycleStatusTypes).FirstOrDefault();
													if (gcCase != null)
													{
														if (routeStep.Type == "pickup")
														{
															gcCase.WWPickupRouteId = wwroute.Id;
														}
														else
														{
															gcCase.WWDeliveryRouteId = wwroute.Id;
														}

														_context.SaveChanges();

														//check case status
														CheckWorkWaveCaseStatus(gcCase, routeStep, routeStep.TrackingData.Status, listRoutes.Drivers[route.DriverId]?.Name ?? "");
													}
												}
											}
											else //if step desnt exist, create new
											{
												RouteStepStatusType? stepStatus = null; //for checking case status
												int stepTimeIn = -1; //for checking case status
												if (routeStep.TrackingData != null)
												{
													if (routeStep.Type == "pickup" || routeStep.Type == "delivery") //only count pickup or delivery steps
													{
														stepStatus = routeStep.TrackingData.Status;
														stepTimeIn = routeStep.TrackingData.TimeInSec;

														var wwtrackingData = new WWTrackingData()
														{
															WWDriverId = driver.Id,
															WWVehicleId = vehicle.Id,
															TImeInSec = routeStep.TrackingData.TimeInSec,
															Status = routeStep.TrackingData.Status != null ? (int)routeStep.TrackingData.Status : -1,
															StatusSec = routeStep.TrackingData.StatusSec
														};
														_context.WWTrackingData.Add(wwtrackingData);
														_context.SaveChanges();

														var wwrouteStep = new WWRouteStep()
														{
															Type = routeStep.Type, //TODO: Should be string type
															WWOrderId = routeStep.OrderId,
															WWRouteId = wwroute.Id,
															WWTrackingDataId = wwtrackingData.Id ///added
														};
														_context.WWRouteSteps.Add(wwrouteStep);
														_context.SaveChanges();
														/////PODs
														if (routeStep.TrackingData.Pods != null)
														{
															//add note
															var wwnote = new WWPod()
															{
																Sec = routeStep.TrackingData.Pods.Note?.Sec ?? 0,
																Text = routeStep.TrackingData.Pods.Note?.Text,
																Token = routeStep.TrackingData.Pods.Note?.Token
															};
															_context.WWPods.Add(wwnote);
															_context.SaveChanges();

															//add pod container
															var wwpodContainer = new WWPodContainer()
															{
																NoteId = wwnote.Id
															};
															_context.WWPodContainers.Add(wwpodContainer);
															_context.SaveChanges();
															wwtrackingData.WWPodContainerId = wwpodContainer.Id;
															_context.SaveChanges();

															//add signatures
															if (routeStep.TrackingData.Pods.Signatures != null)
															{
																foreach (var signature in routeStep.TrackingData.Pods.Signatures)
																{
																	var wwpod = new WWPod()
																	{
																		Sec = signature.Value.Sec,
																		Text = signature.Value.Text,
																		Token = signature.Value.Token
																	};
																	_context.WWPods.Add(wwpod);
																	_context.SaveChanges();
																	var wwsignature = new WWSignature()
																	{
																		SignatureType = (int)signature.Key,
																		WWPodContainerId = wwpodContainer.Id,
																		WWPodId = wwpod.Id
																	};
																	_context.WWSignatures.Add(wwsignature);
																	_context.SaveChanges();
																}
															}


															//add pictures
															if (routeStep.TrackingData.Pods.Pictures != null)
															{
																foreach (var picture in routeStep.TrackingData.Pods.Pictures)
																{
																	var wwpod = new WWPod()
																	{
																		Sec = picture.Value.Sec,
																		Text = picture.Value.Text,
																		Token = picture.Value.Token
																	};
																	_context.WWPods.Add(wwpod);
																	_context.SaveChanges();
																	var wwpicture = new WWPicture()
																	{
																		Url = picture.Key,
																		WWPodContainerId = wwpodContainer.Id,
																		WWPodId = wwpod.Id
																	};
																	_context.WWPictures.Add(wwpicture);
																	_context.SaveChanges();
																}
															}

															if (routeStep.TrackingData.Pods.Barcodes != null && routeStep.TrackingData.Pods.Barcodes.Count > 0)
															{
																foreach (var barcode in routeStep.TrackingData.Pods.Barcodes)
																{
																	var wwbarcode = new WWBarcode()
																	{
																		Barcode = barcode.BarcodeValue,
																		BarcodeStatus = barcode.BarcodeStatus,
																		Sec = barcode.Sec,
																		WWPodContainerId = wwpodContainer.Id
																	};
																	_context.WWBarcodes.Add(wwbarcode);
																}
																_context.SaveChanges();
															}

															//ADD BARCODES
														}

														var gcCase = _context.GCCases.Where(x => x.OutgoingOrderId == routeStep.OrderId || x.IncomingOrderId == routeStep.OrderId).Include(x => x.GCCasesLifecycleStatusTypes).FirstOrDefault();
														if (gcCase != null)
														{
															if (routeStep.Type == "pickup")
															{
																gcCase.WWPickupRouteId = wwroute.Id;
															}
															else
															{
																gcCase.WWDeliveryRouteId = wwroute.Id;
															}

															_context.SaveChanges();

															//check case status
															CheckWorkWaveCaseStatus(gcCase, routeStep, stepStatus, listRoutes.Drivers[route.DriverId]?.Name ?? "");
														}
													}
												}
												else
												{
													if (routeStep.Type == "pickup" || routeStep.Type == "delivery")
													{
														var gcCase = _context.GCCases.Where(x => x.OutgoingOrderId == routeStep.OrderId || x.IncomingOrderId == routeStep.OrderId).Include(x => x.GCCasesLifecycleStatusTypes).FirstOrDefault();
														if (gcCase != null)
														{
															if (routeStep.Type == "pickup")
															{
																gcCase.WWPickupRouteId = wwroute.Id;
															}
															else
															{
																gcCase.WWDeliveryRouteId = wwroute.Id;
															}

															_context.SaveChanges();
														}
													}
												}
											}
											//var GCCase = _context.GCCases.Where(x => x.OutgoingOrderId == routeStep.OrderId || x.IncomingOrderId == routeStep.OrderId).Include(x => x.GCCasesLifecycleStatusTypes).FirstOrDefault();
											//if(GCCase != null && routeStep.TrackingData != null)
											//{
											//	CheckWorkWaveCaseStatus(GCCase, routeStep, routeStep.TrackingData.Status);
											//}

										}

									}
									_context.SaveChanges();
								}
								
							}
						}
						dbContextTransaction.Commit();
					}
					catch (Exception ex)
					{
						dbContextTransaction.Rollback();
						throw ex;
					}
				}


			}
		}
		/// <summary>
		/// This method is used to gate image bytes from ww and store them in out db
		/// </summary>
		/// <returns></returns>
		public async Task SyncImagesWithWorkWave()
		{
			try
			{
				var pods = _context.WWPods.Where(x => x.Token != null && x.GCImageId == null).ToList();
				if (pods != null && pods.Count > 0)
				{
					foreach (var pod in pods)
					{
						var image = await GetImageFromWorkWave(pod.Token);
						var gcImage = new GCImage() { CreatedAtUtc = DateTime.Now, Data = image, Origin = "WW" };
						_context.GCImages.Add(gcImage);
						CreateThumbnailsForGCImage(gcImage);
						_context.SaveChanges();

						pod.GCImageId = gcImage.Id;
						_context.SaveChanges();
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

		public void CreateThumbnailsForGCImage(GCImage gcImage)
		{
			if (gcImage.GCChildImages == null)
			{
				gcImage.GCChildImages = new List<GCImage>();
			}

			var thumbSizes = Enum.GetValues(typeof(ProfitOptics.Modules.Sox.Enums.GCImageSizeType)).Cast<ProfitOptics.Modules.Sox.Enums.GCImageSizeType>().ToList();

			thumbSizes.ForEach(thumb =>
			{
				int biggerSide = 0;
				int h = 0;
				int w = 0;

				byte[] thumbBytes;
				using (MemoryStream ms = new MemoryStream(gcImage.Data, 0, gcImage.Data.Length))
				{
					using (Image img = Image.FromStream(ms))
					{
						switch (thumb)
						{
							case ProfitOptics.Modules.Sox.Enums.GCImageSizeType.SizeMax200:
								biggerSide = 200;
								break;
							default:
								break;
						}

						if (img.Size.Height < biggerSide && img.Size.Width < biggerSide)
						{
							h = img.Size.Height;
							w = img.Size.Width;
						}
						else if (img.Size.Height >= img.Size.Width)
						{
							h = biggerSide;
							w = (biggerSide * img.Size.Width) / img.Size.Height;
							w = CalculateOppositeSide(biggerSide, img.Size.Width, img.Size.Height);
						}
						else
						{
							w = biggerSide;
							h = (biggerSide * img.Size.Height) / img.Size.Width;
							h = CalculateOppositeSide(biggerSide, img.Size.Height, img.Size.Width);
						}

						using (Bitmap b = new Bitmap(img, new Size(w, h)))
						{
							using (MemoryStream ms2 = new MemoryStream())
							{
								b.Save(ms2, System.Drawing.Imaging.ImageFormat.Jpeg);
								thumbBytes = ms2.ToArray();
							}
						}
					}
				}

				Log.Information($"Creating thumbnail {thumb}, for GCImageId: {gcImage.Id}, resized to H:{h}, W:{w}...");

				gcImage.GCChildImages.Add(new GCImage()
				{
					Title = $"{gcImage.Title}_{thumb}",
					Origin = gcImage.Origin,
					Data = thumbBytes,
					Type = (int)thumb,
					//ParentId = gcImage.Id,
					GCParentImage = gcImage,
					CreatedAtUtc = DateTime.UtcNow
				});
			});
		}

		private int CalculateOppositeSide(int biggerSide, int x, int y)
		{
			return (biggerSide * x) / y;
		}

		public void DeleteOldEtlLogs()
        {
			DateTime date = DateTime.Now;
			date = date.AddDays(-14);

			var etlLogsForDelete = _context.ETLLogs.Where(x => x.ProcessingTime < date).ToList();
			_context.ETLLogs.RemoveRange(etlLogsForDelete);
			_context.SaveChanges();
        }

		public void CheckWorkWaveCaseStatus(GCCase gcCase, RouteStep routeStep, RouteStepStatusType? stepStatus, string user)
		{

			//checking if order is done (only done/not done orders have PODs)
			if (stepStatus != null && stepStatus == RouteStepStatusType.done && routeStep.TrackingData != null)
			{
				Log.Information($"Enter CheckWorkWaveCaseStatus: {gcCase.Title} - {routeStep.OrderId} - {routeStep.Type} - {stepStatus.Value}");
				//check barcodes for Loaded and Pickup statuses
				var activeStatus = gcCase.GCCasesLifecycleStatusTypes.Where(x => x.IsActiveStatus == true).FirstOrDefault();
				if (activeStatus != null)
				{
					var currentStatus = _context.GCLifecycleStatusTypes.Find(activeStatus.LifecycleStatusTypeId); //this is activeStatus with db data
					var timestampForDoneStatus = DateTime.UtcNow.TrimHoursMinutesSeconds().AddSeconds(routeStep.TrackingData.StatusSec).AddHours(6); //add 6h for VM timezone
					if (routeStep.Type == "pickup" && gcCase.OutgoingOrderId == routeStep.OrderId)
					{
						//check barcodes for Loaded step
						if (routeStep.TrackingData.Pods != null && routeStep.TrackingData.Pods.Barcodes != null)
						{
							foreach (var barcode in routeStep.TrackingData.Pods.Barcodes)
							{
								var dateForTimestamp = DateTime.UtcNow.TrimHoursMinutesSeconds().AddSeconds(barcode.Sec).AddHours(6);
								_caseManagementClient.ChangeTrayLifecycleStatusByBarcode(gcCase.Id, (int)LifecycleStatusType.Loaded, barcode.BarcodeValue, user, dateForTimestamp);
							}
						}
						var shippedOutboundStatus = _context.GCLifecycleStatusTypes.Where(s => s.Code == (int)LifecycleStatusType.ShippedOutbound).FirstOrDefault(); //Check for outgoing order (ShippedOutbound status)
						if (shippedOutboundStatus != null)
						{
							
								//if not, update case lifecycle status to Loaded
								_caseManagementClient.ChangeGCCaseLifecycleStatus(new Models.ViewModels.ManageGCCaseLifecycleStatuses() { SelectedGCCase = gcCase.Id, SelectedGCLifecycleStatus = shippedOutboundStatus.Id, User = user, Timestamp = timestampForDoneStatus });
							
						}
					}
					else if (routeStep.Type == "delivery" && gcCase.OutgoingOrderId == routeStep.OrderId)//this is for Delivered status
					{
						var deliveredStatus = _context.GCLifecycleStatusTypes.Where(s => s.Code == (int)LifecycleStatusType.Delivered).FirstOrDefault();
						if (deliveredStatus != null)
						{
							
								//if not, update case lifecycle status to Delivered
								_caseManagementClient.ChangeGCCaseLifecycleStatus(new Models.ViewModels.ManageGCCaseLifecycleStatuses() { SelectedGCCase = gcCase.Id, SelectedGCLifecycleStatus = deliveredStatus.Id, User = user, Timestamp = timestampForDoneStatus });
							
						}
					}
					else if (routeStep.Type == "pickup" && gcCase.IncomingOrderId == routeStep.OrderId)//this is for shippedInboundStatus status
					{
						//check barcodes for Picked step
						//if (routeStep.TrackingData.Pods != null && routeStep.TrackingData.Pods.Barcodes != null)
						//{
						//	foreach (var barcode in routeStep.TrackingData.Pods.Barcodes)
						//	{
						//		var dateForTimestamp = DateTime.UtcNow.TrimHoursMinutesSeconds().AddSeconds(barcode.Sec).AddHours(6);
						//		_caseManagementClient.ChangeTrayLifecycleStatusByBarcode(gcCase.Id, (int)LifecycleStatusType.Picked, barcode.BarcodeValue, user, dateForTimestamp);
						//	}
						//}
						var shippedInboundStatus = _context.GCLifecycleStatusTypes.Where(s => s.Code == (int)LifecycleStatusType.ShippedInbound).FirstOrDefault();
						if (shippedInboundStatus != null)
						{
							
								//if not, update case lifecycle status to shippedInboundStatus
								_caseManagementClient.ChangeGCCaseLifecycleStatus(new Models.ViewModels.ManageGCCaseLifecycleStatuses() { SelectedGCCase = gcCase.Id, SelectedGCLifecycleStatus = shippedInboundStatus.Id, User = user, Timestamp = timestampForDoneStatus });
							
						}
					}
					else if (routeStep.Type == "delivery" && gcCase.IncomingOrderId == routeStep.OrderId)//this is for Received status
					{
						var receivedStatus = _context.GCLifecycleStatusTypes.Where(s => s.Code == (int)LifecycleStatusType.Received).FirstOrDefault();
						if (receivedStatus != null)
						{
							
								//if not, update case lifecycle status to Received
								_caseManagementClient.ChangeGCCaseLifecycleStatus(new Models.ViewModels.ManageGCCaseLifecycleStatuses() { SelectedGCCase = gcCase.Id, SelectedGCLifecycleStatus = receivedStatus.Id, User = user, Timestamp = timestampForDoneStatus });
							
						}
					}
				}
			}
			//else if (stepTimeIn > 0 && routeStep.Type == "delivery" && gcCase.OutgoingOrderId == routeStep.OrderId) //this is for arrived status
			//{
			//	var activeStatus = gcCase.GCCasesLifecycleStatusTypes.Where(x => x.IsActiveStatus == true).FirstOrDefault();
			//	if (activeStatus != null)
			//	{
			//		var arrivedStatus = _context.GCLifecycleStatusTypes.Where(s => s.Code == (int)LifecycleStatusType.Arrived).FirstOrDefault();
			//		if (arrivedStatus != null)
			//		{
			//			var currentStatus = _context.GCLifecycleStatusTypes.Find(activeStatus.LifecycleStatusTypeId); //this is activeStatus with db data
			//																										  //check if order isnt already past this status
			//			if (currentStatus.Order < arrivedStatus.Order)
			//			{
			//				//if not, update case lifecycle status to Arrived
			//				_caseManagementClient.ChangeGCCaseLifecycleStatus(new Models.ViewModels.ManageGCCaseLifecycleStatuses() { SelectedGCCase = gcCase.Id, SelectedGCLifecycleStatus = arrivedStatus.Id });
			//			}
			//		}
			//	}
			//}
		}
		public GCLifecycleStatusType GetActiveLifecycleStatusForCase(int caseId)
		{
			var lifecycleStatusId = _context.GCCasesLifecycleStatusTypes.Where(x => x.GCCaseId == caseId && x.IsActiveStatus == true).FirstOrDefault()?.LifecycleStatusTypeId;
			if (lifecycleStatusId != null)
			{
				var lifecycleStatus = _context.GCLifecycleStatusTypes.Find(lifecycleStatusId);

				return lifecycleStatus;
			}
			else
			{
				var lifecycleStatus = _context.GCLifecycleStatusTypes.Where(x => x.Order == 1).FirstOrDefault();

				return lifecycleStatus;
			}

		}

		/// <summary>
		/// This method only gets orderIds from workwave and populates where they miss in GC
		/// </summary>
		/// <returns></returns>
		public async Task GetOrdersFromWorWaveAsync()
		{
			//getting orders in workwave
			var listOrders = await GetOrdersFromWWAsync();
			if(listOrders.Orders!=null)
			{
				foreach (var order in listOrders.Orders)
				{
					if (order.Value.Pickup != null)
					{
						if (order.Value.Pickup.CustomFields != null &&
							order.Value.Pickup.CustomFields.ContainsKey(OrderCustomFiledType.PickUpSurgery.GetAttribute<DisplayAttribute>().Name) &&
							order.Value.Pickup.CustomFields.ContainsKey(OrderCustomFiledType.OutgoingOrder.GetAttribute<DisplayAttribute>().Name) &&
							order.Value.Pickup.CustomFields[OrderCustomFiledType.OutgoingOrder.GetAttribute<DisplayAttribute>().Name] == "True")
						{

							var gcCase = _context.GCCases.Where(x => x.CTCase.CaseReference == order.Value.Pickup.CustomFields[OrderCustomFiledType.PickUpSurgery.GetAttribute<DisplayAttribute>().Name]).FirstOrDefault();
							if (gcCase != null && gcCase.OutgoingOrderId == null)
							{
								gcCase.OutgoingOrderId = order.Key;
								_context.SaveChanges();
							}


						}
						else if (order.Value.Pickup.CustomFields != null &&
							order.Value.Pickup.CustomFields.ContainsKey(OrderCustomFiledType.DeliverySurgery.GetAttribute<DisplayAttribute>().Name) &&
							order.Value.Pickup.CustomFields.ContainsKey(OrderCustomFiledType.OutgoingOrder.GetAttribute<DisplayAttribute>().Name) &&
							order.Value.Pickup.CustomFields[OrderCustomFiledType.OutgoingOrder.GetAttribute<DisplayAttribute>().Name] == "False")
						{

							var gcCase = _context.GCCases.Where(x => x.CTCase.CaseReference == order.Value.Pickup.CustomFields[OrderCustomFiledType.DeliverySurgery.GetAttribute<DisplayAttribute>().Name]).FirstOrDefault();
							if (gcCase != null && gcCase.IncomingOrderId == null)
							{
								gcCase.IncomingOrderId = order.Key;
								_context.SaveChanges();
							}


						}
					}

				}
			}
		

		}
		public async Task<byte[]> GetImageFromWorkWave(string token)
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "pod", token)
					.GetBytesAsync();
				return response;
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}
		}

		public async Task<GPSDeviceCurrentInfos> GetGPSDevicesCurrentPos()
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "gps", "devices", "current")
					.GetAsync<GPSDeviceCurrentInfos>();
				return response;
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}
		}


		public async Task<ListGPSDevices> GetGPSDevices()
		{
			var settings = _context.WWSettings.Where(x => x.IsActive == true).FirstOrDefault();
			if (settings != null)
			{
				var response = await HttpRequestClient._Request.AppendPathSegments("api", "v1", "gps", "devices")
					.GetAsync<ListGPSDevices>();
				return response;
			}
			else
			{
				throw new Exception("There are no valid workwave settings in database");
			}
		}
	}
}
