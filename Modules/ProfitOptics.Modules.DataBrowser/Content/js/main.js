﻿app.activeMenus = ["menuAdmin", "menuDataBrowser"];
$(document).ready(function () {
    page.init();
});

(function (page, $, undefined) {
    page.init = function () {
        page.columns = [];
        page.select2Databases = $('#databases');
        page.select2Tables = $('#tables');
        page.conditionsContainer = $('.conditions-container');
        page.groupingContainer = $('.grouping-container');
        page.buttonsContainer = $('.buttons-container');
        page.tableContainer = $('#table-container');

        page.tableContainerHtml = page.tableContainer.html();

        page.select2Databases.select2({
            allowClear: false,
            placeholder: "Select database",
            minimumInputLength: 0
        });
        page.select2Databases.select2('focus');

        page.select2Databases.on('change', function () {
            page.select2Tables.select2('val', '');
            page.select2Tables.prop('disabled', false);
        });

        //setup tables select2
        page.select2Tables.select2({
            allowClear: false,
            placeholder: "Select table",
            minimumInputLength: 0,
            disabled: false,
            ajax: {
                url: $("#getTablesUrl").val(),
                dataType: 'json',
                type: "POST",
                delay: 250,
                data: function (term) {
                    return {
                        searchQuery: term['term'] !== undefined ? term['term'] : "",
                        databaseName: page.select2Databases.val()
                    };
                },
                processResults: function (data, params) {
                    data = data.map(function (item) {
                        return {
                            id: item.Id,
                            text: item.Name,
                        };
                    });
                    return { results: data };
                },
                formatResult: function (result) {
                    return result.text;
                }
            }
        });

        page.select2Tables.on('change', function (e, a) {
            page.createTable(page.select2Databases.val(), $(this).select2('data')[0].text);
        });

        var executeButton = $('#executeButton');
        executeButton.on('click', function () {
            app.blockUI('Please wait...');
            let groupingColumnNames = page.getGroupings();
            let columnsToDisplay = page.columns;
             // If there are groupings we need to recreate table with only  those colummns along with the count
            if (groupingColumnNames.length > 0) {
                let groupingColumns = page.columns.filter(function (obj) {
                    return groupingColumnNames.includes(obj.Name);
                });
                groupingColumns.push({ Name: "Count", Type: "Int32" });
                columnsToDisplay = groupingColumns;
            }
            page.tableContainer.html(page.tableContainerHtml);
            page.appendColumnsToTable(columnsToDisplay);
            page.setupDataTable('tblName', columnsToDisplay);

            $.unblockUI();
        });

        $('#editWhereClauseBack').on('click', function () {
            $('.conditions-container .tab-pane').removeClass('active');
            $('.conditions-container #conditions').addClass('active');
        });

        $('#editWhereClause').on('click', function () {
            app.blockUI('Loading...');

            $.ajax({
                data: page.getTableQueryModel(page.columns),
                url: $("#whereStatementUrl").val(),
                type: "POST",
                dataType: "json",
                beforeSend: function () {
                    // alertError.hide();
                },
                success: function (data) {
                    if (data.Status === "OK" && data.WhereStatement != null) {
                        var whereStatementText = $('#whereStatementText');
                        whereStatementText.val(data.WhereStatement);

                        $('.conditions-container .tab-pane').removeClass('active');
                        $('.conditions-container #edit-where-clause').addClass('active');
                    } else {
                        $('#error-text').text(data.Message);
                        //alertError.show();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $('#error-text').text('Unknown error has happened. Unable to generate WHERE statement.');
                    //alertError.show();
                },
                complete: function () {
                    $.unblockUI();
                }
            });
        });


        var downloadButton = $('#downloadButton');
        downloadButton.on('click',
            function () {
                app.blockUI('Preparing download...');

                var url = $("#prepareDownloadUrl").val();
                var data = {
                    tableQueryModel: page.getTableQueryModel(page.columns)
                };

                $.ajax({
                    url: url,
                    type: "POST",
                    data: data,
                    dataType: "json",
                    beforeSend: function () {
                        // alertError.hide();
                    },
                    success: function (res) {
                        $.unblockUI();
                        if (res.Status === "OK" && res.Result) {
                            var fileId = res.Result.id;

                            if (res.Result.tooManyRecords) {
                                let modal = $('#modal-too-many-records').modal();
                                $('.number-of-records', modal).text(res.Result.count);
                                modal.show();
                            } else {
                                var download = function () {
                                    window.location = $("#downloadUrl").val() + "/" + fileId;
                                };

                                if (res.Result.downloadWarning) {
                                    let modal = $('#modal-warning-number-of-records').modal();
                                    $('.number-of-records', modal).text(res.Result.count);

                                    $('.download-button', modal).one('click',
                                        function () {
                                            download();
                                        });

                                    modal.on('hide.bs.modal',
                                        function () {
                                            $('.download-button', modal).unbind('click');
                                        });

                                    modal.show();
                                } else {
                                    download();
                                }
                            }
                        } else {
                            $('#error-text').text(data.Message);
                            // alertError.show();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $('#error-text').text('Unknown error has happened. Unable to generate WHERE statement.');
                        //alertError.show();
                    },
                    complete: function () {
                        //$.unblockUI();
                    }
                });
            });
    };

    page.clearFilters = function () {
        var filterContainer = $('#filter-container');

        $('.filter', filterContainer).remove();
        $('#whereStatementText').val('');
    };

    page.clearGrouping = function () {
        var groupingContainer = $('#grouping-container');

        $('.groupBy', groupingContainer).remove();
    };

    page.createTable = function (dbName, tblName) {

        app.blockUI('Please wait...');

        page.conditionsContainer.show();
        page.buttonsContainer.show();
        page.groupingContainer.show();

        page.getTablesColumns(dbName, tblName, function (data) {
            page.tableContainer.html(page.tableContainerHtml);
            page.appendColumnsToTable(data);

            page.clearFilters();
            page.clearGrouping();

            page.columns = data;

            page.getFilter(dbName, tblName, function (filter) {
                var filterContainer = $('#filter-container');

                filterContainer.append(filter);
            });

            page.getGrouping(dbName, tblName, function (grouping) {
                var groupingContainer = $('#grouping-container');
                groupingContainer.append(grouping);
            });

            page.setupDataTable('tblName', data);
        });

        $.unblockUI();
    };

    page.getTablesColumns = function (dbName, tblName, callback) {

        $.ajax({
            url: $("#tableDataColumnsUrl").val(),
            type: "POST",
            data: {
                databaseName: dbName,
                tableName: tblName
            },
            dataType: "json",
            success: function (data) {
                if ($.isFunction(callback)) {
                    callback(data);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#error-text').text('Unknown error has happened. Unable to generate WHERE statement.');
            }
        });

    };

    page.appendColumnsToTable = function (columns) {
        var table = $('#data-table');
        var thead = $('thead', table);

        var row = $('<tr>');
        $.each(columns, function (i, column) {
            var col = $('<th>');
            col.text(column.Name);

            col.appendTo(row);
        });

        thead.append(row);
    };

    page.getTableQueryModel = function (columns, isOrderable = false) {
        const databaseName = $('#databases').val();
        const tableName = $('#tables').select2('data')[0].text;

        const columnNames = columns.map(function (e) { return e.Name; });

        let sortColumnName = isOrderable ? columnNames[0] : undefined;
        let sortDirection = isOrderable ? 'asc' : undefined;
        let displayStart = 1;
        let displayLength = 10;
        let useWhereStatement = false;
        let whereStatement = "";
        if (page.tbl !== null && page.tbl !== undefined) {
            const tableOrder = page.tbl.order();
            if (isOrderable && tableOrder && tableOrder.length > 0) {
                var sortColumnIndex = tableOrder[0][0];
                sortColumnName = columnNames[sortColumnIndex];
                sortDirection = tableOrder[0][1];
            }

            var pageInfo = page.tbl.page.info();
            displayStart = pageInfo.start;
            displayLength = pageInfo.length;
        }

        var editWhereStatementTab = $('#edit-where-clause');
        if (editWhereStatementTab.hasClass('active')) {
            useWhereStatement = true;
            whereStatement = $('#whereStatementText').val();
        }

        var tableQueryModel = {
            DatabaseName: databaseName,
            TableName: tableName,
            Conditions: page.getConditions(),
            GroupByColumns: page.getGroupings(),
            SortColumnName: sortColumnName,
            SortDirection: sortDirection,
            DisplayStart: displayStart,
            DisplayLength: displayLength,
            columns: columnNames,
            UseWhereStatement: useWhereStatement,
            WhereStatement: whereStatement
        };

        return tableQueryModel;
    };

    page.getConditions = function () {
        var filterContainer = $('#filter-container');
        var filters = $('.filter', filterContainer);

        var conditions = [];
        filters.each(function (i, f) {
            var logicOperator = $('#logicOperator', f).val();
            var column = $('#column', f).val();
            var condition = $('#condition', f).val();
            var value = $('#value', f).val();

            conditions.push({
                ColumnName: column,
                ComparisonOperator: condition,
                Value: value,
                LogicOperator: logicOperator
            });
        });

        return conditions;
    };

    page.getGroupings = function () {
        var groupings = $('select', $('#grouping-container'));
        return groupings.val() || [];
    };

    page.setupDataTable = function (tableName, columns, isOrderable = false) {
        var table = $('#data-table');
        var url = $("#tableDataUrl").val();

        var defaultSettings = {
            lengthChange: true,
            autoWidth: false,
            processing: false,
            serverSide: true,
            searching: true,
            ordering: isOrderable,
            paging: true,
            info: true,
            pageLength: 10,
            "rowCallback": function (row, data, index) {

            },
            lengthMenu: [3, 10, 25, 50, 100, 250, 500],
            dom: "<'row'<'col-md-4 left-aligned'><'col-md-8 absolute-right right-aligned pull-right'<'detailsadditional-div pull-right'>>r>t<''<'col-md-4  left-aligned pull-left'li><'col-md-8 right-aligned  pull-right'p>>",

            ajax: {
                url: url,
                type: "POST",
                data: function (d) {
                    d.tableQueryModel = page.getTableQueryModel(columns, isOrderable);
                },
                complete: function (data) {
                    if (data.responseJSON != null && data.responseJSON.Status === 'ERROR')
                        alert(data.responseJSON.Message);

                },
                error: function (data) {
                    console.log(data);
                }
            },
            columns: []
        };
        $.each(columns, function (i, column) {

            var columnSetting = {
                data: column.Name,
                render: function (data, type, full, meta) {

                    return data != null ? formatValue(data, column.Type) : '';
                }
            };

            defaultSettings.columns.push(columnSetting);
        });

        if (page.tbl) {
            page.tbl.destroy();
            page.tbl = null;
        }

        page.tbl =
            table.on('preXhr.dt', function (e, settings, data) {

                if (settings.jqXHR != null) {
                    settings.jqXHR.abort();
                }

                $('.table-overlay').show();
            }
            ).on('xhr.dt', function (e, settings, json, xhr) {
                $('.table-overlay').hide();
            }).DataTable(defaultSettings);
    };

    function formatValue(value, type) {
        if (type === "DateTime") {
            return value.formatJsonDate(true);
        } else {
            return value;
        }
    }

    //Filters
    page.getFilter = function (databaseName, tableName, callback) {
        var postData = {
            databaseName: databaseName,
            tableName: tableName
        };
        $.post($("#filterUrl").val(), postData, function (res) {
            var filter = $(res);

            page.setupFilter(filter, databaseName, tableName);

            callback(filter);
            page.cleanupFilters();
        });
    };

    page.setupFilter = function (filter, databaseName, tableName) {
        $('.filter-dropdown', filter).each(function () {
            var dropdown = $(this);

            dropdown.select2({
                allowClear: true,
                placeholder: '',
                height: "34"
            });

            if (dropdown.hasClass('condition')) {
                dropdown.on('change', function (e) {
                    var valueTextBox = $('#value', filter).hide();

                    if (e.val === 'IsNull' || e.val === 'IsNotNull') {
                        valueTextBox.hide();
                        valueTextBox.val('');
                    } else {
                        valueTextBox.show();
                    }
                });
            }
        });

        var removeCondition = $('.remove-condition', filter);
        removeCondition.on('click', function () {
            var filterContainer = $('#filter-container');
            var numFilters = $('.filter', filterContainer).length;

            if (numFilters === 1) {

                $('#column', filter).val(null).trigger('change');
                $('#condition', filter).val(null).trigger('change');
                $('#value', filter).val('');
            } else {
                filter.remove();
            }

            page.cleanupFilters();
        });

        var addCondition = $('.add-condition', filter);
        addCondition.on('click', function () {
            page.getFilter(databaseName, tableName, function (newFilter) {
                filter.after(newFilter);
            });
        });
    };

    page.cleanupFilters = function () {
        var filterContainer = $('#filter-container');

        $('.logic-operator:first, .logic-operator', filterContainer).hide();
        $('.logic-operator', filterContainer).not(':first').show();

        $('.add-condition', filterContainer).not(':last').hide();
        $('.add-condition:last', filterContainer).show();
    };

    //Grouping
    page.getGrouping = function (databaseName, tableName, callback) {
        var postData = {
            databaseName: databaseName,
            tableName: tableName
        };
        $.post($("#groupByUrl").val(), postData, function (res) {
            var grouping = $(res);

            page.setupGroupBy(grouping, databaseName, tableName);

            callback(grouping);
        });
    };

    page.setupGroupBy = function (groupingPartial, databaseName, tableName) {
        $('.groupby-dropdown', groupingPartial).each(function () {
            var dropdown = $(this);

            dropdown.select2({
                allowClear: true,
                placeholder: 'Select Columns for grouping',
                height: "34"
            });
        });
    };

}(window.page = window.page, $, undefined));