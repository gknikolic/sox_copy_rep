﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCCasesGCTrays
    {
        public int Id { get; set; }
        public int GCCaseId { get; set; }
        public int GCTrayId { get; set; }
        public GCCase GCCase { get; set; }
        public GCTray GCTray { get; set; }
    }
}
