﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.PartialTableViewModels;

namespace ProfitOptics.Modules.Reporting.Factories.RsmPerformance
{
    public interface IRsmPerformanceModelFactory
    {
        /// <summary>
        /// Gets the sales channel chart data.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        Task<List<SalesChannelChartData>> GetSalesChannelChartDataAsync(int year, int month,
            int? regionId = null);

        /// <summary>
        /// Gets the aggregate performance for sales channels.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        Task<List<SalesChannelQuotaPerformanceDataModel>> GetAggregatePerformanceForSalesChannelAsync(
            int year, int month, int? regionId = null);

        /// <summary>
        /// Gets the sales rep sales channel performance.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        Task<List<SalesRepTablePartialViewModel<SalesChannelQuotaPerformanceDataModel>>>
           GetSalesRepSalesChannelPerformanceAsync(int year, int month, int? regionId = null);

        /// <summary>
        /// Gets the aggregate performance for sales channels.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        IEnumerable<SalesChannelQuotaPerformanceDataModel> GetAggregatePerformanceForSalesChannel(int year,
           int month, int zoneId = 0, int? regionId = null);

        /// <summary>
        /// Gets the sales rep sales channel performance.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId"></param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        IEnumerable<SalesChannelQuotaPerformanceDataModel> GetSalesRepPerformanceForSalesChannel(int year,
           int month, int salesRepId, int? regionId = null);

        RsmPerformanceSalesRepDataModel GetSalesRepPerformanceData(int salesRepId, int year, int month);

        RsmPerformanceSalesRepModel GetSalesRepPerformanceModel(int salesRepId, int year, int month);

        Task<List<ItemCategoryQuotaPerformanceDataModel>> GetAggregatePerformanceForItemCategoryAsync(int year,
            int month, int? regionId = null);

        /// <summary>
        /// Gets the sales rep data for item category.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        Task<List<SalesRepTablePartialViewModel<ItemCategoryQuotaPerformanceDataModel>>>
            GetSalesRepItemCategoryPerformanceAsync(int year, int month, int? regionId = null);

        /// <summary>
        /// Gets the aggregate item category performance.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        IEnumerable<ItemCategoryQuotaPerformanceDataModel> GetAggregatePerformanceForItemCategory(int year,
            int month, int zoneId, int? regionId = null);

        /// <summary>
        /// Gets the sales rep performance for item categories.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        IEnumerable<ItemCategoryQuotaPerformanceDataModel> GetSalesRepPerformanceForItemCategory(int year,
            int month, int salesRepId, int? regionId = null);

        Task<List<ProductFamilyChartData>> GetProductFamilyChartDataAsync(int year, int month, int? regionId = null);

        /// <summary>
        /// Gets the aggregate product category data.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        Task<List<ProductFamilyQuotaPerformanceDataModel>> GetAggregatePerformanceForProductFamilyAsync(
            int year, int month, int? regionId = null);

        /// <summary>
        /// Gets the sales rep data for product families.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        Task<List<SalesRepTablePartialViewModel<ProductFamilyQuotaPerformanceDataModel>>>
           GetSalesRepProductFamilyPerformanceAsync(int year, int month, int? regionId = null);

        /// <summary>
        /// Gets the sales rep performance for product families.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        IEnumerable<ProductFamilyQuotaPerformanceDataModel> GetSalesRepPerformanceForProductFamily(int year,
           int month, int salesRepId, int? regionId = null);

        /// <summary>
        /// Gets the aggregate performance for product families.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        IEnumerable<ProductFamilyQuotaPerformanceDataModel> GetAggregatePerformanceForProductFamily(int year,
           int month, int zoneId, int? regionId = null);

        /// <summary>
        /// Gets the region quota performance data
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <returns></returns>
        Task<List<RegionQuotaPerformanceDataModel>> GetRegionTableDataAsync(int year, int month);

        /// <summary>
        /// Gets the region quota performance data
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <returns></returns>
        IEnumerable<RegionQuotaPerformanceDataModel> GetRegionTableData(int year, int month);
    }
}