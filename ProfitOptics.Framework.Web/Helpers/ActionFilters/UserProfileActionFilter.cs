﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ProfitOptics.Framework.Web.Models.LayoutModels;
using System.Linq;
using System.Security.Claims;

namespace ProfitOptics.Framework.Web.Helpers.ActionFilters
{
    public class UserProfileActionFilter : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var fullName = filterContext.HttpContext.User.Claims.SingleOrDefault(x => x.Type == ClaimTypes.GivenName);

            var mainLayoutViewModel = new MainLayoutViewModel
            {
                FullName = fullName?.Value ?? ""
            };
            if (filterContext.Controller is Controller cont) cont.ViewData["MainLayoutViewModel"] = mainLayoutViewModel;
        }
    }
}
