﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
    [Table("WWPod")]
    public class WWPod
    {
        public int Id { get; set; }
        public int Sec { get; set; }
        public string Text { get; set; }
        public string Token { get; set; }
		public int? GCImageId { get; set; }

        public GCImage GCImage { get; set; }
	}
}
