﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.PartialTableViewModels;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.SalesChannel
{
    public class SalesRepSalesChannelTablePartialViewModel : TablePartialViewModelBase
    {
        public List<SalesRepTablePartialViewModel<SalesChannelQuotaPerformanceDataModel>> SalesChannels { get; set; }
    }
}