﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCInvoiceLine
    {
        public int Id { get; set; }
        public int GCInvoiceId { get; set; }
        public int Type { get; set; }
        public int? GCTrayId { get; set; }
        public int? GCCaseId { get; set; }
        public int? QBInvoiceLineId { get; set; }
        public string SKU { get; set; }
        public int LineNum { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string DetailType { get; set; }
        public DateTime? SalesItemServiceDate { get; set; }
        public string SalesItemValue { get; set; }
        public string SalesItemName { get; set; }
        public decimal SalesItemUnitPrice { get; set; }
        public int SalesItemQty { get; set; }
        public string SalesItemTaxCodeValue { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
        public DateTime? EndDateUtc { get; set; }
        public string ItemDisplayName { get; set; }

        public virtual GCInvoice GCInvoice { get; set; }
    }
}
