﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Web.Helpers.ActionFilters;
using ProfitOptics.Framework.Web.Helpers.CustomAttributes;
using UAParser;

// Must be in the same namespace as root areas base controller in order to work as partial class
namespace ProfitOptics.Framework.Web.Areas
{
    [ControllerActivation]
    public partial class BaseController
    {
        partial void RegisterEventHandlers()
        {
            OnActionExecutingEvent += LogCallHandler;
        }

        private void LogCallHandler(ActionExecutingContext context)
        {
            if (HttpContext?.User?.Identity?.IsAuthenticated ?? false)
            {
                LogThisCall(context);
            }
        }

        private void LogThisCall(ActionExecutingContext requestContext)
        {
            IActionDescriptorCollectionProvider actionProvider = HttpContext.RequestServices.GetService<IActionDescriptorCollectionProvider>();

            if (IgnoreAuditLog(actionProvider))
                return;

            var newAction = new POActionAudit
            {
                CreateTime = DateTime.UtcNow,
                Url = requestContext.HttpContext.Request.Path + requestContext.HttpContext.Request.QueryString
            };
            if (newAction.Url != null && newAction.Url.Length > 512)
                newAction.Url = newAction.Url.Substring(0, 512);

            newAction.Username = User.FindFirstValue(ClaimTypes.Name);

            var statusCode = requestContext.HttpContext.Response.StatusCode;
            newAction.Status = statusCode.ToString() + " " + ((HttpStatusCode)statusCode).ToString();

            var uaString = !string.IsNullOrEmpty(requestContext.HttpContext.Request.Headers["User-Agent"]) ?
                requestContext.HttpContext.Request.Headers["User-Agent"].ToString() : "";

            if (!string.IsNullOrEmpty(uaString))
            {
                var uaParser = Parser.GetDefault();
                var clientInfo = uaParser.Parse(uaString);
                newAction.Browser = clientInfo.UA.Family + " " + clientInfo.UA.Major + "." + clientInfo.UA.Minor;
            }
            else
            {
                newAction.Browser = "Unknown";
            }

            var ip = requestContext.HttpContext.Connection.RemoteIpAddress.ToString();
            if (string.IsNullOrEmpty(ip))
                ip = requestContext.HttpContext.Connection.LocalIpAddress.ToString();
            newAction.IpAddress = ip;

            var routeData = requestContext.RouteData;
            newAction.Area = routeData.Values["area"] as string;
            newAction.Controller = routeData.Values["controller"] as string;
            newAction.Action = routeData.Values["action"] as string;

            var metadata = GetActionMetadata(actionProvider);
            newAction.ReflectedType = metadata.Key;
            newAction.ReflectedActionName = metadata.Value;

            Context.POActionAudits.Add(newAction);

            Context.SaveChanges();
        }

        public bool IgnoreAuditLog(IActionDescriptorCollectionProvider actionProvider)
        {
            //Check global ignore filter;
            //Add ignore filter by adding this line:
            //services.AddScoped<IgnoreAuditLogActionFilter>() to Startup.cs
            var ignoreAuditLogGlobal = HttpContext.RequestServices.GetService<IgnoreAuditLogActionFilter>();
            if (ignoreAuditLogGlobal != null)
                return true;

            //Check controller ignore filter
            var controllerAttributes = this.GetType().CustomAttributes;
            if (controllerAttributes != null && controllerAttributes.Any(x => x.AttributeType == typeof(IgnoreAuditLogActionFilter)))
                return true;

            //Check action ignore filter
            var actionDescriptor = actionProvider.ActionDescriptors.Items
                            .Single(x => x.Id == ControllerContext.ActionDescriptor.Id) as ControllerActionDescriptor;

            var actionAttributes = actionDescriptor?.MethodInfo.CustomAttributes;

            return actionAttributes != null && actionAttributes.Any(x => x.AttributeType == typeof(IgnoreAuditLogActionFilter));
        }

        public KeyValuePair<string, string> GetActionMetadata(IActionDescriptorCollectionProvider actionProvider)
        {
            var actionDescriptor = actionProvider.ActionDescriptors.Items
                .Single(x => x.Id == ControllerContext.ActionDescriptor.Id) as ControllerActionDescriptor;

            return new KeyValuePair<string, string>(actionDescriptor?.MethodInfo?.ReflectedType?.ToString(), actionDescriptor?.MethodInfo?.Name);
        }
    }
}
