USE [Sox]

INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (14, 'GCTraySkippedRequiredStepLoanerInbound', 'Tray skipped required step - Loaner (Inbound)')
INSERT INTO [dbo].[GCSystemAlertType] (Id, Name, Message) VALUES (15, 'GCTraySkippedRequiredStepLoanerOutbound', 'Tray skipped required step - Loaner (Outbound)')
