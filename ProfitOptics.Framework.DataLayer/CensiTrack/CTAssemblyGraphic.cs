﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer
{
	[Table("CTAssemblyGraphic")]
    public class CTAssemblyGraphic
	{
		public int Id { get; set; }
		public long ContainerTypeId { get; set; }

        public string CensisSetHistoryId { get; set; }

        public int LifecycleStatusTypeId { get; set; }

        public long GraphicId { get; set; }

        public string Description { get; set; }

        public int? GCImageId { get; set; }
		public long ActualLegacyId { get; set; }

		public virtual GCImage GCImage { get; set; }

        public virtual GCTraysLifecycleStatusTypes LifecycleStatusType { get; set; }
    }
}
