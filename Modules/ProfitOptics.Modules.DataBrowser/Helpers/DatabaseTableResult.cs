﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;

namespace ProfitOptics.Modules.DataBrowser.Helpers
{
    public class DatabaseTableResult : ActionResult
    {
        private readonly string fileName;
        private readonly string separator;
        private readonly SqlCommand sqlCommand;
        private string tableName;
        string connectionString;

        public DatabaseTableResult(string tableName, string fileName, string separator,
            SqlCommand sqlCommand, string connectionString)
        {
            this.tableName = tableName;
            this.fileName = fileName;
            this.separator = separator;
            this.sqlCommand = sqlCommand;
            this.connectionString = connectionString;
        }

        public override void ExecuteResult(ActionContext context)
        {
            context.HttpContext.Response.Headers.Add("Content-Disposition", "inline;filename=" + fileName);
            switch (separator)
            {
                case "\t":
                    context.HttpContext.Response.ContentType = "text/tab-separated-values";
                    break;
                case ",":
                    context.HttpContext.Response.ContentType = "text/csv";
                    break;
                default:
                    context.HttpContext.Response.ContentType = "text/plain";
                    break;
            }

            
            var writer = new StreamWriter(context.HttpContext.Response.Body);
            using (SqlCommand command = sqlCommand)
            {
                command.Connection.ConnectionString = connectionString;
                command.Connection.Open();
                command.CommandTimeout = 60;

                var columns = new List<string>();


                using (SqlDataReader reader = command.ExecuteReader())
                {
                    DataTable dt = reader.GetSchemaTable();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var column = (string)dt.Rows[i]["ColumnName"];
                        columns.Add(column);
                        writer.Write(column);

                        if (i < dt.Rows.Count - 1)
                            writer.Write(separator);
                    }

                    writer.Write(Environment.NewLine);
                    writer.Flush();
                    while (reader.Read())
                    {
                        for (int i = 0; i < columns.Count; i++)
                        {
                            if (!Convert.IsDBNull(reader[i]))
                            {
                                writer.Write("\"" +
                                             Regex.Replace(reader[i].ToString(),
                                                 @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;", string.Empty)
                                                 .Trim()
                                                 .Replace('"', ' ') + "\"");
                            }

                            if (i < columns.Count - 1)
                            {
                                writer.Write(separator);
                            }
                        }

                        writer.Write(Environment.NewLine);
                        writer.Flush();
                    }
                }
            }
        }
    }
}