﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportHierarchyCorporateConfig : IEntityTypeConfiguration<ReportHierarchyCorporate>
    {
        public void Configure(EntityTypeBuilder<ReportHierarchyCorporate> builder)
        {
            builder.ToTable(nameof(ReportHierarchyCorporate));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Name).HasMaxLength(50);
            builder.Property(c => c.StartDate).IsRequired();
        }
    }
}
