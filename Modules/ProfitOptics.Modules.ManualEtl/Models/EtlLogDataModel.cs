﻿using System;
using System.Globalization;

namespace ProfitOptics.Modules.ManualEtl.Models
{
    public class EtlLogDataModel
    {
        public string Source { get; set; }

        public string FileName { get; set; }

        public string Username { get; set; }

        public DateTime? ProcessingTime { get; set; }

        public string ProcessingTimeString => ProcessingTime.HasValue
            ? ProcessingTime.Value.ToString("MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture)
            : "";

        public string Status { get; set; }

        public string StatusMessage { get; set; }

        public int RowCount { get; set; }
    }
}