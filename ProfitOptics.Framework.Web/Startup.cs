﻿using AutoMapper;
using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Etl.Helpers;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Framework.Web.Services;
using ProfitOptics.Modules.Sox.MapperProfilers;
using ProfitOptics.Modules.Sox.Models.CensiTrack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using IRouteRegistrator = ProfitOptics.Framework.Web.Infrastructure.IRouteRegistrator;

namespace ProfitOptics.Framework.Web
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public IWebHostEnvironment Environment { get; }
        public List<Assembly> ExternalAssemblies { get; set; }

        public IConfiguration Configuration => _configuration;

        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            _configuration = configuration;
            Environment = hostingEnvironment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            //Configure settings classes
            StartupConfiguration.ConfigureStartupConfig<Core.Settings.Twilio>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<Email>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<Common>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<Settings>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<PdfPrinter>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<QuickBooksOptions>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<WorkWaveOptions>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<CensiTrackOptions>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<MonnitOptions>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<BoxstormOptions>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<GlobalOptions>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<Recaptcha>(services, _configuration);
            StartupConfiguration.ConfigureStartupConfig<Ldap>(services, _configuration);
            var auth = StartupConfiguration.ConfigureStartupConfig<Auth>(services, _configuration);
            var connectionStrings = StartupConfiguration.ConfigureStartupConfig<ConnectionStrings>(services, _configuration);
            var elmahIo = StartupConfiguration.ConfigureStartupConfig<ElmahIo>(services, _configuration);

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<BreadcrumbService>();
            services.AddTransient<NotificationService>();

            services.AddMemoryCache();

            var engine = EngineContext.Create();
            engine.Initialize(services);

            engine.ConfigureServices(services, Configuration, connectionStrings, auth, elmahIo);

            var mapperConfiguration = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MapperProfiler());
            });

            IMapper mapper = mapperConfiguration.CreateMapper();
            services.AddSingleton(mapper);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error/500");
            }

            app.UseStatusCodePagesWithReExecute("/Error/{0}");

            app.UseHttpsRedirection();

            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "Modules")),
                RequestPath = "/modules"
            });

            app.UseRouting();

            app.UseSession();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseResponseCaching();
            app.UseElmahIo();
            app.UseCookiePolicy();
            app.UseResponseCompression();

            app.UseEndpoints(endpoints =>
            {
                IEnumerable<Type> routeRegistratorTypes = Assembly.GetExecutingAssembly().GetTypes()
                    .Where(type => !type.IsInterface && !type.IsAbstract && typeof(IRouteRegistrator).IsAssignableFrom(type));

                foreach (Type routeRegistratorType in routeRegistratorTypes)
                {
                    var routeRegistartor = (IRouteRegistrator)Activator.CreateInstance(routeRegistratorType);

                    routeRegistartor.RegisterRoutes(endpoints);
                }

                endpoints.MapControllerRoute(
                 name: "areas",
                 pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                 name: "default",
                 pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });

        }
    }
}