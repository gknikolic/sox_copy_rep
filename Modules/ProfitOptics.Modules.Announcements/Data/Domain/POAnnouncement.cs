using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Announcements.Data.Domain
{
    [Table("POAnnouncement")]
    public partial class POAnnouncement
    {
        public POAnnouncement()
        {
            POAnnouncementReads = new HashSet<POAnnouncementRead>();
            POAnnouncementRoles = new HashSet<POAnnouncementRole>();
        }

        public int Id { get; set; }

        public int? UserId { get; set; }

        public DateTime? CreateTime { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public DateTime? ValidUntil { get; set; }

        public bool? IsDeleted { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        public virtual ICollection<POAnnouncementRead> POAnnouncementReads { get; set; }

        public virtual ICollection<POAnnouncementRole> POAnnouncementRoles { get; set; }
    }
}
