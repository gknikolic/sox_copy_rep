﻿using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;

namespace ProfitOptics.Framework.Web.Infrastructure
{
    public interface IModuleManager
    {
        IEnumerable<Assembly> GetExternalModules(IWebHostEnvironment hostingEnvironment);
    }
}
