﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer
{
    [Table("GCContainerType")]
    public partial class GCContainerType
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<GCContainer> Containers { get; set; }
    }
}
