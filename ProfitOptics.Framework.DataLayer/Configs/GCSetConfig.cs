﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCSetConfig : IEntityTypeConfiguration<GCSet>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCSet> builder)
        {
            builder.ToTable("GCSet");
            builder.HasKey(entity => entity.Id);
            builder.HasOne(entity => entity.Case)
               .WithMany(p => p.Sets)
               .HasForeignKey(entity => entity.CaseId);
        }
    }

    public class GCSettingsConfig : IEntityTypeConfiguration<GCSettings>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCSettings> builder)
        {
            builder.ToTable("GCSettings");
            builder.HasKey(entity => entity.Id);
        }
    }
}
