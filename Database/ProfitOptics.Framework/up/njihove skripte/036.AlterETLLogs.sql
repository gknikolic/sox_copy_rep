USE [Sox]

BEGIN
 
ALTER TABLE ETLLog ADD [ActionType] [INT] NULL
ALTER TABLE ETLLog ADD [Options] [NVARCHAR](500) NULL
ALTER TABLE ETLLog ADD [Description] [NVARCHAR](MAX) NULL

 END