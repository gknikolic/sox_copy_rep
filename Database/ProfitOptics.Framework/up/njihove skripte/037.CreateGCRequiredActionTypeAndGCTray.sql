USE [Sox]

BEGIN

CREATE TABLE [dbo].[GCRequiredActionType]
(
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Enumerator] [varchar](50),
	[Caption] [varchar](100),
	[Sequence] [int] NOT NULL,
    [Priority] [int] NOT NULL	
)

CREATE TABLE [dbo].[GCTray]
(
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[CTCaseContainerId] [int],
	FOREIGN KEY ([CTCaseContainerId]) REFERENCES [dbo].[CTCaseContainer] ([Id])
)

CREATE TABLE [dbo].[GCTrayGCRequiredActionTypes]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GCTrayId] [int] NOT NULL,
	[GCRequiredActionTypeId] [int] NOT NULL,
	FOREIGN KEY ([GCTrayId]) REFERENCES [dbo].[GCTray] ([Id]),
	FOREIGN KEY ([GCRequiredActionTypeId]) REFERENCES [dbo].[GCRequiredActionType] ([Id])
)

END