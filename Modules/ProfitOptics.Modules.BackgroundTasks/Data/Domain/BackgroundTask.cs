﻿using System;

namespace ProfitOptics.Modules.BackgroundTasks.Data.Domain
{
    public class BackgroundTask
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Description { get; set; }

        public decimal Progress { get; set; }

        public DateTime DateStart { get; set; }

        public DateTime? DateEnd { get; set; }

        public virtual AspNetUser User { get; set; }
    }
}