﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using OfficeOpenXml;
using ProfitOptics.Modules.Reporting.Attributes;

namespace ProfitOptics.Modules.Reporting.Services.MarginBridge
{
    public interface IEPPlusExtensionService
    {
        List<T> LoadFromExcelFile<T>(string fileName, int worksheetIndex);

        List<T> LoadFromExcelFile<T>(Stream file, int worksheetIndex);

        List<string> GetColumnNames(string fileName, int worksheetIndex);

        List<string> GetColumnNames(Stream file, int worksheetIndex);

        List<string> GetWorksheetNames(string fileName);

        List<string> GetWorksheetNames(Stream file);

        void WriteToExcelFile<T>(string fileName, string sheetName, List<T> values, bool autoFormatTable);

        void WriteToExcelFile<T>(string fileName, string sheetName, List<T> values, List<string> titles,
            bool autoFormatTable);

        byte[] WriteToExcel<T>(string sheetName, List<T> values, bool autoFormatTable);

        byte[] WriteToExcel<T>(string sheetName, List<T> values, List<string> titles, bool autoFormatTable);

        byte[] AppendToExcel<T>(byte[] existingDoc, string sheetName, List<T> values, bool autoFormatTable);

        byte[] WriteToExcel<T>(Dictionary<string, List<T>> valuesInSheets, bool autoFormatTable);

        void WriteToExcelHelper<T>(ExcelPackage pck, string sheetName, List<T> values, bool autoFormatTable,
            PropertyInfo[] members, List<string> titles, List<FieldTitleAttribute> attributes,
            bool wrapStringCells = false);

        PropertyInfo[] GetValidProperties(Type type);

        object GetSafePropertyValue(Type type, string value);

        MemoryStream GetClonedExcelTemplateStream(string templatePath);
    }
}
