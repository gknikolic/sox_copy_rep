﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    [Table("ReportQuota")]
    public partial class ReportQuota
    {
        public int Id { get; set; }

        public int SalesRepId { get; set; }

        public int ProductFamilyId { get; set; }

        public int SalesChannelId { get; set; }

        public string Month { get; set; }

        public decimal Revenue { get; set; }

        public int Year { get; set; }

        public string ItemCategoryName { get; set; }

        public virtual ReportCustomerClass ReportCustomerClass { get; set; }

        public virtual ReportHierarchySalesRep ReportHierarchySalesRep { get; set; }

        public virtual ReportVendor ReportVendor { get; set; }
    }
}