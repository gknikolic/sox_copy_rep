﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Framework.Helpers.Notification
{
    public enum NotificationType
    {
        Success,
        Error
    }
}
