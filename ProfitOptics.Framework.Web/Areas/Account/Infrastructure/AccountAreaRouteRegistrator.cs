﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Infrastructure;

namespace ProfitOptics.Framework.Web.Areas.Account.Infrastructure
{
    public class AccountAreaRouteRegistrator : IRouteRegistrator
    {
        public void RegisterRoutes(IEndpointRouteBuilder endpoints)
        {
            endpoints.MapControllerRoute(name: POFrameworkDefaults.ConfirmEmailRouteName, pattern: "/confirmEmail",
                defaults: new { area = "Account", controller = "Account", action = "ConfirmEmail" });
            endpoints.MapControllerRoute(name: POFrameworkDefaults.PasswordResetRouteName, pattern: "/passwordReset",
                defaults: new { area = "Account", controller = "Account", action = "ResetPassword" });
            endpoints.MapControllerRoute(name: POFrameworkDefaults.AddPasswordRouteName, pattern: "/addPassword",
                defaults: new { area = "UserManagement", controller = "Users", action = "AddPassword" });
        }
    }
}
