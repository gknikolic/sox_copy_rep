﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;

namespace ProfitOptics.Modules.DataBrowser.Helpers
{
    public class DataBrowserColumns
    {
        #region Fields

        private readonly SqlConnection conn;
        private readonly string database;

        #endregion

        #region Ctor

        public DataBrowserColumns(SqlConnection conn, string database)
        {
            this.conn = conn;
            this.database = database;
        }

        #endregion

        #region Methods

        public IEnumerable<DataColumn> GetColumns(string tableName)
        {
            var dataSet = new DataSet();

            using (
                var sqlDataAdapter =
                    (new SqlDataAdapter("SELECT TOP 0 * FROM " + database + ".[dbo]." + tableName + "  ", conn)))
            {
                sqlDataAdapter.Fill(dataSet);

                DataTable table = dataSet.Tables[0];

                return table.Columns.Cast<DataColumn>();
            }
        }

        #endregion
    }
}