﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportCustomerClassConfig : IEntityTypeConfiguration<ReportCustomerClass>
    {
        public void Configure(EntityTypeBuilder<ReportCustomerClass> builder)
        {
            builder.ToTable(nameof(ReportCustomerClass));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Code).HasMaxLength(50);
            builder.Property(c => c.Name).HasMaxLength(50).IsRequired();
            builder.Property(c => c.StartDate).IsRequired();

            builder.HasMany(c => c.ReportSalesHistories)
                .WithOne(s => s.ReportCustomerClass)
                .HasForeignKey(s => s.CustomerClassId)
                .IsRequired(false);

            builder.HasMany(v => v.ReportQuotas)
                .WithOne(s => s.ReportCustomerClass)
                .HasForeignKey(s => s.SalesChannelId);
        }
    }
}
