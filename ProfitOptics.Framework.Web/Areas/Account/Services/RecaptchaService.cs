﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using ProfitOptics.Framework.Core.Settings;

namespace ProfitOptics.Framework.Web.Areas.Account.Services
{
    public class RecaptchaService : IRecaptchaService
    {
        public Recaptcha Settings;
        public RecaptchaService(Recaptcha settings)
        {
            this.Settings = settings;
        }

        public bool Validate(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return false;
            }

            var recaptchaSiteSecret = Settings.RecaptchaSecretKey;
            var verificationRequest =
                $"https://www.google.com/recaptcha/api/siteverify?secret={recaptchaSiteSecret}&response={value}";

            var client = new WebClient();
            var repsonseDownloadString = client.DownloadString(verificationRequest);
            var resultJObject = JObject.Parse(repsonseDownloadString);
            var result = (bool)resultJObject.SelectToken("success");

            return result;
        }
    }
}
