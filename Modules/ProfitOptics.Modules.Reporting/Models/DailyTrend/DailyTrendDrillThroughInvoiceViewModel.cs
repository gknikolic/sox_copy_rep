﻿namespace ProfitOptics.Modules.Reporting.Models.DailyTrend
{
    public class DailyTrendDrillThroughInvoiceViewModel
    {
        public bool IsCorporate { get; set; }

        public string Date { get; set; }

        public int BillToId { get; set; }

        public string BillToNum { get; set; }

        public string BillToName { get; set; }
    }
}
