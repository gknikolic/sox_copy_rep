﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueFixVersion
    {
        public string self { get; set; }

        public string name { get; set; }

        public int? id { get; set; }

        public bool archived { get; set; }

        public bool released { get; set; }
    }
}