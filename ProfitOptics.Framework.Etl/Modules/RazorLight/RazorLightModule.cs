﻿using CommandLine;
using ProfitOptics.Framework.Etl.Helpers;
using ProfitOptics.Framework.Etl.Models;
using Serilog;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Etl.Modules.RazorLight
{
    [Verb("razor_light_module", HelpText = "Supporting multiple commands that Razor Light Module can handle.")]
    public class RazorLightModuleOptions : IOptions
    {

    }

    public class RazorLightModule : IModule
    {
        private readonly IRazorLightService _service;

        public RazorLightModule(IRazorLightService service)
        {
            _service = service;
        }

        public int Execute(IOptions opts)
        {            
            Log.Information("Started rendering template.");

            // an example model
            var model = new List<RazorLightExampleModel>
            {
                new RazorLightExampleModel
                {
                    Vendor = "Test Vendor 1",
                    ReceivedDate = DateTime.UtcNow,
                    FileName = "TestFile.edi",
                    Exception = "Test Exception 1",
                    OrderAmount = (decimal?)12.34,
                    Reviewer = "Ljubisa Stojanovic"
                },
                new RazorLightExampleModel
                {
                    Vendor = "Test Vendor 2",
                    ReceivedDate = DateTime.UtcNow,
                    FileName = "TestFile2.pdf",
                    Exception = "Test Exception 2",
                    OrderAmount = (decimal?)56.12,
                    Reviewer = "Ivan Stanojevic"
                }
            };

            // rendering the template and saving an output
            var result = _service.RenderTemplate("test.cshtml", model).GetAwaiter().GetResult();
            Log.Information("Finished rendering template.");

            return 0;
        }

    }
}
