﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTBILoadResult
    {
        public int Id { get; set; }
        public long LoadId { get; set; }
        public string LoadBarcode { get; set; }
        public string Location { get; set; }
        public long? LocationId { get; set; }
        public string LocationName { get; set; }
        public long? LocationMethodId { get; set; }
        public string LocationMethod { get; set; }
        public long? LocationType { get; set; }
        public DateTime? LoadDate { get; set; }
        public DateTime? InsertTimestamp { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public DateTime? LoadTimestamp { get; set; }
        public string UpdateUser { get; set; }
        public string Operator { get; set; }
        public string SterilizationCycle { get; set; }
        public string CaseReference { get; set; }
        public string Reason { get; set; }
        public string Notes { get; set; }
        public string ResultFlag { get; set; }
        public string Alarm { get; set; }
        public bool? IsLastLoad { get; set; }
        public string SterilantBatch { get; set; }
        public decimal? MaxTemperature { get; set; }
        public string ExposureTime { get; set; }
        public bool? IsImportSuccessfull { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
        public DateTime? LoadDateOriginal { get; set; }
        public DateTime? InsertTimestampOriginal { get; set; }
        public DateTime? UpdateTimestampOriginal { get; set; }
        public DateTime? LoadTimestampOriginal { get; set; }

        public virtual List<CTBILoadResultContent> CTBILoadResultContents { get; set; }
        public virtual List<CTBILoadResultIndicator> CTBILoadResultIndicators { get; set; }
        public virtual List<CTBILoadResultGraphic> CTBILoadResultGraphics { get; set; }
    }
}
