﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QtFedexRateConfig : IEntityTypeConfiguration<QTFedExRate>
    {
        public void Configure(EntityTypeBuilder<QTFedExRate> builder)
        {
            builder.ToTable("QTFedExRate");
            builder.HasKey(entity => entity.Id);

        }
    }
}
