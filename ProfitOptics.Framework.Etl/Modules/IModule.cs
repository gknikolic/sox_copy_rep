﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Etl.Modules
{
    public interface IModule
    {
        int Execute(IOptions opts);
    }
}
