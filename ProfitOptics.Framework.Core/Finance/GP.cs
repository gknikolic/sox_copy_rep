﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Core.Finance
{
    public class GP
    {
        /// <summary>
        /// Calculates gross margin $ from given price and cost. 
        /// </summary>
        /// <param name="price"></param>
        /// <param name="cost"></param>
        /// <returns></returns>
        public static decimal CalculateGP(decimal price, decimal cost)
        {
            return price - cost;
        }

        /// <summary>
        /// Calculates gross margin pct from given price and cost. 
        /// </summary>
        /// <param name="price"></param>
        /// <param name="cost"></param>
        /// <returns></returns>
        public static decimal CalculateGPPct(decimal price, decimal cost)
        {
            return price == 0 ? 0 : (price - cost) / price;
        }

        /// <summary>
        /// Calculates price value from given gross margin pct and cost
        /// </summary>
        /// <param name="gppct"></param>
        /// <param name="cost"></param>
        /// <returns></returns>
        public static decimal CalculatePrice(decimal gppct, decimal cost)
        {
            return gppct == 1 ? 0 : cost / (1 - gppct);
        }

        /// <summary>
        /// Calculates cost value from given gross margin pct and price
        /// </summary>
        /// <param name="gppct"></param>
        /// <param name="cost"></param>
        /// <returns></returns>
        public static decimal CalculateCost(decimal gppct, decimal price)
        {
            return price * (1 - gppct);
        }
    }
}
