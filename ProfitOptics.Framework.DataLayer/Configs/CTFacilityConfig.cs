﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTFacilityConfig : IEntityTypeConfiguration<CTFacility>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTFacility> builder)
        {
            builder.ToTable("CTFacility");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.LegacyId).IsRequired();
            builder.Property(e => e.Name).IsRequired();
        }
    }
}
