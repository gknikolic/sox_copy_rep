﻿namespace ProfitOptics.Modules.Reporting.Models.ReportFilter
{
    public interface IFilterableEntity
    {
        int Id { get; set; }
    }
}
