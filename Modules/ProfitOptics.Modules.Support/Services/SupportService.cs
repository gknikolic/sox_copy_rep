﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Modules.Support.Data;
using ProfitOptics.Modules.Support.Data.Domain;

namespace ProfitOptics.Modules.Support.Services
{
    public class SupportService : ISupportService
    {
        #region Fields

        private readonly Entities _entities;

        #endregion Fields

        #region Ctor

        public SupportService(Entities entities)
        {
            _entities = entities;
        }

        #endregion Ctor

        #region Methods

        #region Issue Watcher Manager

        public void AddWatch(string email, string projectId, string issueId)
        {
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }

            if (projectId == null)
            {
                throw new ArgumentNullException(nameof(projectId));
            }

            if (issueId == null)
            {
                throw new ArgumentNullException(nameof(issueId));
            }

            var existing = (from w in _entities.JiraSupportNotifications
                            where w.Email == email && w.ProjectId == projectId && w.IssueId == issueId
                            select w).FirstOrDefault();

            if (existing == null)
            {
                _entities.JiraSupportNotifications.Add(new JiraSupportNotification
                {
                    Email = email,
                    ProjectId = projectId,
                    IssueId = issueId
                });

                _entities.SaveChanges();
            }
        }

        public void RemoveWatch(string email, string projectId, string issueId)
        {
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }

            if (projectId == null)
            {
                throw new ArgumentNullException(nameof(projectId));
            }

            if (issueId == null)
            {
                throw new ArgumentNullException(nameof(issueId));
            }

            var existing = (from w in _entities.JiraSupportNotifications
                            where w.Email == email && w.ProjectId == projectId && w.IssueId == issueId
                            select w).FirstOrDefault();

            if (existing != null)
            {
                _entities.JiraSupportNotifications.Remove(existing);

                _entities.SaveChanges();
            }
        }

        public List<string> GetWatchedIssues(string email, string projectId)
        {
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }

            if (projectId == null)
            {
                throw new ArgumentNullException(nameof(projectId));
            }

            var issues = (from w in _entities.JiraSupportNotifications
                          where w.Email == email && w.ProjectId == projectId
                          select w.IssueId).ToList();

            if (issues != null && issues.Any())
            {
                return issues;
            }

            return new List<string>();
        }

        public List<string> GetWatchersForIssue(string projectId, string issueId)
        {
            if (projectId == null)
            {
                throw new ArgumentNullException(nameof(projectId));
            }

            if (issueId == null)
            {
                throw new ArgumentNullException(nameof(issueId));
            }

            var emails = (from w in _entities.JiraSupportNotifications
                          where w.IssueId == issueId && w.ProjectId == projectId
                          select w.Email).ToList();

            if (emails != null && emails.Any())
            {
                return emails;
            }

            return new List<string>();
        }

        public bool IsGlobalWatcher(string email)
        {
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }

            var isGlobal = (from w in _entities.JiraSupportGlobalNotifications
                            where w.Email == email
                            select w.Email).FirstOrDefault();

            return isGlobal != null;
        }

        public List<JiraSupportGlobalNotification> GetGlobalWatchers()
        {
            return (from a in _entities.JiraSupportGlobalNotifications
                    select a).ToList();
        }

        public void AddGlobalWatcher(string email)
        {
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }

            var existing = (from w in _entities.JiraSupportGlobalNotifications
                            where w.Email == email
                            select w.Email).FirstOrDefault();

            if (existing == null)
            {
                _entities.JiraSupportGlobalNotifications.Add(new JiraSupportGlobalNotification
                {
                    Email = email
                });

                _entities.SaveChanges();
            }
        }

        public void RemoveGlobalWatcher(string email)
        {
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }

            var existing = (from w in _entities.JiraSupportGlobalNotifications
                            where w.Email == email
                            select w).FirstOrDefault();

            if (existing != null)
            {
                _entities.JiraSupportGlobalNotifications.Remove(existing);

                _entities.SaveChanges();
            }
        }

        public void RemoveAllGlobalWatchers()
        {
            var objCtx = _entities.Database.ExecuteSqlCommand("TRUNCATE TABLE [JiraSupportGlobalNotification]");
        }

        public void AddOrUpdateGlobalWatchers(List<JiraSupportGlobalNotification> notificationEmails)
        {
            if (notificationEmails == null)
            {
                throw new ArgumentNullException(nameof(notificationEmails));
            }

            if (!notificationEmails.Any())
            {
                return;
            }

            var notificationEmailIds = notificationEmails.Select(item => item.Id);

            var existingEmails = _entities.JiraSupportGlobalNotifications
                .Where(item => notificationEmailIds.Contains(item.Id)).ToList();

            foreach (var notificationEmail in notificationEmails)
            {
                var toUpdate = existingEmails.SingleOrDefault(item => item.Id == notificationEmail.Id);
                if (toUpdate != null)
                {
                    toUpdate.Email = notificationEmail.Email;
                }
                else
                {
                    _entities.JiraSupportGlobalNotifications.Add(new JiraSupportGlobalNotification()
                    {
                        Email = notificationEmail.Email
                    });
                }
            }

            _entities.SaveChanges();
        }

        #endregion Issue Watcher Manager

        #endregion Methods
    }
}