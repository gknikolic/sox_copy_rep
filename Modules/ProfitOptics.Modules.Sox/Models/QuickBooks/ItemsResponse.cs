﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class ItemsResponse : ResponseBase
    {
        [JsonProperty("Item")]
        public List<Item> Items { get; set; }
    }
}
