USE [Sox]

ALTER TABLE [Sox].[dbo].[GCImage] ADD [ParentId] INT NULL;
ALTER TABLE [Sox].[dbo].[GCImage] ADD [Type] INT NULL;

GO

ALTER TABLE [dbo].[GCImage]  WITH CHECK ADD  CONSTRAINT [FK_GCImage_GCImage] FOREIGN KEY([ParentId])
REFERENCES [dbo].[GCImage] ([Id])
GO
ALTER TABLE [dbo].[GCImage] CHECK CONSTRAINT [FK_GCImage_GCImage]
GO