﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;
using ProfitOptics.Modules.Reporting.Session.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Factories.MarginBridge
{
    public interface IMarginBridgeFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        MargineBridgeResponseDataModel GetMarginBridgeStatistics(MarginBridgeDataModel model, int userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileNameBase"></param>
        /// <returns></returns>
        string CreateFileName(string fileNameBase);

        MarginBridgeStatisticsModel CreateMarginBridgeStatisticsModel();

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        List<HierarchySelectionLevel> GetProductHierarchySelectionLevels();

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        List<HierarchySelectionLevel> GetProductHierarchySelectionLevelsWithAll();

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        List<HierarchySelectionLevel> GetLocationHierarchySelectionLevels();

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        List<HierarchySelectionLevel> GetLocationHierarchySelectionLevelsWithAll();

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        MarginBridgeIndexModel PrepareMarginBridgeIndexModel(MarginBridgeSession session);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<DataBridgeItem> PrepareDataBridgeData(MarginBridgeDataModel model, int userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        DataBridgeRawData PrepareDataBridgeRawData(MarginBridgeDataModel model, int userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="period"></param>
        /// <returns></returns>
        List<ReportSalesHistoryModel> PrepareRawSalesHistory(int userId, MarginBridgePeriod period);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataModel"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        MarginBridgeFilterModel PrepareMarginBridgeFilterModel(MarginBridgeDataModel dataModel, int userId);
    }
}