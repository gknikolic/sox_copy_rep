﻿
//$(".cld-day").on("click", function () {
//	alert("The paragraph was clicked.");
//});

$(document).ready(function () {
   
    BindCalendarEvents()
    $(".today").click();
});

var GetCalendarDayDetails = function () {
    var today = $(".today").html();
    var selectedDay = $(this).find('span').html();
    $(".calendarSelectedDay").removeClass("calendarSelectedDay");
    this.classList.add("calendarSelectedDay");
    today = today + ", " + selectedDay;
    GetCalendarDayDetailsAjax(today);
};

var UpdateCalendarEvents = function () {
    var today = $(".today").html();
    today = today + ", " + "1";
    $(".clearDataOnArrowClick").html("");

    $(".clearDataOnResubmitTimeTitle").html("Time");
    $(".clearDataOnResubmitTimeItems").html("");

    GetDashboardMonthEventsAjax(today);
};

function GetCalendarDayDetailsAjax(date) {
    //var date = $(this).datepicker('getDate');
    //date.setHours(12);

    app.blockUI('Please wait...');
    var filterModel = GetFilterModel(date);
    $.ajax({
        type: "POST",
        url: $("#GetCalendarDayDetailsUrl").val(),
        contentType: 'application/json',
        data: JSON.stringify(filterModel),
        success: function (response) {
            $("#DashboardHourPreviewPlaceholder").html(response);
            $(".clearDataOnResubmit").html("");
            $(".clearDataOnResubmitTimeTitle").html("Time");
            $(".clearDataOnResubmitTimeItems").html("");
            $(".clearDataOnResubmitCustomerTitle").html("Case");
            $(".clearDataOnResubmitCustomerName").html("");
        },
        error: function (response) {
            var error = 400;
        },
        complete: function () {
            $.unblockUI();
        }
    });
}

function GetCalendarHourDetails(element,date) {
    $(".flex-row-selected").removeClass("flex-row-selected");
    element.classList.add("flex-row-selected");
    GetCalendarHourDetailsAjax(date);
}


function GetCalendarHourDetailsAjax(date) {
    app.blockUI('Please wait...');
    var filterModel = GetFilterModel(date);
    $.ajax({
        type: "POST",
        url: $("#GetCalendarHourDetailsUrl").val(),
        contentType: 'application/json',
        data: JSON.stringify(filterModel),
        success: function (response) {
            $("#DashboardHourPreviewDetailsPlaceholder").html(response);
        },
        error: function (response) {
            var error = 400;
        },
        complete: function () {
            $.unblockUI();
        }
    });
}

function GetCalendarCaseDetails(element, id) {
    $(".flex-row-selected-case").removeClass("flex-row-selected-case");
    element.classList.add("flex-row-selected-case");
    GetCalendarCaseDetailsAjax(id);
}


function GetCalendarCaseDetailsAjax(id) {
    app.blockUI('Please wait...');

    $.ajax({
        type: "GET",
        url: $("#GetCalendarCaseDetailsUrl").val(),
        contentType: 'application/json',
        data: { caseId: id },
        success: function (response) {
            $("#DashboardCaseDetailsPlaceholder").html(response);
        },
        error: function (response) {
            var error = 400;
        },
        complete: function () {
            $.unblockUI();
        }
    });
}

function GetDashboardMonthEventsAjax(date) {
 
    app.blockUI('Please wait...');
    var filterModel = GetFilterModel(date);
    $.ajax({
        type: "POST",
        url: $("#GetDashboardMonthEventsUrl").val(),
        contentType: 'application/json',
        data: JSON.stringify(filterModel),
        success: function (response) {
            var p = response;
            var newEvents = [];
            if (response.Events.length > 0) {
                for (var i = 0; i < response.Events.length; i++) {
                     var event = {};
                    event.Title =  response.Events[i].EventHtmlString;
                     event.Date = new Date(response.Events[i].EventDate); //this new Date() is zero based for months
                     newEvents.push(event);
				}
            }
            //reinitialize calendar
            $("#caleandar").html("");
            var dateForCalendar = new Date(response.StartDate);
            caleandar(element, newEvents, settings, dateForCalendar);
            //calendarObj.Model = newEvents;
            BindCalendarEvents();
        },
        error: function (response) {
            var error = 400;
        },
        complete: function () {
            $.unblockUI();
        }
    });
}

function BindCalendarEvents() {
    var elements = document.getElementsByClassName("cld-day");
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', GetCalendarDayDetails, false);
    }
    var arrowelements = document.getElementsByClassName("cld-nav");
    for (var j = 0; j < arrowelements.length; j++) {
        arrowelements[j].addEventListener('click', UpdateCalendarEvents, false);
    }
}

function GetFilterModel(date) {
    var filterModel = {};
    filterModel.CaseReference = $("#CaseRef").val();
    filterModel.Surgeon = $("#Surgeon").val();
    filterModel.SelectedCustomers = $("#customerList").val();
    filterModel.SelectedVendors = $("#vendorList").val();
    filterModel.StartDate = date;
    return filterModel;
}
function clearSelected(elementId) {
    var elements = document.getElementById(elementId).options;

    for (var i = 0; i < elements.length; i++) {
        elements[i].selected = false;
    }

    $("#" + elementId).val($("#"+elementId+" option:first").val());
}
function ResetDashboardFilters() {
    //$('#customerList').find('option').prop('selected', false);
    //$('#vendorList').find('option').prop('selected', false);
    clearSelected("customerList");
    clearSelected("vendorList");
    $("#CaseRef").val("");
    $("#Surgeon").val("");
    UpdateCalendarEvents();
    $(".clearDataOnResubmit").html("");

    $(".clearDataOnResubmitTimeTitle").html("Time");
    $(".clearDataOnResubmitTimeItems").html("");
}