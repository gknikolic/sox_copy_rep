﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Data.Domain
{
    public class POUserProfile
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
    }
}
