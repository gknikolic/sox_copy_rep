﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Services
{
    public interface IPersistedDataService
    {
        string GetSelectedProject();

        string GetSelectedReporter();

        string GetSelectedAssignee();

        string GetSelectedComponent();

        List<string> GetSelectedMultipleComponents();

        void SetSelectedReporter(string reporter);

        void SetSelectedAssignee(string assignee);

        void SetSelectedComponent(string component);

        void SetSelectedProject(string project);

        void SetSelectedMultipleComponents(List<string> components);
    }
}