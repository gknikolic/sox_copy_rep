﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTHierarchyCorporateConfig : IEntityTypeConfiguration<QTHierarchyCorporate>
    {
        public void Configure(EntityTypeBuilder<QTHierarchyCorporate> builder)
        {
            builder.ToTable("QTHierarchyCorporateConfig");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Name).HasMaxLength(50);
        }
    }
}
