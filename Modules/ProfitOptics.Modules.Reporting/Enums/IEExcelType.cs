﻿using System.ComponentModel;

namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum IEExcelType
    {
        [Description("Locations")]
        Locations = 0,

        [Description("Products")]
        Products = 1,

        [Description("Sales")]
        Sales = 2
    }
}