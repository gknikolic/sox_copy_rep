﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCRequiredActionTypeConfig : IEntityTypeConfiguration<GCRequiredActionType>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCRequiredActionType> builder)
        {
            builder.ToTable("GCRequiredActionType");
            builder.HasKey(entity => entity.Id);
        }
    }
}
