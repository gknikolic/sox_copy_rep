USE [Sox]

BEGIN

  ALTER TABLE [Sox].[dbo].[GCCustomer]
  ADD [CustomerName] nvarchar(30);
  
  ALTER TABLE [Sox].[dbo].[GCCase]
  ADD [GCCustomerId] int,
  FOREIGN KEY ([GCCustomerId]) REFERENCES [dbo].[GCCustomer] ([Id]);

END