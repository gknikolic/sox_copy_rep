﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Linq;
using System.Net.Mail;
using System.Net;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.Core.Email
{

    /// <summary>
    /// Provieds an API for sending emails in both sync & async fashion, as well as sending emails with delays.
    /// </summary>
    public class Emailer : IEmailer
    {
        private readonly Settings.Email _settings;
        private readonly Entities _entities;

        public Emailer(Settings.Email settings,
            Entities entities)
        {
            _settings = settings;
            _entities = entities;

            var listener = _entities.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }

        /// <summary>
        /// Sends an email asynchronously and inserts a new <see cref="POEmail"/> record to the underlying database.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <see cref="ArgumentNullException"/>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        public async Task<bool> SendEmailAndWriteToDbAsync(List<string> to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null, List<string> cc = null)
        {
            var emailResult = await SendEmailAsync(to, subject, body, isHtml, attachments, cc);

            var email = CreateEmail(to, subject, body, isHtml, attachments);

            if (emailResult)
            {
                email.SentTime = DateTime.UtcNow;
            }

            var dbResult = await WriteEmailToDbAsync(email);

            return emailResult && dbResult > 0;
        }

        public async Task<bool> SendEmailAndWriteToDbAsync(string to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null, List<string> cc = null)
        {
            return await SendEmailAndWriteToDbAsync(new List<string> { to }, subject, body, isHtml, attachments, cc);
        }

        /// <summary>
        /// Sends an email synchronously and inserts a new <see cref="POEmail"/> record to the underlying database.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <see cref="ArgumentNullException"/>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        public bool SendEmailAndWriteToDb(List<string> to, string subject, string body, bool isHtml = false,
            List<EmailAttachment> attachments = null, List<string> cc = null)
        {
            var emailResult = SendEmail(to, subject, body, isHtml, attachments, cc);

            var email = CreateEmail(to, subject, body, isHtml, attachments);

            if (emailResult)
            {
                email.SentTime = DateTime.UtcNow;
            }

            var dbResult = WriteEmailToDb(email);

            return emailResult && dbResult > 0;
        }

        public bool SendEmailAndWriteToDb(string to, string subject, string body, bool isHtml = false,
            List<EmailAttachment> attachments = null, List<string> cc = null)
        {
            return SendEmailAndWriteToDb(new List<string> { to }, subject, body, isHtml, attachments, cc);
        }

        /// <summary>
        /// Sends an email synchronously.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <see cref="ArgumentNullException"/>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        public bool SendEmail(List<string> to, string subject, string body, bool isHtml = false,
            List<EmailAttachment> attachments = null, List<string> cc = null)
        {
            try
            {
                using (var smtp = GetSmtpClient())
                {
                    var message = GetMailMessage(to, subject, body, isHtml, attachments, cc);

                    smtp.Send(message);

                    return true;
                }
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new Exception("Failed to send email message.", e);
            }
        }

        /// <summary>
        /// Sends an email asynchronously.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <see cref="ArgumentNullException"/>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        public async Task<bool> SendEmailAsync(List<string> to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null, List<string> cc = null)
        {
            try
            {
                using (var smtp = GetSmtpClient())
                {
                    var message = GetMailMessage(to, subject, body, isHtml, attachments, cc);

                    await smtp.SendMailAsync(message);

                    return true;
                }
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new Exception("Failed to send email message.", e);
            }
        }

        /// <summary>
        /// Creates a <see cref="SmtpClient"/> object and configures it.
        /// </summary>
        /// <returns></returns>
        public SmtpClient GetSmtpClient()
        {
            var smtp = new SmtpClient(_settings.SmtpServer) { Timeout = 1000000 };

            if (!string.IsNullOrWhiteSpace(_settings.SmtpUsername))
            {
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(
                    _settings.SmtpUsername,
                    _settings.SmtpPassword);
                smtp.EnableSsl = _settings.SmtpUseSSL;
                smtp.Port = _settings.SmtpPort;
            }

            return smtp;
        }

        /// <summary>
        /// Creates a new <see cref="MailMessage"/> from the specified arguments.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <returns></returns>
        public MailMessage GetMailMessage(List<string> to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null, List<string> cc = null)
        {
            var message = new MailMessage
            {
                Sender = new MailAddress(_settings.SmtpFrom, _settings.SmtpFromName),
                From = new MailAddress(_settings.SmtpFrom),
                Subject = subject,
                Body = body,
                IsBodyHtml = isHtml
            };

            if (attachments != null && attachments.Any())
            {
                attachments.ForEach(attachment =>
                {
                    message.Attachments.Add(new Attachment(attachment.FileName, attachment.Type));
                });
            }

            if (to.Any())
            {
                message.To.Add(string.Join(",", to.Where(email => !string.IsNullOrWhiteSpace(email))));
            }
            else
            {
                throw new ArgumentException("The list of recipient emails can not be empty");
            }

            if (cc != null && cc.Any())
            {
                message.CC.Add(string.Join(",", cc.Where(email => !string.IsNullOrWhiteSpace(email))));
            }

            return message;
        }

        /// <summary>
        /// Sends an email aysnchronously. If the "dont send before" argument is specified, then the email will be sent with a delay - after the specified time.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="dontSendBefore"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        public async Task<bool> SendEmailWithDelayAsync(List<string> to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null, DateTime? dontSendBefore = null)
        {
            try
            {
                var email = CreateEmail(to, subject, body, isHtml, attachments, dontSendBefore);

                var result = await WriteEmailToDbAsync(email);

                return result > 0;
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new Exception("Error while attempting to send an email with delay.", e);
            }
        }

        /// <summary>
        /// Creates a <see cref="POEmail"/> object with specified arguments.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="dontSendBefore"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <returns></returns>
        public POEmail CreateEmail(List<string> to, string subject, string body, bool isHtml = false,
            List<EmailAttachment> attachments = null, DateTime? dontSendBefore = null)
        {
            if (!to.Any())
            {
                throw new ArgumentException("The list of recipient emails can not be empty");
            }
            var email = new POEmail
            {
                To = to.Aggregate((previous, next) => previous + ";" + next),
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                DontSendBefore = dontSendBefore,
                CreateTime = DateTime.UtcNow
            };

            if (attachments != null && attachments.Any())
            {
                email = FillEmailAttachments(email, attachments);
            }

            return email;
        }

        /// <summary>
        /// Fills the specified <see cref="POEmail"/> object with the specified <see cref="EmailAttachment"/> list.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="attachments"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <returns></returns>
        public POEmail FillEmailAttachments(POEmail email, List<EmailAttachment> attachments)
        {
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email), "Email can not be null");
            }

            if (attachments != null && attachments.Any())
            {
                attachments.ForEach(attachment =>
                {
                    email.POEmailAttachments.Add(new POEmailAttachment
                    {
                        FileName = attachment.FileName,
                        SourceFileName = attachment.SourceFileName,
                        Type = attachment.Type,
                        Disposition = attachment.Disposition
                    });
                });
            }

            return email;
        }

        /// <summary>
        /// Writes the specified <see cref="POEmail"/> object to the underlying database.
        /// </summary>
        /// <param name="email"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        public async Task<int> WriteEmailToDbAsync(POEmail email)
        {
            try
            {
                if (email == null)
                {
                    throw new ArgumentNullException(nameof(email), "Email can not be null");
                }
                _entities.POEmails.Add(email);

                var result = await _entities.SaveChangesAsync();

                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Failed to write entry into the database", e);
            }
        }

        /// <summary>
        /// Writes the specified <see cref="POEmail"/> object to the underlying database.
        /// </summary>
        /// <param name="email"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        public int WriteEmailToDb(POEmail email)
        {
            try
            {
                if (email == null)
                {
                    throw new ArgumentNullException(nameof(email), "Email can not be null");
                }
                _entities.POEmails.Add(email);

                var result = _entities.SaveChanges();

                return result;
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new Exception("Failed to write entry into the database", e);
            }
        }
    }
}
