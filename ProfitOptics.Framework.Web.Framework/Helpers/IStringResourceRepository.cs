﻿namespace ProfitOptics.Framework.Web.Framework.Helpers
{
    public interface IStringResourceRepository
    {
        string GetResource(string key);
    }
}
