﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Core.System;
using ProfitOptics.Framework.Web.Areas.AuditLog.Models;
using ProfitOptics.Framework.Web.Areas.AuditLog.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;

namespace ProfitOptics.Framework.Web.Areas.AuditLog.Controllers
{
    [Authorize(Roles = "admin,superadmin")]
    [Area("AuditLog")]
    public class ManageController : BaseController
    {
        #region Fields

        private readonly IAuditLogService _auditLogService;
        private readonly ITempFile _tempFile;

        #endregion Fields

        #region Ctor

        public ManageController(IAuditLogService auditLogService,
                                ITempFile tempFile)
        {
            _auditLogService = auditLogService;
            _tempFile = tempFile;
        }

        #endregion Ctor

        #region Methods

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ActionFriendlyNames()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetTable([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request,
            string username, string from, string to, int timeOffset, bool showAll)
        {
            var fromDateNullable = from.ConverToDateTimeNullable(StringExtensions.DateTimeFormat, timeOffset);

            var toDateNullable = to.ConverToDateTimeNullable(StringExtensions.DateTimeFormat, timeOffset);

            var data = _auditLogService.GetDataTablesData(request, username, fromDateNullable, toDateNullable, showAll);

            return new LargeJsonResult(data);
        }

        public IActionResult ExportToExcel(string username, DateTime? from, DateTime? to, int timeOffset)
        {
            var data = new List<AuditLogData>();

            var fromParsed = from?.AddMinutes(timeOffset);
            var toParsed = to?.AddMinutes(timeOffset);

            data = _auditLogService.GetExcelDataAudit(username, fromParsed, toParsed);

            foreach (var o in data)
            {
                o.Inserted = o.Inserted.Value.AddMinutes(-1 * timeOffset);
            }

            ExcelFile.ExportToExcel(_tempFile.GetFileName(), "Sheet1", data.OrderByDescending(q => q.Inserted).ToList(), true, true, "A1");
            var stream = _tempFile.ToMemoryStream();

            return File(stream, ExcelFile.ExcelContentType, "data.xlsx");
        }

        public IActionResult GetActionFriendlyNameData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request)
        {
            var data = _auditLogService.GetAuditLogFriendlyNameData(request);

            return new LargeJsonResult(data);
        }

        public IActionResult SaveFriendlyName(AuditLogFriendlyNameData model)
        {
            _auditLogService.SaveFriendlyName(model);

            return Json(new { status = "ok" });
        }

        #endregion Methods
    }
}