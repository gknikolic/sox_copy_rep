﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Atlassian.Jira;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ProfitOptics.Modules.Support.Models.ViewModels
{
    public class UpdateIssueViewModel
    {
        public UpdateIssueViewModel()
        {
        }

        public void InitializeComments(IEnumerable<Comment> comments)
        {
            CommentCount = comments.Count();
            Comments = new List<IssueComment>();
            foreach (var item in comments)
            {
                Comments.Add(new IssueComment
                {
                    CommentAuthor = item.Author,
                    CommentUpdatedBy = item.UpdateAuthor,
                    CommentBody = item.Body,
                    CommentCreated = item.CreatedDate,
                    CommentUpdated = item.UpdatedDate
                });
            }

            Comments.OrderBy(x => x.CommentUpdated);
        }

        public string IssueId { get; set; }

        public int CommentCount { get; set; }

        public bool CanEdit { get; set; }

        public string SupportMessage { get; set; }

        [Display(Name = "Description")]
        [Required]
        public string Description { get; set; }

        [Display(Name = "Summary")]
        [Required]
        public string Summary { get; set; }

        [Display(Name = "Epic Name")]
        public string EpicName { get; set; }

        [Display(Name = "Type")]
        [Required]
        public string Type { get; set; }

        [Display(Name = "Priority")]
        [Required]
        public string Priority { get; set; }

        [Display(Name = "Attachments")]
        public List<IssueAttachment> Attachments { get; set; }

        [Display(Name = "Comments")]
        public List<IssueComment> Comments { get; set; }

        [Display(Name = "Post Comment")]
        public string PostComment { get; set; }

        [Display(Name = "Attachments")]
        public IEnumerable<IFormFile> NewAttachments { get; set; }

        public string IssueKey { get; set; }

        public List<SelectListItem> TypeList { get; set; }
        public List<SelectListItem> PriorityList { get; set; }

        public void InitializeAttachments(IEnumerable<Attachment> attachments)
        {
            Attachments = new List<IssueAttachment>();
            foreach (var item in attachments)
            {
                Attachments.Add(new IssueAttachment
                {
                    FileName = item.FileName,
                    Id = item.Id
                });
            }
        }
    }

    public class IssueAttachment
    {
        public string FileName { get; set; }
        public string Id { get; set; }
    }

    public class IssueComment
    {
        public string CommentBody { get; set; }
        public string CommentAuthor { get; set; }
        public string CommentUpdatedBy { get; set; }
        public DateTime? CommentCreated { get; set; }
        public DateTime? CommentUpdated { get; set; }
    }
}