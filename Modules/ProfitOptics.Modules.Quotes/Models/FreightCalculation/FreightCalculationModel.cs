﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Quotes.Models.FreightCalculation
{
    public class FreightCalculationModel
    {
        public const decimal FuelSurchargeFactor = 1.14m;
        public const decimal GrossUpFactor = 1.15m;
        public int DestinationZipCode { get; set; }
        public int? OriginZipCode { get; set; }
        public List<FreightEntryModel> FreightList { get; set; }
    }
}