﻿using ProfitOptics.Framework.Etl.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public interface ICensiTrackService
    {
        ///// <summary>
        ///// Logs in user to CensiTrack and returns result boolean
        ///// </summary>
        ///// <param name="model">CTLogin model</param>
        ///// <returns>Returned result as a ETLCTLoginResponseModel</returns>
        //Task<ETLCTLoginResponseModel> Login(ETLCTLoginModel model);

        /// <summary>
        /// Fetches cases from CensiTrack
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="facilityId">Id of Facility</param>
        /// <returns>Returned list of CTCases</returns>
        Task<List<ETLCTCase>> GetCTCases(int clientId, long? facilityId);

        /// <summary>
        /// Fetches cases from CensiTrack
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <returns>Returned list of CTCases</returns>
        Task<List<ETLCTCase>> GetCTCasesV2(int clientId);

        ///// <summary>
        ///// Fetches facilities from CensiTrack
        ///// </summary>
        ///// <param name="clientId">Client ID represnts a environment</param>
        ///// <returns>Returned list of CTFacilities</returns>
        //Task<List<ETLCTFacility>> GetCTFacilities(int clientId);

        /// <summary>
        /// Fetches Container Services from CensiTrack
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <returns>Returned list of CTContainerServices</returns>
        Task<List<Models.ETLCTContainerService>> GetCTContainerServices(int clientId);

        /// <summary>
        /// Fetches Containers for provided ServiceId from CensiTrack.
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="facilityId">Id of Facility</param>
        /// <param name="serviceId">Id of Service</param>
        /// <returns>Returned list of CTContainerTypes</returns>
        Task<List<Models.ETLCTContainerType>> GetCTContainerTypesByService(int clientId, long facilityId, long serviceId);

        /// <summary>
        /// Fetches Containers for provided ContainerTypeId from CensiTrack.
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="containerTypeId">Id of ContainerType</param>
        /// <returns>Returned list of CTContainerTypeActuals</returns>
        Task<List<Models.ETLCTContainerTypeActual>> GetCTContainerTypeActualssByContainerTypes(int clientId, long containerTypeId);

        ///// <summary>
        ///// Fetches Products from CensiTrack
        ///// </summary>
        ///// <param name="clientId">Client ID represnts a environment</param>
        ///// <returns>Returned list of CTProducts</returns>
        //Task<List<Models.ETLCTProduct>> GetCTProducts(int clientId);

        /// <summary>
        /// Fetches Vendors from CensiTrack
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <returns>Returned list of CTVendors</returns>
        Task<List<Models.ETLCTVendor>> GetCTVendors(int clientId);

        /// <summary>
        /// Fetches Container Items for provided ContainerTypeId from CensiTrack.
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="containerTypeId">Id of ContainerType</param>
        /// <returns>Returned list of GetCTContainerTypeItems</returns>
        Task<List<Models.ETLCTContainerItem>> GetCTContainerTypeItems(int clientId, long containerTypeId);

        /// <summary>
        /// Fetches containers for a cases from CensiTrack
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="facilityId">Id of Facility</param>
        /// <param name="caseReference">Case reference</param>
        /// <param name="showReprocessingOnly">TBD.</param>
        /// <returns>Returned list of CTCaseContainers</returns>
        Task<List<Models.ETLCTCaseContainer>> GetCTCaseContainers(int clientId, long facilityId, string caseReference, bool? showReprocessingOnly = null);

        ///// <summary>
        ///// Fetches LocationTypes from CensiTrack
        ///// </summary>
        ///// <param name="clientId">Client ID represnts a environment</param>
        ///// <returns>Returned list of CTLocationTypes</returns>
        //Task<List<Models.ETLCTLocationType>> GetCTLocationTypes(int clientId);

        ///// <summary>
        ///// Fetches Locations, Areas, Buildings & Facilities from CensiTrack
        ///// </summary>
        ///// <param name="clientId">Client ID represnts a environment</param>
        ///// <returns>Returned lists of CTLocations, CTAreas, CTBuildings & CTFacilities</returns>
        //Task<Models.ETLCTLocationsResponse> GetCTLocations(int clientId);

        ///// <summary>
        ///// Fetches ContainerTypeActualHistoryItems from CensiTrack
        ///// </summary>
        ///// <param name="clientId">Client ID represnts a environment</param>
        ///// <param name="actualId">Actual ID</param>
        ///// <returns>Returned lists of ContainerTypeActualHistoryItems</returns>
        //Task<ETLCTContainerTypeActualHistoryResponse> GetCTContainerTypeActualHistoryItems(int clientId, long actualId);

        /// <summary>
        /// Fetches ContainerTypeActualHistoryItems from CensiTrack
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="actualId">Actual ID</param>
        /// <returns>Returned lists of ContainerTypeActualHistoryItems</returns>
        Task<List<ETLCTContainerTypeGraphic>> GetCTContainerTypeGraphic(int clientId, long containerTypeId);

        ///// <summary>
        ///// Fetches byte array of image from CensiTrack
        ///// </summary>
        ///// <param name="clientId">Client ID represnts a environment</param>
        ///// <param name="graphicId">Graphic ID</param>
        ///// <returns>Returned byte array</returns>
        //Task<byte[]> GetCTGraphicImage(int clientId, long graphicId);

        /// <summary>
        /// Fetches BI Load Results from CensiTrack
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <returns>Returned list of CTBILoadResults</returns>
        Task<List<ETLCTBILoadResult>> GetCTBILoadResults(int clientId);

        /// <summary>
        /// Fetches Contents for BI Load Result from CensiTrack for prodvided LoadId
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="loadId">loadId represnts an id of BILoadResult</param>
        /// <returns>Returned CTBILoadResultContent for provided result LoadId</returns>
        Task<ETLCTBILoadResultContentResponse> GetCTBILoadResultContents(int clientId, long loadId);

        /// <summary>
        /// Fetches Indicators for BI Load Result from CensiTrack for prodvided LoadId
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="loadId">loadId represnts an id of BILoadResult</param>
        /// <returns>Returned ETLCTBILoadResultIndicators for provided result LoadId</returns>
        Task<ETLCTBILoadResultIndicatorResponse> GetCTBILoadResultIndicators(int clientId, long loadId);

        /// <summary>
        /// Fetches Graphics for BI Load Result from CensiTrack for prodvided LoadId
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="loadId">loadId represnts an id of BILoadResult</param>
        /// <returns>Returned ETLCTBILoadResultGraphics for provided result LoadId</returns>
        Task<List<ETLCTBILoadResultGraphic>> GetCTBILoadResultGraphics(int clientId, long loadId);

        ///// <summary>
        ///// Fetches byte array of image from CensiTrack
        ///// </summary>
        ///// <param name="clientId">Client ID represnts a environment</param>
        ///// <param name="graphicId">Graphic ID</param>
        ///// <returns>Returned byte array</returns>
        //Task<byte[]> GetCTLoadResultGraphicImage(int clientId, long graphicId);

        /// <summary>
        /// Fetches Container type for provided LegacyId from CensiTrack.
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="facilityId">Id of ContainerType</param>
        /// <returns>Returned CTContainerType item</returns>
        Task<ETLCTContainerType> GetCTContainerTypeItem(int clientId, long legacyId);
        /// <summary>
        /// Fetch Container Assembly for Actual
        /// </summary>
        /// <param name="clientId">Client ID represnts a environment</param>
        /// <param name="legacyId">Id of ContainerType</param>
        /// <returns></returns>
        Task<ETLCTContainerAssembly> GetCTContainerAssemblyForActual(int clientId, long legacyId);

        /// <summary>
        /// Get Assembly Graphics for specific container type and set history Id
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="containerTypeId"></param>
        /// <param name="censisSetHistoryId"></param>
        /// <returns></returns>
        Task<ETLCTCurrentAssemblyGraphics> GetCTContainerAssemblyGraphics(int clientId, long containerTypeId, Guid censisSetHistoryId);
        /// <summary>
        /// Get Count sheets for specific  container type
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="containerTypeId"></param>
        /// <returns></returns>
        Task<ETLCTContainerAssemblyCountSheet> GetCTContainerAssemblyCountSheets(int clientId, long containerTypeId);
        Task<ETLCTContainerType> GetCTContainerTypesByService(int clientId, long value);
        Task<ETLCTContainerService> GetCTContainerServiceByLegacyId(int clientId, long serviceId);
    }
}
