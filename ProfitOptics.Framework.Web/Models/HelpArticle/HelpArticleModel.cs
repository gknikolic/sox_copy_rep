﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.ComponentModel.DataAnnotations;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.Web.Models.HelpArticle
{
    public class HelpArticleModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string RelatedUrls { get; set; }
        
        public string Tags { get; set; }

        public DateTime? EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}