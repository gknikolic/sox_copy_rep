﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportItemCategoryConfig : IEntityTypeConfiguration<ReportItemCategory>
    {
        public void Configure(EntityTypeBuilder<ReportItemCategory> builder)
        {
            builder.ToTable(nameof(ReportItemCategory));
            builder.HasKey(i => i.Id);

            builder.Property(i => i.Code).HasMaxLength(50);
            builder.Property(i => i.Name).HasMaxLength(50).IsRequired();
            builder.Property(i => i.StartDate).IsRequired();

            builder.HasMany(i => i.ReportSalesHistories)
                .WithOne(s => s.ReportItemCategory)
                .HasForeignKey(s => s.ItemCategoryId)
                .IsRequired(false);
        }
    }
}
