﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Sox.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ProfitOptics.Framework.Web.Areas.Dashboard.Controllers
{
    [Area("Dashboard")]
    [Authorize]
    public class DashboardController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IGroundControlService _groundControlService;

        public DashboardController(IUserService userService, IGroundControlService groundControlService)
        {
            _userService = userService;
            _groundControlService = groundControlService;
        }

        public IActionResult Index()
        {
            ClaimsTest();
            foreach (var item in HttpContext.User.Claims)
            {
                Debug.WriteLine($"Claim: {item.Type}, Value: {item.Value}");
            }
            
            return View();
        }

        private void ClaimsTest()
        {
            // Dragan UserId = 4037
            var userId = 4037;
            var dict = new Dictionary<string, string>();
            dict.Add(Framework.Enums.CompanyPermissionType.CompanyPermissionTypeView.ToString(), "11,22,33");
            //dict.Add(ProfitOptics.Modules.Sox.Enums.CompanyPermissionType.CompanyPermissionTypeEdit.ToString(), "4,5,6");
            //dict.Add(ProfitOptics.Modules.Sox.Enums.CompanyPermissionType.CompanyPermissionTypeAdd.ToString(), "7,8,9");
            //dict.Add(ProfitOptics.Modules.Sox.Enums.CompanyPermissionType.CompanyPermissionTypeDelete.ToString(), "10,11,12");
            _groundControlService.AddOrEditUserClaims(userId, dict);
        }
    }
}