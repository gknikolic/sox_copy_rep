﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Identity.Domain
{
    public class GCParentCompanyUsers
    {
        public int Id { get; set; }
        public int GCParentCompanyId { get; set; }
        public int UserId { get; set; }
        public virtual GCParentCompany GCParentCompany { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
