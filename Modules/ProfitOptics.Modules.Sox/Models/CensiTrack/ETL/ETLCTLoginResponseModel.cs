﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTLoginResponseModel
    {
        public bool LoginSuccessful { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
