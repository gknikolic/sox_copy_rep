﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportCustomerClassQuotaConfig : IEntityTypeConfiguration<ReportCustomerClassQuota>
    {
        public void Configure(EntityTypeBuilder<ReportCustomerClassQuota> builder)
        {
            builder.ToTable("ReportSalesChannelQuota");
            builder.HasKey(q => q.Id);

            builder.Property(q => q.CustomerClassId).HasColumnName("SalesChannelId");
            builder.Property(q => q.Month).IsRequired();
            builder.Property(q => q.Year).IsRequired();
            builder.Property(q => q.Revenue).HasColumnType("decimal(19,4)").IsRequired();

            builder.HasOne(q => q.ReportCustomerClass)
                .WithMany()
                .HasForeignKey(q => q.CustomerClassId)
                .IsRequired();

            builder.HasOne(q => q.ReportHierarchySalesRep)
                .WithMany()
                .HasForeignKey(q => q.SalesRepId)
                .IsRequired();
        }
    }
}
