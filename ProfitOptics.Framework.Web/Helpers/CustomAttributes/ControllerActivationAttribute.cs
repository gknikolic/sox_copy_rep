﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ProfitOptics.Framework.Core.Settings;
using System.Linq;

namespace ProfitOptics.Framework.Web.Helpers.CustomAttributes
{
    public class ControllerActivationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var area = filterContext.RouteData.Values["area"]?.ToString().ToLower();
            var settings = filterContext.HttpContext.RequestServices.GetService(typeof(Common)) as Common;
            var modules = settings.DisabledModules.Split(',').Select(x => x.ToLower());

            if (modules.Contains(area))
            {
                var result = new ObjectResult("Not Found")
                {
                    StatusCode = StatusCodes.Status404NotFound
                };
                filterContext.Result = result;
            }
            else
            {
                base.OnActionExecuting(filterContext);
            }
        }

    }
}