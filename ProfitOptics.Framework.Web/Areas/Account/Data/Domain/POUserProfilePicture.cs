﻿using ProfitOptics.Framework.Web.Framework.Identity.Domain;

namespace ProfitOptics.Framework.Web.Areas.Account.Data.Domain
{
    public partial class POUserProfilePicture
    {
        public int UserId { get; set; }
        public byte[] Data { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
