﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ProfitOptics.Framework.Web.Framework.Models.UserAssignments
{
    public class RoleAssignmentModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsUserInRole { get; set; }
        public bool? ImplementsIUserAssignment { get; set; }
        public int? SelectedRegion { get; set; }
        public int? SelectedZone { get; set; }
        public int? SelectedSalesRep { get; set; }
        public List<SelectListItem> Regions { get; set; }
        public List<SelectListItem> Zones { get; set; }
        public List<SelectListItem> SalesReps { get; set; }
    }
}
