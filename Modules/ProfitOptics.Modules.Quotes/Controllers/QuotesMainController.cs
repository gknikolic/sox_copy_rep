﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.Quotes.Factories;
using ProfitOptics.Modules.Quotes.Models;
using ProfitOptics.Modules.Quotes.Services;
using PrintModel = ProfitOptics.Modules.Quotes.Models.PrintModel;

namespace ProfitOptics.Modules.Quotes.Controllers
{
    [Area("Quotes")]
    [Authorize(Roles = "quoting,admin,superadmin")]
    public class QuotesMainController : BaseController
    {
        private readonly IQuoteService _quoteService;
        private readonly IQuoteFactory _quoteFactory;
        private readonly IWebHostEnvironment _host;

        public QuotesMainController(IQuoteService quoteService,
            IQuoteFactory quoteFactory,
            IWebHostEnvironment host)
        {
            _quoteService = quoteService;
            _quoteFactory = quoteFactory;
            _host = host;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult CreateQuote()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateQuote(CreateQuoteModel quote)
        {
            if (ModelState.IsValid)
            {
                quote.UserId = GetCurrentUserId();
                int createdQuoteId = _quoteService.CreateQuote(quote);
                return RedirectToAction("DetailQuote", "QuotesMain", new { area = "Quotes", quoteId = createdQuoteId });
            }

            return View(quote);
        }


        [HttpPost]
        public IActionResult UpdateQuote(UpdateQuoteModel quote)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var shouldRecalculateFreight = _quoteService.UpdateQuote(quote);

                    return Json(new { success = true, message = "Quote Successfully Updated", shouldRecalculateFreight = shouldRecalculateFreight });
                }
                return Json(new { success = false, message = "Invalid Input Data" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult UpdateQuoteDetails(UpdateQuoteDetailsModel quote)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var shouldRecalculateFreight = _quoteService.UpdateQuoteDetails(quote);

                    return Json(new { success = true, message = "Quote Successfully Updated", shouldRecalculateFreight = shouldRecalculateFreight });
                }
                return Json(new { success = false, message = "Invalid Input Data" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteQuote(int quoteId)
        {
            try
            {
                _quoteService.DeleteQuote(quoteId);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult UnarchiveQuote(int quoteId)
        {
            try
            {
                _quoteService.UnarchiveQuote(quoteId);
                return RedirectToAction("HeaderQuote", "QuotesMain", new { area = "Quotes" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult CloneQuote(int quoteId, string cloneName, int customerId)
        {
            try
            {
                var newQuoteId = _quoteService.CloneQuote(quoteId, cloneName, customerId);
                return Json(new { quoteId = newQuoteId });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult GetCustomersData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request)
        {
            var userId = GetCurrentUserId();

            var data = _quoteFactory.GetCustomerModels(request, userId);

            return new LargeJsonResult(data.ToDataTablesResponse(request));
        }

        [HttpPost]
        public JsonResult GetShipToZip(int shipToId)
        {
            var shipToZip = _quoteService.GetShipToZip(shipToId);
            return Json(new { shipToZip = shipToZip });
        }

        [HttpPost]
        public JsonResult GetQuotesData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request, bool showArchived, int urlQuoteId)
        {
            var userId = GetCurrentUserId();
            var data = _quoteService.GetQuoteModelData(userId, showArchived, urlQuoteId, request);
            return new LargeJsonResult(data.ToDataTablesResponse(request));
        }

        public IActionResult HeaderQuotePartial(int quoteId)
        {
            var userId = GetCurrentUserId();
            ViewBag.userId = userId;
            var model = _quoteService.GetQuoteHeader(userId, quoteId);
            if (model == null)
            {
                model = new QuoteHeaderModel();
            }
            return PartialView(model);
        }

        public IActionResult DetailQuote(int? quoteId)
        {
            var userId = GetCurrentUserId();
            ViewBag.userId = userId;

            if (quoteId == null)
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            var model = _quoteService.GetQuoteDetailsData((int)quoteId, userId);
            if (model == null)
            {
                model = new QuoteDetailsModel();
            }

            model.ShipTos = _quoteService.ShipToDropdown(userId, quoteId.Value);
            model.PriceListModel = new PriceListCodeModel
            {
                QuoteId = quoteId,
                PricingList = _quoteService.GetPricingListForDropdown(quoteId.Value)
            };
            model.StandingOrders = new StandingOrderListModel
            {
                QuoteId = quoteId,
                StandingOrderList = _quoteService.GetStandingOrderDropdown(quoteId.Value)
            };

            return View(model);
        }

        public IActionResult HeaderQuote(int? quoteId)
        {
            ViewBag.QuoteId = Convert.ToInt32(quoteId);

            return View();
        }

        public IActionResult DropDown()
        {
            return View();
        }
        public IActionResult QuoteApprovalRules()
        {
            return View();
        }

        public JsonResult CustomerFacingXls(int quoteId)
        {
            try
            {
                var handle = Guid.NewGuid().ToString();
                var downloadFileName = string.Format("Customer Facing XLS - {0:MM-dd-yyyy}.xlsx", DateTime.Today);

                var fileName = _host.ContentRootPath + string.Format("\\wwwroot\\{0}.xlsx", handle);

                var data = _quoteService.GetPrintQuoteItemsForQuote(GetCurrentUserId(), quoteId);
                ExcelFile.ExportToExcel<PrintModel.PrintQuoteItems>(fileName, "Sheet1", data, true, true);

                return Json(new { success = true, filename = downloadFileName, handle = handle });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        public IActionResult CustomerFacingNotExtended(int quoteId)
        {
            var userId = GetCurrentUserId();
            var model = _quoteService.GetDataForPrint(userId, quoteId);
            model.IsNotExtended = true;
            return View("OutputQuote", model);
        }

        public IActionResult CustomerFacingExtended(int quoteId)
        {
            var userId = GetCurrentUserId();
            var model = _quoteService.GetDataForPrint(userId, quoteId);
            return View("OutputQuote", model);
        }

        [HttpGet]
        public virtual IActionResult Download(string handle, string filename, bool inline = false)
        {
            var path = _host.ContentRootPath + string.Format("\\wwwroot\\{0}{1}", handle, Path.GetExtension(filename));
            var bytes = System.IO.File.ReadAllBytes(path);
            System.IO.File.Delete(path);

            new FileExtensionContentTypeProvider().TryGetContentType(filename, out string contentType);
            return new FileContentResult(bytes, contentType) { FileDownloadName = filename };
        }
    }
}