﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCImage
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Origin { get; set; }
        public byte[] Data { get; set; }
        public int? ParentId { get; set; }
        public int? Type { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual GCImage GCParentImage { get; set; }
        public virtual ICollection<GCImage> GCChildImages { get; set; }
    }
}
