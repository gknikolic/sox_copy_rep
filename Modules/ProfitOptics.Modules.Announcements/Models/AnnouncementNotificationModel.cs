﻿namespace ProfitOptics.Modules.Announcements.Models
{
    public class AnnouncementNotificationModel
    {
        public int Id { get; set; }
        public string UserProfilePictureBase64 { get; set; }
        public string CreatedAgo { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
