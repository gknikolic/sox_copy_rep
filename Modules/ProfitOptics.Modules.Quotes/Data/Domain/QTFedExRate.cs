using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTFedExRate")]
    public partial class QTFedExRate
    {
        public int Id { get; set; }

        public int OriginZip { get; set; }

        public int Ldzip { get; set; }

        public int Hdzip { get; set; }

        public decimal Class { get; set; }

        public decimal MinChg { get; set; }

        public decimal L5 { get; set; }

        public decimal M5C { get; set; }

        public decimal M1M { get; set; }

        public decimal M2M { get; set; }

        public decimal M5M { get; set; }

        public decimal M10M { get; set; }
    }
}
