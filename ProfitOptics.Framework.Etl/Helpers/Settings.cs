﻿
using ProfitOptics.Framework.Core.Settings;

namespace ProfitOptics.Framework.Etl.Helpers
{
    /// <summary>
    /// Provides methods for querying the database with custom SQL checks and generating email reports.
    /// </summary>
    public class Settings
    {
        public string DefaultConnection { get; set; }
        public string SourceFolder { get; set; }
        public string BackupFolder { get; set; }
        public string ImapServer { get; set; }
        public string ImapUsername { get; set; }
        public string ImapPassword { get; set; }
        public int    ImapPort { get; set; }
        public bool   ImapUseSSL { get; set; }
        public string FtpDirectoryUrl { get; set; }
        public string FtpUsername { get; set; }
        public string FtpPassword { get; set; }
        public string FtpDestinationFolder { get; set; }
        public string SqlFolder { get; set; }
        public int CopyFilesRetryCount { get; set; }
        public int CopyFilesRetryWaitTime { get; set; }
        public int ServerOffsetInMinsFromUTC { get; set; }
        public QuickBooksOptions QuickBooksOptions { get; set; }
    }
}
