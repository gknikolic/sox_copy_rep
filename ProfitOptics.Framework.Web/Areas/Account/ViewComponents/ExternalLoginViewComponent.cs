﻿using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Areas.Account.Factories;
using ProfitOptics.Framework.Web.Areas.Account.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.ViewComponents
{
    public class ExternalLoginViewComponent : ViewComponent
    {
        private readonly IAccountModelFactory _accountModelFactory;

        public ExternalLoginViewComponent(IAccountModelFactory accountModelFactory)
        {
            this._accountModelFactory = accountModelFactory;
        }
        public async Task<IViewComponentResult> InvokeAsync(string returnUrl, List<ExternalLoginProviderModel> externalLoginProviders, string externalLoginCallbackUrl)
        {            
            ExternalLoginListModel model = await _accountModelFactory.PrepareExternalLoginListAsync(returnUrl, externalLoginProviders, externalLoginCallbackUrl);
            return View(model);
        }
    }
}
