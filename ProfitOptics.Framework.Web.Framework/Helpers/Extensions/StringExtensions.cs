﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace ProfitOptics.Framework.Web.Framework.Helpers.Extensions
{
    public static class StringExtensions
    {
        //This format will parse the input string correctly
        public static string DateTimeFormat = "MM/dd/yyyy h:mm tt";

        public static DateTime? ConverToDateTimeNullable(this string value, string format, int offset)
        {
            DateTime? dateTimeNullable;
            DateTime dateTime;
            if (DateTime.TryParseExact(value, format,
                                       CultureInfo.InvariantCulture, DateTimeStyles.None,
                                       out dateTime))
                dateTimeNullable = dateTime.AddMinutes(offset);
            else
                dateTimeNullable = null;

            return dateTimeNullable;
        }

        public static string SplitCamelCase(this string nameSplit)
        {
            return Regex.Replace(nameSplit, "(\\B[A-Z])", " $1");
        }

        public static string GetDomainName(this string value)
        {
            return value.Contains('@') ? value.Substring(value.IndexOf('@') + 1) : string.Empty;
        }

        public static string ToJQueryClassSelector(this string value)
        {
            return $".{value}";
        }

        public static string ToJQueryIdSelector(this string value)
        {
            return $"#{value}";
        }
    }
}
