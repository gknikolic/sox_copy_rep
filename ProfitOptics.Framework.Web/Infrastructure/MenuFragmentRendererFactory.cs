﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using ProfitOptics.Framework.Web.Framework.Infrastructure;

namespace ProfitOptics.Framework.Web.Infrastructure
{
    public class MenuFragmentRendererFactory : IMenuFragmentRendererFactory
    {
        private readonly IMemoryCacheHelper _memoryCacheHelper;
        private readonly IModuleManager _moduleManager;
        private readonly IServiceProvider _serviceProvider;

        public MenuFragmentRendererFactory(IMemoryCacheHelper memoryCacheHelper, IModuleManager moduleManager, IServiceProvider serviceProvider)
        {
            this._memoryCacheHelper = memoryCacheHelper;
            this._moduleManager = moduleManager;
            this._serviceProvider = serviceProvider;
        }
        /// <inheritdoc />
        public IEnumerable<IMenuFragmentRenderer> CreateMenuFragmentRenderers()
        {
            var menuFragmentRenderers = Assembly.GetExecutingAssembly().GetExportedTypes()
                .Where(type =>
                    !type.IsInterface && !type.IsAbstract && typeof(IMenuFragmentRenderer).IsAssignableFrom(type))
                .Select(type => (IMenuFragmentRenderer) Activator.CreateInstance(type));
            
            //TODO: Framework Rewrite: We are not loading types from dynamic assemblies. We need to figure out a way to do that.
            var hostingEnvironment = (IWebHostEnvironment)_serviceProvider.GetService(typeof(IWebHostEnvironment));
            var assembliyList = _memoryCacheHelper.Get("externalAssemblies", () => _moduleManager.GetExternalModules(hostingEnvironment).ToList(), Int32.MaxValue);
            foreach (var assembly in assembliyList)
            {
                var externalMenuFragmentRenderers = assembly.GetTypes()
                    .Where(type =>
                        !type.IsInterface && !type.IsAbstract && typeof(IMenuFragmentRenderer).IsAssignableFrom(type))
                    .Select(type => (IMenuFragmentRenderer) Activator.CreateInstance(type));

                foreach (var externalMenuFragmentRenderer in externalMenuFragmentRenderers)
                {
                    if (!menuFragmentRenderers.Any(x => x.GetType() == externalMenuFragmentRenderer.GetType()))
                    {
                        menuFragmentRenderers = menuFragmentRenderers.Append(externalMenuFragmentRenderer);
                    }
                }
            }

            return menuFragmentRenderers.OrderBy(mfr => mfr.Order)
                .ToList(); 
        }
    }
}