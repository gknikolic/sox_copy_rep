﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable
{
    public class SalesRepRegionQuotaPerformanceDataModel
    {
        public int RegionId { get; set; }

        public string RegionName { get; set; }

        public RegionQuotaPerformanceSalesRepModel SalesRepData { get; set; }
    }
}