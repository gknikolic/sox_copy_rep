﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class CasePlanningModel
    {
        public IEnumerable<SelectListItem> WWVehicles { get; set; }
        public IEnumerable<SelectListItem> WWDrivers { get; set; }
    }
}
