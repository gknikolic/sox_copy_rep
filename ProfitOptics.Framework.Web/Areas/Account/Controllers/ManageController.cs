﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Areas.Account.Factories;
using ProfitOptics.Framework.Web.Areas.Account.Models;
using ProfitOptics.Framework.Web.Areas.Account.Services;
using ProfitOptics.Framework.Web.Helpers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Helpers;
using Twilio.Rest.Api.V2010.Account.AvailablePhoneNumberCountry;
using Twilio.Rest.Api.V2010.Account.Recording;
using Twilio.Rest.Api.V2010.Account.Usage.Record;
using QRCoder;
using System.Drawing;
using System.IO;
using System;

namespace ProfitOptics.Framework.Web.Areas.Account.Controllers
{
    [Authorize]
    [Area("Account")]
    public class ManageController : BaseController
    {
        #region Fields

        private readonly IAccountModelFactory _accountModelFactory;
        private readonly IAccountManagementService _accountManagementService;
        private readonly IStringResourceRepository _stringResourceRepository;

        #endregion

        #region Constructor

        public ManageController(IAccountModelFactory accountModelFactory,
            IAccountManagementService accountManagementService,
            IStringResourceRepository stringResourceRepository)
        {
            this._accountModelFactory = accountModelFactory;
            this._accountManagementService = accountManagementService;
            this._stringResourceRepository = stringResourceRepository;
        }

        #endregion

        #region Profile

        public async Task<IActionResult> Index(ManageMessageId? message)
        {
            AccountManagementModel model = await _accountModelFactory.PrepareAccountManagementModelAsync(message);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save(AccountManagementModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _accountManagementService.UpdateAccountInfoForCurrentUserAsync(model);
            if (result.Status == AccountManagementServiceResultStatusCode.UserNotFound)
            {
                return Challenge();
            }
            if (result.Succeeded)
            {
                return RedirectToAction("Index", ManageMessageId.ProfileUpdated);
            }

            ModelState.AddModelError("", result.Error);
            return View(model);
        }

        #endregion

        #region Phone number

        public IActionResult AddPhoneNumber()
        {
            AddPhoneNumberModel model = _accountModelFactory.PrepareAddPhoneNumberModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPhoneNumber(AddPhoneNumberModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _accountManagementService.SendPhoneVerificationCodeAsync(model.Number);

            if (result.Status == AccountManagementServiceResultStatusCode.UserNotFound)
            {
                return Challenge();
            }

            if (result.Succeeded)
            {
                return RedirectToAction("VerifyPhoneNumber", new { phoneNumber = model.Number });
            }

            model.ErrorMessage =_stringResourceRepository.GetResource(ResourceKeys.MessagesAccountManagementPhoneVerificationSendingFailed);

            return View(model);
        }

        public async Task<IActionResult> RemovePhoneNumber()
        {
            var result = await _accountManagementService.RemovePhoneNumberAsync();
            if (result.Status == AccountManagementServiceResultStatusCode.UserNotFound)
            {
                return Challenge();
            }

            if (result.Succeeded)
            {
                return RedirectToAction("Index", ManageMessageId.RemovePhoneSuccess);
            }
            return RedirectToAction("Index", ManageMessageId.Error);
        }

        public async Task<IActionResult> SetPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetPassword(SetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            AccountManagementServiceResult result = await _accountManagementService.SetPasswordAsync(model.Password);
            if (result.Status == AccountManagementServiceResultStatusCode.UserNotFound)
            {
                return Challenge();
            }

            if (result.Succeeded)
            {
                return RedirectToAction("Index", ManageMessageId.SetPasswordSuccess);
            }

            return RedirectToAction("Index", ManageMessageId.Error);
        }

        public IActionResult VerifyPhoneNumber(string phoneNumber)
        {
            if (phoneNumber == null)
            {
                return RedirectToAction("AddPhoneNumber");
            }
            VerifyPhoneNumberModel model = _accountModelFactory.PrepareVerifyPhoneNumberModel(phoneNumber);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> VerifyPhoneNumber(VerifyPhoneNumberModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await _accountManagementService.ChangePhoneNumberAsync(model.PhoneNumber, model.Code);
            if (result.Status == AccountManagementServiceResultStatusCode.UserNotFound)
            {
                return Challenge();
            }
            if (result.Succeeded)
            {
                return RedirectToAction("Index", new { message = ManageMessageId.AddPhoneSuccess });
            }
            ModelState.AddModelError("", result.Error);
            return View(model);
        }

        #endregion

        #region 2FA and Google Authenticator

        public ActionResult QRCode()
        {
            var qrText = Request.Query["id"];
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(qrText,
            QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(4);
            return base.File(BitmapToBytes(qrCodeImage), "image/jpeg");
        }

        private static Byte[] BitmapToBytes(Bitmap img)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ToggleTwoFactorAuthentication()
        {
            ManageMessageId message = ManageMessageId.Error;
            var result = await _accountManagementService.ToggleTwoFactorAuthenticationAsync();
            if (result.Succeeded)
            {
                message = ManageMessageId.SetTwoFactorSuccess;
            }
            return RedirectToAction("Index", new { message = message });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ToggleGoogleAuthenticator()
        {
            var googleAuthenticatorEnabledResult = await _accountManagementService.IsGoogleAuthenticatorEnabledAsync();
            if (googleAuthenticatorEnabledResult.Status == AccountManagementServiceResultStatusCode.UserNotFound)
            {
                return Challenge();
            }

            if (googleAuthenticatorEnabledResult.Succeeded)
            {
                if (googleAuthenticatorEnabledResult.Enabled)
                {
                    await _accountManagementService.DisableGoogleAuthenticatorAsync();
                    return RedirectToAction("Index", ManageMessageId.ProfileUpdated);
                }
                else
                {
                    GoogleAuthenticatorModel model = await _accountModelFactory.PrepareGoogleAuthenticatorModelAsync();
                    if (model != null)
                    {
                        return View("EnableGoogleAuthenticator", model);
                    }
                }
            }
            return RedirectToAction("Index", ManageMessageId.Error);
        }

        [HttpPost]
        public async Task<IActionResult> EnableGoogleAuthenticator(GoogleAuthenticatorModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (_accountManagementService.VerifyGoogleAuthenticator(model))
            {
                await _accountManagementService.SetAccountGoogleAuthenticatorAsync(model.SecretKey);
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", _stringResourceRepository.GetResource(ResourceKeys.MessagesAccountManagementGoogleAuthenticatorInvalidCode));
            return View(model);
        }

        #endregion

        #region Change password

        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            var result = await _accountManagementService.ChangePasswordAsync(model.CurrentPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return RedirectToAction("Index", new { message = ManageMessageId.ChangePasswordSuccess });
            }
            ModelState.AddModelError("", result.Error);
            return View(model);
        }

        #endregion

        #region External logins

        public async Task<IActionResult> ManageLogins(ManageMessageId message)
        {
            var model = await _accountModelFactory.PrepareManageLoginsModelAsync(message);
            return View(model);
        }

        public async Task<IActionResult> LinkLoginCallback()
        {
            var response = await HttpContext.AuthenticateAsync(IdentityConstants.ExternalScheme);
            var result = await _accountManagementService.AddExternalLoginAsync(response);
            if (result.Status == AccountManagementServiceResultStatusCode.UserNotFound)
            {
                return Challenge();
            }

            if (result.Succeeded)
            {
                return RedirectToAction("ManageLogins");
            }
            return RedirectToAction("ManageLogins", new { message = ManageMessageId.Error });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveLogin(ExternalLoginManageInfo model)
        {
            var result = await _accountManagementService.RemoveLoginAsync(model.LoginProvider, model.ProviderKey);
            if (result.Status == AccountManagementServiceResultStatusCode.UserNotFound)
            {
                return Challenge();
            }
            var message = result.Succeeded ? ManageMessageId.RemoveLoginSuccess : ManageMessageId.Error;
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        #endregion
    }
}