﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
	[Table("WWSettings")]
	public partial class WWSettings
	{
		public int Id { get; set; }
		public string TerritoryId { get; set; }


		public bool IsActive { get; set; }
	}
}
