﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class EventDetailsModel
	{
		public List<GCCaseModel> Cases { get; set; }
		public DateTime Date { get; set; }
		public string Description { get; set; }
		public string BtnColor { get; set; }

        //public DateTime DueTime { get; set; }
        //public string Surgeon { get; set; }
        //public string InstrumentTrays { get; set; }
    }


}
