USE [ProfitOptics.Framework]

DELETE FROM [ProfitOptics.Framework].[dbo].[ReportSalesChannelQuota] 
WHERE SalesRepId NOT IN (SELECT Id FROM ReportHierarchySalesRep)

DELETE FROM [ProfitOptics.Framework].[dbo].[ReportProductFamilyQuota] 
WHERE SalesRepId NOT IN (SELECT Id FROM ReportHierarchySalesRep)

DELETE FROM [ProfitOptics.Framework].[dbo].[ReportItemCategoryQuota] 
WHERE SalesRepId NOT IN (SELECT Id FROM ReportHierarchySalesRep)

DELETE FROM [ProfitOptics.Framework].[dbo].[ReportItemCategoryQuota] 
WHERE ItemCategoryId NOT IN (SELECT Id FROM ReportItemCategory)

DELETE FROM [ProfitOptics.Framework].[dbo].[ReportProductFamilyQuota] 
WHERE ProductFamilyId NOT IN (SELECT Id FROM ReportVendor)

DELETE FROM [ProfitOptics.Framework].[dbo].[ReportSalesChannelQuota] 
WHERE SalesChannelId NOT IN (SELECT Id FROM ReportCustomerClass)

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ReportItemCategory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ReportHierarchySalesRep SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO

IF (OBJECT_ID('dbo.FK_ReportItemCategoryQuota_ReportHierarchySalesRep', 'F') IS NULL)
BEGIN
ALTER TABLE dbo.ReportItemCategoryQuota ADD CONSTRAINT
	FK_ReportItemCategoryQuota_ReportHierarchySalesRep FOREIGN KEY
	(
	SalesRepId
	) REFERENCES dbo.ReportHierarchySalesRep
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
END

IF (OBJECT_ID('dbo.FK_ReportItemCategoryQuota_ReportItemCategory', 'F') IS NULL)
BEGIN
ALTER TABLE dbo.ReportItemCategoryQuota ADD CONSTRAINT
	FK_ReportItemCategoryQuota_ReportItemCategory FOREIGN KEY
	(
	ItemCategoryId
	) REFERENCES dbo.ReportItemCategory
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
END


ALTER TABLE dbo.ReportItemCategoryQuota SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ReportVendor SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ReportHierarchySalesRep SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO

IF (OBJECT_ID('dbo.FK_ReportProductFamilyQuota_ReportHierarchySalesRep', 'F') IS NULL)
BEGIN
ALTER TABLE dbo.ReportProductFamilyQuota ADD CONSTRAINT
	FK_ReportProductFamilyQuota_ReportHierarchySalesRep FOREIGN KEY
	(
	SalesRepId
	) REFERENCES dbo.ReportHierarchySalesRep
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 	
END

IF (OBJECT_ID('dbo.FK_ReportProductFamilyQuota_ReportVendor', 'F') IS NULL)
BEGIN
ALTER TABLE dbo.ReportProductFamilyQuota ADD CONSTRAINT
	FK_ReportProductFamilyQuota_ReportVendor FOREIGN KEY
	(
	ProductFamilyId
	) REFERENCES dbo.ReportVendor
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
END

ALTER TABLE dbo.ReportProductFamilyQuota SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ReportHierarchySalesRep SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ReportCustomerClass SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
IF (OBJECT_ID('dbo.FK_ReportSalesChannelQuota_ReportCustomerClass', 'F') IS NULL)
BEGIN
ALTER TABLE dbo.ReportSalesChannelQuota ADD CONSTRAINT
	FK_ReportSalesChannelQuota_ReportCustomerClass FOREIGN KEY
	(
	SalesChannelId
	) REFERENCES dbo.ReportCustomerClass
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
END
IF (OBJECT_ID('dbo.FK_ReportSalesChannelQuota_ReportHierarchySalesRep', 'F') IS NULL)
BEGIN
ALTER TABLE dbo.ReportSalesChannelQuota ADD CONSTRAINT
	FK_ReportSalesChannelQuota_ReportHierarchySalesRep FOREIGN KEY
	(
	SalesRepId
	) REFERENCES dbo.ReportHierarchySalesRep
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
END
ALTER TABLE dbo.ReportSalesChannelQuota SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
