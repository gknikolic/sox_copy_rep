﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCCaseColorStatusConfig : IEntityTypeConfiguration<GCCaseColorStatus>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCCaseColorStatus> builder)
        {
            builder.ToTable("GCCaseColorStatus");
            builder.HasKey(entity => entity.Id);
        }
    }
}
