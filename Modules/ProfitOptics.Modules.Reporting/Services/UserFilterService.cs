﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Services
{
    public class UserFilterService : IUserFilterService
    {
        private readonly Entities _context;

        public UserFilterService(Entities context)
        {
            _context = context;
        }

        /// <inheritdoc />
        public ReportUserFilter GetUserFilterById(int userFilterId)
        {
            if (userFilterId <= 0)
            {
                return null;
            }

            return _context.ReportUserFilter.SingleOrDefault(f => f.Id == userFilterId);
        }

        /// <inheritdoc />
        public ReportUserFilter GetDefaultReportFilterForUserId(int userId)
        {
            if (userId <= 0)
            {
                return null;
            }

            return _context.ReportUserFilter.SingleOrDefault(f => f.UserId == userId && f.IsDefault);
        }

        /// <inheritdoc />
        public IEnumerable<ReportUserFilter> GetAllReportFiltersForUserId(int userId, bool loadDefaultFilter = false)
        {
            if (userId <= 0)
            {
                return new List<ReportUserFilter>();
            }

            return _context.ReportUserFilter.Where(f => f.UserId == userId && f.IsDefault == loadDefaultFilter && f.EndDate == null).ToList();
        }

        /// <inheritdoc />
        public bool AddOrUpdateReportFilter(int userId, SaveReportFilterModel userFilter, out int filterId)
        {
            filterId = default(int);

            if (userId <= 0)
            {
                return false;
            }

            if (!string.IsNullOrWhiteSpace(userFilter.Name))
            {
                filterId = AddUserFilter(userId, userFilter);
            }

            ReportUserFilter defaultFilter = GetDefaultReportFilterForUserId(userId);
            
            if (defaultFilter == null)
            {
                AddDefaultUserFilter(userId, userFilter);
            }
            else
            {
                UpdateDefaultUserFilter(userFilter, defaultFilter);
            }

            return true;
        }

        /// <inheritdoc />
        public bool DeleteUserFilter(int userFilterId)
        {
            ReportUserFilter filter = GetUserFilterById(userFilterId);

            if (filter == null)
            {
                return false;
            }

            _context.ReportUserFilter.Remove(filter);

            return _context.SaveChanges() > 0;
        }

        /// <inheritdoc />
        public bool BulkRenamedUserFilters(IList<RenameFilterModel> filtersToRename)
        {
            if (!filtersToRename.Any())
            {
                return true;
            }

            foreach (RenameFilterModel renameFilterModel in filtersToRename)
            {
                ReportUserFilter filter = GetUserFilterById(renameFilterModel.Id);

                if (filter == null)
                {
                    continue;
                }

                filter.Name = renameFilterModel.Name;
            }

            return _context.SaveChanges() > 0;
        }

        private int AddUserFilter(int userId, SaveReportFilterModel userFilter)
        {
            var newFilter = new ReportUserFilter
            {
                UserId = userId,
                Name = userFilter.Name,
                Value = JsonConvert.SerializeObject(userFilter),
                IsDefault = false,
                StartDate = DateTime.Now
            };

            _context.ReportUserFilter.Add(newFilter);

            _context.SaveChanges();

            return newFilter.Id;
        }

        private void AddDefaultUserFilter(int userId, SaveReportFilterModel userFilter)
        {
            var defaultFilter = new ReportUserFilter
            {
                UserId = userId,
                Name = string.Empty,
                Value = JsonConvert.SerializeObject(userFilter),
                IsDefault = true,
                StartDate = DateTime.Now
            };

            _context.ReportUserFilter.Add(defaultFilter);

            _context.SaveChanges();
        }

        private void UpdateDefaultUserFilter(SaveReportFilterModel userFilter, ReportUserFilter defaultFilter)
        {
            defaultFilter.Value = JsonConvert.SerializeObject(userFilter);

            _context.SaveChanges();
        }
    }
}