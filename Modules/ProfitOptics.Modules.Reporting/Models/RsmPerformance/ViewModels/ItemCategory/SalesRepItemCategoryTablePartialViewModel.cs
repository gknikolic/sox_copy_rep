﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.PartialTableViewModels;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.ItemCategory
{
    public class SalesRepItemCategoryTablePartialViewModel : TablePartialViewModelBase
    {
        public List<SalesRepTablePartialViewModel<ItemCategoryQuotaPerformanceDataModel>> ItemCategories { get; set; }
    }
}