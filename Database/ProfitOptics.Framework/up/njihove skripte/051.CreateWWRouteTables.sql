USE [Sox]

BEGIN


  --DROP TABLE IF EXISTS [dbo].[WWDriver]
  CREATE TABLE [dbo].[WWDriver]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50),
	[Email] [varchar](50),
	[WWDriverId] [varchar](50) NOT NULL
  )
  --DROP TABLE IF EXISTS [dbo].[WWVehicle]
  CREATE TABLE [dbo].[WWVehicle]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[ExternalId] [varchar](50),
	[Notes] [varchar](500),
	[Available] [bit],
	[WWVehicleId] [varchar](50)  NOT NULL
  )
  
  --DROP TABLE IF EXISTS [dbo].[WWRoute]
  CREATE TABLE [dbo].[WWRoute]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[WWRouteId] [varchar](50),
	[Revision] [int],
	[Date] [varchar](50),
	[VehicleId] int not null,
	[DriverId] int null,
	FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[WWVehicle] ([Id]),
	FOREIGN KEY ([DriverId]) REFERENCES [dbo].[WWDriver] ([Id])
	
  )
  

  --DROP TABLE IF EXISTS[dbo].[WWRouteStep]
  CREATE TABLE [dbo].[WWRouteStep]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Type] [int] NOT NULL,
	[WWOrderId] [varchar](50)  NOT NULL,
	[WWRouteId] [int]  NOT NULL,
	FOREIGN KEY ([WWRouteId]) REFERENCES [dbo].[WWRoute] ([Id])
  )
  
  ALTER TABLE [dbo].[GCCase] ADD WWRouteId INT
  ALTER TABLE [dbo].[GCCase]  WITH CHECK ADD  CONSTRAINT [FK_GCCase_WWRoute] FOREIGN KEY([WWRouteId])
  REFERENCES [dbo].[WWRoute] ([Id])

END