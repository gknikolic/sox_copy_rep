﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class ProposedTrayLCHistoryModel
    {
        public int GCTrayId { get; set; }
        public string ActualName { get; set; }
        public int GCLifeCycleId { get; set; }
        public int GCTrayLifeCycleId { get; set; }
        public string GCLifeCycleName { get; set; }
        public DateTime DueTimestamp { get; set; }

        [Required]
        public int SelectedHI { get; set; }
        public List<SelectListItem> HistoryItemSelectList { get; set; }
    }
}
