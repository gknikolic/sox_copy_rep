﻿namespace ProfitOptics.Framework.Core.Settings
{
    public class ElmahIo
    {
        public string ApiKey { get; set; }
        public string LogId { get; set; }
    }
}
