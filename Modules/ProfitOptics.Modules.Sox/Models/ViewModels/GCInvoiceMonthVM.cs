﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCInvoiceMonthVM
    {
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
    }
}
