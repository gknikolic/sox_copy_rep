﻿namespace ProfitOptics.Framework.DataLayer
{
    public partial class POEmailAttachment
    {
        public long Id { get; set; }
        public long EmailId { get; set; }
        public string FileName { get; set; }
        public string SourceFileName { get; set; }
        public string Disposition { get; set; }
        public string Type { get; set; }

        public virtual POEmail Email { get; set; }
    }
}
