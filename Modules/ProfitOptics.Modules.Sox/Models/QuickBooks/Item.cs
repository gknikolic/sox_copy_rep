﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class Item
    {
        public string Name { get; set; }

        public string Sku { get; set; }

        public string Description { get; set; }

        public bool Active { get; set; }

        public bool SubItem { get; set; }

        public GeneralRef ParentRef { get; set; }

        public long Level { get; set; }

        public string FullyQualifiedName { get; set; }

        public bool Taxable { get; set; }

        public decimal UnitPrice { get; set; }

        public string Type { get; set; }

        public GeneralRef IncomeAccountRef { get; set; }

        public GeneralRef ExpenseAccountRef { get; set; }

        public GeneralRef AssetAccountRef { get; set; }

        public decimal PurchaseCost { get; set; }

        public bool TrackQtyOnHand { get; set; }

        public int? QtyOnHand { get; set; }

        [JsonProperty("domain")]
        public string Domain { get; set; }

        [JsonProperty("sparse")]
        public bool Sparse { get; set; }

        public long Id { get; set; }

        public string SyncToken { get; set; }

        public DateTime? InvStartDate { get; set; }

        //public MetaData MetaData { get; set; }
    }
}
