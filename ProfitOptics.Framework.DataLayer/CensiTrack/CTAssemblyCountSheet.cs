﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer
{
	[Table("CTAssemblyCountSheet")]
    public class CTAssemblyCountSheet
    {
        public int Id { get; set; }
        public long ContainerTypeId { get; set; }

        public string VendorName { get; set; }
        public string ModelNumber { get; set; }
        public string SetName { get; set; }

        public int TrayLifecycleStatusTypeId { get; set; }

		public int RequiredCount { get; set; }
        public int ActualCount { get; set; }
        public string Description { get; set; }

        public virtual GCTraysLifecycleStatusTypes TrayLifecycleStatusType { get; set; }
    }
}
