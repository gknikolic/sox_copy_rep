﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueStatus
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Self { get; set; }
    }
}