﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class VehiclesChanged
	{
		[JsonProperty("created")]
		public List<string> Created { get; set; }
		[JsonProperty("updated")]
		public List<string> Updated { get; set; }
		[JsonProperty("deleted")]
		public List<string> Deleted { get; set; }
	}
}
