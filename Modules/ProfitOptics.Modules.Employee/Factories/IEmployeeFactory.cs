﻿using ProfitOptics.Modules.Employee.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Employee.Factories
{
    public interface IEmployeeFactory
    {
        Task<EmployeeModel> PrepareEmployeeModelAsync(int id);

        Task<AllEmployeesModel> PrepareAllEmployeesModelAsync();

        Task<EmployeeModel> PrepareEmptyModelAsync();

        //Task<ManagerModel> PrepareManagerModelAsync();
    }
}