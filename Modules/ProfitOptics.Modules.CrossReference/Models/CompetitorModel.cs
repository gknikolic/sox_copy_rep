﻿namespace ProfitOptics.Modules.CrossReference.Models
{
    public class CompetitorModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Deleted { get; set; }
    }
}