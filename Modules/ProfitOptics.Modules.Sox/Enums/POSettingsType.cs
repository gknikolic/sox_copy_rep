﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Enums
{
    public enum POSettingsType
    {
        QuickBooksRefreshToken = 1,
        QuickBooksRefreshTokenExpiresAt = 2
    }
}
