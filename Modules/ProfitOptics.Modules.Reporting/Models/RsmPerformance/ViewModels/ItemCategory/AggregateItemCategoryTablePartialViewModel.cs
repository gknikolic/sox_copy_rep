﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.PartialTableViewModels;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.ItemCategory
{
    public class AggregateItemCategoryTablePartialViewModel : TablePartialViewModelBase
    {
        public List<ItemCategoryQuotaPerformanceDataModel> ItemCategories { get; set; }
    }
}