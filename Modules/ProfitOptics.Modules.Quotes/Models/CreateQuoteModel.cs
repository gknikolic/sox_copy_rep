﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class CreateQuoteModel
    {
        [Required(ErrorMessage = "Quote name is required.")]
        [MaxLength(50, ErrorMessage = "Quote name is limited to 50 characters.")]
        public string QuoteName { get; set; }

        [MaxLength(250, ErrorMessage = "Quote description is limited to 250 characters.")]
        public string QuoteDescription { get; set; }

        [Required(ErrorMessage = "Please select a customer.")]
        public int CustomerBillToId { get; set; }

        public int CustomerId { get; set; }
        public int? CustomerShipToId { get; set; }
        public int Id { get; set; }
        public int UserId { get; set; }
    }
}