﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public partial class ETLCTGraphicKey
    {
        [JsonProperty("graphicId")]
        public long GraphicId { get; set; }

        [JsonProperty("isGlobal")]
        public bool IsGlobal { get; set; }
    }
}
