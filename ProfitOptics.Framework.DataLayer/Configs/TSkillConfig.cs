﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.DataLayer.Test;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    class TSkillConfig : IEntityTypeConfiguration<TSkill>
    {
        public void Configure(EntityTypeBuilder<TSkill> builder)
        {
            builder.ToTable("TSkill");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(50).IsRequired();

            builder.HasMany(x => x.EmployeeSkills)
                .WithOne(x => x.Skill)
                .HasForeignKey(x => x.SkillId);
                
        }
    }
}
