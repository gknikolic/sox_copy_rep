﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Modules.BackgroundTasks.Data;
using ProfitOptics.Modules.BackgroundTasks.Data.Domain;

namespace ProfitOptics.Modules.BackgroundTasks.Services
{
    public class BackgroundTaskService : IBackgroundTaskService
    {
        private readonly Entities _entities;

        public BackgroundTaskService(Entities entities)
        {
            _entities = entities;
        }

        /// <inheritdoc />
        public async Task<List<BackgroundTask>> GetActiveTasksAsync(int userId)
        {
            return await _entities.BackgroundTasks.Where(x => x.UserId == userId && x.DateEnd != null).ToListAsync();
        }
    }
}