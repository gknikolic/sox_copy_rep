using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using ProfitOptics.Framework.Web.RestApi.DataLayer;
using ProfitOptics.Framework.Web.RestApi.Models.Auth;
using ProfitOptics.Framework.Web.RestApi.Services.Auth;
using ProfitOptics.Framework.Web.RestApi.Services.Customer;
using ProfitOptics.Framework.Web.RestApi.Services.Distributor;
using ProfitOptics.Framework.Web.RestApi.Services.Product;
using ProfitOptics.Framework.Web.RestApi.Services.User;
using ProfitOptics.Framework.Web.RestApiControllers.Config;
using ProfitOptics.Framework.Web.RestApiInfrastructure;
using ProfitOptics.Modules.Sox.Infrastructure;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Services;
using System;
using System.ComponentModel;
using TokenHandler = ProfitOptics.Framework.Web.RestApi.Services.Auth.TokenHandler;

namespace ProfitOptics.Framework.Web.RestApi
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IConfiguration Configuration => _configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            StartupConfiguration.ConfigureStartupConfig<Core.Settings.QuickBooksOptions>(services, _configuration);

            var connectionStrings = StartupConfiguration.ConfigureStartupConfig<ConnectionStrings>(services, _configuration);

            services.AddControllers()
                .AddNewtonsoftJson(
                    options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                )
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .ConfigureApiBehaviorOptions(options =>
                {
                    // Adds a custom error response factory when ModelState is invalid
                    options.InvalidModelStateResponseFactory = InvalidModelStateResponseFactory.ProduceErrorResponse;
                });

            services.AddApiVersioning(
                options => {
                    options.ReportApiVersions = true;
                    options.AssumeDefaultVersionWhenUnspecified = true;
                    options.DefaultApiVersion = new ApiVersion(1, 0);
                });
            services.AddSwaggerGen(
                c =>
                {
                    c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "ProfitOptics.Framework Rest Api", Version = "v1" });
                });

            services.AddDbContext<ApiEntitiesContext>(options => options.UseSqlServer(connectionStrings.ApiEntities));

            // Auth
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IJwtAuthenticationService, JwtAuthenticationService>();
            services.AddSingleton<IPasswordHasher, PasswordHasher>();
            services.AddScoped<ITokenHandler, TokenHandler>();        

            var signingConfigurations = new SigningConfigurations();
            services.AddSingleton(signingConfigurations);

            services.Configure<TokenOptions>(Configuration.GetSection("TokenOptions"));
            var tokenOptions = Configuration.GetSection("TokenOptions").Get<TokenOptions>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(jwtBearerOptions =>
                {
                    jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = tokenOptions.Issuer,
                        ValidAudience = tokenOptions.Audience,
                        IssuerSigningKey = signingConfigurations.Key,
                        ClockSkew = TimeSpan.Zero,
                        NameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"
                    };
                });

            // Entities
            services.AddScoped<IDistributorService, DistributorService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IProductService, ProductService>();

            services.AddAutoMapper(typeof(Startup));

			//services.AddDbContext<Framework.DataLayer.Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));
			//services.AddScoped<IWorkWaveService, WorkWaveService>();
			//services.AddScoped<IWorkWaveClient, WorkWaveClient>();
			//services.AddScoped<IQuickBooksClient, QuickBooksClient>();

			//Sox services
			//services.AddScoped<IWorkWaveClient, WorkWaveClient>();
			//services.AddScoped<ISoxService, SoxService>();
			var serviceRegistrator = new ApiServiceRegistrator();
			serviceRegistrator.RegisterServices(services, _configuration, new Core.Settings.ConnectionStrings() { AreaEntities = connectionStrings.SoxEntities });
		}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // swager configuration
            app.UseSwagger(c => c.SerializeAsV2 = true);
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProfitOptics.Framework Rest Api V1");
            });
        }
    }
}
