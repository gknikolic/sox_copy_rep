﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
	public partial class GCSettings
	{
		public int Id { get; set; }
		public string Environment { get; set; }
		public string VMAddress { get; set; }
		public string VMPickupStart { get; set; }
		public string VMPickupEnd { get; set; }
	}
}
