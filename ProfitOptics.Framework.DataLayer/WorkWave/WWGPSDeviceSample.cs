﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
	[Table("WWGPSDeviceSample")]
	public partial class WWGPSDeviceSample
	{
		public int Id { get; set; }
		public DateTime Timestamp { get; set; }
		public long Latitude { get; set; }
		public long Longitude { get; set; }
		public int Heading { get; set; }
		public int SpeedMs { get; set; }
		public string DeviceId { get; set; }
		public int WWGPSDeviceId { get; set; }
		public virtual WWGPSDevice WWGPSDevice{ get; set; }

	}
}
