﻿namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class EditVendorResponse : EditBase
    {
        public Vendor Vendor { get; set; }
    }
}
