﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.TokenizationSearch;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.MultiRowTable;
using ProfitOptics.Framework.Web.Framework.Models.Components;
using ProfitOptics.Modules.Showcase.Models;

namespace ProfitOptics.Modules.Showcase.Factories
{
    public class ShowcaseFactory : IShowcaseFactory
    {
        #region ProfitOptics MuiltiRow Table

        public PagedList<ProfitOpticsMultiRowTableLevelsExampleModel> GetProfitOpticsMultiRowTableLevelsExampleModel(MultiRowTableRequest request)
        {
            var records = GetProfitOpticsMultiRowTableLevelsExampleData();

            var pagedList = ProcessRequest(request, records);

            return pagedList;
        }

        public PagedList<ProfitOpticsMultiRowTableLevelsAjaxExampleModel> GetProfitOpticsMultiRowTableLevelsWithAjaxExampleModel(MultiRowTableRequest request)
        {
            var records = GetProfitOpticsMultiRowTableLevelsWithAjaxExampleData();

            var pagedList = ProcessRequest(request, records);

            return pagedList;
        }
        

        public PagedList<ProfitOpticsMultiRowTableLevelsAjaxExampleModel> GetProfitOpticsMultiRowTableLevelsExampleModelEmployees(MultiRowTableRequest request, string masterId)
        {
            var counter = 1;

            var model = new PagedList<ProfitOpticsMultiRowTableLevelsAjaxExampleModel>();
            for (int i = 0; i < 120; i++)
            {
                counter += 1;

                var parent = new ProfitOpticsMultiRowTableLevelsAjaxExampleModel
                {
                    Id = $"employee_{counter}",
                    TextValue = $"Employee { counter.ToString().PadLeft(3, '0') }",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    ParentId = masterId,
                    HasChildren = true
                };

                model.Add(parent);
            }

            return model;
        }

        public PagedList<ProfitOpticsMultiRowTableLevelsAjaxExampleModel> GetProfitOpticsMultiRowTableLevelsExampleModelOutsourced(MultiRowTableRequest request, string masterId)
        {
            var counter = 1;

            var model = new PagedList<ProfitOpticsMultiRowTableLevelsAjaxExampleModel>();
            for (int i = 0; i < 120; i++)
            {
                counter += 1;

                var parent = new ProfitOpticsMultiRowTableLevelsAjaxExampleModel
                {
                    Id = $"outsourced_{counter}",
                    TextValue = $"Outsourced { counter.ToString().PadLeft(3, '0') }",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    ParentId = masterId,
                    HasChildren = false
                };

                model.Add(parent);
            }

            return model;
        }

        private IQueryable<ProfitOpticsMultiRowTableLevelsAjaxExampleModel> GetProfitOpticsMultiRowTableLevelsWithAjaxExampleData()
        {
            var counter = 1;

            var model = new List<ProfitOpticsMultiRowTableLevelsAjaxExampleModel>();
            for (int i = 0; i < 120; i++)
            {
                counter += 1;

                var parent = new ProfitOpticsMultiRowTableLevelsAjaxExampleModel
                {
                    Id = $"manager_{counter}",
                    TextValue = $"Manager { counter.ToString().PadLeft(3, '0') }",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    HasChildren = true
                };

                model.Add(parent);
            }

            return model.AsQueryable();
        }

        private IQueryable<ProfitOpticsMultiRowTableLevelsExampleModel> GetProfitOpticsMultiRowTableLevelsExampleData()
        {
            var counter = 1;

            var model = new List<ProfitOpticsMultiRowTableLevelsExampleModel>();
            for (int i = 0; i < 120; i++)
            {
                counter += 1;

                var parent = new ProfitOpticsMultiRowTableLevelsExampleModel
                {
                    Id = $"manager_{counter}",
                    TextValue = $"Manager { counter.ToString().PadLeft(3, '0') }",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    Children = new List<ProfitOpticsMultiRowTableLevelsExampleModel>()
                };

                // load n childs
                Random rand = new Random();
                var n = rand.Next(2, 8);
                for (int j = 0; j < n; j++)
                {
                    var child1 = new ProfitOpticsMultiRowTableLevelsExampleModel
                    {
                        Id = $"child1_{counter}_{j}",
                        TextValue = "Employee " + (j + 1).ToString(),
                        DateValue = DateTime.Now.AddDays(-counter),
                        PercentValue = 0.1234 + counter / 10.0,
                        ParentId = parent.Id,
                        Children = new List<ProfitOpticsMultiRowTableLevelsExampleModel>()
                    };

                    parent.Children.Add(child1);

                    // load m childs
                    var m = rand.Next(2, 8);
                    for (int k = 0; k < m; k++)
                    {
                        var child2 = new ProfitOpticsMultiRowTableLevelsExampleModel
                        {
                            Id = $"child2_{counter}_{j}_{k}",
                            TextValue = "Outsourced " + (k + 1).ToString(),
                            DateValue = DateTime.Now.AddDays(-counter),
                            PercentValue = 0.1234 + counter / 10.0,
                            ParentId = child1.Id
                        };
                        child1.Children.Add(child2);
                    }
                }

                model.Add(parent);
            }

            return model.AsQueryable();
        }

        public PagedList<ProfitOpticsMultiRowTableExampleModel> GetProfitOpticsMultiRowTableExampleModel(MultiRowTableRequest request)
        {
            var records = GetProfitOpticsMultiRowTableExampleData();

            var pagedList = ProcessRequest(request, records);

            return pagedList;
        }

        private IQueryable<ProfitOpticsMultiRowTableExampleModel> GetProfitOpticsMultiRowTableExampleData()
        {
            var counter = 1;

            var model = new List<ProfitOpticsMultiRowTableExampleModel>();
            for(int i = 0; i < 120; i++)
            {
                model.Add(new ProfitOpticsMultiRowTableExampleModel
                {
                    Id = counter += 1,
                    TextValue = "John Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter,
                    Price = counter,
                    Notes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                });
            }

            return model.AsQueryable();
        }

        private PagedList<ProfitOpticsMultiRowTableLevelsAjaxExampleModel> ProcessRequest(MultiRowTableRequest request, IQueryable<ProfitOpticsMultiRowTableLevelsAjaxExampleModel> records)
        {
            // filter
            if (request.Search != null && !string.IsNullOrEmpty(request.Search))
            {
                var strings = Tokenizer.TokenizeToWords(request.Search);

                foreach (var str in strings)
                {
                    records = records.Where(x =>
                        (x.TextValue != null && x.TextValue.Contains(str)));
                }
            }

            string sortBy = request.curSort1 + " " + request.curSort1Order;
            if (request.curSort2 != null && request.curSort2 != "")
            {
                sortBy += "," + request.curSort2 + " " + request.curSort2Order;
            }

            // return data table response
            var totalCount = records.Count();

            var model = records.AsQueryable().OrderBy(sortBy).Skip(request.pageIndex * request.length).Take(request.length);

            return new PagedList<ProfitOpticsMultiRowTableLevelsAjaxExampleModel>(model.ToList(), totalCount);
        }

        private PagedList<ProfitOpticsMultiRowTableLevelsExampleModel> ProcessRequest(MultiRowTableRequest request, IQueryable<ProfitOpticsMultiRowTableLevelsExampleModel> records)
        {
            // filter
            if (request.Search != null && !string.IsNullOrEmpty(request.Search))
            {
                var strings = Tokenizer.TokenizeToWords(request.Search);

                foreach (var str in strings)
                {
                    records = records.Where(x =>
                        (x.TextValue != null && x.TextValue.Contains(str)));
                }
            }

            string sortBy = request.curSort1 + " " + request.curSort1Order;
            if (request.curSort2 != null && request.curSort2 != "")
            {
                sortBy += "," + request.curSort2 + " " + request.curSort2Order;
            }

            // return data table response
            var totalCount = records.Count();

            var model = records.AsQueryable().OrderBy(sortBy).Skip(request.pageIndex * request.length).Take(request.length);

            return new PagedList<ProfitOpticsMultiRowTableLevelsExampleModel>(model.ToList(), totalCount);
        }

        private PagedList<ProfitOpticsMultiRowTableExampleModel> ProcessRequest(MultiRowTableRequest request, IQueryable<ProfitOpticsMultiRowTableExampleModel> records)
        {
            // filter
            if (request.Search != null && !string.IsNullOrEmpty(request.Search))
            {
                var strings = Tokenizer.TokenizeToWords(request.Search);

                foreach (var str in strings)
                {
                    records = records.Where(x =>
                        (x.TextValue != null && x.TextValue.Contains(str)) ||
                        (x.Notes != null && x.Notes.Contains(str)));
                }
            }

            string sortBy = request.curSort1 + " " + request.curSort1Order;
            if (request.curSort2 != null && request.curSort2 != "")
            {
                sortBy += "," + request.curSort2 + " " + request.curSort2Order;
            }

            // return data table response
            var totalCount = records.Count();

            var model = records.AsQueryable().OrderBy(sortBy).Skip(request.pageIndex * request.length).Take(request.length);

            return new PagedList<ProfitOpticsMultiRowTableExampleModel>(model.ToList(), totalCount);
        }

        #endregion ProfitOptics MuiltiRow Table

        #region Data Tables

        public PagedList<DataTablesExampleModel> GetDataTablesExampleModels(IDataTablesRequest request)
        {
            var records = GetExampleData();

            var pagedList = ProcessRequest(request, records);

            return pagedList;
        }

        private IQueryable<DataTablesExampleModel> GetExampleData()
        {
            var counter = 1;
            return (new DataTablesExampleModel[]
            {
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "John Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },

                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "John Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "John Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "John Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
                new DataTablesExampleModel
                {
                    Id = counter,
                    TextValue = "Jane Doe",
                    DateValue = DateTime.Now.AddDays(-counter),
                    PercentValue = 0.1234 + counter / 10.0,
                    CurrencyValue = 12345.678M + counter++,
                    Notes = "Cras mollis ullamcorper ultrices"
                },
            })
            .AsQueryable();
        }

        private PagedList<DataTablesExampleModel> ProcessRequest(IDataTablesRequest request, IQueryable<DataTablesExampleModel> records)
        {
            // filter
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                var strings = Tokenizer.TokenizeToWords(request.Search.Value);

                foreach (var str in strings)
                {
                    records = records.Where(x =>
                        (x.TextValue != null && x.TextValue.Contains(str)) ||
                        (x.Notes != null && x.Notes.Contains(str)));
                }
            }

            // sort
            records = records.ApplySort(request);

            // page
            var paged = records.ApplyPagination(request).ToList();

            // return data table response
            var totalCount = records.Count();

            return new PagedList<DataTablesExampleModel>(paged, totalCount);
        }

        #endregion Data Tables

        #region Select2 Demo

        public List<Select2DemoModel> GetSelect2DemoData()
        {
            var result = new List<Select2DemoModel>();

            for (var i = 0; i < 10; i++)
            {
                result.Add(new Select2DemoModel
                {
                    StringProperty1 = $"Iteration {i}",
                    StringProperty2 = $"Iteration {i}",
                    StringProperty3 = $"Iteration {i}",
                    StringProperty4 = $"Iteration {i}"
                });
            }

            return result;
        }

        public IEnumerable<Select2DemoModel> GetSelect2DemoData(string search, int page, string filterName)
        {
            var result = new List<Select2DemoModel>();
            for (var i = 0; i < page; i++)
            {
                result.AddRange(GetSelect2DemoData());
            }

            return result;
        }

        #endregion Select2 Demo

        #region HierarchyCharts

        public HierarchyChartNodeModel GenerateExampleChartNode()
        {
            // temporary userImage
            var userImagePath = "../../wwwroot/AdminLTE/plugins/hierarchyChart/img/user.png";

            var earth = new HierarchyChartNodeModel
            {
                Title = "Earth",
                Subtitle = "Planet",
                EntityId = "0",
                //Image = userImagePath
            };

            var africa = new HierarchyChartNodeModel
            {
                Title = "Africa",
                Subtitle = "Continent",
                EntityId = "1",
                //Image = userImagePath
            };
            var egypt = new HierarchyChartNodeModel
            {
                Title = "Egypt",
                Subtitle = "Country",
                EntityId = "5",
                //Image = userImagePath
            };
            var europe = new HierarchyChartNodeModel
            {
                Title = "Europe",
                Subtitle = "Continent",
                EntityId = "2",
                //Image = userImagePath
            };

            var serbia = new HierarchyChartNodeModel
            {
                Title = "Serbia",
                Subtitle = "Country",
                EntityId = "3",
                //Image = userImagePath
            };
            var italy = new HierarchyChartNodeModel
            {
                Title = "Italy",
                Subtitle = "Country",
                EntityId = "4",
                //Image = userImagePath
            };

            europe.children.Add(serbia);
            europe.children.Add(italy);
            africa.children.Add(egypt);

            earth.children.Add(europe);
            earth.children.Add(africa);

            return earth;
        }

       

        #endregion HierarchyCharts
    }
}