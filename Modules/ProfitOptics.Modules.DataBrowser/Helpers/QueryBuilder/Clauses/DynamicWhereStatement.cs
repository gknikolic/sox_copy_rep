﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using Microsoft.Data.SqlClient;

namespace ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Clauses
{
    public class DynamicWhereStatement : WhereStatement
    {
        private readonly string whereStatement;

        public DynamicWhereStatement(string whereStatement)
        {
            this.whereStatement = whereStatement;
        }

        public override int ClauseLevels
        {
            get { return !string.IsNullOrEmpty(whereStatement) ? 1 : 0; }
        }

        public override string BuildWhereStatement()
        {
            return whereStatement ?? string.Empty;
        }

        public override string BuildWhereStatement(bool useCommandObject, ref SqlCommand usedDbCommand)
        {
            return whereStatement ?? string.Empty;
        }
    }
}