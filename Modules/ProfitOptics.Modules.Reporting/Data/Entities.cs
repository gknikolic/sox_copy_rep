﻿using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Modules.Reporting.Data.Configs;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data
{
    public class Entities : DbContext
    {
        public Entities(DbContextOptions<Entities> dbContextOptions) : base(dbContextOptions)
        {
            var listener = this.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AspNetUserConfig());
            modelBuilder.ApplyConfiguration(new ReportBillToConfig());
            modelBuilder.ApplyConfiguration(new ReportBuyGroupConfig());
            modelBuilder.ApplyConfiguration(new ReportCustomerClassConfig());
            modelBuilder.ApplyConfiguration(new ReportHierarchyCorporateConfig());
            modelBuilder.ApplyConfiguration(new ReportHierarchyRegionConfig());
            modelBuilder.ApplyConfiguration(new ReportHierarchyZoneConfig());
            modelBuilder.ApplyConfiguration(new ReportHierarchySalesRepConfig());
            modelBuilder.ApplyConfiguration(new ReportHolidayConfig());
            modelBuilder.ApplyConfiguration(new ReportItemCategoryConfig());
            modelBuilder.ApplyConfiguration(new ReportItemConfig());
            modelBuilder.ApplyConfiguration(new ReportSalesHistoryConfig());
            modelBuilder.ApplyConfiguration(new ReportShipToConfig());
            modelBuilder.ApplyConfiguration(new ReportVendorConfig());
            modelBuilder.ApplyConfiguration(new ReportUserFilterConfig());
            modelBuilder.ApplyConfiguration(new ReportUserMappingConfig());
            modelBuilder.ApplyConfiguration(new ReportUserLoginConfig());
            modelBuilder.ApplyConfiguration(new ReportCustomerClassQuotaConfig());
            modelBuilder.ApplyConfiguration(new ReportItemCategoryQuotaConfig());
            modelBuilder.ApplyConfiguration(new ReportVendorQuotaConfig());
            modelBuilder.ApplyConfiguration(new ReportRegionQuotaConfig());
            modelBuilder.ApplyConfiguration(new ReportQuotaConfig());

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.EnableSensitiveDataLogging();
        }

        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<ReportBillTo> ReportBillTo { get; set; }
        public virtual DbSet<ReportBuyGroup> ReportBuyGroup { get; set; }
        public virtual DbSet<ReportCustomerClass> ReportCustomerClasses { get; set; }
        public virtual DbSet<ReportHierarchyCorporate> ReportHierarchyCorporate { get; set; }
        public virtual DbSet<ReportHierarchyRegion> ReportHierarchyRegion { get; set; }
        public virtual DbSet<ReportHierarchySalesRep> ReportHierarchySalesRep { get; set; }
        public virtual DbSet<ReportHierarchyZone> ReportHierarchyZone { get; set; }
        public virtual DbSet<ReportHoliday> ReportHolidays { get; set; }
        public virtual DbSet<ReportItem> ReportItem { get; set; }
        public virtual DbSet<ReportQuota> ReportQuotas { get; set; }
        public virtual DbSet<ReportItemCategory> ReportItemCategory { get; set; }
        public virtual DbSet<ReportSalesHistory> ReportSalesHistory { get; set; }
        public virtual DbSet<ReportShipTo> ReportShipTo { get; set; }
        public virtual DbSet<ReportUserFilter> ReportUserFilter { get; set; }
        public virtual DbSet<ReportUserLogin> ReportUserLogin { get; set; }
        public virtual DbSet<ReportUserMapping> ReportUserMapping { get; set; }
        public virtual DbSet<ReportVendor> ReportVendor { get; set; }
        public virtual DbSet<ReportCustomerClassQuota> ReportCustomerClassQuotas { get; set; }
        public virtual DbSet<ReportItemCategoryQuota> ReportItemCategoryQuotas { get; set; }
        public virtual DbSet<ReportVendorQuota> ReportVendorQuotas { get; set; }
        public virtual DbSet<ReportRegionQuota> ReportRegionQuotas { get; set; }
    }
}
