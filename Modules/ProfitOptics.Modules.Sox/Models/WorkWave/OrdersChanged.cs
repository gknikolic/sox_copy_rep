﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class OrdersChanged
	{
		[JsonProperty("created")]
		public List<ChangedOrder> Created { get; set; }
		[JsonProperty("updated")]
		public List<ChangedOrder> Updated { get; set; }
		[JsonProperty("deleted")]
		public List<Guid> Deleted { get; set; }
	}
}
