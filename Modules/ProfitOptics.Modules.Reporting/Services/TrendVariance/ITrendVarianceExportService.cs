﻿using System;
using System.IO;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.TrendVariance;

namespace ProfitOptics.Modules.Reporting.Services.TrendVariance
{
    public interface ITrendVarianceExportService
    {
        /// <summary>
        /// Exports the customer item category variances for the specified customer identifier.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isBillTo"></param>
        /// <param name="customerId"></param>
        /// <param name="comparisonType"></param>
        /// <param name="comparisonStartDate"></param>
        /// <param name="comparisonEndDate"></param>
        /// <returns></returns>
        MemoryStream ExportCustomerDrillThroughVariances(int userId, bool isBillTo, int customerId, ComparisonType comparisonType,
            DateTime? comparisonStartDate, DateTime? comparisonEndDate);

        /// <summary>
        /// Exports the trend variances for the specified parameters.
        /// </summary>
        /// <param name="userId">User identifier</param>
        /// <param name="parameters">Trend variance parameters</param>
        /// <returns></returns>
        MemoryStream ExportVariancesByType(int userId, TrendVarianceVarianceByTypeRequestParameters parameters);
    }
}
