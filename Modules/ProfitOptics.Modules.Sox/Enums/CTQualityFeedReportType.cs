﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ProfitOptics.Modules.Sox.Enums
{
    public enum CTQualityFeedReportType
    {
        [Description("QA Pass")]
        QAPass = 1,

        [Description("QA Fail")]
        QAFail = 2,

        [Description("Tray Lock Broken")]
        TrayLockBroken = 3,

        [Description("Missing Instruments")]
        MissingInstruments = 4,

        [Description("Damaged Instruments")]
        DamagedInstruments = 5
    }
}
