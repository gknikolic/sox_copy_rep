﻿using System.ComponentModel;

namespace ProfitOptics.Framework.Web.Framework
{
    public enum SessionKeys
    {
        [Description("ProfilePicture")]
        ProfilePicture,

        [Description("AnnouncementList")]
        AnnouncementList,

        [Description("AnnouncementReloadTime")]
        AnnouncementReloadTime,

        [Description("AnnouncementUpdateTime")]
        AnnouncementUpdateTime,

        [Description("BackgroundTasks")]
        BackgroundTasks,

        [Description("EnabledSidebar")]
        EnabledSidebar,

        [Description("DataBrowserModel")]
        DataBrowserModel,

        [Description("AllJiraUsers")]
        MarginBridge
    }
}