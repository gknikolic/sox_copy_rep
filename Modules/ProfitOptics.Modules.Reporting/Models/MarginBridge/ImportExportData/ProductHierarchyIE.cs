﻿using ProfitOptics.Modules.Reporting.Attributes;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge.ImportExportData
{
    public class ProductHierarchyIE
    {
        [FieldTitle("Product Level 1")]
        public string ProductLevel1 { get; set; }

        [FieldTitle("Product Level 2")]
        public string ProductLevel2 { get; set; }

        [FieldTitle("Product Level 3")]
        public string ProductLevel3 { get; set; }

        [FieldTitle("Product Level 4")]
        public string ProductLevel4 { get; set; }

        [FieldTitle("Product Level 5")]
        public string ProductLevel5 { get; set; }

        [FieldTitle("Product Level 6")]
        public string ProductLevel6 { get; set; }

        [FieldTitle("Product Number")]
        public string ProductNumber { get; set; }

        public object this[string propertyName]
        {
            get => GetType().GetProperty(propertyName)?.GetValue(this, null);
            set => GetType().GetProperty(propertyName)?.SetValue(this, value, null);
        }
    }
}