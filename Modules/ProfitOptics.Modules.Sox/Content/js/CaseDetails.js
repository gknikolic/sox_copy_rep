﻿$(document).ready(function () {
    $('#instrumentSetsTable').DataTable();

    $(".category-expand").click(function () {
        var popupId = "#" + this.id + "Popup";
        $(popupId).toggle();
    });
    $("span.popUpCloseButton").click(function () {
        var target = document.getElementById(this.id).getAttribute("target-element");
        $("#" + target + "Popup").css("display", "none");
    });

    $('.select2-comment-tray').select2({
        dropdownParent: $('#commentModal')
    });
    $('.select2-comment-color').select2({
        dropdownParent: $('#commentModal')
    });

    $("#toggleCommentsCheckbox").change(function () {
        $("#allComments").prop("hidden", !this.checked);
        $("#userComments").prop("hidden", this.checked);
    });
});


$(function () {
    // Enables popover
    $(".additional-popover").popover({
        html: true,
        content: function () {
            var contentId = "#" + this.id + "popover-content"
            return $(contentId).html();
        },
        title: function () {
            var contentId = "#" + this.id + "popover-title"
            return $(contentId).html();
        }
    });
  
});

var colorCommentShow = function () {
    $('#commentModal').find('#Title').val('');
    $('#commentModal').find('#Description').val('');
    $('#commentModal').find('#RequiresResolution').prop('checked', false);
    $('#commentModal').find('#GCTrays').val('');
    $('#commentModal').find('#GCTrays').trigger('change');
    $('#commentModal').find('#GCColors').val('');
    $('#commentModal').find('#GCColors').trigger('change');
    $('#commentModal').modal('show');
}

var saveColorComment = function () {
    $('#commentModal').find('#GCTrayId').val($('#commentModal').find('#GCTrays').val());
    $('#commentModal').find('#GCColorId').val($('#commentModal').find('#GCColors').val());
    debugger;
    $('#colorStatusForm').submit();
}

//var colorCommentShow = function () {

//    $('#commentModal').modal('show');
//}

app.activeMenus = ["menuDashboard"];


