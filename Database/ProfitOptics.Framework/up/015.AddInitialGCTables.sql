USE [Sox]

BEGIN
 
CREATE TABLE GCCase
(
	Id int primary key IDENTITY(1,1),
	Title varchar(100) not null	
)

CREATE TABLE GCSet
(
	Id int primary key IDENTITY(1,1),
	Title varchar(100) not null,
	CaseId int,
	CONSTRAINT FK_Case FOREIGN KEY(CaseId) REFERENCES GCCase(Id)
)

CREATE TABLE GCContainerType
(
	Id int primary key IDENTITY(1,1),
	Title varchar(100) not null	
)

CREATE TABLE GCContainer
(
	Id int primary key IDENTITY(1,1),
	Title varchar(100) not null,
	ContainerTypeId int,
	SetId int,
	CONSTRAINT FK_ContainerType FOREIGN KEY(ContainerTypeId) REFERENCES GCContainerType(Id),
	CONSTRAINT FK_Set FOREIGN KEY(SetId) REFERENCES GCSet(Id)
)

CREATE TABLE GCProductType
(
	Id int primary key IDENTITY(1,1),
	Title varchar(100) not null	
)

CREATE TABLE GCProduct
(
	Id int primary key IDENTITY(1,1),
	Title varchar(100) not null,
	productTypeId int,
	ContainerId int,
	CONSTRAINT FK_productType FOREIGN KEY(productTypeId) REFERENCES GCProductType(Id),
	CONSTRAINT FK_Container FOREIGN KEY(ContainerId) REFERENCES GCContainer(Id)
)
 END
