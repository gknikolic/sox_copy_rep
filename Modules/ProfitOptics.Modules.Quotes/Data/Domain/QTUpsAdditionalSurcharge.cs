using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTUpsAdditionalSurcharge")]
    public partial class QTUpsAdditionalSurcharge
    {
        public int Id { get; set; }

        public int Ldzip { get; set; }

        public int Hdzip { get; set; }

        public decimal Surcharge { get; set; }
    }
}
