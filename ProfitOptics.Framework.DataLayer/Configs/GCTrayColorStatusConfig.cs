﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCTrayColorStatusConfig : IEntityTypeConfiguration<GCTrayColorStatus>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCTrayColorStatus> builder)
        {
            builder.ToTable("GCTrayColorStatus");
            builder.HasKey(entity => entity.Id);
        }
    }
}
