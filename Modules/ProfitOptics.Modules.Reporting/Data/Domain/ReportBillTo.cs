using System;
using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportBillTo : IFilterableEntity
    {
        public ReportBillTo()
        {
            ReportSalesHistories = new HashSet<ReportSalesHistory>();
            ReportShipTos = new HashSet<ReportShipTo>();
        }

        [Select2Id]
        public int Id { get; set; }

        public int? SalesRepId { get; set; }
        
        public string BillToNum { get; set; }
        
        public string BillToName { get; set; }

        [Select2Text]
        public string FullBillToName => $"{BillToName} ({BillToNum})";

        public string Address { get; set; }
        
        public string City { get; set; }

        public string State { get; set; }
        
        public string Zip { get; set; }
        
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual ReportHierarchySalesRep ReportHierarchySalesRep { get; set; }
        
        public virtual ICollection<ReportSalesHistory> ReportSalesHistories { get; set; }
        
        public virtual ICollection<ReportShipTo> ReportShipTos { get; set; }
    }
}
