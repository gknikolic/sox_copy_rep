﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.PartialTableViewModels;
using ProfitOptics.Modules.Reporting.Services;
using ProfitOptics.Modules.Reporting.Services.RsmPerformance;

namespace ProfitOptics.Modules.Reporting.Factories.RsmPerformance
{
    public class RsmPerformanceModelFactory : IRsmPerformanceModelFactory
    {
        #region Fields

        private readonly IRsmPerformanceService _rsmPerformanceService;
        private readonly ReportingSettings _reportingSettings;
        private readonly IDaysOffService _daysOffService;
        private readonly IRsmSalesAndQuotaService _rsmSalesAndQuotaService;
        private readonly IUserHierarchyService _userHierarchyService;
        private readonly IUserMappingService _userMappingService;
        private IEnumerable<string> AllowedItemCategories => _reportingSettings.AllowedItemCategories.Split(',');
        private IEnumerable<string> AllowedProductFamilies => _reportingSettings.AllowedProductFamilies.Split(',');

        #endregion Fields

        #region Ctor

        public RsmPerformanceModelFactory(IRsmPerformanceService rsmPerformanceService,
            ReportingSettings reportingSettings,
            IDaysOffService daysOffService,
            IRsmSalesAndQuotaService rsmSalesAndQuotaService,
            IUserHierarchyService userHierarchyService,
            IUserMappingService userMappingService)
        {
            _rsmPerformanceService = rsmPerformanceService;
            _reportingSettings = reportingSettings;
            _daysOffService = daysOffService;
            _rsmSalesAndQuotaService = rsmSalesAndQuotaService;
            _userHierarchyService = userHierarchyService;
            _userMappingService = userMappingService;
        }

        #endregion Ctor

        #region Item Category/Family Name methods

        /// <summary>
        /// Gets the aggregate item category data.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public async Task<List<ItemCategoryQuotaPerformanceDataModel>> GetAggregatePerformanceForItemCategoryAsync(int year, int month, int? regionId = null)
        {
            MonthDetails monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportItemCategoryQuota>> quotaQuery = _rsmSalesAndQuotaService.GetItemCategoryQuotaGroupedByItemCategoryId(year);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedByItemCategoryId(year, month, regionId: regionId);

            var joinedList = await quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToListAsync();

            var quotasAndSalesGroupedByItemCategoryId = joinedList.Select(x => new
            {
                Quotas = x.Quota
                    .Where(z => AllowedItemCategories.Contains(z.ReportItemCategory.Name))
                    .Select(z => new
                    {
                        z.ItemCategoryId,
                        ItemCategoryName = z.ReportItemCategory.Name,
                        z.Month,
                        z.Revenue
                    }),
                Sales = x.Sale
                    .Where(z => AllowedItemCategories.Contains(z.ReportItemCategory.Name))
                    .Select(y => new
                    {
                        y.ItemCategoryId,
                        ItemCategoryName = y.ReportItemCategory.Name,
                        InvoiceYear = y.InvoiceDate.Year,
                        InvoiceMonth = y.InvoiceDate.Month,
                        y.InvoiceDate,
                        y.ExtPrice,
                        y.Qty
                    })
            });

            var itemCategoryPerformance = (from quotasAndSales in quotasAndSalesGroupedByItemCategoryId
                                           let quotas = quotasAndSales.Quotas
                                           let sales = quotasAndSales.Sales
                                           select new ItemCategoryQuotaPerformanceDataModel
                                           {
                                               SalesRepId = 0,
                                               ItemCategoryId = quotas.FirstOrDefault()?.ItemCategoryId ?? 0,
                                               ItemCategoryName = quotas.FirstOrDefault()?.ItemCategoryName ?? string.Empty,
                                               MtdActualRevenue = sales.Any(z => z.InvoiceMonth == month) ?
                                                    sales.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) : 0.0m,
                                               MtdActualUnits = sales.Any(z => z.InvoiceMonth == month) ?
                                                    sales.Where(z => z.InvoiceMonth == month).Sum(x => x.Qty) : 0.0m,
                                               MtdTotalMonthQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                                    quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) : 0.0m,
                                               ProjectedAvgRevPerDay = sales.Any(z => z.InvoiceMonth == month) ?
                                                    (monthDetails.WorkDaysToDateCount == 0 ? 0 : sales.Where(z => z.InvoiceMonth == month)
                                                    .Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) : 0.0m,
                                               ProjectedRevRemainingDays = sales.Any(z => z.InvoiceMonth == month) ?
                                                    (monthDetails.WorkDaysToDateCount == 0 ? 0 : sales.Where(z => z.InvoiceMonth == month)
                                                    .Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) * monthDetails.RemainingWorkDaysCount : 0.0m,
                                               YtdActualRevenue = sales.Any(z => z.InvoiceYear == year &&
                                                    z.InvoiceDate < DateTime.Now) ?
                                                    sales.Where(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now)
                                                    .Sum(x => x.ExtPrice) : 0.0m,
                                               YtdActualUnits = sales.Any(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now) ?
                                                    sales.Where(z => z.InvoiceYear == year &&
                                                    z.InvoiceDate < DateTime.Now).Sum(x => x.Qty) : 0.0m,
                                               YtdQuota = quotas.Any() ?
                                                    (monthDetails.PreviousMonthsNames.Any() ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month)).Sum(x => x.Revenue) : 0.0m) +
                                                    (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                                    month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                                    (monthDetails.WorkDaysCount == 0 ? 0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName)
                                                    .Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m)
                                                    : 0.0m
                                           }).ToList();

            itemCategoryPerformance = _rsmPerformanceService.OrderItemsAccordingToAppSetting(itemCategoryPerformance, monthDetails, 0, year, month);

            itemCategoryPerformance = _rsmPerformanceService.AddTotalsRowForItemCategory(itemCategoryPerformance);

            return itemCategoryPerformance;
        }

        /// <summary>
        /// Gets the sales rep data for item category.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public async Task<List<SalesRepTablePartialViewModel<ItemCategoryQuotaPerformanceDataModel>>> GetSalesRepItemCategoryPerformanceAsync(int year, int month, int? regionId = null)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportItemCategoryQuota>> quotaQuery = _rsmSalesAndQuotaService.GetItemCategoryQuotaGroupedBySalesRepId(year);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedBySalesRepId(year, month, regionId: regionId);

            var joinedList = await quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToListAsync();

            var quotasAndSalesGroupedBySalesRepId = joinedList.Select(x => new
            {
                Quotas = x.Quota
                    .Where(z => AllowedItemCategories.Contains(z.ReportItemCategory.Name))
                    .Select(z => new
                    {
                        z.SalesRepId,
                        SalesRepName = z.ReportHierarchySalesRep.Name,
                        z.ItemCategoryId,
                        ItemCategoryName = z.ReportItemCategory.Name,
                        z.Month,
                        z.Revenue
                    }),
                Sales = x.Sale
                    .Where(z => AllowedItemCategories.Contains(z.ReportItemCategory.Name))
                    .Select(y => new
                    {
                        y.SalesRepId,
                        SalesRepName = y.ReportHierarchySalesRep.Name,
                        y.ItemCategoryId,
                        ItemCategoryName = y.ReportItemCategory.Name,
                        InvoiceYear = y.InvoiceDate.Year,
                        InvoiceMonth = y.InvoiceDate.Month,
                        y.InvoiceDate,
                        y.ExtPrice,
                        y.Qty
                    })
            }).ToList();

            var salesRepPerformance = (from quotasAndSales in quotasAndSalesGroupedBySalesRepId
                                       let quotas = quotasAndSales.Quotas
                                       let sales = quotasAndSales.Sales
                                       select new SalesRepTablePartialViewModel<ItemCategoryQuotaPerformanceDataModel>
                                       {
                                           SalesRepId = quotas.Any()
                                               ? quotas.Select(x => x.SalesRepId).FirstOrDefault()
                                               : 0,
                                           SalesRepName = quotas.Any()
                                               ? quotas.Select(x => x.SalesRepName).FirstOrDefault()
                                               : string.Empty,
                                           Items = (from q1 in quotas.GroupBy(x => x.ItemCategoryId)
                                                    join s1 in sales.GroupBy(x => x.ItemCategoryId ?? 0) on q1.Key equals s1.Key
                                                    where s1.Key != 0
                                                    select new ItemCategoryQuotaPerformanceDataModel
                                                    {
                                                        SalesRepId = q1.Any()
                                                            ? q1.Select(x => x.SalesRepId).FirstOrDefault()
                                                            : 0,
                                                        ItemCategoryId = q1.Key,
                                                        ItemCategoryName = q1.FirstOrDefault()?.ItemCategoryName ?? string.Empty,
                                                        MtdActualRevenue = s1.Any(z => z.InvoiceMonth == month)
                                                            ? s1.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice)
                                                            : 0.0m,
                                                        MtdActualUnits = s1.Any(z => z.InvoiceMonth == month)
                                                            ? s1.Where(z => z.InvoiceMonth == month).Sum(x => x.Qty)
                                                            : 0.0m,
                                                        MtdTotalMonthQuota = q1.Any(z => z.Month == monthDetails.CurrentMonthName)
                                                            ? q1.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue)
                                                            : 0.0m,
                                                        ProjectedAvgRevPerDay = s1.Any(z => z.InvoiceMonth == month)
                                                            ? (monthDetails.WorkDaysToDateCount == 0
                                                                ? 0
                                                                : s1.Where(z => z.InvoiceMonth == month)
                                                                      .Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount)
                                                            : 0.0m,
                                                        ProjectedRevRemainingDays = s1.Any(z => z.InvoiceMonth == month)
                                                            ? (monthDetails.WorkDaysToDateCount == 0
                                                                  ? 0
                                                                  : s1.Where(z => z.InvoiceMonth == month)
                                                                        .Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) * monthDetails.RemainingWorkDaysCount
                                                            : 0.0m,
                                                        YtdActualRevenue = s1.Any(z => z.InvoiceYear == year &&
                                                                                       z.InvoiceDate < DateTime.Now)
                                                            ? s1.Where(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now)
                                                                .Sum(x => x.ExtPrice)
                                                            : 0.0m,
                                                        YtdActualUnits = s1.Any(z => z.InvoiceYear == year &&
                                                                                     z.InvoiceDate < DateTime.Now)
                                                            ? s1.Where(z => z.InvoiceYear == year &&
                                                                            z.InvoiceDate < DateTime.Now).Sum(x => x.Qty)
                                                            : 0.0m,
                                                        YtdQuota = q1.Any()
                                                            ? (monthDetails.PreviousMonthsNames.Any()
                                                                  ? q1.Where(z => monthDetails.PreviousMonthsNames.Contains(z.Month)).Sum(q => q.Revenue)
                                                                  : 0.0m) +
                                                              (q1.Any(z => z.Month == monthDetails.CurrentMonthName)
                                                                  ? month < DateTime.Now.Month ? q1.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                                                  (monthDetails.WorkDaysCount == 0
                                                                      ? 0
                                                                      : q1.Where(z => z.Month == monthDetails.CurrentMonthName)
                                                                            .Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount
                                                                  : 0.0m)
                                                            : 0.0m
                                                    }).OrderBy(x => x.ItemCategoryId).ToList()
                                       }).OrderBy(x => x.SalesRepName).ToList();

            foreach (var repPerformance in salesRepPerformance)
            {
                repPerformance.Items = _rsmPerformanceService.OrderItemsAccordingToAppSetting(repPerformance.Items, monthDetails, repPerformance.SalesRepId, year, month);

                repPerformance.Items = _rsmPerformanceService.AddTotalsRowForItemCategory(repPerformance.Items, repPerformance.SalesRepName);
            }

            return salesRepPerformance;
        }

        /// <summary>
        /// Gets the aggregate item category performance.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public IEnumerable<ItemCategoryQuotaPerformanceDataModel> GetAggregatePerformanceForItemCategory(int year,
            int month, int zoneId, int? regionId = null)
        {
            return GetItemCategoryQuotaPerformanceDataModels(year, month, 0, regionId: regionId);
        }

        /// <summary>
        /// Gets the sales rep performance for item categories.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public IEnumerable<ItemCategoryQuotaPerformanceDataModel> GetSalesRepPerformanceForItemCategory(int year,
            int month, int salesRepId, int? regionId = null)
        {
            return GetItemCategoryQuotaPerformanceDataModels(year, month, salesRepId, regionId: regionId);
        }

        /// <summary>
        /// Gets the item category quota performance data.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep filter.</param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        private IEnumerable<ItemCategoryQuotaPerformanceDataModel> GetItemCategoryQuotaPerformanceDataModels(int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null, int? regionId = null)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportItemCategoryQuota>> quotaQuery = _rsmSalesAndQuotaService.GetItemCategoryQuotaGroupedByItemCategoryId(year, salesRepId);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedByItemCategoryId(year, month, salesRepId, zoneId, regionId);

            var joinedList = quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToList();

            var quotasAndSalesGroupedByItemCategoryId = joinedList.Select(x => new
            {
                Quotas = x.Quota
                    .Where(z => AllowedItemCategories.Contains(z.ReportItemCategory.Name))
                    .Select(z => new
                    {
                        z.ItemCategoryId,
                        ItemCategoryName = z.ReportItemCategory.Name,
                        z.Month,
                        z.Revenue
                    }),
                Sales = x.Sale
                    .Where(z => AllowedItemCategories.Contains(z.ReportItemCategory.Name))
                    .Select(y => new
                    {
                        y.ItemCategoryId,
                        ItemCategoryName = y.ReportItemCategory.Name,
                        InvoiceYear = y.InvoiceDate.Year,
                        InvoiceMonth = y.InvoiceDate.Month,
                        y.InvoiceDate,
                        y.ExtPrice,
                        y.Qty
                    })
            });

            var itemCategoryPerformance = (from quotasAndSales in quotasAndSalesGroupedByItemCategoryId
                                           let quotas = quotasAndSales.Quotas
                                           let sales = quotasAndSales.Sales
                                           select new ItemCategoryQuotaPerformanceDataModel
                                           {
                                               SalesRepId = 0,
                                               ItemCategoryId = quotas.FirstOrDefault()?.ItemCategoryId ?? 0,
                                               ItemCategoryName = quotas.FirstOrDefault()?.ItemCategoryName ?? string.Empty,
                                               MtdActualRevenue = sales.Any(z => z.InvoiceMonth == month) ?
                                                    sales.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) : 0.0m,
                                               MtdActualUnits = sales.Any(z => z.InvoiceMonth == month) ?
                                                    sales.Where(z => z.InvoiceMonth == month).Sum(x => x.Qty) : 0.0m,
                                               MtdTotalMonthQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? quotas
                                                    .Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) : 0.0m,
                                               ProjectedAvgRevPerDay = sales.Any(z => z.InvoiceMonth == month) ?
                                                    (monthDetails.WorkDaysToDateCount == 0 ? 0 : sales.Where(z => z.InvoiceMonth == month)
                                                    .Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) : 0.0m,
                                               ProjectedRevRemainingDays = sales.Any(z => z.InvoiceMonth == month) ?
                                                    (monthDetails.WorkDaysToDateCount == 0 ? 0 : sales.Where(z => z.InvoiceMonth == month)
                                                    .Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) * monthDetails.RemainingWorkDaysCount : 0.0m,
                                               YtdActualRevenue = sales.Any(z => z.InvoiceYear == year &&
                                                    z.InvoiceDate < DateTime.Now) ?
                                                    sales.Where(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now)
                                                    .Sum(x => x.ExtPrice) : 0.0m,
                                               YtdActualUnits = sales.Any(z => z.InvoiceYear == year &&
                                                    z.InvoiceDate < DateTime.Now) ?
                                                    sales.Where(z => z.InvoiceYear == year &&
                                                    z.InvoiceDate < DateTime.Now).Sum(x => x.Qty) : 0.0m,
                                               YtdQuota = quotas.Any() ?
                                                    (monthDetails.PreviousMonthsNames.Any() ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month)).Sum(x => x.Revenue) : 0.0m) +
                                                    (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                                    month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                                    (monthDetails.WorkDaysCount == 0 ? 0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName)
                                                    .Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m)
                                                    : 0.0m
                                           }).ToList();

            itemCategoryPerformance = _rsmPerformanceService.OrderItemsAccordingToAppSetting(itemCategoryPerformance, monthDetails, salesRepId, year, month);

            itemCategoryPerformance = _rsmPerformanceService.AddTotalsRowForItemCategory(itemCategoryPerformance);

            return itemCategoryPerformance;
        }

        #endregion Item Category/Family Name methods

        #region Sales Channel/Customer Class methods

        /// <summary>
        /// Gets the sales channel chart data.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public async Task<List<SalesChannelChartData>> GetSalesChannelChartDataAsync(int year, int month, int? regionId = null)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportCustomerClassQuota>> quotaQuery = _rsmSalesAndQuotaService.GetSalesChannelQuotaGroupedBySalesChannelId(year);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedBySalesChannelId(year, month, regionId: regionId);

            var joinedList = await quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToListAsync();

            var quotasAndSalesGroupedBySalesChannelId = joinedList.Select(x => new
            {
                Quotas = x.Quota.Select(z => new
                {
                    z.CustomerClassId,
                    SalesChannelName = z.ReportCustomerClass.Name,
                    z.Month,
                    z.Revenue
                }),
                Sales = x.Sale.Select(y => new
                {
                    SalesChannelId = y.CustomerClassId,
                    SalesChannelName = y.ReportCustomerClass.Name,
                    y.InvoiceDate.Year,
                    y.InvoiceDate.Month,
                    y.InvoiceDate,
                    y.ExtPrice
                })
            });

            var chartData = (from quotasAndSales in quotasAndSalesGroupedBySalesChannelId
                             let quotas = quotasAndSales.Quotas
                             let sales = quotasAndSales.Sales
                             select new SalesChannelChartData
                             {
                                 SalesChannelId = sales.FirstOrDefault()?.SalesChannelId ?? 0,
                                 SalesChannelName = sales.FirstOrDefault()?.SalesChannelName ?? string.Empty,
                                 YtdActual = sales.Any() ? sales.Where(z => z.InvoiceDate.Year == year && z.InvoiceDate < DateTime.Now).Sum(x => x.ExtPrice) : 0.0m,
                                 YtdQuota = quotas.Any() ?
                                     (monthDetails.PreviousMonthsNames.Any() ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month))
                                         .Sum(x => x.Revenue) : 0.0m) +
                                     (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                         month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                         (monthDetails.WorkDaysCount == 0 ? 0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName)
                                         .Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m)
                                     : 0.0m
                             }).OrderBy(x => x.SalesChannelId).ToList();

            chartData = _rsmPerformanceService.OrderItemsAccordingToAppSetting(chartData);

            return chartData;
        }

        /// <summary>
        /// Gets the aggregate performance for sales channels.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public async Task<List<SalesChannelQuotaPerformanceDataModel>> GetAggregatePerformanceForSalesChannelAsync(int year, int month, int? regionId = null)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportCustomerClassQuota>> quotaQuery = _rsmSalesAndQuotaService.GetSalesChannelQuotaGroupedBySalesChannelId(year);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedBySalesChannelId(year, month, regionId: regionId);

            var joinedList = await quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToListAsync();

            var quotasAndSalesGroupedBySalesChannelId = joinedList.Select(x => new
            {
                Quotas = x.Quota.Select(z => new
                {
                    Id = z.CustomerClassId,
                    SalesChannelName = z.ReportCustomerClass.Name,
                    z.Month,
                    z.Revenue
                }),
                Sales = x.Sale.Select(y => new
                {
                    SalesChannelId = y.CustomerClassId ?? 0,
                    SalesChannelName = y.ReportCustomerClass.Name,
                    y.InvoiceDate.Year,
                    y.InvoiceDate.Month,
                    y.InvoiceDate,
                    y.ExtPrice
                })
            });

            var salesChannelPerformance = (from quotasAndSales in quotasAndSalesGroupedBySalesChannelId
                                           let quotas = quotasAndSales.Quotas
                                           let sales = quotasAndSales.Sales
                                           select new SalesChannelQuotaPerformanceDataModel
                                           {
                                               SalesChannelId = sales.FirstOrDefault()?.SalesChannelId ?? 0,
                                               SalesChannelName = sales.FirstOrDefault()?.SalesChannelName ?? string.Empty,
                                               MtdActualRevenue = sales.Any(z => z.InvoiceDate.Month == month) ? sales.Where(z => z.InvoiceDate.Month == month).Sum(x => x.ExtPrice) : 0.0m,
                                               MtdTotalMonthQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) : 0.0m,
                                               MtdDailyQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? (monthDetails.WorkDaysCount == 0 ? 0 :
                                                   quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) / monthDetails.WorkDaysCount) : 0.0m,
                                               ProjectedAvgRevPerDay = sales.Any(z => z.InvoiceDate.Month == month) ? (monthDetails.WorkDaysToDateCount == 0 ? 0 :
                                                   sales.Where(z => z.InvoiceDate.Month == month).Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) : 0.0m,
                                               ProjectedRevRemainingDays = sales.Any(z => z.InvoiceDate.Month == month) ? (monthDetails.WorkDaysToDateCount == 0 ? 0 :
                                                   sales.Where(z => z.InvoiceDate.Month == month).Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) * monthDetails.RemainingWorkDaysCount : 0.0m,
                                               YtdActualRevenue = sales.Any() ? sales.Where(z => z.InvoiceDate.Year == year && z.InvoiceDate < DateTime.Now).Sum(x => x.ExtPrice) : 0.0m,
                                               YtdQuota = quotas.Any() ?
                                                    (monthDetails.PreviousMonthsNames.Any() ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month)).Sum(x => x.Revenue) : 0.0m) +
                                                    (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                                        month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                                        (monthDetails.WorkDaysCount == 0 ? 0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m)
                                                    : 0.0m
                                           }).OrderBy(x => x.SalesChannelId).ToList();

            salesChannelPerformance = _rsmPerformanceService.OrderItemsAccordingToAppSetting(salesChannelPerformance, monthDetails, 0, year, month);

            salesChannelPerformance = _rsmPerformanceService.AddTotalsRowForSalesChannel(salesChannelPerformance);

            return salesChannelPerformance;
        }

        /// <summary>
        /// Gets the sales rep sales channel performance.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public async Task<List<SalesRepTablePartialViewModel<SalesChannelQuotaPerformanceDataModel>>> GetSalesRepSalesChannelPerformanceAsync(int year, int month, int? regionId = null)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportCustomerClassQuota>> quotaQuery = _rsmSalesAndQuotaService.GetSalesChannelQuotaGroupedBySalesRepId(year);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedBySalesRepId(year, month, regionId: regionId);

            var joinedList = await quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToListAsync();

            var quotasAndSalesGroupedBySalesRepId = joinedList.Select(x => new
            {
                Quotas = x.Quota.Select(z => new
                {
                    z.SalesRepId,
                    SalesRepName = z.ReportHierarchySalesRep.Name,
                    z.CustomerClassId,
                    SalesChannelName = z.ReportCustomerClass.Name,
                    z.Month,
                    z.Revenue
                }),
                Sales = x.Sale.Select(y => new
                {
                    y.SalesRepId,
                    SalesRepName = y.ReportHierarchySalesRep.Name,
                    SalesChannelId = y.CustomerClassId,
                    SalesChannelName = y.ReportCustomerClass.Name,
                    y.InvoiceDate,
                    y.InvoiceDate.Year,
                    y.InvoiceDate.Month,
                    y.ExtPrice
                })
            });

            var salesRepPerformance = (from quotasAndSales in quotasAndSalesGroupedBySalesRepId
                                       let quotas = quotasAndSales.Quotas
                                       let sales = quotasAndSales.Sales
                                       select new SalesRepTablePartialViewModel<SalesChannelQuotaPerformanceDataModel>
                                       {
                                           SalesRepId = quotas.Any() ? quotas.Select(x => x.SalesRepId).FirstOrDefault() : 0,
                                           SalesRepName = quotas.Any() ? quotas.Select(x => x.SalesRepName).FirstOrDefault() : string.Empty,
                                           Items = (from q1 in quotas.GroupBy(x => x.CustomerClassId)
                                                    join s1 in sales.GroupBy(x => x.SalesChannelId ?? 0) on q1.Key equals s1.Key
                                                    where s1.Key != 0
                                                    select new SalesChannelQuotaPerformanceDataModel
                                                    {
                                                        SalesChannelId = q1.Key,
                                                        SalesChannelName = s1.Any() ? s1.Select(x => x.SalesChannelName).FirstOrDefault() : string.Empty,
                                                        MtdActualRevenue = s1.Any(z => z.InvoiceDate.Month == month) ? s1.Where(z => z.InvoiceDate.Month == month).Sum(x => x.ExtPrice) : 0.0m,
                                                        MtdTotalMonthQuota = q1.Any(z => z.Month == monthDetails.CurrentMonthName)
                                                       ? q1.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue)
                                                       : 0.0m,
                                                        MtdDailyQuota =
                                                       q1.Any(z => z.Month == monthDetails.CurrentMonthName)
                                                           ? (monthDetails.WorkDaysCount == 0 ? 0 : q1.Where(z => z.Month == monthDetails.CurrentMonthName)
                                                                                                        .Sum(q => q.Revenue) / monthDetails.WorkDaysCount)
                                                           : 0.0m,
                                                        ProjectedAvgRevPerDay = s1.Any(z => z.InvoiceDate.Month == month) ?
                                                       (monthDetails.WorkDaysToDateCount == 0 ? 0 : s1.Where(z => z.InvoiceDate.Month == month).Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount)
                                                       : 0.0m,
                                                        ProjectedRevRemainingDays = s1.Any(z => z.InvoiceDate.Month == month) ?
                                                       (monthDetails.WorkDaysToDateCount == 0 ? 0 : s1.Where(z => z.InvoiceDate.Month == month).Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) * monthDetails.RemainingWorkDaysCount
                                                       : 0.0m,
                                                        YtdActualRevenue = s1.Any() ?
                                                       s1.Where(z => z.InvoiceDate.Year == year && z.InvoiceDate < DateTime.Now).Sum(x => x.ExtPrice)
                                                       : 0.0m,
                                                        YtdQuota = q1.Any()
                                                       ? (monthDetails.PreviousMonthsNames.Any()
                                                             ? q1.Where(z => monthDetails.PreviousMonthsNames.Contains(z.Month))
                                                                 .Sum(q => q.Revenue)
                                                             : 0.0m) +
                                                         (q1.Any(z => z.Month == monthDetails.CurrentMonthName)
                                                             ? month < DateTime.Now.Month ?
                                                                 q1.Where(z => z.Month == monthDetails.CurrentMonthName)
                                                                     .Sum(q => q.Revenue) :
                                                                 (monthDetails.WorkDaysCount == 0 ? 0 :
                                                                     q1.Where(z => z.Month == monthDetails.CurrentMonthName)
                                                                         .Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount
                                                             : 0.0m)
                                                       : 0.0m
                                                    }).OrderBy(x => x.SalesChannelId).ToList()
                                       }).OrderBy(x => x.SalesRepName).ToList();

            foreach (var repPerformance in salesRepPerformance)
            {
                repPerformance.Items = _rsmPerformanceService.OrderItemsAccordingToAppSetting(repPerformance.Items, monthDetails, repPerformance.SalesRepId, year, month);

                repPerformance.Items = _rsmPerformanceService.AddTotalsRowForSalesChannel(repPerformance.Items, repPerformance.SalesRepName);
            }

            return salesRepPerformance;
        }

        /// <summary>
        /// Gets the aggregate performance for sales channels.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public IEnumerable<SalesChannelQuotaPerformanceDataModel> GetAggregatePerformanceForSalesChannel(int year, int month, int zoneId = 0, int? regionId = null)
        {
            return GetSalesChannelQuotaPerformanceDataModels(year, month, 0, regionId: regionId);
        }

        /// <summary>
        /// Gets the sales rep sales channel performance.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId"></param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public IEnumerable<SalesChannelQuotaPerformanceDataModel> GetSalesRepPerformanceForSalesChannel(int year, int month, int salesRepId, int? regionId = null)
        {
            return GetSalesChannelQuotaPerformanceDataModels(year, month, salesRepId, regionId: regionId);
        }

        /// <summary>
        /// Gets the quota performance for sales channels.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId"></param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        private IEnumerable<SalesChannelQuotaPerformanceDataModel> GetSalesChannelQuotaPerformanceDataModels(int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null, int? regionId = null)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportCustomerClassQuota>> quotaQuery = _rsmSalesAndQuotaService.GetSalesChannelQuotaGroupedBySalesChannelId(year, salesRepId);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedBySalesChannelId(year, month, salesRepId, zoneId, regionId);

            var joinedList = quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToList();

            var quotasAndSalesGroupedBySalesChannelId = joinedList.Select(x => new
            {
                Quotas = x.Quota.Select(z => new
                {
                    z.CustomerClassId,
                    SalesChannelName = z.ReportCustomerClass.Name,
                    z.Month,
                    z.Revenue
                }),
                Sales = x.Sale.Select(y => new
                {
                    SalesChannelId = y.CustomerClassId,
                    SalesChannelName = y.ReportCustomerClass.Name,
                    y.InvoiceDate.Year,
                    y.InvoiceDate.Month,
                    y.InvoiceDate,
                    y.ExtPrice
                })
            });

            var salesRepPerformance = (from quotasAndSales in quotasAndSalesGroupedBySalesChannelId
                                       let quotas = quotasAndSales.Quotas
                                       let sales = quotasAndSales.Sales
                                       select new SalesChannelQuotaPerformanceDataModel
                                       {
                                           SalesChannelId = sales.FirstOrDefault()?.SalesChannelId ?? 0,
                                           SalesChannelName = sales.FirstOrDefault()?.SalesChannelName ?? string.Empty,
                                           MtdActualRevenue = sales.Any(z => z.InvoiceDate.Month == month)
                                               ? sales.Where(z => z.InvoiceDate.Month == month).Sum(x => x.ExtPrice)
                                               : 0.0m,
                                           MtdTotalMonthQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName)
                                               ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue)
                                               : 0.0m,
                                           MtdDailyQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName)
                                               ? monthDetails.WorkDaysCount == 0 ? 0 :
                                               quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) /
                                               monthDetails.WorkDaysCount
                                               : 0.0m,
                                           ProjectedAvgRevPerDay = sales.Any(z => z.InvoiceDate.Month == month)
                                               ? monthDetails.WorkDaysToDateCount == 0 ? 0 :
                                               sales.Where(z => z.InvoiceDate.Month == month).Sum(x => x.ExtPrice) /
                                               monthDetails.WorkDaysToDateCount
                                               : 0.0m,
                                           ProjectedRevRemainingDays = sales.Any(z => z.InvoiceDate.Month == month)
                                               ? (monthDetails.WorkDaysToDateCount == 0
                                                     ? 0
                                                     : sales.Where(z => z.InvoiceDate.Month == month).Sum(x => x.ExtPrice) /
                                                       monthDetails.WorkDaysToDateCount) * monthDetails.RemainingWorkDaysCount
                                               : 0.0m,
                                           YtdActualRevenue = sales.Any()
                                               ? sales.Where(z => z.InvoiceDate.Year == year && z.InvoiceDate < DateTime.Now)
                                                   .Sum(x => x.ExtPrice)
                                               : 0.0m,
                                           YtdQuota = quotas.Any() ?
                                           (monthDetails.PreviousMonthsNames.Any()
                                               ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month)).Sum(x => x.Revenue)
                                               : 0.0m) +
                                           (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                               month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                               (monthDetails.WorkDaysCount == 0
                                                   ? 0
                                                   : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) /
                                                     monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount
                                               : 0.0m)
                                           : 0.0m
                                       }).ToList();

            salesRepPerformance = _rsmPerformanceService.OrderItemsAccordingToAppSetting(salesRepPerformance, monthDetails, salesRepId, year, month);

            salesRepPerformance = _rsmPerformanceService.AddTotalsRowForSalesChannel(salesRepPerformance);

            return salesRepPerformance;
        }

        #endregion Sales Channel/Customer Class methods

        #region Product Family/Product Category/Vendor methods

        /// <summary>
        /// Gets the product category chart data.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public async Task<List<ProductFamilyChartData>> GetProductFamilyChartDataAsync(int year, int month, int? regionId = null)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportVendorQuota>> quotaQuery = _rsmSalesAndQuotaService.GetProductFamilyQuotaGroupedByProductFamilyId(year);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedByProductFamilyId(year, month, regionId: regionId);

            var joinedList = await quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToListAsync();

            var quotasAndSalesGroupedByProductFamilyId = joinedList.Select(x => new
            {
                Quotas = x.Quota
                    .Where(z => AllowedProductFamilies.Contains(z.ReportVendor.Name))
                    .Select(z => new
                    {
                        z.VendorId,
                        ProductFamilyName = z.ReportVendor.Name,
                        z.Month,
                        z.Revenue
                    }),
                Sales = x.Sale
                    .Where(z => AllowedProductFamilies.Contains(z.ReportVendor.Name))
                    .Select(y => new
                    {
                        ProductFamilyId = y.VendorId,
                        ProductFamilyName = y.ReportVendor.Name,
                        ItemCategoryName = y.ReportItemCategory.Name,
                        InvoiceYear = y.InvoiceDate.Year,
                        InvoiceMonth = y.InvoiceDate.Month,
                        y.InvoiceDate,
                        y.ExtPrice
                    })
            });

            var chartData = (from quotasAndSales in quotasAndSalesGroupedByProductFamilyId
                             let quotas = quotasAndSales.Quotas
                             let sales = quotasAndSales.Sales
                             select new ProductFamilyChartData
                             {
                                 ProductFamilyId = sales.FirstOrDefault()?.ProductFamilyId ?? 0,
                                 ProductFamilyName = sales.Any() ? sales.Select(x => x.ProductFamilyName).FirstOrDefault() : string.Empty,
                                 YtdActual = sales.Any(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now) ?
                                     sales.Where(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now).Sum(x => x.ExtPrice) : 0.0m,
                                 YtdQuota = quotas.Any() ?
                                     (monthDetails.PreviousMonthsNames.Any() ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month)).Sum(x => x.Revenue) : 0.0m) +
                                     (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                        month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                        (monthDetails.WorkDaysCount == 0 ? 0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName)
                                        .Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m)
                                     : 0.0m
                             }).ToList();

            chartData = _rsmPerformanceService.OrderItemsAccordingToAppSetting(chartData);

            return chartData;
        }

        /// <summary>
        /// Gets the aggregate product category data.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public async Task<List<ProductFamilyQuotaPerformanceDataModel>> GetAggregatePerformanceForProductFamilyAsync(int year, int month, int? regionId = null)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportVendorQuota>> quotaQuery = _rsmSalesAndQuotaService.GetProductFamilyQuotaGroupedByProductFamilyId(year);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedByProductFamilyId(year, month, regionId: regionId);

            var joinedList = await quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToListAsync();

            var quotasAndSalesGroupedByProductFamilyId = joinedList.Select(x => new
            {
                Quotas = x.Quota
                    .Where(z => AllowedProductFamilies.Contains(z.ReportVendor.Name))
                    .Select(z => new
                    {
                        z.VendorId,
                        ProductFamilyName = z.ReportVendor.Name,
                        z.Month,
                        z.Revenue
                    }),
                Sales = x.Sale
                    .Where(z => AllowedProductFamilies.Contains(z.ReportVendor.Name))
                    .Select(y => new
                    {
                        ProductFamilyId = y.VendorId,
                        ProductFamilyName = y.ReportVendor.Name,
                        ItemCategoryName = y.ReportItemCategory.Name,
                        InvoiceYear = y.InvoiceDate.Year,
                        InvoiceMonth = y.InvoiceDate.Month,
                        y.InvoiceDate,
                        y.ExtPrice,
                        y.Qty
                    })
            });

            var productFamilyPerformance = (from quotasAndSales in quotasAndSalesGroupedByProductFamilyId
                                            let quotas = quotasAndSales.Quotas
                                            let sales = quotasAndSales.Sales
                                            select new ProductFamilyQuotaPerformanceDataModel
                                            {
                                                SalesRepId = 0,
                                                ProductFamilyId = quotas.FirstOrDefault()?.VendorId ?? 0,
                                                ProductFamilyName = sales.Any() ? sales.Select(x => x.ProductFamilyName).FirstOrDefault() : string.Empty,
                                                MtdActualRevenue = sales.Any(z => z.InvoiceMonth == month) ?
                                                    sales.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) : 0.0m,
                                                MtdActualUnits = sales.Any(z => z.InvoiceMonth == month) ?
                                                    sales.Where(z => z.InvoiceMonth == month).Sum(x => x.Qty)
                                                            : 0.0m,
                                                MtdTotalMonthQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) : 0.0m,
                                                ProjectedAvgRevPerDay = sales.Any(z => z.InvoiceMonth == month) ?
                                                                    (monthDetails.WorkDaysToDateCount == 0 ? 0 : sales.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount)
                                                            : 0.0m,
                                                ProjectedRevRemainingDays = sales.Any(z => z.InvoiceMonth == month) ?
                                                                    (monthDetails.WorkDaysToDateCount == 0 ? 0 : sales.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) * monthDetails.RemainingWorkDaysCount
                                                            : 0.0m,
                                                YtdActualRevenue = sales.Any(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now) ?
                                                    sales.Where(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now).Sum(x => x.ExtPrice) : 0.0m,
                                                YtdActualUnits = sales.Any(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now) ?
                                                    sales.Where(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now).Sum(x => x.Qty) : 0.0m,
                                                YtdQuota = quotas.Any() ?
                                                    (monthDetails.PreviousMonthsNames.Any() ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month)).Sum(x => x.Revenue) : 0.0m) +
                                                    (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                                        month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                                        (monthDetails.WorkDaysCount == 0 ? 0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m)
                                                    : 0.0m
                                            }).ToList();

            productFamilyPerformance = _rsmPerformanceService.OrderItemsAccordingToAppSetting(productFamilyPerformance, monthDetails, 0, year, month);

            productFamilyPerformance = _rsmPerformanceService.AddTotalsRowForProductFamily(productFamilyPerformance);

            return productFamilyPerformance;
        }

        /// <summary>
        /// Gets the sales rep data for product families.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public async Task<List<SalesRepTablePartialViewModel<ProductFamilyQuotaPerformanceDataModel>>> GetSalesRepProductFamilyPerformanceAsync(int year, int month, int? regionId = null)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportVendorQuota>> quotaQuery = _rsmSalesAndQuotaService.GetProductFamilyQuotaGroupedBySalesRepId(year);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedBySalesRepId(year, month, regionId: regionId);

            var joinedList = await quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToListAsync();

            var quotasAndSalesGroupedBySalesRepId = joinedList.Select(x => new
            {
                Quotas = x.Quota
                    .Where(z => AllowedProductFamilies.Contains(z.ReportVendor.Name))
                    .Select(z => new
                    {
                        z.SalesRepId,
                        SalesRepName = z.ReportHierarchySalesRep.Name,
                        z.VendorId,
                        ProductFamilyName = z.ReportVendor.Name,
                        z.Month,
                        z.Revenue
                    }),
                Sales = x.Sale
                    .Where(z => AllowedProductFamilies.Contains(z.ReportVendor.Name))
                    .Select(y => new
                    {
                        y.SalesRepId,
                        SalesRepName = y.ReportHierarchySalesRep.Name,
                        ProductFamilyId = y.VendorId,
                        ProductFamilyName = y.ReportVendor.Name,
                        ItemCategoryName = y.ReportItemCategory.Name,
                        InvoiceYear = y.InvoiceDate.Year,
                        InvoiceMonth = y.InvoiceDate.Month,
                        y.InvoiceDate,
                        y.ExtPrice,
                        y.Qty
                    })
            });

            var salesRepPerformance = (from quotasAndSales in quotasAndSalesGroupedBySalesRepId
                                       let quotas = quotasAndSales.Quotas
                                       let sales = quotasAndSales.Sales
                                       select new SalesRepTablePartialViewModel<ProductFamilyQuotaPerformanceDataModel>
                                       {
                                           SalesRepId = quotas.Any() ? quotas.Select(x => x.SalesRepId).FirstOrDefault() : 0,
                                           SalesRepName = quotas.Any() ? quotas.Select(x => x.SalesRepName).FirstOrDefault() : string.Empty,
                                           Items = (from q1 in quotas.GroupBy(x => x.VendorId)
                                                    join s1 in sales.GroupBy(x => x.ProductFamilyId ?? 0) on q1.Key equals s1.Key
                                                    where s1.Key != 0
                                                    select new ProductFamilyQuotaPerformanceDataModel
                                                    {
                                                        SalesRepId = quotas.Any() ? quotas.Select(x => x.SalesRepId).FirstOrDefault() : 0,
                                                        ProductFamilyId = q1.Key,
                                                        ProductFamilyName = s1.Any() ? s1.Select(x => x.ProductFamilyName).FirstOrDefault() : string.Empty,
                                                        MtdActualRevenue = s1.Any(z => z.InvoiceMonth == month) ?
                                                            s1.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) : 0.0m,
                                                        MtdActualUnits = s1.Any(z => z.InvoiceMonth == month) ?
                                                            s1.Where(z => z.InvoiceMonth == month).Sum(x => x.Qty)
                                                            : 0.0m,
                                                        MtdTotalMonthQuota = q1.Any(z => z.Month == monthDetails.CurrentMonthName) ? q1.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) : 0.0m,
                                                        ProjectedAvgRevPerDay = s1.Any(z => z.InvoiceMonth == month) ?
                                                  (monthDetails.WorkDaysToDateCount == 0 ? 0 :
                                                      s1.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount)
                                          : 0.0m,
                                                        ProjectedRevRemainingDays = s1.Any(z => z.InvoiceMonth == month) ?
                                                  (monthDetails.WorkDaysToDateCount == 0 ? 0 :
                                                      s1.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) * monthDetails.RemainingWorkDaysCount
                                          : 0.0m,
                                                        YtdActualRevenue = s1.Any(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now) ?
                                                  s1.Where(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now).Sum(x => x.ExtPrice) : 0.0m,
                                                        YtdActualUnits = s1.Any(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now) ?
                                                  s1.Where(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now).Sum(x => x.Qty) : 0.0m,
                                                        YtdQuota = q1.Any() ?
                                                      (monthDetails.PreviousMonthsNames.Any() ? q1.Where(z => monthDetails.PreviousMonthsNames.Contains(z.Month)).Sum(q => q.Revenue) : 0.0m) +
                                                      (q1.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                                          month < DateTime.Now.Month ? q1.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                                          (monthDetails.WorkDaysCount == 0 ? 0 : q1.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m)
                                                      : 0.0m
                                                    }).OrderBy(x => x.ProductFamilyId).ToList()
                                       }).OrderBy(x => x.SalesRepName).ToList();

            foreach (SalesRepTablePartialViewModel<ProductFamilyQuotaPerformanceDataModel> repPerformance in salesRepPerformance)
            {
                repPerformance.Items = _rsmPerformanceService.OrderItemsAccordingToAppSetting(repPerformance.Items, monthDetails, repPerformance.SalesRepId, year, month);

                repPerformance.Items = _rsmPerformanceService.AddTotalsRowForProductFamily(repPerformance.Items, repPerformance.SalesRepName);
            }

            return salesRepPerformance;
        }

        /// <summary>
        /// Gets the sales rep performance for product families.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public IEnumerable<ProductFamilyQuotaPerformanceDataModel> GetSalesRepPerformanceForProductFamily(int year, int month, int salesRepId, int? regionId = null)
        {
            return GetProductFamilyQuotaPerformanceDataModels(year, month, salesRepId, regionId: regionId);
        }

        /// <summary>
        /// Gets the aggregate performance for product families.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        public IEnumerable<ProductFamilyQuotaPerformanceDataModel> GetAggregatePerformanceForProductFamily(int year, int month, int zoneId, int? regionId = null)
        {
            return GetProductFamilyQuotaPerformanceDataModels(year, month, 0, regionId: regionId);
        }

        /// <summary>
        /// Gets the product category quota performance data.
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="salesRepId">The sales rep filter.</param>
        /// <param name="zoneId">The zone filter.</param>
        /// <param name="regionId">The region filter.</param>
        /// <returns></returns>
        private IEnumerable<ProductFamilyQuotaPerformanceDataModel> GetProductFamilyQuotaPerformanceDataModels(
            int year = 0, int month = 0, int salesRepId = 0, int? zoneId = null,
            int? regionId = null)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportVendorQuota>> quotaQuery = _rsmSalesAndQuotaService.GetProductFamilyQuotaGroupedByProductFamilyId(year, salesRepId);

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedByProductFamilyId(year, month, salesRepId, zoneId, regionId);

            var joinedList = quotaQuery.Join(salesQuery, quo => quo.Key, sal => sal.Key,
                (quo, sal) => new { Quota = quo, Sale = sal }).ToList();

            var quotasAndSalesGroupedByProductFamilyId = joinedList.Select(x => new
            {
                Quotas = x.Quota
                    .Where(z => AllowedProductFamilies.Contains(z.ReportVendor.Name))
                    .Select(z => new
                    {
                        z.VendorId,
                        ProductFamilyName = z.ReportVendor.Name,
                        z.Month,
                        z.Revenue
                    }),
                Sales = x.Sale
                    .Where(z => AllowedProductFamilies.Contains(z.ReportVendor.Name))
                    .Select(y => new
                    {
                        ProductFamilyId = y.VendorId,
                        ProductFamilyName = y.ReportVendor.Name,
                        ItemCategoryName = y.ReportItemCategory.Name,
                        InvoiceYear = y.InvoiceDate.Year,
                        InvoiceMonth = y.InvoiceDate.Month,
                        y.InvoiceDate,
                        y.ExtPrice,
                        y.Qty
                    })
            });

            var productFamilyPerformance = (from quotasAndSales in quotasAndSalesGroupedByProductFamilyId
                                            let quotas = quotasAndSales.Quotas
                                            let sales = quotasAndSales.Sales
                                            select new ProductFamilyQuotaPerformanceDataModel
                                            {
                                                SalesRepId = 0,
                                                ProductFamilyId = quotas.FirstOrDefault()?.VendorId ?? 0,
                                                ProductFamilyName = sales.Any() ? sales.Select(x => x.ProductFamilyName).FirstOrDefault() : string.Empty,
                                                MtdActualRevenue = sales.Any(z => z.InvoiceMonth == month) ?
                                                    sales.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) : 0.0m,
                                                MtdActualUnits = sales.Any(z => z.InvoiceMonth == month) ?
                                                    sales.Where(z => z.InvoiceMonth == month).Sum(x => x.Qty)
                                                            : 0.0m,
                                                MtdTotalMonthQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) : 0.0m,
                                                ProjectedAvgRevPerDay = sales.Any(z => z.InvoiceMonth == month) ?
                                                                    (monthDetails.WorkDaysToDateCount == 0 ? 0 : sales.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount)
                                                            : 0.0m,
                                                ProjectedRevRemainingDays = sales.Any(z => z.InvoiceMonth == month) ?
                                                                    (monthDetails.WorkDaysToDateCount == 0 ? 0 : sales.Where(z => z.InvoiceMonth == month).Sum(x => x.ExtPrice) / monthDetails.WorkDaysToDateCount) * monthDetails.RemainingWorkDaysCount
                                                            : 0.0m,
                                                YtdActualRevenue = sales.Any(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now) ?
                                                    sales.Where(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now).Sum(x => x.ExtPrice) : 0.0m,
                                                YtdActualUnits = sales.Any(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now) ?
                                                    sales.Where(z => z.InvoiceYear == year && z.InvoiceDate < DateTime.Now).Sum(x => x.Qty) : 0.0m,
                                                YtdQuota = quotas.Any() ?
                                                    (monthDetails.PreviousMonthsNames.Any() ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month)).Sum(x => x.Revenue) : 0.0m) +
                                                    (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                                        month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                                        (monthDetails.WorkDaysCount == 0 ? 0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m)
                                                    : 0.0m
                                            }).ToList();

            productFamilyPerformance = _rsmPerformanceService.OrderItemsAccordingToAppSetting(productFamilyPerformance, monthDetails, salesRepId, year, month);

            productFamilyPerformance = _rsmPerformanceService.AddTotalsRowForProductFamily(productFamilyPerformance);

            return productFamilyPerformance;
        }

        #endregion Product Family/Product Category/Vendor methods

        #region Region Table methods

        /// <summary>
        /// Gets the region quota performance data
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <returns></returns>
        public async Task<List<RegionQuotaPerformanceDataModel>> GetRegionTableDataAsync(int year, int month)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportRegionQuota>> quotaQuery = _rsmSalesAndQuotaService.GetRegionQuotaGroupedBySalesRepId(year);

            // Return empty model if there are no quotas
            if (!quotaQuery.Any())
            {
                return new List<RegionQuotaPerformanceDataModel>();
            }

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedBySalesRepIdYearOverYear(year, month);

            IQueryable<ReportUserMapping> mappings = _userMappingService.GetAllUserMappings();

            var joinedList = await quotaQuery
                .Join(salesQuery, quo => quo.Key, sal => sal.Key, (quo, sal) => new { Quota = quo, Sale = sal })
                .Join(mappings, sales => sales.Quota.Key, maps => maps.SalesRepId, (sales, maps) => new { Quota = sales.Quota, Sale = sales.Sale, Mappings = maps })
                .ToListAsync();

            var quotasAndSalesGroupedBySalesRepId = joinedList.Select(x => new
            {
                Quotas = x.Quota
                    .Select(z => new
                    {
                        IsRegionLeader = x.Mappings.ZoneId == null && x.Mappings.RegionId != null,
                        z.Month,
                        z.Year,
                        z.Revenue
                    }),
                Sales = x.Sale
                    .Select(y => new
                    {
                        y.RegionId,
                        RegionName = y.ReportHierarchySalesRep.ReportHierarchyRegion.Name,
                        SalesRep = y.ReportHierarchySalesRep,
                        InvoiceYear = y.InvoiceDate.Year,
                        InvoiceMonth = y.InvoiceDate.Month,
                        y.InvoiceDate,
                        y.ExtPrice
                    })
            });

            _daysOffService.GetCurrentQuarterDetails(year, month, out DateTime quarterStart, out DateTime quarterEnd, out int currentQuarter);

            var salesRepData = (from quotasAndSales in quotasAndSalesGroupedBySalesRepId
                                let quotas = quotasAndSales.Quotas
                                let sales = quotasAndSales.Sales
                                select new SalesRepRegionQuotaPerformanceDataModel
                                {
                                    RegionId = sales.FirstOrDefault()?.RegionId ?? 0,
                                    RegionName = sales.FirstOrDefault()?.RegionName ?? string.Empty,
                                    SalesRepData = new RegionQuotaPerformanceSalesRepModel
                                    {
                                        SalesRepId = sales.FirstOrDefault()?.SalesRep.Id ?? 0,
                                        SalesRepName = sales.FirstOrDefault()?.SalesRep.Name ?? string.Empty,
                                        IsRegionLeader = quotas.FirstOrDefault()?.IsRegionLeader ?? false,
                                        MtdRevenue = sales.Any(z => z.InvoiceMonth == month && z.InvoiceYear == year) ?
                                             sales.Where(z => z.InvoiceMonth == month && z.InvoiceYear == year).Sum(x => x.ExtPrice) : 0.0m,
                                        MtdQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                             month < DateTime.Now.Month ?
                                             quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                             (monthDetails.WorkDaysCount == 0 ? 0 :
                                             quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m,
                                        QtdRevenue = sales.Any(z => z.InvoiceDate >= quarterStart && z.InvoiceDate <= quarterEnd) ?
                                             sales.Where(z => z.InvoiceDate >= quarterStart && z.InvoiceDate <= quarterEnd).Sum(x => x.ExtPrice) : 0.0m,
                                        QtdQuota = quotas.Any(z => _reportingSettings.MonthMappingDict[z.Month.ToLower()] == currentQuarter) ?
                                             quotas.Where(z => _reportingSettings.MonthMappingDict[z.Month.ToLower()] == currentQuarter && monthDetails.PreviousMonthsNames.Contains(z.Month))
                                             .Sum(x => x.Revenue) + (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                             (monthDetails.WorkDaysCount == 0 ?
                                             0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m) : 0.0m,
                                        YtdRevenue = sales.Any(z => z.InvoiceYear == year) ? sales.Where(z => z.InvoiceYear == year).Sum(x => x.ExtPrice) : 0.0m,
                                        YtdQuota = quotas.Any(z => z.Year == year) ? (monthDetails.PreviousMonthsNames.Any() ?
                                             quotas.Where(z => monthDetails.PreviousMonthsNames.Contains(z.Month))
                                             .Sum(x => x.Revenue) : 0.0m) +
                                             (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                             (monthDetails.WorkDaysCount == 0 ?
                                             0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m) : 0.0m,
                                        PreviousYtdRevenue = sales.Any(z => z.InvoiceYear == year - 1) ? sales.Where(z => z.InvoiceYear == year - 1).Sum(x => x.ExtPrice) : 0.0m,
                                    }
                                }).ToList();

            salesRepData = _rsmPerformanceService.AddMissingData(salesRepData, monthDetails, year, month, currentQuarter);

            salesRepData = _rsmPerformanceService.ApplyRankings(salesRepData);

            salesRepData = _rsmPerformanceService.AddTotals(salesRepData);

            var groupedModel = _rsmPerformanceService.ConvertAndGroupSalesReps(salesRepData);

            return groupedModel;
        }

        /// <summary>
        /// Gets the region quota performance data
        /// </summary>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <returns></returns>
        public IEnumerable<RegionQuotaPerformanceDataModel> GetRegionTableData(int year, int month)
        {
            var monthDetails = _daysOffService.GetMonthDetails(year, month);

            IQueryable<IGrouping<int, ReportRegionQuota>> quotaQuery = _rsmSalesAndQuotaService.GetRegionQuotaGroupedBySalesRepId(year);

            // Return empty model if there are no quotas
            if (!quotaQuery.Any())
            {
                return new List<RegionQuotaPerformanceDataModel>();
            }

            IQueryable<IGrouping<int, ReportSalesHistory>> salesQuery = _rsmSalesAndQuotaService.GetSalesGroupedBySalesRepIdYearOverYear(year, month);

            IQueryable<ReportUserMapping> mappings = _userMappingService.GetAllUserMappings();

            var joinedList = quotaQuery
                .Join(salesQuery, quo => quo.Key, sal => sal.Key, (quo, sal) => new { Quota = quo, Sale = sal })
                .Join(mappings, sales => sales.Quota.Key, maps => maps.SalesRepId, (sales, maps) => new { Quota = sales.Quota, Sale = sales.Sale, Mappings = maps })
                .ToList();

            var quotasAndSalesGroupedBySalesRepId = joinedList.Select(x => new
            {
                Quotas = x.Quota
                    .Select(z => new
                    {
                        IsRegionLeader = x.Mappings.ZoneId == null && x.Mappings.RegionId != null,
                        z.Month,
                        z.Year,
                        z.Revenue
                    }),
                Sales = x.Sale
                    .Select(y => new
                    {
                        y.RegionId,
                        RegionName = y.ReportHierarchySalesRep.ReportHierarchyRegion.Name,
                        SalesRep = y.ReportHierarchySalesRep,
                        InvoiceYear = y.InvoiceDate.Year,
                        InvoiceMonth = y.InvoiceDate.Month,
                        y.InvoiceDate,
                        y.ExtPrice
                    })
            });

            _daysOffService.GetCurrentQuarterDetails(year, month, out DateTime quarterStart, out DateTime quarterEnd, out int currentQuarter);

            var salesRepData = (from quotasAndSales in quotasAndSalesGroupedBySalesRepId
                                let quotas = quotasAndSales.Quotas
                                let sales = quotasAndSales.Sales
                                select new SalesRepRegionQuotaPerformanceDataModel
                                {
                                    RegionId = sales.FirstOrDefault()?.RegionId ?? 0,
                                    RegionName = sales.FirstOrDefault()?.RegionName ?? string.Empty,
                                    SalesRepData = new RegionQuotaPerformanceSalesRepModel
                                    {
                                        SalesRepId = sales.FirstOrDefault()?.SalesRep.Id ?? 0,
                                        SalesRepName = sales.FirstOrDefault()?.SalesRep.Name ?? string.Empty,
                                        IsRegionLeader = quotas.FirstOrDefault()?.IsRegionLeader ?? false,
                                        MtdRevenue = sales.Any(z => z.InvoiceMonth == month && z.InvoiceYear == year) ?
                                             sales.Where(z => z.InvoiceMonth == month && z.InvoiceYear == year).Sum(x => x.ExtPrice) : 0.0m,
                                        MtdQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                             month < DateTime.Now.Month ?
                                             quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                             (monthDetails.WorkDaysCount == 0 ? 0 :
                                             quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m,
                                        QtdRevenue = sales.Any(z => z.InvoiceDate >= quarterStart && z.InvoiceDate <= quarterEnd) ?
                                             sales.Where(z => z.InvoiceDate >= quarterStart && z.InvoiceDate <= quarterEnd).Sum(x => x.ExtPrice) : 0.0m,
                                        QtdQuota = quotas.Any(z => _reportingSettings.MonthMappingDict[z.Month.ToLower()] == currentQuarter) ?
                                             quotas.Where(z => _reportingSettings.MonthMappingDict[z.Month.ToLower()] == currentQuarter && monthDetails.PreviousMonthsNames.Contains(z.Month))
                                             .Sum(x => x.Revenue) + (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                             (monthDetails.WorkDaysCount == 0 ?
                                             0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m) : 0.0m,
                                        YtdRevenue = sales.Any(z => z.InvoiceYear == year) ? sales.Where(z => z.InvoiceYear == year).Sum(x => x.ExtPrice) : 0.0m,
                                        YtdQuota = quotas.Any(z => z.Year == year) ? (monthDetails.PreviousMonthsNames.Any() ?
                                             quotas.Where(z => monthDetails.PreviousMonthsNames.Contains(z.Month))
                                             .Sum(x => x.Revenue) : 0.0m) +
                                             (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                             (monthDetails.WorkDaysCount == 0 ?
                                             0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m) : 0.0m,
                                        PreviousYtdRevenue = sales.Any(z => z.InvoiceYear == year - 1) ? sales.Where(z => z.InvoiceYear == year - 1).Sum(x => x.ExtPrice) : 0.0m,
                                    }
                                }).ToList();

            salesRepData = _rsmPerformanceService.AddMissingData(salesRepData, monthDetails, year, month, currentQuarter);

            salesRepData = _rsmPerformanceService.ApplyRankings(salesRepData);

            salesRepData = _rsmPerformanceService.AddTotals(salesRepData);

            var groupedModel = _rsmPerformanceService.ConvertAndGroupSalesReps(salesRepData);

            return groupedModel;
        }

        #endregion Region Table methods

        #region Sales Reps

        public RsmPerformanceSalesRepDataModel GetSalesRepPerformanceData(int salesRepId, int year, int month)
        {
            var result = new RsmPerformanceSalesRepDataModel
            {
                SalesChannelPerformance = GetSalesRepPerformanceForSalesChannel(year, month, salesRepId, _userHierarchyService.GetUserRegionIdIfExistsForCurrentUser()).ToList(),
                ProductFamilyPerformance = GetSalesRepPerformanceForProductFamily(year, month, salesRepId, _userHierarchyService.GetUserRegionIdIfExistsForCurrentUser()).ToList(),
                ItemCategoryPerformance = GetSalesRepPerformanceForItemCategory(year, month, salesRepId, _userHierarchyService.GetUserRegionIdIfExistsForCurrentUser()).ToList(),
                RegionQuotaPerformance = GetRegionTableData(year, month).ToList()
            };

            return result;
        }

        public RsmPerformanceSalesRepModel GetSalesRepPerformanceModel(int salesRepId, int year, int month)
        {
            return new RsmPerformanceSalesRepModel
            {
                Year = year,
                Month = month,
                SalesRepId = salesRepId,
                SalesRepName = _userHierarchyService.GetSalesRepNameById(salesRepId)
            };
        }

        #endregion Sales Reps
    }
}