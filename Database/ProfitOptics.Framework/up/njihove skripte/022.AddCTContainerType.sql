USE [SoxQATest]

CREATE TABLE CTContainerType
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[CTConainerServiceId] [INT] NOT NULL,
	[LegacyId] [BIGINT] NOT NULL,
	[ServiceId] [BIGINT] NOT NULL,
	[ServiceName] [NVARCHAR](200) NOT NULL,
	[HomeLocationName] [NVARCHAR](200) NULL,
	[ProcessingLocationName] [NVARCHAR](200) NULL,
	[Name] [NVARCHAR](200) NOT NULL,
	[SterilizationMethodName] [NVARCHAR](200) NOT NULL,
	[Modified] [NVARCHAR](100) NULL,
	[IsDiscarded] [BIT] NOT NULL,
	[AutomaticActuals] [BIT] NOT NULL,
	[CreatedAtUtc] DateTime NOT NULL,
	[ModifiedAtUtc] DateTime NULL
CONSTRAINT [PK_CTContainerType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTContainerType]  WITH CHECK ADD  CONSTRAINT [FK_CTContainerType_CTContainerService] FOREIGN KEY([CTConainerServiceId])
REFERENCES [dbo].[CTContainerService] ([Id])
GO

ALTER TABLE [dbo].[CTContainerType] CHECK CONSTRAINT [FK_CTContainerType_CTContainerService]
GO