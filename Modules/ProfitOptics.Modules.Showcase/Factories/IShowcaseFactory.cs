﻿using System.Collections.Generic;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.MultiRowTable;
using ProfitOptics.Framework.Web.Framework.Models.Components;
using ProfitOptics.Modules.Showcase.Models;

namespace ProfitOptics.Modules.Showcase.Factories
{
    public interface IShowcaseFactory
    {
        PagedList<DataTablesExampleModel> GetDataTablesExampleModels(IDataTablesRequest request);

        PagedList<ProfitOpticsMultiRowTableExampleModel> GetProfitOpticsMultiRowTableExampleModel(MultiRowTableRequest request);
        PagedList<ProfitOpticsMultiRowTableLevelsExampleModel> GetProfitOpticsMultiRowTableLevelsExampleModel(MultiRowTableRequest request);
        PagedList<ProfitOpticsMultiRowTableLevelsAjaxExampleModel> GetProfitOpticsMultiRowTableLevelsWithAjaxExampleModel(MultiRowTableRequest request);
        PagedList<ProfitOpticsMultiRowTableLevelsAjaxExampleModel> GetProfitOpticsMultiRowTableLevelsExampleModelEmployees(MultiRowTableRequest request, string masterId);
        PagedList<ProfitOpticsMultiRowTableLevelsAjaxExampleModel> GetProfitOpticsMultiRowTableLevelsExampleModelOutsourced(MultiRowTableRequest request, string masterId);

        List<Select2DemoModel> GetSelect2DemoData();

        IEnumerable<Select2DemoModel> GetSelect2DemoData(string search, int page, string filterName);

        HierarchyChartNodeModel GenerateExampleChartNode();
        
    }
}