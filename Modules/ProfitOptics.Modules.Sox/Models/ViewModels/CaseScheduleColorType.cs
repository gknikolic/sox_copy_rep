﻿namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public enum CaseScheduleColorType
	{
		Green = 1, //if case is between delivered and received
		Yellow = 2, //everything else
		Red = 3, //if case color is red
		Gray = 4 //if case is completed (received)
	}

}
