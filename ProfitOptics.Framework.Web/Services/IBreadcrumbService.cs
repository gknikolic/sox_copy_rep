﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Services
{
    public interface IBreadcrumbService
    {
        void Contextualize(ViewContext viewContext);
        IList<Models.BreadcrumbModel> GetBreadcrumbs();
    }
}
