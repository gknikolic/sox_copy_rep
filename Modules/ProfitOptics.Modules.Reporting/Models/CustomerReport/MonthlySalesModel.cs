﻿namespace ProfitOptics.Modules.Reporting.Models.CustomerReport
{
    public class MonthlySalesModel
    {
        public int Month { get; set; }
        public decimal Sales { get; set; }
    }
}