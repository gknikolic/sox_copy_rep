﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily
{
    public class ProductFamilyQuotaPerformanceDataModel : GenericQuotaPerformanceModel
    {
        public int SalesRepId { get; set; }
        public int ProductFamilyId { get; set; }
        public string ProductFamilyName { get; set; }
    }
}