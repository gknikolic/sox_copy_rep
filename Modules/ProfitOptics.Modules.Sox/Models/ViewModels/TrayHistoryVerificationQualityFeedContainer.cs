﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class TrayHistoryVerificationQualityFeedContainer
    {
        public TrayHistoryVerificationQualityFeed Result { get; set; }
    }
}
