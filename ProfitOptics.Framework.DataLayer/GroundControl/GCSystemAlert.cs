﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCSystemAlert
    {
        public int Id { get; set; }
        public int GCSystemAlertTypeId { get; set; }
        public int? GCTrayId { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? MarkedAsReadAtUtc { get; set; }

        public virtual GCSystemAlertType GCSystemAlertType { get; set; }
        public virtual GCTray GCTray { get; set; }
    }
}
