﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable
{
    public class RegionQuotaPerformanceDataModel
    {
        public RegionQuotaPerformanceDataModel()
        {
            RegionQuotaPerformanceData = new List<SalesRepRegionQuotaPerformanceDataModel>();
        }

        public string RegionLeader { get; set; }

        public List<SalesRepRegionQuotaPerformanceDataModel> RegionQuotaPerformanceData { get; set; }
    }
}