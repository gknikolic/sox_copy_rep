﻿namespace ProfitOptics.Modules.Reporting.Models.BusinessReview
{
    public class BusinessReviewReportTypeClassSelectorsModel
    {
        public string ReportButtonClassName { get; set; }

        public string PdfButtonClassName { get; set; }

        public string SelectControlSelector { get; set; }

        public string SelectControlClassName { get; set; }

        public string NotesClassName { get; set; }

        public string NotesCharacterClassName { get; set; }

        public string MessageClassName { get; set; }

        public string PdfPreviewIdName { get; set; }

        public string ReportType { get; set; }

        public string NavigationTabId { get; set; }
    }
}
