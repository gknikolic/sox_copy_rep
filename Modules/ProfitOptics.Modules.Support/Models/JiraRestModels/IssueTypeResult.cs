﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueTypeResult
    {
        public int? id { get; set; }

        public string self { get; set; }

        public List<IssueTypeStatus> statuses { get; set; }

        public string name { get; set; }
    }
}