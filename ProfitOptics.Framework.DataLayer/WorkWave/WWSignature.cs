﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
    [Table("WWSignature")]
    public class WWSignature
    {
        public int Id { get; set; }
        public int SignatureType { get; set; }
        public int WWPodId { get; set; }

        public int WWPodContainerId { get; set; }

        public WWPodContainer WWPodContainer { get; set; }

        public WWPod WWPod { get; set; }
    }
}
