﻿using AutoMapper;
using CommandLine;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Core.System;
using ProfitOptics.Framework.Etl.DataLayer;
using ProfitOptics.Framework.Etl.Helpers;
using ProfitOptics.Framework.Etl.Modules;
using ProfitOptics.Framework.Etl.Modules.CensiTrack;
using ProfitOptics.Framework.Etl.Modules.QuickBooks;
using ProfitOptics.Framework.Etl.Modules.RazorLight;
using ProfitOptics.Framework.Etl.Modules.SomeDataCrunching;
using ProfitOptics.Framework.Etl.Modules.SomeDataCrunching2;
using ProfitOptics.Framework.Etl.Modules.WorkWave;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Sox.Infrastructure;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Services;
using Serilog;
using System;
using System.Diagnostics;
using System.IO;

namespace ProfitOptics.Framework.Etl
{
    internal class Program
    {
        private static IServiceProvider _serviceProvider;

        private static void Main(string[] args)
        {
            RegisterServices();

            // Log each command into a different file
            var logName = "default";
            if (args.Length > 0) logName = args[0];
            Log.Logger = new LoggerConfiguration().
                WriteTo.File(Path.Combine((new IO(null)).GetAssemblyDir(), $"log/{logName}.log"), rollingInterval: RollingInterval.Day).
                WriteTo.Console().
                CreateLogger();

            try
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                Parser.Default.ParseArguments<SomeDataCrunchingOptions, SomeDataCrunching2Options, RazorLightModuleOptions, CensiTrackModuleOptions, QuickBooksModuleOptions, WorkWaveModuleOptions, iMonnitModuleOptions>(args)
                    .MapResult(
                        (SomeDataCrunchingOptions opts) => _serviceProvider.GetService<SomeDataCrunching>().Execute(opts),
                        (SomeDataCrunching2Options opts) => _serviceProvider.GetService<SomeDataCrunching2>().Execute(opts),
                        (RazorLightModuleOptions opts) => _serviceProvider.GetService<RazorLightModule>().Execute(opts),
                        (CensiTrackModuleOptions opts) => _serviceProvider.GetService<CensiTrackModule>().Execute(opts),
                        (QuickBooksModuleOptions opts) => _serviceProvider.GetService<QuickBooksModule>().Execute(opts),
                        (WorkWaveModuleOptions opts) => _serviceProvider.GetService<WorkWaveModule>().Execute(opts),
                        (iMonnitModuleOptions opts) => _serviceProvider.GetService<iMonnitModule>().Execute(opts),
                        errors => { Log.Error("Failed to parse arguments."); return 1; });

                stopwatch.Stop();
                Log.Information($"Finished: {DateTime.UtcNow.ToString()}. The process lasted for {stopwatch.ElapsedMilliseconds} milliseconds");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Unhandled error.");
                throw;
            }

            DisposeServices();
        }

        private static void RegisterServices()
        {
            var collection = new ServiceCollection();
            var builder = new ConfigurationBuilder()
                .SetBasePath(new IO(null).GetAssemblyDir())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            IConfigurationRoot configuration = builder.Build();

            var settings = ConfigureStartupConfig<Settings>(collection, configuration);
            StartupConfiguration.ConfigureStartupConfig<QuickBooksOptions>(collection, configuration);
            StartupConfiguration.ConfigureStartupConfig<CensiTracOptions>(collection, configuration);
            StartupConfiguration.ConfigureStartupConfig<WorkWaveOptions>(collection, configuration);
            StartupConfiguration.ConfigureStartupConfig<MonnitOptions>(collection, configuration);
            StartupConfiguration.ConfigureStartupConfig<BoxstormOptions>(collection, configuration);
            //StartupConfiguration.ConfigureStartupConfig<GlobalOptions>(collection, configuration);

            // Register services
            collection.AddDbContext<Framework.DataLayer.Entities>(options => options.UseSqlServer(settings.DefaultConnection));
            collection.AddSingleton<IEtlHelper, EtlHelper>();
            collection.AddSingleton<ISQLServerHelper, SQLServerHelper>();
            collection.AddSingleton<IRazorLightService, RazorLightService>();
            collection.AddSingleton<ICensiTrackService, CensiTrackService>();
            collection.AddSingleton<ICTCaseService, CTCaseService>();
            collection.AddSingleton<ICTContainerService, CTContainerService>();
            collection.AddSingleton<ICTProductService, CTProductService>();
            collection.AddSingleton<ICTBIService, CTBIService>();
            collection.AddSingleton<IWorkWaveService, WorkWaveService>();
            collection.AddSingleton<IGroundControlService, GroundControlService>();
            collection.AddSingleton<IQBService, QBService>();
            collection.AddSingleton<ISoxLogger, SoxLogger>();

            collection.AddScoped<IWorkWaveClient, WorkWaveClient>();
            collection.AddScoped<IWorkWaveService, WorkWaveService>();
            collection.AddScoped<ICaseManagementClient, CaseManagementClient>();
            collection.AddScoped<IQuickBooksClient, QuickBooksClient>();
            collection.AddScoped<ICaseManagementClient, CaseManagementClient>();
            collection.AddScoped<IMonnitClient, MonnitClient>();
            collection.AddScoped<IMonnitService, MonnitService>();
            collection.AddScoped<IBoxstormClient, BoxstormClient>();
            collection.AddScoped<IBoxstormService, BoxstormService>();

            // Add modules as services
            collection.AddTransient<SomeDataCrunching>();
            collection.AddTransient<SomeDataCrunching2>();
            collection.AddTransient<RazorLightModule>();
            collection.AddTransient<CensiTrackModule>();
            collection.AddTransient<WorkWaveModule>();
            collection.AddTransient<QuickBooksModule>();
            collection.AddTransient<iMonnitModule>();

            _serviceProvider = collection.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable disposable)
            {
                disposable.Dispose();
            }
        }

        public static TConfig ConfigureStartupConfig<TConfig>(IServiceCollection services, IConfiguration configuration) where TConfig : class, new()
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            //create instance of config
            var config = new TConfig();
            var classType = typeof(TConfig);
            //bind it to the appropriate section of configuration
            configuration.Bind(classType.Name, config);

            //and register it as a service
            services.AddSingleton(config);

            services.AddAutoMapper(typeof(TConfig));

            return config;
        }
    }
}
