using System;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportSalesHistory
    {
        public int Id { get; set; }

        public DateTime InvoiceDate { get; set; }

        public int? ZoneId { get; set; }

        public int? RegionId { get; set; }

        public int? SalesRepId { get; set; }

        public int BillToId { get; set; }

        public int ShipToId { get; set; }

        public int? VendorId { get; set; }

        public int ItemId { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal ExtPrice { get; set; }

        public decimal ExtCost { get; set; }

        public decimal Qty { get; set; }

        public int? BuyGroupId { get; set; }

        public int? CustomerClassId { get; set; }

        public int? ItemCategoryId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? InvoiceYearMonth { get; set; }
        
        public string SOPType { get; set; }
        
        public string SOPNumber { get; set; }

        public decimal RebateRate { get; set; }
        
        public string Comment1 { get; set; }
        
        public string Comment2 { get; set; }

        public string Comment3 { get; set; }
        
        public string Comment4 { get; set; }

        public virtual ReportBillTo ReportBillTo { get; set; }

        public virtual ReportBuyGroup ReportBuyGroup { get; set; }

        public virtual ReportCustomerClass ReportCustomerClass { get; set; }

        public virtual ReportHierarchySalesRep ReportHierarchySalesRep { get; set; }

        public virtual ReportItem ReportItem { get; set; }

        public virtual ReportItemCategory ReportItemCategory { get; set; }

        public virtual ReportShipTo ReportShipTo { get; set; }

        public virtual ReportVendor ReportVendor { get; set; }
    }
}
