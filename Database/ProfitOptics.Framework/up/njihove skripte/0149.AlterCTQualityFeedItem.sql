USE [Sox]

ALTER TABLE [Sox].[dbo].[CTQualityFeedItem] ADD [UpdatedDateUtc] DATETIME2 NOT NULL DEFAULT GETDATE();
ALTER TABLE [Sox].[dbo].[CTQualityFeedItem] ADD [EventDateUtc] DATETIME2 NOT NULL DEFAULT GETDATE();

GO
