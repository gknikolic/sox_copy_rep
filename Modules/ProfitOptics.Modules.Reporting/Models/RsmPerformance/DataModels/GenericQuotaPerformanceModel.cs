﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels
{
    public class GenericQuotaPerformanceModel
    {
        public int Id { get; set; }
        public decimal MtdActualRevenue { get; set; }
        public decimal MtdActualUnits { get; set; }
        public decimal MtdTotalMonthQuota { get; set; }
        public decimal MtdPercentToQuota => MtdTotalMonthQuota == decimal.Zero ? decimal.Zero : MtdActualRevenue / MtdTotalMonthQuota;
        public decimal MtdDailyQuota { get; set; }
        public decimal MtdGapToQuota => MtdActualRevenue - MtdTotalMonthQuota;
        public decimal ProjectedAvgRevPerDay { get; set; }
        public decimal ProjectedRevRemainingDays { get; set; }
        public decimal ProjectedMonthRevenue => MtdActualRevenue + ProjectedRevRemainingDays;
        public decimal ProjectedPercentToQuotaTracking => MtdTotalMonthQuota == decimal.Zero ? decimal.Zero : ProjectedMonthRevenue / MtdTotalMonthQuota;
        public decimal YtdActualRevenue { get; set; }
        public decimal YtdActualUnits { get; set; }
        public decimal YtdQuota { get; set; }
        public decimal YtdPercentQuota => YtdQuota == decimal.Zero ? decimal.Zero : YtdActualRevenue / YtdQuota;
    }
}