﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class ErrorResponse
	{
		[JsonProperty("errorCode")]
		public int ErrorCode { get; set; }

		[JsonProperty("errorMessage")]
		public string ErrorMessage { get; set; }
	}
}
