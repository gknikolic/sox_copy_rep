﻿using System;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Framework.Web.Infrastructure;
using ProfitOptics.Framework.Web.ViewComponents;

namespace ProfitOptics.Framework.Web.TagHelpers
{
    [HtmlTargetElement("header-widgets")]
    public class HeaderWidgetsTagHelper : TagHelper
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IViewComponentHelper _viewComponentHelper;
        private readonly IModuleManager _moduleManager;
        public IWebHostEnvironment _hostingEnvironment { get; }

        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public HeaderWidgetsTagHelper(IServiceProvider serviceProvider,
            IViewComponentHelper viewComponentHelper,
            IModuleManager moduleManager,
            IWebHostEnvironment hostingEnvironment)
        {
            this._serviceProvider = serviceProvider;
            this._viewComponentHelper = viewComponentHelper;
            this._moduleManager = moduleManager;
            this._hostingEnvironment = hostingEnvironment;
        }
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "ul";
            output.TagMode = TagMode.StartTagAndEndTag;
            output.AddClass("nav", HtmlEncoder.Default);
            output.AddClass("navbar-nav", HtmlEncoder.Default);

            ((IViewContextAware)_viewComponentHelper).Contextualize(ViewContext);

            var headerWidgets = _serviceProvider.GetServices<IHeaderWidget>().ToList();

            //var modules = _moduleManager.GetExternalModules(_hostingEnvironment);
            //foreach (var module in modules)
            //{
            //    var types = module.GetExportedTypes().Where(x => typeof(IHeaderWidget).IsAssignableFrom(x)).Select(x => (IHeaderWidget)Activator.CreateInstance(x));
            //    headerWidgets.AddRange(types);
            //}
            headerWidgets = headerWidgets.OrderBy(x => x.OrderNumber()).ToList();

            foreach (var widget in headerWidgets)
            {
                output.Content.AppendHtml(await _viewComponentHelper.InvokeAsync(widget.GetType()));
            }

            output.Content.AppendHtml(await _viewComponentHelper.InvokeAsync(typeof(HeaderAccountMenuViewComponent)));

            StringBuilder sb = new StringBuilder()
                .Append("<li>")
                .Append(
                    "<a id='help-content-toggle' href='#' data-toggle='control-sidebar'><i class='fa fa-question-circle'></i></a>")
                .Append("</li>");
            output.Content.AppendHtml(sb.ToString());
        }

        
    }
}
