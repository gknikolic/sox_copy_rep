﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.Web.Areas.AuditLog.Data.Domain;

namespace ProfitOptics.Framework.Web.Areas.AuditLog.Data.Config
{
    public class POActionAuditConfig : IEntityTypeConfiguration<POActionAudit>
    {
        public void Configure(EntityTypeBuilder<POActionAudit> builder)
        {
            builder.ToTable("POActionAudit");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Username).HasMaxLength(100).IsRequired();
            builder.Property(entity => entity.Url).HasMaxLength(512).IsRequired();
            builder.Property(entity => entity.Status).HasMaxLength(50).IsRequired();
            builder.Property(entity => entity.IpAddress).HasMaxLength(50).IsRequired();
            builder.Property(entity => entity.Browser).HasMaxLength(100).IsRequired();

            builder.Property(entity => entity.Area).HasMaxLength(50);
            builder.Property(entity => entity.Controller).HasMaxLength(50);
            builder.Property(entity => entity.Action).HasMaxLength(50);
            builder.Property(entity => entity.ReflectedType).HasMaxLength(256);
            builder.Property(entity => entity.ReflectedActionName).HasMaxLength(50);
        }
    }
}
