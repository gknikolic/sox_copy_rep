﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Infrastructure
{
    public interface ISoxLogger
    {
        void WWInformation(string log);
        void SoxInformation(string log);
    }
}
