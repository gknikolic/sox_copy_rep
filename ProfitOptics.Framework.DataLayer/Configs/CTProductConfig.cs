﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTProductConfig : IEntityTypeConfiguration<CTProduct>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTProduct> builder)
        {
            builder.ToTable("CTProduct");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.LegacyId).IsRequired();
            builder.Property(e => e.VendorName).IsRequired();
            builder.Property(e => e.VendorProductName).IsRequired();
            builder.Property(e => e.ModelNumber).IsRequired();
        }
    }
}
