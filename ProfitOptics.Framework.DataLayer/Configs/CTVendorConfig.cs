﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTVendorConfig : IEntityTypeConfiguration<CTVendor>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTVendor> builder)
        {
            builder.ToTable("CTVendor");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.LegacyId).IsRequired();
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.VendorTypes).IsRequired();
        }
    }
}
