﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTQualityFeedItemGraphicConfig : IEntityTypeConfiguration<CTQualityFeedItemGraphic>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTQualityFeedItemGraphic> builder)
        {
            builder.ToTable("CTQualityFeedItemGraphic");
            builder.HasKey(entity => entity.Id);
        }
    }
}
