﻿using System.ComponentModel;

namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum ProductLevelType
    {
        [Description("All Products")]
        AllProducts = 0,

        [Description("Product")]
        Product = 1,

        [Description("Product Category")]
        ProductCategory = 2
    }
}