﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTHierarchyRegionConfig : IEntityTypeConfiguration<QTHierarchyRegion>
    {
        public void Configure(EntityTypeBuilder<QTHierarchyRegion> builder)
        {
            builder.ToTable("QTHierarchyRegion");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Name).HasMaxLength(50).IsRequired();
        }
    }
}
