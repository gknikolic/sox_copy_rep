﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCCaseStatus
    {
        public int Id { get; set; }
        public int Code { get; set; }
        public string Title { get; set; }
    }
}
