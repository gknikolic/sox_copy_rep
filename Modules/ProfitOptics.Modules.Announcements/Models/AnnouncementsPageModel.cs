﻿namespace ProfitOptics.Modules.Announcements.Models
{
    public class AnnouncementsPageModel
    {
        public bool EnableAdding { get; set; }
        public AnnouncementsFilter Filter { get; set; } 
    }
}
