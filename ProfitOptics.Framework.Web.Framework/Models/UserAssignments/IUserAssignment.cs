﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ProfitOptics.Framework.Web.Framework.Models.UserAssignments
{
    public interface IUserAssignment
    {
        string GetRoleName();
        void Save(int userId, int? zoneId, int? regionId, int? salesRepId);
        void Clear(int userId);
        List<SelectListItem> GetAllZones();
        List<SelectListItem> GetAllRegions();
        List<SelectListItem> GetAllSalesReps();
        AreaUserMapping GetUserMapping(int userId);
        RoleAssignmentModel MatchByName(RoleHierarchyModel roleHierarchy);
    }
}
