namespace ProfitOptics.Framework.Web.RestApi.Resources.Distributor
{
    public class DistributorQueryResource : QueryResource
    {
        public int? CustomerId { get; set; }
        public int? ProductId { get; set; }
    }
}