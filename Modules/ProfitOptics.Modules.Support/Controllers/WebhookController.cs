﻿using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProfitOptics.Framework.Core.Email;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Modules.Support.Models.JiraRestModels;
using ProfitOptics.Modules.Support.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Support.Controllers
{
    public class WebhookController : BaseController
    {
        #region Fields

        private readonly IEmailer _emailer;
        private readonly ISupportService _supportService;

        #endregion Fields

        #region Ctor

        public WebhookController(IEmailer emailer,
            ISupportService supportService)
        {
            this._emailer = emailer;
            this._supportService = supportService;
        }

        #endregion Ctor

        [Route("/jira-webhook")]
        public async Task<IActionResult> Catch()
        {
            string data = new StreamReader(Request.Body).ReadToEnd();

            try
            {
                var webhookEvent = JsonConvert.DeserializeObject<WebhookEvent>(data);

                var emails = new List<string>();
                var globalWatchers = _supportService.GetGlobalWatchers();
                globalWatchers.ForEach(g => emails.Add(g.Email));

                var watchers = _supportService.GetWatchersForIssue(webhookEvent.issue.fields.project.key, webhookEvent.issue.key);
                watchers.RemoveAll(w => globalWatchers.Exists(gw => gw.Email == w));
                emails.AddRange(watchers);

                if (emails.Count > 0)
                {
                    var emailBody = await this.RenderViewAsync("_EmailTemplate", webhookEvent);

                    _emailer.SendEmail(emails, "Support Ticket Update", emailBody, isHtml: true);
                }
            }
            catch (Exception ex)
            {
                var dataException = new Exception(data);

                dataException.Ship(HttpContext);
            }

            return null;
        }
    }
}