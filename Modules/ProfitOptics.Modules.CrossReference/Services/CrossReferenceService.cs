﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Modules.CrossReference.Data;
using ProfitOptics.Modules.CrossReference.Data.Domain;
using ProfitOptics.Modules.CrossReference.Models;

namespace ProfitOptics.Modules.CrossReference.Services
{
    public class CrossReferenceService : ICrossReferenceService
    {
        #region Fields

        private readonly Entities _context;

        #endregion Fields

        #region Ctor

        public CrossReferenceService(Entities context)
        {
            _context = context;
        }

        #endregion Ctor

        #region Compatibility Option

        public int CreateCompatibilityOption(string compatibilityName)
        {
            var compatibilityOption = _context.CrtCompatibilityOptions.Add(new CrtCompatibilityOption
            {
                CompatibilityOptionName = compatibilityName
            });

            _context.SaveChanges();
            return compatibilityOption.Entity.Id;
        }

        public void UpdateCompatibilityOption(int id, string compatibilityOptionName)
        {
            var compatibilityOption = _context.CrtCompatibilityOptions.Find(id);

            if (compatibilityOption != null)
            {
                compatibilityOption.CompatibilityOptionName = compatibilityOptionName;
                _context.SaveChanges();
            }
            else
            {
                throw new Exception("CompatibilityOptionId not valid!");
            }
        }

        public void DeleteCompatibilityOption(int compatibilityId)
        {
            var compatibilityOption = _context.CrtCompatibilityOptions.Find(compatibilityId);

            if (compatibilityOption != null)
            {
                compatibilityOption.EndDate = DateTime.UtcNow;
                _context.SaveChanges();
            }
            else
            {
                throw new Exception("Compatibility option Id not valid!");
            }
        }

        public IEnumerable<CrtCompatibilityOption> GetCompatibilityOptions(bool getDeleted = false)
        {
            var crtOptions = _context.CrtCompatibilityOptions.Where(co => getDeleted || co.EndDate == null);

            return crtOptions;
        }

        #endregion Compatibility Option

        #region Competitor

        public void DeleteCompetitor(int id)
        {
            var competitor = _context.CrtCompetitors.Find(id);

            if (competitor != null)
            {
                competitor.EndDate = DateTime.UtcNow;
                _context.SaveChanges();
            }
            else
            {
                throw new Exception("Competitor Id not valid!");
            }
        }

        public void UpdateCompetitor(int id, string competitorName)
        {
            var competitor = _context.CrtCompetitors.Find(id);

            if (competitor != null)
            {
                competitor.CompetitorName = competitorName;
                _context.SaveChanges();
            }
            else
            {
                throw new Exception("Competitor Id not valid!");
            }
        }

        public int CreateCompetitor(string competitorName)
        {
            var competitor = _context.CrtCompetitors.Add(new CrtCompetitor
            {
                CompetitorName = competitorName,
                StartDate = DateTime.UtcNow,
                CompetitorDescription = "No description"
            });

            _context.SaveChanges();
            return competitor.Entity.Id;
        }

        public IEnumerable<CrtCompetitor> GetCompetitors(bool getDeleted = false)
        {
            var competitors = _context.CrtCompetitors.Where(c => getDeleted || c.EndDate == null);

            return competitors;
        }

        #endregion Competitor

        #region Cross Reference Export

        public IEnumerable<CrtCrossReferenceRecord> GetCrossReferenceExportModelList(List<int?> crossReferenceIds)
        {
            var records = from cr in _context.CrtCrossReferenceRecords.Where(x => crossReferenceIds.Contains(x.Id))
                          where cr.EndDate == null
                          select cr;

            return records;
        }

        #endregion Cross Reference Export

        #region Cross Reference Record

        public IEnumerable<CrtCrossReferenceRecord> GetCrossReferenceRecords(int? competitorId,
            int? compatibilityId, string competitorItem, int userId)
        {
            var records = from cr in _context.CrtCrossReferenceRecords
                          from co in _context.CrtCompatibilityOptions.Where(x => x.Id == cr.CompatibilityOptionId).DefaultIfEmpty()
                          where cr.Approved
                          && cr.EndDate == null
                          && (competitorId == null || cr.CompetitorId == competitorId)
                          && (compatibilityId == null || co.Id == compatibilityId)
                          && (string.IsNullOrEmpty(competitorItem) || cr.CompetitorItemDescription.Contains(competitorItem))
                          select cr;

            return records;
        }

        public IEnumerable<CrtCrossReferenceRecord> GetCrossReferenceRecordsForApproval(int userId)
        {
            var records = from cr in _context.CrtCrossReferenceRecords
                          where cr.Approved == false && cr.EndDate == null
                          select cr;

            return records;
        }

        public int GetThumbsUpCountForCrRecord(int recordId)
        {
            return _context.CrtCrossReferenceVotes.Count(x => x.CrossReferenceRecordId == recordId && x.Vote > 0);
        }

        public int GetThumbsDownCountForCrRecord(int recordId)
        {
            return _context.CrtCrossReferenceVotes.Count(x => x.CrossReferenceRecordId == recordId && x.Vote < 0);
        }

        public IEnumerable<QTItem> GetQtItemsForAutoComplete(string term)
        {
            var results = _context.QTItems.Where(q => q.ItemNum.Contains(term) ||
                                                      (q.ItemDescription != null && q.ItemDescription.Contains(term)))
                .OrderBy(m => m.ItemNum.ToLower().StartsWith(term.ToLower()) ? 0 : 1)
                .Take(10);

            return results;
        }

        public int AddCrossReferenceRecord(CrossReferenceRecordModel crossReferenceRecord, int userId)
        {
            var newCrossReferenceRecord = _context.CrtCrossReferenceRecords.Add(new CrtCrossReferenceRecord
            {
                SubmittedBy = userId,
                CompetitorId = crossReferenceRecord.CompetitorId,
                CompetitorItemNumber = crossReferenceRecord.CompetitorItemNumber,
                CompetitorItemDescription = crossReferenceRecord.CompetitorItemDescription,
                CompatibilityOptionId = crossReferenceRecord.CompatibilityOptionId,
                ItemId = crossReferenceRecord.ItemId,
                CompetitorWebsiteUrl = crossReferenceRecord.CompetitorWebsiteUrl,
                Notes = crossReferenceRecord.Notes,
                Approved = false,
                StartDate = DateTime.UtcNow
            });

            _context.SaveChanges();
            return newCrossReferenceRecord.Entity.Id;
        }

        public void EditCrossReferenceRecordForApproval(CrossReferenceRecordModel crossReferenceRecord)
        {
            var id = crossReferenceRecord.Id;

            var crossReferenceRecordForApproval = _context.CrtCrossReferenceRecords.Find(id);

            if (crossReferenceRecordForApproval != null)
            {
                crossReferenceRecordForApproval.CompetitorId = crossReferenceRecord.CompetitorId;
                crossReferenceRecordForApproval.CompetitorItemNumber = crossReferenceRecord.CompetitorItemNumber;
                crossReferenceRecordForApproval.CompetitorItemDescription = crossReferenceRecord.CompetitorItemDescription;
                crossReferenceRecordForApproval.CompatibilityOptionId = crossReferenceRecord.CompatibilityOptionId;
                crossReferenceRecordForApproval.CompetitorWebsiteUrl = crossReferenceRecord.CompetitorWebsiteUrl;
                crossReferenceRecordForApproval.ItemId = crossReferenceRecord.ItemId;
                crossReferenceRecordForApproval.Notes = crossReferenceRecord.Notes;
                _context.Update(crossReferenceRecordForApproval);
                _context.SaveChanges();
            }
            else
            {
                throw new Exception("Cross Reference Record Id not valid!");
            }
        }

        public void RemoveCrossReferenceRecordForApproval(int id)
        {
            var crossReferenceRecordForApproval = _context.CrtCrossReferenceRecords.Find(id);

            if (crossReferenceRecordForApproval != null)
            {
                crossReferenceRecordForApproval.EndDate = DateTime.UtcNow;
                _context.SaveChanges();
            }
            else
            {
                throw new Exception("Competitor Id not valid!");
            }
        }

        public void ApproveCrossReferenceRecord(int id)
        {
            var crossReferenceRecordForApproval = _context.CrtCrossReferenceRecords.Find(id);

            if (crossReferenceRecordForApproval != null)
            {
                crossReferenceRecordForApproval.Approved = true;
                _context.SaveChanges();
            }
            else
            {
                throw new Exception("Cross Reference Record Id not valid!");
            }
        }

        public void ThumbsUpClick(int crossReferenceId, int userId)
        {
            //check if a vote already existed
            var crossReferenceVoteRecord = _context.CrtCrossReferenceVotes
                .FirstOrDefault(x =>
                    x.UserId == userId && x.CrossReferenceRecordId == crossReferenceId);

            if (crossReferenceVoteRecord != null)
            {
                // if a user already had a thumbsUp vote, then a thumbsUp click means that the vote is revoked
                // if a user had a thumbsDown vote or a zero value (a no-vote), then a thumbsUp click will assign a thumbsUp vote
                crossReferenceVoteRecord.Vote = crossReferenceVoteRecord.Vote > 0
                    ? 0
                    : 1;
                _context.SaveChanges();
            }
            else
            {
                // if a user doesn't have a vote, then a thumbsUp vote is assigned
                _context.CrtCrossReferenceVotes.Add(new CrtCrossReferenceVote
                {
                    UserId = userId,
                    CrossReferenceRecordId = crossReferenceId,
                    Vote = 1,
                    StartDate = DateTime.UtcNow
                });
                _context.SaveChanges();
            }
        }

        public void ThumbsDownClick(int crossReferenceId, int userId)
        {
            //check if a vote already existed
            var crossReferenceVoteRecord = _context.CrtCrossReferenceVotes
                .FirstOrDefault(x =>
                    x.UserId == userId && x.CrossReferenceRecordId == crossReferenceId);

            if (crossReferenceVoteRecord != null)
            {
                crossReferenceVoteRecord.Vote = crossReferenceVoteRecord.Vote < 0
                    ? 0
                    : -1;
                _context.SaveChanges();
            }
            else
            {
                _context.CrtCrossReferenceVotes.Add(new CrtCrossReferenceVote
                {
                    UserId = userId,
                    CrossReferenceRecordId = crossReferenceId,
                    Vote = -1,
                    StartDate = DateTime.UtcNow
                });
                _context.SaveChanges();
            }
        }

        public bool CheckCompetitorMedcoItemCombinationOnAdd(int competitorId, string competitorItemNumber, int medcoItemNumberId)
        {
            var crossReferenceRecords = _context.CrtCrossReferenceRecords.Where(x => x.CompetitorId == competitorId
                                                                          && x.CompetitorItemNumber == competitorItemNumber
                                                                          && x.ItemId == medcoItemNumberId);
            return !crossReferenceRecords.Any();
        }

        public bool CheckCompetitorMedcoItemCombinationOnEdit(int id, int competitorId, string competitorItemNumber, int medcoItemNumberId)
        {
            var crossReferenceRecord = _context.CrtCrossReferenceRecords.Where(x => x.CompetitorId == competitorId
                                                                         && x.CompetitorItemNumber == competitorItemNumber
                                                                         && x.ItemId == medcoItemNumberId);

            return crossReferenceRecord.Any(x => x.Id == id);
        }

        #endregion Cross Reference Record

        #region QTItem

        public IEnumerable<SelectListItem> GetItemListForDropdown(int? itemId)
        {
            return _context.QTItems
                    .AsEnumerable()
                    .Select(qi => new SelectListItem
                    {
                        Value = qi.Id.ToString(),
                        Selected = qi.Id == itemId,
                        Text = qi.ItemDescription
                    }).ToList();
        }

        #endregion QTItem
    }
}