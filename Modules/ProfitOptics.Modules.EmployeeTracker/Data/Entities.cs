﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;
using System.Diagnostics;

namespace ProfitOptics.Modules.EmployeeTracker.Data
{
    public class Entities : DbContext
    {
        public Entities(DbContextOptions<Entities> dbContextOptions) : base(dbContextOptions)
        {
            var listener = this.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Example model builder
            //modelBuilder.ApplyConfiguration(new FooBarConfig());
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.EnableSensitiveDataLogging();
        }

        //Example data set
        //public virtual DbSet<FooBar> FooBar { get; set; }
    }
}
