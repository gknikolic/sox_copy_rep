﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.TrendVariance;
using ProfitOptics.Modules.Reporting.Services;
using ProfitOptics.Modules.Reporting.Services.TrendVariance;

namespace ProfitOptics.Modules.Reporting.Factories.TrendVariance
{
    public class TrendVarianceModelFactory : ITrendVarianceModelFactory
    {
        private readonly IUserHierarchyService _userHierarchyService;
        private readonly ITrendVarianceDataService _trendVarianceDataService;

        public TrendVarianceModelFactory(IUserHierarchyService userHierarchyService,
            ITrendVarianceDataService trendVarianceDataService)
        {
            _userHierarchyService = userHierarchyService;
            _trendVarianceDataService = trendVarianceDataService;
        }

        /// <inheritdoc />
        public TrendVarianceViewModel CreateTrendVarianceViewModel(int userId)
        {
            DateTime now = DateTime.Now;

            return new TrendVarianceViewModel
            {
                IsCorporate = _userHierarchyService.IsCorporateUser(userId),
                StartDateDefault = new DateTime(now.Year, now.Month, 1).ToString("MM-dd-yyyy"),
                EndDateDefault = now.ToString("MM-dd-yyyy")
            };
        }

        /// <inheritdoc />
        public List<TrendVarianceYearOverYearVarianceModel> CreateYearOverYearVarianceModels(int userId, IDataTablesRequest request, int yearsToShow = 3)
        {
            DateTime endDate = DateTime.Now;
            var startDate = new DateTime(endDate.Year - (yearsToShow - 1), 1, 1);

            IEnumerable<TrendVarianceSalesForMonthModel> monthlyData = _trendVarianceDataService.GetSalesHistoryForMonths(userId,
                startDate, endDate);

            List<TrendVarianceYearOverYearVarianceModel> models = CreateYerYearOverYearVarianceModels(yearsToShow, monthlyData, endDate, request.ViewBySales);

            return models;
        }

        /// <inheritdoc />
        public PagedList<TrendVarianceVarianceByTypeModel> CreateVarianceModels(int userId, IDataTablesRequest request, TrendVarianceVarianceByTypeRequestParameters parameters)
        {
            var periods = new TrendVariancePeriodsModel(parameters.ComparisonType, parameters.StartDate, parameters.EndDate);

            PagedList<TrendVarianceVarianceByTypeModel> models = 
                _trendVarianceDataService.GetVariancesGroupedByViewByType(userId, request, parameters.ViewByType, periods);

            return models;
        }

        /// <inheritdoc />
        public List<TrendVarianceVarianceChartModel> CreateSalesHistoryForMonthModels(int userId, int yearsToShow = 3)
        {
            DateTime endDate = DateTime.Now;
            var startDate = new DateTime(endDate.Year - (yearsToShow - 1), 1, 1);

            IEnumerable<TrendVarianceSalesForMonthModel> monthlyData = _trendVarianceDataService.GetSalesHistoryForMonths(userId,
                startDate, endDate);

            bool isCorporate = _userHierarchyService.IsCorporateUser(userId);

            List<TrendVarianceVarianceChartModel> models = (from data in monthlyData
                group data by data.Year
                into groupedByYear
                select new TrendVarianceVarianceChartModel
                {
                    Year = groupedByYear.Key,
                    MonthlyData = groupedByYear.GroupBy(groupedByMonth => groupedByMonth.Month)
                        .Select(m => new TrendVarianceVarianceChartModel.VarianceChartMonthlyData
                        {
                            Month = m.Key,
                            Sales = m.Any() ? m.Sum(s => s.ExtPrice) : decimal.Zero,
                            GrossProfit = m.Any() && isCorporate ? m.Sum(s => s.GrossProfit) : decimal.Zero,
                            GrossProfitPercent = m.Any() && m.Sum(s => s.ExtPrice) != decimal.Zero && isCorporate ?
                                m.Sum(s => s.GrossProfit) / m.Sum(s => s.ExtPrice) : decimal.Zero
                        }).ToList()
                }).ToList();

            return models;
        }

        /// <inheritdoc />
        public TrendVarianceBillToCustomerDrillThroughViewModel CreateCustomerDrillThroughViewModelForBillToId(int userId, int billToId,
            ComparisonType comparisonType, DateTime? comparisonStartDate, DateTime? comparisonEndDate, IUrlHelper urlHelper)
        {
            ReportBillTo billTo = _trendVarianceDataService.GetBillToById(billToId);

            if (billTo == null)
            {
                throw new ArgumentException($"No bill to was found with the identifier: {billToId}");
            }

            return new TrendVarianceBillToCustomerDrillThroughViewModel(
                billTo.Id, 
                billTo.BillToName,
                billTo.BillToNum, 
                comparisonType, 
                comparisonStartDate,
                comparisonEndDate,
                _userHierarchyService.IsCorporateUser(userId),
                urlHelper);
        }

        /// <inheritdoc />
        public TrendVarianceShipToCustomerDrillThroughViewModel CreateCustomerDrillThroughViewModelForShipToId(int userId, int shipToId,
            ComparisonType comparisonType, DateTime? comparisonStartDate, DateTime? comparisonEndDate, IUrlHelper urlHelper)
        {
            ReportShipTo shipTo = _trendVarianceDataService.GetShipToById(shipToId);

            if (shipTo == null)
            {
                throw new ArgumentException($"No ship to was found with the identifier: {shipToId}");
            }

            return new TrendVarianceShipToCustomerDrillThroughViewModel(
                shipTo.Id,
                shipTo.ShipToName,
                shipTo.ShipToNum,
                comparisonType,
                comparisonStartDate,
                comparisonEndDate,
                _userHierarchyService.IsCorporateUser(userId),
                urlHelper);
        }

        /// <inheritdoc />
        public PagedList<TrendVarianceVarianceByTypeModel> CreateVarianceModelsForBillToParameters(int userId,
            IDataTablesRequest request, TrendVarianceBillToDrillThroughVarianceParameters parameters)
        {
            var periods = new TrendVariancePeriodsModel(parameters.ComparisonType, parameters.StartDate, parameters.EndDate);

            return _trendVarianceDataService
                .GetVariancesGroupedByItemCategoryForBillToId(userId, parameters.BillToId, request, periods);
        }

        /// <inheritdoc />
        public PagedList<TrendVarianceVarianceByTypeModel> CreateVarianceModelsForShipToParameters(int userId, IDataTablesRequest request,
            TrendVarianceShipToDrillThroughVarianceParameters parameters)
        {
            var periods = new TrendVariancePeriodsModel(parameters.ComparisonType, parameters.StartDate, parameters.EndDate);

            return _trendVarianceDataService
                .GetVariancesGroupedByItemCategoryForShipToId(userId, parameters.ShipToId, request, periods);
        }

        /// <inheritdoc />
        public TrendVarianceBillToItemCategoryDrillThroughViewModel
            CreateItemCategoryDrillThroughViewModelForItemCategoryIdAndBillToId(int userId, int billToId, int itemCategoryId,
                ComparisonType comparisonType, DateTime? comparisonStartDate, DateTime? comparisonEndDate, IUrlHelper urlHelper)
        {
            ReportBillTo billTo = _trendVarianceDataService.GetBillToById(billToId);

            if (billTo == null)
            {
                throw new ArgumentException($"No bill to to was found with the identifier: {billToId}");
            }

            ReportItemCategory itemCategory = _trendVarianceDataService.GetItemCategoryById(itemCategoryId);

            if (itemCategory == null)
            {
                throw new ArgumentException($"No item category was found with the identifier: {itemCategoryId}");
            }

            return new TrendVarianceBillToItemCategoryDrillThroughViewModel(
                billTo.Id,
                billTo.BillToName,
                billTo.BillToNum,
                itemCategory.Id,
                itemCategory.Name,
                comparisonType,
                comparisonStartDate,
                comparisonEndDate,
                _userHierarchyService.IsCorporateUser(userId),
                urlHelper);
        }

        /// <inheritdoc />
        public TrendVarianceShipToItemCategoryDrillThroughViewModel
            CreateItemCategoryDrillThroughViewModelForItemCategoryIdAndShipToId(int userId, int shipToId, int itemCategoryId,
                ComparisonType comparisonType, DateTime? comparisonStartDate, DateTime? comparisonEndDate, IUrlHelper urlHelper)
        {
            ReportShipTo shipTo = _trendVarianceDataService.GetShipToById(shipToId);

            if (shipTo == null)
            {
                throw new ArgumentException($"No ship to was found with the identifier: {shipToId}");
            }

            ReportItemCategory itemCategory = _trendVarianceDataService.GetItemCategoryById(itemCategoryId);

            if (itemCategory == null)
            {
                throw new ArgumentException($"No ship to was found with the identifier: {itemCategoryId}");
            }

            return new TrendVarianceShipToItemCategoryDrillThroughViewModel(
                shipTo.Id,
                shipTo.ShipToName,
                shipTo.ShipToNum,
                itemCategory.Id,
                itemCategory.Name,
                comparisonType,
                comparisonStartDate,
                comparisonEndDate,
                _userHierarchyService.IsCorporateUser(userId),
                urlHelper);
        }

        /// <inheritdoc />
        public PagedList<TrendVarianceVarianceForItemModel> CreateItemVarianceModelsForBillToItemCategoryParameters(int userId, IDataTablesRequest request,
            TrendVarianceBillToItemCategoryDrillThroughParameters parameters)
        {
            var periods = new TrendVariancePeriodsModel(parameters.ComparisonType, parameters.StartDate, parameters.EndDate);

            return _trendVarianceDataService
                .GetVariancesGroupedByItemForItemCategoryIdAndBillToId(userId, parameters.BillToId, parameters.ItemCategoryId, request, periods);
        }

        /// <inheritdoc />
        public PagedList<TrendVarianceVarianceForItemModel> CreateItemVarianceModelsForShipToItemCategoryParameters(int userId, IDataTablesRequest request,
            TrendVarianceShipToItemCategoryDrillThroughParameters parameters)
        {
            var periods = new TrendVariancePeriodsModel(parameters.ComparisonType, parameters.StartDate, parameters.EndDate);

            return _trendVarianceDataService
                .GetVariancesGroupedByItemForItemCategoryIdAndShipToId(userId, parameters.ShipToId, parameters.ItemCategoryId, request, periods);
        }

        /// <inheritdoc />
        public List<TrendVarianceItemCategoryDrillThroughChartModel> CreateItemCategoryChartModels(int userId, int itemCategoryId, int billToId = 0, int shipToId = 0)
        {
            if (userId <= 0)
            {
                return new List<TrendVarianceItemCategoryDrillThroughChartModel>();
            }

            DateTime endDate = DateTime.Now;

            DateTime endDateMinus2Years = endDate.AddYears(-2);

            var startDate = new DateTime(endDateMinus2Years.Year, endDateMinus2Years.Month, 1);

            List<ReportSalesHistory> salesHistoriesForDateRange = _trendVarianceDataService
                .GetSalesHistoriesForDateRange(userId, startDate, endDate);

            IEnumerable<TrendVarianceItemCategoryDrillThroughChartModel> items = 
                from salesHistory in salesHistoriesForDateRange
                where salesHistory.ItemCategoryId == itemCategoryId &&
                      (billToId == 0 || salesHistory.BillToId == billToId) &&
                      (shipToId == 0 || salesHistory.ShipToId == shipToId)
                group salesHistory by salesHistory.InvoiceYearMonth
                into groupedSalesHistories
                orderby groupedSalesHistories.Key
                select new TrendVarianceItemCategoryDrillThroughChartModel
                {
                    YearMonth = groupedSalesHistories.Key ?? 0,
                    MonthlySales = groupedSalesHistories.Sum(sh => sh.ExtPrice),
                    MonthlyGrossProfit = groupedSalesHistories.Sum(sh => sh.ExtPrice - sh.ExtCost)
                };

            return items.ToList();
        }

        private static List<TrendVarianceYearOverYearVarianceModel> CreateYerYearOverYearVarianceModels(int yearsToShow,
            IEnumerable<TrendVarianceSalesForMonthModel> monthlyData, DateTime endDate, bool viewBySales)
        {
            List<TrendVarianceYearOverYearVarianceModel> models = (from reportSales in monthlyData
                group reportSales by reportSales.Year into groupedSales
                  select new TrendVarianceYearOverYearVarianceModel
                  {
                      YearInteger = groupedSales.Key,
                      Year = groupedSales.Any() ? groupedSales.Key.ToString() : "N/A",
                      Ytd = groupedSales.Any()
                          ? groupedSales.Where(x => x.Month <= endDate.Month)
                              .Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Total = groupedSales.Any() ? groupedSales.Sum(m => SumSelector(m, viewBySales)) : (decimal?)null,
                      Jan = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Jan))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Jan)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Feb = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Feb))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Feb)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Mar = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Mar))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Mar)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Apr = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Apr))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Apr)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      May = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.May))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.May)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Jun = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Jun))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Jun)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Jul = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Jul))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Jul)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Aug = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Aug))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Aug)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Sep = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Sep))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Sep)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Oct = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Oct))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Oct)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Nov = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Nov))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Nov)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null,
                      Dec = groupedSales.Any(x => MonthlyModelMonthPredicate(x, Month.Dec))
                          ? groupedSales.Where(x => MonthlyModelMonthPredicate(x, Month.Dec)).Sum(m => SumSelector(m, viewBySales))
                          : (decimal?)null
                  }).ToList();

            models = AddYearOverYearVarianceModelsForMissingYears(models, endDate, yearsToShow)
                .OrderByDescending(m => m.YearInteger).ToList();

            models = AddGrowthRows(models, endDate, yearsToShow);

            return models;
        }

        private static IEnumerable<TrendVarianceYearOverYearVarianceModel> AddYearOverYearVarianceModelsForMissingYears(List<TrendVarianceYearOverYearVarianceModel> models,
            DateTime endDate, int yearsToShow)
        {
            int endYear = endDate.Year;

            int decrement = yearsToShow - 1;
            while (decrement >= 0)
            {
                string year = (endYear - decrement).ToString();

                if (models.All(m => m.Year != year))
                {
                    models.Add(new TrendVarianceYearOverYearVarianceModel
                    {
                        YearInteger = endYear - decrement,
                        Year = year
                    });
                }

                decrement--;
            }

            return models;
        }

        private static List<TrendVarianceYearOverYearVarianceModel> AddGrowthRows(List<TrendVarianceYearOverYearVarianceModel> models,
            DateTime endDate, int yearsToShow)
        {
            int endYear = endDate.Year;
            var decrement = 0;
            while (decrement < yearsToShow - 1)
            {
                string firstYear = (endYear - decrement).ToString();

                TrendVarianceYearOverYearVarianceModel firstVariance = models.Single(m => m.Year == firstYear);

                string secondYear = (endYear - decrement - 1).ToString();

                TrendVarianceYearOverYearVarianceModel secondVariance = models.Single(m => m.Year == secondYear);

                models.Add(new TrendVarianceYearOverYearVarianceModel
                {
                    Year = firstYear + " Growth",
                    Jan = secondVariance.Jan == decimal.Zero ? null : (firstVariance.Jan - secondVariance.Jan) / secondVariance.Jan,
                    Feb = secondVariance.Feb == decimal.Zero ? null : (firstVariance.Feb - secondVariance.Feb) / secondVariance.Feb,
                    Mar = secondVariance.Mar == decimal.Zero ? null : (firstVariance.Mar - secondVariance.Mar) / secondVariance.Mar,
                    Apr = secondVariance.Apr == decimal.Zero ? null : (firstVariance.Apr - secondVariance.Apr) / secondVariance.Apr,
                    May = secondVariance.May == decimal.Zero ? null : (firstVariance.May - secondVariance.May) / secondVariance.May,
                    Jun = secondVariance.Jun == decimal.Zero ? null : (firstVariance.Jun - secondVariance.Jun) / secondVariance.Jun,
                    Jul = secondVariance.Jul == decimal.Zero ? null : (firstVariance.Jul - secondVariance.Jul) / secondVariance.Jul,
                    Aug = secondVariance.Aug == decimal.Zero ? null : (firstVariance.Aug - secondVariance.Aug) / secondVariance.Aug,
                    Sep = secondVariance.Sep == decimal.Zero ? null : (firstVariance.Sep - secondVariance.Sep) / secondVariance.Sep,
                    Oct = secondVariance.Oct == decimal.Zero ? null : (firstVariance.Oct - secondVariance.Oct) / secondVariance.Oct,
                    Nov = secondVariance.Nov == decimal.Zero ? null : (firstVariance.Nov - secondVariance.Nov) / secondVariance.Nov,
                    Dec = secondVariance.Dec == decimal.Zero ? null : (firstVariance.Dec - secondVariance.Dec) / secondVariance.Dec,
                    Ytd = secondVariance.Ytd == decimal.Zero ? null : (firstVariance.Ytd - secondVariance.Ytd) / secondVariance.Ytd,
                    Total = secondVariance.Total == decimal.Zero ? null : (firstVariance.Total - secondVariance.Total) / secondVariance.Total
                });

                decrement++;
            }

            return models;
        }

        private static bool MonthlyModelMonthPredicate(TrendVarianceSalesForMonthModel dataModel, Month month)
        {
            return dataModel.Month == (int) month;
        }

        private static decimal SumSelector(TrendVarianceSalesForMonthModel dataModel, bool viewBySales)
        {
            return viewBySales ? dataModel.ExtPrice : dataModel.GrossProfit;
        }
    }
}