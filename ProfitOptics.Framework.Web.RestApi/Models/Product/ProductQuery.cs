using ProfitOptics.Framework.Web.RestApi.Models;

namespace ProfitOptics.Framework.Web.RestApi.Models.Product
{
    public class ProductQuery : Query
    {
        public int? DistributorId { get; set; }

        public int? CustomerId { get; set; }

        public ProductQuery(int? distributorId, int? customerId, int page, int itemsPerPage, string sortColumn, string sortDirection, string filterColumn, string filterValue) 
            : base(page, itemsPerPage, sortColumn, sortDirection, filterColumn, filterValue)
        {
            DistributorId = distributorId;

            CustomerId = customerId;
        }
    }
}