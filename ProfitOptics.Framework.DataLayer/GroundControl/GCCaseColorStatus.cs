﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCCaseColorStatus
    {
        public int Id { get; set; }
        public int GCColorStatusTypeId { get; set; }
        public int GCCaseId { get; set; }
        public DateTime Timestamp { get; set; }
        public bool IsActiveStatus { get; set; }

        public virtual GCColorStatusType GCColorStatusType { get; set; }
        public virtual GCCase GCCase { get; set; }
    }
}
