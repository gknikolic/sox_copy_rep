﻿namespace ProfitOptics.Modules.Support
{
    public class SupportSettings
    {
        public string SupportJiraUsername { get; set; }

        public string SupportJiraPassword { get; set; }

        public string SupportJiraUrlBasic { get; set; }

        public string SupportJiraUrl { get; set; }

        public string SupportJiraUrlAttachment { get; set; }

        public string SupportJiraUrlIssue { get; set; }

        public string SupportJiraUrlStatus { get; set; }

        public string SupportJiraUrlIssueRank { get; set; }

        public string SupportJiraUrlAgile { get; set; }

        public string SupportJiraUrlWebhook { get; set; }

        public string SupportJiraLogoImg { get; set; }
    }
}