﻿using ComponentSpace.Saml2.Assertions;
using Intuit.Ipp.Data;
using Intuit.Ipp.OAuth2PlatformClient;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.Anotations;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Factories;
using ProfitOptics.Modules.Sox.Models.QuickBooks;
using ProfitOptics.Modules.Sox.Models.ViewModels;
using ProfitOptics.Modules.Sox.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Controllers
{
    [Area("Sox")]
    [Authorize]
    public class AccountingsController : BaseController
    {
        private readonly ISoxFactory _SoxFactory;
        private readonly ISoxService _soxService;
        private readonly IGroundControlService _groundControlService;
        private readonly IUserService _userService;
        private readonly IHelperService _helperService;

        public AccountingsController(ISoxFactory SoxFactory,
                             ISoxService soxService,
                             IGroundControlService groundControlService,
                             IUserService userService,
                             IHelperService helperService)
        {
            _SoxFactory = SoxFactory;
            _soxService = soxService;
            _groundControlService = groundControlService;
            _userService = userService;
            _helperService = helperService;
        }

        //[Route("Sox/Accountings/{id?}")]
        [Authorize(Roles = "superadmin,admin,user")]
        public IActionResult Index(int? id)
        {
            var vm = new AccountingsMainVM();
            vm.Title = "Invoicing";
            vm.SelectedCustomer = id;
            vm.QBAuthorized = _groundControlService.IsQuickBooksAuthorized();
            var customers = _soxService.GetAllGCCustomers(true).Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem() { Value = x.Id.ToString(), Text = x.CustomerName, Selected = (id.HasValue && id.Value.Equals(x.Id))  }).ToList();
            vm.Customers = customers;
            var gcInvoices = new List<GCInvoice>();
            if (id.HasValue)
            {
                gcInvoices = _soxService.GetGCInvoicesForCustomer(id.Value, true);
            }
            vm.GCInvoices = gcInvoices;
            string qbInstanceUrl = null;
            var qbOptions = _groundControlService.GetQuickBooksOptions();
            if (qbOptions != null && !string.IsNullOrWhiteSpace(qbOptions.InstanceUrl))
            {
                qbInstanceUrl = qbOptions.InstanceUrl;
            }
            vm.QBExternalUrl = qbInstanceUrl;
            if(id.HasValue)
            {
                var customer = _soxService.GetCustomer(id.Value);
                if(customer != null && customer.QBBillingType.HasValue)
                {
                    vm.SelectedCustomerName = customer.CustomerName;
                    vm.QBBillingType = (QBBillingType)customer.QBBillingType.Value;
                }
            }
            return View(vm);
        }

        [Authorize(Roles = "superadmin,admin,user")]
        public IActionResult QuickBooks()
        {
            var vm = new AccountingsVM();
            vm.Title = "Quick Books integration";
            vm.QBAuthorized = _groundControlService.IsQuickBooksAuthorized();
			return View(vm);
		}

        [Authorize(Roles = "superadmin,admin,user")]
        public IActionResult ProcedureTypes()
        {
            var vm = _SoxFactory.PrepareProcedureTypesModel();
            vm.Title = "Procedure Types";
            return View(vm);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult EditProcedureTypeDialog(int procedureTypeId)
        {
            ProcedureTypeModel model = _SoxFactory.PrepareProcedureTypeModel(procedureTypeId);
            return PartialView("_UpdateProcedureTypeModal", model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult UpdateProcedureType(ProcedureTypeModel model)
        {
            _soxService.UpdateProcedureType(model);
            return RedirectToAction("ProcedureTypes");
        }

        [Authorize(Roles = "superadmin,admin,user")]
        public IActionResult Procedures()
        {
            ProceduresModel vm = _SoxFactory.PrepareProceduresModel();
            vm.Title = "Procedures";
            return View(vm);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpGet]
        public IActionResult EditProcedureDialog(int procedureId)
        {
            ProcedureModel model = _SoxFactory.PrepareProcedureModel(procedureId);
            return PartialView("_UpdateProcedureModal", model);
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public IActionResult UpdateProcedure(ProcedureModel model)
        {
            _soxService.UpdateProcedure(model);
            return RedirectToAction("Procedures");
        }

        public IActionResult Authorize()
        {
            //var scope = OidcScopes.Accounting.GetStringValue() + " " + OidcScopes.Payment.GetStringValue();
            var authorizeUrl = _groundControlService.GetQBAuthorizeUrl();
            // perform the redirect here.
            return Redirect(authorizeUrl);
        }

        public IActionResult Callback()
        {
            var code = Request.Query["code"];
            var result = _groundControlService.UpdateQBToken(code);
            if(result != null)
            {
                var dict = new Dictionary<string, string>();
                dict.Add(Enums.QBTokenEnum.quickbooks_access_token.ToString(), result.AccessToken ?? string.Empty);
                dict.Add(Enums.QBTokenEnum.quickbooks_access_token_expires_at.ToString(), DateTime.UtcNow.AddSeconds(result.AccessTokenExpiresIn).ToString());
                _groundControlService.AddOrEditUserClaims(User.GetUserId(), dict);
                _groundControlService.AddOrUpdatePOSetting(Enums.POSettingsType.QuickBooksRefreshToken, result.RefreshToken, User.GetFullName());
                _groundControlService.AddOrUpdatePOSetting(Enums.POSettingsType.QuickBooksRefreshTokenExpiresAt, DateTime.UtcNow.AddSeconds(result.RefreshTokenExpiresIn).ToString(), User.GetFullName());
            }

            return Redirect("QuickBooks");
        }

        public IActionResult Revoke()
        {
            this._helperService.RevokeQBAccessTokenAsync(User.GetUserId());
            return Redirect("QuickBooks");
        }

        public IActionResult ColorUpdate()
        {
            var result = "Success!";

            try
            {
                var gcCases = _soxService.GetGCCases();
                if (gcCases != null && gcCases.Count > 0)
                {
                    gcCases.ForEach(x =>
                    {
                        _soxService.UpdateGCCaseColorStatus(x.Id);
                    });
                }
            }
            catch(Exception e)
            {
                result = e.Message;
            }

            return Content(result);
        }

        public IActionResult SkuUpdate(bool? overwrite = false)
        {
            var result = "Success!";

            try
            {
                _soxService.AddSKUToCTContainerTypes(overwrite);
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return Content(result);
        }

        public IActionResult QbCustomerTest()
        {
            var token = _helperService.GetQuickBooksAccessToken(User.GetUserId());

            var customerId = 59;

            // Test Creating QB Customer:
            //var displayName = $"Customer_Test-{DateTime.Now.ToString("MMddyyhhmm")}";
            //var customerToEdit = new Models.QuickBooks.Customer()
            //{
            //    Id = customerId,
            //    DisplayName = displayName,
            //    Notes = "Creating a QB Customer using API call from GC",
            //    BillAddr = new Models.QuickBooks.Address()
            //    {
            //        Line1 = "Line1",
            //        City = "City",
            //        Country = "Country",
            //        CountrySubDivisionCode = "CountrySubDivisionCode",
            //        PostalCode = "PostalCode"
            //    },
            //    PrimaryPhone = new Models.QuickBooks.Phone()
            //    { 
            //        FreeFormNumber = "FreeFormNumber"
            //    },
            //    PrimaryEmailAddr = new Models.QuickBooks.Email()
            //    {
            //        Address = $"{displayName}@yopmail.com"
            //    }
            //};
            var customer = _groundControlService.GetQBCustomer(customerId, token);
            if (customer != null)
            {
                customer.Notes += " Edited at: " + DateTime.UtcNow.ToString();
                var syncToken = customer != null ? customer.SyncToken : "0";
                var result = _groundControlService.EditQBCustomer(customer, token);
                var isActive = !customer.Active;
                syncToken = (result != null && result.Customer != null) ? result.Customer.SyncToken : syncToken;
                EditCustomerResponse statusUpdate = _groundControlService.SetQBCustomerActiveStatus(customerId, isActive, token, syncToken);
            }

            var customers = _groundControlService.GetQBCustomerList(token);
            var vm = new QbCustomerTestVM();
            vm.Customers = customers;
            vm.Status = customers != null ? $"Successfully fetched {customers.Count} QB Customers" : "- no data returned -";
            return View(vm);
        }

        //private void SetTempState(string state)
        //{
        //    var tempId = new ClaimsIdentity("TempState");
        //    tempId.AddClaim(new Claim("state", state));

        //    Request.GetOwinContext().Authentication.SignIn(tempId);
        //}

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public async Task<IActionResult> PullTraysOrCasesInvoiceInfo(int customerId, string month)
        {
            try
            {
                await _groundControlService.PushTrayOrCaseInfoToQuickBooks(customerId, month, _helperService.GetQuickBooksAccessToken(User.GetUserId()));
                return Ok(new { success = true, message = "Success!" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public async Task<IActionResult> PushInvoiceDataToQB(int customerId)
        {
            try
            {
                var gcInvoices = _soxService.GetGCInvoicesForCustomer(customerId, true, true);
                if(gcInvoices.Count > 0)
                {
                    var userId = User.GetUserId();
                    var qbToken = _helperService.GetQuickBooksAccessToken(userId);

                    foreach (var gcInvoice in gcInvoices)
                    {
                        await _groundControlService.PushInvoicesToQB(qbToken, gcInvoice);
                    }
                }
                return Ok(new { success = true, message = "Success!" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        [Authorize(Roles = "superadmin,admin,user,vendoruser,customeruser")]
        [HttpPost]
        public async Task<IActionResult> RemoveGCInvoiceLine(int gcInvoiceLineId)
        {
            try
            {
                await _soxService.SetEndDateToGCInvoiceLine(gcInvoiceLineId);
                return Ok(new { success = true, message = "Success!" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}