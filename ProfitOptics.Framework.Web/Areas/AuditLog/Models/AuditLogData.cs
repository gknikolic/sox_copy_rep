﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using ProfitOptics.Framework.Core.Data;
using System;

namespace ProfitOptics.Framework.Web.Areas.AuditLog.Models
{
    public class AuditLogData : IDataRecord
    {
        [DataField("Date / Time", "MM/dd/yyyy h:mm:ss", 1)]
        public DateTime? Inserted { get; set; }

        public string Message { get; set; }

        public bool? IsSuccess { get; set; }

        [DataField("Username", "", 2)]
        public string Username { get; set; }

        [DataField("Action Type", "", 3)]
        public string StatusValue { get; set; }

        public string Url { get; set; }

        [DataField("Area", "", 4)]
        public string Area { get; set; }

        [DataField("Controller", "", 5)]
        public string Controller { get; set; }

        [DataField("Action", "", 6)]
        public string Action { get; set; }

        [DataField("IpAddress", "", 7)]
        public string IpAddress { get; set; }

        [DataField("Browser", "", 8)]
        public string Browser { get; set; }

        [DataField("Friendly Name", "", 9)]
        public string FriendlyName { get; set; }

        public string ReflectedType { get; set; }

        public string ReflectedActionName { get; set; }
    }

    public class AuditLogFriendlyNameData : IDataRecord
    {
        public string ReflectedType { get; set; }
        public string ReflectedActionName { get; set; }
        public string FriendlyName { get; set; }
    }

}