﻿var Tree = function () {
    var Private = {
        Methods: {
            Step: function(tab) {
                switch (tab) {
                    case 1:
                        Private.Methods.Step1();
                        break;
                    case 2:
                        Private.Methods.Step2(); 
                        break;
                    case 3:
                        Private.Methods.Step3();
                        break;
                    default:
                        $("#accordionFilters").accordion('option', 'active', 0);
                        $(".apply-selected-filters").attr("data-accordion", 1).text("NEXT");
                        $(".go-back").attr("data-accordion", 1).hide();
                        Private.Methods.ValidateDatePicker();
                        break;
                };
            },

            ValidateDatePicker: function () {
                var validDateformat = /([0]?[1-9]|[1-2]\\d|3[0-1])/;
                var error = 0;
               $(".filters .datepicker").each(function () {
                   var value = $(this).val();
                   if (value == "") {
                       error++;
                   } else if (!validDateformat.test(value)) {
                       error++;
                   }
                   
               });
                
               if (error > 0) {
                   $(".apply-selected-filters").prop("disabled", true);
               } else {
                   $(".apply-selected-filters").prop("disabled", false);
               }

               return error;
            },

            ValidatejsTree: function () {
                var error = 0;
                
                $('.jstree').each(function (i) {
                    var value = $(this).jstree(true).get_selected();
                    var tree = "";
                    if (value.length < 1) {
                        tree = i;
                        error++;
                    }
                });

                if (error > 0) {
                    $(".apply-selected-filters").prop("disabled", true);
                } else {
                    $(".apply-selected-filters").prop("disabled", false);
                }

                return error;
            },

            ValidateLevelFilters: function () {
                var error = 0;

                $(".selection-levels  input[type=radio]").each(function () {
                    var levelChecked = $(this).is(":checked");
                    if (levelChecked) {
                        error++;
                    }

                });

                if (error < 2) {
                    $(".apply-selected-filters").prop("disabled", true);                   
                } else {
                    $(".apply-selected-filters").prop("disabled", false);
                }

                return error;
            },

            Step1: function () {
                if (Private.Methods.ValidateDatePicker() == 0) {

                    $(".filters .datepicker").each(function (i) {
                        $(".periods-heading .subtitle.selected-range .date" + i).text("").append($(this).val());
                    });

                    $("#accordionFilters h3:first-of-type .subtitle").hide();
                    $("#accordionFilters h3:first-of-type .subtitle.selected-range").css("display", "block");
                    $(".go-back").show();

                    $("#accordionFilters h3:nth-of-type(2)").removeClass("ui-state-disabled");
                    $(".apply-selected-filters").attr("data-accordion", 2);
                    $(".go-back").attr("data-accordion", 2);
                    $(".apply-selected-filters").text("NEXT").prop("disabled", true);
                    $("#accordionFilters").accordion('option', 'active', 1);
                    Private.Methods.ValidatejsTree();
                    $(".error-info").hide().text('');
                } else {
                    $(".error-info").show().text("Fill all date fields('MM/dd/yyyy')!");
                    $("#accordionFilters").accordion('option', 'active', 0);
                }
            },

            Step2: function () {
                var areCheckedLocationNodes = $('#jstree').jstree(true).get_selected();
                var areCheckedProductNodes = $('#jstree1').jstree(true).get_selected();
                var includeSDItems = $("#IncludeSDItems").is(':checked');

                $(".go-back").show();

                if (Private.Methods.ValidateDatePicker() == 0) {
                    $(".error-info").hide().text('');
                    if (Private.Methods.ValidatejsTree() == 0) {

                        $("#accordionFilters h3:nth-of-type(2) .subtitle").hide();
                        $("#accordionFilters h3:nth-of-type(2) .subtitle.selected-nodes").css("display", "block");

                        $(".filters .datepicker").each(function (i) {
                            $(".periods-heading .subtitle.selected-range .date" + i).text("").append($(this).val());
                        });

                        $(".selected-nodes .locations").text(areCheckedLocationNodes.length);
                        $(".selected-nodes .products").text(areCheckedProductNodes.length);

                        Main.GetMinLevel();

                        $("#accordionFilters h3:last-of-type()").removeClass("ui-state-disabled");
                        
                        $(".apply-selected-filters").attr("data-accordion", 3).prop("disabled", true).text("GO");
                        $(".go-back").attr("data-accordion", 3);
                        $("#accordionFilters").accordion('option', 'active', 2);
                        Private.Methods.ValidateLevelFilters();
                        $(".error-info").hide().text('');
                    } else {

                        $(".error-info").show().text("Check at least one node in both Location and Products filters tree!");

                        $("#accordionFilters").accordion('option', 'active', 1);
                        $(".apply-selected-filters").attr("data-accordion", 2).prop("disabled", true);
                        $(".go-back").attr("data-accordion", 2);
                    }
                } else {
                    $(".error-info").show().text("Fill all date fields('MM/dd/yyyy')!");
                    $("#accordionFilters").accordion('option', 'active', 0);
                    $(".apply-selected-filters").attr("data-accordion", 1).prop("disabled", true);
                    $(".go-back").attr("data-accordion", 1).hide();
                }
                if (includeSDItems) {
                    $("input[name='productSelection']").attr("disabled", true);
                    $("input[name='productSelection']").prop("checked", false);
                    $("input[id='ProductSubgroup']").prop("checked", true);
                } else {
                    $("input[name='productSelection']").attr("disabled", false);
                }


            },

            Step3: function () {
                var firstPeriodFrom = $(".filters .first-period .from").val();
                var firstPeriodTo = $(".filters .first-period .to").val();
                var secondPeriodFrom = $(".filters .second-period .from").val();
                var secondPeriodTo = $(".filters .second-period .to").val();
                
                var areCheckedLocationNodes = $('#jstree').jstree(true).get_selected();
                var areCheckedProductNodes = $('#jstree1').jstree(true).get_selected();

                if (Private.Methods.ValidateDatePicker() == 0 && Private.Methods.ValidatejsTree() == 0 && Private.Methods.ValidateLevelFilters() == 2) {
                    $("#accordionFilters h3:last-of-type() .subtitle").hide();
                    $("#accordionFilters h3:last-of-type() .subtitle.selected-level").css("display", "block");


                    var productSelectionLevel = $(".product-selection-levels input:checked").val();
                    var locationSelectionLevel = $(".location-selection-levels input:checked").val();

                    var productSelectionLevelText = $(".product-selection-levels input:checked").next('label').text();
                    var locationSelectionLevelText = $(".location-selection-levels input:checked").next('label').text();

                    $(".selected-level .locations-level").text(locationSelectionLevelText);
                    $(".selected-level .products-level").text(productSelectionLevelText);

                    blockUI("Getting statistics data...");

                    treeArrayProducts = [];
                    var treeData = $('#jstree1').jstree(true).get_json('#', { flat: false });
                    $.each(treeData, function (i) {
                        Main.ChildrenProducts(treeData[i]);
                        treeArrayProducts.push(Main.ChildrenProducts(treeData[i]));
                    });

                    treeArrayLocations = [];
                    var treeDataLocations = $('#jstree').jstree(true).get_json('#', { flat: false });
                    $.each(treeDataLocations, function (i) {
                        Main.ChildrenLocations(treeDataLocations[i]);
                        treeArrayLocations.push(Main.ChildrenLocations(treeDataLocations[i]));
                    });

                    //Ajax to controller action
                    var applyFilterPath = $("#applyFilterPath").val();

                    var firstPeriod = {
                        "StartDate": firstPeriodFrom,
                        "EndDate": firstPeriodTo
                    };

                    var secondPeriod = {
                        "StartDate": secondPeriodFrom,
                        "EndDate": secondPeriodTo
                    };

                    var checkedNodes = [];
                    var listOfLocationCheckedModes = [];
                    var listOfProductCheckedModes = [];

                    listOfLocationCheckedModes = $('#jstree').jstree(true).get_selected();
                    listOfProductCheckedModes = $('#jstree1').jstree(true).get_selected();
                    
                    checkedNodes.push(listOfLocationCheckedModes);
                    checkedNodes.push(listOfProductCheckedModes);



                    $.ajax({
                        method: "POST",
                        //traditional: false,
                        url: applyFilterPath,
                        dataType: 'json',
                        data: {
                            productSelectionLevel: productSelectionLevel,
                            firstPeriod: firstPeriod,
                            secondPeriod: secondPeriod,
                            
                            locationSelectionLevel: locationSelectionLevel,
                            checkedNodes: checkedNodes,
                            products: treeArrayProducts,
                            locations: treeArrayLocations
                        },
                        success: function (data) {

                            $.unblockUI();

                            if (data.Success) {
                                $(".filters .datepicker").each(function () {
                                    $(this).val("");
                                });
                                $(".product-selection-levels input").prop("checked", false);
                                $(".location-selection-levels input").prop("checked", false);

                                var url = $("#redirectToStatistics").val();
                                window.location.href = url;
                                $(".error-info").hide().text('');
                                
                            } else {
                                $.unblockUI();
                                $(".error-info").show().text(data.ErrorMessage);
                            }
                        },
                        error: function (e) {
                            $.unblockUI();
                            $(".error-info").show().text("Error sending data!");
                        }
                    });
                    $(".error-info").hide().text('');
                } else {
                    $(".error-info").show().text("Please fill all fields.");
                }
            },

            DatePicker: function () {
                //$(".filters .datepicker").each(function () {
                //    $(this).val("");
                //});

                $(".first-period .from")
                .datepicker({
                    format: 'yyyy-mm',
                    startDate: new Date('08/01/2014'),
                    viewMode: "months",
                    minViewMode: "months"
                })
                .on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf());
                    $(".first-period .to").val('').datepicker('setStartDate', minDate);
                    $(".second-period .from").val('').datepicker('setStartDate', Private.Methods.AddMonths(minDate, 1));
                    $(".second-period .to").val('').datepicker('setStartDate', Private.Methods.AddMonths(minDate, 1));
                    Private.Methods.ValidateDatePicker();
                });


                $(".first-period .to").datepicker({
                    format: 'yyyy-mm',
                    startDate: new Date('08/01/2014'),
                    viewMode: "months",
                    minViewMode: "months"
                })
                .on("changeDate", function (selected) {
                    var maxDate = new Date(selected.date.valueOf());
                    $(".second-period .from").val('').datepicker('setStartDate', Private.Methods.AddMonths(maxDate, 1));
                    $(".second-period .to").val('').datepicker('setStartDate', Private.Methods.AddMonths(maxDate, 1));
                    Private.Methods.ValidateDatePicker();
                });

                $(".second-period .from")
                .datepicker({
                    format: 'yyyy-mm',
                    startDate: new Date('08/01/2014'),
                    viewMode: "months",
                    minViewMode: "months"
                })
                .on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf());
                    Private.Methods.AddDays(minDate, -1);
                    $(".second-period .to").val('').datepicker('setStartDate', minDate);
                    Private.Methods.ValidateDatePicker();
                });

                $(".second-period .to").datepicker({
                    format: 'yyyy-mm',
                    startDate: new Date('08/01/2014'),
                    viewMode: "months",
                    minViewMode: "months"
                })
                .on('changeDate', function (selected) {
                    Private.Methods.ValidateDatePicker();
                });
            },

            CheckAllNodes: function () {
                $(".selected-tree-nodes").jstree("check_all");
                $(".initial-tree-nodes").jstree("check_all");
            },

            AddDays: function (startDate, numberOfDays) {
                var returnDate = new Date(
                               startDate.getFullYear(),
                               startDate.getMonth(),
                               startDate.getDate() + numberOfDays,
                               startDate.getHours(),
                               startDate.getMinutes(),
                               startDate.getSeconds());
                return returnDate;
            },

            AddMonths: function (startDate, numberOfMonths) {
                var returnDate = new Date(
                               startDate.getFullYear(),
                               startDate.getMonth() + numberOfMonths,
                               startDate.getDate(),
                               startDate.getHours(),
                               startDate.getMinutes(),
                               startDate.getSeconds());
                return returnDate;
            },

          /*  RefreshThirdTree: function () {
                $('#jstree3').jstree(true).settings.core.data = newData;
                $('#jstree3').jstree(true).refresh();
            }*/
        },
        

        Events: {
            NextButtonClick: function () {
                $("body").on("click", ".apply-selected-filters", function (e) {
                    var tabNumber = $(this).attr("data-accordion");
                    Private.Methods.Step(parseInt(tabNumber));
                });

                $("#accordionFilters h3").on("click", function () {
                    var currentTab = $(this).attr("data-id");
                    var disabled = $(this).hasClass("ui-state-disabled");
                    if (!disabled) {
                        Private.Methods.Step(parseInt(parseInt(currentTab) - 1));
                    }
                });
            },

            GoBackButton: function () {
                $("body").on("click", ".go-back", function (e) {
                    var tabNumber = $(this).attr("data-accordion");
                    var previousTab = parseInt(tabNumber) - 2;
                    
                    if (previousTab >= 0) {
                        Private.Methods.Step(parseInt(previousTab));
                    } else {
                        $(".go-back").hide();
                    }
                });
            },

            ResetButton: function () {
                $(".reset-selected-filters").on("click", function () {
                    $(".filters .datepicker").each(function () {
                        $(this).val("").datepicker('setStartDate', null);
                    });

                    $(".product-selection-levels input").prop("checked", false);
                    $(".location-selection-levels input").prop("checked", false);
                    $("#jstree").jstree("check_all");
                    $("#jstree1").jstree("check_all");

                    $("#accordionFilters h3:nth-of-type(2)").addClass("ui-state-disabled");
                    $("#accordionFilters h3:last-of-type()").addClass("ui-state-disabled");
                    $("#accordionFilters").accordion('option', 'active', 0);
                    $(".apply-selected-filters").attr("data-accordion", 1).text("NEXT").prop("disabled", true);
                    $("#accordionFilters h3 .subtitle").css("display", "block");
                    $(".selected-nodes, .selected-range, .selected-level").css("display", "none");
                    $(".go-back").hide().attr("data-accordion", 1);
                });
            },

            OnSelectTreeNode: function() {
                $(".jstree").on("changed.jstree", function () {
                    Private.Methods.ValidatejsTree();
                });
            },

            OnChangeLevels: function () {
                $(".selection-levels input[type='radio']:radio").on("change", function () {
                    Private.Methods.ValidateLevelFilters();
                });
            },
        },
    };

    var Public = {
        DisableHierachyFilters: function (minLocationLevel, minProductLevel) {
            var hierarchyLocationLevel = $(".location-selection-levels input");
            var hierarchyProductLevel = $(".product-selection-levels input");

           
            hierarchyLocationLevel.attr("disabled", false);
            hierarchyProductLevel.attr("disabled", false);
            

            $(".location-selection-levels input[type='radio']:radio").each(function (i) {
                if (i + 1 < minLocationLevel) {
                    $(this).prop("checked", false);
                    $(this).attr('disabled', true);
                }
            });

            $(".product-selection-levels input[type='radio']:radio").each(function (i) {
                if (i + 1 < minProductLevel) {
                    $(this).prop("checked", false);
                    $(this).attr('disabled', true);
                }
            });
        },

        Init: function () {
            Private.Methods.DatePicker();
            Private.Methods.CheckAllNodes();
         //   Private.Methods.RefreshThirdTree();
            Private.Events.NextButtonClick();

            Private.Events.ResetButton();
            Private.Events.OnChangeLevels();
            Private.Events.GoBackButton();
            Private.Events.OnSelectTreeNode();
        }
    };
    return Public;
}();

Tree.Init();