﻿using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Models.Select2;

namespace ProfitOptics.Modules.Showcase.Models
{
    public class MiscViewModel
    {
        public List<Select2Model> DropdownData { get; set; }
    }
}
