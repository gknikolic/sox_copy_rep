﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.IO;
using System.Reflection;
using ProfitOptics.Framework.Core.Settings;

namespace ProfitOptics.Framework.Core.System
{
    public class IO : IIO
    {
        private readonly Common _settings;

        public IO(Common settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Gets directory of executing assembly
        /// </summary>
        public string GetAssemblyDir()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        /// <summary>
        /// Gets location of temp folder.
        /// </summary>
        /// <returns></returns>
        public string GetTempFolder()
        {
            return _settings.TempDirectory;
        }

        /// <summary>
        /// Gets location of folder with attachments.
        /// </summary>
        /// <returns></returns>
        public string GetAttachmentsFolder()
        {
            return _settings.AttachmentsDirectory;
        }

        /// <summary>
        /// Compares two files byte by byte
        /// Taken from: http://stackoverflow.com/questions/1358510/how-to-compare-2-files-fast-using-net
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public bool FilesAreEqual(string firstFileName, string secondFileName)
        {
            var first = new FileInfo(firstFileName);
            var second = new FileInfo(secondFileName);
            const int BYTES_TO_READ = sizeof(Int64);
            if (first.Length != second.Length)
                return false;

            int iterations = (int)Math.Ceiling((double)first.Length / BYTES_TO_READ);
            using (FileStream fs1 = first.OpenRead())
            using (FileStream fs2 = second.OpenRead())
            {
                byte[] one = new byte[BYTES_TO_READ];
                byte[] two = new byte[BYTES_TO_READ];

                for (int i = 0; i < iterations; i++)
                {
                    fs1.Read(one, 0, BYTES_TO_READ);
                    fs2.Read(two, 0, BYTES_TO_READ);

                    if (BitConverter.ToInt64(one, 0) != BitConverter.ToInt64(two, 0))
                        return false;
                }
            }

            return true;
        }
    }
}
