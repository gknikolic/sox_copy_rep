﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Enums
{
    public enum ETLStatusType
    {
        OK = 1,
        Error = 2
    }
}
