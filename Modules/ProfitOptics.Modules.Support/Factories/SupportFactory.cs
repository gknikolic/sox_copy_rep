﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Support.Models.JiraRestModels;
using ProfitOptics.Modules.Support.Models.ViewModels;
using ProfitOptics.Modules.Support.Services;
using BacklogIssueModel = ProfitOptics.Modules.Support.Models.BacklogIssueModel;
using BoardColumnViewModel = ProfitOptics.Modules.Support.Models.ViewModels.BoardColumnViewModel;
using BoardViewModel = ProfitOptics.Modules.Support.Models.ViewModels.BoardViewModel;
using CreateIssueViewModel = ProfitOptics.Modules.Support.Models.ViewModels.CreateIssueViewModel;
using IssueDataModel = ProfitOptics.Modules.Support.Models.IssueDataModel;
using IssuesViewModel = ProfitOptics.Modules.Support.Models.ViewModels.IssuesViewModel;

namespace ProfitOptics.Modules.Support.Factories
{
    public class SupportFactory : ISupportFactory
    {
        #region Fields

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISupportService _supportService;
        private readonly IPersistedDataService _persistedDataService;
        private readonly IJiraService _jiraService;
        private readonly ISupportMemoryCacheService _supportMemoryCacheService;

        #endregion Fields

        #region Ctor

        public SupportFactory(
            IHttpContextAccessor httpContextAccessor,
            ISupportService supportService,
            IPersistedDataService persistedDataService,
            IJiraService jiraService,
            ISupportMemoryCacheService supportMemoryCacheService)
        {
            _httpContextAccessor = httpContextAccessor;
            _supportService = supportService;
            _persistedDataService = persistedDataService;
            _jiraService = jiraService;
            _supportMemoryCacheService = supportMemoryCacheService;
        }

        #endregion Ctor

        #region Methods

        #region View Model Preparation

        public IssuesViewModel PrepareIssuesViewModel(string suppMsg)
        {
            var selectedProjectKey = _persistedDataService.GetSelectedProject();

            if (string.IsNullOrEmpty(selectedProjectKey))
            {
                selectedProjectKey = _supportMemoryCacheService.GetFirstProjectKey();
                _persistedDataService.SetSelectedProject(selectedProjectKey);
            }

            List<SelectListItem> assigneesSelectList = PrepareAssigneeSelectListModel(selectedProjectKey);

            var selectedAssignee = _persistedDataService.GetSelectedAssignee();

            List<SelectListItem> reporterSelectList = PrepareReportersList(selectedProjectKey);

            var selectedReporter = _persistedDataService.GetSelectedReporter();

            List<SelectListItem> componentList = PrepareComponentsList(selectedProjectKey);

            List<string> selectedComponents = _persistedDataService.GetSelectedMultipleComponents();

            var boardConfiguration = _supportMemoryCacheService.GetBoardConfiguration(selectedProjectKey);

            var hasBacklog = _jiraService.HasBackLog(boardConfiguration);

            var model = new IssuesViewModel
            {
                SupportMessage = !string.IsNullOrEmpty(suppMsg) ? suppMsg : string.Empty,
                ProjectSelectList = PrepareProjectsList(),
                SelectedProjectKey = selectedProjectKey,
                AssigneesSelectList = assigneesSelectList,
                SelectedAssignee = assigneesSelectList.FirstOrDefault(x => x.Value == selectedAssignee)?.Value,
                ReporterSelectList = reporterSelectList,
                SelectedReporter = reporterSelectList.FirstOrDefault(x => x.Value == selectedReporter)?.Value,
                Board = PrepareBoardModel(boardConfiguration),
                HasBacklog = hasBacklog,
                AllComponents = componentList,
                SelectedComponents = selectedComponents
            };

            return model;
        }

        public List<SelectListItem> PrepareComponentsList(string selectedProjectKey, bool addEmptyComponent = false)
        {
            var components = _supportMemoryCacheService.GetComponentsForProject(selectedProjectKey);

            var componentList = components.Select(x => new SelectListItem(x.Name, x.Id)).ToList();

            if (addEmptyComponent)
            {
                componentList.Add(new SelectListItem("No Component", ""));
            }

            return componentList;
        }

        public List<SelectListItem> PrepareReportersList(string projectKey)
        {
            var users = _supportMemoryCacheService.GetJiraUsersForProject(projectKey).AsQueryable();

            var userList = users.Select(x => new SelectListItem(x.DisplayName, x.Key))
                .Prepend(new SelectListItem("All", string.Empty)).ToList();

            return userList;
        }

        private BoardViewModel PrepareBoardModel(BoardConfiguration config)
        {
            if (config?.ColumnConfig?.Columns == null || config.ColumnConfig.Columns.Length == 0)
            {
                return null;
            }

            var boardColumnViewModels = config.ColumnConfig.Columns
                .Where(x => config.Type == SupportModuleConstants.Jira.BoardTypes.Kanban && x.Name != SupportModuleConstants.Jira.BoardColumnNames.Backlog
                            || config.Type == SupportModuleConstants.Jira.BoardTypes.Scrum)
                .Select(x => new BoardColumnViewModel
                {
                    ColumnName = x.Name,
                    Statuses = x.Statuses?.Select(s => s.Id).ToList() ?? new List<string>()
                }).ToList();

            return new BoardViewModel
            {
                Columns = boardColumnViewModels
            };
        }

        public List<SelectListItem> PrepareProjectsList()
        {
            var projects = _supportMemoryCacheService.GetProjects();

            return projects.Select(x => new SelectListItem(x.Name, x.Key)).ToList();
        }

        public UpdateIssueViewModel PrepareUpdateIssueModel(string issueKey)
        {
            var issue = _jiraService.GetIssueById(issueKey);

            if (issue == null)
            {
                return new UpdateIssueViewModel();
            }

            var project = _persistedDataService.GetSelectedProject();

            var comments = Task.Run(async () => await issue.GetCommentsAsync()).GetAwaiter().GetResult();

            var attachments = Task.Run(async () => await issue.GetAttachmentsAsync()).GetAwaiter().GetResult();

            var result = new UpdateIssueViewModel
            {
                Type = issue.Type.Name,
                IssueKey = issueKey,
                Priority = issue.Priority.Name,
                Summary = issue.Summary,
                EpicName = issue.CustomFields.FirstOrDefault(x => x.Name.Equals("Epic Name"))?.Values.First() ?? string.Empty,
                IssueId = issue.JiraIdentifier,
                Description = issue.Description,
                PriorityList = PreparePriorityList(project),
                TypeList = PrepareIssueTypeList(project),
                Comments = comments.Select(item => new IssueComment
                {
                    CommentAuthor = item.Author,
                    CommentBody = item.Body,
                    CommentUpdatedBy = item.UpdateAuthor,
                    CommentCreated = item.CreatedDate ?? new DateTime(),
                    CommentUpdated = item.UpdatedDate ?? new DateTime()
                }).ToList(),
                CommentCount = comments.Count(),
                Attachments = attachments.Select(item => new IssueAttachment
                {
                    FileName = item.FileName,
                    Id = item.Id
                }).ToList(),
                CanEdit = issue.Labels.Contains(PrepareUsername(_httpContextAccessor.HttpContext.User.Identity.Name))
            };

            result.PriorityList.Single(x => x.Text == result.Priority).Selected = true;
            result.TypeList.Single(x => x.Text == result.Type).Selected = true;

            return result;
        }

        public CreateIssueViewModel PrepareCreateIssueModel(string supportMessage, string referrerUrl)
        {
            var project = _persistedDataService.GetSelectedProject();

            var model = new CreateIssueViewModel
            {
                SupportMessage = supportMessage ?? string.Empty,
                Environment = referrerUrl,
                Summary = string.Empty,
                Description = string.Empty,
                ComponentList = PrepareComponentsList(project, true),
                ProjectList = PrepareProjectsList(),
                Project = _persistedDataService.GetSelectedProject(),
                PriorityList = PreparePriorityList(project),
                TypeList = PrepareIssueTypeList(project)
            };

            return model;
        }

        public Select2PagedResult PrepareUsersPagedResult(string projectKey, string searchTerm, int page)
        {
            var users = _supportMemoryCacheService.GetJiraUsersForProject(projectKey).AsQueryable();

            searchTerm = !string.IsNullOrWhiteSpace(searchTerm) ? searchTerm.Trim() : null;

            if (searchTerm != null)
            {
                users = users.Where(x => x.Name.Contains(searchTerm));
            }

            var totalUsers = users.Count();

            var usersList = users.OrderBy(x => x.DisplayName).Skip((page - 1) * 10).Take(10).Select(x => new Select2Model
            { id = x.Name, selected = false, text = x.DisplayName }).ToList();

            return new Select2PagedResult
            {
                results = usersList,
                count_filtered = totalUsers
            };
        }

        public List<SelectListItem> PrepareAssigneeSelectListModel(string projectKey)
        {
            var users = _supportMemoryCacheService.GetJiraUsersForProject(projectKey);

            var assignees = users.Select(x => new SelectListItem(x.DisplayName, x.Name)).OrderBy(x => x.Text)
                .Prepend(new SelectListItem("Unassigned", "null"))
                .Prepend(new SelectListItem("All", string.Empty));

            return assignees.ToList();
        }

        public List<SelectListItem> PrepareIssueTypeList(string projectKey)
        {
            var issueTypes = _supportMemoryCacheService.GetIssueTypesForProject(projectKey);

            var issueTypeList = new List<SelectListItem>();

            issueTypeList.AddRange(issueTypes.Select(issueType =>
                new SelectListItem(issueType.Name, issueType.Name, false)));

            return issueTypeList;
        }

        public List<SelectListItem> PreparePriorityList(string projectKey)
        {
            var priorities = _supportMemoryCacheService.GetPrioritiesForProject(projectKey);

            var priorityList = new List<SelectListItem>();

            priorityList.AddRange(priorities.Select(priority =>
                new SelectListItem(priority.Name, priority.Name, false)));

            return priorityList;
        }

        public List<SelectListItem> PrepareIssueList(string projectKey)
        {
            var configuration = _supportMemoryCacheService.GetBoardConfiguration(projectKey);

            var repo = _jiraService.FetchIssueKeys(projectKey, configuration);

            return repo;
        }

        #endregion View Model Preparation

        #region Issue preparation

        public DataTablesResponse<BacklogIssueModel> PrepareBacklogIssueDataModels(IDataTablesRequest request,
         List<string> components, string project, string assignee, string reporter)
        {
            var boardConfig = _supportMemoryCacheService.GetBoardConfiguration(project);

            var issuesFromRepo = _jiraService.FindBacklogIssues(project, components, assignee, reporter, request.Start, request.Length, boardConfig);

            var issues = new List<BacklogIssueModel>();

            if (issuesFromRepo != null)
            {
                foreach (var tmpIssue in issuesFromRepo)
                {
                    issues.Add(new BacklogIssueModel
                    {
                        CanEdit = !string.IsNullOrEmpty(tmpIssue.LocalCreator) && tmpIssue.LocalCreator ==
                                              PrepareUsername(_httpContextAccessor.HttpContext.User.Identity.Name),
                        LocalCreator = tmpIssue.LocalCreator,
                        Assignee = tmpIssue.AssigneeDisplayName,
                        Description = tmpIssue.Description,
                        Key = tmpIssue.Key,
                        Priority = tmpIssue.Priority,
                        ProjectName = tmpIssue.ProjectName,
                        Reporter = tmpIssue.ReporterName,
                        Status = tmpIssue.Status,
                        Component = tmpIssue.Component,
                        Summary = tmpIssue.Summary,
                        Type = tmpIssue.Type,
                        Updated = tmpIssue.Updated.HasValue ? tmpIssue.Updated.Value.ToString("MM-dd-yyyy") : "N/A",
                        CommentCount = tmpIssue.CommentCount
                    });
                }
            }

            var total = issuesFromRepo?.TotalCount ?? 0;

            return new DataTablesResponse<BacklogIssueModel>(request.Draw, issues, total, total);
        }

        public DataTablesResponse<IssueDataModel> PrepareIssueDataModels(IDataTablesRequest request,
            List<string> statuses, List<string> components, string project, string assignee, string reporter,
            string userEmail)
        {
            var boardConfig = _supportMemoryCacheService.GetBoardConfiguration(project);

            var issuesFromRepo = _jiraService.FindIssues(project, statuses.ToArray(), components.ToArray(), assignee,
                reporter, request.Start, request.Length, boardConfig);

            var issues = new List<IssueDataModel>();

            var isGlobalWatcher = _supportService.IsGlobalWatcher(userEmail);
            var watchedIssues = new List<string>();
            if (!isGlobalWatcher)
            {
                watchedIssues = _supportService.GetWatchedIssues(userEmail, project);
            }

            if (issuesFromRepo != null)
            {
                foreach (var issue in issuesFromRepo)
                {
                    var isWatcher = false;

                    if (isGlobalWatcher)
                    {
                        isWatcher = true;
                    }
                    else
                    {
                        isWatcher = watchedIssues.Any(i => i == issue.Key);
                    }

                    issues.Add(new IssueDataModel
                    {
                        CanEdit = !string.IsNullOrEmpty(issue.LocalCreator) && issue.LocalCreator == PrepareUsername(_httpContextAccessor.HttpContext.User.Identity.Name),
                        LocalCreator = issue.LocalCreator,
                        Assignee = issue.AssigneeDisplayName,
                        Description = issue.Description,
                        DueDate = issue.DueDate.HasValue
                            ? issue.DueDate.Value.ToString("MM-dd-yyyy")
                            : "N/A",
                        Key = issue.Key,
                        Priority = issue.Priority,
                        ProjectName = issue.ProjectName,
                        Reporter = issue.ReporterName,
                        Status = issue.Status,
                        FixVersion = issue.FixVersion,
                        Component = issue.Component,
                        Summary = issue.Summary,
                        Type = issue.Type,
                        Updated = issue.Updated.HasValue
                            ? issue.Updated.Value.ToString("MM-dd-yyyy")
                            : "N/A",
                        IsWatching = isWatcher,
                        IsGlobalWatcher = isGlobalWatcher
                    });
                }
            }

            var total = issuesFromRepo.TotalCount;

            return new DataTablesResponse<IssueDataModel>(request.Draw, issues, total, total);
        }

        #endregion Issue preparation

        #endregion Methods

        #region Helpers

        private string PrepareUsername(string username)
        {
            return "\"" + username.Trim().Replace(" ", string.Empty).ToLower() + "\"";
        }

        #endregion Helpers
    }
}