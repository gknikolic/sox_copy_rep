﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class NoteGroup
	{
		public DateTime Date { get; set; }
		public DateTime DateLocal { get { return ProfitOptics.Modules.Sox.Helpers.Helpers.GetLocalDateTime(Date); } }
		public List<Note> Notes { get; set; }
		public bool HasUserNotes { get; set; }
	}
}
