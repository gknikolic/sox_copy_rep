﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.DataLayer.Test;
using ProfitOptics.Modules.Employee.Models;
using ProfitOptics.Modules.Employee.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Employee.Factories
{
    public class EmployeeFactory : IEmployeeFactory
    {
        #region Fields

        private readonly IEmployeeService _EmployeeService;

        #endregion Fields

        #region Ctor

        public EmployeeFactory(IEmployeeService EmployeeService)
        {
            _EmployeeService = EmployeeService;
        }


        #endregion Ctor

        #region Methods
        public async Task<EmployeeModel> PrepareEmptyModelAsync()
        {
            var employee = new TEmployee();
            //Employee.Id = nextId;
            var availableManagers = await _EmployeeService.GetAllManagersAsync();
            var availableSkills = await _EmployeeService.GetAllSkillsAsync();
            return new EmployeeModel
            {
                FirstName = string.Empty,
                Id = employee.Id,
                LastName = string.Empty,
                ManagerId = -1,
                EmployeeSkillIds = employee.EmployeeSkills.Select(x => x.SkillId).ToList(),
                AllManagers = PrepareManagersSelectListModel(availableManagers),
                AllSkills = PrepareSkillsSelectListModel(availableSkills),
                updateFlag = false
            };
        }
        public async Task<EmployeeModel> PrepareEmployeeModelAsync(int id)
        {
            var employee = await _EmployeeService.GetEmployeeByIdAsync(id);
            var availableManagers = await _EmployeeService.GetAllManagersAsync();
            var availableSkills = await _EmployeeService.GetAllSkillsAsync();
            return new EmployeeModel
            {
                FirstName = employee.FirstName,
                Id = employee.Id,
                LastName = employee.LastName,
                ManagerId = employee.ManagerId,
                EmployeeSkillIds = employee.EmployeeSkills.Select(x => x.SkillId).ToList(),
                AllManagers = PrepareManagersSelectListModel(availableManagers),
                AllSkills = PrepareSkillsSelectListModel(availableSkills),
                updateFlag = true
            };
        }
        private List<SelectListItem> PrepareSkillsSelectListModel(List<TSkill> availableSkills)
{
            return availableSkills.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            }).ToList();
        }
        private List<SelectListItem> PrepareManagersSelectListModel(List<TManager> availableManagers)
        {
            return availableManagers.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = $"{x.FirstName} {x.LastName}"?.Trim() ?? string.Empty
            }).ToList();
        }

        public async Task<AllEmployeesModel> PrepareAllEmployeesModelAsync()
        {
            var employees = await _EmployeeService.GetAllEmployeesAsync();
            var model = new AllEmployeesModel();
            foreach(var e in employees)
            {
                model.Employees.Add(await PrepareEmployeeModelAsync(e.Id));
            }
            return model;
        }




        #endregion
    }
}