﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Microsoft.AspNetCore.Identity;
using ProfitOptics.Framework.Web.Framework.ServiceResults;
using ProfitOptics.Framework.Web.Models.Common;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Models
{
    public class UserManagementServiceResult : ServiceActionResult<UserManagementServiceResultStatus>
    {
        public UserManagementServiceResult()
        {
        }

        public UserManagementServiceResult(IdentityResult result)
        {
            Status = result.Succeeded
                ? UserManagementServiceResultStatus.Success
                : UserManagementServiceResultStatus.Failed;
            Error = string.Join(';', result.Errors.Select(x => x.Description))?.TrimEnd(';');
        }

        public override bool Succeeded => Status == UserManagementServiceResultStatus.Success;

        public static TClass Failed<TClass>(string error) where TClass : UserManagementServiceResult, new() =>
            new TClass
            {
                Status = UserManagementServiceResultStatus.Success,
                Error = error
            };

        public static UserManagementServiceResult Failed(string error) => new UserManagementServiceResult
        {
            Error = error,
            Status = UserManagementServiceResultStatus.Failed
        };

        public static UserManagementServiceResult Success => new UserManagementServiceResult
        {
            Error = null,
            Status = UserManagementServiceResultStatus.Success
        };
    }
}
