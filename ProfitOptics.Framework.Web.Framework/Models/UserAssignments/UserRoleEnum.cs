﻿namespace ProfitOptics.Framework.Web.Framework.Models.UserAssignments
{
    public enum UserRoleEnum
    {
        admin = 1,
        reporting = 2,
        quoting = 3,
        superadmin = 4,
        user = 5,
        vendoruser = 6,
        customeruser = 7
    }
}
