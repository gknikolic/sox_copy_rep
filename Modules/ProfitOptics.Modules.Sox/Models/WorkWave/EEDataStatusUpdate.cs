﻿using Newtonsoft.Json;
using ProfitOptics.Modules.Sox.Models.WorkWave;

namespace ProfitOptics.Modules.Sox.Models
{
	public class EEDataStatusUpdate
	{
		[JsonProperty("sec")]
		public int Sec { get; set; }
	
		public RouteStepStatusType? StepStatus { get; set; }
		[JsonProperty("status")]
		public string Status { get { return StepStatus.ToString(); } }
	}


}
