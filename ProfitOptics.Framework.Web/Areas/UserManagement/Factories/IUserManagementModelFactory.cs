﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Areas.UserManagement.Models;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Models.Common;
using ProfitOptics.Framework.Web.Framework.Models;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Factories
{
    public interface IUserManagementModelFactory
    {
        /// <summary>
        /// Creates list of all users
        /// </summary>
        /// <param name="dataTablesRequest"></param>
        /// <returns></returns>
        PagedList<ApplicationUserListModel> CreateUserModels(IDataTablesRequest dataTablesRequest);

        /// <summary>
        /// Prepares a ApplicationUserModel
        /// </summary>
        /// <param name="userId">Id of an user</param>
        /// <returns></returns>
        Task<ApplicationUserModel> PrepareApplicationUserModelAsync(int userId);

        /// <summary>
        /// Prepares a DropdownModel for ApplicationUserModel
        /// </summary>
        /// <returns></returns>
        IEnumerable<SelectListItem> PrepareUserRolesSelectListItems();

        ParentCompanyModel PrepareParentCompanyModel(int parentCompanyId);
        ChildCompanyModel PrepareChildCompanyModel(int childCompanyId);
    }
}
