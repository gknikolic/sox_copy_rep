﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.ManualEtl.Data;
using ProfitOptics.Modules.ManualEtl.Factories;
using ProfitOptics.Modules.ManualEtl.Services;

namespace ProfitOptics.Modules.ManualEtl.Infrastructure
{
    public sealed class ManualEtlServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<IManualEtlService, ManualEtlService>();
            services.AddScoped<IManualEtlFactory, ManualEtlFactory>();
        }
    }
}
