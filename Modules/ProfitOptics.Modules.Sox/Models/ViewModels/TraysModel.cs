﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class TraysModel
    {
        public List<TrayModel> Trays { get; set; }
    }
}
