﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ProfitOptics.Framework.Core.Settings;

namespace ProfitOptics.Framework.Core.Data
{
    public class Printer : IPrinter
    {
        private readonly PdfPrinter _settings;

        public Printer(PdfPrinter settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Converts xls,pptx or docx file into pdf.
        /// </summary>
        /// <param name="inputfile">MemoryStream of the file that will be converted.</param>
        /// <param name="outputFileName">Name of the converted file.</param>
        /// <param name="fileExtension">File extension of the file that is converted. It can be xls, pptx, docx.</param>
        /// <returns></returns>
        public async Task<MemoryStream> ConvertToPDF(MemoryStream inputfile, string outputFileName, string fileExtension)
        {
            if (fileExtension == "xls" || fileExtension == "pptx" || fileExtension == "docx")
            {
                using (var client = new HttpClient())
                {
                    var base64String = Convert.ToBase64String(inputfile.ToArray());
                    using (var request = new HttpRequestMessage(HttpMethod.Post, _settings.Url))
                    {
                        request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        request.Headers.Add("AppCode", _settings.AppCode);
                        request.Headers.Add("AppKey", _settings.AppKey);
                        var jsonString = JsonConvert.SerializeObject(new
                        {
                            InputType = fileExtension,
                            OutputType = "pdf",
                            Content = base64String
                        });

                        request.Content = new StringContent(jsonString, Encoding.UTF8, "application/json");
                        using (var response = await client.SendAsync(request))
                        {
                            var responseContent = await response.Content.ReadAsStringAsync();
                            if (response.StatusCode != HttpStatusCode.OK)
                                throw new Exception(string.Format("Failed to convert {0} to pdf.", outputFileName));
                            var reponseObject = await response.Content.ReadAsAsync<PrinterResponse>();
                            var reponseObject2 = response.Content.ReadAsStringAsync().Result;
                            return new MemoryStream(Convert.FromBase64String(reponseObject.Content));
                        }
                    }
                }
            }
            else throw new Exception(fileExtension + "is incompatible file extension.");
        }
    }

    public class PrinterResponse
    {
        public string Content { get; set; }
    }

    public static class HttpContentExtensions
    {
        public static async Task<T> ReadAsAsync<T>(this HttpContent content)
        {
            string json = await content.ReadAsStringAsync();
            T value = JsonConvert.DeserializeObject<T>(json);
            return value;
        }
    }

}
