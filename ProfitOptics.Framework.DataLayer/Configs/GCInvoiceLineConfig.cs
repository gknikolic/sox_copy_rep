﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCInvoiceLineConfig : IEntityTypeConfiguration<GCInvoiceLine>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCInvoiceLine> builder)
        {
            builder.ToTable("GCInvoiceLine");
            builder.HasKey(entity => entity.Id);
        }
    }
}
