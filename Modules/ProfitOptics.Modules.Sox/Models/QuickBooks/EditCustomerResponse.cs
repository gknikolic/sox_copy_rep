﻿namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class EditCustomerResponse : EditBase
    {
        public Customer Customer { get; set; }
    }
}
