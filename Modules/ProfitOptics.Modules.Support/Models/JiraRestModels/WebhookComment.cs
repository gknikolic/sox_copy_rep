﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class WebhookComment
    {
        public string id { get; set; }

        public IssueAssignee author { get; set; }

        public string body { get; set; }

        public string updated { get; set; }
    }
}