﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.Web.Areas.AuditLog.Data;
using ProfitOptics.Framework.Web.Areas.AuditLog.Data.Domain;
using ProfitOptics.Framework.Web.Areas.AuditLog.Models;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ProfitOptics.Framework.Web.Areas.AuditLog.Services
{
    public class AuditLogService : IAuditLogService
    {
        #region Fields

        private readonly Entities _context;

        #endregion

        #region Ctor

        public AuditLogService(Entities context)
        {
            _context = context;
        }

        #endregion

        #region Methods

        public DataTablesResponse<AuditLogData> GetDataTablesData(IDataTablesRequest request, string username, DateTime? from, DateTime? to, bool showAll)
        {
            var records = GetRawData(_context).Where(q => q.Inserted >= from && q.Inserted <= to);
            if (!string.IsNullOrEmpty(username))
                records = records.Where(q => q.Username.Contains(username));

            string search = request.Search.Value.ToLower();

            // sort
            records = records.ApplySort(request);
            var recordList = records.ToList();

            //friendly names

            PopulateFriendlyNames(_context, recordList);
            recordList = Filter(recordList, search);
            recordList = recordList.Where(item => showAll || item.FriendlyName != null).AsQueryable().ApplySort(request).ToList();

            // page
            var paged = recordList.Skip(request.Start).Take(request.Length).ToList();

            // return data table response
            var totalCount = recordList.Count();


            return new DataTablesResponse<AuditLogData>(request != null ? request.Draw : 0, paged, totalCount, totalCount);

        }

        public List<AuditLogData> GetExcelDataAudit(string username, DateTime? from, DateTime? to)
        {
            var records = GetRawData(_context).Where(q => q.Inserted >= from && q.Inserted <= to);

            if (!string.IsNullOrEmpty(username))
            {
                records = records.Where(q => q.Username == username);
            }

            var result = records.ToList();

            PopulateFriendlyNames(_context, result);

            return result;
        }

        public void UpdateAuditLogFriendlyNameData()
        {
            var dbData = _context.POActionAuditFriendlyNames.ToList();
            var methods = Assembly.GetExecutingAssembly().GetTypes()
                .Where(type => typeof(Controller).IsAssignableFrom(type))
                .SelectMany(type => type.GetMethods().Where(x => x.DeclaringType == type))
                .Where(method => method.IsPublic && !method.IsDefined(typeof(NonActionAttribute)))
                .GroupBy(item => new { x = item.Name, y = item.ReflectedType.ToString() })
                .Select(item => new AuditLogFriendlyNameData()
                {
                    ReflectedActionName = item.Key.x,
                    ReflectedType = item.Key.y
                })
                .Where(item => item.ReflectedType != "ProfitOptics.Framework.Web.Areas.BaseController").ToList();

            foreach (var dbmethod in dbData)
            {
                methods.ForEach(item =>
                {
                    if (item.ReflectedActionName == dbmethod.ReflectedActionName && item.ReflectedType == dbmethod.ReflectedType)
                    {
                        item.FriendlyName = dbmethod.FriendlyName;
                    }
                });
            }

            _context.Database.ExecuteSqlRaw("DELETE FROM POActionAuditFriendlyName");
            _context.POActionAuditFriendlyNames.AddRange(methods.Select(item => new POActionAuditFriendlyName
            {
                FriendlyName = item.FriendlyName,
                ReflectedActionName = item.ReflectedActionName,
                ReflectedType = item.ReflectedType
            }));
            _context.SaveChanges();

        }

        public DataTablesResponse<AuditLogFriendlyNameData> GetAuditLogFriendlyNameData(IDataTablesRequest request)
        {
            var records = _context.POActionAuditFriendlyNames.Select(item => new AuditLogFriendlyNameData
            {
                FriendlyName = item.FriendlyName,
                ReflectedActionName = item.ReflectedActionName,
                ReflectedType = item.ReflectedType
            }).AsQueryable();

            // sort
            records = records.OrderBy(item => item.ReflectedType).ThenBy(item => item.ReflectedActionName).ApplySort(request);

            // page
            var paged = records.AsQueryable().ApplyPagination(request).ToList();
            var totalCount = records.Count();

            return new DataTablesResponse<AuditLogFriendlyNameData>(request?.Draw ?? 0, paged, totalCount, totalCount);
        }

        public void SaveFriendlyName(AuditLogFriendlyNameData model)
        {
            var dbModel = _context.POActionAuditFriendlyNames.FirstOrDefault(x => x.ReflectedActionName == model.ReflectedActionName && x.ReflectedType == model.ReflectedType);

            if (dbModel == null)
            {
                dbModel = new POActionAuditFriendlyName();
                _context.POActionAuditFriendlyNames.Add(dbModel);
            }

            dbModel.ReflectedActionName = model.ReflectedActionName;
            dbModel.ReflectedType = model.ReflectedType;
            dbModel.FriendlyName = model.FriendlyName;

            _context.SaveChanges();
        }

        private List<AuditLogData> Filter(List<AuditLogData> records, string search)
        {
            // filter
            if (search != null && !string.IsNullOrEmpty(search))
            {
                records = records.Where(item => (!string.IsNullOrEmpty(item.Action) && item.Action.ToLower().Contains(search)) ||
                                                (!string.IsNullOrEmpty(item.Area) && item.Area.ToLower().Contains(search)) ||
                                                (item.Browser.ToLower().Contains(search)) ||
                                                (!string.IsNullOrEmpty(item.Controller) && item.Controller.ToLower().Contains(search)) ||
                                                (item.Username.ToLower().Contains(search)) ||
                                                (!string.IsNullOrEmpty(item.FriendlyName) && item.FriendlyName.ToLower().Contains(search))).ToList();
            }

            return records;
        }

        private IQueryable<AuditLogData> GetRawData(Entities data)
        {
            var results = from audit in data.POActionAudits
                          select new AuditLogData
                          {
                              Username = audit.Username,
                              Inserted = audit.CreateTime,
                              IsSuccess = true,
                              Url = audit.Url,
                              Action = audit.Action,
                              Area = audit.Area,
                              Controller = audit.Controller,
                              IpAddress = audit.IpAddress,
                              Browser = audit.Browser,
                              StatusValue = audit.Status,
                              ReflectedType = audit.ReflectedType,
                              ReflectedActionName = audit.ReflectedActionName
                          };

            return results;
        }

        private static void PopulateFriendlyNames(Entities data, IEnumerable<AuditLogData> records)
        {
            var fNames = data.POActionAuditFriendlyNames.ToList();
            foreach (var item in records)
            {
                var fname = fNames.FirstOrDefault(x => x.ReflectedActionName == item.ReflectedActionName && x.ReflectedType == item.ReflectedType);
                item.FriendlyName = fname?.FriendlyName;
            }
        }

        #endregion
    }
}