﻿using System;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    public interface IMemoryCacheHelper
    {
        T Get<T>(string key, Func<T> acquire, int cacheTimeInMinutes);
        void Set<T>(string key, T data, int cacheTimeInMinutes);
    }
}