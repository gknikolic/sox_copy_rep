﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Framework.Helpers
{    
    public class StringResourcesRepository : IStringResourceRepository
    {
        private IConfigurationRoot _configuration;
        public StringResourcesRepository()
        {
            var configurationBuilder = new ConfigurationBuilder()
                .AddJsonFile("localResources.json");
            this._configuration = configurationBuilder.Build();            
        }        

        public string GetResource(string key)
        {
            var sections = this.SplitKeyIntoSectionNames(key);
            var configuarationSubset = this.GetConfigurationSubset(sections);
            var value = configuarationSubset.GetValue<string>(sections.Last());
            return value != null ? value : string.Empty;
        }

        private string[] SplitKeyIntoSectionNames(string key)
        {
            return key.Split('.');
        }

        private IConfiguration GetConfigurationSubset(string[] sections)
        {
            IConfiguration currentSubset = _configuration;
            for(var i=0; i < sections.Length - 1; i++)
            {
                currentSubset = currentSubset.GetSection(sections[i]);
            }
            return currentSubset;
        }

    }
}
