﻿using System;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVariancePeriodsModel
    {
        public TrendVariancePeriodsModel(ComparisonType comparisonType, DateTime? startDate = null, DateTime? endDate = null)
        {
            DateTime now = DateTime.Now;

            switch (comparisonType)
            {
                case ComparisonType.MonthToDateYearOverYear:
                    InitializeMonthToDateYearOverYearPeriod(now);
                    break;
                case ComparisonType.QuarterToDateYearOverYear:
                    InitializeQuarterToDateYearOverYearPeriod(now);
                    break;
                case ComparisonType.YearToDateYearOverYear:
                    InitializeYearToDateYearOverYearPeriod(now);
                    break;
                case ComparisonType.Last12Months:
                    InitializeLast12MonthsPeriod(now);
                    break;
                case ComparisonType.CustomDateRangeYearOverYear:
                    InitializeCustomDateRangeYearOverYearPeriod(now, startDate, endDate);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(comparisonType), comparisonType, null);
            }
        }

        public DateTime CurrentPeriodStartDate { get; private set; }

        public DateTime CurrentPeriodEndDate { get; private set; }

        public DateTime PreviousPeriodStartDate => CurrentPeriodStartDate.AddYears(-1);

        public DateTime PreviousPeriodEndDate => CurrentPeriodEndDate.AddYears(-1);
        
        private void InitializeMonthToDateYearOverYearPeriod(DateTime now)
        {
            CurrentPeriodStartDate = new DateTime(now.Year, now.Month, 1);
            CurrentPeriodEndDate = now;
        }

        private void InitializeQuarterToDateYearOverYearPeriod(DateTime now)
        {
            int quarterStartMonth = (now.Month - 1) / 3 * 3 + 1;

            CurrentPeriodStartDate = new DateTime(now.Year, quarterStartMonth, 1);
            CurrentPeriodEndDate = CurrentPeriodStartDate.AddMonths(3).AddDays(-1);
        }

        private void InitializeYearToDateYearOverYearPeriod(DateTime now)
        {
            CurrentPeriodStartDate = new DateTime(now.Year, 1, 1);
            CurrentPeriodEndDate = now;
        }

        private void InitializeLast12MonthsPeriod(DateTime now)
        {
            CurrentPeriodStartDate = now.AddYears(-1);
            CurrentPeriodEndDate = now;
        }

        private void InitializeCustomDateRangeYearOverYearPeriod(DateTime now, DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null || endDate == null || startDate > endDate)
            {
                InitializeMonthToDateYearOverYearPeriod(now);

                return;
            }

            CurrentPeriodStartDate = startDate.Value;
            CurrentPeriodEndDate = endDate.Value;
        }
    }
}
