﻿namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayPart
	{
		public int RequiredCount { get; set; }
		public int ActualCount { get; set; }
		public string ModelNumber { get; set; }
		public string Vendor { get; set; }
		public string Description { get; set; }
		public string Placement { get; set; }
		public string SetName { get; set; }
	}
}
