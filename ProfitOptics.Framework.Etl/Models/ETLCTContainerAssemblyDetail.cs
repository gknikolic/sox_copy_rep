﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Models
{
    public class ETLCTContainerAssemblyDetail
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("parentId")]
        public long ParentId { get; set; }

        [JsonProperty("updateTimestamp")]
        public DateTimeOffset UpdateTimestamp { get; set; }

        [JsonProperty("vendorName")]
        public string VendorName { get; set; }

        [JsonProperty("censisSetHistoryId")]
        public Guid? CensisSetHistoryId { get; set; }
    }

    public class ETLCTAssemblyGraphic
	{
        [JsonProperty("containerTypeId")]
        public long ContainerTypeId { get; set; }

        [JsonProperty("censisSetHistoryId")]
        public Guid CensisSetHistoryId { get; set; }

        [JsonProperty("graphicId")]
        public long GraphicId { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }

    public class ETLCTCurrentAssemblyGraphics
	{
        [JsonProperty("currentAssemblyGraphics")]
        public List<ETLCTAssemblyGraphic> AssemblyGraphics { get; set; }
	}
}
