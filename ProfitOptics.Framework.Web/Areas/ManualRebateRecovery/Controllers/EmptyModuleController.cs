﻿using Microsoft.AspNetCore.Mvc;

namespace ProfitOptics.Framework.Web.Areas.ManualRebateRecovery.Controllers
{
    [Area("ManualRebateRecovery")]
    public class ManualRebateRecoveryController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}