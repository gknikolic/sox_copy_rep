﻿using System;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class Note
	{
        public int Id { get; set; }
        public string TrayTitle { get; set; }
		public string Color { get; set; }
		public int TrayId { get; set; }
		public int ColorStatusId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public DateTime Timestamp { get; set; }
		public DateTime TimestampLocal { get { return ProfitOptics.Modules.Sox.Helpers.Helpers.GetLocalDateTime(Timestamp); } }
		public bool RequiresResolution { get; set; }
		public bool IsSystemGenerated { get; set; }
		public bool IsColorStatusLocked { get; set; }
		public DateTime? ResolvedAtUtc { get; set; }
        public string CreatedBy { get; set; }
    }
}
