﻿namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVarianceItemCategoryDrillThroughChartModel
    {
        public int YearMonth { get; set; }

        public decimal MonthlySales { get; set; }

        public decimal MonthlyGrossProfit { get; set; }

        public decimal MonthlyGrossProfitPercent => MonthlySales == decimal.Zero
            ? decimal.Zero
            : (MonthlySales - MonthlyGrossProfit) / MonthlySales;
    }
}
