﻿namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class LocationHierarchyRecord
    {
        public string LocationBusinessUnitNew { get; set; }

        public string LocationSuperDivision { get; set; }

        public string LocationSubRegionNameNew { get; set; }

        public string LocationAreaName { get; set; }

        public string LocationName { get; set; }
    }
}