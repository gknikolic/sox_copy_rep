﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class ManageGCCaseLifecycleStatuses
    {
        public IEnumerable<SelectListItem> GCCases { get; set; }
        public IEnumerable<SelectListItem> GCLifecycleStatuses { get; set; }

        public int SelectedGCCase { get; set; }
        public int SelectedGCLifecycleStatus { get; set; }
		public string User { get; set; }
        public DateTime? Timestamp { get; set; }
    }
}
