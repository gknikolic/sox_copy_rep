﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;
using ProfitOptics.Modules.Quotes.Data;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Helpers
{
    public class QuoteUserAssignment : IUserAssignment
    {
        private readonly Entities _entities;

        //public QuoteUserAssignment(IServiceProvider serviceProvider)
        //{                           
        //    _entities = ((Entities)serviceProvider.GetService(typeof(Entities)));
        //}

        public QuoteUserAssignment(Entities entities)
        {
            _entities = entities;
        }

        public void Clear(int userId)
        {
            var record = _entities.QTUserMappings.Where(item => item.UserId == userId).FirstOrDefault();
            if (record != null)
            {
                _entities.QTUserMappings.Remove(record);
                _entities.SaveChanges();
            }
        }

        public List<SelectListItem> GetAllRegions()
        {
            return _entities.QTHierarchyRegions.Select(item => new SelectListItem
            {
                Text = item.Name,
                Value = item.Id.ToString()
            }).ToList();
        }

        public List<SelectListItem> GetAllSalesReps()
        {
            return _entities.QTHierarchySalesReps.Select(item => new SelectListItem
            {
                Text = item.Name,
                Value = item.Id.ToString()
            }).ToList();
        }

        public List<SelectListItem> GetAllZones()
        {
            return _entities.QTHierarchyZones.Select(item => new SelectListItem
            {
                Text = item.Name,
                Value = item.Id.ToString()
            }).ToList();
        }

        public string GetRoleName()
        {
            return "quoting";
        }

        public AreaUserMapping GetUserMapping(int userId)
        {
            if (_entities.QTUserMappings.Where(item => item.UserId == userId).FirstOrDefault() != null)
            {
                return _entities.QTUserMappings.Where(item => item.UserId == userId).Select(item => new AreaUserMapping
                {
                    UserId = userId,
                    RegionId = item.RegionId,
                    ZoneId = item.ZoneId,
                    SalesRepId = item.SalesRepId
                }).FirstOrDefault();
            }
            else return null;
        }

        public RoleAssignmentModel MatchByName(RoleHierarchyModel roleHierarchy)
        {
            var result = new RoleAssignmentModel();

            if (!String.IsNullOrEmpty(roleHierarchy.ZoneName))
            {
                var zone = _entities.QTHierarchyZones.Where(item => item.Name == roleHierarchy.ZoneName).FirstOrDefault();
                if (zone != null)
                    result.SelectedZone = zone.Id;
                else return null;
            }
            if (!String.IsNullOrEmpty(roleHierarchy.RegionName))
            {
                var region = _entities.QTHierarchyRegions.Where(item => item.Name == roleHierarchy.RegionName).FirstOrDefault();
                if (region != null)
                    result.SelectedRegion = region.Id;
                else return null;
            }
            if (!String.IsNullOrEmpty(roleHierarchy.SalesRepName))
            {
                var salesRep = _entities.QTHierarchySalesReps.Where(item => item.Name == roleHierarchy.SalesRepName).FirstOrDefault();
                if (salesRep != null)
                    result.SelectedSalesRep = salesRep.Id;
                else return null;
            }
            result.Name = GetRoleName();

            return result;
        }

        public void Save(int userId, int? zoneId, int? regionId, int? salesRepId)
        {
            var userMapping = _entities.QTUserMappings.Where(item => item.UserId == userId).FirstOrDefault();
            if (userMapping == null)
            {
                var record = new QTUserMapping
                {
                    UserId = userId,
                    RegionId = regionId,
                    ZoneId = zoneId,
                    SalesRepId = salesRepId
                };
                _entities.QTUserMappings.Add(record);
                _entities.SaveChanges();
                return;
            }
            userMapping.RegionId = regionId;
            userMapping.ZoneId = zoneId;
            userMapping.SalesRepId = salesRepId;
            _entities.SaveChanges();
        }
    }
}