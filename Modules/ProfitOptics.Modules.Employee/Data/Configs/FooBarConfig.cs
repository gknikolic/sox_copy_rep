﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Employee.Data.Domain;

namespace ProfitOptics.Modules.Employee.Data.Configs
{
    //This is an example data set configuration see more here https://www.entityframeworktutorial.net/efcore/fluent-api-in-entity-framework-core.aspx
    public class FooBarConfig : IEntityTypeConfiguration<FooBar>
    {
        public void Configure(EntityTypeBuilder<FooBar> builder)
        {
            builder.ToTable("FooBar");
            builder.HasKey(u => u.Id);

            builder.Property(u => u.Id);
            builder.Property(u => u.Foo);
            builder.Property(u => u.Bar);
        }
    }
}
