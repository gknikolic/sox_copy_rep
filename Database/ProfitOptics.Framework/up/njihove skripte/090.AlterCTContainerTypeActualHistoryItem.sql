USE [Sox]

ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem] ADD CTLocationId INT NULL;

ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem]  WITH CHECK ADD  CONSTRAINT [FK_CTContainerTypeActualHistoryItem_CTLocation] FOREIGN KEY([CTLocationId])
REFERENCES [dbo].[CTLocation] ([Id])
GO
ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem] CHECK CONSTRAINT [FK_CTContainerTypeActualHistoryItem_CTLocation]
GO
