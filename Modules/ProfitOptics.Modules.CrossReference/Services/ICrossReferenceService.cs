﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Modules.CrossReference.Data.Domain;
using ProfitOptics.Modules.CrossReference.Models;

namespace ProfitOptics.Modules.CrossReference.Services
{
    public interface ICrossReferenceService
    {
        #region Compatibility Option

        IEnumerable<CrtCompatibilityOption> GetCompatibilityOptions(bool getDeleted = false);

        int CreateCompatibilityOption(string compatibilityName);

        void DeleteCompatibilityOption(int compatibilityId);

        void UpdateCompatibilityOption(int id, string compatibilityOptionName);

        #endregion Compatibility Option

        #region Comeptitor

        void DeleteCompetitor(int id);

        void UpdateCompetitor(int id, string competitorName);

        int CreateCompetitor(string competitorName);

        IEnumerable<CrtCompetitor> GetCompetitors(bool getDeleted = false);

        #endregion Comeptitor

        #region Cross Reference Export

        IEnumerable<CrtCrossReferenceRecord> GetCrossReferenceExportModelList(List<int?> crossReferenceIds);

        #endregion Cross Reference Export

        #region QTItem

        IEnumerable<SelectListItem> GetItemListForDropdown(int? itemId);

        #endregion QTItem

        #region Cross Reference Record

        IEnumerable<CrtCrossReferenceRecord> GetCrossReferenceRecords(int? competitorId, int? compatibilityId, string competitorItem, int userId);

        IEnumerable<CrtCrossReferenceRecord> GetCrossReferenceRecordsForApproval(int userId);

        int GetThumbsUpCountForCrRecord(int recordId);

        int GetThumbsDownCountForCrRecord(int recordId);

        void RemoveCrossReferenceRecordForApproval(int id);

        void ApproveCrossReferenceRecord(int id);

        void ThumbsUpClick(int crossReferenceId, int userId);

        void ThumbsDownClick(int crossReferenceId, int userId);

        void EditCrossReferenceRecordForApproval(CrossReferenceRecordModel crossReferenceRecord);

        int AddCrossReferenceRecord(CrossReferenceRecordModel crossReferenceRecord, int userId);

        IEnumerable<QTItem> GetQtItemsForAutoComplete(string term);

        bool CheckCompetitorMedcoItemCombinationOnAdd(int competitorId, String competitorItemNumber, int medcoItemNumberId);

        bool CheckCompetitorMedcoItemCombinationOnEdit(int id, int competitorId, String competitorItemNumber, int medcoItemNumberId);

        #endregion Cross Reference Record
    }
}