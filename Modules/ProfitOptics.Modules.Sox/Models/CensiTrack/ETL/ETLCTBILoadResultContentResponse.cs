﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTBILoadResultContentResponse
    {
        [JsonProperty("loadHeader")]
        public ETLCTBILoadResult LoadHeader { get; set; }

        [JsonProperty("loadItems")]
        public List<ETLCTBILoadResultContent> LoadItems { get; set; }
    }
}
