USE [Sox]

BEGIN

  CREATE TABLE [dbo].[GCVendor]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50)
  )
  
  ALTER TABLE [Sox].[dbo].[GCCase]
  ADD [GCVendorId] int,
  FOREIGN KEY ([GCVendorId]) REFERENCES [dbo].[GCVendor] ([Id]);
  
  ALTER TABLE [Sox].[dbo].[GCChildCompany]
  ADD [GCVendorId] int,
      [GCCustomerId] int, 
  FOREIGN KEY ([GCVendorId]) REFERENCES [dbo].[GCVendor] ([Id]),
  FOREIGN KEY ([GCCustomerId]) REFERENCES [dbo].[GCCustomer] ([Id]);

END