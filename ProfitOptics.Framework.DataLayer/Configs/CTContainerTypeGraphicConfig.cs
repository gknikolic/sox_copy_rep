﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerTypeGraphicConfig : IEntityTypeConfiguration<CTContainerTypeGraphic>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTContainerTypeGraphic> builder)
        {
            builder.ToTable("CTContainerTypeGraphic");
            builder.HasKey(entity => entity.Id);
        }
    }
}
