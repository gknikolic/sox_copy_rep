﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class CurrencyRef
    {
        public string Value { get; set; }
        public string Name { get; set; }
    }
}
