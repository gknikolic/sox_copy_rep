﻿using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Reporting.Models.CustomerReport;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Factories.CustomerReport
{
    public interface ICustomerReportFactory
    {
        /// <summary>
        /// Gets the view model for the Customer Report page
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isShipTo"></param>
        /// <returns></returns>
        CustomerReportViewModel GetCustomerReportViewModel(int userId, bool isShipTo = false);

        /// <summary>
        /// Gets Data for the Top Customers Data Table 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="filter"></param>
        /// <param name="isShipTo"></param>
        /// <returns></returns>
        PagedList<CustomerReportModel> GetTopCustomersReportModels(IDataTablesRequest request,
            ReportFilterModel filter, bool isShipTo);

        /// <summary>
        /// Gets Data for the Bottom Customers Data Table
        /// </summary>
        /// <param name="request"></param>
        /// <param name="filter"></param>
        /// <param name="isShipTo"></param>
        /// <returns></returns>
        PagedList<CustomerReportModel> GetBottomCustomersReportModels(IDataTablesRequest request,
            ReportFilterModel filter, bool isShipTo);
    }
}