﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTProduct
    {
        public int Id { get; set; }
        public decimal LegacyId { get; set; }
        public string VendorName { get; set; }
        public string VendorProductName { get; set; }
        public string ModelNumber { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual List<CTContainerItem> CTContainerItems { get; set; }
    }
}
