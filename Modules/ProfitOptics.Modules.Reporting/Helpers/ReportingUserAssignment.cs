﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Helpers
{
    public class ReportingUserAssignment : IUserAssignment
    {
        private readonly Entities _context;

        public ReportingUserAssignment(Entities context)
        {
            _context = context;
        }

        public void Clear(int userId)
        {
            var record = _context.ReportUserMapping.FirstOrDefault(item => item.UserId == userId);
            if (record != null)
            {
                _context.ReportUserMapping.Remove(record);
                _context.SaveChanges();
            }
        }

        public List<SelectListItem> GetAllRegions()
        {
            return _context.ReportHierarchyRegion.Select(item => new SelectListItem
            {
                Text = item.Name,
                Value = item.Id.ToString()
            }).ToList();
        }

        public List<SelectListItem> GetAllSalesReps()
        {
            return _context.ReportHierarchySalesRep.Select(item => new SelectListItem
            {
                Text = item.Name,
                Value = item.Id.ToString()
            }).ToList();
        }

        public List<SelectListItem> GetAllZones()
        {
            return _context.ReportHierarchyZone.Select(item => new SelectListItem
            {
                Text = item.Name,
                Value = item.Id.ToString()
            }).ToList();
        }

        public string GetRoleName()
        {
            return "reporting";
        }


        public AreaUserMapping GetUserMapping(int userId)
        {
            if (_context.ReportUserMapping.FirstOrDefault(item => item.UserId == userId) != null)
            {
                return _context.ReportUserMapping.Where(item => item.UserId == userId).Select(item => new AreaUserMapping
                {
                    UserId = userId,
                    RegionId = item.RegionId,
                    ZoneId = item.ZoneId,
                    SalesRepId = item.SalesRepId
                }).FirstOrDefault();
            }

            return null;
        }

        public RoleAssignmentModel MatchByName(RoleHierarchyModel roleHierarchy)
        {

            var result = new RoleAssignmentModel();
            if (!string.IsNullOrEmpty(roleHierarchy.ZoneName))
            {
                var zone = _context.ReportHierarchyZone.FirstOrDefault(item => item.Name == roleHierarchy.ZoneName);
                if (zone != null)
                    result.SelectedZone = zone.Id;
                else return null;
            }
            if (!string.IsNullOrEmpty(roleHierarchy.RegionName))
            {
                var region = _context.ReportHierarchyRegion.FirstOrDefault(item => item.Name == roleHierarchy.RegionName);
                if (region != null)
                    result.SelectedRegion = region.Id;
                else return null;
            }
            if (!string.IsNullOrEmpty(roleHierarchy.SalesRepName))
            {
                var salesRep = _context.ReportHierarchySalesRep.FirstOrDefault(item => item.Name == roleHierarchy.SalesRepName);
                if (salesRep != null)
                    result.SelectedSalesRep = salesRep.Id;
                else return null;
            }

            result.Name = GetRoleName();
            return result;
        }

        public void Save(int userId, int? zoneId, int? regionId, int? salesRepId)
        {
            var userMapping = _context.ReportUserMapping.FirstOrDefault(item => item.UserId == userId);
            if (userMapping == null)
            {
                var record = new ReportUserMapping
                {
                    UserId = userId,
                    RegionId = regionId,
                    ZoneId = zoneId,
                    SalesRepId = salesRepId
                };
                _context.ReportUserMapping.Add(record);
                _context.SaveChanges();
                return;
            }
            userMapping.RegionId = regionId;
            userMapping.ZoneId = zoneId;
            userMapping.SalesRepId = salesRepId;
            _context.SaveChanges();
        }
    }
}