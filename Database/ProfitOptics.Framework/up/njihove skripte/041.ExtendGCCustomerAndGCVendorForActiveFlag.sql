USE [Sox]

BEGIN

  ALTER TABLE [Sox].[dbo].[GCCustomer]
  ADD [Active] bit;
  
  ALTER TABLE [Sox].[dbo].[GCVendor]
  ADD [Active] bit;     

END