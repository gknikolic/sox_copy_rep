﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerService
    {
        public int Id { get; set; }
        public int? GCProcedureTypeId { get; set; }
        public string ICD9 { get; set; }
        public string ICD10 { get; set; }

        public int LegacyId { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual GCProcedureType GCProcedureType { get; set; }
    }
}
