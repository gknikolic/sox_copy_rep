﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class BoardColumn
    {
        public string Name { get; set; }
        public List<IssueStatus> Statuses { get; set; }
    }
}