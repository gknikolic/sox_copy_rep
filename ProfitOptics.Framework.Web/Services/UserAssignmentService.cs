﻿using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Framework.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;
using ProfitOptics.Modules.Sox.Services;

namespace ProfitOptics.Framework.Web.Services
{
    public class UserAssignmentService : IUserAssignmentService
    {
        
        private readonly IServiceProvider _serviceProvider;
        private List<IUserAssignment> userAssignments;
        private readonly IUserService _userService;
        private readonly ISoxService _soxService;

        public UserAssignmentService(IServiceProvider serviceProvider,
                                     IUserService userService,
                                     ISoxService soxService)
        {
            this._serviceProvider = serviceProvider;
            this._userService = userService;
            this._soxService = soxService;
            ResolveUserAssignments();
        }

        private void ResolveUserAssignments()
        {
            userAssignments = _serviceProvider.GetServices<IUserAssignment>().ToList();
        }

        public async Task<List<RoleAssignmentModel>> GetRoleAssignmentsByUserIdAsync(int userId)
        {
            var applicationUser = await _userService.FindUserByIdAsync(userId);
            var userRoles = await _userService.GetUserRolesAsync(applicationUser);
            return await GetRoleAssignmentsByUser(applicationUser, userRoles.Select(x => x.Name).ToList());
        }

        public void SaveRoleAssignments(ApplicationUser user, ApplicationUserModel model)
        {
            foreach (var role in model.Roles)
            {
                var userAssignment = userAssignments.FirstOrDefault(x => x.GetRoleName() == role.Name);
                userAssignment?.Save(user.Id, role.SelectedZone, role.SelectedRegion, role.SelectedSalesRep);
            }
        }

        public async Task<List<RoleAssignmentModel>> GetRoleAssignmentsByUser(ApplicationUser user, List<string> userRoles)
        {
            List<RoleAssignmentModel> result = new List<RoleAssignmentModel>();

            var roles = await _userService.GetRolesAsync();
                
            foreach (var role in roles)
            {   
                var roleAssignmentToAdd = new RoleAssignmentModel
                {
                    Id = role.Id,
                    Name = role.Name,
                    IsUserInRole = userRoles.Any(x => x == role.Name),
                };

                IUserAssignment userAssignment = userAssignments.FirstOrDefault(x => x.GetRoleName() == role.Name);
                if (userAssignment != null)
                {
                    roleAssignmentToAdd.Zones = userAssignment.GetAllZones();
                    roleAssignmentToAdd.SalesReps = userAssignment.GetAllSalesReps();
                    roleAssignmentToAdd.Regions = userAssignment.GetAllRegions();
                    roleAssignmentToAdd.ImplementsIUserAssignment = true;

                    var userMapping = userAssignment.GetUserMapping(user.Id);
                        
                    if (userMapping != null)
                    {
                        roleAssignmentToAdd.SelectedRegion = userMapping.RegionId;
                        roleAssignmentToAdd.SelectedSalesRep = userMapping.SalesRepId;
                        roleAssignmentToAdd.SelectedZone = userMapping.ZoneId;
                    }
                }
                result.Add(roleAssignmentToAdd);                    
            }
            return result;
        }
    }
}
