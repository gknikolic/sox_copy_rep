﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class GoogleAuthenticatorEnabledResult : AccountManagementServiceResult
    {
        public bool Enabled;

        public static GoogleAuthenticatorEnabledResult Success(bool enabled) => new GoogleAuthenticatorEnabledResult
        {
            Status = AccountManagementServiceResultStatusCode.Success,
            Error = null,
            Enabled = enabled
        };
    }
}
