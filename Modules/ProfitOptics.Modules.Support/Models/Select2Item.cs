﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace ProfitOptics.Modules.Support.Models
{
    public class Select2Item
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }
    }

    public class Select2Items
    {
        [JsonProperty(PropertyName = "results")]
        public IEnumerable<Select2Item> Results { get; set; }
    }

    public static class Select2ItemExtensions
    {
        public static Select2Items AsSelect2List<T, U, V>(this IEnumerable<T> list, Func<T, U> keySelector, Func<T, V> elementSelector)
        {
            Select2Items select2Items = new Select2Items
            {
                Results = list.Select(x => new Select2Item
                {
                    Id = keySelector(x) != null ? keySelector(x).ToString() : string.Empty,
                    Text = elementSelector(x) != null ? elementSelector(x).ToString() : string.Empty
                })
            };

            return select2Items;
        }

        public static Select2Items AsSelect2List<T, U>(this IEnumerable<T> list, Func<T, U> keySelector)
        {
            Select2Items select2Items = new Select2Items
            {
                Results = list.Select(x => new Select2Item
                {
                    Id = keySelector(x) != null ? keySelector(x).ToString() : string.Empty,
                    Text = keySelector(x) != null ? keySelector(x).ToString() : string.Empty
                })
            };

            return select2Items;
        }
    }
}