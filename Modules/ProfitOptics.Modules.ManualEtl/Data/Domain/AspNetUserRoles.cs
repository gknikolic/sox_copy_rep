﻿namespace ProfitOptics.Modules.ManualEtl.Data.Domain
{
    public class AspNetUserRoles
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }

        public virtual AspNetRole Role { get; set; }
        public virtual AspNetUser User { get; set; }
    }
}