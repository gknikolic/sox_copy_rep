﻿using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
