﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Quotes.Data.Domain;
using ProfitOptics.Modules.Quotes.Models;
using ProfitOptics.Modules.Quotes.Models.FreightCalculation;

namespace ProfitOptics.Modules.Quotes.Services
{
    public interface IQuoteService
    {
        IQueryable<ItemModel> GetItemsData(IDataTablesRequest request, int quoteId, string filter);

        int CreateQuote(CreateQuoteModel quote);

        void DeleteQuote(int quoteId);

        void BulkDeleteQuotes(string quoteName);

        void UnarchiveQuote(int quoteId);

        int? CloneQuote(int quoteId, string cloneName, int customerId);

        string GetShipToZip(int shipToId);

        IQueryable<QTBillTo> GetQtBillTos(int userId);
        
        List<SelectListItem> ShipToDropdown(int userId, int quoteId);

        List<SelectListItem> CustomersToDropdown();

        IEnumerable<SelectListItem> GetPricingListForDropdown(int quoteId);

        IQueryable<QTItem> GetQtItems(int quoteId, bool filterByBillToCustomerId = false, bool filterQuoteItems = true, bool filterByEndDate = true);

        IQueryable<QTCustomerPricing> GetCustomerPriceList(string plcCode);

        List<PrintModel.PrintQuoteItems> GetPrintQuoteItemsForQuote(int userId, int quoteId);

        List<ItemModel> GetItemsForQuote(int userId, int quoteId);

        PrintModel.CustomerFacingExtendedModel GetDataForPrint(int userId, int quoteId);

        QuoteDetailsModel GetQuoteDetailsData(int quoteId, int userId);

        object GetQuoteHeader(int userId, int quoteId);

        IQueryable<ItemModel> GetItemsValuesForFreightCalc(int userId, int quoteId);

        List<ItemModel> GetItemsListForQuote(int userId, int quoteId);

        void UpdateLineNumbers(int quoteItemId, int currentLineNumber, int newLineNumber);

        QuoteItemModel GetQuoteItemModel(int userId, int quoteId);

        decimal GetBasePrice(int quoteId, int itemId);

        void AddItemsToQuote(int quoteId, List<int> itemIds);

        void AddItemsFromList(int quoteId, List<string> itemNums);

        List<string> ValidateItemsFromList(int quoteId, List<string> itemNums);

        void AddItemToQuote(int quoteId, string itemNum);

        void RemoveItemFromQuote(int quoteId, int itemId);

        void RemoveItemListFromQuote(int quoteId, List<int> items);

        void UpdateItemQty(int quoteId, int itemId, int units);

        void UpdateQuoteItemPrice(int quoteId, int itemId, decimal newValue);

        void UpdateQuoteItemGmPerc(int quoteId, int itemId, decimal newGpPercValue);

        void UpdateCompetitorPrice(int quoteId, int itemId, decimal newValue);

        void UpdateCompetitorUoM(int quoteId, int itemId, string newValue);

        void UpdateCompetitorCf(int quoteId, int itemId, decimal newValue);

        void RevertPriceToPlcPrice(int quoteId, int itemId);

        void ReplaceItemInQuote(int quoteId, string oldItemNum, string newItemNum);

        List<QuoteItemModel.LookupSelect2Data> GetItemAutocompleteData(int quoteId, string term);

        PagedList<QuoteModel> GetQuoteModelData(int userId, bool showArchived, int urlQuoteId, IDataTablesRequest request);

        IEnumerable<SelectListItem> GetStandingOrderDropdown(int quoteId);

        bool UpdateQuoteDetails(UpdateQuoteDetailsModel quote);

        bool UpdateQuote(UpdateQuoteModel quote);

        QuoteItemModel CalculateFreightCost(QuoteHeaderModel data, int userId);

        QuoteItemModel GetTotAjdAndFloorMarginPct(int userId, int quoteId,
            decimal? freightCost, decimal? freightCapAmount, string freightPaymentType);

        decimal CalculateFreightCost(FreightCalculationModel model);

        decimal CalculateCostForSingleFreightEntry(List<QTUpsRate> upsRateData,
            decimal weightPerShipment, FreightEntryModel freightEntry);

        int GetZipCode(QuoteHeaderModel data);

        decimal GetPhantomWeightAmount(decimal weight);

        List<QTUpsRate> GetUpsRates(int? originZip, int destinationZip);

        decimal? GetUpsAdditionalSurcharge(int destinationZip);

        void UpdateQuoteShippingCost(int quoteId, decimal? freightCost);

        IQueryable<ItemModel> GetStandingOrderQtItems(int quoteId, int orderNum);
    }
}
