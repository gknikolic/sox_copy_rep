﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.Collections.Generic;

namespace ProfitOptics.Modules.DataBrowser.Models
{
    public class DataBrowserPaginationResult
    {
        public long TotalCount { get; set; }
        public long TotalFilteredCount { get; set; }
        public List<object> Data { get; set; }
    }
}