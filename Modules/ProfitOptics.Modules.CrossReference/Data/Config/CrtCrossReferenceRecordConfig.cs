﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.CrossReference.Data.Domain;

namespace ProfitOptics.Modules.CrossReference.Data.Config
{
    public class CrtCrossReferenceRecordConfig : IEntityTypeConfiguration<CrtCrossReferenceRecord>
    {
        public void Configure(EntityTypeBuilder<CrtCrossReferenceRecord> builder)
        {
            builder.ToTable("CrtCrossReferenceRecord");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.CompetitorItemNumber).HasMaxLength(50).IsRequired();
            builder.Property(entity => entity.CompetitorItemDescription).HasMaxLength(250);
            builder.Property(entity => entity.CompetitorWebsiteUrl).HasMaxLength(250);
            builder.Property(entity => entity.Notes).HasMaxLength(500);

            builder.HasOne(entity => entity.AspNetUser)
                .WithMany(p => p.CrtCrossReferenceRecords)
                .HasForeignKey(entity => entity.SubmittedBy);

            builder.HasOne(entity => entity.CrtCompatibilityOption)
               .WithMany(p => p.CrtCrossReferenceRecords)
               .HasForeignKey(entity => entity.CompatibilityOptionId);

            builder.HasOne(entity => entity.CrtCompetitor)
               .WithMany(p => p.CrtCrossReferenceRecords)
               .HasForeignKey(entity => entity.CompetitorId);

            builder.HasOne(entity => entity.QTItem)
               .WithMany(p => p.CrtCrossReferenceRecords)
               .HasForeignKey(entity => entity.ItemId);

        }
    }
}
