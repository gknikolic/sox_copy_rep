﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Linq;
using System.Net.Mail;
using System.Net;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.Core.Email
{
    /// <summary>
    /// Provieds an API for sending emails in both sync & fashion, as well as sending emails with delays.
    /// </summary>
    public interface IEmailer
    {
        /// <summary>
        /// Sends an email asynchronously and inserts a new <see cref="POEmail"/> record to the underlying database.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <see cref="ArgumentNullException"/>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        Task<bool> SendEmailAndWriteToDbAsync(List<string> to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null,
            List<string> cc = null);

        /// <summary>
        /// Sends an email asynchronously and inserts a new <see cref="POEmail"/> record to the underlying database.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <see cref="ArgumentNullException"/>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        Task<bool> SendEmailAndWriteToDbAsync(string to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null,
            List<string> cc = null);

        /// <summary>
        /// Sends an email synchronously and inserts a new <see cref="POEmail"/> record to the underlying database.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <see cref="ArgumentNullException"/>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        bool SendEmailAndWriteToDb(List<string> to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null, List<string> cc = null);

        /// <summary>
        /// Sends an email synchronously and inserts a new <see cref="POEmail"/> record to the underlying database.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <see cref="ArgumentNullException"/>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        bool SendEmailAndWriteToDb(string to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null, List<string> cc = null);


        /// <summary>
        /// Sends an email synchronously.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <see cref="ArgumentNullException"/>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        bool SendEmail(List<string> to, string subject, string body, bool isHtml = false,
            List<EmailAttachment> attachments = null, List<string> cc = null);

        /// <summary>
        /// Sends an email asynchronously.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <see cref="ArgumentNullException"/>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        Task<bool> SendEmailAsync(List<string> to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null, List<string> cc = null);

        /// <summary>
        /// Creates a <see cref="SmtpClient"/> object and configures it.
        /// </summary>
        /// <returns></returns>
        SmtpClient GetSmtpClient();

        /// <summary>
        /// Creates a new <see cref="MailMessage"/> from the specified arguments.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <returns></returns>
        MailMessage GetMailMessage(List<string> to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null, List<string> cc = null);

        /// <summary>
        /// Sends an email aysnchronously. If the "dont send before" argument is specified, then the email will be sent with a delay - after the specified time.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="dontSendBefore"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        Task<bool> SendEmailWithDelayAsync(List<string> to, string subject, string body, bool isHtml = false, List<EmailAttachment> attachments = null,
            DateTime? dontSendBefore = null);

        /// <summary>
        /// Creates a <see cref="POEmail"/> object with specified arguments.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="dontSendBefore"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <returns></returns>
        POEmail CreateEmail(List<string> to, string subject, string body, bool isHtml = false,
            List<EmailAttachment> attachments = null, DateTime? dontSendBefore = null);

        /// <summary>
        /// Fills the specified <see cref="POEmail"/> object with the specified <see cref="EmailAttachment"/> list.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="attachments"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <returns></returns>
        POEmail FillEmailAttachments(POEmail email, List<EmailAttachment> attachments);

        /// <summary>
        /// Writes the specified <see cref="POEmail"/> object to the underlying database.
        /// </summary>
        /// <param name="email"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        Task<int> WriteEmailToDbAsync(POEmail email);

        /// <summary>
        /// Writes the specified <see cref="POEmail"/> object to the underlying database.
        /// </summary>
        /// <param name="email"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        int WriteEmailToDb(POEmail email);
    }
}
