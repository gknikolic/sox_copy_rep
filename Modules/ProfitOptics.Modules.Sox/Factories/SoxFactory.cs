﻿//using AspNetCore;
using AutoMapper;
using Flurl.Util;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using ProfitOptics.Framework;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Helpers;
using ProfitOptics.Modules.Sox.Infrastructure;
using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using ProfitOptics.Modules.Sox.Models.ViewModels;
using ProfitOptics.Modules.Sox.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GCColorStatusType = ProfitOptics.Framework.GCColorStatusType;

namespace ProfitOptics.Modules.Sox.Factories
{
    public class SoxFactory : ISoxFactory
    {
        #region Fields

        private readonly ISoxService _SoxService;
        private readonly IGroundControlService _groundControlService;
        private readonly IUserService _UserService;
        private readonly IMapper mapper;

        #endregion Fields

        #region Ctor

        public SoxFactory(ISoxService SoxService,
                          IGroundControlService groundControlService,
                          IMapper mapper,
                          IUserService _UserService)
        {
            _SoxService = SoxService;
            _groundControlService = groundControlService;
            this.mapper = mapper;
            this._UserService = _UserService;

        }



        #endregion Ctor

        #region Methods
        public DashboardModel PrepareNewDashboardModel(DashboardModel filterModel)
		{
            var cases = _SoxService.FilterCases(filterModel);
            var model = new DashboardModel();
            model.StartDate = filterModel.StartDate;
            if(cases!= null && cases.Count>0)
			{
				foreach (var singleCase in cases)
				{
                    var caseColor = _SoxService.GetCaseColorStatus(singleCase.Id);
                    var eventDate = new DateTime(singleCase.CTCase.DueTimestamp.Year, singleCase.CTCase.DueTimestamp.Month, singleCase.CTCase.DueTimestamp.Day); //get only date without hours
                    var existingEvent = model.Events.Where(x => x.EventDate == eventDate && x.EventColor == caseColor).FirstOrDefault();
                    if(existingEvent==null) //only add event if already deosnt exist for specific date and color
					{
                        var eventHtml = new Microsoft.AspNetCore.Html.HtmlString(GetCaseCalendarEventHtml(singleCase.Id));
                        model.Events.Add(new CalendarCaseEvent()
                        {
                            CaseId = singleCase.Id,
                            Title = singleCase.GCCustomer?.CustomerName,
                            Start = singleCase.CTCase.DueTimestamp.TrimMinutesAndSeconds(),
                            EventHtml = eventHtml,
                            EventColor = caseColor,
                            EventDate = eventDate,
                            EventHtmlString = eventHtml.ToString()
                        });
                    }
                   
                }
			}
            model.GCCustomers = _SoxService.GetGCCustomersSelectList();
            model.GCVendors = _SoxService.GetGCVendorsSelectList();


            model.LatestETLLogs = new LatestETLLogsModel();

            var latestLog = _SoxService.GetLatestCTETLLogCycle(false);
            model.LatestETLLogs.LatestETLLog = new ETLLogModel();
            model.LatestETLLogs.LatestETLLog = mapper.Map<ETLLogModel>(latestLog);

            var latestSuccessfulLog = _SoxService.GetLatestCTETLLogCycle(false);
            model.LatestETLLogs.LatestSuccessfulETLLog = new ETLLogModel();
            model.LatestETLLogs.LatestSuccessfulETLLog = mapper.Map<ETLLogModel>(latestSuccessfulLog);

            var etlLogWWOrder = _SoxService.GetLatestSuccessfulllETLLogsWWOrder();
            model.ETLLogWWOrder = mapper.Map<ETLLogModel>(etlLogWWOrder);

            var etlLogWWRoute = _SoxService.GetLatestSuccessfulllETLLogsWWRoute();
            model.ETLLogWWRoute = mapper.Map<ETLLogModel>(etlLogWWRoute);
            model.MapMarkers = _SoxService.GetWWMapMarkers();
            return model;
        }

        public string GetCaseCalendarEventHtml(int caseId)
		{
            string result = String.Empty;
            var color = _SoxService.GetCaseColorStatus(caseId);
            if (color == Models.GroundControl.GCColorStatusType.Yellow.ToString())
			{
                result = "<span class=\"calendarDot fc-status-5\"></span>";

            }
            else if (color == Models.GroundControl.GCColorStatusType.Red.ToString())
            {
                result = "<span class=\"calendarDot fc-status-2\"></span>";

            }

            else if (color == Models.GroundControl.GCColorStatusType.Purple.ToString())
            {
                result = "<span class=\"calendarDot fc-status-3\"></span>";

            }

            else if (color == Models.GroundControl.GCColorStatusType.Blue.ToString())
            {
                result = "<span class=\"calendarDot fc-status-1\"></span>";

            }

            else if (color == Models.GroundControl.GCColorStatusType.Green.ToString())
            {
                result = "<span class=\"calendarDot fc-status-6\"></span>";

            }

            else if (color == Models.GroundControl.GCColorStatusType.Orange.ToString())
            {
                result = "<span class=\"calendarDot fc-status-4\"></span>";

            }

            else if (color == Models.GroundControl.GCColorStatusType.Gray.ToString())
            {
                result = "<span class=\"calendarDot fc-status-7\"></span>";

            }
            return result;

        }

        public DashboardHourPreviewModel PrepareDashboardHourPreviewModel(DashboardModel filterModel)
		{
            try
			{
                var model = _SoxService.GetDashboardHourPreviewModel(filterModel);
                return model;
            }
            catch(Exception ex)
			{
                throw ex;
			}
		}

        public DashboardDetailedHourPreviewModel PrepareDashboardDetailedHourPreviewModel(DashboardModel filterModel)
        {
            try
            {
                var model = _SoxService.GetDashboardDetailedHourPreviewModel(filterModel);
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GCCaseModel PrepareDashboardCaseDetailsPreviewModel(int caseId)
        {
            try
            {
                var model = _SoxService.GetDashboardCaseDetailsModel(caseId);
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DashboardModel PrepareDashboardModel(DashboardModel filterModel)
        {
            //var cases = _SoxService.GetCCases();
            var cases = _SoxService.FilterCases(filterModel); //.Where(x => x.CanceledAtGCLifecycleStatusTypeId == null);

            var model = new DashboardModel();
            //var grouped = cases.GroupBy(x => x.CTCase.DueTimestamp);
            if (cases != null)
            {
                foreach (var item in cases)
                {

                    var color = _SoxService.GetCaseColorStatus(item.Id);

                    var caseEvent = model.Events.Where(x => x.Start == item.CTCase.DueTimestamp.TrimMinutesAndSeconds()).FirstOrDefault();
                    if (caseEvent != null)
                    {
                        //case
                        caseEvent.CasesLeft++;

                        if (color == Models.GroundControl.GCColorStatusType.Yellow.ToString())
                            caseEvent.NumOfYellowCases++;

                        if (color == Models.GroundControl.GCColorStatusType.Red.ToString())
                            caseEvent.NumOfRedCases++;

                        if (color == Models.GroundControl.GCColorStatusType.Purple.ToString())
                            caseEvent.NumOfPurpleCases++;

                        if (color == Models.GroundControl.GCColorStatusType.Blue.ToString())
                            caseEvent.NumOfBlueCases++;

                        if (color == Models.GroundControl.GCColorStatusType.Green.ToString())
                            caseEvent.NumOfGreenCases++;

                        if (color == Models.GroundControl.GCColorStatusType.Orange.ToString())
                            caseEvent.NumOfOrangeCases++;

                        if (color == Models.GroundControl.GCColorStatusType.Gray.ToString())
                            caseEvent.NumOfGrayCases++;
                    }
                    else
                    {
                        var yellow = 0;
                        var red = 0;
                        var purple = 0;
                        var blue = 0;
                        var green = 0;
                        var orange = 0;
                        var gray = 0;

                        if (color == ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Yellow.ToString())
                            yellow++;

                        if (color == ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Red.ToString())
                            red++;

                        if (color == ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Purple.ToString())
                            purple++;

                        if (color == ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Blue.ToString())
                            blue++;

                        if (color == ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Green.ToString())
                            green++;

                        if (color == ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Orange.ToString())
                            orange++;

                        if (color == ProfitOptics.Modules.Sox.Models.GroundControl.GCColorStatusType.Gray.ToString())
                            gray++;

                        model.Events.Add(new CalendarCaseEvent()
                        {
                            CaseId = item.Id,
                            Title = item.GCCustomer?.CustomerName,
                            CasesLeft = 0,
                            Start = item.CTCase.DueTimestamp.TrimMinutesAndSeconds(),
                            End = item.CTCase.DueTimestamp.TrimMinutesAndSeconds().AddMinutes(59),
                            EventColor = "white",
                            NumOfBlueCases = blue,
                            NumOfGreenCases = green,
                            NumOfOrangeCases = orange,
                            NumOfPurpleCases = purple,
                            NumOfRedCases = red,
                            NumOfYellowCases = yellow,
                            NumOfGrayCases = gray
                        });
                    }

                }
            }
            model.Events.ForEach(x => x.RenderCaseEvent()); //prepare html for all cases

            model.GCCustomers = _SoxService.GetGCCustomersSelectList();
            model.GCVendors = _SoxService.GetGCVendorsSelectList();


            model.LatestETLLogs = new LatestETLLogsModel();

            var latestLog = _SoxService.GetLatestCTETLLogCycle(false);
            model.LatestETLLogs.LatestETLLog = new ETLLogModel();
            model.LatestETLLogs.LatestETLLog = mapper.Map<ETLLogModel>(latestLog);

            var latestSuccessfulLog = _SoxService.GetLatestCTETLLogCycle(false);
            model.LatestETLLogs.LatestSuccessfulETLLog = new ETLLogModel();
            model.LatestETLLogs.LatestSuccessfulETLLog = mapper.Map<ETLLogModel>(latestSuccessfulLog);

            var etlLogWWOrder = _SoxService.GetLatestSuccessfulllETLLogsWWOrder();
            model.ETLLogWWOrder = mapper.Map<ETLLogModel>(etlLogWWOrder);

            var etlLogWWRoute = _SoxService.GetLatestSuccessfulllETLLogsWWRoute();
            model.ETLLogWWRoute = mapper.Map<ETLLogModel>(etlLogWWRoute);
            //test function with one query
            //_SoxService.GetLatestAndLatestSuccessfulETLLogs();

            return model;
        }

        public EventDetailsModel PrepareEventPreviewModel(DateTime dateFrom, ClaimsPrincipal user)
        {
            //DateTime dateTo = dateFrom.AddHours(1);
            var casesByRole = _SoxService.FilterCasesByUserRole(user).ToList(); //.Where(x => x.CanceledAtGCLifecycleStatusTypeId == null).ToList();
            var cases = casesByRole.Where(x => x.CTCase.DueTimestamp.TrimMinutesAndSeconds() == dateFrom).ToList();
            //var cases = _SoxService.GetGCCases.Where(x=>x.CTCase.DueTimestamp.TrimMinutesAndSeconds() == dateFrom ).ToList();

            List<GCCaseModel> casesModel = new List<GCCaseModel>();

            for (var i = 0; i < cases.Count(); i++)
            {
                var caseColor = _SoxService.GetCaseColorStatus(cases[i].Id);

                var caseModel = mapper.Map<GCCaseModel>(cases[i]);
                casesModel.Add(caseModel);

                //var caseContainers = _SoxService.GetCTCaseContainers().Where(x => x.CTCaseId == cases[i].CTCaseId).ToList();
                var trays = _SoxService.GetAllGCTrays().Where(x => x.GCCaseId == cases[i].Id).ToList();

                if (trays.Count > 0)
                {
                    StringBuilder sb = new StringBuilder("");
                    for (var k = 0; k < trays.Count; k++)
                    {
                        var caseContainer = _SoxService.GetCTCaseContainerById((int)trays[k].CTCaseContainerId);

                        if (k > 0)
                            sb.Append(", ");

                        sb.Append(caseContainer.ContainerName);

                        casesModel[i].InstrumentTrays = sb.ToString();
                    }
                }

                //if (caseContainers.Count>0)
                //{
                //	StringBuilder sb = new StringBuilder(caseContainers[0].ContainerName);

                //	for (var j = 1; j < caseContainers.Count(); j++)
                //	{

                //		sb.Append(", ");
                //		sb.Append(caseContainers[i].ContainerName);
                //	}
                //	casesModel[i].InstrumentTrays = sb.ToString();				
                //}

                casesModel[i].CaseColor = caseColor;
            }

            EventDetailsModel model = new EventDetailsModel()
            {
                Cases = casesModel,
                Date = dateFrom,
                //Surgeon = cases[0].CTCase.CaseDoctor,
                //DueTime = cases[0].CTCase.DueTimestamp,
                //InstrumentTrays = sb.ToString()
            };

            return model;
        }

        public GCCasesModel PrepareCasesForPlanning(DateTime date, ClaimsPrincipal user, string caseReference, int? vehicleId, int? driverId)
        {
            var casesByRole = _SoxService.FilterCasesByUserRole(user).ToList();

            var cases = casesByRole.Where(x => x.CTCase.DueTimestamp.TrimHoursMinutesSeconds() == date.TrimHoursMinutesSeconds() || x.CTCase.DueTimestamp.TrimHoursMinutesSeconds() == date.AddDays(1).TrimHoursMinutesSeconds()).AsEnumerable(); //get all cases for today or tomorrow because tomorrow cases have outgoing orders day before

            var filteredCases = _SoxService.FilterCasesForCasePlanning(cases, caseReference, vehicleId, driverId).Where(x => x.CanceledAtGCLifecycleStatusTypeId == null);

            List<GCCaseModel> caseModelList = new List<GCCaseModel>();
            foreach (var item in filteredCases)
            {
                var lifecycleStatus = _SoxService.GetActiveLifecycleStatusForCase(item.Id);
                var caseModel = mapper.Map<GCCaseModel>(item);
                caseModel.LifecycleStatusType = lifecycleStatus;

                var trays = _SoxService.GetTraysForGCCase(item.Id);
                caseModel.TrayCount = trays.Count();
                caseModel.CustomerName = item.GCCustomer.CustomerName;

                caseModelList.Add(caseModel);
            }

            GCCasesModel model = new GCCasesModel()
            {
                GCCases = caseModelList
            };

            int count = 0;
            var groupedCases = cases.GroupBy(x => x.WWPickupRouteId);
            if (groupedCases.Any())
            {
                foreach (var group in groupedCases)
                {
                    foreach (var caseInGroup in group)
                    {
                        var lifecycleStatus = _SoxService.GetActiveLifecycleStatusForCase(caseInGroup.Id);
                        if (lifecycleStatus.Title == "Shipment Staged")
                            count++;
                    }

                }
                if (count == groupedCases.FirstOrDefault().Count())
                {
                    //this will work only if there is just one group of cases 
                    model.AllCasesInSterileStagingForLoading = true;
                }
            }



            return model;
        }

        public GCCasesModel PrepareGCCasesModel(GCCasesModel filterModel)
        {
            var cases = _SoxService.FilterCasesList(filterModel);
            //var cases = _SoxService.GetCCases();
            var gcCustomers = _SoxService.GetGCCustomersSelectList();
            var gcVendors = _SoxService.GetGCVendorsSelectList();

            GCCasesModel model = new GCCasesModel()
            {
                GCCases = mapper.Map<List<GCCaseModel>>(cases),
                GCCustomers = gcCustomers,
                GCVendors = gcVendors
            };
            model.User = filterModel.User;
            model.DateFrom = filterModel.DateFrom;
            model.DateTo = filterModel.DateTo;
            return model;
        }

        public GCCasesModel PrepareGCCasesModelForCaseSchedule(GCCasesModel filterModel)
        {
            var cases = _SoxService.FilterCasesList(filterModel);
            //var cases = _SoxService.GetCCases();
            var gcCustomers = _SoxService.GetGCCustomersFilteredSelectList(filterModel.User);
            var gcVendors = _SoxService.GetGCVendorsFilteredSelectList(filterModel.User);
            if(filterModel.User.IsInRole(ApplicationRoleNames.VendorUser)) //always preselect vendor for vendor user if he has only one company
			{
                if(gcVendors!=null && gcVendors.Count() == 1)
				{
                    gcVendors.First().Selected = true;
				}
			}
            GCCasesModel model = new GCCasesModel()
            {
                GCCases = mapper.Map<List<GCCaseModel>>(cases),
                GCCustomers = gcCustomers,
                GCVendors = gcVendors
            };
            model.User = filterModel.User;
            model.DateFrom = filterModel.DateFrom;
            model.DateTo = filterModel.DateTo;
            model.CaseScheduleColorType = CaseScheduleColorType.Gray;
            if(model.GCCases != null && model.GCCases.Count>0)
			{
				foreach (var gcCase in model.GCCases)
				{
                    bool isCaseCompleted = _SoxService.CheckIfCaseWasInStatus(Models.GroundControl.LifecycleStatusType.Received, gcCase.Id);
                    if(isCaseCompleted)
					{
                        gcCase.CaseScheduleColorType = CaseScheduleColorType.Gray;
                    }
                    else
					{
                        gcCase.CaseScheduleColorType = _SoxService.GetCaseScheduleColorType(gcCase.Id);
                    }

                }
                //check color type of entire page (Color priority: Red, Yellow, Green, Gray)
                var redColor = model.GCCases.Where(x => x.CaseScheduleColorType == CaseScheduleColorType.Red).ToList();
                if(redColor != null && redColor.Count>0)
				{
                    model.CaseScheduleColorType = CaseScheduleColorType.Red;
				}
                else
				{
                    var yellowColor = model.GCCases.Where(x => x.CaseScheduleColorType == CaseScheduleColorType.Yellow).ToList();
                    if (yellowColor != null && yellowColor.Count > 0)
                    {
                        model.CaseScheduleColorType = CaseScheduleColorType.Yellow;
                    }
                    else
					{
                        var greenColor = model.GCCases.Where(x => x.CaseScheduleColorType == CaseScheduleColorType.Green).ToList();
                        if (greenColor != null && greenColor.Count > 0)
                        {
                            model.CaseScheduleColorType = CaseScheduleColorType.Green;
                        }
                    }
                }

			}
            return model;
        }


        public TraysModel PrepareTraysModel(ClaimsPrincipal user, int? actualId = null)
        {
            List<TrayModel> traysList = GetTraysForUser(user, actualId);

            // Candidate for Async Action (or loading paged record set):
            if (traysList.Count > 0)
            {
                traysList.ForEach(x =>
                {
                    x.CountSheetStatus = Helpers.Constants.COUNT_SHEET_COMPLETE;

                    if (_groundControlService.HasActualIncompleteCountSheet(x.CTContainerTypeActualId))
                    {
                        x.CountSheetStatus = Helpers.Constants.COUNT_SHEET_NOT_COMPLETE;
                    }
                });
            }

            TraysModel traysModel = new TraysModel()
            {
                Trays = traysList
            };

            return traysModel;
        }

        private List<TrayModel> GetTraysForUser(ClaimsPrincipal user, int? actualId)
        {
            List<TrayModel> traysList = new List<TrayModel>();

            if (user.IsInRole(ApplicationRoleNames.CustomerUser) || user.IsInRole(ApplicationRoleNames.VendorUser))
            {
                var cases = _SoxService.FilterCasesByUserRole(user);

                List<GCTray> trays = new List<GCTray>();
                foreach (var item in cases)
                {
                    var traysForCase = _SoxService.GetTraysForGCCase(item.Id, true);
                    trays.AddRange(traysForCase);
                }

                //trays = trays.DistinctBy(x => x.CTContainerTypeActualId).ToList();
                trays = _SoxService.GetAllInventoryTrays(trays.Where(x => x.CTContainerTypeActualId.HasValue).DistinctBy(x => x.CTContainerTypeActualId).Select(x => x.CTContainerTypeActualId.Value).ToList(), true);
                if (actualId != null)
                {
                    //actuals = actuals.Where(x => x.Id == trayId).ToList();
                    trays = trays.Where(x => x.CTContainerTypeActualId.HasValue && x.CTContainerTypeActualId.Value == actualId).ToList();
                }

                foreach (var tray in trays)
                {
                    if (tray.CTContainerTypeId.HasValue)
                    {
                        var ctContainerTypeModel = mapper.Map<CTContainerTypeModel>(tray.CTContainerType);
                        ctContainerTypeModel.CTContainerTypeActualId = tray.CTContainerTypeActualId ?? 0;

                        var trayModel = mapper.Map<TrayModel>(ctContainerTypeModel);

                        trayModel.Barcode = tray.Barcode;
                        trayModel.LifecycleStatusId = tray.GCLifecycleStatusTypeId;
                        if (tray.GCLifecycleStatusType != null)
                        {
                            trayModel.LifecycleStatus = tray.GCLifecycleStatusType.Title;
                        }

                        var gcTray = trays.Where(x => x.CTContainerTypeActualId == tray.Id).FirstOrDefault();
                        if (gcTray != null)
                        {
                            trayModel.Barcode = gcTray.Barcode;
                        }
                        trayModel.VendorName = tray.CTContainerType.VendorName;
                        traysList.Add(trayModel);
                    }
                }

                if (user.IsInRole(ApplicationRoleNames.VendorUser))
                {
                    if (user.Claims != null)
                    {
                        var companiesIds = user.Claims.Where(x => x.Type == Framework.Web.Framework.Enums.CompanyPermissionType.CompanyPermissionTypeView.ToString()).FirstOrDefault();
                        if (companiesIds != null && !string.IsNullOrWhiteSpace(companiesIds.Value))
                        {
                            var companiesIdsList = companiesIds.Value.Split(',').ToList();
                            if (companiesIdsList != null && companiesIdsList.Count > 0)
                            {
                                int companyId = 0;
                                if (int.TryParse(companiesIdsList[0], out companyId) && companyId > 0)
                                {
                                    var company = _UserService.GetChildCompanyById(companyId);

                                    if (company != null)
                                    {
                                        var ctContainerTypes = _SoxService.GetAllCTConntainerTypes(company.Title);

                                        if (ctContainerTypes != null && ctContainerTypes.Count > 0)
                                        {
                                            List<TrayModel> traysListForVendor = new List<TrayModel>();

                                            foreach (var ctContainerType in ctContainerTypes)
                                            {
                                                var ctContainerTypeModel = mapper.Map<CTContainerTypeModel>(ctContainerType);
                                                var trayModel = mapper.Map<TrayModel>(ctContainerTypeModel);
                                                trayModel.VendorName = ctContainerType.VendorName;
                                                traysListForVendor.Add(trayModel);
                                            }
                                            var traysListForVendorDifferent = traysListForVendor.Except(traysList).ToList();

                                            traysList.AddRange(traysListForVendorDifferent);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (user.IsInRole(ApplicationRoleNames.SuperAdmin) || user.IsInRole(ApplicationRoleNames.Admin) || user.IsInRole(ApplicationRoleNames.User))
            {

                List<GCTray> trays = new List<GCTray>();
                if (actualId.HasValue)
                {
                    var actualIDs = new List<int>();
                    actualIDs.Add(actualId.Value);
                    var inventoryTrays = _SoxService.GetAllInventoryTrays(actualIDs, true);
                    if (inventoryTrays != null && inventoryTrays.Count == 1)
                    {
                        var tray = inventoryTrays.First();
                        //// since _SoxService.GetTrayById doesn't include Actual nor LifeCycle, fetching now:
                        //if (tray.CTContainerTypeActualId.HasValue)
                        //{
                        //    var actual = _SoxService.GetCTContainerTypeActualById(tray.CTContainerTypeActualId.Value);
                        //    if (actual != null)
                        //    {
                        //        tray.CTContainerTypeActual = actual;
                        //    }
                        //}

                        //if (tray.GCLifecycleStatusTypeId.HasValue)
                        //{
                        //    var lifeCycle = _SoxService.GetLifecycleStatusById(tray.GCLifecycleStatusTypeId.Value);
                        //    if (lifeCycle != null)
                        //    {
                        //        tray.GCLifecycleStatusType = lifeCycle;
                        //    }
                        //}
                        trays.Add(tray);
                    }
                }
                else
                {
                    //trays = _SoxService.GetAllGCTrays();
                    //trays = trays.DistinctBy(x => x.CTContainerTypeActualId).ToList();
                    trays = _SoxService.GetAllInventoryTrays(null, true);
                }

                foreach (var tray in trays)
                {
                    //var status = _SoxService.GetActiveLifecycleStatusAndTimestampForTray(tray.Id);


                    //var ctContainerType = _SoxService.GetCTContainerType((int)tray.CTContainerTypeId);
                    var ctContainerTypeModel = mapper.Map<CTContainerTypeModel>(tray.CTContainerType);
                    ctContainerTypeModel.CTContainerTypeActualId = tray.CTContainerTypeActualId ?? 0;
                    //if (status.LifecycleStatus != null)
                    //	ctContainerTypeModel.LifecycleStatus = status.LifecycleStatus.Title;

                    var trayModel = mapper.Map<TrayModel>(ctContainerTypeModel);

                    if (tray.CTContainerTypeActual != null)
                    {
                        trayModel.Name = tray.CTContainerTypeActual.Name;
                        trayModel.ParentName = tray.CTContainerTypeActual.ParentName;
                    }

                    trayModel.Barcode = tray.Barcode;
                    trayModel.LifecycleStatusId = tray.GCLifecycleStatusTypeId;
                    if (tray.GCLifecycleStatusType != null)
                    {
                        trayModel.LifecycleStatus = tray.GCLifecycleStatusType.Title;
                    }

                    var gcTray = trays.Where(x => x.CTContainerTypeActualId == tray.Id).FirstOrDefault();
                    if (gcTray != null)
                    {
                        trayModel.Barcode = gcTray.Barcode;
                    }
                    trayModel.VendorName = tray.CTContainerType.VendorName;
                    traysList.Add(trayModel);
                }
            }

            return traysList;
        }

        public TraysModel PrepareTarysForCaseModel(int customerId)
        {
            //var ctCasesIds = _SoxService.FilterCasesByUserRole(user).Select(x => x.CTCaseId).ToList();
            //var trays = _SoxService.GetCTCaseContainers().Where(x => ctCasesIds.Contains(x.CTCaseId));
            var casesByCustomer = _SoxService.GetCasesByCustomer(customerId);
            var date = DateTime.Now;
            var cases = casesByCustomer.Where(x => x.CTCase.DueTimestamp.TrimHoursMinutesSeconds() == date.TrimHoursMinutesSeconds()).ToList();

            List<GCTray> trays = new List<GCTray>();
            foreach (var item in cases)
            {
                var traysForCase = _SoxService.GetTraysForGCCase(item.Id, true);

                trays.AddRange(traysForCase);
            }

            List<TrayModel> traysList = new List<TrayModel>();
            foreach (var tray in trays)
            {
                var status = _SoxService.GetActiveLifecycleStatusAndTimestampForTray(tray.Id);
                //var ctContainerType = _SoxService.GetCTContainerType(tray.CTContainerTypeId ?? 0);
                if (tray.CTContainerType != null)
                {
                    var ctContainerTypeModel = mapper.Map<CTContainerTypeModel>(tray.CTContainerType);
                    ctContainerTypeModel.CTContainerTypeActualId = tray.CTContainerTypeActualId ?? 0;
                    if (status.LifecycleStatus != null)
                    {
                        ctContainerTypeModel.LifecycleStatus = status.LifecycleStatus.Title;
                        ctContainerTypeModel.Timestamp = status.Timestamp;
                    }
                    else if (tray.GCLifecycleStatusTypeId.HasValue && tray.GCLifecycleStatusType != null)
                    {
                        ctContainerTypeModel.LifecycleStatus = tray.GCLifecycleStatusType.Title;
                        ctContainerTypeModel.Timestamp = status.Timestamp;
                    }

                    var trayModel = mapper.Map<TrayModel>(ctContainerTypeModel);
                    trayModel.Id = tray.Id;
                    trayModel.CaseReference = tray.GCCase.CTCase.CaseReference;
                    trayModel.DueTimestamp = tray.GCCase.CTCase.DueTimestamp;
                    trayModel.ColorStatus = _SoxService.GetTrayColorStatus(tray.Id);
                    trayModel.WasInPickupStatus = _SoxService.CheckTrayWasInStatus("Pickup", tray.Id);

                    traysList.Add(trayModel);
                }
            }

            TraysModel traysModel = new TraysModel()
            {
                Trays = traysList
            };

            return traysModel;
        }

        //public TraysModel PrepareTaryModel(ClaimsPrincipal user, int? trayId)
        //{
        //	//var ctCasesIds = _SoxService.FilterCasesByUserRole(user).Select(x => x.CTCaseId).ToList();
        //	//var trays = _SoxService.GetCTCaseContainers().Where(x => ctCasesIds.Contains(x.CTCaseId));

        //	var trays = _SoxService.GetTrayById(trayId);
        //	List<TrayModel> traysList = new List<TrayModel>();
        //	foreach (var tray in trays)
        //	{
        //		var status = _SoxService.GetActiveLifecycleStatusAndTimestampForTray(tray.Id);
        //		var ctContainerType = _SoxService.GetCTContainerType(tray.CTContainerTypeId ?? 0);
        //		var ctContainerTypeModel = mapper.Map<CTContainerTypeModel>(ctContainerType);
        //		ctContainerTypeModel.CTContainerTypeActualId = tray.CTContainerTypeActualId;
        //		if (status.LifecycleStatus != null)
        //			ctContainerTypeModel.LifecycleStatus = status.LifecycleStatus.Title;

        //		var trayModel = mapper.Map<TrayModel>(ctContainerTypeModel);
        //		traysList.Add(trayModel);
        //	}

        //	TraysModel traysModel = new TraysModel()
        //	{
        //		Trays = traysList
        //	};

        //	return traysModel;
        //}

        public GCCustomersModel PrepareGCCustomersModel()
        {
            var customers = _SoxService.GetAllGCCustomers(true);
            foreach (var customer in customers)
            {
                customer.ServiceTime = customer.ServiceTime / 60;
            }

            List<GCCustomerModel> customersModel = new List<GCCustomerModel>();

            var companies = _UserService.GetGCParentCompanies();
            var childCompanies = _UserService.GetChildCompanies();

            foreach (var customer in customers)
            {
                var startTime = Converters.ConvertSecondsToDateTime(customer.DeliveryStart);
                var endTime = Converters.ConvertSecondsToDateTime(customer.DeliveryEnd);

                var customerModel = mapper.Map<GCCustomerModel>(customer);
                customerModel.DeliveryStart = startTime.ToString("hh:mm:tt");
                customerModel.DeliveryEnd = endTime.ToString("hh:mm:tt");

                customerModel.SelectedDeliveryStart = startTime;
                customerModel.SelectedDeliveryEnd = endTime;

                customerModel.GCParentCompanies = companies;
                var childCompany = childCompanies.Where(x => x.GCCustomerId == customer.Id).FirstOrDefault();
                if (childCompany != null)
                {
                    customerModel.SelectedCompany = childCompany.GCParentCompanyId;
                }

                customersModel.Add(customerModel);
            }

            GCCustomersModel model = new GCCustomersModel()
            {
                Customers = customersModel
            };

            return model;
        }

        public GCCustomerModel PrepareGCCustomerModel(int customerId)
        {
            var gcCustomer = _SoxService.GetCustomer(customerId);
            var childCompany = _UserService.GetChildCompanyForCustomer(customerId);

            var startTime = Converters.ConvertSecondsToDateTime(gcCustomer.DeliveryStart);
            var endTime = Converters.ConvertSecondsToDateTime(gcCustomer.DeliveryEnd);

            var customerModel = mapper.Map<GCCustomerModel>(gcCustomer);

            if (childCompany != null)
            {
                customerModel.SelectedCompany = childCompany.GCParentCompanyId;
            }

            customerModel.ServiceTime = customerModel.ServiceTime / 60;
            customerModel.DeliveryStart = startTime.ToString("hh:mm tt");
            customerModel.DeliveryEnd = endTime.ToString("hh:mm tt");

            return customerModel;
        }

        public GCVendorModel PrepareGCVendorModel(int vendorId)
        {
            var gcVendor = _SoxService.GetGCVendor(vendorId);
			var childCompany = _UserService.GetChildCompanyForVendor(vendorId);
			var vendorModel = mapper.Map<GCVendorModel>(gcVendor);

			if (childCompany != null)
			{
                vendorModel.SelectedCompany = childCompany.GCParentCompanyId;
			}
			return vendorModel;
        }

        public GCVendorsModel PrepareGCVendorsModel()
        {
            var vendors = _SoxService.GetVendors();

            List<GCVendorModel> vendorsModel = new List<GCVendorModel>();

            var companies = _UserService.GetGCParentCompanies();
            var childCompanies = _UserService.GetChildCompanies();

            foreach (var vendor in vendors)
            {
                var vendorModel = mapper.Map<GCVendorModel>(vendor);
                vendorModel.GCParentCompanies = companies;
                var childCompany = childCompanies.Where(x => x.GCVendorId == vendor.Id).FirstOrDefault();
                if (childCompany != null)
                {
                    vendorModel.SelectedCompany = childCompany.GCParentCompanyId;
                }
                vendorsModel.Add(vendorModel);
            }

            var qbCusomerExternalUrl = "";
            var qbOptions = _groundControlService.GetQuickBooksOptions();
            if (qbOptions != null && !string.IsNullOrWhiteSpace(qbOptions.InstanceUrl))
            {
                qbCusomerExternalUrl = qbOptions.InstanceUrl;
            }
            GCVendorsModel model = new GCVendorsModel()
            {
                Vendors = vendorsModel,
                QBCusomerExternalUrl = qbCusomerExternalUrl
            };

            return model;
        }

        public CaseDetailsModel PrepareCaseDetailsModel(int id, ClaimsPrincipal user)
        {
            var model = new CaseDetailsModel();

            var gcCase = _SoxService.GetGCCaseById(id, user);
            if (gcCase == null)
                return null;
           
            bool isLoanerCase = false;
            var trays = _SoxService.GetTraysForGCCase(id, true);
            var ctContainersTypes = new List<CTContainerTypeModel>();
            var loanerIds = new List<int>();

            var customerStatuses = new List<LifecycleStatusType>() { LifecycleStatusType.Picked, LifecycleStatusType.Received, LifecycleStatusType.Assembly, LifecycleStatusType.Verification,
                                                                     LifecycleStatusType.Sterilize, LifecycleStatusType.ShippedOutbound, LifecycleStatusType.Picked};
            var vendorStatuses = new List<LifecycleStatusType>() { LifecycleStatusType.Sink, LifecycleStatusType.Washer, LifecycleStatusType.Assembly,
                                                                   LifecycleStatusType.Verification, LifecycleStatusType.Sterilize, LifecycleStatusType.ShippedOutbound, LifecycleStatusType.Delivered,
                                                                   LifecycleStatusType.InSurgery, LifecycleStatusType.Picked};

            foreach (var tray in trays)
            {
                var status = _SoxService.GetActiveLifecycleStatusAndTimestampForTray(tray.Id);
                //var ctContainerType = _SoxService.GetCTContainerType(tray.CTContainerTypeId ?? 0);
                if (tray.CTContainerType != null)
                {
                    var ctContainerTypeModel = mapper.Map<CTContainerTypeModel>(tray.CTContainerType);
                    if (status.LifecycleStatus != null && status.LifecycleStatus.Title != null)
                    {
                        ctContainerTypeModel.LifecycleStatus = status.LifecycleStatus.Title;
                        ctContainerTypeModel.Timestamp = status.Timestamp;
                    }
                    //ctContainerTypeModel.LifecycleStatus = status.LifecycleStatus?.Title;
                    else if (tray.GCLifecycleStatusType != null)
                    {
                        ctContainerTypeModel.LifecycleStatus = tray.GCLifecycleStatusType.Title;
                        ctContainerTypeModel.Timestamp = status.Timestamp;
                    }
                    ctContainerTypeModel.IsGCTrayActiveStatus = (tray.IsActiveStatus.HasValue && tray.IsActiveStatus.Value);
                    ctContainerTypeModel.ColorStatus = _SoxService.GetTrayColorStatus(tray.Id);
                    ctContainerTypeModel.CTContainerTypeActualId = tray.CTContainerTypeActualId ?? 0;
                    ctContainerTypeModel.ActualLapCount = tray.ActualLapCount;
                    if (user.IsInRole(ApplicationRoleNames.CustomerUser))
                    {
                        ctContainerTypeModel.LifecycleCategory = customerStatuses.Contains((LifecycleStatusType)status.LifecycleStatus?.Code) ? status.LifecycleStatus?.GCLifecycleStatusTypeCategory?.Title : null;
                        //ctContainerTypeModel.LifecycleCategory = customerStatuses.Contains((LifecycleStatusType)tray.GCLifecycleStatusType?.Code) ? tray.GCLifecycleStatusType?.GCLifecycleStatusTypeCategory?.Title : null;
                    }

                    if (user.IsInRole(ApplicationRoleNames.VendorUser))
                    {
                        ctContainerTypeModel.LifecycleCategory = vendorStatuses.Contains((LifecycleStatusType)status.LifecycleStatus?.Code) ? status.LifecycleStatus?.GCLifecycleStatusTypeCategory?.Title : null;
                        //ctContainerTypeModel.LifecycleCategory = vendorStatuses.Contains((LifecycleStatusType)tray.GCLifecycleStatusType?.Code) ? tray.GCLifecycleStatusType?.GCLifecycleStatusTypeCategory?.Title : null;
                    }

                    var test = vendorStatuses.Contains((LifecycleStatusType)tray.GCLifecycleStatusType?.Code);
                    //ctContainerTypeModel.LifecycleCategory = tray.GCLifecycleStatusType?.GCLifecycleStatusTypeCategory?.Title;

                    var actual = _SoxService.GetCTContainerTypeActualById(ctContainerTypeModel.CTContainerTypeActualId);

                    if (tray.CTCaseContainer != null) 
                    {
                        ctContainerTypeModel.Name = tray.CTCaseContainer.ContainerName != null ? tray.CTCaseContainer.ContainerName /*+ " " + actual.Name*/ : null;
                        ctContainerTypeModel.VendorName = tray.CTCaseContainer.VendorName != null ? tray.CTCaseContainer.VendorName : null;
                    }
                    if (tray.IsLoaner != null && tray.IsLoaner == true)
                    {
                        isLoanerCase = true;
                        ctContainerTypeModel.IsLoaner = true;
                    }
                    if (tray.IsLoaner != null && tray.IsLoaner == true)
                    {
                        //loanerIds.Add(tray.)
                    }
                    //find current tray location
                    if (status.LifecycleStatus != null)
                    {
                        var currentStatus = (LifecycleStatusType)Enum.Parse(typeof(LifecycleStatusType), status.LifecycleStatus?.Code.ToString());
                        var currentLocation = _SoxService.GetTraysPhisicalLocation(currentStatus);
                        switch (currentLocation)
                        {
                            case TrayPhisicalLocationType.VM:
                                ctContainerTypeModel.Location = "VestedMedical";
                                break;
                            case TrayPhisicalLocationType.Vehicle:
                                ctContainerTypeModel.Location = "Vehicle/In Transit";
                                break;
                            case TrayPhisicalLocationType.Provider:
                                ctContainerTypeModel.Location = "Provider";
                                break;
                            default:
                                ctContainerTypeModel.Location = "No data";
                                break;
                        }
                    }
                    else
					{
                        ctContainerTypeModel.Location = "No data";
                    }
                    
					ctContainersTypes.Add(ctContainerTypeModel);
				}
			}

			model.GCCase = mapper.Map<GCCaseModel>(gcCase);
            model.CaseDetailsTraysModel = new CaseDetailsTraysModel();
            model.CaseDetailsTraysModel.Trays = new List<TrayModel>();

            for (var i = 0; i < ctContainersTypes.Count(); i++)
            {
                var tray = mapper.Map<TrayModel>(ctContainersTypes[i]);
                model.CaseDetailsTraysModel.Trays.Add(tray);

            }
            model.LifecycleStatuses = new List<LifecycleStatusModel>();
            if (isLoanerCase)
            {
                model.LifecycleCategories = _SoxService.GetCaseLifecyceCategories(gcCase.Id, false, true);
            }
            else
            {
                if (gcCase != null)
                {
                    model.LifecycleCategories = _SoxService.GetCaseLifecyceCategories(gcCase.Id);
                    model.PreviousLifecycleCategories = _SoxService.GetCaseLifecyceCategories(gcCase.Id, true);
                }
            }

            if (model.GCCase != null)
                model.GCCase.IsLoaner = isLoanerCase;

            if (gcCase != null)
            {
                model.Color = _SoxService.GetCaseColorStatus(gcCase.Id);
                model.NoteGroups = _SoxService.GetCaseNotes(gcCase.Id);
            }

            string qbInstanceUrl = null;
            var qbOptions = _groundControlService.GetQuickBooksOptions();
            if (qbOptions != null && !string.IsNullOrWhiteSpace(qbOptions.InstanceUrl))
            {
                qbInstanceUrl = qbOptions.InstanceUrl;
            }
            model.QBCusomerExternalUrl = qbInstanceUrl;

            List<GCColorStatusType> colorStatuses = _SoxService.GetCaseColorStatusTypes();

            // for MVP, BLUE is not available:
            if (colorStatuses != null)
            {
                var blueColor = colorStatuses.Where(x => x.Name.Equals("Blue")).FirstOrDefault();
                if (blueColor != null)
                {
                    colorStatuses.Remove(blueColor);
                }
            }

            model.AddColorCommentModal = new AddColorCommentModal()
            {
                GCTrays = gcCase.GCTrays.Select(x => new SelectListItem() { Text = x.CTCaseContainer.ContainerName, Value = x.Id.ToString() }).ToList(),
                GCColors = colorStatuses.Select(x => new SelectListItem() { Text = x.Name, Value = x.Id.ToString() }).ToList()
            };
            if(user.IsInRole(ApplicationRoleNames.VendorUser) || user.IsInRole(ApplicationRoleNames.CustomerUser))
			{
                if (model.CaseDetailsTraysModel != null && model.CaseDetailsTraysModel.Trays != null && model.CaseDetailsTraysModel.Trays.Count > 0)
                {
                    model.CaseDetailsTraysModel.AssemblyPhotos = new List<TrayAssemblyPhotos>();
                    model.CaseDetailsTraysModel.LatestBIResults = new List<TrayHistorySterilizeBIResultPhotos>();
                    model.CaseDetailsTraysModel.LatestDeliveryPods = new List<TrayHistoryDeliveryPhotos>();
                    model.CaseDetailsTraysModel.NumOfPhotos = 0;
                    foreach (var tray in model.CaseDetailsTraysModel.Trays)
                    {
                        var trayHistory = PrepareTrayHistoryModelForVendors(tray.CTContainerTypeActualId);
                        if (trayHistory.TrayAssemblyPhotos != null && trayHistory.TrayAssemblyPhotos.AssemblyPhotos != null && trayHistory.TrayAssemblyPhotos.AssemblyPhotos.Pictures != null && trayHistory.TrayAssemblyPhotos.AssemblyPhotos.Pictures.Count > 0)
                        {
                            model.CaseDetailsTraysModel.AssemblyPhotos.Add(trayHistory.TrayAssemblyPhotos);
                        }
                        if (trayHistory.LatestBIResults != null && trayHistory.LatestBIResults.ImagesData != null && trayHistory.LatestBIResults.ImagesData.Count > 0)
						{
                            model.CaseDetailsTraysModel.LatestBIResults.Add(trayHistory.LatestBIResults);
						}
                        if (trayHistory.LatestDeliveryPods != null && trayHistory.LatestDeliveryPods.PodContainer != null && trayHistory.LatestDeliveryPods.PodContainer.ShowPictures && trayHistory.LatestDeliveryPods.PodContainer.Pictures != null && trayHistory.LatestDeliveryPods.PodContainer.Pictures.Count > 0)
						{
                            model.CaseDetailsTraysModel.LatestDeliveryPods.Add(trayHistory.LatestDeliveryPods);
                        }
                        model.CaseDetailsTraysModel.NumOfPhotos += trayHistory.NumOfPhotos;
                    }
                }
            }
                        
            return model;
		}

		public TrayHistoryModel PrepareTrayHistoryModel(int trayId)
        {
            TrayHistoryModel model = _SoxService.GetTrayHistory(trayId);
            model.CTContainerTypeActualId = trayId;
            return model;
        }

        public TrayHistoryModel PrepareTrayHistoryModelForVendors(int trayId)
        {
            TrayHistoryModel model = _SoxService.GetTrayHistoryForVendors(trayId);
            model.CTContainerTypeActualId = trayId;
            return model;
        }

        public ProcedureTypesModel PrepareProcedureTypesModel(bool? includeInactive = false)
        {
            ProcedureTypesModel model = _SoxService.GetProcedureTypes(includeInactive);
            return model;
        }

        public ProcedureTypeModel PrepareProcedureTypeModel(int procedureTypeId)
        {
            ProcedureTypeModel model = _SoxService.GetProcedureType(procedureTypeId);
            return model;
        }

        public ProceduresModel PrepareProceduresModel()
        {
            ProceduresModel model = _SoxService.GetProcedures();
            return model;
        }

        public ProcedureModel PrepareProcedureModel(int procedureId)
        {
            ProcedureModel model = _SoxService.GetProcedure(procedureId);
            var procedureTypeSelectItems = new List<SelectListItem>();
            var procedureTypes = _SoxService.GetProcedureTypes(false);
            if(procedureTypes != null && procedureTypes.ProcedureTypes != null && procedureTypes.ProcedureTypes.Count > 0)
            {
                procedureTypeSelectItems = procedureTypes.ProcedureTypes.Select(x => new SelectListItem
                {
                    Text = x.Title,
                    Value = x.Id.ToString()
                }).ToList();
            }
            model.ProcdureTypes = procedureTypeSelectItems;
            return model;
        }

        public InventoryAlerStatusModel PrepareInventoryAlertStatusModel(ClaimsPrincipal user)
        {
            var inventoryAlertStatus = new InventoryAlerStatusModel();
            //inventoryAlertStatus.QuarantinedTrayCount = await _context.GCTrays.CountAsync(x => x.GCLifecycleStatusTypeId.HasValue && x.GCLifecycleStatusTypeId.Value == (int)LifecycleStatusType.Quarantine);
            //inventoryAlertStatus.IncompleteCountSheetCount = await _context.CTAssemblyCountSheets.CountAsync(x => x.ActualCount < x.RequiredCount);

            var trays = GetTraysForUser(user, null);
            if(trays != null && trays.Count > 0)
            {
                inventoryAlertStatus.QuarantinedTrayCount = trays.Count(x => x.LifecycleStatusId.HasValue && x.LifecycleStatusId.Value == (int)Models.GroundControl.LifecycleStatusType.QualityHold);
                var actualIds = trays.Select(x => x.CTContainerTypeActualId).Distinct().ToList();
                inventoryAlertStatus.IncompleteCountSheetCount = _groundControlService.GetIncompleteSheetCountForTrayIds(actualIds);
            }
            return inventoryAlertStatus;
        }

        public GCSystemAlertInfoModel PrepareSystemAlertsStatusModel(ClaimsPrincipal user)
        {
            var gcSystemAlertModel = new GCSystemAlertInfoModel();

            var cases = _SoxService.FilterCasesByUserRole(user).ToList();

            List<GCTray> trays = new List<GCTray>();
            foreach (var item in cases)
            {
                var traysForCase = _SoxService.GetTraysForGCCase(item.Id, true);
                trays.AddRange(traysForCase);
            }

            if (trays != null && trays.Count > 0)
            {
                gcSystemAlertModel.SystemAlertCount = _groundControlService.GetSystemAlertCountForTrayIds(trays.Select(x => x.Id).ToList());
            }
            return gcSystemAlertModel;
        }

        public CaseDetailsTraysModel PrepareTrayDetailsForVendorModel(int trayActualId)
		{
            var model = new CaseDetailsTraysModel();


            var tray = _SoxService.GetTrayByActualId(trayActualId);
            var ctContainersTypes = new List<CTContainerTypeModel>();
            var loanerIds = new List<int>();
            var status = _SoxService.GetActiveLifecycleStatusAndTimestampForTray(tray.Id);
            //var ctContainerType = _SoxService.GetCTContainerType(tray.CTContainerTypeId ?? 0);
            if (tray.CTContainerType != null)
            {
                var ctContainerTypeModel = mapper.Map<CTContainerTypeModel>(tray.CTContainerType);
                if (status.LifecycleStatus != null && status.LifecycleStatus.Title != null)
                {
                    ctContainerTypeModel.LifecycleStatus = status.LifecycleStatus.Title;
                    ctContainerTypeModel.Timestamp = status.Timestamp;
                }
                //ctContainerTypeModel.LifecycleStatus = status.LifecycleStatus?.Title;
                else if (tray.GCLifecycleStatusType != null)
                {
                    ctContainerTypeModel.LifecycleStatus = tray.GCLifecycleStatusType.Title;
                    ctContainerTypeModel.Timestamp = status.Timestamp;
                }
                ctContainerTypeModel.IsGCTrayActiveStatus = (tray.IsActiveStatus.HasValue && tray.IsActiveStatus.Value);
                ctContainerTypeModel.ColorStatus = _SoxService.GetTrayColorStatus(tray.Id);
                ctContainerTypeModel.CTContainerTypeActualId = tray.CTContainerTypeActualId ?? 0;
                ctContainerTypeModel.ActualLapCount = tray.ActualLapCount;
                ctContainerTypeModel.LifecycleCategory = tray.GCLifecycleStatusType?.GCLifecycleStatusTypeCategory?.Title;

                var actual = _SoxService.GetCTContainerTypeActualById(ctContainerTypeModel.CTContainerTypeActualId);

                if (tray.CTCaseContainer != null)
                {
                    ctContainerTypeModel.Name = tray.CTCaseContainer.ContainerName != null ? tray.CTCaseContainer.ContainerName /*+ " " + actual.Name*/ : null;
                    ctContainerTypeModel.VendorName = tray.CTCaseContainer.VendorName != null ? tray.CTCaseContainer.VendorName : null;
                }
                //find current tray location
                if (status.LifecycleStatus != null)
                {
                    var currentStatus = (LifecycleStatusType)Enum.Parse(typeof(LifecycleStatusType), status.LifecycleStatus?.Code.ToString());
                    var currentLocation = _SoxService.GetTraysPhisicalLocation(currentStatus);
                    switch (currentLocation)
                    {
                        case TrayPhisicalLocationType.VM:
                            ctContainerTypeModel.Location = "VestedMedical";
                            break;
                        case TrayPhisicalLocationType.Vehicle:
                            ctContainerTypeModel.Location = "Vehicle/In Transit";
                            break;
                        case TrayPhisicalLocationType.Provider:
                            ctContainerTypeModel.Location = "Provider";
                            break;
                        default:
                            ctContainerTypeModel.Location = "No data";
                            break;
                    }
                }
                else
                {
                    ctContainerTypeModel.Location = "No data";
                }

                ctContainersTypes.Add(ctContainerTypeModel);
            }
            
            model.Trays = new List<TrayModel>();

            for (var i = 0; i < ctContainersTypes.Count(); i++)
            {
                var mappedtray = mapper.Map<TrayModel>(ctContainersTypes[i]);
                model.Trays.Add(mappedtray);

            }

            List<GCColorStatusType> colorStatuses = _SoxService.GetCaseColorStatusTypes();

            // for MVP, BLUE is not available:
            if (colorStatuses != null)
            {
                var blueColor = colorStatuses.Where(x => x.Name.Equals("Blue")).FirstOrDefault();
                if (blueColor != null)
                {
                    colorStatuses.Remove(blueColor);
                }
            }

           
           
            if (model != null && model.Trays != null && model.Trays.Count > 0)
            {
                model.AssemblyPhotos = new List<TrayAssemblyPhotos>();
                model.LatestBIResults = new List<TrayHistorySterilizeBIResultPhotos>();
                model.LatestDeliveryPods = new List<TrayHistoryDeliveryPhotos>();
                model.NumOfPhotos = 0;
                
                var trayHistory = PrepareTrayHistoryModelForVendors(trayActualId); //this is for a single tray so there should be only one tray in a list
                if (trayHistory.TrayAssemblyPhotos != null && trayHistory.TrayAssemblyPhotos.AssemblyPhotos != null && trayHistory.TrayAssemblyPhotos.AssemblyPhotos.Pictures != null && trayHistory.TrayAssemblyPhotos.AssemblyPhotos.Pictures.Count > 0)
                {
                    model.AssemblyPhotos.Add(trayHistory.TrayAssemblyPhotos);
                }
                if (trayHistory.LatestBIResults != null && trayHistory.LatestBIResults.ImagesData != null && trayHistory.LatestBIResults.ImagesData.Count > 0)
                {
                    model.LatestBIResults.Add(trayHistory.LatestBIResults);
                }
                if (trayHistory.LatestDeliveryPods != null && trayHistory.LatestDeliveryPods.PodContainer != null && trayHistory.LatestDeliveryPods.PodContainer.ShowPictures && trayHistory.LatestDeliveryPods.PodContainer.Pictures != null && trayHistory.LatestDeliveryPods.PodContainer.Pictures.Count > 0)
                {
                    model.LatestDeliveryPods.Add(trayHistory.LatestDeliveryPods);
                }
                model.NumOfPhotos += trayHistory.NumOfPhotos;
                
            }
            

            return model;
        }
    

        public ProposedTrayLCHistoryModel PrepareProposedHistoryItemsForLC(int trayId, int lifecycleId, int trayLifecycleId)
        {
            ProposedTrayLCHistoryModel result = new ProposedTrayLCHistoryModel();
            result.GCTrayLifeCycleId = trayLifecycleId;
            result.HistoryItemSelectList = new List<SelectListItem>();

            var gcTray = _SoxService.GetTrayById(trayId);
            if(gcTray != null && gcTray.CTContainerTypeActualId.HasValue)
            {
                result.GCTrayId = gcTray.Id;

                if(gcTray.CTContainerTypeActual != null)
                {
                    result.ActualName = $"{gcTray.CTContainerTypeActual.ParentName} {gcTray.CTContainerTypeActual.Name}";
                }

                List<CTContainerTypeActualHistoryItem> hiItems = _groundControlService.GetActualHistoryItems(gcTray.CTContainerTypeActualId.Value, lifecycleId);
                if(hiItems != null && hiItems.Count > 0)
                {
                    DateTime? lcTimestamp = null;
                    GCTraysLifecycleStatusTypes lc = _groundControlService.GetGCTraysLifeCycleForId(trayLifecycleId);
                    if(lc != null)
                    {
                        lcTimestamp = lc.Timestamp;

                        var currentHI = hiItems.Where(x => x.UpdateTimestamp == lc.Timestamp).FirstOrDefault();
                        if(currentHI != null)
                        {
                            result.SelectedHI = currentHI.Id;
                        }
                    }

                    //result.HistoryItems = mapper.Map<List<CTContainerTypeActualHistoryItemModel>>(hiItems);
                    result.HistoryItemSelectList = hiItems.Select(x =>
                    new SelectListItem()
                    {
                        Value = x.Id.ToString(),
                        Selected = x.UpdateTimestamp == lcTimestamp,
                        Text = $"{x.LocationElapsedCase} | {x.UpdateTimestamp.GetLocalDateTime()} | {x.UpdateUserId}"
                    }).ToList();
                }

                result.HistoryItemSelectList.Insert(0, new SelectListItem() { Value = "0", Selected = (trayLifecycleId == 0), Text = "Please select"  });

                result.GCLifeCycleId = lifecycleId;
                var lifeCycle = _SoxService.GetLifecycleStatusById(lifecycleId);
                if(lifeCycle != null)
                {
                    result.GCLifeCycleName = lifeCycle.Title;
                }

                if (gcTray.GCCaseId.HasValue)
                {
                    var gcCase = _SoxService.GetCaseById(gcTray.GCCaseId.Value);
                    if(gcCase != null)
                    {
                        result.DueTimestamp = gcCase.DueTimestamp;
                    }
                }
            }
            return result;
        }

        public GCSystemAlertsModel PrepareSystemAlerts(ClaimsPrincipal user)
        {
            var model = new GCSystemAlertsModel();
            model.Alerts = new List<GCSystemAlertModel>();

            List<GCTray> trays = new List<GCTray>();
            
            // Is this ever going to be called? Customer/Vendor user might never get to see alerts page
            if (user.IsInRole(ApplicationRoleNames.CustomerUser) || user.IsInRole(ApplicationRoleNames.VendorUser))
            {
                var cases = _SoxService.FilterCasesByUserRole(user).ToList();
                if (cases != null && cases.Count > 0)
                {
                    trays = _SoxService.GetTraysForGCCaseIds(cases.Select(x => x.Id).ToList());
                }
            }
            else
            {
                trays = _SoxService.GetAllGCTrays();
            }

            if (trays != null && trays.Count > 0)
            {
                List<GCSystemAlert> alerts = _SoxService.GetSystemAlerts(trays.Select(x => x.Id).ToList());
                if (alerts != null && alerts.Count > 0)
                {
                    model.Alerts = mapper.Map<List<GCSystemAlertModel>>(alerts.OrderByDescending(x => x.CreatedAtUtc));
                }
            }

            return model;
        }

        #endregion

    }
}