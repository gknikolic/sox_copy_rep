﻿namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayImage
	{
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsAssembly { get; set; }
        public bool IsDecontam { get; set; }
        public bool IsSterilization { get; set; }
        public string ImageDataUrl { get; set; }
    }
}
