﻿namespace ProfitOptics.Framework.Etl.Models
{
    public class DbTableStatus
    {
        public string TableName { get; set; }
        public string ConstraintBroken { get; set; }
        public int RecordsThatBrokeConstraintCount { get; set; }
    }
}
