﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportHierarchyRegionConfig : IEntityTypeConfiguration<ReportHierarchyRegion>
    {
        public void Configure(EntityTypeBuilder<ReportHierarchyRegion> builder)
        {
            builder.ToTable(nameof(ReportHierarchyRegion));
            builder.HasKey(r => r.Id);

            builder.Property(r => r.Name).HasMaxLength(50).IsRequired();
            builder.Property(r => r.StartDate).IsRequired();

            builder.HasOne(r => r.ReportHierarchyZone)
                .WithMany(z => z.ReportHierarchyRegions)
                .HasForeignKey(r => r.ZoneId)
                .IsRequired(false);

            builder.HasMany(r => r.ReportHierarchySalesReps)
                .WithOne(s => s.ReportHierarchyRegion)
                .HasForeignKey(s => s.RegionId)
                .IsRequired(false);
        }
    }
}
