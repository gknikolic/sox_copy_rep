﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels
{
    public class RsmPerformanceSalesRepModel
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int SalesRepId { get; set; }
        public string SalesRepName { get; set; }
        public string MonthName { get; set; }
        public int RemainingWorkDaysCount { get; set; }
    }
}