﻿using System;

namespace ProfitOptics.Modules.ManualEtl.Models
{
    public class ManualUploadFileTypeModel
    {
        public string FileTypeId { get; set; }
        public string FileTypeName { get; set; }
        public DateTime? LastImportTime { get; set; }
        public string LastImportUsername { get; set; }
        public int? RecordCount { get; set; }

        public static ManualUploadFileTypeModel Default(string fileTypeId, string fileTypeName) => new ManualUploadFileTypeModel
        {
            FileTypeId = fileTypeId,
            FileTypeName = fileTypeName,
            LastImportTime = null,
            LastImportUsername = string.Empty,
            RecordCount = null
        };
    }
}