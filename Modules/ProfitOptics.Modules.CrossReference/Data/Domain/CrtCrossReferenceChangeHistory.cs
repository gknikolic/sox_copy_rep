using System;

namespace ProfitOptics.Modules.CrossReference.Data.Domain
{
    public partial class CrtCrossReferenceChangeHistory
    {
        public int Id { get; set; }

        public int CrossReferenceId { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public int CompetitorId { get; set; }

        public string CompetitorItemNumber { get; set; }

        public string CompetitorItemDescription { get; set; }

        public string CompetitorWebsiteUrl { get; set; }

        public int ItemId { get; set; }

        public int? CompatibilityOptionId { get; set; }

        public string Notes { get; set; }

        public bool Approved { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
