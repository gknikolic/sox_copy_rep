﻿// You have to implement the following  on every page you want to use this widget on:
//
// --> page.onApplyFilterClicked
// --> page.reloadTablesAndCharts
//
// This is because every page might have unique charts and tables to display, so these functions may vary every time.

$(function () {
    (function (page, $, undefined) {
        page.updateFilterTitles = function () {
            page.updateZoneFilterTitles();
            page.updateRegionFilterTitles();
            page.updateCustomerClassFilterTitles();
            page.updateBillToFilterTitles();
            page.updateShipToFilterTitles();
            page.updateSalesRepFilterTitles();
            page.updateVendorFilterTitles();
            page.updateItemCategoryFilterTitles();
            page.updateBuyGroupFilterTitles();
        };

        page.updateZoneFilterTitles = function () {
            updateFilterTitles(".zoneFilterTitle", "zone[]");
        };

        page.updateRegionFilterTitles = function () {
            updateFilterTitles(".regionFilterTitle", "region[]");
        };

        page.updateCustomerClassFilterTitles = function () {
            updateFilterTitles(".customerClassFilterTitle", "customerClass[]");
        };

        page.updateBillToFilterTitles = function () {
            updateFilterTitles(".billToFilterTitle", "billTo[]");
        };

        page.updateShipToFilterTitles = function () {
            updateFilterTitles(".shipToFilterTitle", "shipTo[]");
        };

        page.updateSalesRepFilterTitles = function () {
            updateFilterTitles(".salesRepFilterTitle", "salesRep[]");
        };

        page.updateVendorFilterTitles = function () {
            updateFilterTitles(".vendorFilterTitle", "vendor[]");
        };

        page.updateItemCategoryFilterTitles = function () {
            updateFilterTitles(".itemCategoryFilterTitle", "itemCategory[]");
        };

        page.updateBuyGroupFilterTitles = function () {
            updateFilterTitles(".buyGroupFilterTitle", "buyGroup[]");
        };

        function updateFilterTitles(filterTitleSelector, filterName) {
            if ($("#clearFilters").val() === "true") {
                $(filterTitleSelector).html("All");
                $("#clearFilters").val("false");
                return;
            }

            var selectedOptions = $("select[name='" + filterName + "'] option:selected");

            $(filterTitleSelector).html(selectedOptions.length === 1
                ? selectedOptions.text()
                : selectedOptions.length === 0
                    ? "All"
                    : "Many");
        }

        page.saveOrUpdateUserFilter = function (filterName, callback) {
            const data = {
                Name: filterName,
                FilterZone: $("select[name='zone[]']").val(),
                FilterRegion: $("select[name='region[]']").val(),
                FilterCustomerClass: $("select[name='customerClass[]']").val(),
                FilterBillTo: $("select[name='billTo[]']").val(),
                FilterShipTo: $("select[name='shipTo[]']").val(),
                FilterSalesRep: $("select[name='salesRep[]']").val(),
                FilterVendor: $("select[name='vendor[]']").val(),
                FilterItemCategory: $("select[name='itemCategory[]']").val(),
                FilterBuyGroup: $("select[name='buyGroup[]']").val()
            };

            app.AddOrUpdateUserFilter(data).done(function (returnData) {
                if (!returnData.success) {
                    alert("Failed to save user filter!");

                    return;
                }

                if (filterName !== "") {
                    $("#loadFilterModal").find("tbody").append(
                        $("<tr />").attr("data-filterId", returnData.filterId)
                            .append($("<td />").text(filterName).addClass("filter-name"))
                            .append($("<td />").addClass("filter-actions text-center")
                                .append($("<button />").text(" Load").addClass("btn btn-default btn-xs load-user-filter-btn")
                                    .prepend($("<span />").addClass("glyphicon glyphicon-download")))
                                .append($("<button />").text(" Rename")
                                    .addClass("btn btn-default btn-xs rename-user-filter-btn")
                                    .prepend($("<span />").addClass("glyphicon glyphicon-pencil")))
                                .append($("<button />").text(" Delete")
                                    .addClass("btn btn-default btn-xs delete-user-filter-btn")
                                    .prepend($("<span />").addClass("glyphicon glyphicon-trash")))
                            ));
                }

                if (typeof callback !== 'undefined' && callback !== null) {
                    callback();
                }

                $("#saveFilterModal").modal("hide");
            });
        };

        page.loadUserFilter = function (filter, filterData) {
            if (filterData === undefined || filterData === null || filterData.length <= 0) {
                return;
            }

            for (var i = 0, len = filterData.length; i < len; i++) {
                if (filter.find("option[value='" + filterData[i].id + "']").length) {
                    filter.val(filterData[i].id).trigger('change');
                } else {
                    const newOption = new Option(filterData[i].text, filterData[i].id, true, true);

                    filter.append(newOption).trigger('change');
                }
            }
        };

    }(window.page = window.page || {}, jQuery));

    (function () {
        page.updateFilterTitles();

        $("#apply-filter-btn").unbind();

        $("#apply-filter-btn").on("click", function () {
            page.saveOrUpdateUserFilter("", function () {
                page.onApplyFilterClicked();
            });
        });

        $("#clear-filter-btn").unbind();

        $("#clear-filter-btn").on("click", function () {
            $("#clearFilters").val(true);

            $(".dynamic-select2").val(null).trigger("change");
        });

        page.loadDefaultFilters = function () {
            app.LoadDefaultFilters().done(function (data) {
                loadUserFilters(data);
            });
        };

        $("#user-filters-tbl").on("click", ".load-user-filter-btn", function (event) {
            let $this = $(event.target);

            var customFilterId = $this.parents("tr").attr("data-filterId");

            var submitData = {
                userFilterId: customFilterId
            };

            app.LoadUserFilter(submitData).done(function (data) {
                loadUserFilters(data);

                $("#loadFilterModal").modal("hide");

                page.saveOrUpdateUserFilter("", function () {
                    page.onApplyFilterClicked();
                });
            });
        });

        function loadUserFilters(data) {
            page.loadUserFilter($("select[name='zone[]']"), data.filters.FilterZone);
            page.loadUserFilter($("select[name='region[]']"), data.filters.FilterRegion);
            page.loadUserFilter($("select[name='customerClass[]']"), data.filters.FilterCustomerClass);
            page.loadUserFilter($("select[name='billTo[]']"), data.filters.FilterBillTo);
            page.loadUserFilter($("select[name='shipTo[]']"), data.filters.FilterShipTo);
            page.loadUserFilter($("select[name='salesRep[]']"), data.filters.FilterSalesRep);
            page.loadUserFilter($("select[name='itemCategory[]']"), data.filters.FilterItemCategory);
            page.loadUserFilter($("select[name='vendor[]']"), data.filters.FilterVendor);
            page.loadUserFilter($("select[name='buyGroup[]']"), data.filters.FilterBuyGroup);
        }

        $("#user-filters-tbl").on("click", ".rename-user-filter-btn", function (event) {
            let $this = $(event.target);

            var parentRow = $this.parents("tr");

            var filterToRename = parentRow.find(".filter-name");

            parentRow.attr("data-rename", true);

            filterToRename.attr("contenteditable", true).focus();
        });

        $("#loadFilterModal").on("click", "#save-filters-btn", function () {
            var submitData = [];

            var rowsToRename = $("#loadFilterModal").find("tr[data-rename]");

            $.each(rowsToRename, function (k, v) {
                submitData.push({
                    Id: $(v).attr("data-filterId"),
                    Name: $(v).find("td.filter-name").html()
                });
            });

            if (submitData.length === 0) {
                return;
            }

            app.BulkRenameUserFilters({
                filtersToRename: submitData
            }).done(function (returnData) {
            });
        });

        $("#user-filters-tbl").on("blur", ".filter-name", function (event) {
            let $this = $(event.target);

            $this.removeAttr("contenteditable");
        });

        $("#user-filters-tbl").on("click", ".delete-user-filter-btn", function (event) {
            let $this = $(event.target);

            var rowToDelete = $this.parents("tr");

            var filterToDeleteId = rowToDelete.attr("data-filterId");

            var submitData = { userFilterId: filterToDeleteId };

            bootbox.confirm("Are you sure you want to delete the filter?", function (result) {
                if (result === true) {
                    app.DeleteUserFilter(submitData).done(function (returnData) {
                        if (returnData.success === true) {
                            rowToDelete.remove();
                        } else {
                            alert("Could not delete the filter, please try again later.");
                        }
                    });
                }
            });
        });

        $("#saveFilterModal").on("click", "#save-user-filter-btn", function () {
            var filterName = $("#saveFilterModal").find("input[name='userFilterName']").val();

            page.saveOrUpdateUserFilter(filterName, null);
        });

        $(".select2-no-close").select2({
            width: 'resolve',
            closeOnSelect: false
        });

        const take = 30;

        $(".dynamic-select2").select2({
            minimumInputLength: 0,
            ajax: {
                url: $("#getFilterOptionsUrl").val(),
                dataType: "JSON",
                data: function (params) {
                    const query = {
                        filterType: $(this).data("filter-type"),
                        search: params.term,
                        page: params.page || 1,
                        take: take
                    };

                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.results,
                        pagination: {
                            more: params.page * take < data.count_filtered
                        }
                    };
                },
                delay: 500
            }
        });
    })();
});