﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class ManageLoginsModel
    {
        public ManageLoginsModel()
        {
            CurrentLogins = new List<ExternalLoginManageInfo>();
            OtherLogins = new List<ExternalLoginProviderModel>();
        }
        public string StatusMessage { get; set; }
        public IList<ExternalLoginManageInfo> CurrentLogins { get; set; }
        public IList<ExternalLoginProviderModel> OtherLogins { get; set; }
        public bool ShowRemoveButton { get; set; }
        public string LinkLoginCallbackUrl { get; set; }
    }
}
