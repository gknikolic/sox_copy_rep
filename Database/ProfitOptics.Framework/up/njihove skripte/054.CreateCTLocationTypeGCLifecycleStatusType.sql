USE [Sox]

BEGIN
  
  EXEC sp_rename '[dbo].[LifecycleStatusType]', 'GCLifecycleStatusType';

  CREATE TABLE [dbo].[CTLocationTypesGCLifecycleStatusTypes]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[GCLifecycleStatusTypeId] int,
    [CTLocationTypeId] int,
	FOREIGN KEY ([GCLifecycleStatusTypeId]) REFERENCES [dbo].[GCLifecycleStatusType] ([Id]),
	FOREIGN KEY ([CTLocationTypeId]) REFERENCES [dbo].[CTLocationType] ([Id])
  )

END