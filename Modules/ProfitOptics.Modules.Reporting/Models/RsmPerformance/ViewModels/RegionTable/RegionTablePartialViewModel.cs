﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.RegionTable
{
    public class RegionTablePartialViewModel
    {
        public RegionTablePartialViewModel()
        {
            RegionQuotaPerformanceData = new List<RegionQuotaPerformanceDataModel>();
        }

        public int PreviousYear { get; set; }

        public int CurrentYear { get; set; }

        public int CurrentQuarter { get; set; }

        public string CurrentMonth { get; set; }

        public List<RegionQuotaPerformanceDataModel> RegionQuotaPerformanceData { get; set; }
    }
}