﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.CrossReference.Data.Domain;

namespace ProfitOptics.Modules.CrossReference.Data.Config
{
    public class CrtCrossReferenceVoteConfig : IEntityTypeConfiguration<CrtCrossReferenceVote>
    {
        public void Configure(EntityTypeBuilder<CrtCrossReferenceVote> builder)
        {
            builder.ToTable("CrtCrossReferenceVotes");
            builder.HasKey(entity => entity.Id);
        }
    }
}
