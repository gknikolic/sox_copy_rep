﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Employee.Models
{
    public class ManagerModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }

    }
}
