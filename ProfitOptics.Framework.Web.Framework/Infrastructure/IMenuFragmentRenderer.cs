﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Core.Settings;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    /// <summary>
    /// Represents the menu fragment renderer interface.
    /// </summary>
    public interface IMenuFragmentRenderer
    {
        /// <summary>
        /// Validates the menu fragment renderer instance.
        /// </summary>
        /// <param name="settings">The common settings.</param>
        /// <param name="user">The current user.</param>
        /// <returns>True if the menu fragment renderer is valid, otherwise false.</returns>
        bool Validate(Common settings, ClaimsPrincipal user);

        /// <summary>
        /// Return the menu fragment asynchronously.
        /// </summary>
        /// <param name="htmlHelper">The html helper instance used for creating the menu fragment.</param>
        /// <returns>The html content representing the menu fragment.</returns>
        Task<IHtmlContent> RenderMenuFragmentAsync(IHtmlHelper htmlHelper);

        /// <summary>
        /// Gets the order in which the menu fragment renderer will be called.
        /// </summary>
        /// <remarks>
        /// The menu fragment renderers are ordered in ascending order based on the value of this property.
        /// </remarks>
        int Order { get; }
    }
}
