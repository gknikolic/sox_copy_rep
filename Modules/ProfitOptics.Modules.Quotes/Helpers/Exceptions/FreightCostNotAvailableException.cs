﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;

namespace ProfitOptics.Modules.Quotes.Helpers.Exceptions
{
    [Serializable]
    public class FreightCostNotAvailableException : Exception
    {
        public FreightCostNotAvailableException(string message) : base(message) { }
    }
}