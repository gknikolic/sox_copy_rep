﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCProductConfig : IEntityTypeConfiguration<GCProduct>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCProduct> builder)
        {
            builder.ToTable("GCProduct");
            builder.HasKey(entity => entity.Id);
            builder.HasOne(entity => entity.ProductType)
               .WithMany(p => p.Products)
               .HasForeignKey(entity => entity.ProductTypeId);
            builder.HasOne(entity => entity.Container)
              .WithMany(p => p.Products)
              .HasForeignKey(entity => entity.ContainerId);
        }
    }
}
