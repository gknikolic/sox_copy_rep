﻿using ProfitOptics.Modules.Reporting.Attributes;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge.ImportExportData
{
    public class LocationHierarchyIE
    {

        [FieldTitle("Location Level 1")]
        public string LocationLevel1 { get; set; }

        [FieldTitle("Location Level 2")]
        public string LocationLevel2 { get; set; }

        [FieldTitle("Location Level 3")]
        public string LocationLevel3 { get; set; }

        [FieldTitle("Location Level 4")]
        public string LocationLevel4 { get; set; }

        [FieldTitle("Location Level 5")]
        public string LocationLevel5 { get; set; }

        [FieldTitle("Location Level 6")]
        public string LocationLevel6 { get; set; }

        [FieldTitle("Location Name")]
        public string LocationName { get; set; }

        public object this[string propertyName]
        {
            get => GetType().GetProperty(propertyName)?.GetValue(this, null);
            set => GetType().GetProperty(propertyName)?.SetValue(this, value, null);
        }
    }
}