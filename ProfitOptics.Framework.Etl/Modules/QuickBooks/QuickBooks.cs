﻿using CommandLine;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Etl.Helpers;
using ProfitOptics.Framework.Etl.Models;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Models.CensiTrack.ETL;
using Serilog;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Etl.Modules.QuickBooks
{
    [Verb(ProfitOptics.Modules.Sox.Helpers.Constants.ETL_QB_MODULE_NAME, HelpText = "Commands for QuickBooks integration.")]
    public class QuickBooksModuleOptions : IOptions
    {
        [Option('a', "action", Required = true, HelpText = "Action type Enumerated value of type: QBActionType.")]
        public string Action { get; set; }

        [Option('u', "username", Required = true, HelpText = "Login Username.")]
        public string UserName { get; set; }
    }

    public class QuickBooksModule : IModule
    {
        private readonly IEtlHelper _etlHelper;
        private readonly IQBService _quickBooksService;

        public QuickBooksModule(IEtlHelper etlHelper, IQBService quickBooksService)
        {
            _etlHelper = etlHelper;
            _quickBooksService = quickBooksService;
        }

        public int Execute(IOptions opts)
        {
            var qbOpts = opts as QuickBooksModuleOptions;
            QBActionType qbActionType = (QBActionType)(int.Parse(qbOpts.Action));

            ETLLogs etlLog = new ETLLogs();
            etlLog.Source = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_QB_MODULE_NAME;
            etlLog.FileName = "no file";
            etlLog.Username = "no username";
            etlLog.Status = ETLStatusType.OK.ToString();
            etlLog.Message = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE;
            etlLog.RowCount = 0;
            etlLog.ActionType = (int)qbActionType;
            etlLog.Options = $@"Action: {qbOpts.Action}; UserName: {qbOpts.UserName}";
            etlLog.Description = string.Empty;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            try
            {
                switch (qbActionType)
                {
                    case QBActionType.PostGCTraysToQBEstimates:
                        ETLResponseModel postGCTraysToQBEstimatesResponse = PostGCTraysToQBEstimates(qbOpts).GetAwaiter().GetResult();
                        if (!string.IsNullOrWhiteSpace(postGCTraysToQBEstimatesResponse.Message))
                        {
                            etlLog.Status = ETLStatusType.Error.ToString();
                            etlLog.Message = postGCTraysToQBEstimatesResponse.Message;
                        }
                        etlLog.RowCount = postGCTraysToQBEstimatesResponse.RowCount;
                        etlLog.Description = postGCTraysToQBEstimatesResponse.Description;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                etlLog.Status = ETLStatusType.Error.ToString();
                etlLog.Message = e.Message;
                etlLog.Description = e.StackTrace;
            }
            finally
            {
                stopwatch.Stop();
                etlLog.Description += $" | Time elapsed: { stopwatch.Elapsed }";
                etlLog.ProcessingTime = DateTime.UtcNow;
                _etlHelper.LogProcessedAction(etlLog.Username, etlLog.Source, etlLog.FileName, etlLog.ProcessingTime.ToString(), etlLog.Status, etlLog.RowCount, etlLog.Message, etlLog.ActionType, etlLog.Options, etlLog.Description);
            }

            return 0;
        }

        private async Task<ETLResponseModel> PostGCTraysToQBEstimates(QuickBooksModuleOptions qbOpts)
        {
            Log.Information("PostGCTraysToQBEstimates - start.");
            var result = new ETLResponseModel();

            result.Description = _quickBooksService.PostGCTraysToQBEstimates(qbOpts.UserName);

            Log.Information("PostGCTraysToQBEstimates - end.");
            return result;
        }
    }
}
