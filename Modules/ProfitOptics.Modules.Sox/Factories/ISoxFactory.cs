﻿using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Factories
{
    public interface ISoxFactory
    {
        //EmptyModel PrepareEmptyModel();
        GCCasesModel PrepareGCCasesModel(GCCasesModel filterModel);
        GCCasesModel PrepareGCCasesModelForCaseSchedule(GCCasesModel filterModel);
        GCCasesModel PrepareCasesForPlanning(DateTime date, ClaimsPrincipal user, string caseReference, int? vehicleId, int? driverId);
        DashboardModel PrepareDashboardModel(DashboardModel filterModel);

        EventDetailsModel PrepareEventPreviewModel(DateTime date, ClaimsPrincipal user);
        TraysModel PrepareTraysModel(ClaimsPrincipal user, int? actualId = null);
        GCCustomersModel PrepareGCCustomersModel();
        GCVendorsModel PrepareGCVendorsModel();

        CaseDetailsModel PrepareCaseDetailsModel(int id, ClaimsPrincipal user);

        TrayHistoryModel PrepareTrayHistoryModel(int trayId);
        TrayHistoryModel PrepareTrayHistoryModelForVendors(int trayId);
        TraysModel PrepareTarysForCaseModel(int caseId);
        GCCustomerModel PrepareGCCustomerModel(int customerId);
        GCVendorModel PrepareGCVendorModel(int vendorId);
        ProcedureTypesModel PrepareProcedureTypesModel(bool? includeInactive = false);
        ProcedureTypeModel PrepareProcedureTypeModel(int procedureTypeId);
        ProceduresModel PrepareProceduresModel();
        ProcedureModel PrepareProcedureModel(int procedureId);
        DashboardModel PrepareNewDashboardModel(DashboardModel filterModel);
        DashboardHourPreviewModel PrepareDashboardHourPreviewModel(DashboardModel filterModel);
        DashboardDetailedHourPreviewModel PrepareDashboardDetailedHourPreviewModel(DashboardModel filterModel);
        GCCaseModel PrepareDashboardCaseDetailsPreviewModel(int caseId);
        InventoryAlerStatusModel PrepareInventoryAlertStatusModel(ClaimsPrincipal user);
        ProposedTrayLCHistoryModel PrepareProposedHistoryItemsForLC(int trayId, int lifecycleId, int trayLifecycleId);
        CaseDetailsTraysModel PrepareTrayDetailsForVendorModel(int actualId);
        GCSystemAlertInfoModel PrepareSystemAlertsStatusModel(ClaimsPrincipal user);
        GCSystemAlertsModel PrepareSystemAlerts(ClaimsPrincipal user);
    }
}