﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models
{
	public class CheckTrayBarcodeModel
	{
		public BarcodeResultType BarcodeResultType { get; set; }
		public List<SimpleTrayModel> Trays { get; set; }
		public int GCTrayId { get; set; }
	}
}
