﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.Anotations;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Showcase.Factories;
using ProfitOptics.Modules.Showcase.Models;
using Rotativa.NetCore;
using Rotativa.NetCore.Options;

namespace ProfitOptics.Modules.Showcase.Controllers
{
    [Authorize]
    [Area("Showcase")]
    public class MiscController : BaseController
    {
        private readonly IShowcaseFactory _showcaseFactory;

        public MiscController(IShowcaseFactory showcaseFactory)
        {
            _showcaseFactory = showcaseFactory;
        }

        public IActionResult Index()
        {
            var dummyData = _showcaseFactory.GetSelect2DemoData();

            var model = new MiscViewModel
            {
                // Here we are using attributes defined on the classes properites to specify the mapping
                DropdownData = dummyData.ToSelect2ListFromModel()
                //DropdownData = dummyData.ToSelect2Enumerable().ToList()

                // We can use delegates to map specific properties
                // Very flexible approach
                //DropdownData = dummyData.ToSelect2List(x=> $"{x.StringProperty2} { x.StringProperty3}", x=> x.StringProperty4)
                //DropdownData = dummyData.ToSelect2Enumerable(x=> $"{x.StringProperty2} { x.StringProperty3}", x=> x.StringProperty4).ToList()
            };

            return View(model);
        }

        public IActionResult GetDropdownDelegateVariant(string search, int page, string filterName)
        {
            var data = _showcaseFactory.GetSelect2DemoData(search, page, filterName);

            // Delegate mapping
            Select2PagedResult result = data.ToSelect2PagedResultFromModel(x => x.StringProperty1, x => x.StringProperty4, page * 10);

            return Json(result);
        }

        public IActionResult GetDropdownAttributeVariant(string search, int page, string filterName)
        {
            var data = _showcaseFactory.GetSelect2DemoData(search, page, filterName);

            // Attribute mapping
            Select2PagedResult result = data.ToSelect2PagedResultFromModel(page * 10);

            return Json(result);
        }

        [HttpPost]
        [AjaxCall]
        public IActionResult AjaxShowcase(int id, string name)
        {
            var ids = "(" + id + ")";
            name = name.ToUpper();
            return Json(new { result = "Success: " + ids + " - " + name });
        }

        [HttpPost]
        [AjaxCall]
        public IActionResult AjaxError()
        {
            return new StatusCodeResult(500);
        }

        [HttpPost]
        [AjaxCall]
        public async Task<IActionResult> AjaxLogoff()
        {
            await HttpContext.SignOutAsync("Identity.Application");
            return Json(new { Status = "OK" });
        }

        public IActionResult GetURLAsPDF()
        {
            // In some cases you might want to pull completely different URL that is not related to your application.
            // You can do that by specifying full URL.

            return new UrlAsPdf("http://www.github.com")
            {
                FileName = "TestExternalUrl.pdf",
                PageMargins = new Margins(0, 0, 0, 0)
            };
        }
    }
}