﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class DashboardDetailedHourPreviewModel
	{
		public DateTime Date { get; set; }
		public List<DashboardDetailedHourPreviewItemModel> Items { get; set; }
	}


}
