﻿using Newtonsoft.Json;
using System;

namespace ProfitOptics.Modules.Sox.Models
{
	public partial class MonnitSensor
    {
        [JsonProperty("SensorID")]
        public long SensorId { get; set; }

        [JsonProperty("ApplicationID")]
        public long ApplicationId { get; set; }

        [JsonProperty("CSNetID")]
        public long CsNetId { get; set; }

        [JsonProperty("SensorName")]
        public string SensorName { get; set; }

        [JsonProperty("LastCommunicationDate")]
        public string LastCommunicationDate { get; set; }

        [JsonProperty("NextCommunicationDate")]
        public string NextCommunicationDate { get; set; }

        [JsonProperty("LastDataMessageMessageGUID")]
        public Guid LastDataMessageMessageGuid { get; set; }

        [JsonProperty("PowerSourceID")]
        public long PowerSourceId { get; set; }

        [JsonProperty("Status")]
        public long Status { get; set; }

        [JsonProperty("CanUpdate")]
        public bool CanUpdate { get; set; }

        [JsonProperty("CurrentReading")]
        public string CurrentReading { get; set; }

        [JsonProperty("BatteryLevel")]
        public long BatteryLevel { get; set; }

        [JsonProperty("SignalStrength")]
        public long SignalStrength { get; set; }

        [JsonProperty("AlertsActive")]
        public bool AlertsActive { get; set; }

        [JsonProperty("CheckDigit")]
        public string CheckDigit { get; set; }

        [JsonProperty("AccountID")]
        public long AccountId { get; set; }

        [JsonProperty("MonnitApplicationID")]
        public long MonnitApplicationId { get; set; }
    }
}
