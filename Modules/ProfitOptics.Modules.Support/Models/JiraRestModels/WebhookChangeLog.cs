﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class WebhookChangeLog
    {
        public string id { get; set; }

        public List<WebhookChangelogItem> items { get; set; }
    }
}