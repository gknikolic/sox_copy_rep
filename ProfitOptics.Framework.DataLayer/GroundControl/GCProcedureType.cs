﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCProcedureType
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Abbreviation { get; set; }
        public int TrayCount { get; set; }
        public decimal ProcedurePriceOEM { get; set; }
        public int? CappedTrayNumber { get; set; }
        public decimal? TrayPriceAboveCap { get; set; }
        public decimal ProcedurePriceCustomer { get; set; }
        public bool IsActive { get; set; }
        public string ICD9 { get; set; }
        public string ICD10 { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
    }
}
