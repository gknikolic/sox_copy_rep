using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTBillToPriceList")]
    public partial class QTBillToPriceList
    {
        public string Company { get; set; }

        public int BillToNum { get; set; }

        public string ShipToNum { get; set; }

        public int? SeqNum { get; set; }

        public string PriceListCode { get; set; }

        public string PriceListDescription { get; set; }
    }
}
