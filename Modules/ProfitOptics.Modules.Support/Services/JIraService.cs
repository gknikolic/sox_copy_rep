﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Modules.Support.Extensions;
using ProfitOptics.Modules.Support.Models.JiraRestModels;
using ProfitOptics.Modules.Support.Models.ViewModels;
using RestSharp;
using RestSharp.Authenticators;
using CreateIssueViewModel = ProfitOptics.Modules.Support.Models.ViewModels.CreateIssueViewModel;
using Issue = Atlassian.Jira.Issue;
using IssuePriority = Atlassian.Jira.IssuePriority;
using IssueType = Atlassian.Jira.IssueType;
using JiraUser = Atlassian.Jira.JiraUser;

namespace ProfitOptics.Modules.Support.Services
{
    public class JiraService : IJiraService
    {
        #region Fields

        private readonly Jira _jira;
        private readonly SupportSettings _settings;

        #endregion Fields

        #region Ctor

        public JiraService(SupportSettings settings)
        {
            if (_jira == null)
            {
                _jira = Jira.CreateRestClient(settings.SupportJiraUrlBasic, settings.SupportJiraUsername, settings.SupportJiraPassword);
            }

            _settings = settings;
        }

        #endregion Ctor

        #region Methods

        public async Task<Issue> CreateJiraIssueAsync(CreateIssueViewModel model, string creator)
        {
            var issue = _jira.CreateIssue(model.Project, model.ParentIssueKey);
            issue.Type = model.Type;
            issue.Priority = model.Priority;
            issue.Summary = model.Summary;

            if (model.Type == "Epic")
            {
                issue.CustomFields.Add("Epic Name", model.EpicName);
            }

            model.Description += $" (Created by {creator})";
            issue.Description = model.Description;
            

            if (model.Component != null)
            {
                issue.Components.Add(model.Component);
            }

            issue.Environment = model.Environment;
            issue.Reporter = _settings.SupportJiraUsername;

            Issue result = await issue.SaveChangesAsync();

            if (model.Attachments != null)
            {
                foreach (var att in model.Attachments)
                {
                    if (att != null)
                    {
                        var target = new MemoryStream();
                        att.CopyTo(target);
                        result.AddAttachment(att.FileName, target.ToArray());
                    }
                }
            }

            await issue.AddWatcherAsync(creator);

            return result;
        }

        public void UpdateIssue(UpdateIssueViewModel viewModel)
        {
            var issue = GetIssueById(viewModel.IssueKey);
            issue.Type = viewModel.Type;
            issue.Priority = viewModel.Priority;
            issue.Summary = viewModel.Summary;

            issue.Description = viewModel.Description;

            if (!string.IsNullOrEmpty(viewModel.PostComment))
            {
                Task.Run(async () => await issue.AddCommentAsync(viewModel.PostComment)).GetAwaiter().GetResult();
            }

            if (viewModel.NewAttachments != null)
            {
                foreach (var att in viewModel.NewAttachments)
                {
                    if (att != null)
                    {
                        var target = new MemoryStream();
                        att.CopyTo(target);
                        issue.AddAttachment(att.FileName, target.ToArray());
                    }
                }
            }

            issue.SaveChanges();
        }

        public List<Project> FetchProjects()
        {
            var projects = Task.Run(async () => await _jira.Projects.GetProjectsAsync()).GetAwaiter().GetResult();

            return projects.OrderBy(x => x.Name).ToList();
        }

        public List<ComponentResult> FetchComponents(string projectKey)
        {
            var componentsResource =
                $"{SupportModuleConstants.Jira.ResourceNames.Project}/{projectKey}/{SupportModuleConstants.Jira.ResourceNames.Components}";

            var componentsRequest = new RestRequest(componentsResource)
                .AcceptApplicationJson()
                .ContentTypeApplicationJson();
            var response = ExecuteApiRequest(componentsRequest);

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Request did not complete.");
            }

            return JsonConvert.DeserializeObject<List<ComponentResult>>(response.Content, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            })
                .OrderBy(x => x.Name)
                .ToList();
        }

        public async Task<List<JiraUser>> GetJiraUsersAsync(string search)
        {
            search = search?.Trim();

            return (await _jira.Users.SearchUsersAsync(search)).ToList();
        }

        public List<IssuePriority> FetchPriorities()
        {
            var priorities = Task.Run(async () => await _jira.Priorities.GetPrioritiesAsync()).GetAwaiter()
                .GetResult().ToList();

            return priorities;
        }

        public List<IssueType> FetchIssueTypes(string projectKey)
        {
            var project = Task.Run(async () => await _jira.Projects.GetProjectsAsync()).GetAwaiter().GetResult()
                .FirstOrDefault(item => item.Key == projectKey);

            var issueTypes = project != null ?
                Task.Run(async () => await project.GetIssueTypesAsync()).GetAwaiter().GetResult().ToList() :
                new List<IssueType>();

            return issueTypes;
        }

        public List<JiraUserFromApi> FetchUsersForProject(string projectKey)
        {
            var jiraUsersRequest = new RestRequest(SupportModuleConstants.Jira.ResourceNames.UsersPerProject)
                .AddParameter(SupportModuleConstants.Jira.ParameterNames.Project, projectKey)
                .ContentTypeApplicationJson()
                .AcceptApplicationJson();

            var response = ExecuteApiRequest(jiraUsersRequest);

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Request did not complete.");
            }

            return JsonConvert.DeserializeObject<List<JiraUserFromApi>>(response.Content);
        }

        public BoardConfiguration FetchBoardConfiguration(string projectKey)
        {
            var boardsForProjectRequest = new RestRequest(SupportModuleConstants.Jira.ResourceNames.Board, Method.GET)
                .AddParameter(SupportModuleConstants.Jira.ParameterNames.ProjectKeyOrId, projectKey)
                .ContentTypeApplicationJson()
                .AcceptApplicationJson();

            var response = ExecuteAgileRequest(boardsForProjectRequest);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Request did not complete.");
            }

            var boardsForProject = JsonConvert.DeserializeObject<BoardsResult>(response.Content, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
            if (boardsForProject.ErrorMessages != null && boardsForProject.ErrorMessages.Count > 0)
            {
                throw new Exception(boardsForProject.ErrorMessages.First());
            }

            if (boardsForProject.Values == null || boardsForProject.Values.Count == 0)
            {
                return null;
            }

            var boardUrl = boardsForProject.Values.OrderBy(x => x.Id).First().Self;

            var configurationResponse = GetBoardConfiguration(boardUrl);
            if (configurationResponse.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Request did not complete.");
            }

            var boardConfig = JsonConvert.DeserializeObject<BoardConfiguration>(configurationResponse.Content, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            return boardConfig;
        }

        public IRestResponse RankBeforeIssue(string issueIdToRank, string rankBeforeIssueId, string customSortField)
        {
            var jsonObject = new
            {
                issues = new[] { issueIdToRank },
                rankBeforeIssue = rankBeforeIssueId,
                rankCustomFieldId = customSortField
            };

            string jsonString = JsonConvert.SerializeObject(jsonObject);

            var request = new RestRequest(Method.PUT)
                .ContentTypeApplicationJson()
                .AddJsonParameter(jsonString, ParameterType.RequestBody);

            var url = _settings.SupportJiraUrlIssueRank;

            var response = ExecuteRequest(url, request);

            return response;
        }

        public IRestResponse RankAfterIssue(string issueIdToRank, string rankAfterIssueId, string customSortField)
        {
            var jsonObject = new
            {
                issues = new[] { issueIdToRank },
                rankAfterIssue = rankAfterIssueId,
                rankCustomFieldId = customSortField
            };

            string jsonString = JsonConvert.SerializeObject(jsonObject);

            var request = new RestRequest(Method.PUT)
                .ContentTypeApplicationJson()
                .AddJsonParameter(jsonString, ParameterType.RequestBody);

            var url = _settings.SupportJiraUrlIssueRank;

            var response = ExecuteRequest(url, request);

            return response;
        }

        public bool HasBackLog(BoardConfiguration config)
        {
            if (config?.ColumnConfig?.Columns == null || config.ColumnConfig.Columns.Length == 0)
            {
                return false;
            }

            //Scrum boards always have a backlog
            if (config.Type == SupportModuleConstants.Jira.BoardTypes.Scrum)
            {
                return true;
            }

            //Kanban board backlog is defined as a column and can be enabled and disabled in the settings
            if (config.Type == SupportModuleConstants.Jira.BoardTypes.Kanban)
            {
                var hasBacklog = config.ColumnConfig.Columns.Any(x => x.Name == SupportModuleConstants.Jira.BoardColumnNames.Backlog
                                                                      && x.Statuses.Any());
                return hasBacklog;
            }

            return false;
        }

        public Issue GetIssueById(string issueKey)
        {
            var result = (from issue in _jira.Issues.Queryable
                          where issue.Key == issueKey
                          select issue).SingleOrDefault();

            return result;
        }

        public List<IssueTransition> GetPossibleTransitionsForIssue(string issueKey)
        {
            var transitions = Task.Run(async () => await GetIssueById(issueKey).GetAvailableActionsAsync()).GetAwaiter().GetResult();

            return transitions.ToList();
        }

        public List<string> GetPossibleBoxTransitions(string destinationBox, BoardConfiguration config)
        {
            var statusIds = config.ColumnConfig.Columns.Single(x => x.Name.ToLower().Equals(destinationBox.ToLower()))
                .Statuses.Select(x => x.Id);

            return statusIds.ToList();
        }

        public void ChangeIssueStatus(string issueKey, string transitionName)
        {
            Task.Run(async () => await GetIssueById(issueKey).WorkflowTransitionAsync(transitionName)).GetAwaiter().GetResult();
        }

        public void GetAttachmentsAndCommentsForIssue(string issueKey, out IEnumerable<Attachment> attachments, out IEnumerable<Comment> comments)
        {
            attachments = Task.Run(async () => await _jira.Issues.GetAttachmentsAsync(issueKey)).GetAwaiter().GetResult();

            comments = Task.Run(async () => await _jira.Issues.GetCommentsAsync(issueKey)).GetAwaiter().GetResult();
        }

        public Attachment DownloadAttachment(string issueKey, string attachmentId)
        {
            var attachments = Task.Run(async () => await _jira.Issues.GetAttachmentsAsync(issueKey)).GetAwaiter().GetResult();

            var attachment = attachments.SingleOrDefault(i => i.Id == attachmentId);

            return attachment;
        }

        public void DeleteAttachment(string issueKey, string attachmentId)
        {
            Task.Run(async () => await _jira.Issues.DeleteAttachmentAsync(issueKey, attachmentId)).GetAwaiter().GetResult();
        }

        public PagedList<Modules.Support.Models.Issue> FindIssues(string project, string[] statuses, string[] components,
            string assignee, string reporter, int skip, int take, BoardConfiguration config)
        {
            if (string.IsNullOrWhiteSpace(project))
            {
                throw new ArgumentException(nameof(project));
            }

            var customSortField = config?.Ranking?.RankCustomFieldId;

            if (statuses == null || statuses.Length == 0)
            {
                return new PagedList<Modules.Support.Models.Issue>(new List<Modules.Support.Models.Issue>(), 0);
            }

            var componentList = components == null ? new List<string>() : components.ToList();

            var issues = GetIssuesFromServer(project, statuses.ToList(), componentList, assignee, reporter, customSortField);

            var totalCount = issues.Count;

            return new PagedList<Modules.Support.Models.Issue>(issues.Skip(skip).Take(take), totalCount);
        }

        public List<SelectListItem> FetchIssueKeys(string project, BoardConfiguration config)
        {
            var customSortField = config.Ranking?.RankCustomFieldId;

            var issues = GetIssuesFromServer(project, null, null, null, null, customSortField);

            var issueList = new List<SelectListItem>();

            issueList.AddRange(issues.Select(x => new SelectListItem(x.Key, x.Key)));

            return issueList;
        }

        public PagedList<Modules.Support.Models.Issue> FindBacklogIssues(string project, List<string> components,
            string assignee, string reporter, int start, int length, BoardConfiguration config)
        {
            if (config == null)
            {
                throw new Exception("Board configuration has not been found");
            }

            var customSortField = config.Ranking?.RankCustomFieldId;

            string url;

            string requestType;

            var statuses = new List<string>();

            if (config?.Type == SupportModuleConstants.Jira.BoardTypes.Scrum)
            {
                requestType = SupportModuleConstants.Jira.ResourceNames.Backlog;

                url = _settings.SupportJiraUrlAgile + $"/{SupportModuleConstants.Jira.ResourceNames.Board}/{config.Id}";
            }
            else //kanban
            {
                url = _settings.SupportJiraUrl;
                requestType = SupportModuleConstants.Jira.ResourceNames.Search;

                var backlogColumn = config?.ColumnConfig.Columns.First(x => x.Name.Equals(SupportModuleConstants.Jira.BoardColumnNames.Backlog));
                var statusIds = backlogColumn?.Statuses.Select(x => x.Id).ToList();

                statuses.AddRange(statusIds);
            }

            var jqlString = CreateSearchIssueJQLString(statuses, project, components, assignee, reporter, customSortField);

            var request = new RestRequest(requestType, Method.GET)
                .AddParameter("jql", jqlString)
                .AddParameter("startAt", start)
                .AddParameter("maxResults", length)
                .AcceptApplicationJson()
                .ContentTypeApplicationJson();

            var response = ExecuteRequest(url, request);

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Request did not complete.");
            }

            var result = JsonConvert.DeserializeObject<SearchResult>(response.Content);

            if (result == null)
            {
                throw new Exception("Error fetching backlog issues");
            }

            var issueList = result.Issues?.Select(x => new Modules.Support.Models.Issue(x)).ToList() ?? new List<Modules.Support.Models.Issue>();

            return new PagedList<Modules.Support.Models.Issue>(issueList, result.Total ?? 0);
        }

        #endregion Methods

        #region Helpers

        private IRestResponse GetBoardConfiguration(string boardUrl)
        {
            var boardConfiguration = new RestRequest(SupportModuleConstants.Jira.ResourceNames.Configuration, Method.GET)
                .ContentTypeApplicationJson()
                .AcceptApplicationJson();

            return ExecuteRequest(boardUrl, boardConfiguration);
        }

        private IRestResponse ExecuteAgileRequest(IRestRequest request)
        {
            var url = _settings.SupportJiraUrlAgile;
            return ExecuteRequest(url, request);
        }

        private IRestResponse ExecuteApiRequest(IRestRequest request)
        {
            var url = _settings.SupportJiraUrl;
            return ExecuteRequest(url, request);
        }

        private IRestResponse ExecuteRequest(string url, IRestRequest request)
        {
            string username = _settings.SupportJiraUsername;
            string password = _settings.SupportJiraPassword;

            var restClient = new RestClient(url);
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                restClient.Authenticator = new HttpBasicAuthenticator(username, password);
            }

            return restClient.Execute(request);
        }

        private string CreateSearchIssueJQLString(List<string> statuses, string project, List<string> components, string assignee, string reporter, string rankCustomFieldId)
        {
            StringBuilder jql = new StringBuilder($"Project={project}");

            var filterByStatus = string.Empty;
            if (statuses?.Count > 0)
            {
                filterByStatus = " and Status in (";
                foreach (var status in statuses)
                {
                    filterByStatus += status + ",";
                }

                filterByStatus = filterByStatus.TrimEnd(new[] { ',' });
                filterByStatus += ")";
            }

            jql.Append(filterByStatus);

            var filterByComponent = string.Empty;
            if (components?.Count > 0)
            {
                filterByComponent = " and component in (";
                foreach (var comp in components)
                {
                    filterByComponent += comp + ",";
                }

                filterByComponent = filterByComponent.TrimEnd(new[] { ',' });
                filterByComponent += ")";
            }

            jql.Append(filterByComponent);

            var filterByAssignee = string.Empty;
            if (!string.IsNullOrEmpty(assignee))
            {
                if (assignee.Equals("null"))
                {
                    filterByAssignee = " and assignee = null";
                }
                else
                {
                    filterByAssignee = " and assignee=" + PrepareUsername(assignee);
                }
            }

            jql.Append(filterByAssignee);

            var filterByReporter = string.Empty;
            if (!string.IsNullOrEmpty(reporter))
            {
                filterByReporter = " and reporter=" + PrepareUsername(reporter);
            }

            jql.Append(filterByReporter);

            jql.Append($" order by cf[{rankCustomFieldId}] asc");

            return jql.ToString();
        }

        private List<Modules.Support.Models.Issue> GetIssuesFromServer(string project, List<string> statuses, List<string> components,
            string assignee, string reporter, string rankCustomFieldId)
        {
            var url = _settings.SupportJiraUrl;
            var jqlString = CreateSearchIssueJQLString(statuses, project, components, assignee, reporter, rankCustomFieldId);

            var request = new RestRequest(SupportModuleConstants.Jira.ResourceNames.Search, Method.GET)
                .ContentTypeApplicationJson()
                .AcceptApplicationJson()
                .AddParameter("jql", jqlString);

            var response = ExecuteRequest(url, request);

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Request did not complete.");
            }

            var result = JsonConvert.DeserializeObject<SearchResult>(response.Content);

            return result?.Issues?.Select(x => new Modules.Support.Models.Issue(x)).ToList() ?? new List<Modules.Support.Models.Issue>();
        }

        private string PrepareUsername(string username)
        {
            return "\"" + username.Trim().Replace(" ", string.Empty).ToLower() + "\"";
        }

        #endregion Helpers
    }
}