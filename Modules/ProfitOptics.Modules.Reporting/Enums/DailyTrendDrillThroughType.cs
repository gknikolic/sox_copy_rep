﻿namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum DailyTrendDrillThroughType
    {
        BillTo = 0,
        ShipTo = 1,
        CustomerClass = 2,
        ItemCategory = 3,
        SalesRep = 4
    }
}