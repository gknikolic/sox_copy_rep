using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.ManualEtl.Data.Domain
{
    [Table("ETLLog")]
    public partial class ETLLog
    {
        public int Id { get; set; }

        public string Source { get; set; }

        public string FileName { get; set; }

        public string Username { get; set; }

        public DateTime ProcessingTime { get; set; }

        public string Status { get; set; }

        public string Message { get; set; }

        public int RowCount { get; set; }
    }
}