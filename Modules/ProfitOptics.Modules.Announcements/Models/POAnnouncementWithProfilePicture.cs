﻿using ProfitOptics.Modules.Announcements.Data.Domain;

namespace ProfitOptics.Modules.Announcements.Models
{
    public class POAnnouncementWithProfilePicture : POAnnouncement
    {
        public byte[] UserProfilePicture { get; set; }

        public POAnnouncementWithProfilePicture(POAnnouncement announcement, byte[] userProfilePicture)
        {
            Id = announcement.Id;
            AspNetUser = announcement.AspNetUser;
            CreateTime = announcement.CreateTime;
            IsDeleted = announcement.IsDeleted;
            POAnnouncementReads = announcement.POAnnouncementReads;
            POAnnouncementRoles = announcement.POAnnouncementRoles;
            Text = announcement.Text;
            Title = announcement.Title;
            UserId = announcement.UserId;
            ValidUntil = announcement.ValidUntil;
            UserProfilePicture = userProfilePicture;
        }
    }
}
