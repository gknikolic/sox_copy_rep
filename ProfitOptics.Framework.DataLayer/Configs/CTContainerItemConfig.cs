﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerItemConfig : IEntityTypeConfiguration<CTContainerItem>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTContainerItem> builder)
        {
            builder.ToTable("CTContainerItem");
            builder.HasKey(e => e.Id);

            builder.HasOne(d => d.CTProduct)
                .WithMany(p => p.CTContainerItems)
                .HasForeignKey(d => d.CTProductId)
                .HasConstraintName("FK_CTContainerItem_CTProduct");

            builder.HasOne(d => d.CTContainerType)
                .WithMany(p => p.CTContainerItems)
                .HasForeignKey(d => d.CTContainerTypeId)
                .HasConstraintName("FK_CTContainerItem_CTContainerType");
        }
    }
}
