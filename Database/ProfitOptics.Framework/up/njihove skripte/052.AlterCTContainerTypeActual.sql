USE [Sox]

ALTER TABLE [Sox].[dbo].[CTContainerTypeActual] ADD ParentName NVARCHAR(100) NULL;
ALTER TABLE [Sox].[dbo].[CTContainerTypeActual] ALTER COLUMN ProcurementReferenceId NVARCHAR(100) NULL;
