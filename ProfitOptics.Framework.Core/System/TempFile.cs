﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.IO;

namespace ProfitOptics.Framework.Core.System
{
    /// <summary>
    /// Manages creation and deletion of temp file
    /// </summary>
    /// <example>
    /// TempFile can be used in this way
    /// <code>
    /// using(var tempFile = new TempFile())
    /// {
    ///     // write something to tempFile.FileName
    ///     File.WriteAllText(tempFile.FileName, "blahblah");
    /// }
    /// </code>
    /// </example>
    public class TempFile : IDisposable, ITempFile
    {
        private readonly IIO _inputOutput;

        public TempFile(IIO inputOutput)
        {
            _inputOutput = inputOutput;
            FileName = Path.Combine(_inputOutput.GetTempFolder(), Guid.NewGuid().ToString() + ".tmp");
        }

        public string GetTempFileForUser(int userId, string fileName)
        {
            var folder = Path.Combine(_inputOutput.GetTempFolder(), userId.ToString());
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            return Path.Combine(folder, fileName);
        }

        public string FileName { get; set; }

        public MemoryStream ToMemoryStream()
        {
            if (File.Exists(FileName))
            {
                var stream = new MemoryStream();

                using (var fStream = new FileStream(FileName, FileMode.Open, FileAccess.Read))
                {
                    fStream.CopyTo(stream);
                }

                stream.Close();
                return new MemoryStream(stream.ToArray());
            }

            return null;
        }

        public void Dispose()
        {
            if (File.Exists(FileName))
                File.Delete(FileName);
        }

        public string GetFileName()
        {
            return FileName;
        }
    }
}
