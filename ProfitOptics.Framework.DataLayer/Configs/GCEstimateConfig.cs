﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCEstimateConfig : IEntityTypeConfiguration<GCEstimate>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCEstimate> builder)
        {
            builder.ToTable("GCEstimate");
            builder.HasKey(entity => entity.Id);
        }
    }
}
