﻿namespace ProfitOptics.Framework.Web.Models.HelpArticle
{
    public class HelpArticleFilterModel
    {
        public string SearchValue { get; set; }
        public string CurrentUrl { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }

        public HelpArticleFilterModel(string currentUrl, string searchValue, int skip, int take)
        {
            CurrentUrl = currentUrl;
            SearchValue = searchValue;
            Skip = skip;
            Take = take;
        }
    }
}