﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Core.Flurl
{
    public static class FlurlExtensions
    {
        public static async Task<T> GetAsync<T>(this IFlurlRequest request)
        {
            return await GetResponseAsync<T>(async () =>
            {
                return await request.AllowHttpStatus("4xx")
                .AllowHttpStatus("5xx")
                .GetAsync()
                .ReceiveJson<T>();
            });
        }


        public static async Task<T> PostAsync<T>(this IFlurlRequest request, object data)
        {

            return await GetResponseAsync<T>(async () =>
            {
                var resp = await request.AllowHttpStatus("4xx")
                .AllowHttpStatus("5xx")
                .PostJsonAsync(data)
                .ReceiveJson<T>();
                return resp;
            });


        }

        public static async Task<T> PutAsync<T>(this IFlurlRequest request, object data)
        {

            return await GetResponseAsync<T>(async () =>
            {
                var resp = await request.AllowHttpStatus("4xx")
                .AllowHttpStatus("5xx")
                .PutJsonAsync(data)
                .ReceiveJson<T>();
                return resp;
            });


        }

        static async Task<T> GetResponseAsync<T>(Func<Task<T>> action)
        {
            try
            {
                var result = await action();
                return result;
            }
            catch (FlurlParsingException)
            {
                throw;
            }
            catch (FlurlHttpTimeoutException)
            {
                throw;
            }
            catch (FlurlHttpException)
            {
                //var response = await ex.GetResponseJsonAsync<Contracts.OrderResponse>();
                //throw new Exception("response.Message", ex);//TODO: examine error response
                throw;
            }
        }
    }
}
