﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTBillToPriceListConfig : IEntityTypeConfiguration<QTBillToPriceList>
    {
        public void Configure(EntityTypeBuilder<QTBillToPriceList> builder)
        {
            builder.ToTable("QTBillToPriceList");
            builder.HasKey(entity => new { entity.Company, entity.BillToNum });

            builder.Property(entity => entity.Company).HasMaxLength(50);
            builder.Property(entity => entity.BillToNum).ValueGeneratedNever();
            builder.Property(entity => entity.ShipToNum).HasMaxLength(50);
            builder.Property(entity => entity.SeqNum);
            builder.Property(entity => entity.PriceListCode).HasMaxLength(50);
            builder.Property(entity => entity.PriceListDescription).HasMaxLength(50);

        }
    }
}
