﻿using ComponentSpace.Saml2;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Novell.Directory.Ldap;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.ServiceResults;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace ProfitOptics.Framework.Web.Framework.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        #region Fields

        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IUserService _userService;
        private readonly IStringResourceRepository _stringResourceRepository;
        private readonly IUrlHelper Url;
        private readonly HttpContext HttpContext;
        private readonly IServiceProvider _serviceProvider;
        private readonly IUserClaimsPrincipalFactory<ApplicationUser> _principalFactory;
        private readonly Ldap _ldapConfig;
        private readonly LdapConnection _ldapConnection;


        #endregion

        #region Ctor

        public AuthenticationService(SignInManager<ApplicationUser> signInManager,
            IUserService userService,
            IStringResourceRepository resourceRepository,
            IUrlHelper urlHelper,
            IHttpContextAccessor httpContextAccessor,
            IServiceProvider serviceProvider,
            IUserClaimsPrincipalFactory<ApplicationUser> principalFactory,
            Ldap ldapConfig)
        {
            this._signInManager = signInManager;
            this._userService = userService;
            this._stringResourceRepository = resourceRepository;
            this.Url = urlHelper;
            this.HttpContext = httpContextAccessor.HttpContext;
            this._serviceProvider = serviceProvider;
            this._principalFactory = principalFactory;
            this._ldapConfig = ldapConfig;
            this._ldapConnection = new LdapConnection
            {
                SecureSocketLayer = true
            };
        }

        #endregion

        #region Methods

        public async Task<SignInActionResult> PasswordSignInAsync(string username, string password, bool rememberMe)
        {
            var user = await _userService.FindUserByUsernameAsync(username);

            if (user == null)
            {
                //We want to hide that user doesn't exists
                return SignInActionResult.Failed;
            }

            var validationResult = await this.ValidateUserAsync(user);

            if (!validationResult.Succeeded)
            {
                return AuthenticationServiceResult.Failed<SignInActionResult>(validationResult.Error);
            }

            var signInResult = await _signInManager.PasswordSignInAsync(username, password, rememberMe, true);
            if (signInResult.Succeeded)
            {
                HttpContext.Session.Clear();
                await _userService.ResetAccessFailedCountAsync(user);
                byte[] userProfilePictureData = await _userService.GetProfilePictureAsync(user.Id);
                HttpContext.Session.SetByteArray(SessionKeys.ProfilePicture, userProfilePictureData);
            }
            else if (signInResult.Failed())
            { 
                await _userService.AddAccessFailedAttemptToUser(user);
            }
            return new SignInActionResult(signInResult);
        }

        public async Task SignOutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<UserRegistrationResult> RegisterUserAsync(string firstName, string lastName, string email, string password, string phone, string requestScheme)
        {
            IdentityResult createResult = await _userService.CreateUserAsync(firstName, lastName, email, password, phone);
            if (createResult.Succeeded)
            {

                if (_signInManager.Options.SignIn.RequireConfirmedEmail)
                {
                    var emailConfirmationUrlResult = await GetEmailConfirmationUrlAsync(email, requestScheme);
                    if (emailConfirmationUrlResult.Succeeded)
                    {
                        return UserRegistrationResult.EmailNotConfirmed(emailConfirmationUrlResult.Url);
                    }
                    return AuthenticationServiceResult.Failed<UserRegistrationResult>(GetStringFromResource(ResourceKeys.MessagesGenericError));

                }
                return UserRegistrationResult.Success();
            }
            else
            {
                return AuthenticationServiceResult.Failed<UserRegistrationResult>(FormatErrorMessage(createResult.Errors.Select(x => x.Description)));
            }
        }

        public async Task<EmailConfirmationUrlResult> GetEmailConfirmationUrlAsync(string email, string scheme)
        {
            var user = await _userService.FindUserByEmailAsync(email);
            if (user == null)
            {
                return AuthenticationServiceResult.UserNotFound<EmailConfirmationUrlResult>();
            }

            var token = await _userService.GenerateEmailConfirmationTokenAsync(user);
            var url = Url.RouteUrl(POFrameworkDefaults.ConfirmEmailRouteName,
                new { userId = user.Id, confirmationToken = WebUtility.UrlEncode(token) }, scheme);

            return EmailConfirmationUrlResult.Success(url);
        }

        public async Task<AuthenticationServiceResult> ConfirmEmailAsync(int userId, string confirmationToken)
        {
            var user = await _userService.FindUserByIdAsync(userId);
            if (user == null)
            {
                return AuthenticationServiceResult.UserNotFound();
            }

            IdentityResult confirmationResult = await _userService.ConfirmEmailAsync(user, confirmationToken);
            if (!confirmationResult.Succeeded)
            {
                return AuthenticationServiceResult.Failed(FormatErrorMessage(confirmationResult.Errors.Select(x => x.Description)));
            }
            return AuthenticationServiceResult.Success();
        }

        public async Task<SignInActionResult> AuthenticateExternalLoginAsync(AuthenticateResult externalAuthenticationResult)
        {
            var externalLoginDetails = this.GetExternalLoginDetails(externalAuthenticationResult);

            var signInResult = await _signInManager.ExternalLoginSignInAsync(externalLoginDetails.Provider, externalLoginDetails.User.Key, false);
            if (!signInResult.Failed())
            {
                return new SignInActionResult(signInResult);
            }
            //We have to create new user for this login
            var user = await this.SaveExternalLoginAsync(externalLoginDetails);
            if (user != null)
            {
                await _signInManager.SignInAsync(user, false);
                HttpContext.Session.Clear();
                return SignInActionResult.Success;
            }

            var errorMessage = "There was an error adding external login.";
            return AuthenticationServiceResult.Failed<SignInActionResult>(errorMessage);
        }

        public async Task<TwoFactorProvidersResult> GetValidTwoFactorProvidersAsync()
        {
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                AuthenticationServiceResult.UserNotFound<TwoFactorProvidersResult>();
            }

            IList<string> providers = await _userService.GetValidTwoFactorProvidersForUserAsync(user);
            return TwoFactorProvidersResult.Success(providers);
        }

        public async Task<AuthenticationServiceResult> SendTwoFactorAuthenticationCodeAsync(string providerName)
        {
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return AuthenticationServiceResult.UserNotFound();
            }

            var code = await _userService.GenerateTwoFactorTokenAsync(user, providerName);
            var providerService = _serviceProvider.GetServices<ITwoFactorResolver>().FirstOrDefault(x => x.GetProviderName() == providerName);
            if (providerService != null)
            {
                var sendingSuccessful = await providerService.SendCode(code, user);
                if (!sendingSuccessful)
                {
                    return AuthenticationServiceResult.Failed(GetStringFromResource(ResourceKeys.Messages2FASendingCodeFailed));
                }
            }

            return AuthenticationServiceResult.Success();
        }

        public async Task<SignInActionResult> TwoFactorSignInAsync(string provider, string code, bool rememberMe, bool rememberBrowser)
        {
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return AuthenticationServiceResult.UserNotFound<SignInActionResult>();
            }
            var signInResult = await _signInManager.TwoFactorSignInAsync(provider, code, rememberMe, rememberBrowser);

            return new SignInActionResult(signInResult);
        }

        public async Task<PasswordResetUrlResult> GetPasswordResetUrlByEmailAsync(string email, string scheme)
        {
            var user = await _userService.FindUserByEmailAsync(email);
            if (user == null)
            {
                return PasswordResetUrlResult.UserNotFound;
            }
            
            string url = null, token = null;
            if (!await _userService.IsEmailConfirmedAsync(user))
            {
                return PasswordResetUrlResult.EmailNotConfirmed;
            }

            token = await _userService.GeneratePasswordResetTokenAsync(user);
            url = Url.RouteUrl(POFrameworkDefaults.PasswordResetRouteName, new { userId = user.Id, code = WebUtility.UrlEncode(token), email = email }, scheme);
            return PasswordResetUrlResult.Success(url);
        }

        public async Task<AuthenticationServiceResult> PasswordResetAsync(string email, string code, string password)
        {
            var user = await _userService.FindUserByEmailAsync(email);
            if (user == null)
            {
                AuthenticationServiceResult.UserNotFound();
            }

            IdentityResult resetResult = await _userService.ResetPasswordAsync(user, code, password);
            if (resetResult.Succeeded)
            {
                if (await _userService.IsLockedOutAsync(user))
                {
                    await _userService.SetLockoutEndDateAsync(user, DateTimeOffset.UtcNow);
                }
                return AuthenticationServiceResult.Success();
            }

            var errors = resetResult.Errors.Select(x => x.Description);
            return AuthenticationServiceResult.Failed(FormatErrorMessage(errors));
        }

        public async Task<AuthenticationServiceResult> StartImpersonatingSessionAsync(int userId)
        {
            var originalUserId = HttpContext.User.GetUserId();
            await SignOutAsync();

            var user = await _userService.FindUserByIdAsync(userId);
            if (user == null)
            {
                return AuthenticationServiceResult.UserNotFound();
            }
            var baseClaimPrincipal = (await _principalFactory.CreateAsync(user)).Identities.First();
            baseClaimPrincipal.AddClaim(new Claim(ApplicationClaimTypes.OriginalUserId, originalUserId.ToString()));
            var claimsPrincipal = new ClaimsPrincipal(baseClaimPrincipal);

            await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, claimsPrincipal);
            byte[] userProfileImageData = await _userService.GetProfilePictureAsync(userId);
            HttpContext.Session.SetByteArray(SessionKeys.ProfilePicture, userProfileImageData);

            return AuthenticationServiceResult.Success();
        }

        public async Task<AuthenticationServiceResult> FinishImpersonatingSessionAsync()
        {
            var originalUserId = int.Parse(HttpContext.User.Claims.First(x => x.Type == ApplicationClaimTypes.OriginalUserId).Value);
            var user = await _userService.FindUserByIdAsync(originalUserId);
            if (user == null)
            {
                return AuthenticationServiceResult.UserNotFound();
            }
            await SignOutAsync();
            var claimsPrincipal = await _principalFactory.CreateAsync(user);
            await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, claimsPrincipal);
            var userProfilePictureData = await _userService.GetProfilePictureAsync(originalUserId);
            HttpContext.Session.SetByteArray(SessionKeys.ProfilePicture, userProfilePictureData);
            return AuthenticationServiceResult.Success();
        }

        public async Task<bool> IsTwoFactorClientRememberedAsync(int userId)
        {
            var user = await _userService.FindUserByIdAsync(userId);
            return await _signInManager.IsTwoFactorClientRememberedAsync(user);
        }

        public async Task<IList<AuthenticationScheme>> GetExternalLoginProvidersAsync()
        {
            return (await _signInManager.GetExternalAuthenticationSchemesAsync())?.ToList();
        }

        #endregion

        #region Utilities

        private string FormatErrorMessage(IEnumerable<string> errors)
        {
            return string.Join(";", errors).TrimEnd(';');
        }

        private string GetStringFromResource(string key)
        {
            return _stringResourceRepository.GetResource(key);
        }

        private async Task<AuthenticationServiceResult> ValidateUserAsync(ApplicationUser user)
        {
            if (!user.IsEnabled)
            {
                return AuthenticationServiceResult.Failed(GetStringFromResource(ResourceKeys.MessagesSignInResultDisabled));
            }

            bool hasRoles = (await _userService.GetUserRolesAsync(user)).Any();
            if (!hasRoles)
            {
                return  AuthenticationServiceResult.Failed(GetStringFromResource(ResourceKeys.MessagesSignInResultNotApproved));
            }
            return AuthenticationServiceResult.Success();
        }

        public ExternalLoginDetails GetExternalLoginDetails(AuthenticateResult response)
        {
            return new ExternalLoginDetails
            {
                Provider = response.Properties.Items["scheme"],
                ReturnUrl = response.Properties.Items["returnUrl"],
                User = new ExternalUserDetails
                {
                    Key = response.Principal.FindFirstValue(ClaimTypes.NameIdentifier),
                    Email = response.Principal.FindFirstValue(ClaimTypes.Email),
                    FirstName = response.Principal.FindFirstValue(ClaimTypes.GivenName),
                    LastName = response.Principal.FindFirstValue(ClaimTypes.Surname),
                    ImageUrl = response.Principal.FindFirstValue("image")
                }
            };
        }

        public async Task<ApplicationUser> SaveExternalLoginAsync(ExternalLoginDetails externalLoginDetails, ApplicationUser user = null)
        {
            var externalUser = externalLoginDetails.User;
            if (externalUser.Email != null)
            {
                //check if there is an exsisting user with that email
                user = user ?? await _userService.FindUserByEmailAsync(externalUser.Email);
                if (user == null)
                {
                    var profilePictureData = GetProfilePictureFromUrl(externalUser.ImageUrl);
                    await _userService.CreateUserAsync(externalUser.FirstName, externalUser.LastName, externalUser.Email, profilePictureData: profilePictureData);
                }
                var result = await _userService.AddExternalLoginAsync(user, new UserLoginInfo(externalLoginDetails.Provider, externalUser.Key, externalLoginDetails.Provider));
                if (result.Succeeded)
                {
                    return user;
                }
            }
            return null;
        }

        public async Task<AuthenticationServiceResult> AuthenticateSSOAsync()
        {
            var samlServiceProvider = HttpContext.RequestServices.GetService<ISamlServiceProvider>();
            if (samlServiceProvider == null)
            {
                return AuthenticationServiceResult.Failed("Error while injecting SAMLServiceProvider.");
            }
            var ssoResult = await samlServiceProvider.ReceiveSsoAsync();
            var user = await _userService.FindUserByUsernameAsync(ssoResult.UserID);
            if (user == null)
            {
                var firstName = ssoResult.Attributes.SingleOrDefault(item => item.Name == ClaimTypes.GivenName).ToString();
                var lastName = ssoResult.Attributes.SingleOrDefault(item => item.Name == ClaimTypes.Surname).ToString();
                var createUserResult = await _userService.CreateUserAsync(firstName, lastName, ssoResult.UserID);
                if (!createUserResult.Succeeded)
                {
                    var error = string.Join(';', createUserResult.Errors.Select(x => x.Description)).TrimEnd(';');
                    return AuthenticationServiceResult.Failed(error);
                }

                user = await _userService.FindUserByUsernameAsync(ssoResult.UserID);
                IdentityResult addToRolesResult = await _userService.AddUserToRolesAsync(user, new[] {ApplicationRoleNames.Reporting, ApplicationRoleNames.Quoting});
                if (!addToRolesResult.Succeeded)
                {
                    var error = string.Join(';', addToRolesResult.Errors.Select(x => x.Description)).TrimEnd(';');
                    return AuthenticationServiceResult.Failed(error);
                }
            }

            var userClaimsPrincipal = await _principalFactory.CreateAsync(user);
            await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, userClaimsPrincipal);
            return AuthenticationServiceResult.Success();
        }

        public async Task<SingleLogoutServiceResult> SingleLogoutAsync()
        {
            var samlServiceProvider = HttpContext.RequestServices.GetService<ISamlServiceProvider>();
            if (samlServiceProvider == null)
            {
                return SingleLogoutServiceResult.Failed("Error while injecting SAMLServiceProvider.");
            }
            var sloResult = await samlServiceProvider.ReceiveSloAsync();
            if (sloResult.IsResponse)
            {
                return SingleLogoutServiceResult.Failed(redirectUrl: sloResult.RelayState);
            }

            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            await samlServiceProvider.SendSloAsync();
            return SingleLogoutServiceResult.Success();
        }

        public async Task<SignInActionResult> ActiveDirectoryLoginAsync(string username, string password)
        {
            _ldapConnection.Connect(_ldapConfig.Url, LdapConnection.DEFAULT_SSL_PORT);
            _ldapConnection.Bind(_ldapConfig.BindOn, _ldapConfig.BindCredentials);

            var searchFilter = string.Format(_ldapConfig.SearchFilter, username);
            var result = _ldapConnection.Search(_ldapConfig.SearchBase, LdapConnection.SCOPE_SUB, searchFilter,
                new[] {"memberOf", "displayName", "sAMAccountName"}, false);
            try
            {
                var adUser = result.next();
                if (adUser != null)
                {
                    _ldapConnection.Bind(adUser.DN, password);
                    if (_ldapConnection.Bound)
                    {
                        var user = new ApplicationUser
                        {
                            UserName = adUser.getAttribute("sAMAccountName").StringValue,
                            Email = adUser.getAttribute("sAMAccountName").StringValue,
                            FirstName = adUser.getAttribute("displayName").StringValue,
                        };

                        var claimsPrincipal = await _principalFactory.CreateAsync(user);
                        await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, claimsPrincipal);
                        return SignInActionResult.Success;
                    }
                }
            }
            catch
            {
                return new SignInActionResult(SignInResult.Failed);
            }
            _ldapConnection.Disconnect();
            return new SignInActionResult(SignInResult.Failed);
        }

        public async Task RefreshAsync()
        {
            var userId = HttpContext.User.GetUserId();
            var user = await _userService.FindUserByIdAsync(userId);
            await _signInManager.SignInAsync(user, false, IdentityConstants.ApplicationScheme);
        }

        private byte[] GetProfilePictureFromUrl(string imageUrl)
        {
            if (imageUrl == null)
            {
                return null;
            }
            var imageBytes = new WebClient().DownloadData(imageUrl);
            MemoryStream memoryStream = new MemoryStream(imageBytes);
            return Core.System.ImageHelper.Resize(memoryStream, true);
        }

        #endregion
    }
}
