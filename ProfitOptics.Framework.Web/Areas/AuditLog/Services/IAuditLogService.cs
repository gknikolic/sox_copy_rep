﻿using ProfitOptics.Framework.Web.Areas.AuditLog.Models;
using System;
using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;

namespace ProfitOptics.Framework.Web.Areas.AuditLog.Services
{
    public interface IAuditLogService
    {
        /// <summary>
        /// Gets audit log data packed in datatables response.
        /// </summary>
        /// <param name="request">Datatables request</param>
        /// <param name="username">User name</param>
        /// <param name="from">"From" date</param>
        /// <param name="to">"To" date</param>
        /// <param name="showAll">show all- return all if true, otherwise only those with friendly names.</param>
        /// <returns>Datatables response with audit log data</returns>
        DataTablesResponse<AuditLogData> GetDataTablesData(IDataTablesRequest request, string username, DateTime? from, DateTime? to, bool showAll);

        /// <summary>
        /// Gets list of audit logs
        /// </summary>
        /// <param name="username">User name</param>
        /// <param name="from">"From" date</param>
        /// <param name="to">"To" date</param>
        /// <returns>List of audit logs</returns>
        List<AuditLogData> GetExcelDataAudit(string username, DateTime? from, DateTime? to);

        /// <summary>
        /// Updates audit log friendly names 
        /// </summary>
        void UpdateAuditLogFriendlyNameData();

        /// <summary>
        /// Gets list of audit log friendly names
        /// </summary>
        /// <param name="request"></param>
        /// <returns>List of audit log friendly names</returns>
        DataTablesResponse<AuditLogFriendlyNameData> GetAuditLogFriendlyNameData(IDataTablesRequest request);

        /// <summary>
        /// Sets new audit log friendly name for a given model
        /// </summary>
        /// <param name="model">Model</param>
        void SaveFriendlyName(AuditLogFriendlyNameData model);

    }
}
