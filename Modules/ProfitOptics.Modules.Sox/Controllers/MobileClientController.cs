﻿using ComponentSpace.Saml2.Assertions;
using Intuit.Ipp.Data;
using Intuit.Ipp.OAuth2PlatformClient;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.Anotations;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Factories;
using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Models.GroundControl;
using ProfitOptics.Modules.Sox.Models.QuickBooks;
using ProfitOptics.Modules.Sox.Models.ViewModels;
using ProfitOptics.Modules.Sox.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Controllers
{
    [Area("Sox")]
    //[Authorize]
    public class MobileClientController : BaseController
    {
        private readonly ISoxFactory _SoxFactory;
        private readonly ISoxService _soxService;
        private readonly IGroundControlService _groundControlService;
        private readonly IUserService _userService;
        private readonly IHelperService _helperService;

        public MobileClientController(ISoxFactory SoxFactory,
                             ISoxService soxService,
                             IGroundControlService groundControlService,
                             IUserService userService,
                             IHelperService helperService)
        {
            _SoxFactory = SoxFactory;
            _soxService = soxService;
            _groundControlService = groundControlService;
            _userService = userService;
            _helperService = helperService;
        }

        [HttpGet]
        public IActionResult GetCustomersForToday()
        {
            var gcCustomers = _soxService.GetGCCustomersSelectListByDate(DateTime.Now);
            return Json(gcCustomers);
        }

        [HttpGet]
        public IActionResult GetTraysForCustomer(int customerId)
        {
            var model = _SoxFactory.PrepareTarysForCaseModel(customerId);
			var mappedTrays = new List<TrayForPickup>();
			if(model!=null && model.Trays != null && model.Trays.Count>0)
			{
				foreach (var tray in model.Trays)
				{
					var mappedTray = new TrayForPickup()
					{
						Id = tray.Id,
						CaseReference = tray.CaseReference,
						DueTimestamp = tray.DueTimestamp,
						IsProccessed = tray.WasInPickupStatus,
						Title = tray.Name
					};
					mappedTrays.Add(mappedTray);
				}
			}

            return Json(mappedTrays);
        }

		[HttpGet]
		public IActionResult CheckTrayBarcode(string barcode, int customerId)
		{
			//check trays for barcode and customer
			var trays = _soxService.CheckTrayBarcodeForCustomer(barcode, customerId);
			var model = new CheckTrayBarcodeModel() {BarcodeResultType = BarcodeResultType.Ok, Trays = new List<SimpleTrayModel>() };
			//if there is only one tray found
			if (trays!=null && trays.Count == 1)
			{
				var wasInStatus = _soxService.CheckTrayWasInStatus("Pickup", trays.FirstOrDefault().Id);
				//check if it is not already in Pickup status so it is already processed
				if (wasInStatus)
				{
					model.BarcodeResultType = BarcodeResultType.AlreadyProcessed;
					return Json(model);
				}
				//if not, tray is ok and continue for processing
				else
				{
					model.BarcodeResultType = BarcodeResultType.Ok;
					model.GCTrayId = trays.FirstOrDefault().Id;
					return Json(model);
				}

			}
			//if there are multiple trays, prompt user to chose which one is for processing
			else if (trays != null && trays.Count > 1)
			{

				foreach (var tray in trays)
				{
					var trayModel = new SimpleTrayModel();
					trayModel.Id = tray.Id;
					trayModel.CaseReference = tray.GCCase?.CTCase?.CaseReference;
					model.Trays.Add(trayModel);
				}

				model.BarcodeResultType = BarcodeResultType.FoundMultiple;
				return Json(model);
			}
			//if non of the above, tray is not found
			else
			{
				model.BarcodeResultType = BarcodeResultType.NotFound;
				return Json(model);
			}

		}
		[HttpGet]
		public IActionResult ChangeTrayStatus(bool isFoamed, string barcode, int gcTrayId)
		{
			//set tray status
			_soxService.SetTrayStatus(gcTrayId, (int)LifecycleStatusType.Picked, User);
			//update status in WW
			_soxService.UpdateBarcodeToWWAsync(gcTrayId);

			var tray = _soxService.GetTrayById(gcTrayId);
			//check how many trays for case are processed and in PIckup, if all of them, set order status in ww
			var traysForCase = _soxService.GetTraysForGCCase((int)tray.GCCaseId);

			int traysProcessed = 0;
			foreach (var item in traysForCase)
			{
				if (_soxService.CheckTrayWasInStatus("Pickup", item.Id))
					traysProcessed++;
			}
			//if all are proccessed, set orders status in ww
			if (traysProcessed == traysForCase.Count)
			{ 
				if(tray.GCCaseId!=null)
				{
					_soxService.SetOrderStatusInWWAsync((int)tray.GCCaseId, Models.WorkWave.RouteStepStatusType.done);
				}
				
			}
			//set foamed pod to status
			_soxService.Foamed(gcTrayId, barcode, isFoamed);
			return Ok();	
		}
		[HttpGet]
		public IActionResult GetPhotoCaptureData(int gcTrayId)
		{
			PhotoCaptureModel model = new PhotoCaptureModel();
			model.TrayImages = _soxService.GetTrayImagesById(gcTrayId);

			return Json(model);
		}

		[HttpPost]
		public IActionResult StoreImage(string data, int gcTrayId)
		{
			//Only get bytes characters
			var base64 = data.Split(',').Last();
			byte[] bytes = System.Convert.FromBase64String(base64);

			var myUniqueFileName = Convert.ToString(Guid.NewGuid());
			var newFileName = string.Concat(myUniqueFileName, ".png");

			_soxService.StorePictureInPodContainer(bytes,newFileName, gcTrayId);
			return Ok();
		}
		[HttpGet]
		public IActionResult FinishPhotoCapture(bool isPassed, int gcTrayId, string note)
		{
			//we have note only if inspection is failed (isPassed == false)
			if(!isPassed)
			{
				_soxService.StoreNote(note, gcTrayId);
			}
			_soxService.SetOrderStatusInWWIfTraysArePickedUp(gcTrayId);
			return Ok();
		}
	}
}