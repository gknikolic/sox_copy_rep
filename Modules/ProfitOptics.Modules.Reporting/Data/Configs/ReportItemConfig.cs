﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportItemConfig : IEntityTypeConfiguration<ReportItem>
    {
        public void Configure(EntityTypeBuilder<ReportItem> builder)
        {
            builder.ToTable(nameof(ReportItem));
            builder.HasKey(i => i.Id);

            builder.Property(i => i.ItemNum).HasMaxLength(50).IsRequired();
            builder.Property(i => i.ItemDescription).HasMaxLength(255).IsRequired();
            builder.Property(i => i.ModelName).HasMaxLength(50);
            builder.Property(i => i.ListPrice).HasColumnType("decimal(19,4)").IsRequired();
            builder.Property(i => i.Cost).HasColumnType("decimal(19,4)").IsRequired();
            builder.Property(i => i.StartDate).IsRequired();

            builder.HasMany(i => i.ReportSalesHistories)
                .WithOne(s => s.ReportItem)
                .HasForeignKey(s => s.ItemId);
        }
    }
}
