﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer
{
    public partial class GCContainer
    {
        public int Id { get; set; }
        //public int CTCaseId { get; set; }
        public string Title { get; set; }
        public int ContainerTypeId { get; set; }
        public int SetId { get; set; }
        public virtual GCContainerType ContainerType { get; set; }
        public virtual GCSet Set { get; set; }
        public virtual ICollection<GCProduct> Products { get; set; }
    }
}
