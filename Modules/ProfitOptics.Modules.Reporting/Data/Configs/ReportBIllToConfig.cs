﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportBillToConfig : IEntityTypeConfiguration<ReportBillTo>
    {
        public void Configure(EntityTypeBuilder<ReportBillTo> builder)
        {
            builder.ToTable(nameof(ReportBillTo));
            builder.HasKey(rb => rb.Id);
            
            builder.Property(rb => rb.BillToNum).HasMaxLength(50).IsRequired();
            builder.Property(rb => rb.BillToName).HasMaxLength(50).IsRequired();
            builder.Property(rb => rb.Address).HasMaxLength(255).IsRequired();
            builder.Property(rb => rb.City).HasMaxLength(50).IsRequired();
            builder.Property(rb => rb.State).HasMaxLength(50).IsRequired();
            builder.Property(rb => rb.Zip).HasMaxLength(50).IsRequired();
            builder.Property(rb => rb.StartDate).IsRequired();

            builder.HasMany(rb => rb.ReportSalesHistories)
                .WithOne(s => s.ReportBillTo)
                .HasForeignKey(s => s.BillToId);

            builder.HasMany(rb => rb.ReportShipTos)
                .WithOne(rs => rs.ReportBillTo)
                .HasForeignKey(rs => rs.BillToId);

            builder.HasOne(rb => rb.ReportHierarchySalesRep)
                .WithMany(sr => sr.ReportBillTos)
                .HasForeignKey(rb => rb.SalesRepId)
                .IsRequired(false);

            builder.Ignore(rb => rb.FullBillToName);
        }
    }
}
