﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;

namespace ProfitOptics.Framework.Web.Infrastructure
{
    public class ModuleManager : IModuleManager
    {
        public IEnumerable<Assembly> GetExternalModules(IWebHostEnvironment hostingEnvironment)
        {
            var moduleDirectoryName = Path.Combine(hostingEnvironment.ContentRootPath, "Modules");

            if (!Directory.Exists(moduleDirectoryName))
            {
                yield break;
            }

            var moduleDirectories = Directory.GetDirectories(moduleDirectoryName);
            foreach (var moduleDirectory in moduleDirectories)
            {
                var moduleFullName = Path.Combine(hostingEnvironment.ContentRootPath, moduleDirectory,
                    $"{Path.GetFileName(moduleDirectory)}.dll");

                if (File.Exists(moduleFullName))
                {
                    yield return Assembly.LoadFile(moduleFullName);
                }
            }
        }
    }
}
