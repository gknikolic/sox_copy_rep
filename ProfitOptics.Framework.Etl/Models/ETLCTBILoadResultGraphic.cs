﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Models
{
    public class ETLCTBILoadResultGraphic
    {
        [JsonProperty("graphicKey")]
        public ETLCTGraphicKey GraphicKey { get; set; }

        [JsonProperty("fileName")]
        public string FileName { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("updateTimestamp")]
        public DateTime UpdateTimestamp { get; set; }

        [JsonProperty("useLocalImage")]
        public bool UseLocalImage { get; set; }
    }
}
