﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class Invoice
    {
        public int? Id { get; set; }
        //public bool AllowIpnPayment { get; set; }
        //public bool AllowOnlinePayment { get; set; }
        //public bool AllowOnlineCreditCardPayment { get; set; }
        //public bool AllowOnlineAchPayment { get; set; }
        //public string Domain { get; set; }
        //public bool Sparse { get; set; }
        public string SyncToken { get; set; }
        public MetaData MetaData { get; set; }
        //public CustomField[] CustomField { get; set; }
        public string DocNumber { get; set; }
        public DateTime TxnDate { get; set; }
        public GeneralRef CurrencyRef { get; set; }
        //public LinkedTxn[] LinkedTxn { get; set; }
        [JsonProperty("Line")]
        public List<InvoiceLine> InvoiceLines { get; set; }
        public TaxDetail TxnTaxDetail { get; set; }
        public GeneralRef CustomerRef { get; set; }
        public GeneralValue CustomerMemo { get; set; }
        public Address BillAddr { get; set; }
        public GeneralValue SalesTermRef { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal TotalAmt { get; set; }
        public bool ApplyTaxAfterDiscount { get; set; }
        public string PrintStatus { get; set; }
        public string EmailStatus { get; set; }
        public decimal Balance { get; set; }
    }
}
