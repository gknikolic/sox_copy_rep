﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCCustomerModel
    {
        public int Id { get; set; }

        [Required]
        public string FacilityId { get; set; }
        [Required]
        [Display(Name = "Facility Code")]
        public string FacilityCode { get; set; }
        public string DeliveryState { get; set; }
        public string DeliveryStreet { get; set; }
        [Required]
        [Display(Name = "City")]
        public string DeliveryCity { get; set; }
        [Required]
        [Display(Name = "Zip")]
        public string DeliveryZip { get; set; }
        [Required]
        [Display(Name = "Service Time")]
        public int ServiceTime { get; set; }
        [Required]
        [Display(Name = "Delivery Start")]
        public string DeliveryStart { get; set; }
        [Required]
        [Display(Name = "Delivery End")]
        public string DeliveryEnd { get; set; }
        public string DeliverNotes { get; set; }
        public string DeliverAccessCode { get; set; }
        public string DeliverPhoneNumber { get; set; }
        public string DeliverName { get; set; }
        [Required]
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        public int? QBCustomerId { get; set; }
        public int? WWDepoId { get; set; }
        public bool Active { get; set; }
        public DateTime SelectedDeliveryStart { get; set; }
        public DateTime SelectedDeliveryEnd { get; set; }
        public string PickupName { get; set; }
        public string PickupInstructions { get; set; }
        public string DropoffInstructions { get; set; }
        public int QBBillingType { get; set; }

        [Display(Name = "AP Email")]
        public string APEmail { get; set; }
        public string APContactName { get; set; }
        public string APPhone { get; set; }

        public int? SelectedCompany { get; set; }
        public IEnumerable<SelectListItem> GCParentCompanies { get; set; }
    }
}
