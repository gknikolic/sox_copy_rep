﻿namespace ProfitOptics.Framework.Core.Settings
{
	public class MonnitOptions
	{
		public string BaseUrl { get; set; }
		public string ResponseType { get; set; } = "json";
		public string AuthToken { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
	}
}
