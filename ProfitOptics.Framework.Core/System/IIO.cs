﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Core.System
{
    public interface IIO
    {
        /// <summary>
        /// Gets directory of executing assembly
        /// </summary>
        string GetAssemblyDir();

        /// <summary>
        /// Gets location of temp folder.
        /// </summary>
        /// <returns></returns>
        string GetTempFolder();

        /// <summary>
        /// Gets location of folder with attachments.
        /// </summary>
        /// <returns></returns>
        string GetAttachmentsFolder();

        /// <summary>
        /// Compares two files byte by byte
        /// Taken from: http://stackoverflow.com/questions/1358510/how-to-compare-2-files-fast-using-net
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        bool FilesAreEqual(string firstFileName, string secondFileName);
    }
}
