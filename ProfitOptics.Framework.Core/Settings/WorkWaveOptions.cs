﻿namespace ProfitOptics.Framework.Core.Settings
{
	public class WorkWaveOptions
    {
        public string BaseUrl { get; set; }
		public string Token { get; set; }
		public string ApiKey { get; set; }

	}
}
