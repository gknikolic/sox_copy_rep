﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCCaseConfig : IEntityTypeConfiguration<GCCase>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCCase> builder)
        {
            builder.ToTable("GCCase");
            builder.HasKey(entity => entity.Id);

            builder.HasOne<CTCase>(s => s.CTCase)
                .WithOne(gc => gc.GCCase)
                .HasForeignKey<GCCase>(gc => gc.CTCaseId)
                .HasConstraintName("FK_GCCase_CTCase");
        }
    }
}
