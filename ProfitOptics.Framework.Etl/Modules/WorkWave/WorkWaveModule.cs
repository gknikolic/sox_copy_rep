﻿using CommandLine;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Etl.Helpers;
using ProfitOptics.Framework.Etl.Models;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Services;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ProfitOptics.Framework.Etl.Modules.WorkWave
{
    [Verb(ProfitOptics.Modules.Sox.Helpers.Constants.ETL_WW_MODULE_NAME, HelpText = "Module for WorkWave Integration.")]
    public class WorkWaveModuleOptions : IOptions
    {
        [Option('a', "action", Required = true, HelpText = "Action type Enumerated value of type: WWActionType.")]
        public string Action { get; set; }
    }

    public class WorkWaveModule : IModule
    {
        private readonly IWorkWaveClient _workWaveClient;
        private readonly IEtlHelper _etlHelper;
        private readonly IWorkWaveService _workWaveService;

        public WorkWaveModule(IWorkWaveClient workWaveClient, IEtlHelper etlHelper, IWorkWaveService workWaveService)
        {
            _workWaveClient = workWaveClient;
            _etlHelper = etlHelper;
            _workWaveService = workWaveService;
        }

        public int Execute(IOptions opts)
        {
            //Log.Information("Started rendering template.");
            var wwOpts = opts as WorkWaveModuleOptions;
            WWActionType wwActionType = (WWActionType)(int.Parse(wwOpts.Action));
            ETLLogs etlLog = new ETLLogs();
            etlLog.Source = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_WW_MODULE_NAME;
            etlLog.FileName = "no file";
            etlLog.Username = "no username";
            etlLog.Status = ETLStatusType.OK.ToString();
            etlLog.Message = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE;
            etlLog.RowCount = 0;
            etlLog.ActionType = (int)wwActionType;
            etlLog.Options = $@" Action: {wwOpts.Action};";
            etlLog.Description = string.Empty;

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            try
			{
				switch (wwActionType)
				{
					case WWActionType.SyncOrder: //get orders from WW and creates new in ww if missing
                        var res = _workWaveService.SyncOrdersWithWorkWaveAsync().GetAwaiter().GetResult();
                        etlLog.Description = res;
                        break;
					case WWActionType.SyncRoutes:
                        TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                        DateTime cstTime = TimeZoneInfo.ConvertTime(DateTime.Now, cstZone);
                        _workWaveClient.SyncRoutesWithWorkWaveAsync(cstTime).GetAwaiter().GetResult();
                        _workWaveClient.SyncImagesWithWorkWave().GetAwaiter().GetResult();
                        break;
                    case WWActionType.DeleteOldETLLogs:
                        _workWaveClient.DeleteOldEtlLogs();
                        break;
                    case WWActionType.GetOrdersFromWW: //only gets existing orders from ww
                        _workWaveClient.GetOrdersFromWorWaveAsync().GetAwaiter().GetResult();
                        break;
                    case WWActionType.GetLatestGPSDeviceSamples: //gets current GPS device readings
                        _workWaveService.GetGPSDevicesCurrentPos().GetAwaiter().GetResult();
                        break;
                    case WWActionType.SyncGPSDevices: //gets current GPS device info
                        _workWaveService.SyncGPSDevices().GetAwaiter().GetResult();
                        break;
                    default:
						break;
				}
			}
            catch(Exception ex)
			{
                etlLog.Status = ETLStatusType.Error.ToString();
                etlLog.Message = ex.Message;
                etlLog.Description = ex.StackTrace;
            }
            finally
			{
                stopwatch.Stop();
                etlLog.Description += $" | Time elapsed: { stopwatch.Elapsed }";
                etlLog.ProcessingTime = DateTime.UtcNow;
                _etlHelper.LogProcessedAction(etlLog.Username, etlLog.Source, etlLog.FileName, etlLog.ProcessingTime.ToString(), etlLog.Status, etlLog.RowCount, etlLog.Message, etlLog.ActionType, etlLog.Options, etlLog.Description);
            }

            return 0;
        }

    }

}
