﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ProfitOptics.Modules.Support.Models.ViewModels
{
    public class CreateIssueViewModel
    {
        [Display(Name = "Project")]
        [Required]
        public string Project { get; set; }

        [Display(Name = "Component")]
        public string Component { get; set; }

        [Display(Name = "Title")]
        [Required]
        public string Summary { get; set; }

        [Display(Name = "Epic Name")]
        public string EpicName { get; set; }

        [Display(Name = "Description")]
        [Required]
        public string Description { get; set; }

        [Display(Name = "Type")]
        [Required]
        public string Type { get; set; }

        [Display(Name = "Priority")]
        [Required]
        public string Priority { get; set; }

        [Display(Name = "Environment")]
        public string Environment { get; set; }

        [Display(Name = "Parent Issue")]
        public string ParentIssueKey { get; set; }

        [Display(Name = "Support Message")]
        public string SupportMessage { get; set; }

        [Display(Name = "Attachments")]
        public IEnumerable<IFormFile> Attachments { get; set; }

        public List<SelectListItem> TypeList { get; set; }
        public List<SelectListItem> ProjectList { get; set; }
        public List<SelectListItem> ComponentList { get; set; }
        public List<SelectListItem> PriorityList { get; set; }
    }
}