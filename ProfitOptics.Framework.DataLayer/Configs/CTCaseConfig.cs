﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTCaseConfig : IEntityTypeConfiguration<CTCase>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTCase> builder)
        {
            builder.ToTable("CTCase");
            builder.HasKey(entity => entity.Id);
            builder.Property(e => e.CaseReference).IsRequired();
            builder.Property(e => e.DueTimestamp).IsRequired();
        }
    }
}
