﻿namespace ProfitOptics.Framework.Web.Framework.Models.Select2
{
    public class Select2Model
    {
        public string id { get; set; }
        public string text { get; set; }
        public bool selected { get; set; }
    }
}
