﻿using DataBrowser;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.DataBrowser.Factories;
using ProfitOptics.Modules.DataBrowser.Services;

namespace ProfitOptics.Modules.DataBrowser.Infrastructure
{
    public sealed class DataBrowserServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            StartupConfiguration.ConfigureStartupConfig<DataBrowserSettings>(services, configuration);

            services.AddScoped<IDataBrowserConfigurationService, DataBrowserConfigurationService>();
            services.AddScoped<IDataBrowserValidatorService, DataBrowserValidatorService>();
            services.AddScoped<IDataBrowserModelFactory, DataBrowserModelFactory>();
        }
    }
}
