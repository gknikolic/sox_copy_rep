﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class LifecycleCategory
	{
		public int Id { get; set; }
		public string Title { get; set; }
        public int Order { get; set; }
		public List<LifecycleStatusModel> LifecycleStatuses { get; set; }

		public int Percentage { get; set; }
		public bool IsFinished { get; set; }
		public bool IsDisabled { get; set; }
	}
}
