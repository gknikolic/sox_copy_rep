﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCChildCompanyConfig : IEntityTypeConfiguration<GCChildCompany>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCChildCompany> builder)
        {
            builder.ToTable("GCChildCompany");
            builder.HasKey(entity => entity.Id);
        }
    }
}
