﻿using System;

namespace ProfitOptics.Framework.Etl.Models
{
    public class EtlLogStatus
    {
        public string Status { get; set; }
        public string TableName { get; set; }
        public DateTime ProcessingTime { get; set; }
    }
}
