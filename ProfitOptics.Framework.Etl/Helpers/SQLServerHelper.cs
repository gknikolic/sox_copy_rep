﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using ProfitOptics.Framework.Etl.Models;
using Serilog;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public class SQLServerHelper: ISQLServerHelper
    {

        private readonly Settings _settings;

        public SQLServerHelper(Settings settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Backups a database to a file
        /// </summary>
        /// <param name="csb"></param>
        /// <param name="destinationPath"></param>
        public void BackupDatabase(SqlConnectionStringBuilder csb, string destinationPath)
        {
            ServerConnection connection = new ServerConnection(csb.DataSource, csb.UserID, csb.Password);
            Server sqlServer = new Server(connection);

            Backup bkpDatabase = new Backup();
            bkpDatabase.Action = BackupActionType.Database;
            bkpDatabase.Database = csb.InitialCatalog;

            BackupDeviceItem bkpDevice = new BackupDeviceItem(destinationPath, DeviceType.File);
            bkpDatabase.Devices.Add(bkpDevice);
            bkpDatabase.SqlBackup(sqlServer);
            connection.Disconnect();

        }

        /// <summary>
        /// Restores a database from a file
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="backUpFile"></param>
        public void RestoreDatabase(string serverName, string databaseName, string userName, string password, string backUpFile)
        {
            var dbFileLocations = ReadDbMdfAndLogLocation(serverName, databaseName, userName, password);

            if (dbFileLocations.MdfLocation == null || dbFileLocations.LogLocation == null)
                throw new Exception("The database you're trying to restore to doesn't exist.");

            Log.Information($"Restoring backup to {databaseName}");

            ServerConnection connection = new ServerConnection(serverName, userName, password);
            connection.StatementTimeout = 2400;
            Server sqlServer = new Server(connection);

            sqlServer.KillAllProcesses(databaseName);

            Restore rstDatabase = new Restore();
            rstDatabase.Action = RestoreActionType.Database;
            rstDatabase.Database = databaseName;


            BackupDeviceItem bkpDevice = new BackupDeviceItem(backUpFile, DeviceType.File);
            rstDatabase.Devices.Add(bkpDevice);
            rstDatabase.ReplaceDatabase = true;

            DataTable logicalRestoreFiles = rstDatabase.ReadFileList(sqlServer);
            rstDatabase.RelocateFiles.Add(new RelocateFile(logicalRestoreFiles.Rows[0][0].ToString(), dbFileLocations.MdfLocation));
            rstDatabase.RelocateFiles.Add(new RelocateFile(logicalRestoreFiles.Rows[1][0].ToString(), dbFileLocations.LogLocation));

            rstDatabase.SqlRestore(sqlServer);
        }

        /// <summary>
        /// Shrinks all of the log files to a specific size
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="size"></param>
        /// <param name="shrinkMethod"></param>
        public void ShrinkLogFiles(string serverName, string databaseName, string userName, string password, int size, ShrinkMethod shrinkMethod)
        {
            Log.Information($"Shrinking log files for: {databaseName}");

            ServerConnection connection = new ServerConnection(serverName, userName, password);
            Server sqlServer = new Server(connection);

            sqlServer.KillAllProcesses(databaseName);

            Database database = sqlServer.Databases[databaseName];

            if (database == null)
                throw new Exception("The database you're trying to shrink doesn't exist.");

            for (int i = 0; i < database.LogFiles.Count; ++i)
                database.LogFiles[i].Shrink(size, shrinkMethod);

        }

        /// <summary>
        /// Changes the recovery model for the specific db.
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="recoveryModel"></param>
        public void ChangeDbRecoveryModel(string serverName, string databaseName, string userName, string password, RecoveryModel recoveryModel)
        {
            Log.Information($"Changing recovery mode for: {databaseName}");

            ServerConnection connection = new ServerConnection(serverName, userName, password);
            Server sqlServer = new Server(connection);

            sqlServer.KillAllProcesses(databaseName);

            Database database = sqlServer.Databases[databaseName];

            if (database == null)
                throw new Exception("The database you're trying to change the recovery mode for doesn't exist.");

            database.RecoveryModel = recoveryModel;
            database.Alter();
        }

        /// <summary>
        /// Queries the location of the mdf and log files
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public DbFilesLocationModel ReadDbMdfAndLogLocation(string serverName, string databaseName, string userName, string password)
        {
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();

            csb.DataSource = serverName;
            csb.InitialCatalog = databaseName;
            csb.UserID = userName;
            csb.Password = password;

            using (var connection = new SqlConnection(csb.ConnectionString))
            {
                DbFilesLocationModel filesLocationModel = new DbFilesLocationModel();

                SqlCommand command = connection.CreateCommand();
                connection.Open();

                command.CommandText = @"
                select physical_name, type from sys.database_files
                ";

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (reader.GetByte(1) == 0)
                            filesLocationModel.MdfLocation = reader.GetString(0);
                        else
                            filesLocationModel.LogLocation = reader.GetString(0);
                    }
                }


                return filesLocationModel;

            }
        }

        /// <summary>
        /// Adds a user to the db and assignes it a dbo_owner role
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="user"></param>
        public void AddUserToDb(string serverName, string databaseName, string userName, string password, string user)
        {
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();

            csb.DataSource = serverName;
            csb.InitialCatalog = databaseName;
            csb.UserID = userName;
            csb.Password = password;

            using (var connection = new SqlConnection(csb.ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = $@"
                    CREATE USER [{user}] FOR LOGIN [{user}]
                    ";

                    command.ExecuteNonQuery();

                    command.CommandText = $@"
                    EXEC sp_addrolemember N'db_owner', '{user}'
                    ";

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
