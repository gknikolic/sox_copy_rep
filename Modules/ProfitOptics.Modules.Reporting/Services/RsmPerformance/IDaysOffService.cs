﻿using System;
using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels;

namespace ProfitOptics.Modules.Reporting.Services.RsmPerformance
{
    public interface IDaysOffService
    {
        List<DateTime> GetHolidayDatesForCurrentYear();

        List<DateTime> GetHolidayDatesForYear(int year);
        
        List<DateTime> GetAllNonWorkDatesForCurrentYear();

        List<DateTime> GetAllNonWorkDatesForYear(int year);

        IEnumerable<DateTime> GetAllNonWorkDatesForCurrentYearToDate();

        IEnumerable<DateTime> GetAllNonWorkDatesForYearToDate(int year, DateTime date);

        IEnumerable<DateTime> GetAllWorkDaysForMonth(int year, int month);

        IEnumerable<DateTime> GetAllWorkDaysForMonthToDate(int year, int month);

        IEnumerable<DateTime> GetRemainingWorkDays(int year, int month);

        string GetMonthName(int month);

        IEnumerable<string> GetPreviousMonthNames(int month);

        MonthDetails GetMonthDetails(int year, int month);

        int GetQuarterNumber(int monthNumber);

        int GetQuarterNumber(string monthName);

        void GetCurrentQuarterDetails(int year, int month, out DateTime quarterStart, out DateTime quarterEnd,
            out int currentQuarter);
    }
}