USE [Sox]

ALTER TABLE [Sox].[dbo].[CTContainerService] ADD [GCProcedureTypeId] INT NULL;
ALTER TABLE [Sox].[dbo].[CTContainerService] ADD [ICD9] NVARCHAR(20) NULL;
ALTER TABLE [Sox].[dbo].[CTContainerService] ADD [ICD10] NVARCHAR(20) NULL;

GO

ALTER TABLE [dbo].[CTContainerService]  WITH CHECK ADD  CONSTRAINT [FK_CTContainerService_GCProcedureType] FOREIGN KEY([GCProcedureTypeId])
REFERENCES [dbo].[GCProcedureType] ([Id])
GO
ALTER TABLE [dbo].[CTContainerService] CHECK CONSTRAINT [FK_CTContainerService_GCProcedureType]
GO
