﻿using Newtonsoft.Json;
using System;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTQualityFeedItemGraphic
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("referenceId")]
        public long ReferenceId { get; set; }

        [JsonProperty("graphicKey")]
        public ETLCTGraphicKey GraphicKey { get; set; }

        [JsonProperty("updateTimestamp")]
        public DateTime? UpdateTimestamp { get; set; }

        [JsonProperty("updateUserId")]
        public long updateUserId { get; set; }
    }
}
