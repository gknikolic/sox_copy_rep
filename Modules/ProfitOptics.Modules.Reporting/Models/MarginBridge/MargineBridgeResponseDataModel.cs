﻿using ProfitOptics.Modules.Reporting.Attributes;
using ProfitOptics.Modules.Reporting.Enums;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class MargineBridgeResponseDataModel
    {
        public List<ProductHierarchyFilterModel> ProductsHierarchy { get; set; }

        public List<LocationHierarchyFilterModel> LocationsHierarchy { get; set; }

        public List<ProductHierarchyFilterModel> ProductsHierarchyPermanent { get; set; }

        public List<LocationHierarchyFilterModel> LocationsHierarchyPermanent { get; set; }

        public List<ProductHierarchyFilterModel> ProductsHierarchyInitial { get; set; }

        public List<LocationHierarchyFilterModel> LocationsHierarchyInitial { get; set; }

        public string PeriodId { get; set; }

        public MarginBridgePeriod FirstPeriod { get; set; }

        public MarginBridgePeriod SecondPeriod { get; set; }

        public string HeaderCaption { get; set; }

        public List<StatisticsResponse> AllResults { get; set; }

        public StatisticsResponse Totals { get; set; }

        public StatisticsResponse TotalsPermanent { get; set; }

        public List<FullOutput> FullOutputData { get; set; }

        public Dictionary<LocationLevelType, BreadcrumbItem> LocationsBreadcrumbs { get; set; }

        public Dictionary<ProductLevelType, BreadcrumbItem> ProductsBreadcrumbs { get; set; }

        public LocationLevelType LocationLevel { get; set; }

        public string LocationValue { get; set; }

        public ProductLevelType ProductLevel { get; set; }

        public string ProductValue { get; set; }

        public GroupByType GroupedBy { get; set; }

        public bool GroupedByMax { get; set; }

        public LocationLevelType MaxLocationLevel { get; set; }

        public string MaxLocationLevelString { get; set; }

        public ProductLevelType MaxProductLevel { get; set; }

        public string MaxProductLevelString { get; set; }

        public int NumberOfRows { get; set; }

        public bool CanDrillThrough { get; set; }

        public List<List<string>> CheckedNodes { get; set; }

        public List<List<string>> CheckedNodesInitial { get; set; }

        public List<OutputDataPoint> AlgorithmOutput { get; set; }

        public bool DownloadFinished { get; set; }

        public bool IncludeNLItems { get; set; }

        public bool IncludeSDItems { get; set; }

        public bool IncludeListedItems { get; set; }

        public PricingLevelsModel PricingLevels { get; set; }

        public MarginBridgeType MarginBridgeType { get; set; }
    }

    public class StatisticsData
    {
        public string BusinessUnitNew { get; set; }

        public string SuperDivision { get; set; }

        public string Region { get; set; }

        public string Area { get; set; }

        public string LocationNameEnglish { get; set; }

        public string CoreFocus { get; set; }

        public string PGSGBusinessUnit { get; set; }

        public string PGSGCategory { get; set; }

        public string ProductGroup { get; set; }

        public string PGSG { get; set; }

        public StatisticsResponse Response { get; set; }
    }

    public class AlgorithmOutputDataPoint
    {
        [FieldTitle("LocationKey")]
        public string LocationKey { get; set; }

        [FieldTitle("ProductKey")]
        public string ProductKey { get; set; }

        [FieldTitle("Price", ColumnType.PercentFour)]
        public double Price { get; set; }

        [FieldTitle("Cost", ColumnType.PercentFour)]
        public double Cost { get; set; }

        [FieldTitle("Volume", ColumnType.PercentFour)]
        public double Volume { get; set; }

        [FieldTitle("ProductMix", ColumnType.PercentFour)]
        public double ProductMix { get; set; }

        [FieldTitle("RebateRate", ColumnType.PercentFour)]
        public double RebateRate { get; set; }

        [FieldTitle("RebateVolume", ColumnType.PercentFour)]
        public double RebateVolume { get; set; }

        [FieldTitle("RebateProductMix", ColumnType.PercentFour)]
        public double RebateProductMix { get; set; }
    }

    public class AlgorithmOutputDataPointDolar
    {
        [FieldTitle("LocationKey")]
        public string LocationKey { get; set; }

        [FieldTitle("ProductKey")]
        public string ProductKey { get; set; }

        [FieldTitle("Price", ColumnType.DollarsInThousands)]
        public double Price { get; set; }

        [FieldTitle("Cost", ColumnType.DollarsInThousands)]
        public double Cost { get; set; }

        [FieldTitle("Volume", ColumnType.DollarsInThousands)]
        public double Volume { get; set; }

        [FieldTitle("ProductMix", ColumnType.DollarsInThousands)]
        public double ProductMix { get; set; }

        [FieldTitle("RebateRate", ColumnType.DollarsInThousands)]
        public double RebateRate { get; set; }

        [FieldTitle("RebateVolume", ColumnType.DollarsInThousands)]
        public double RebateVolume { get; set; }

        [FieldTitle("RebateProductMix", ColumnType.DollarsInThousands)]
        public double RebateProductMix { get; set; }
    }

    public class StatisticsResponse
    {
        [FieldTitle("Caption")]
        public string Caption { get; set; }

        [FieldTitle("PGSG Description")]
        public string Description { get; set; }

        [FieldTitle("P1Sales", ColumnType.Dollars)]
        public decimal Period1Sales { get; set; }

        public decimal Period1RebateCost { get; set; }

        [FieldTitle("P2Sales", ColumnType.Dollars)]
        public decimal Period2Sales { get; set; }

        public decimal Period2RebateCost { get; set; }

        [FieldTitle("StartMargin", ColumnType.Percent)]
        public decimal StartMargin { get; set; }

        [FieldTitle("Price", ColumnType.PercentThree)]
        public double Price { get; set; }

        [FieldTitle("Cost", ColumnType.PercentThree)]
        public double Cost { get; set; }

        [FieldTitle("Volume", ColumnType.PercentThree)]
        public double Volume { get; set; }

        [FieldTitle("ProductMix", ColumnType.PercentThree)]
        public double ProductMix { get; set; }

        [FieldTitle("RebateRate", ColumnType.PercentThree)]
        public double RebateRate { get; set; }

        [FieldTitle("RebateVol", ColumnType.PercentThree)]
        public double RebateVol { get; set; }

        [FieldTitle("RebateProdMix", ColumnType.PercentThree)]
        public double RebateProdMix { get; set; }

        [FieldTitle("TotalImpact", ColumnType.PercentThree)]
        public double TotalImpact { get; set; }

        [FieldTitle("EndMargin", ColumnType.Percent)]
        public decimal EndMargin { get; set; }

        //Utility
        public string Period1SalesFormatted => $"$ {Period1Sales:n0}";

        public string Period2SalesFormatted => $"$ {Period2Sales:n0}";

        public string StartMarginFormatted => $"{StartMargin:0.00} %";

        public string PriceFormatted => $"{Price:0.000} %";

        public string CostFormatted => $"{Cost:0.000} %";

        public string VolumeFormatted => $"{Volume:0.000} %";

        public string ProductMixFormatted => $"{ProductMix:0.000} %";

        public string RebateRateFormatted => $"{RebateRate:0.000} %";

        public string RebateVolFormatted => $"{RebateVol:0.000} %";

        public string RebateProdMixFormatted => $"{RebateProdMix:0.000} %";

        public string TotalImpactFormatted => $"{TotalImpact:0.000} %";

        public string EndMarginFormatted => $"{EndMargin:0.00} %";
    }

    public class StatisticsResponseDollar
    {
        [FieldTitle("Caption")]
        public string Caption { get; set; }

        public string Description { get; set; }

        [FieldTitle("P1Sales", ColumnType.Dollars)]
        public decimal Period1Sales { get; set; }

        public decimal Period1RebateCost { get; set; }

        [FieldTitle("P2Sales", ColumnType.Dollars)]
        public decimal Period2Sales { get; set; }

        public decimal Period2RebateCost { get; set; }

        [FieldTitle("StartMargin", ColumnType.DollarsInThousands)]
        public decimal StartMargin { get; set; }

        [FieldTitle("Price", ColumnType.DollarsInThousands)]
        public double Price { get; set; }

        [FieldTitle("Cost", ColumnType.DollarsInThousands)]
        public double Cost { get; set; }

        [FieldTitle("Volume", ColumnType.DollarsInThousands)]
        public double Volume { get; set; }

        [FieldTitle("ProductMix", ColumnType.DollarsInThousands)]
        public double ProductMix { get; set; }

        [FieldTitle("RebateRate", ColumnType.DollarsInThousands)]
        public double RebateRate { get; set; }

        [FieldTitle("RebateVol", ColumnType.DollarsInThousands)]
        public double RebateVol { get; set; }

        [FieldTitle("RebateProdMix", ColumnType.DollarsInThousands)]
        public double RebateProdMix { get; set; }

        [FieldTitle("TotalImpact", ColumnType.DollarsInThousands)]
        public double TotalImpact { get; set; }

        [FieldTitle("EndMargin", ColumnType.DollarsInThousands)]
        public decimal EndMargin { get; set; }
    }

    public class StatisticsResponseHierarchy
    {
        public StatisticsResponse Response { get; set; }

        public List<StatisticsResponseHierarchy> SubItems { get; set; }
    }
}