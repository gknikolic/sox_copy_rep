using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Framework.Web.RestApi.Resources.Customer
{
    public class SaveCustomerResource
    {
        [Required]
        [MaxLength(150)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Address { get; set; }

        [Required]
        public int DistributorId { get; set; }

        public List<int> ProductIds { get; set; }
    }
}