﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.Web.RestApi.Models.Product;

namespace ProfitOptics.Framework.Web.RestApi.DataLayer
{
    public class ApiEntitiesContext : DbContext
    {
        public DbSet<Distributor> Distributors { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Customer> Customers { get; set; }

        public DbSet<AspNetUsers> AspNetUsers { get; set; }
        public DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public DbSet<AspNetRoles> AspNetRoles { get; set; }

        public ApiEntitiesContext(DbContextOptions<ApiEntitiesContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Distributor

            builder.Entity<Distributor>().ToTable("Distributors");
            builder.Entity<Distributor>().HasKey(p => p.Id);
            builder.Entity<Distributor>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Distributor>().Property(p => p.Name).IsRequired().HasMaxLength(150);
            builder.Entity<Distributor>()
                .HasMany(p => p.Customers)
                .WithOne(p => p.Distributor)
                .HasForeignKey(p => p.DistributorId);

            builder.Entity<Distributor>().HasData
            (
                new Distributor { Id = 100, Name = "ProfitOptics" }, 
                new Distributor { Id = 101, Name = "Competition" }
            );

            // Product

            builder.Entity<Product>().ToTable("Products");
            builder.Entity<Product>().HasKey(p => p.Id);
            builder.Entity<Product>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Product>().Property(p => p.Name).IsRequired().HasMaxLength(50);
            builder.Entity<Product>().Property(p => p.QuantityInPackage).IsRequired();
            builder.Entity<Product>().Property(p => p.UnitOfMeasurement).IsRequired();
            builder.Entity<Product>().Property(p => p.Price).HasColumnType("decimal(18,2)");

            builder.Entity<Product>().HasData
            (
                new Product
                {
                    Id = 100,
                    Name = "Web Site",
                    QuantityInPackage = 2,
                    UnitOfMeasurement = EUnitOfMeasurement.Unity,
                    Price = 2500
                },
                new Product
                {
                    Id = 101,
                    Name = "Advice",
                    QuantityInPackage = 1,
                    UnitOfMeasurement = EUnitOfMeasurement.Unity,
                },
                new Product
                {
                    Id = 102,
                    Name = "Enterprise Solution Portal",
                    QuantityInPackage = 1,
                    UnitOfMeasurement = EUnitOfMeasurement.Unity,
                    Price = 10000
                }
            );

            builder.Entity<DistributorProduct>()
                .HasKey(bc => new { bc.DistributorId, bc.ProductId });
            builder.Entity<DistributorProduct>()
                .HasOne(bc => bc.Distributor)
                .WithMany(b => b.DistributorProducts)
                .HasForeignKey(bc => bc.DistributorId);
            builder.Entity<DistributorProduct>()
                .HasOne(bc => bc.Product)
                .WithMany(c => c.DistributorProducts)
                .HasForeignKey(bc => bc.ProductId);

            builder.Entity<DistributorProduct>().HasData
            (
                new DistributorProduct
                {
                    DistributorId = 100,
                    ProductId = 100
                },
                new DistributorProduct
                {
                    DistributorId = 100,
                    ProductId = 101
                },
                new DistributorProduct
                {
                    DistributorId = 100,
                    ProductId = 102
                },
                new DistributorProduct
                {
                    DistributorId = 101,
                    ProductId = 101
                }
            );

            // Customer

            builder.Entity<Customer>().ToTable("Customers");
            builder.Entity<Customer>().HasKey(p => p.Id);
            builder.Entity<Customer>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Customer>().Property(p => p.Name).IsRequired().HasMaxLength(150);
            builder.Entity<Customer>()
                .HasOne(p => p.Distributor)
                .WithMany(p => p.Customers)
                .HasForeignKey(p => p.DistributorId);

            builder.Entity<Customer>().HasData
            (
                new Customer
                {
                    Id = 100,
                    Name = "Paying Customer",
                    Address = "Neverland City, FantasyLand",
                    DistributorId = 100
                },
                new Customer
                {
                    Id = 101,
                    Name = "Non-Paying Customer",
                    Address = "Real City, World",
                    DistributorId = 101
                }
            );

            builder.Entity<CustomerProduct>()
                .HasKey(bc => new { bc.CustomerId, bc.ProductId });
            builder.Entity<CustomerProduct>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.CustomerProducts)
                .HasForeignKey(bc => bc.CustomerId);
            builder.Entity<CustomerProduct>()
                .HasOne(bc => bc.Product)
                .WithMany(c => c.CustomerProducts)
                .HasForeignKey(bc => bc.ProductId);

            builder.Entity<CustomerProduct>().HasData
            (
                new CustomerProduct
                {
                    CustomerId = 100,
                    ProductId = 100
                },
                new CustomerProduct
                {
                    CustomerId = 100,
                    ProductId = 101
                },
                new CustomerProduct
                {
                    CustomerId = 100,
                    ProductId = 102
                },
                new CustomerProduct
                {
                    CustomerId = 101,
                    ProductId = 100
                },
                new CustomerProduct
                {
                    CustomerId = 101,
                    ProductId = 101
                },
                new CustomerProduct
                {
                    CustomerId = 101,
                    ProductId = 102
                }
            );

            // in case you want to name the many to many table and ids, outside conventions
            //modelBuilder.Entity<Student>()
            //    .HasMany<Course>(s => s.Courses)
            //    .WithMany(c => c.Students)
            //    .Map(cs =>
            //    {
            //        cs.MapLeftKey("StudentRefId");
            //        cs.MapRightKey("CourseRefId");
            //        cs.ToTable("StudentCourse");
            //    });

            builder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.UserName)
                    .HasName("UserNameIndex")
                    .IsUnique();

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.LockoutEndDateUtc).HasColumnType("datetime");

                entity.Property(e => e.PasswordHash).HasMaxLength(255);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.SecurityStamp).HasMaxLength(255);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            builder.Entity<AspNetUserRoles>(entity =>
            {
                builder.Entity<AspNetUserRoles>().HasKey(p => new { p.UserId, p.RoleId });
                entity.Property(e => e.UserId)
                    .IsRequired();
                entity.Property(e => e.RoleId)
                    .IsRequired();
            });

            builder.Entity<AspNetRoles>(entity =>
            {
                builder.Entity<AspNetRoles>().HasKey(p => p.Id);
                entity.Property(e => e.Id)
                    .IsRequired();
                entity.Property(e => e.Name)
                    .IsRequired().HasMaxLength(256);
                entity.Property(e => e.NormalizedName)
                    .IsRequired().HasMaxLength(256);
            });
        }
    }
}
