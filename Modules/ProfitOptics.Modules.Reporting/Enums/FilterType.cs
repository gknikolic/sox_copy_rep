﻿namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum FilterType
    {
        Zone = 0,
        Region = 1,
        CustomerClass = 2,
        ItemCategory = 3,
        Vendor = 4,
        BuyGroup = 5,
        SalesRep = 6,
        BillTo = 7,
        ShipTo = 8
    }
}
