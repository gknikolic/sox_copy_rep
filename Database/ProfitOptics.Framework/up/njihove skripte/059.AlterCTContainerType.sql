USE [Sox]

BEGIN TRANSACTION

	ALTER TABLE [dbo].[CTContainerType] DROP CONSTRAINT [FK_CTContainerType_CTContainerService]
	ALTER TABLE [dbo].[CTContainerType] ALTER COLUMN CTConainerServiceId INT NULL;
	UPDATE [dbo].[CTContainerType] SET CTConainerServiceId = NULL;
	ALTER TABLE [dbo].[CTContainerType] DROP COLUMN CTConainerServiceId;
	ALTER TABLE [dbo].[CTContainerType] ADD GCVendorId INT NULL;

	ALTER TABLE [dbo].[CTContainerType]  WITH CHECK ADD  CONSTRAINT [CTContainerType_CTContainerType] FOREIGN KEY([GCVendorId])
	REFERENCES [dbo].[GCVendor] ([Id])
	GO
	ALTER TABLE [dbo].[CTContainerType] CHECK CONSTRAINT [CTContainerType_CTContainerType]
	GO

	DROP TABLE [CTContainerService]
	
COMMIT TRANSACTION