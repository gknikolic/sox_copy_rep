﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCSystemAlertModel
    {
        public int Id { get; set; }
        public int GCTrayId { get; set; }
        public int GCCaseId { get; set; }
        public int GCSystemAlertTypeId { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public string CaseReference { get; set; }
        public string ActualsName { get; set; }
        public string ActualsParentName { get; set; }
        public string ActualsFullName { get { return $"{ActualsParentName} {ActualsName}"; } }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? MarkedAsReadAtUtc { get; set; }
    }
}
