﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Services
{
    public interface IHelperService
    {
        string GetQuickBooksAccessToken(int userId);
        void RevokeQBAccessTokenAsync(int userId);
    }
}
