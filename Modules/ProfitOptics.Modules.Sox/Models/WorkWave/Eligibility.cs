﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class Eligibility
	{
		
		public EligibilityType EligibilitType { get; set; }

		[JsonProperty("type")]
		public string Type { get { return EligibilitType.ToString(); } }

		[JsonProperty("byDate")]
		public string ByDate { get; set; }

		[JsonProperty("onDates")]
		public List<string> onDates { get; set; }
	}
}
