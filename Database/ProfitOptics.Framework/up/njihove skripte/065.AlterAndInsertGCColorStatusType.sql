USE [Sox]

ALTER TABLE [dbo].[GCColorStatusType] ALTER COLUMN [Code] INT NOT NULL;

INSERT INTO [dbo].[GCColorStatusType] ([Id],[Name],[Code]) VALUES (1, 'Blue', 100)
INSERT INTO [dbo].[GCColorStatusType] ([Id],[Name],[Code]) VALUES (2, 'Purple', 200)
INSERT INTO [dbo].[GCColorStatusType] ([Id],[Name],[Code]) VALUES (3, 'Red', 300)
INSERT INTO [dbo].[GCColorStatusType] ([Id],[Name],[Code]) VALUES (4, 'Orange', 400)
INSERT INTO [dbo].[GCColorStatusType] ([Id],[Name],[Code]) VALUES (5, 'Yellow', 500)
INSERT INTO [dbo].[GCColorStatusType] ([Id],[Name],[Code]) VALUES (6, 'Green', 600)