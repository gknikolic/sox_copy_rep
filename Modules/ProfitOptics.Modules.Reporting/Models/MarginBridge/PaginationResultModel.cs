﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class PaginationResultModel<T>
    {
        public int TotalCount { get; set; }
        public List<T> Result { get; set; }

        public T SummaryRow { get; set; }
    }
}