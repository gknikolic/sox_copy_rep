﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using Rotativa.NetCore;

namespace ProfitOptics.Framework.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("DashboardNew", "Sox", new { area = "Sox" });
        }

        [HttpPost]
        public JsonResult LogJavaScriptError(string message)
        {
            return Json(true);
        }

        [HttpPost]
        public JsonResult ToggleSidebar()
        {
            var setting = HttpContext.Session.GetObject<string>(SessionKeys.EnabledSidebar);

            var settingvalue = string.IsNullOrWhiteSpace(setting)
                ? "sidebar-collapse"
                : string.Empty;

            HttpContext.Session.SetObject(SessionKeys.EnabledSidebar, settingvalue);

            return Json(new { Success = true });
        }

        public ActionResult PrintViewIndex()
        {
            return new ViewAsPdf("Index") { FileName = "Test.pdf" };
        }

        public ActionResult PrintActionIndex()
        {
            return new ActionAsPdf("Index") { FileName = "Test.pdf" };
        }
    }
}