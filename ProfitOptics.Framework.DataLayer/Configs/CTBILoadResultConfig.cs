﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTBILoadResultConfig : IEntityTypeConfiguration<CTBILoadResult>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTBILoadResult> builder)
        {
            builder.ToTable("CTBILoadResult");
            builder.HasKey(entity => entity.Id);
        }
    }
}
