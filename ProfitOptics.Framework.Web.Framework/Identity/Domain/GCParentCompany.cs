﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Identity.Domain
{
    public class GCParentCompany
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public virtual ICollection<GCChildCompany> GCChildCompanies { get; set; }
        public virtual ICollection<GCParentCompanyUsers> GCParentCompanyUsers { get; set; }
    }
}
