﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.BackgroundTasks.Data.Domain;

namespace ProfitOptics.Modules.BackgroundTasks.Data.Configs
{
    public class BackgroundTaskConfig : IEntityTypeConfiguration<BackgroundTask>
    {
        public void Configure(EntityTypeBuilder<BackgroundTask> builder)
        {
            builder.ToTable("POBackgroundTask");

            builder.Property(e => e.DateEnd).HasColumnType("datetime");
            builder.Property(e => e.DateStart).HasColumnType("datetime");
            builder.Property(e => e.Description).IsRequired().HasMaxLength(255).IsUnicode(false);
            builder.Property(e => e.Progress).HasColumnType("decimal(19, 4)");

            builder.HasOne<AspNetUser>(d => d.User)
                .WithMany(p => p.BackgroundTasks)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_POBackgroundTask_AspNetUsers");
        }
    }
}
