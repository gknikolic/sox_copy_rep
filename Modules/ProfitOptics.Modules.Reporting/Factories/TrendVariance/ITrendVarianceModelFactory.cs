﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.TrendVariance;

namespace ProfitOptics.Modules.Reporting.Factories.TrendVariance
{
    public interface ITrendVarianceModelFactory
    {
        /// <summary>
        /// Creates the trend variance view model.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        TrendVarianceViewModel CreateTrendVarianceViewModel(int userId);

        /// <summary>
        /// Creates the year over year variance models.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="yearsToShow"></param>
        /// <returns></returns>
        List<TrendVarianceYearOverYearVarianceModel> CreateYearOverYearVarianceModels(int userId, IDataTablesRequest request, int yearsToShow  = 3);

        /// <summary>
        /// Creates the variance models.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        PagedList<TrendVarianceVarianceByTypeModel> CreateVarianceModels(int userId, IDataTablesRequest request,
            TrendVarianceVarianceByTypeRequestParameters parameters);

        /// <summary>
        /// Creates the variance chart models.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="yearsToShow"></param>
        /// <returns></returns>
        List<TrendVarianceVarianceChartModel> CreateSalesHistoryForMonthModels(int userId, int yearsToShow = 3);

        /// <summary>
        /// Creates the customer drill through view model for the specified bill to id.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="billToId"></param>
        /// <param name="comparisonType"></param>
        /// <param name="comparisonStartDate"></param>
        /// <param name="comparisonEndDate"></param>
        /// <param name="urlHelper"></param>
        /// <returns></returns>
        TrendVarianceBillToCustomerDrillThroughViewModel CreateCustomerDrillThroughViewModelForBillToId(int userId, int billToId,
            ComparisonType comparisonType, DateTime? comparisonStartDate, DateTime? comparisonEndDate, IUrlHelper urlHelper);
        
        /// <summary>
        /// Creates the customer drill through view model for the specified bill to id.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="shipToId"></param>
        /// <param name="comparisonType"></param>
        /// <param name="comparisonStartDate"></param>
        /// <param name="comparisonEndDate"></param>
        /// <param name="urlHelper"></param>
        /// <returns></returns>
        TrendVarianceShipToCustomerDrillThroughViewModel CreateCustomerDrillThroughViewModelForShipToId(int userId, int shipToId,
            ComparisonType comparisonType, DateTime? comparisonStartDate, DateTime? comparisonEndDate, IUrlHelper urlHelper);

        /// <summary>
        /// Creates the variance models for the specified user identifier and bill to parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        PagedList<TrendVarianceVarianceByTypeModel> CreateVarianceModelsForBillToParameters(int userId,
            IDataTablesRequest request, TrendVarianceBillToDrillThroughVarianceParameters parameters);

        /// <summary>
        /// Creates the variance models for the specified user identifier and ship to parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        PagedList<TrendVarianceVarianceByTypeModel> CreateVarianceModelsForShipToParameters(int userId, 
            IDataTablesRequest request, TrendVarianceShipToDrillThroughVarianceParameters parameters);

        /// <summary>
        /// Creates the item category drill through view model for the specified item category and bill to identifiers.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="billToId"></param>
        /// <param name="itemCategoryId"></param>
        /// <param name="comparisonType"></param>
        /// <param name="comparisonStartDate"></param>
        /// <param name="comparisonEndDate"></param>
        /// <param name="urlHelper"></param>
        /// <returns></returns>
        TrendVarianceBillToItemCategoryDrillThroughViewModel CreateItemCategoryDrillThroughViewModelForItemCategoryIdAndBillToId(int userId,
            int billToId, int itemCategoryId, ComparisonType comparisonType, DateTime? comparisonStartDate, DateTime? comparisonEndDate, IUrlHelper urlHelper);

        /// <summary>
        /// Creates the item category drill through view model for the specified item category and ship to identifiers.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="shipToId"></param>
        /// <param name="itemCategoryId"></param>
        /// <param name="comparisonType"></param>
        /// <param name="comparisonStartDate"></param>
        /// <param name="comparisonEndDate"></param>
        /// <param name="urlHelper"></param>
        /// <returns></returns>
        TrendVarianceShipToItemCategoryDrillThroughViewModel CreateItemCategoryDrillThroughViewModelForItemCategoryIdAndShipToId(int userId,
            int shipToId, int itemCategoryId, ComparisonType comparisonType, DateTime? comparisonStartDate, DateTime? comparisonEndDate, IUrlHelper urlHelper);


        /// <summary>
        /// Creates the item variance models for the specified user identifier and bill to/item category parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        PagedList<TrendVarianceVarianceForItemModel> CreateItemVarianceModelsForBillToItemCategoryParameters(int userId,
            IDataTablesRequest request, TrendVarianceBillToItemCategoryDrillThroughParameters parameters);

        /// <summary>
        /// Creates the item variance models for the specified user identifier and ship to/item category parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        PagedList<TrendVarianceVarianceForItemModel> CreateItemVarianceModelsForShipToItemCategoryParameters(int userId,
            IDataTablesRequest request, TrendVarianceShipToItemCategoryDrillThroughParameters parameters);

        /// <summary>
        /// Creates the item category chart models.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="itemCategoryId"></param>
        /// <param name="billToId"></param>
        /// <param name="shipToId"></param>
        /// <returns></returns>
        List<TrendVarianceItemCategoryDrillThroughChartModel> CreateItemCategoryChartModels(int userId,
            int itemCategoryId, int billToId = 0, int shipToId = 0);
    }
}
