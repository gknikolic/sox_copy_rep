﻿using ProfitOptics.Framework.Web.Helpers.Attributes;
using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class RegisterModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} can't be longer than {1} characters.")]
        [Display(Name="First name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} can't be longer than {1} characters.")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        //[ValidDomain(ErrorMessage = "You are not allowed to register with an email address from this domain.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The {0} can't be longer than {1} characters.")]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
    }
}
