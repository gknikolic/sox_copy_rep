USE [Sox]

CREATE TABLE [dbo].[CTAssemblyCountSheet]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[ContainerTypeId] BIGINT NOT NULL,
	[VendorName] NVARCHAR(200) NULL,
	[ModelNumber] NVARCHAR(200) NULL,
	[SetName] NVARCHAR(200) NULL,
	[TrayLifecycleStatusTypeId] INT NOT NULL,
	[RequiredCount] INT NOT NULL,
	[ActualCount] BIGINT NOT NULL,
	[Description] NVARCHAR(MAX) NULL

CONSTRAINT [PK_CTAssemblyCountSheet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTAssemblyCountSheet]  WITH CHECK ADD  CONSTRAINT [FK_CTAssemblyCountSheet_GCTraysLifecycleStatusTypes] FOREIGN KEY([TrayLifecycleStatusTypeId])
REFERENCES [dbo].[GCTraysLifecycleStatusTypes] ([Id])
GO
ALTER TABLE [dbo].[CTAssemblyCountSheet] CHECK CONSTRAINT [FK_CTAssemblyCountSheet_GCTraysLifecycleStatusTypes]
GO
