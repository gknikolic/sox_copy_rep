USE [Sox]

CREATE TABLE CTCaseContainer
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[CTCaseId] [INT] NOT NULL,
	[ContainerId] [BIGINT] NOT NULL,
	[ContainerTypeId] [BIGINT] NOT NULL,
	[CaseReference] [NVARCHAR](50) NOT NULL,
	[CaseCartId] [BIGINT] NOT NULL,
	[CaseCartName] [NVARCHAR](200) NULL,
	[DueMinutes] [BIGINT] NOT NULL,
	[CaseTime] [DateTime] NULL,
	[DueTimestamp] [DateTime] NOT NULL,
	[DestinationName] [NVARCHAR](200) NULL,
	[EndTimestamp] [DateTime] NULL,
	[CaseDoctor] [NVARCHAR](200) NULL,
	[ContainerName] [NVARCHAR](200) NOT NULL,
	[ItemName] [NVARCHAR](200) NOT NULL,
	[Status] [NVARCHAR](100) NULL,
	[ScheduleStatusCode] [NVARCHAR](50) NULL,
	[ScheduleStatus] [NVARCHAR](50) NULL,
	[StandardTime] [DateTime] NULL,
	[ExpediteTime] [DateTime] NULL,
	[ItemCode] [NVARCHAR](50) NULL,
	[ItemType] [NVARCHAR](50) NULL,
	[ItemTypeName] [NVARCHAR](100) NULL,
	[StatTime] [DateTime] NULL,
	[MultipleBasis] [BIT] NULL,
	[LocationName] [NVARCHAR](200) NULL,
	[UpdateTimestamp] [DateTime] NULL,
	[ScheduleCartChanged] [DateTime] NULL,
	[IsModified] [BIT] NOT NULL,
	[HoldQuantity] [NUMERIC](18,2) NOT NULL,
	[ItemQuantity] [BIGINT] NOT NULL,
	[IsCanceled] [BIT] NOT NULL,
	[IsCartComplete] [BIT] NOT NULL,
	[Shipped] [BIT] NOT NULL,
	[CaseStatus] [NVARCHAR](50) NULL,
	[StandardTimeInHoursAndMinutes] [NVARCHAR](10) NULL,
	[ExpediteTimeInHoursAndMinutes] [NVARCHAR](10) NULL,
	[StatTimeInHoursAndMinutes] [NVARCHAR](10) NULL,
	[DueInMinutes] [NUMERIC](18,6) NOT NULL,
	[DueInHoursAndMinutes] [NVARCHAR](10) NULL,
	[ProcessingFacilityId] [BIGINT] NULL,
	[LocationFacilityId] [BIGINT] NULL,
	[CaseItemNumber] [BIGINT] NOT NULL,
	[CaseContainerStatus] [BIGINT] NOT NULL,
	[CurrentLocationName] [NVARCHAR](200) NOT NULL,
	[CreatedAtUtc] DateTime NOT NULL,
	[ModifiedAtUtc] DateTime NULL
CONSTRAINT [PK_CTCaseContainer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTCaseContainer]  WITH CHECK ADD  CONSTRAINT [FK_CTCaseContainer_CTCase] FOREIGN KEY([CTCaseId])
REFERENCES [dbo].[CTCase] ([Id])
GO
ALTER TABLE [dbo].[CTCaseContainer] CHECK CONSTRAINT [FK_CTCaseContainer_CTCase]
GO