﻿using ProfitOptics.Modules.EmployeeTracker.Models;
using ProfitOptics.Modules.EmployeeTracker.Services;

namespace ProfitOptics.Modules.EmployeeTracker.Factories
{
    public class EmployeeTrackerFactory : IEmployeeTrackerFactory
    {
        #region Fields

        private readonly IEmployeeTrackerService _EmployeeTrackerService;

        #endregion Fields

        #region Ctor

        public EmployeeTrackerFactory(IEmployeeTrackerService EmployeeTrackerService)
        {
            _EmployeeTrackerService = EmployeeTrackerService;
        }

        #endregion Ctor

        #region Methods

        public EmptyModel PrepareEmptyModel()
        {
            return new EmptyModel
            {
                Message = "New module is ready for development"
            };
        }

        #endregion
    }
}