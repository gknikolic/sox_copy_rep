﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework
{
    public class GCColorStatusType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Code { get; set; }
		public string ColorHexCode { get; set; }
	}
}
