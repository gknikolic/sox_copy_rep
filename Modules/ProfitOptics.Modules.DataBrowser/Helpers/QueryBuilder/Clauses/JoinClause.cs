﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Enums;

namespace ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Clauses
{
    public struct JoinClause
    {
        public Comparison ComparisonOperator;
        public string FromColumn;
        public string FromTable;
        public JoinType JoinType;
        public string ToColumn;
        public string ToTable;

        public JoinClause(JoinType join, string toTableName, string toColumnName, Comparison @operator,
            string fromTableName, string fromColumnName)
        {
            JoinType = join;
            FromTable = fromTableName;
            FromColumn = fromColumnName;
            ComparisonOperator = @operator;
            ToTable = toTableName;
            ToColumn = toColumnName;
        }
    }
}