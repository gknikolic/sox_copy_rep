﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class ItemQueryResponse
    {
        public ItemsResponse QueryResponse { get; set; }
    }
}
