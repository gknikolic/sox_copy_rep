﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Announcements.Models;

namespace ProfitOptics.Modules.Announcements.Factories
{
    public interface IAnnouncementModelFactory
    {
        /// <summary>
        /// Gets the announcement model by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<AnnouncementModel> GetAnnouncementModelByIdAsync(int id);

        /// <summary>
        /// Gets all announcement models and processes the data table request
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<PagedList<AnnouncementModel>> PrepareAnnouncementListModelAsync(IDataTablesRequest request, int? userId = null, bool validOnly = false, bool activeOnly = false);

        /// <summary>
        ///  Gets all announcement models and processes the data table request for a specific user Id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<PagedList<AnnouncementModel>> PrepareAnnouncementModelsForUserIdAsync(IDataTablesRequest request, int userId);

        /// <summary>
        /// Gets all active announcements for a certain user Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<AnnouncementNotificationModel>> PrepareActiveAnnouncementModelsForUserIdAsync(int userId);

        AnnouncementsPageModel PrepareAnnouncementsPageModel(bool isAdminOrSuperAdmin);
    }
}