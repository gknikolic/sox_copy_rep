﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using Serilog;
using ProfitOptics.Modules.Sox.Services;
using Microsoft.AspNetCore.Identity;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public class QBService : IQBService
    {
        private readonly Entities _entities;
        private readonly IQuickBooksClient _quickBooksClient;
        private readonly IGroundControlService _groundControlService;

        public QBService(Entities entities, IQuickBooksClient quickBooksClient, IGroundControlService groundControlService)
        {
            _entities = entities;
            _quickBooksClient = quickBooksClient;
            _groundControlService = groundControlService;
        }

        public string PostGCTraysToQBEstimates(string userName)
        {
            string description = string.Empty;
            //int userId = 0;
            //var user = _entities.AspNetUsers.Where(x => x.UserName == userName).FirstOrDefault();
            //List<AspNetUserClaims> userClaims = null;
            //if (user != null)
            //{
            //    userId = user.Id;
            //    userClaims = _entities.AspNetUserClaims.Where(x => x.UserId == userId).ToList();
            //}
            //string qbToken = _groundControlService.GetQuickBooksAccessToken(userId, userClaims, userName);
            //var gcCases = _entities.GCCases
            //    .Include(x => x.CTCase)
            //    .Include(x => x.GCCustomer)
            //    .Include(x => x.GCTrays).ThenInclude(x => x.CTContainerType)
            //    .Include(x => x.GCTrays).ThenInclude(x => x.CTCaseContainer)
            //    .Where(x => x.QBEstimateId == null).ToList();

            //if(gcCases != null && gcCases.Count > 0)
            //{
            //    description += $"Fetched {gcCases.Count} GCCases | ";

            //    gcCases.ForEach(gcCase =>
            //    {
            //        if (gcCase.GCTrays != null && gcCase.GCTrays.Count > 0)
            //        {
            //            _groundControlService.CreateQBEstimate(gcCase, qbToken);
            //            _entities.SaveChanges();
            //        }
            //    });
            //}

            return description;
        }
    }
}
