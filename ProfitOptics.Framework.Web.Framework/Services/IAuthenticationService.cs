﻿using Microsoft.AspNetCore.Authentication;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.ServiceResults;

namespace ProfitOptics.Framework.Web.Framework.Services
{
    public interface IAuthenticationService
    {
        /// <summary>
        /// Attempts to login a user with provided credentials and returns sign in status for further processing
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="rememberMe"></param>
        /// <returns></returns>
        Task<SignInActionResult> PasswordSignInAsync(string username, string password, bool rememberMe);

        /// <summary>
        /// Signs out user
        /// </summary>
        /// <returns></returns>
        Task SignOutAsync();

        /// <summary>
        /// Registers a user and, if email confirmation is required, then sends a confirmation email
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="requestScheme"></param>
        /// <param name="phone"></param>
        /// <returns><see cref="UserRegistrationResult"/></returns>
        Task<UserRegistrationResult> RegisterUserAsync(string firstName, string lastName, string email, string password, string phone, string requestScheme);

        /// <summary>
        /// Attempts to confirm email and login user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="confirmationToken"></param>
        /// <returns></returns>
        Task<AuthenticationServiceResult> ConfirmEmailAsync(int userId, string confirmationToken);

        /// <summary>
        /// Resolves external login
        /// </summary>
        /// <returns></returns>
        Task<SignInActionResult> AuthenticateExternalLoginAsync(AuthenticateResult externalAuthenticationResult);

        /// <summary>
        /// Returns a list of available 2FA providers for the user that is waiting for 2FA
        /// </summary>
        /// <returns></returns>
        Task<TwoFactorProvidersResult> GetValidTwoFactorProvidersAsync();

        /// <summary>
        /// Sends a 2FA code using specified provider
        /// </summary>
        /// <param name="modelProvider"></param>
        /// <returns></returns>
        Task<AuthenticationServiceResult> SendTwoFactorAuthenticationCodeAsync(string providerName);

        /// <summary>
        /// Validates 2FA code and attempts to login user
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="code"></param>
        /// <param name="rememberMe"></param>
        /// <param name="rememberBrowser"></param>
        /// <returns></returns>
        Task<SignInActionResult> TwoFactorSignInAsync(string provider, string code, bool rememberMe, bool rememberBrowser);

        /// <summary>
        /// Generates password reset token and returns url for password reset
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns></returns>
        Task<PasswordResetUrlResult> GetPasswordResetUrlByEmailAsync(string email, string scheme);

        /// <summary>
        /// Validates password reset code and sets the new password
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="code">Password reset code</param>
        /// <param name="password">New password</param>
        /// <returns></returns>
        Task<AuthenticationServiceResult> PasswordResetAsync(string email, string code, string password);

        /// <summary>
        /// Finishes impersonation session and returns original user
        /// </summary>
        /// <returns></returns>
        Task<AuthenticationServiceResult> FinishImpersonatingSessionAsync();

        /// <summary>
        /// Starts new impersonating session
        /// </summary>
        /// <returns></returns>
        Task<AuthenticationServiceResult> StartImpersonatingSessionAsync(int userId);

        Task<EmailConfirmationUrlResult> GetEmailConfirmationUrlAsync(string email, string scheme);

        Task<bool> IsTwoFactorClientRememberedAsync(int userId);

        Task<IList<AuthenticationScheme>> GetExternalLoginProvidersAsync();

        ExternalLoginDetails GetExternalLoginDetails(AuthenticateResult response);

        Task<ApplicationUser> SaveExternalLoginAsync(ExternalLoginDetails externalLoginDetails,
            ApplicationUser user = null);

        Task<AuthenticationServiceResult> AuthenticateSSOAsync();

        Task<SingleLogoutServiceResult> SingleLogoutAsync();

        Task<SignInActionResult> ActiveDirectoryLoginAsync(string username, string password);

        Task RefreshAsync();
    }
}