USE [Sox]

ALTER TABLE [dbo].[CTLocation] DROP CONSTRAINT FK_CTLocation_CTFacility;

ALTER TABLE [dbo].[CTLocation] DROP COLUMN CTFacilityId;
ALTER TABLE [dbo].[CTLocation] ADD CTFacilityId INT NULL;
ALTER TABLE [dbo].[CTLocation] ADD GCCustomerId INT NULL;

ALTER TABLE [dbo].[CTLocation]  WITH CHECK ADD  CONSTRAINT [FK_CTLocation_GCCustomer] FOREIGN KEY([GCCustomerId])
REFERENCES [dbo].[GCCustomer] ([Id])
GO
ALTER TABLE [dbo].[CTLocation] CHECK CONSTRAINT [FK_CTLocation_GCCustomer]
GO