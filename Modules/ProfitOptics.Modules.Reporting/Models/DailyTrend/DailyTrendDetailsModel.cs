﻿using System;

namespace ProfitOptics.Modules.Reporting.Models.DailyTrend
{
    public class DailyTrendDetailsModel
    {
        public string BillToName { get; set; }
        
        public string BillToNum { get; set; }

        public string ShipToName { get; set; }

        public string ShipToNum { get; set; }

        public string CustomerClass { get; set; }
        
        public string VendorName { get; set; }
        
        public string ItemCategory { get; set; }

        public string ItemNum { get; set; }

        public DateTime InvoiceDate { get; set; }

        public string Date => InvoiceDate.ToString("MM-dd-yyyy");

        public decimal Quantity { get; set; }
        
        public decimal ExtPrice { get; set; }
    }
}
