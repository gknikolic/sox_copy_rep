﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Extensions;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Services.MarginBridge
{
    public class MarginBridgeAlgorithmService : IMarginBridgeAlgorithmService
    {
        public OutputData Process1(InputData period1, InputData period2, MarginBridgeType marginBridgeType)
        {
            var period1WeightedDataPoints = period1.DataPoints;
            var period2WeightedDataPoints = period2.DataPoints;

            // Make sure that items are in both period1 and period2
            var period1Full = new List<InputDataPointExt>();
            var period2Full = new List<InputDataPointExt>();
            var allItems = period1WeightedDataPoints.Keys.Union(period2WeightedDataPoints.Keys);
            foreach (var item in allItems)
            {
                if (period1WeightedDataPoints.ContainsKey(item))
                    period1Full.Add(period1WeightedDataPoints[item]);
                else
                {
                    period1Full.Add(new InputDataPointExt
                    {
                        Item = period2WeightedDataPoints[item].Item,
                        ItemCategory = period2WeightedDataPoints[item].ItemCategory,
                        ShipTo = period2WeightedDataPoints[item].ShipTo,
                        BillTo = period2WeightedDataPoints[item].BillTo,
                        Territory = period2WeightedDataPoints[item].Territory,
                        Region = period2WeightedDataPoints[item].Region,
                        Zone = period2WeightedDataPoints[item].Zone,
                        Cost = period2WeightedDataPoints[item].Cost,
                        CostExtra = period2WeightedDataPoints[item].CostExtra,
                        Price = period2WeightedDataPoints[item].Price,
                        RebateDollars = period2WeightedDataPoints[item].RebateDollars,
                        Qty = 0
                    });
                }

                if (period2WeightedDataPoints.ContainsKey(item))
                    period2Full.Add(period2WeightedDataPoints[item]);
                else
                {
                    period2Full.Add(new InputDataPointExt
                    {
                        Item = period1WeightedDataPoints[item].Item,
                        ItemCategory = period1WeightedDataPoints[item].ItemCategory,
                        ShipTo = period1WeightedDataPoints[item].ShipTo,
                        BillTo = period1WeightedDataPoints[item].BillTo,
                        Territory = period1WeightedDataPoints[item].Territory,
                        Region = period1WeightedDataPoints[item].Region,
                        Zone = period1WeightedDataPoints[item].Zone,
                        Cost = period1WeightedDataPoints[item].Cost,
                        CostExtra = period1WeightedDataPoints[item].CostExtra,
                        Price = period1WeightedDataPoints[item].Price,
                        RebateDollars = period1WeightedDataPoints[item].RebateDollars,
                        Qty = 0
                    });
                }
            }

            // Get contributions
            List<List<Double>> contributions;
            switch (marginBridgeType)
            {
                case MarginBridgeType.GPPercentage:
                    contributions = CalculateContributionsForGPPercentage(period1Full, period2Full);
                    break;
                case MarginBridgeType.GPDollarAmount:
                    contributions = CalculateContributionsForGPDollarAmount(period1Full, period2Full);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("Invalid Margin Bridge Type: {0}", marginBridgeType));
            }

            var toReturn = new OutputData();
            //Now we need to output everything.
            for (int i = 0; i < period1Full.Count; i++)
            {
                double pCont = contributions[i][0];
                double cCont = contributions[i][1] + contributions[i][2];//Put Cost and CostExtra back together.
                double vCont = 0;
                double pMixCont = 0;
                double rebateCont = contributions[i][3];
                double rebateVCont = 0;
                double rebatePMixCont = 0;
                //This determines whether it goes into Product Mix or Volume.
                if (period1Full[i].Qty == 0 || period2Full[i].Qty == 0)
                {
                    if (period1Full[i].RebateRate == 0 && period2Full[i].RebateRate == 0)//This controlls whether it goes into rebates or not.
                        pMixCont = contributions[i][4];
                    else
                        rebatePMixCont = contributions[i][4];
                }
                else
                {
                    if (period1Full[i].RebateRate == 0 && period2Full[i].RebateRate == 0)
                        vCont = contributions[i][4];
                    else
                        rebateVCont = contributions[i][4];
                }

                toReturn.DataPoints.Add(new OutputDataPoint
                {
                    LocationKey = period1Full[i].LocationKey,
                    ProductKey = period1Full[i].ProductKey,
                    Price = pCont,
                    Cost = cCont,
                    Volume = vCont,
                    ProductMix = pMixCont,
                    RebateRate = rebateCont,
                    RebateVolume = rebateVCont,
                    RebateProductMix = rebatePMixCont
                });

            }

            return toReturn;
        }

        #region Helper Functions

        private double Integral1(double a, double b, double c, double d, double e)
        {// Computes the integral (a*x + b)/(c*x^2 + d*x + e) dx from zero to one and returns the result.

            double temp1 = d;// Unfortunately, I initially did this integral as something else:
            double temp2 = e;// It was originally (d + e*x)/(a*x^2 + b*x + c) dx
            d = b;
            e = a;
            a = c;
            b = temp1;
            c = temp2;

            double result = 0;

            if (a != 0)
            {

                if (b * b < 4 * a * c)
                {
                    //Do not go gentle into that good night
                    result = ((4 * a * Math.Atan((b + 2 * a) / Math.Sqrt(4 * a * c - Math.Pow(b, 2))) - 4 * a * Math.Atan(b / Math.Sqrt(4 * a * c - Math.Pow(b, 2)))) * d + e * Math.Sqrt(4 * a * c - Math.Pow(b, 2)) * Math.Log(Math.Abs(c + b + a)) - e * Math.Sqrt(4 * a * c - Math.Pow(b, 2)) * Math.Log(Math.Abs(c)) - 2 * e * b * Math.Atan((b + 2 * a) / Math.Sqrt(4 * a * c - Math.Pow(b, 2))) + 2 * e * b * Math.Atan(b / Math.Sqrt(4 * a * c - Math.Pow(b, 2)))) / (2 * a * Math.Sqrt(4 * a * c - Math.Pow(b, 2)));
                    //Old age should burn and rave at close of day
                }
                else if (b * b == 4 * a * c)
                {
                    //Rage, rage against the dying of the light.
                    double div = a;
                    a = b / (2 * a);
                    double x = 1;
                    result = e * Math.Log(Math.Abs(x + a)) + (e * a - d) / (x + a);
                    x = 0;
                    result = result - (e * Math.Log(Math.Abs(x + a)) + (e * a - d) / (x + a));
                    result = result / div;
                    //Encoding the quantum data into Morse.
                }
                else
                {
                    //EUREKA!!!
                    double x = 1;
                    result = e * Math.Log(Math.Abs(x * (a * x + b) + c)) / (2 * a) + (2 * a * d - e * b) * Math.Log(Math.Abs((2 * a * x + b - Math.Sqrt(b * b - 4 * a * c)) / (2 * a * x + b + Math.Sqrt(b * b - 4 * a * c)))) / (2 * a * Math.Sqrt(b * b - 4 * a * c));
                    x = 0;
                    result = result - (e * Math.Log(Math.Abs(x * (a * x + b) + c)) / (2 * a) + (2 * a * d - e * b) * Math.Log(Math.Abs((2 * a * x + b - Math.Sqrt(b * b - 4 * a * c)) / (2 * a * x + b + Math.Sqrt(b * b - 4 * a * c)))) / (2 * a * Math.Sqrt(b * b - 4 * a * c)));
                    //It's traditional.
                }

            }
            else
            {

                if (b != 0)
                {

                    double x = 1;
                    result = ((d - e * c / b) * Math.Log(Math.Abs(b * x + c)) + e * x) / b;
                    x = 0;
                    result = result - (((d - e * c / b) * Math.Log(Math.Abs(b * x + c)) + e * x) / b);

                }
                else
                {

                    if (c != 0)
                    {

                        result = (2 * d + e) / (2 * c);

                    }
                    else
                    {

                        result = 0;

                    }

                }

            }

            return result;

        }

        private double Integral2(double a, double b, double c, double d, double e, double f, double g)
        {// Computes the integral (a*x^3 + b*x^2 + c*x + d)/(e*x^2 + f*x + g)^2 dx from zero to one and returns the result.

            double result = 0;

            if (e != 0)
            {

                if (f * f < 4 * e * g)
                {
                    //4 8 15 16 23 42
                    double x = 1;
                    result = Math.Pow(e, -2) * a * Math.Log(Math.Abs(e * Math.Pow(x, 2) + f * x + g)) / 2 - ((6 * e * a * f - 4 * Math.Pow(e, 2) * b) * g - a * Math.Pow(f, 3) + 2 * Math.Pow(e, 2) * c * f - 4 * Math.Pow(e, 3) * d) * Math.Atan((2 * e * x + f) / Math.Sqrt(4 * e * g - Math.Pow(f, 2))) / (Math.Sqrt(4 * e * g - Math.Pow(f, 2)) * (4 * Math.Pow(e, 3) * g - Math.Pow(e, 2) * Math.Pow(f, 2))) + (((3 * e * a * f - 2 * Math.Pow(e, 2) * b) * g - a * Math.Pow(f, 3) + e * b * Math.Pow(f, 2) - Math.Pow(e, 2) * c * f + 2 * Math.Pow(e, 3) * d) * x + 2 * e * a * Math.Pow(g, 2) + (-a * Math.Pow(f, 2) + e * b * f - 2 * Math.Pow(e, 2) * c) * g + Math.Pow(e, 2) * d * f) / ((4 * Math.Pow(e, 4) * g - Math.Pow(e, 3) * Math.Pow(f, 2)) * Math.Pow(x, 2) + (4 * Math.Pow(e, 3) * f * g - Math.Pow(e, 2) * Math.Pow(f, 3)) * x + 4 * Math.Pow(e, 3) * Math.Pow(g, 2) - Math.Pow(e, 2) * Math.Pow(f, 2) * g);
                    x = 0;
                    double tempVar2 = Math.Pow(e, -2) * a * Math.Log(Math.Abs(e * Math.Pow(x, 2) + f * x + g)) / 2 - ((6 * e * a * f - 4 * Math.Pow(e, 2) * b) * g - a * Math.Pow(f, 3) + 2 * Math.Pow(e, 2) * c * f - 4 * Math.Pow(e, 3) * d) * Math.Atan((2 * e * x + f) / Math.Sqrt(4 * e * g - Math.Pow(f, 2))) / (Math.Sqrt(4 * e * g - Math.Pow(f, 2)) * (4 * Math.Pow(e, 3) * g - Math.Pow(e, 2) * Math.Pow(f, 2))) + (((3 * e * a * f - 2 * Math.Pow(e, 2) * b) * g - a * Math.Pow(f, 3) + e * b * Math.Pow(f, 2) - Math.Pow(e, 2) * c * f + 2 * Math.Pow(e, 3) * d) * x + 2 * e * a * Math.Pow(g, 2) + (-a * Math.Pow(f, 2) + e * b * f - 2 * Math.Pow(e, 2) * c) * g + Math.Pow(e, 2) * d * f) / ((4 * Math.Pow(e, 4) * g - Math.Pow(e, 3) * Math.Pow(f, 2)) * Math.Pow(x, 2) + (4 * Math.Pow(e, 3) * f * g - Math.Pow(e, 2) * Math.Pow(f, 3)) * x + 4 * Math.Pow(e, 3) * Math.Pow(g, 2) - Math.Pow(e, 2) * Math.Pow(f, 2) * g);
                    result = result - tempVar2;
                    //NO, HURLEY! Don't use the numbers!
                }
                else if (f * f == 4 * e * g)
                {
                    //The numbers are cursed! You've doomed us all!
                    double div = Math.Pow(e, 2);
                    e = f / (2 * e);
                    double x = 1;
                    result = a * log(abs(x + e)) - ((6 * b - 18 * e * a) * x.Power(2) + (3 * c + 6 * e * b - 27 * e.Power(2) * a) * x + 2 * d + e * c + 2 * e.Power(2) * b - 11 * e.Power(3) * a) / (6 * x.Power(3) + 18 * e * x.Power(2) + 18 * e.Power(2) * x + 6 * e.Power(3));
                    x = 0;
                    result = result - (a * log(abs(x + e)) - ((6 * b - 18 * e * a) * x.Power(2) + (3 * c + 6 * e * b - 27 * e.Power(2) * a) * x + 2 * d + e * c + 2 * e.Power(2) * b - 11 * e.Power(3) * a) / (6 * x.Power(3) + 18 * e * x.Power(2) + 18 * e.Power(2) * x + 6 * e.Power(3)));
                    result = result / div;
                    //The numbers are the only thing that make this work!
                }
                else
                {
                    //I warned you.
                    double x = 1;
                    result = Math.Pow(e, -2) * a * Math.Log(Math.Abs(e * Math.Pow(x, 2) + f * x + g)) / 2 - ((6 * e * a * f - 4 * Math.Pow(e, 2) * b) * g - a * Math.Pow(f, 3) + 2 * Math.Pow(e, 2) * c * f - 4 * Math.Pow(e, 3) * d) * (Math.Log(Math.Abs((2 * e * x + f - Math.Sqrt(f * f - 4 * e * g)) / (2 * e * x + f + Math.Sqrt(f * f - 4 * e * g))))) / (2 * (Math.Sqrt(f * f - 4 * e * g)) * (4 * Math.Pow(e, 3) * g - Math.Pow(e, 2) * Math.Pow(f, 2))) + (((3 * e * a * f - 2 * Math.Pow(e, 2) * b) * g - a * Math.Pow(f, 3) + e * b * Math.Pow(f, 2) - Math.Pow(e, 2) * c * f + 2 * Math.Pow(e, 3) * d) * x + 2 * e * a * Math.Pow(g, 2) + (-a * Math.Pow(f, 2) + e * b * f - 2 * Math.Pow(e, 2) * c) * g + Math.Pow(e, 2) * d * f) / ((4 * Math.Pow(e, 4) * g - Math.Pow(e, 3) * Math.Pow(f, 2)) * Math.Pow(x, 2) + (4 * Math.Pow(e, 3) * f * g - Math.Pow(e, 2) * Math.Pow(f, 3)) * x + 4 * Math.Pow(e, 3) * Math.Pow(g, 2) - Math.Pow(e, 2) * Math.Pow(f, 2) * g);
                    x = 0;
                    result = result - (Math.Pow(e, -2) * a * Math.Log(Math.Abs(e * Math.Pow(x, 2) + f * x + g)) / 2 - ((6 * e * a * f - 4 * Math.Pow(e, 2) * b) * g - a * Math.Pow(f, 3) + 2 * Math.Pow(e, 2) * c * f - 4 * Math.Pow(e, 3) * d) * (Math.Log(Math.Abs((2 * e * x + f - Math.Sqrt(f * f - 4 * e * g)) / (2 * e * x + f + Math.Sqrt(f * f - 4 * e * g))))) / (2 * (Math.Sqrt(f * f - 4 * e * g)) * (4 * Math.Pow(e, 3) * g - Math.Pow(e, 2) * Math.Pow(f, 2))) + (((3 * e * a * f - 2 * Math.Pow(e, 2) * b) * g - a * Math.Pow(f, 3) + e * b * Math.Pow(f, 2) - Math.Pow(e, 2) * c * f + 2 * Math.Pow(e, 3) * d) * x + 2 * e * a * Math.Pow(g, 2) + (-a * Math.Pow(f, 2) + e * b * f - 2 * Math.Pow(e, 2) * c) * g + Math.Pow(e, 2) * d * f) / ((4 * Math.Pow(e, 4) * g - Math.Pow(e, 3) * Math.Pow(f, 2)) * Math.Pow(x, 2) + (4 * Math.Pow(e, 3) * f * g - Math.Pow(e, 2) * Math.Pow(f, 3)) * x + 4 * Math.Pow(e, 3) * Math.Pow(g, 2) - Math.Pow(e, 2) * Math.Pow(f, 2) * g));
                    //We have to go back to the island now.
                }

            }
            else
            {

                if (f != 0)
                {

                    double x = 1;
                    result = (3 * a * Math.Pow(g, 2) - 2 * b * f * g + c * Math.Pow(f, 2)) * Math.Log(Math.Abs(f * x + g)) / Math.Pow(f, 4) + (a * Math.Pow(g, 3) - b * f * Math.Pow(g, 2) + c * Math.Pow(f, 2) * g - d * Math.Pow(f, 3)) / (Math.Pow(f, 5) * x + Math.Pow(f, 4) * g) + (a * f * Math.Pow(x, 2) + (2 * b * f - 4 * a * g) * x) / (2 * Math.Pow(f, 3));
                    x = 0;
                    result = result - ((3 * a * Math.Pow(g, 2) - 2 * b * f * g + c * Math.Pow(f, 2)) * Math.Log(Math.Abs(f * x + g)) / Math.Pow(f, 4) + (a * Math.Pow(g, 3) - b * f * Math.Pow(g, 2) + c * Math.Pow(f, 2) * g - d * Math.Pow(f, 3)) / (Math.Pow(f, 5) * x + Math.Pow(f, 4) * g) + (a * f * Math.Pow(x, 2) + (2 * b * f - 4 * a * g) * x) / (2 * Math.Pow(f, 3)));

                }
                else
                {

                    if (g != 0)
                    {

                        result = (12 * d + 6 * c + 4 * b + 3 * a) / (12 * Math.Pow(g, 2));

                    }
                    else
                    {

                        result = 0;

                    }

                }

            }

            return result;

        }

        private double log(double a)
        {

            return Math.Log(a);

        }
        private double abs(double a)
        {

            return Math.Abs(a);

        }
        private double arctan(double a)
        {

            return Math.Atan(a);

        }
        private double sqrt(double a)
        {

            return Math.Sqrt(a);

        }

        private double Integral3(double a, double b, double c, double d, double e, double f)
        {//Computes the integral (ax^2 + bx + c)/(dx^2 + ex + f) from 0 to 1 and returns the result

            double result = 0;

            if (d != 0)
            {

                if (e * e < 4 * d * f)
                {

                    double x = 1;
                    result = (b * d - e * a) * log(abs(d * x.Power(2) + e * x + f)) / (2 * d.Power(2)) - (2 * a * d * f - 2 * c * d.Power(2) + e * b * d - e.Power(2) * a) * arctan((2 * d * x + e) / sqrt(4 * d * f - e.Power(2))) / (d.Power(2) * sqrt(4 * d * f - e.Power(2))) + a * x / d;
                    x = 0;
                    double tempVar2 = (b * d - e * a) * log(abs(d * x.Power(2) + e * x + f)) / (2 * d.Power(2)) - (2 * a * d * f - 2 * c * d.Power(2) + e * b * d - e.Power(2) * a) * arctan((2 * d * x + e) / sqrt(4 * d * f - e.Power(2))) / (d.Power(2) * sqrt(4 * d * f - e.Power(2))) + a * x / d;
                    result = result - tempVar2;

                }
                else if (e * e == 4 * d * f)
                {

                    double div = d;
                    d = e / (2 * d);
                    double x = 1;
                    result = (b - 2 * a * d) * log(abs(x + d)) + (-a * d.Power(2) + b * d - c) / (x + d) + a * x;
                    x = 0;
                    double tempVar = (b - 2 * a * d) * log(abs(x + d)) + (-a * d.Power(2) + b * d - c) / (x + d) + a * x;
                    result = (result - tempVar) / div;

                }
                else
                {

                    double x = 1;
                    result = (b * d - e * a) * log(abs(d * x.Power(2) + e * x + f)) / (2 * d.Power(2)) - (2 * a * d * f - 2 * c * d.Power(2) + e * b * d - e.Power(2) * a) * (log(abs((2 * d * x + e - sqrt(e.Power(2) - 4 * d * f)) / (2 * d * x + e + sqrt(e.Power(2) - 4 * d * f))))) / (d.Power(2) * (2 * sqrt(e.Power(2) - 4 * d * f))) + a * x / d;
                    x = 0;
                    result = result - ((b * d - e * a) * log(abs(d * x.Power(2) + e * x + f)) / (2 * d.Power(2)) - (2 * a * d * f - 2 * c * d.Power(2) + e * b * d - e.Power(2) * a) * (log(abs((2 * d * x + e - sqrt(e.Power(2) - 4 * d * f)) / (2 * d * x + e + sqrt(e.Power(2) - 4 * d * f))))) / (d.Power(2) * (2 * sqrt(e.Power(2) - 4 * d * f))) + a * x / d);

                }

            }
            else
            {

                if (e != 0)
                {

                    double x = 1;
                    result = e.Power(-3) * (a * f.Power(2) - e * b * f + e.Power(2) * c) * log(abs(e * x + f)) + e.Power(-2) * (e * a * x.Power(2) + (2 * e * b - 2 * a * f) * x) / 2;
                    x = 0;
                    result = result - (e.Power(-3) * (a * f.Power(2) - e * b * f + e.Power(2) * c) * log(abs(e * x + f)) + e.Power(-2) * (e * a * x.Power(2) + (2 * e * b - 2 * a * f) * x) / 2);

                }
                else
                {

                    if (f != 0)
                    {

                        result = (6 * c + 3 * b + 2 * a) / (6 * f);

                    }
                    else
                    {

                        result = 0;

                    }

                }

            }

            return result;

        }

        private double Integral4(double a, double b, double c, double d, double e, double f, double g, double h)//Should be working now.
        {//Computes the integral (ax^4 + bx^3 + cx^2 + dx + e)/((fx^2 + gx + h)^2) from 0 to 1 and returns the result

            double result = 0;

            if (f != 0)
            {

                if (g * g < 4 * f * h)
                {

                    double x = 1;
                    result = -(2 * a * g - b * f) * log(abs(f * x.Power(2) + g * x + h)) / (2 * f.Power(3)) - (12 * a * f.Power(2) * h.Power(2) + (-12 * a * f * g.Power(2) + 6 * b * f.Power(2) * g - 4 * c * f.Power(3)) * h + 2 * a * g.Power(4) - b * f * g.Power(3) + 2 * d * f.Power(3) * g - 4 * e * f.Power(4)) * arctan((2 * f * x + g) / sqrt(4 * f * h - g.Power(2))) / (sqrt(4 * f * h - g.Power(2)) * (4 * f.Power(4) * h - f.Power(3) * g.Power(2))) + ((2 * a * f.Power(2) * h.Power(2) + (-4 * a * f * g.Power(2) + 3 * b * f.Power(2) * g - 2 * c * f.Power(3)) * h + a * g.Power(4) - b * f * g.Power(3) + c * f.Power(2) * g.Power(2) - d * f.Power(3) * g + 2 * e * f.Power(4)) * x + (2 * b * f.Power(2) - 3 * a * f * g) * h.Power(2) + (a * g.Power(3) - b * f * g.Power(2) + c * f.Power(2) * g - 2 * d * f.Power(3)) * h + e * f.Power(3) * g) / ((4 * f.Power(5) * h - f.Power(4) * g.Power(2)) * x.Power(2) + (4 * f.Power(4) * g * h - f.Power(3) * g.Power(3)) * x + 4 * f.Power(4) * h.Power(2) - f.Power(3) * g.Power(2) * h) + a * x / f.Power(2);
                    x = 0;
                    double tempVar2 = -(2 * a * g - b * f) * log(abs(f * x.Power(2) + g * x + h)) / (2 * f.Power(3)) - (12 * a * f.Power(2) * h.Power(2) + (-12 * a * f * g.Power(2) + 6 * b * f.Power(2) * g - 4 * c * f.Power(3)) * h + 2 * a * g.Power(4) - b * f * g.Power(3) + 2 * d * f.Power(3) * g - 4 * e * f.Power(4)) * arctan((2 * f * x + g) / sqrt(4 * f * h - g.Power(2))) / (sqrt(4 * f * h - g.Power(2)) * (4 * f.Power(4) * h - f.Power(3) * g.Power(2))) + ((2 * a * f.Power(2) * h.Power(2) + (-4 * a * f * g.Power(2) + 3 * b * f.Power(2) * g - 2 * c * f.Power(3)) * h + a * g.Power(4) - b * f * g.Power(3) + c * f.Power(2) * g.Power(2) - d * f.Power(3) * g + 2 * e * f.Power(4)) * x + (2 * b * f.Power(2) - 3 * a * f * g) * h.Power(2) + (a * g.Power(3) - b * f * g.Power(2) + c * f.Power(2) * g - 2 * d * f.Power(3)) * h + e * f.Power(3) * g) / ((4 * f.Power(5) * h - f.Power(4) * g.Power(2)) * x.Power(2) + (4 * f.Power(4) * g * h - f.Power(3) * g.Power(3)) * x + 4 * f.Power(4) * h.Power(2) - f.Power(3) * g.Power(2) * h) + a * x / f.Power(2);
                    result = result - tempVar2;

                }
                else if (g * g == 4 * f * h)
                {

                    double div = Math.Pow(f, 2);
                    f = g / (2 * f);
                    double x = 1;
                    result = (b - 4 * a * f) * log(abs(x + f)) - ((36 * a * f.Power(2) - 18 * b * f + 6 * c) * x.Power(2) + (60 * a * f.Power(3) - 27 * b * f.Power(2) + 6 * c * f + 3 * d) * x + 26 * a * f.Power(4) - 11 * b * f.Power(3) + 2 * c * f.Power(2) + d * f + 2 * e) / (6 * x.Power(3) + 18 * f * x.Power(2) + 18 * f.Power(2) * x + 6 * f.Power(3)) + a * x;
                    x = 0;
                    double tempVar2 = (b - 4 * a * f) * log(abs(x + f)) - ((36 * a * f.Power(2) - 18 * b * f + 6 * c) * x.Power(2) + (60 * a * f.Power(3) - 27 * b * f.Power(2) + 6 * c * f + 3 * d) * x + 26 * a * f.Power(4) - 11 * b * f.Power(3) + 2 * c * f.Power(2) + d * f + 2 * e) / (6 * x.Power(3) + 18 * f * x.Power(2) + 18 * f.Power(2) * x + 6 * f.Power(3)) + a * x;
                    result = (result - tempVar2) / div;

                }
                else
                {

                    double x = 1;
                    result = -(2 * a * g - b * f) * log(abs(f * x.Power(2) + g * x + h)) / (2 * f.Power(3)) - (12 * a * f.Power(2) * h.Power(2) + (-12 * a * f * g.Power(2) + 6 * b * f.Power(2) * g - 4 * c * f.Power(3)) * h + 2 * a * g.Power(4) - b * f * g.Power(3) + 2 * d * f.Power(3) * g - 4 * e * f.Power(4)) * (log(abs((2 * f * x + g - sqrt(g.Power(2) - 4 * f * h)) / (2 * f * x + g + sqrt(g.Power(2) - 4 * f * h))))) / (2 * sqrt(g.Power(2) - 4 * f * h) * (4 * f.Power(4) * h - f.Power(3) * g.Power(2))) + ((2 * a * f.Power(2) * h.Power(2) + (-4 * a * f * g.Power(2) + 3 * b * f.Power(2) * g - 2 * c * f.Power(3)) * h + a * g.Power(4) - b * f * g.Power(3) + c * f.Power(2) * g.Power(2) - d * f.Power(3) * g + 2 * e * f.Power(4)) * x + (2 * b * f.Power(2) - 3 * a * f * g) * h.Power(2) + (a * g.Power(3) - b * f * g.Power(2) + c * f.Power(2) * g - 2 * d * f.Power(3)) * h + e * f.Power(3) * g) / ((4 * f.Power(5) * h - f.Power(4) * g.Power(2)) * x.Power(2) + (4 * f.Power(4) * g * h - f.Power(3) * g.Power(3)) * x + 4 * f.Power(4) * h.Power(2) - f.Power(3) * g.Power(2) * h) + a * x / f.Power(2);
                    x = 0;
                    double tempVar2 = -(2 * a * g - b * f) * log(abs(f * x.Power(2) + g * x + h)) / (2 * f.Power(3)) - (12 * a * f.Power(2) * h.Power(2) + (-12 * a * f * g.Power(2) + 6 * b * f.Power(2) * g - 4 * c * f.Power(3)) * h + 2 * a * g.Power(4) - b * f * g.Power(3) + 2 * d * f.Power(3) * g - 4 * e * f.Power(4)) * (log(abs((2 * f * x + g - sqrt(g.Power(2) - 4 * f * h)) / (2 * f * x + g + sqrt(g.Power(2) - 4 * f * h))))) / (2 * sqrt(g.Power(2) - 4 * f * h) * (4 * f.Power(4) * h - f.Power(3) * g.Power(2))) + ((2 * a * f.Power(2) * h.Power(2) + (-4 * a * f * g.Power(2) + 3 * b * f.Power(2) * g - 2 * c * f.Power(3)) * h + a * g.Power(4) - b * f * g.Power(3) + c * f.Power(2) * g.Power(2) - d * f.Power(3) * g + 2 * e * f.Power(4)) * x + (2 * b * f.Power(2) - 3 * a * f * g) * h.Power(2) + (a * g.Power(3) - b * f * g.Power(2) + c * f.Power(2) * g - 2 * d * f.Power(3)) * h + e * f.Power(3) * g) / ((4 * f.Power(5) * h - f.Power(4) * g.Power(2)) * x.Power(2) + (4 * f.Power(4) * g * h - f.Power(3) * g.Power(3)) * x + 4 * f.Power(4) * h.Power(2) - f.Power(3) * g.Power(2) * h) + a * x / f.Power(2);
                    result = result - tempVar2;

                }

            }
            else
            {

                if (g != 0)
                {

                    double x = 1;
                    result = -(4 * a * h.Power(3) - 3 * b * g * h.Power(2) + 2 * c * g.Power(2) * h - d * g.Power(3)) * log(abs(g * x + h)) / g.Power(5) + (-a * h.Power(4) + b * g * h.Power(3) - c * g.Power(2) * h.Power(2) + d * g.Power(3) * h - e * g.Power(4)) / (g.Power(6) * x + g.Power(5) * h) + (2 * a * g.Power(2) * x.Power(3) + (3 * b * g.Power(2) - 6 * a * g * h) * x.Power(2) + (18 * a * h.Power(2) - 12 * b * g * h + 6 * c * g.Power(2)) * x) / (6 * g.Power(4));
                    x = 0;
                    result = result - (-(4 * a * h.Power(3) - 3 * b * g * h.Power(2) + 2 * c * g.Power(2) * h - d * g.Power(3)) * log(abs(g * x + h)) / g.Power(5) + (-a * h.Power(4) + b * g * h.Power(3) - c * g.Power(2) * h.Power(2) + d * g.Power(3) * h - e * g.Power(4)) / (g.Power(6) * x + g.Power(5) * h) + (2 * a * g.Power(2) * x.Power(3) + (3 * b * g.Power(2) - 6 * a * g * h) * x.Power(2) + (18 * a * h.Power(2) - 12 * b * g * h + 6 * c * g.Power(2)) * x) / (6 * g.Power(4)));

                }
                else
                {

                    if (h != 0)
                    {

                        result = (30 * d + 20 * c + 15 * b + 12 * a + 60 * e) / (60 * h * h);

                    }
                    else
                    {

                        result = 0;

                    }

                }

            }

            return result;

        }

        private List<List<Double>> CalculateContributionsForGPPercentage(List<InputDataPointExt> period1Full, List<InputDataPointExt> period2Full)
        {
            //At this point, we essentially need a very large set of parametric equations.
            //For example, if we want the equation for price of item i (call it p_i), this is what it should look like:
            //p_i(t) = p_0 + t*(p_1 - p_0)
            //Where p_0 is the price of p_i in period 1, and p_1 is the price of p_i in period 2.
            //Then we have p_i(0) = p_0, p_i(1) = p_1.
            //Repeat this for every variable. If the equation is of the form x_0 + s*t, then we will store it as an
            //ArrayList of the form [x_0, s]. Note that if s = 0, then the contribution of this variable is zero. This will
            //allow us to skip the evil equations sometimes.

            List<List<List<Double>>> equations = new List<List<List<Double>>>();
            //It's probably best not to change things past this point. If you want more variable contributions, you will likely
            //have to use a different equation, and you'd have to make a couple changes based on your new equation.
            for (int i = 0; i < period2Full.Count; i++)
            {

                equations.Add(new List<List<Double>>());
                //Price
                equations[i].Add(new List<Double>());
                equations[i][0].Add((double)period1Full[i].Price);
                equations[i][0].Add((double)(period2Full[i].Price - period1Full[i].Price));
                //Cost (in some cases, referred to as b_i)
                equations[i].Add(new List<Double>());
                equations[i][1].Add((double)period1Full[i].Cost);
                equations[i][1].Add((double)(period2Full[i].Cost - period1Full[i].Cost));
                //CostExtra (in some cases, referred to as e_i)
                equations[i].Add(new List<Double>());
                equations[i][2].Add((double)period1Full[i].CostExtra);
                equations[i][2].Add((double)(period2Full[i].CostExtra - period1Full[i].CostExtra));
                //RebateRate
                equations[i].Add(new List<Double>());
                equations[i][3].Add((double)period1Full[i].RebateRate);
                equations[i][3].Add((double)(period2Full[i].RebateRate - period1Full[i].RebateRate));
                //Quantity
                equations[i].Add(new List<Double>());
                equations[i][4].Add((double)period1Full[i].Qty);
                equations[i][4].Add((double)(period2Full[i].Qty - period1Full[i].Qty));

            }
            //However, I did create a shorter PDF so that most of this process should make sense. I'm just computing the coefficients here.
            double qTimesP1 = 0;//This is SUM(q_i * p_i)
            double qTimesP2 = 0;
            double qTimesP3 = 0;

            for (int i = 0; i < equations.Count; i++)
            {

                qTimesP1 += equations[i][0][1] * equations[i][4][1];
                qTimesP2 += equations[i][0][1] * equations[i][4][0];
                qTimesP2 += equations[i][0][0] * equations[i][4][1];
                qTimesP3 += equations[i][0][0] * equations[i][4][0];

            }

            double biggestProd1 = 0;//This is SUM(q_i (p_i - (b_i (1 - r_i) + e_i)))
            double biggestProd2 = 0;
            double biggestProd3 = 0;
            double biggestProd4 = 0;

            for (int i = 0; i < equations.Count; i++)
            {

                biggestProd1 += equations[i][4][1] * equations[i][1][1] * equations[i][3][1];
                biggestProd2 += equations[i][4][0] * equations[i][1][1] * equations[i][3][1] + equations[i][4][1] * (equations[i][0][1] - equations[i][2][1] + equations[i][1][0] * equations[i][3][1] + equations[i][1][1] * (equations[i][3][0] - 1));
                biggestProd3 += equations[i][4][0] * (equations[i][0][1] - equations[i][2][1] + equations[i][1][0] * equations[i][3][1] + equations[i][1][1] * (equations[i][3][0] - 1)) + equations[i][4][1] * (equations[i][0][0] + equations[i][1][0] * (equations[i][3][0] - 1) - equations[i][2][0]);
                biggestProd4 += equations[i][4][0] * (equations[i][0][0] + equations[i][1][0] * (equations[i][3][0] - 1) - equations[i][2][0]);

            }

            double bigProd1 = 0;//This is SUM(q_i (b_i (1 - r_i) + e_i))
            double bigProd2 = 0;
            double bigProd3 = 0;
            double bigProd4 = 0;

            for (int i = 0; i < equations.Count; i++)
            {

                bigProd1 += equations[i][4][1] * -equations[i][1][1] * equations[i][3][1];
                bigProd2 += equations[i][4][0] * -equations[i][1][1] * equations[i][3][1] + equations[i][4][1] * (equations[i][2][1] - equations[i][1][0] * equations[i][3][1] + equations[i][1][1] * (1 - equations[i][3][0]));
                bigProd3 += equations[i][4][0] * (equations[i][2][1] - equations[i][1][0] * equations[i][3][1] + equations[i][1][1] * (1 - equations[i][3][0])) + equations[i][4][1] * (equations[i][1][0] * (1 - equations[i][3][0]) + equations[i][2][0]);
                bigProd4 += equations[i][4][0] * (equations[i][1][0] * (1 - equations[i][3][0]) + equations[i][2][0]);

            }

            //Now it's time to do the calculations. They are EVIL!
            //Which is why I put them in the methods at top. Now it doesn't look too messy down here!
            List<List<Double>> contributions = new List<List<Double>>();

            for (int i = 0; i < equations.Count; i++)
            {

                contributions.Add(new List<Double>());

                //Price
                //Price and quantity are very similar. In fact, once you get the constants, the same equations are used.

                if (equations[i][0][1] != 0)
                {

                    double a = 0;
                    double b = 0;
                    double c = 0;
                    double d = 0;
                    double e = 0;
                    double f = 0;
                    double g = 0;
                    double h = 0;

                    double slope = equations[i][0][1];
                    double x0 = equations[i][0][0];

                    double qjS = equations[i][4][1];
                    double qjIV = equations[i][4][0];

                    a = qjS * bigProd1;
                    b = qjIV * bigProd1 + qjS * bigProd2;
                    c = qjIV * bigProd2 + qjS * bigProd3;
                    d = qjIV * bigProd3 + qjS * bigProd4;
                    e = qjIV * bigProd4;
                    f = qTimesP1;
                    g = qTimesP2;
                    h = qTimesP3;

                    double result = Integral4(a, b, c, d, e, f, g, h);

                    contributions[i].Add(result * slope);//Don't forget, it's result times slope.

                }
                else
                {

                    contributions[i].Add(0.0);

                }

                //Cost
                //Cost and RebateRate are very similar.

                if (equations[i][1][1] != 0)
                {

                    //Numerator: a*x^2 + b*x + c
                    double a = equations[i][4][1] * equations[i][3][1];
                    double b = equations[i][4][1] * (equations[i][3][0] - 1) + equations[i][4][0] * equations[i][3][1];
                    double c = equations[i][4][0] * (equations[i][3][0] - 1);
                    double d = qTimesP1;
                    double e = qTimesP2;
                    double f = qTimesP3;
                    //Denominator: d*x^2 + e*x + f

                    double result = Integral3(a, b, c, d, e, f);

                    contributions[i].Add(equations[i][1][1] * result);

                }
                else
                {

                    contributions[i].Add(0.0);

                }

                //CostExtra
                //This is the simplest one.
                //I haven't tested the data when this is nonzero. Since we probably won't be using this, I'll leave it in, but nonzero values could mess things up.
                if (equations[i][2][1] != 0)
                {

                    //Numerator: a*x + b
                    double b = -1 * equations[i][4][0];
                    double a = -1 * equations[i][4][1];
                    double c = qTimesP1;
                    double d = qTimesP2;
                    double e = qTimesP3;
                    //Denominator: c*x^2 + d*x + e

                    double result = Integral1(a, b, c, d, e);

                    contributions[i].Add(equations[i][2][1] * result);

                }
                else
                {

                    contributions[i].Add(0.0);

                }

                //RebateRate

                if (equations[i][3][1] != 0)
                {

                    //Numerator: a*x^2 + b*x + c
                    double a = equations[i][4][1] * equations[i][1][1];
                    double b = equations[i][4][1] * equations[i][1][0] + equations[i][4][0] * equations[i][1][1];
                    double c = equations[i][4][0] * equations[i][1][0];
                    double d = qTimesP1;
                    double e = qTimesP2;
                    double f = qTimesP3;
                    //Denominator: d*x^2 + e*x + f

                    double result = Integral3(a, b, c, d, e, f);

                    contributions[i].Add(equations[i][3][1] * result);

                }
                else
                {

                    contributions[i].Add(0.0);

                }

                //Quantity
                //By far, this is the ugliest one for the coefficients.

                if (equations[i][4][1] != 0)
                {

                    double a = 0;
                    double b = 0;
                    double c = 0;
                    double d = 0;
                    double e = 0;
                    double f = 0;
                    double g = 0;
                    double h = 0;

                    double slope = equations[i][4][1];
                    double qjIV = equations[i][4][0];

                    double pjS = equations[i][0][1];
                    double pjIV = equations[i][0][0];

                    double bjS = equations[i][1][1];
                    double bjIV = equations[i][1][0];

                    double ejS = equations[i][2][1];
                    double ejIV = equations[i][2][0];

                    double rjS = equations[i][3][1];
                    double rjIV = equations[i][3][0];

                    double aQuantity = bjS * rjS;
                    double bQuantity = pjS + bjS * rjIV + bjIV * rjS - ejS - bjS;
                    double cQuantity = pjIV + bjIV * rjIV - ejIV - bjIV;
                    double dQuantity = qTimesP1 - slope * pjS;
                    double eQuantity = qTimesP2 - slope * pjIV - qjIV * pjS;
                    double fQuantity = qTimesP3 - qjIV * pjIV;
                    double gQuantity = aQuantity * dQuantity;
                    double hQuantity = aQuantity * eQuantity + bQuantity * dQuantity;
                    double iQuantity = aQuantity * fQuantity + bQuantity * eQuantity + cQuantity * dQuantity;
                    double jQuantity = bQuantity * fQuantity + cQuantity * eQuantity;
                    double kQuantity = cQuantity * fQuantity;

                    double lQuantity = biggestProd1 - (equations[i][4][1] * equations[i][1][1] * equations[i][3][1]);
                    double mQuantity = biggestProd2 - (equations[i][4][0] * equations[i][1][1] * equations[i][3][1] + equations[i][4][1] * (equations[i][0][1] - equations[i][2][1] + equations[i][1][0] * equations[i][3][1] + equations[i][1][1] * (equations[i][3][0] - 1)));
                    double nQuantity = biggestProd3 - (equations[i][4][0] * (equations[i][0][1] - equations[i][2][1] + equations[i][1][0] * equations[i][3][1] + equations[i][1][1] * (equations[i][3][0] - 1)) + equations[i][4][1] * (equations[i][0][0] + equations[i][1][0] * (equations[i][3][0] - 1) - equations[i][2][0]));
                    double oQuantity = biggestProd4 - (equations[i][4][0] * (equations[i][0][0] + equations[i][1][0] * (equations[i][3][0] - 1) - equations[i][2][0]));
                    aQuantity = lQuantity * pjS;
                    bQuantity = mQuantity * pjS + lQuantity * pjIV;
                    cQuantity = nQuantity * pjS + mQuantity * pjIV;
                    dQuantity = oQuantity * pjS + nQuantity * pjIV;
                    eQuantity = oQuantity * pjIV;
                    a = gQuantity - aQuantity;
                    b = hQuantity - bQuantity;
                    c = iQuantity - cQuantity;
                    d = jQuantity - dQuantity;
                    e = kQuantity - eQuantity;
                    f = qTimesP1;
                    g = qTimesP2;
                    h = qTimesP3;

                    double result = Integral4(a, b, c, d, e, f, g, h);

                    contributions[i].Add(result * slope);

                }
                else
                {

                    contributions[i].Add(0.0);

                }
            }

            return contributions;
        }

        private List<List<double>> CalculateContributionsForGPDollarAmount(List<InputDataPointExt> period1Full, List<InputDataPointExt> period2Full)
        {
            var contributions = new List<List<Double>>();
            for (int i = 0; i < period2Full.Count; i++)
            {
                contributions.Add(new List<Double>());

                //Price contribution is given by
                //(1/2 * (q_2 + q_1) ) * (p_2 - p_1)
                var pCont = (1m / 2m * (period2Full[i].Qty + period1Full[i].Qty)) * (period2Full[i].Price - period1Full[i].Price);
                contributions[i].Add(Convert.ToDouble(pCont));

                //Cost contribution is given by  // b signifies buy cost, represented as cost property.
                //(1/3 * (q_2 * r_2 + q_1 * r_1) + 1/6 * (q_1 * r_2 + q_2 * r_1) - 1/2 * (q_2 + q_1) ) * (b_2 - b_1)
                var cCont = (1m / 3m * (period2Full[i].Qty * period2Full[i].RebateRate + period1Full[i].Qty * period1Full[i].RebateRate)
                             + 1m / 6m * (period1Full[i].Qty * period2Full[i].RebateRate + period2Full[i].Qty * period1Full[i].RebateRate)
                             - 1m / 2m * (period2Full[i].Qty + period1Full[i].Qty))
                            * (period2Full[i].Cost - period1Full[i].Cost);
                contributions[i].Add(Convert.ToDouble(cCont));

                //CostExtra
                contributions[i].Add(0.0); // no cost extra defined in request

                //Rebate rate contribution is given by
                //(1/3 * (b_2 * q_2 + b_1 * q_1) + 1/6 * (b_1 * q_2 + b_2 * q_1) ) * (r_2 - r_1)
                var rCont = (1m / 3m * (period2Full[i].Cost * period2Full[i].Qty + period1Full[i].Cost * period1Full[i].Qty)
                             + 1m / 6m * (period1Full[i].Cost * period2Full[i].Qty + period2Full[i].Cost * period1Full[i].Qty))
                            * (period2Full[i].RebateRate - period1Full[i].RebateRate);
                contributions[i].Add(Convert.ToDouble(rCont));

                //Quantity contribution is given by
                //(1/3 * (b_2*r_2 + b_1*r_1) + 1/6 * (b_1 * r_2 + b_2 * r_1) + 1/2 * (p_2 + p_1 - b_2 - b_1) ) * (q_2 - q_1)
                var qCont = (1m / 3m * (period2Full[i].Cost * period2Full[i].RebateRate + period1Full[i].Cost * period1Full[i].RebateRate)
                             + 1m / 6m * (period1Full[i].Cost * period2Full[i].RebateRate + period2Full[i].Cost * period1Full[i].RebateRate)
                             + 1m / 2m * (period2Full[i].Price + period1Full[i].Price - period2Full[i].Cost - period1Full[i].Cost))
                            * (period2Full[i].Qty - period1Full[i].Qty);
                contributions[i].Add(Convert.ToDouble(qCont));
            }

            return contributions;
        }

        #endregion
    }
}
