﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class MetaData
    {
        public DateTime CreateTime { get; set; }

        public DateTime? LastUpdatedTime { get; set; }
    }
}
