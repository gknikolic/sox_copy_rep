﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Web.Helpers.CustomAttributes;
using System.Diagnostics;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;

namespace ProfitOptics.Framework.Web.Areas
{
    [ControllerActivation]
    public partial class BaseController : Controller
    {
        private Entities _context;
        private DiagnosticSource _listener;

        private Entities Context
        {
            get
            {
                if (_context != null)
                {
                    return _context;
                }
                _context = HttpContext.RequestServices.GetService<Entities>();

                if (_listener == null)
                {
                    var _listener = _context.GetService<DiagnosticSource>();
                    (_listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
                }

                return _context;
            }
        }
        private delegate void ActionExecuting(ActionExecutingContext context);

        private event ActionExecuting OnActionExecutingEvent; 
        
        public BaseController()
        {
            RegisterEventHandlers();
            
        }

        partial void RegisterEventHandlers();

        public int GetCurrentUserId()
        {
            return int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
        }

        public string GetCurrentUserFullName()
        {
            var firstName = User.FindFirstValue(ClaimTypes.GivenName);

            var lastName = User.FindFirstValue(ClaimTypes.Surname);
            
            return $"{firstName} {lastName}";
        }

        public string GetCurrentUserUsername()
        {
            return User.FindFirstValue(ClaimTypes.Name);
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            OnActionExecutingEvent?.Invoke(context);

            base.OnActionExecuting(context);
        }
    }
}