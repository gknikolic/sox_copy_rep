﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Factories;
using ProfitOptics.Modules.Reporting.Factories.BusinessReview;
using ProfitOptics.Modules.Reporting.Factories.CustomerReport;
using ProfitOptics.Modules.Reporting.Factories.DailyTrend;
using ProfitOptics.Modules.Reporting.Factories.RsmPerformance;
using ProfitOptics.Modules.Reporting.Factories.TrendVariance;
using ProfitOptics.Modules.Reporting.Helpers;
using ProfitOptics.Modules.Reporting.Services;
using ProfitOptics.Modules.Reporting.Services.BusinessReview;
using ProfitOptics.Modules.Reporting.Services.DailyTrend;
using ProfitOptics.Modules.Reporting.Services.RsmPerformance;
using ProfitOptics.Modules.Reporting.Services.TrendVariance;

namespace ProfitOptics.Modules.Reporting.Infrastructure
{
    public sealed class ReportingServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            // Services
            services.AddScoped<IUserMappingService, UserMappingService>();
            services.AddScoped<IUserHierarchyService, UserHierarchyService>();
            services.AddScoped<IDaysOffService, DaysOffService>();
            services.AddScoped<IUserFilterService, UserFilterService>();
            services.AddScoped<ISalesHistoryService, SalesHistoryService>();
            services.AddScoped<IDailyTrendExportService, DailyTrendExportService>();
            services.AddScoped<IDailyTrendDataService, DailyTrendDataService>();
            services.AddScoped<IBusinessReviewDataService, BusinessReviewDataService>();
            services.AddScoped<ITrendVarianceDataService, TrendVarianceDataService>();
            services.AddScoped<IRsmPerformanceService, RsmPerformanceService>();
            services.AddScoped<IRsmSalesAndQuotaService, RsmSalesAndQuotaService>();
            services.AddScoped<IRsmMissingRevenueService, RsmMissingRevenueService>();
            services.AddScoped<IUserAssignment, ReportingUserAssignment>();
            services.AddScoped<ITrendVarianceExportService, TrendVarianceExportService>();

            // Factories
            services.AddScoped<IReportFilterModelFactory, ReportFilterModelFactory>();
            services.AddScoped<IDailyTrendModelFactory, DailyTrendModelFactory>();
            services.AddScoped<IBusinessReviewModelFactory, BusinessReviewModelFactory>();
            services.AddScoped<ITrendVarianceModelFactory, TrendVarianceModelFactory>();
            services.AddScoped<ICustomerReportFactory, CustomerReportFactory>();
            services.AddScoped<IRsmPerformanceModelFactory, RsmPerformanceModelFactory>();

            //Settings
            StartupConfiguration.ConfigureStartupConfig<ReportingSettings>(services, configuration);
        }
    }
}