﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Extensions;
using ProfitOptics.Modules.Reporting.Factories.MarginBridge;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;
using ProfitOptics.Modules.Reporting.Services.MarginBridge;
using MarginBridgeDataModel = ProfitOptics.Modules.Reporting.Models.MarginBridge.MarginBridgeDataModel;

namespace ProfitOptics.Modules.Reporting.Controllers
{
    [Authorize(Roles = "admin, reporting")]
    public class MarginBridgeController : BaseReportingController
    {
        #region MyRegion

        private readonly IMarginBridgeService _marginBridgeService;
        private readonly IMarginBridgeFactory _marginBridgeFactory;
        private readonly IEPPlusExtensionService _epPlusExtensionService;
        private readonly IMarginBridgeSessionService _sessionService;
        private const string AllLocations = "All Locations";
        private const string AllProducts = "All Products";
        private static readonly Stopwatch Stopwatch = new Stopwatch();

        #endregion

        public MarginBridgeController(IMarginBridgeService marginBridgeService,
            IMarginBridgeFactory marginBridgeFactory,
            IEPPlusExtensionService epPlusExtensionService,
            IMarginBridgeSessionService sessionService)
        {
            _marginBridgeService = marginBridgeService;
            _marginBridgeFactory = marginBridgeFactory;
            _epPlusExtensionService = epPlusExtensionService;
            _sessionService = sessionService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var indexModel = _marginBridgeFactory.PrepareMarginBridgeIndexModel(_sessionService.Current);

            _sessionService.Current.LocationsHierarchyInitial = null;
            _sessionService.Current.ProductsHierarchyInitial = null;
            _sessionService.Current.CheckedNodesInitial = null;

            if (_sessionService.Current.StatisticsDataModel == null)
            {
                _sessionService.Current.ClearSortData();
            }

            _sessionService.Save(_sessionService.Current);

            return View(indexModel);
        }

        [HttpPost]
        public IActionResult ProcessStatistics(MarginBridgeDataModel model)
        {
            try
            {
                Stopwatch.Reset();
                Stopwatch.Start();
                
                var statisticsModel = _marginBridgeFactory.GetMarginBridgeStatistics(model, GetCurrentUserId());

                statisticsModel.LocationLevel = LocationLevelType.AllLocations;
                statisticsModel.LocationValue = AllLocations;
                statisticsModel.ProductLevel = ProductLevelType.AllProducts;
                statisticsModel.ProductValue = AllProducts;
                statisticsModel.GroupedBy = GroupByType.Location;
                statisticsModel.PeriodId = model.PeriodId;
                statisticsModel.FirstPeriod = model.FirstPeriod;
                statisticsModel.SecondPeriod = model.SecondPeriod;
                statisticsModel.ProductsHierarchyInitial = _sessionService.Current.ProductsHierarchyInitial;
                statisticsModel.LocationsHierarchyInitial = _sessionService.Current.LocationsHierarchyInitial;
                statisticsModel.CheckedNodesInitial = _sessionService.Current.CheckedNodesInitial;

                _sessionService.Current.StatisticsDataModel = statisticsModel;

                _marginBridgeService.CalculateResults(_sessionService.Current.StatisticsDataModel);

                _sessionService.Save(_sessionService.Current);

                Stopwatch.Stop();
                Logger.WriteLog(Logger.LogType.Info, "Margin Bridge filter data",
                    Convert.ToString(Stopwatch.ElapsedMilliseconds), model.MarginBridgeType, model.FirstPeriod, model.SecondPeriod,
                    model.ProductSelectionLevel, model.LocationSelectionLevel);
                Stopwatch.Reset();

                return Json(new
                {
                    Success = true,
                    Data = _sessionService.Current.StatisticsDataModel.TotalsPermanent,
                    MarginBridgeType = model.MarginBridgeType
                });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, ErrorMessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult GetDataBridgeModel()
        {
            var dataModel = new MarginBridgeDataModel
            {
                FirstPeriod = _sessionService.Current.StatisticsDataModel.FirstPeriod,
                SecondPeriod = _sessionService.Current.StatisticsDataModel.SecondPeriod,
                LocationSelectionLevel = _sessionService.Current.StatisticsDataModel.LocationLevel,
                ProductSelectionLevel = _sessionService.Current.StatisticsDataModel.ProductLevel,
                MarginBridgeType = _sessionService.Current.StatisticsDataModel.MarginBridgeType
            };

            var result = _marginBridgeFactory.PrepareDataBridgeData(dataModel, GetCurrentUserId());

            return new LargeJsonResult(new
            {
                Items = result
            });
        }

        public IActionResult ApplyFilter()
        {
            try
            {
                return ProcessStatistics( new MarginBridgeDataModel
                {
                    FirstPeriod = _sessionService.Current.StatisticsDataModel.FirstPeriod,
                    SecondPeriod = _sessionService.Current.StatisticsDataModel.SecondPeriod,
                    MarginBridgeType = _sessionService.Current.MarginBridgeType,
                    LocationSelectionLevel = _sessionService.Current.StatisticsDataModel.LocationLevel,
                    ProductSelectionLevel = _sessionService.Current.StatisticsDataModel.ProductLevel
                });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, ErrorMessage = ex.Message });
            }
        }

        public IActionResult UnApplyFilter()
        {
            try
            {
                return ProcessStatistics(new MarginBridgeDataModel
                {
                    FirstPeriod = _sessionService.Current.StatisticsDataModel.FirstPeriod,
                    SecondPeriod = _sessionService.Current.StatisticsDataModel.SecondPeriod,
                    MarginBridgeType = _sessionService.Current.MarginBridgeType,
                    LocationSelectionLevel = _sessionService.Current.StatisticsDataModel.LocationLevel,
                    ProductSelectionLevel = _sessionService.Current.StatisticsDataModel.ProductLevel
                });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, ErrorMessage = ex.Message });
            }
        }

        public IActionResult CalculateResults(GroupByType groupByType, int level, string value)
        {
            try
            {
                if (_sessionService.Current.StatisticsDataModel == null)
                {
                    return Json(new { Success = false, RedirectToIndex = true });
                }

                _sessionService.Current.StatisticsDataModel.GroupedBy = groupByType;
                _sessionService.Current.StatisticsDataModel.GroupedByMax = false;
                switch (groupByType)
                {
                    case GroupByType.Location:
                        _sessionService.Current.StatisticsDataModel.LocationLevel = (LocationLevelType)level;
                        _sessionService.Current.StatisticsDataModel.LocationValue = string.IsNullOrEmpty(value)
                            ? _sessionService.Current.StatisticsDataModel.LocationValue
                            : value;
                        _marginBridgeService.CalculateResults(_sessionService.Current.StatisticsDataModel);
                        _sessionService.Save(_sessionService.Current);
                        break;

                    case GroupByType.Product:
                        _sessionService.Current.StatisticsDataModel.ProductLevel = (ProductLevelType)level;
                        _sessionService.Current.StatisticsDataModel.ProductValue = string.IsNullOrEmpty(value)
                            ? _sessionService.Current.StatisticsDataModel.ProductValue
                            : value;
                        _marginBridgeService.CalculateResults(_sessionService.Current.StatisticsDataModel);
                        _sessionService.Save(_sessionService.Current);
                        break;

                    default:
                        throw new Exception($"Invalid GroupBy type selected: {groupByType}");
                }

                return Json(new { Success = true, RedirectToIndex = false });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, ErrorMessage = ex.Message, RedirectToIndex = false });
            }
        }

        public IActionResult GroupByMaxLevel(GroupByType groupByType)
        {
            try
            {
                if (_sessionService.Current.StatisticsDataModel == null)
                {
                    return Json(new { Success = false, RedirectToIndex = true });
                }

                _sessionService.Current.StatisticsDataModel.GroupedBy = groupByType;
                _sessionService.Current.StatisticsDataModel.GroupedByMax = true;
                _marginBridgeService.CalculateResults(_sessionService.Current.StatisticsDataModel, true);
                _sessionService.Save(_sessionService.Current);

                return Json(new { Success = true, RedirectToIndex = false });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, ErrorMessage = ex.Message, RedirectToIndex = false });
            }
        }

        public IActionResult CheckDownloadStatus()
        {
            try
            {
                return Json(new { Success = true, Downloaded = _sessionService.DownloadFinished });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, ErrorMessage = ex.Message });
            }
        }

        public IActionResult DownloadGpDollar()
        {
            if (_sessionService.Current?.StatisticsDataModel?.AllResults != null)
            {
                _sessionService.DownloadFinished = false;
                var tableData = new List<StatisticsResponseDollar>();
                tableData.AddRange(_sessionService.Current.StatisticsDataModel.AllResults.Select(result => new StatisticsResponseDollar
                {
                    Cost = result.Cost,
                    EndMargin = result.EndMargin,
                    StartMargin = result.StartMargin,
                    Price = result.Price,
                    ProductMix = result.ProductMix,
                    RebateProdMix = result.RebateProdMix,
                    RebateRate = result.RebateRate,
                    Volume = result.Volume,
                    RebateVol = result.RebateVol,
                    Period1Sales = result.Period1Sales,
                    Period2Sales = result.Period2Sales,
                    Description = result.Description,
                    Caption = result.Caption,
                    TotalImpact = result.TotalImpact
                }));

                var totals = new StatisticsResponseDollar
                {
                    Cost = _sessionService.Current.StatisticsDataModel.Totals.Cost,
                    EndMargin = _sessionService.Current.StatisticsDataModel.Totals.EndMargin,
                    StartMargin = _sessionService.Current.StatisticsDataModel.Totals.StartMargin,
                    Price = _sessionService.Current.StatisticsDataModel.Totals.Price,
                    ProductMix = _sessionService.Current.StatisticsDataModel.Totals.ProductMix,
                    RebateProdMix = _sessionService.Current.StatisticsDataModel.Totals.RebateProdMix,
                    RebateRate = _sessionService.Current.StatisticsDataModel.Totals.RebateRate,
                    Volume = _sessionService.Current.StatisticsDataModel.Totals.Volume,
                    RebateVol = _sessionService.Current.StatisticsDataModel.Totals.RebateVol,
                    Period1Sales = _sessionService.Current.StatisticsDataModel.Totals.Period1Sales,
                    Period2Sales = _sessionService.Current.StatisticsDataModel.Totals.Period2Sales,
                    Description = _sessionService.Current.StatisticsDataModel.Totals.Description,
                    TotalImpact = _sessionService.Current.StatisticsDataModel.Totals.TotalImpact,
                    Caption = _sessionService.Current.StatisticsDataModel.Totals.Caption
                };
                tableData.Add(totals);

                var document = _epPlusExtensionService.WriteToExcel("MarginBridge", tableData, true);
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = _marginBridgeFactory.CreateFileName("MarginBridge_"),
                    Inline = false,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());


                _sessionService.DownloadFinished = true;

                return File(document, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            return null;
        }

        public IActionResult DownloadGpPercentage()
        {
            if (_sessionService.Current?.StatisticsDataModel?.AllResults != null)
            {
                _sessionService.DownloadFinished = false;

                var tableData = new List<StatisticsResponse>();
                tableData.AddRange(_sessionService.Current.StatisticsDataModel.AllResults.Select(result => new StatisticsResponse
                {
                    Cost = result.Cost,
                    EndMargin = result.EndMargin,
                    StartMargin = result.StartMargin,
                    Price = result.Price,
                    ProductMix = result.ProductMix,
                    RebateProdMix = result.RebateProdMix,
                    RebateRate = result.RebateRate,
                    Volume = result.Volume,
                    RebateVol = result.RebateVol,
                    Period1Sales = result.Period1Sales,
                    Period2Sales = result.Period2Sales,
                    Description = result.Description,
                    Caption = result.Caption,
                    TotalImpact = result.TotalImpact
                }));

                var totals = new StatisticsResponse
                {
                    Cost = _sessionService.Current.StatisticsDataModel.Totals.Cost,
                    EndMargin = _sessionService.Current.StatisticsDataModel.Totals.EndMargin,
                    StartMargin = _sessionService.Current.StatisticsDataModel.Totals.StartMargin,
                    Price = _sessionService.Current.StatisticsDataModel.Totals.Price,
                    ProductMix = _sessionService.Current.StatisticsDataModel.Totals.ProductMix,
                    RebateProdMix = _sessionService.Current.StatisticsDataModel.Totals.RebateProdMix,
                    RebateRate = _sessionService.Current.StatisticsDataModel.Totals.RebateRate,
                    Volume = _sessionService.Current.StatisticsDataModel.Totals.Volume,
                    RebateVol = _sessionService.Current.StatisticsDataModel.Totals.RebateVol,
                    Period1Sales = _sessionService.Current.StatisticsDataModel.Totals.Period1Sales,
                    Period2Sales = _sessionService.Current.StatisticsDataModel.Totals.Period2Sales,
                    Description = _sessionService.Current.StatisticsDataModel.Totals.Description,
                    TotalImpact = _sessionService.Current.StatisticsDataModel.Totals.TotalImpact,
                    Caption = _sessionService.Current.StatisticsDataModel.Totals.Caption
                };
                tableData.Add(totals);

                var document = _epPlusExtensionService.WriteToExcel("MarginBridge", tableData, true);
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = _marginBridgeFactory.CreateFileName("MarginBridge_"),
                    Inline = false,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());

                _sessionService.DownloadFinished = true;

                return File(document, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            return null;
        }

        public IActionResult Download()
        {
            if (_sessionService.Current.MarginBridgeType == MarginBridgeType.GPDollarAmount)
            {
                return DownloadGpDollar();
            }

            return DownloadGpPercentage();
        }

        public IActionResult DownloadRawGpPercentage()
        {
            if (_sessionService.Current?.StatisticsDataModel?.AlgorithmOutput != null)
            {
                // Write algorithm output

                _sessionService.DownloadFinished = false;

                var dataPoints = _sessionService.Current.StatisticsDataModel.AlgorithmOutput.Select(p => new AlgorithmOutputDataPoint
                {
                    LocationKey = p.LocationKey,
                    ProductKey = p.ProductKey,
                    Price = p.Price,
                    Cost = p.Cost,
                    Volume = p.Volume,
                    ProductMix = p.ProductMix,
                    RebateRate = p.RebateRate,
                    RebateVolume = p.RebateVolume,
                    RebateProductMix = p.RebateProductMix,
                }).ToList();

                var document = _epPlusExtensionService.WriteToExcel<AlgorithmOutputDataPoint>("AlgorithmOutput", dataPoints, true);

                // Write algorithm input and raw sales history
                var maxProductLevel = _sessionService.Current.StatisticsDataModel.MaxProductLevel;
                var maxLocationLevel = _sessionService.Current.StatisticsDataModel.MaxLocationLevel;
                var firstPeriodRawData = _marginBridgeFactory.PrepareRawSalesHistory(GetCurrentUserId(),_sessionService.Current.StatisticsDataModel.FirstPeriod);
                var secondPeriodRawData = _marginBridgeFactory.PrepareRawSalesHistory(GetCurrentUserId(),_sessionService.Current.StatisticsDataModel.SecondPeriod);

                var dataModel = new MarginBridgeDataModel
                {
                    FirstPeriod = _sessionService.Current.StatisticsDataModel.FirstPeriod,
                    SecondPeriod = _sessionService.Current.StatisticsDataModel.SecondPeriod,
                    LocationSelectionLevel = _sessionService.Current.StatisticsDataModel.LocationLevel,
                    ProductSelectionLevel = _sessionService.Current.StatisticsDataModel.ProductLevel,
                    MarginBridgeType = _sessionService.Current.StatisticsDataModel.MarginBridgeType
                };
                var filter = _marginBridgeFactory.PrepareMarginBridgeFilterModel(dataModel, GetCurrentUserId());
                var inputData1 = _marginBridgeService.CreateProcessInputData(filter, _sessionService.Current.StatisticsDataModel.FirstPeriod, PeriodType.First, maxProductLevel, maxLocationLevel);
                var inputData2 = _marginBridgeService.CreateProcessInputData(filter, _sessionService.Current.StatisticsDataModel.SecondPeriod, PeriodType.Second, maxProductLevel, maxLocationLevel);

                document = _epPlusExtensionService.AppendToExcel<ReportSalesHistoryModel>(document, "SalesHistoryFirstPeriod", firstPeriodRawData, true);
                document = _epPlusExtensionService.AppendToExcel<ReportSalesHistoryModel>(document, "SalesHistorySecondPeriod", secondPeriodRawData, true);

                document = _epPlusExtensionService.AppendToExcel<InputDataPointExt>(document, "AlgorithmInputFirstPeriod", inputData1.DataPoints.Values.ToList(), true);
                document = _epPlusExtensionService.AppendToExcel<InputDataPointExt>(document, "AlgorithmInputSecondPeriod", inputData2.DataPoints.Values.ToList(), true);

                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = _marginBridgeFactory.CreateFileName("MarginBridgeAlgorithmOutput_"),
                    Inline = false,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());

                _sessionService.DownloadFinished = true;

                return File(document, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            return null;
        }

        public IActionResult DownloadRawGpDollar()
        {
            if (_sessionService.Current?.StatisticsDataModel?.AlgorithmOutput != null)
            {
                // Write algorithm output
                _sessionService.DownloadFinished = false;

                var dataPoints = _sessionService.Current.StatisticsDataModel.AlgorithmOutput.Select(p => new AlgorithmOutputDataPointDolar
                {
                    LocationKey = p.LocationKey,
                    ProductKey = p.ProductKey,
                    Price = p.Price,
                    Cost = p.Cost,
                    Volume = p.Volume,
                    ProductMix = p.ProductMix,
                    RebateRate = p.RebateRate,
                    RebateVolume = p.RebateVolume,
                    RebateProductMix = p.RebateProductMix,
                }).ToList();

                var document = _epPlusExtensionService.WriteToExcel<AlgorithmOutputDataPointDolar>("AlgorithmOutput", dataPoints, true);

                // Write algorithm input and raw sales history
                var maxProductLevel = _sessionService.Current.StatisticsDataModel.MaxProductLevel;
                var maxLocationLevel = _sessionService.Current.StatisticsDataModel.MaxLocationLevel;
                var firstPeriodRawData = _marginBridgeFactory.PrepareRawSalesHistory(GetCurrentUserId(), _sessionService.Current.StatisticsDataModel.FirstPeriod);
                var secondPeriodRawData = _marginBridgeFactory.PrepareRawSalesHistory(GetCurrentUserId(), _sessionService.Current.StatisticsDataModel.SecondPeriod);

                var dataModel = new MarginBridgeDataModel
                {
                    FirstPeriod = _sessionService.Current.StatisticsDataModel.FirstPeriod,
                    SecondPeriod = _sessionService.Current.StatisticsDataModel.SecondPeriod,
                    LocationSelectionLevel = _sessionService.Current.StatisticsDataModel.LocationLevel,
                    ProductSelectionLevel = _sessionService.Current.StatisticsDataModel.ProductLevel,
                    MarginBridgeType = _sessionService.Current.StatisticsDataModel.MarginBridgeType
                };
                var filter = _marginBridgeFactory.PrepareMarginBridgeFilterModel(dataModel, GetCurrentUserId());
                var inputData1 = _marginBridgeService.CreateProcessInputData(filter, _sessionService.Current.StatisticsDataModel.FirstPeriod, PeriodType.First, maxProductLevel, maxLocationLevel);
                var inputData2 = _marginBridgeService.CreateProcessInputData(filter, _sessionService.Current.StatisticsDataModel.SecondPeriod, PeriodType.Second, maxProductLevel, maxLocationLevel);

                document = _epPlusExtensionService.AppendToExcel<ReportSalesHistoryModel>(document, "SalesHistoryFirstPeriod", firstPeriodRawData, true);
                document = _epPlusExtensionService.AppendToExcel<ReportSalesHistoryModel>(document, "SalesHistorySecondPeriod", secondPeriodRawData, true);

                document = _epPlusExtensionService.AppendToExcel<InputDataPointExt>(document, "AlgorithmInputFirstPeriod", inputData1.DataPoints.Values.ToList(), true);
                document = _epPlusExtensionService.AppendToExcel<InputDataPointExt>(document, "AlgorithmInputSecondPeriod", inputData2.DataPoints.Values.ToList(), true);

                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = _marginBridgeFactory.CreateFileName("MarginBridgeAlgorithmOutput_"),
                    Inline = false,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());

                _sessionService.DownloadFinished = true;

                return File(document, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            return null;
        }

        public IActionResult DownloadRaw()
        {
            if (_sessionService.Current.MarginBridgeType == MarginBridgeType.GPDollarAmount)
            {
                return DownloadRawGpDollar();
            }

            return DownloadRawGpPercentage();
        }

        public IActionResult DownloadDataBridge()
        {
            if (_sessionService.Current?.StatisticsDataModel != null)
            {
                _sessionService.DownloadFinished = false;

                var marginBridgeDataModel = new MarginBridgeDataModel
                {
                    FirstPeriod = _sessionService.Current.StatisticsDataModel.FirstPeriod,
                    SecondPeriod = _sessionService.Current.StatisticsDataModel.SecondPeriod,
                    MarginBridgeType = _sessionService.Current.MarginBridgeType,
                    LocationSelectionLevel = _sessionService.Current.StatisticsDataModel.LocationLevel,
                    ProductSelectionLevel = _sessionService.Current.StatisticsDataModel.ProductLevel
                };

                var rawData = _marginBridgeFactory.PrepareDataBridgeRawData(marginBridgeDataModel, GetCurrentUserId());

                var document =
                    _epPlusExtensionService.WriteToExcel<DataBridgeItem>("DataBridge", rawData.DataBridge, true);

                document = _epPlusExtensionService.AppendToExcel<SalesHistory>(document, "Pricing Credits P1", rawData.PricingCreaditsP1, true);
                document = _epPlusExtensionService.AppendToExcel<SalesHistory>(document, "Pricing Credits P2", rawData.PricingCreaditsP2, true);
                document = _epPlusExtensionService.AppendToExcel<SalesHistory>(document, "Ship & Debit P1", rawData.ShipDebitP1, true);
                document = _epPlusExtensionService.AppendToExcel<SalesHistory>(document, "Ship & Debit P2", rawData.ShipDebitP2, true);
                document = _epPlusExtensionService.AppendToExcel<SalesHistory>(document, "Politique Records P1", rawData.PolitiqueP1, true);
                document = _epPlusExtensionService.AppendToExcel<SalesHistory>(document, "Politique Records P2", rawData.PolitiqueP2, true);
                document = _epPlusExtensionService.AppendToExcel<SalesHistory>(document, "Invoices - No Sales P1", rawData.InvoicesNoSalesP1, true);
                document = _epPlusExtensionService.AppendToExcel<SalesHistory>(document, "Invoices - No Sales P2", rawData.InvoicesNoSalesP2, true);
                document = _epPlusExtensionService.AppendToExcel<SalesHistory>(document, "Invoices - No Cost P1", rawData.InvoicesNoCostP1, true);
                document = _epPlusExtensionService.AppendToExcel<SalesHistory>(document, "Invoices - No Cost P2", rawData.InvoicesNoCostP2, true);

                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = _marginBridgeFactory.CreateFileName("DataBridge_"),
                    Inline = false,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());

                _sessionService.DownloadFinished = true;

                return File(document, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }

            return null;
        }

        [HttpPost]
        public IActionResult GetStatisticsDataTable([FromBody]JQueryDataTablesParamModel aoData)
        {
            var result = _marginBridgeService.GetStatisticsData(aoData);

            return new LargeJsonResult(new
            {
                aaData = result.Result,
                aaSummaryRow = result.SummaryRow,
                iTotalRecords = result.TotalCount,
                iTotalDisplayRecords = result.TotalCount
            });
        }
    }
}
