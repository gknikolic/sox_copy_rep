﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Email;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Areas.Account.Factories;
using ProfitOptics.Framework.Web.Areas.Account.Models;
using ProfitOptics.Framework.Web.Areas.Account.Services;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.ServiceResults;
using ProfitOptics.Framework.Web.Framework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using IAuthenticationService = ProfitOptics.Framework.Web.Framework.Services.IAuthenticationService;
using System.Net;

namespace ProfitOptics.Framework.Web.Areas.Account.Controllers
{

    [Area("Account")]
    public class AccountController : BaseController
    {
        #region Fields

        private readonly IAccountModelFactory _accountModelFactory;
        private readonly IRecaptchaService _recaptchaService;
        private readonly IEmailer _emailer;
        private readonly IUserService _userService;
        private readonly Common CommonSettings;
        private readonly IStringResourceRepository _stringResourceRepository;
        private readonly SignInJsonResult _signInJsonResult;
        private readonly IAuthenticationService _authenticationService;

        #endregion

        #region Ctor

        public AccountController(IAccountModelFactory accountModelFactory,
                                 IRecaptchaService recaptchaService,
                                 IEmailer emailer,
                                 IUserService userService,
                                 Common common,
                                 IStringResourceRepository stringResourceRepository,
                                 SignInJsonResult signInJsonResult,
                                 IAuthenticationService authenticationService)
        {
            this._recaptchaService = recaptchaService;
            this._accountModelFactory = accountModelFactory;
            this._emailer = emailer;
            this._userService = userService;
            this.CommonSettings = common;
            this._stringResourceRepository = stringResourceRepository;
            this._signInJsonResult = signInJsonResult;
            this._authenticationService = authenticationService;
        }

        #endregion

        #region Login/Logout/Registration

        [Route("/login")]
        public IActionResult Login(string returnUrl)
        {
            var model = _accountModelFactory.PrepareLoginModel(returnUrl);
            return View(model);
        }

        [HttpPost]
        [Route("/login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var signInActionResult = await _authenticationService.PasswordSignInAsync(model.Username, model.Password, model.RememberMe);
            if (signInActionResult.Status == AuthenticationServiceResultStatusCode.Failed)
            {
                ModelState.AddModelError("", signInActionResult.Error);
                return View(model);
            }
            return ProcessSignInResult(signInActionResult, model);
        }

        [Route("/logout")]
        public async Task<IActionResult> LogOff()
        {
            await _authenticationService.SignOutAsync();
            return RedirectToAction("Index", "Home", new { area = "" });
        }

        public IActionResult Register()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            var captchaResponse = Request.Form["g-recaptcha-response"];
            var recaptchaPassed = _recaptchaService.Validate(captchaResponse);
            if (!ModelState.IsValid || !recaptchaPassed)
            {
                if (!recaptchaPassed)
                {
                    ModelState.AddModelError("", _stringResourceRepository.GetResource(ResourceKeys.MessagesAccountRegistrationCaptchaError));
                }                
                return View(model);
            }
            var createUserResult = await _authenticationService.RegisterUserAsync(model.FirstName, model.LastName, model.Email, model.Password, model.PhoneNumber, Request.Scheme);
            if (createUserResult.Succeeded)
            {
                if (CommonSettings.NotifyAdminsOnUserRegister && User.IsInRole(ApplicationRoleNames.UserManager))
                {
                    var callbackUrl = Url.RouteUrl(POFrameworkDefaults.UserManagementRouteName);
                    var adminNotificationBody = await this.RenderViewAsync<string>("_EmailNotifyAdminsAboutRegistration", callbackUrl, partial: true);

                    var adminEmails = (await _userService.GetUsersInRoleAsync(ApplicationRoleNames.Admin))?.Select(x => x.Email).ToList() ?? new List<string>();
                    _emailer.SendEmail(adminEmails, _stringResourceRepository.GetResource(ResourceKeys.EmailSubjectsNewUserNotification), adminNotificationBody, isHtml: true);

                    var superAdminEmails = (await _userService.GetUsersInRoleAsync(ApplicationRoleNames.SuperAdmin))?.Select(x => x.Email).ToList() ?? new List<string>();
                    _emailer.SendEmail(superAdminEmails, _stringResourceRepository.GetResource(ResourceKeys.EmailSubjectsNewUserNotification), adminNotificationBody, isHtml: true);
                }
            }
            else
            {
                ModelState.AddModelError("", createUserResult.Error);
                return View(model);
            }
            if (createUserResult.Status == AuthenticationServiceResultStatusCode.EmailNotConfirmed)
            {
                var emailBody = await this.RenderViewAsync<string>("_EmailConfirmationRegistration", createUserResult.ConfirmEmailUrl, partial: true);
                _emailer.SendEmailAndWriteToDb(model.Email, _stringResourceRepository.GetResource(ResourceKeys.EmailSubjectsConfirmEmail), emailBody, isHtml: true);
                return View("DisplayEmail");
            }

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> ConfirmEmail(int userId, string confirmationToken)
        {
            var result = await _authenticationService.ConfirmEmailAsync(userId, WebUtility.UrlDecode(confirmationToken));
            if (result.Succeeded)
            {
                return View("ConfirmEmail");
            }

            return Redirect("/Error/500");
        }

        [Route("/access-denied")]
        public IActionResult AccessDenied()
        {
            return Redirect("/Error/403");
        }

        #endregion

        #region External login

        public IActionResult ExternalLogin(string providerName, string callbackUrl, string returnUrl)
        {
            var prop = new AuthenticationProperties
            {
                RedirectUri = callbackUrl,
                Items = { { "scheme", providerName }, { "returnUrl", returnUrl } }
            };
            return Challenge(prop, providerName);
        }

        public async Task<IActionResult> ExternalLoginCallback()
        {
            var externalAuthenticationResult = await HttpContext.AuthenticateAsync(IdentityConstants.ExternalScheme);
            var signInResult = await _authenticationService.AuthenticateExternalLoginAsync(externalAuthenticationResult);

            if (signInResult.IsLockedOut())
            {
                return View("Lockout");
            }
            else if (signInResult.RequiresTwoFactor())
            {
                return RedirectToAction("SendCode");
            }

            var returnUrl = externalAuthenticationResult.Properties.Items["returnUrl"];
            return RedirectToLocal(returnUrl);
        }

        #endregion

        #region 2FA

        public async Task<IActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var twoFactorProvidersResult = await _authenticationService.GetValidTwoFactorProvidersAsync();
            if (!twoFactorProvidersResult.Succeeded)
            {
                return RedirectToAction("Login");
            }
            var model = _accountModelFactory.PrepareSendCodeModel(returnUrl, rememberMe, twoFactorProvidersResult.Providers);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendCode(SendCodeModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var sendingResult = await _authenticationService.SendTwoFactorAuthenticationCodeAsync(model.Provider);

            if (!sendingResult.Succeeded)
            {
                ModelState.AddModelError("", sendingResult.Error);
                return View(model);
            }

            return RedirectToAction("VerifyCode", new { Provider = model.Provider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        public async Task<IActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            var model = _accountModelFactory.PrepareVerifyCodeModel(provider, returnUrl, rememberMe);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> VerifyCode(VerifyCodeModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            
            var result = await _authenticationService.TwoFactorSignInAsync(model.Provider, model.Code, model.RememberMe, model.RememberBrowser);
            return ProcessSignInResult(result, new LoginModel(model));
        }

        #endregion

        #region Forgot password

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var passwordResetUrlResult = await _authenticationService.GetPasswordResetUrlByEmailAsync(model.Email, Request.Scheme);
            string emailBody = null;
            if (passwordResetUrlResult.Status == AuthenticationServiceResultStatusCode.EmailNotConfirmed)
            {
                var emailConfirmationUrlResult = await _authenticationService.GetEmailConfirmationUrlAsync(model.Email, Request.Scheme);
                if (emailConfirmationUrlResult.Succeeded)
                {
                    emailBody = await this.RenderViewAsync<string>("_EmailConfirmationRegistration", emailConfirmationUrlResult.Url, partial: true);
                    _emailer.SendEmailAndWriteToDb(model.Email, _stringResourceRepository.GetResource(ResourceKeys.EmailSubjectsConfirmEmail), emailBody, isHtml: true);
                    return View("DisplayEmail");
                }
                ModelState.AddModelError("", emailConfirmationUrlResult.Error);
            }
            else if (passwordResetUrlResult.Succeeded)
            {
                emailBody = await this.RenderViewAsync<string>("_EmailResetPassword", passwordResetUrlResult.Url, partial: true);
                await _emailer.SendEmailAndWriteToDbAsync(model.Email, _stringResourceRepository.GetResource(ResourceKeys.EmailSubjectsResetPassword), emailBody, isHtml: true);
                return View("ForgotPasswordConfirmation");
            } else if (passwordResetUrlResult.Status == AuthenticationServiceResultStatusCode.UserNotFound)
            {
                return View("ForgotPasswordConfirmation");
            }
            ModelState.AddModelError("", passwordResetUrlResult.Error);
            return View(model);
        }

        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string code, string email)
        {
            if (code == null)
            {
                return Redirect("/Error/500");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _authenticationService.PasswordResetAsync(model.Email, WebUtility.UrlDecode(model.Code), model.Password);

            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            
            ModelState.AddModelError("", result.Error);
            return View(model);
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        #endregion

        #region Util

        private IActionResult ProcessSignInResult(SignInActionResult signInActionResult, LoginModel model)
        {
            bool isAjaxRequest = HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";

            if (signInActionResult.IsLockedOut())
            {
                return isAjaxRequest ? Json(_signInJsonResult.IsLockedOut) as IActionResult : View("Lockout");
            }
            if (signInActionResult.IsNotAllowed())
            {
                //Email not confirmed
                ModelState.AddModelError("", _stringResourceRepository.GetResource(ResourceKeys.MessagesSignInResultNotApproved));
                return isAjaxRequest ? Json(_signInJsonResult.IsNotAllowed) as IActionResult : View("Login", model);
            }

            if (signInActionResult.RequiresTwoFactor())
            {
                var redirectUrl = Url.Action("SendCode", "Account", new { returnUrl = model.ReturnUrl, rememberMe = model.RememberMe });
                return isAjaxRequest ? Json(_signInJsonResult.Redirect(redirectUrl)) as IActionResult : Redirect(redirectUrl);
            }

            if (signInActionResult.Succeeded())
            {
                return isAjaxRequest ? Json(_signInJsonResult.Succeeded) as IActionResult : Redirect(model.ReturnUrl ?? "/");
            }

            //Failed LogIn
            ModelState.AddModelError("", _stringResourceRepository.GetResource(ResourceKeys.MessagesSignInResultFailed));
            return isAjaxRequest ? Json(_signInJsonResult.Failed) as IActionResult : View("Login", model);
        }

        public async Task<IActionResult> Timeout(string returnUrl)
        {
            await _authenticationService.SignOutAsync();
            var jsonResponse = new
            {
                Successful = true
            };
            return Json(jsonResponse);
        }
        
        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home", new { area = "" });
        }

        #endregion

        #region Active Directory login

        public IActionResult ADLogin(string returnUrl)
        {
            var model = _accountModelFactory.PrepareLoginModel(returnUrl);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ADLogin(LoginModel model)
        {
            var result = await _authenticationService.ActiveDirectoryLoginAsync(model.Username, model.Password);
            return ProcessSignInResult(result, model);
        }

        #endregion

        #region SSO

        public async Task<IActionResult> AssertionConsumerService()
        {
            if (CommonSettings.EnableSAML)
            {
                AuthenticationServiceResult result = await _authenticationService.AuthenticateSSOAsync();
                if (!result.Succeeded)
                {
                    throw new Exception(result.Error);
                }
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            return Redirect("/Error/404");
        }

        public async Task<IActionResult> SingleLogoutService()
        {
            if (CommonSettings.EnableSAML)
            {
                SingleLogoutServiceResult singleLoguoutResult = await _authenticationService.SingleLogoutAsync();

                if (!singleLoguoutResult.Succeeded)
                {
                    if (string.IsNullOrWhiteSpace(singleLoguoutResult.RedirectUrl))
                    {
                        return Redirect("/Error/500");
                    }

                    return LocalRedirect(singleLoguoutResult.RedirectUrl);
                }
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            return Redirect("/Error/404");
        }

        #endregion
    }
}
