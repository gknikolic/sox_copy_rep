USE [Sox]

ALTER TABLE [Sox].[dbo].[CTContainerType] ADD [CTContainerServiceId] INT NULL;

GO

ALTER TABLE [dbo].[CTContainerType]  WITH CHECK ADD  CONSTRAINT [FK_CTContainerType_CTContainerService] FOREIGN KEY([CTContainerServiceId])
REFERENCES [dbo].[CTContainerService] ([Id])
GO
ALTER TABLE [dbo].[CTContainerType] CHECK CONSTRAINT [FK_CTContainerType_CTContainerService]
GO