﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Enums;

namespace ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Clauses
{
    public struct TopClause
    {
        public int Quantity;
        public TopUnit Unit;

        public TopClause(int nr)
        {
            Quantity = nr;
            Unit = TopUnit.Records;
        }

        public TopClause(int nr, TopUnit aUnit)
        {
            Quantity = nr;
            Unit = aUnit;
        }
    }
}