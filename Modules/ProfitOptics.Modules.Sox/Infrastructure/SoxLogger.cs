﻿using ProfitOptics.Modules.Sox.Infrastructure;
using Serilog;
using Serilog.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Infrastructure
{
    public class SoxLogger : ISoxLogger
    {
        public Serilog.Core.Logger WWLog { get; set; }
        public Serilog.Core.Logger SoxLog { get; set; }

        //private static SoxLogger instance = null;

        public SoxLogger() 
        {
            WWLog = new LoggerConfiguration()
                                  .Enrich.WithExceptionDetails()
                                  .MinimumLevel.Debug()
                                  .WriteTo.File(@"AppData\WWResponseLogs\log-.log", rollingInterval: RollingInterval.Day)
                                  .CreateLogger();

            SoxLog = new LoggerConfiguration()
                                  .Enrich.WithExceptionDetails()
                                  .MinimumLevel.Debug()
                                  .WriteTo.File(@"AppData\Logs\log-.log", rollingInterval: RollingInterval.Day)
                                  .CreateLogger();
        }

        //public static SoxLogger GetInstance()
        //{
        //    if (instance == null)
        //    {
        //        instance = new SoxLogger();
        //    }
        //    return instance;
        //}

        public void WWInformation(string log)
        {
            WWLog.Information(log);
        }

        public void SoxInformation(string log)
        {
           SoxLog.Information(log);
        }
    }
}
