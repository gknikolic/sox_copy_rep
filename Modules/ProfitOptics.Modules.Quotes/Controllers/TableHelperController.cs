﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;

namespace ProfitOptics.Modules.Quotes.Controllers
{
    [Area("Quotes")]
    [Authorize(Roles = "quoting,admin,superadmin")]
    public class TableHelperController : BaseController
    {
        // GET: Quotes/TableHelper
        public IActionResult SetTab()
        {
            return Content("");
        }
    }
}