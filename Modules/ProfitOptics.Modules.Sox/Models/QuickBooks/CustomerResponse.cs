﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class CustomerResponse : ResponseBase
    {
        public Customer Customer { get; set; }
    }
}
