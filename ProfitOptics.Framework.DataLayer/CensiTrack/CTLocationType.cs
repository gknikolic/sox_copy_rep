﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTLocationType
    {
        public int Id { get; set; }
        public long LegacyId { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual ICollection<CTLocation> CTLocations { get; set; }
        public virtual ICollection<CTLocationTypeGCLifecycleStatusType> CTLocationTypeGCLifecycleStatusTypes { get; set; }
    }
}
