﻿using ProfitOptics.Framework.Web.RestApi.Models.Auth;
using ProfitOptics.Framework.Web.RestApi.Services.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.Services.Auth
{
    public class JwtAuthenticationService : IJwtAuthenticationService
    {
        private readonly IUserService _userService;
        private readonly IPasswordHasher _passwordHasher;
        private readonly ITokenHandler _tokenHandler;

        public JwtAuthenticationService(IUserService userService, IPasswordHasher passwordHasher, ITokenHandler tokenHandler)
        {
            _tokenHandler = tokenHandler;
            _passwordHasher = passwordHasher;
            _userService = userService;
        }

        public object AuthenticationServiceResultStatusCode { get; private set; }

        public async Task<TokenResponse> CreateAccessTokenAsync(string username, string password)
        {
            var user = await _userService.FindByUsernameAsync(username);

            if (user == null || !_passwordHasher.PasswordMatches(password, user.PasswordHash))
            {
                return new TokenResponse("Invalid credentials.");
            }

            var token = await _tokenHandler.CreateAccessToken(username);

            return new TokenResponse(token);
        }

        public async Task<TokenResponse> RefreshTokenAsync(string refreshToken, string username)
        {
            var token = _tokenHandler.TakeRefreshToken(refreshToken);
            if (token == null)
            {
                return new TokenResponse("Invalid refresh token.");
            }

            if (token.IsExpired())
            {
                return new TokenResponse("Expired refresh token.");
            }

            var user = await _userService.FindByUsernameAsync(username);
            if (user == null)
            {
                return new TokenResponse("Invalid refresh token.");
            }

            var accessToken = await _tokenHandler.CreateAccessToken(username);
            return new TokenResponse(accessToken);
        }

        public void RevokeRefreshToken(string refreshToken)
        {
            _tokenHandler.RevokeRefreshToken(refreshToken);
        }
    }
}
