using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.RestApi.Models
{
    public class QueryResult<T>
    {
        public List<T> Items { get; set; } = new List<T>();
        public int TotalItems { get; set; } = 0;
    }
}