﻿(function (app, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    // Public properties
    /*
    * Ids of menus that should be activated when some page is opened. Set this array at the beginning of the view. Check out Dashboard/Index for example
    * In case submenu needs to be activated, this array should contain id of the parent menu, too.
    */
    app.activeMenus = []; // Set these active

    app.settings = {
        defaultAlertMessageTimeout: 5000
    };

    // Public Methods
    app.documentReady = function () {
        app.activeMenus.forEach(function (menu) {
            $("#" + menu).addClass("active");
        });

        // warning when mobile browser is used
        if (jQuery.browser.mobile) {
            //$(".browser-alert").show();
        }
    };

    // Log some error to elmah
    app.logError = function (ex, stack) {
        if (ex == null) return;
        var url = ex.fileName != null ? ex.fileName : document.location;
        if (stack == null && ex.stack != null) stack = ex.stack;

        // format output
        var out = ex.message != null ? ex.name + ": " + ex.message : ex;
        out += ": at document path '" + url + "'.";
        if (stack != null) out += "\n  at " + stack.join("\n  at ");

        // send error message
        $.post({
            url: "/Home/LogJavaScriptError",
            dataType: 'json',
            data: { message: out },
            success: function (data, status, jqXHR) {
                app.showNotification("An error occured. Team is notified about error details. Someone might reach to you in order to gather more info.")
            }
        });
    };

    // Post through Ajax
    app.postJson = function (url, jsonData, config) {
        var defaultConfig = {
            type: "POST",
            dataType: "json",
            //contentType: "application/json",
            cache: false,
            timeout: 300000
        };

        var appliedConfig = $.extend(defaultConfig, config);
        appliedConfig = $.extend(appliedConfig, {
            url: url,
            data: jsonData
        });

        return $.ajax(appliedConfig);
    };

    // Pop "dissapearing" notification message    
    app.showNotification = function (message) {
        return bootbox.alert(message);
    };

    // Modal dialog for alert (bootbox curently) 
    app.showAlert = function (config) {
        return bootbox.alert(config);
    };

    /**
     * Displays alert message, which can disappear after a timeout
     * @param {string} title - Title of the alert
     * @param {string} message - Alert message
     * @param {string} alertType - Alert type can be one of success, warning, info or danger
     * @param {number} alertFadeoutTimeout - Timeout after which the alert disappears. 
     * It can be configured globally for the application using app.settings.defaultAlertMessageTimeout. 
     * undefined or 0 value mean alert doesn't disappear.
     */
    app.showAlertMessage = function (title, message, alertType, alertFadeoutTimeout = app.settings.defaultAlertMessageTimeout) {
        if (alertType !== "success" || alertType !== "warning" || alertType !== "info" || alertType !== "danger") {
            return;
        }

        if (title === null || title === "" || typeof title === 'undefined') {
            title = alertType.charAt(0).toUpperCase() + alertType.slice(1);
        }

        $('#alertPlaceHolder').append(
            '<div class="fixed-alert">' +
                '<div class="alert alert-' + alertType + ' alert-dismissible">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                    '<h4><i class="icon fa fa-warning"></i> ' + title + '</h4>' + message +
                '</div>' +
            '</div>');
        if (alertFadeoutTimeout !== undefined && alertFadeoutTimeout > 0) {
            $("#alertPlaceHolder div.fixed-alert").fadeOut(alertFadeoutTimeout, function () { $(this).remove(); });
        }
    };

    app.unblockUI = function () {
        $.unblockUI();
    };

    app.blockUI = function (msg = "Please wait..") {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            overlayCSS: {
                backgroundColor: '#666',
                opacity: 0.5
            },
            baseZ: 50000,
            message: msg
        });
    };

    app.formValidationErrorHandler = function (form, errorMap, errorList) {
        // Clean up any tooltips for valid elements
        $.each(form.validElements(), function (index, element) {
            var $element = $(element);

            if ($element.hasClass("select2")) {
                var value = "select2-" + $element.attr("id") + '-container';
                $element = $("[aria-labelledby='" + value + "']");
            }           

            $element.data("title", "") // Clear the title - there is no error associated anymore
                .removeClass("error-validation")
                .tooltip("destroy");
        });

        // Create new tooltips for invalid elements
        $.each(errorList, function (index, error) {
            var $element = $(error.element);

            if ($element.hasClass("select2")) {
                var value = "select2-" + $element.attr("id") + '-container';
                $element = $("[aria-labelledby='" + value + "']");
            }           

            $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                .data("title", error.message)
                .addClass("error-validation")
                .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
        });
    }

    // Handle general AJAX error
    app.handleAjaxError = function (data, xr) {
        // Skip errors due to abort state
        if (xr.statusText == "abort") {
            return;
        }

        if (xr.getResponseHeader("X-Responded-JSON") != null) {
            // Handle unauthorized AJAX call
            if (JSON.parse(xr.getResponseHeader("X-Responded-JSON")).status == 401)
                window.location.reload();
        }
        else
        {
            if (xr.responseText != "") {
                if (JSON.parse(xr.responseText).Status == "Error") {
                    app.showNotification("An error occured. Team is notified about error details. Someone might reach to you in order to gather more info.");
                }
            }
        }
    };

    app.showConfirmModal = function (title, message, callback, cancelCallback, yesButtonText, noButtonText) {

        var element = document.getElementById("confirmDialog");
        if (element === null) {
            $("body").append('<div data-backdrop="static" class="modal fade" id="confirmDialog" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title" id="confirmDialongTitle"></h4>' +
                '</div>' +
                '<div id="confirmDialongBody" class="modal-body" style="text-align: center;">' +
                '</div>' +
                '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" data-dismiss="modal">No</button>' +
                '<button type="button" class="btn btn-primary" data-dismiss="modal">Yes</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>');
        }

        var modalDialog = $("#confirmDialog");

        $("#confirmDialongTitle", modalDialog).html(title);
        $("#confirmDialongBody", modalDialog).html(message);
        if (title == "Warning") {
            $('.modal-header', modalDialog).addClass("modal-header-warning ");
        }

        if (yesButtonText != undefined) {
            $('.btn-primary', modalDialog).text(yesButtonText);
        } else {
            $('.btn-primary', modalDialog).text("Yes");
        }
        $('.btn-primary', modalDialog).unbind().click(function () {
            modalDialog.modal("hide");
            if ($.isFunction(callback)) {
                callback();
            }
        });

        if (noButtonText != undefined) {
            $('.btn-default', modalDialog).text(noButtonText);
        }
        else {
            $('.btn-default', modalDialog).text("No");
        }

        $('.btn-default', modalDialog).unbind().click(function () {
            modalDialog.modal("hide");
            if ($.isFunction(cancelCallback)) {
                cancelCallback();
            }
        });
        $('.close', modalDialog).unbind().click(function () {
            modalDialog.modal("hide");
            $('.modal-header', modalDialog).removeClass("modal-header-warning ");
            if ($.isFunction(cancelCallback)) {
                cancelCallback();
            }
        });
        modalDialog.modal("show");
    };

    // Private Methods
    function someMethod(item) {
    }
}(window.app = window.app || {}, jQuery));

/**
 * App document ready
 */
$(document).ready(function () {
    app.documentReady();
});

/**
 * App handle general ajax error
 */
$(document).ajaxError(function (data, xr) {
    app.handleAjaxError(data, xr);
});

/**
 * DOM
 */
window.onerror = function (msg, url, line) {
    app.logError(msg, arguments.callee.trace());
}
