using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTItem")]
    public partial class QTItem
    {
        public int Id { get; set; }

        public string Company { get; set; }

        public string ItemNum { get; set; }

        public string ItemDescription { get; set; }

        public decimal? SellingListPrice { get; set; }

        public decimal? SellingFloorPrice { get; set; }

        public decimal? SalesCost { get; set; }

        public decimal? FreightClass { get; set; }

        public decimal? UnitNetWeight { get; set; }

        public string SellingUOM { get; set; }

        public bool? BuyToOrder { get; set; }

        public bool? DropShip { get; set; }

        public bool? ShipCold { get; set; }

        public bool? ShipFrozen { get; set; }

        public bool? ShipOvernight { get; set; }

        public bool? HazItem { get; set; }

        public decimal? CommissionCost { get; set; }

        public decimal? CostUD19 { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string VendorId { get; set; }

        public string VendorName { get; set; }

        public bool? PrivateLabel { get; set; }

        public bool? OnHold { get; set; }

        public bool? Inactive { get; set; }

        public string ClassId { get; set; }

        public bool? RawSaleable { get; set; }

        public bool? WebSaleable { get; set; }

        public string CustomCustID { get; set; }
    }
}
