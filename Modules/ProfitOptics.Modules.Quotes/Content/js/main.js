﻿(function (quotes, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    // Public properties
    quotes.somePublicProperty = {};

    // Public Methods
    quotes.somePublicMethod = function () {
    };

    // Urls will be set in Quotes area _Layout.cshtml
    quotes.urls = {};

    // QuotesHeader jquery datatables object
    quotes.headerTable = null;
    quotes.itemsTable = null;
    quotes.quoteItemsTable = null;
    quotes.customerTable = null;
    quotes.quoteItemsFromPriceListTable = null;
    quotes.quoteItemsFromStandingOrderTable = null;

    quotes.freightPaymentType = null;

    quotes.getShipToZip = function (shipToId, callback) {
        // ajax call will be used 
        $.ajax({
            url: quotes.urls["getShipToZip"],
            type: "json",
            method: "POST",
            data: { shipToId: shipToId },
            success: function (res) {
                if (typeof callback == "function") {
                    callback(res.shipToZip);
                }
            },
            error: function (res) {
                app.showAlertMessage("Error getting ToZip", "", "error");
            }
        });
    };

    var autosaveInProgress = false;
    var autosaveTimeout = null;

    // when user tries to navigate from page but there are unsaved changes, ask for confirmation
    window.onbeforeunload = function () {
        if (autosaveInProgress || autosaveTimeout) {
            return 'Changes to this quote have not been saved. If you leave, your changes will be lost.';
        } else {
            return null;
        }
    };

    // display a success alert about autosave, and slowly fade it out
    function autosaveAlert(title) {
        app.showAlertMessage(title, "", "success");
        var theAlert = $('.alert-success');
        theAlert.css('opacity', '0.7').delay(2500).fadeOut(2500, function () {
            theAlert.remove();
        });
    }

    // if an autosave is not already scheduled, schedule it in e.g. 5000 milliseconds
    quotes.scheduleAutosave = function (saveFunction) {
        if (!autosaveTimeout) {
            autosaveTimeout = setTimeout(function () {
                quotes.autosave(saveFunction)
            }, 5000);
        }
    };
    // cancel any scheduled autosave
    quotes.unscheduleAutosave = function () {
        clearTimeout(autosaveTimeout);
        autosaveTimeout = undefined;
    };

    quotes.autosave = function (saveFunction) {
        // in case an autosave is scheduled for later, cancel it
        quotes.unscheduleAutosave();
        // if an autosave is already in progress (the request has been sent), reschedule it
        if (autosaveInProgress) {
            quotes.scheduleAutosave(saveFunction);
            return;
        }
        // do the save
        if (typeof saveFunction === "function") {
            saveFunction();
        }
    };

    quotes.updateQuote = function (quote) {
        // ajax call will be used 
        autosaveAlert("Saving...");
        autosaveInProgress = true;
        $.ajax({
            url: quotes.urls["updateQuote"],
            type: "json",
            method: "POST",
            data: quote,
            success: function (res) {
                autosaveInProgress = false;
                // if autosaveTimeout is set, it means there's another autosave scheduled,
                // so don't display display any misleading alerts or reload the header table
                if (!autosaveTimeout) {
                    if (res.success) {
                        autosaveAlert("Saved!");
                        if (res.shouldRecalculateFreight) {
                            quotes.calculateFreight(quote.QuoteId, function (freightCost, totAdjMarginPerc, totFloorMarginPerc) {
                                if (freightCost != null)
                                    $('#estNetShippingCost').val(freightCost.formatMoney(2, '.', ','));
                                //quotes.headerTable.ajax.reload();
                            });
                        } else {
                            //quotes.headerTable.ajax.reload();
                        }

                    } else {
                        app.showAlertMessage("Error", res.message, "error");
                    }
                }
            },
            error: function (res) {
                autosaveInProgress = false;
                if (!autosaveTimeout) {
                    app.showAlertMessage("Error", "Quote update error", "error");
                }
            }
        });
    };

    quotes.archiveQuote = function (quoteId) {
        app.blockUI("Please wait...");
        $.ajax({
            url: quotes.urls["deleteQuote"],
            data: {
                quoteId: quoteId
            },
            type: "json",
            method: "POST",
            success: function (data) {
                app.unblockUI();
                window.location.href = "/Quotes/QuotesMain/HeaderQuote";
            },
            error: function (xr, data) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error archiving quote", "error");
            }
        });
    }

    quotes.cloneQuote = function (quoteId, cloneName, customerId) {
        app.blockUI("Please wait...");

        $.ajax({
            url: quotes.urls["cloneQuote"],
            data: {
                quoteId: quoteId,
                cloneName: cloneName,
                customerId: customerId
            },
            type: "json",
            method: "POST",
            success: function (data) {
                window.location.href = '/Quotes/QuotesMain/DetailQuote?quoteId=' + data.quoteId
                app.unblockUI();
            },
            error: function (xr, data) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error deleting quote", "error");
            }
        });
    }

    quotes.unarchiveQuote = function (quoteId, callback) {
        // ajax call will be used 
        app.blockUI("Please wait...");
        $.ajax({
            url: quotes.urls["unarchiveQuote"],
            type: "json",
            method: "POST",
            data: { quoteId: quoteId },
            success: function (res) {
                app.unblockUI();
                if (typeof callback == "function") {
                    callback();
                }
            },
            error: function (res) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while unarchiving quote", "error");
            }
        });
    };

    quotes.updateItemQty = function (quoteId, itemId, qty, tr) {
        qty = qty.replace('$', '');
        if (isNaN(qty) || !Number.isInteger(parseFloat(qty)) || qty < 0) {
            app.showAlertMessage("Error", "Please enter valid intiger number", "error");
            var theAlert = $('.alert-error');
            theAlert.fadeOut(5000, function () {
                theAlert.remove();
            });
            return;
        } else {
            // ajax call will be used 
            app.blockUI("Please wait...");
            $.ajax({
                url: quotes.urls["updateQuoteItemQty"],
                type: "json",
                method: "POST",
                data: {
                    quoteId: quoteId,
                    itemId: itemId,
                    units: qty
                },
                success: function (res) {
                    quotes.calculateFreight(quoteId, function (freightCost, totAdjMarginPerc, totFloorMarginPerc) {
                        quotes.updateFreightCalcValues(freightCost, totAdjMarginPerc, totFloorMarginPerc);
                        quotes.quoteItemsTable.ajax.reload();
                        app.unblockUI();
                    });
                },
                error: function (res) {
                    app.unblockUI();
                    app.showAlertMessage("Error", "Error occurred while updating item quantity", "error");
                }
            });
        }
    };

    quotes.updateItemGMPerc = function (quoteId, itemId, percValue, tr) {
        percValue = percValue.replace('%', '');
        if (isNaN(percValue) || percValue < 0) {
            app.showAlertMessage("Error", "Please enter valid number", "error");
            var theAlert = $('.alert-error');
            theAlert.fadeOut(5000, function () {
                theAlert.remove();
            });
            return;
        } else {
            // ajax call will be used 
            app.blockUI("Please wait...");
            $.ajax({
                url: quotes.urls["updateQuoteItemGmPerc"],
                type: "json",
                method: "POST",
                data: {
                    quoteId: quoteId,
                    itemId: itemId,
                    newValue: percValue
                },
                success: function (res) {
                    quotes.calculateFreight(quoteId, function (freightCost, totAdjMarginPerc, totFloorMarginPerc) {
                        quotes.updateFreightCalcValues(freightCost, totAdjMarginPerc, totFloorMarginPerc);
                        quotes.quoteItemsTable.ajax.reload();
                    });
                },
                error: function (res) {
                    app.unblockUI();
                    app.showAlertMessage("Error", "Error occurred while updating GP%", "error");
                }
            });
        }
    };

    quotes.updateItemGMPrice = function (quoteId, itemId, priceValue, tr) {
        priceValue = priceValue.replace('$', '');
        if (isNaN(priceValue) || priceValue < 0) {
            app.showAlertMessage("Error", "Please enter valid number", "error");
            var theAlert = $('.alert-error');
            theAlert.fadeOut(5000, function () {
                theAlert.remove();
            });
            return;
        } else {
            // ajax call will be used 
            app.blockUI("Please wait...");
            $.ajax({
                url: quotes.urls["updateQuoteItemPrice"],
                type: "json",
                method: "POST",
                data: {
                    quoteId: quoteId,
                    itemId: itemId,
                    newValue: priceValue
                },
                success: function (res) {
                    quotes.calculateFreight(quoteId, function (freightCost, totAdjMarginPerc, totFloorMarginPerc) {
                        quotes.updateFreightCalcValues(freightCost, totAdjMarginPerc, totFloorMarginPerc);
                        quotes.quoteItemsTable.ajax.reload();
                    });
                },
                error: function (res) {
                    app.unblockUI();
                    app.showAlertMessage("Error", "Error occurred while updating GM price", "error");
                }
            });
        }
    };

    quotes.updateQuoteDetails = function (quote) {
        // ajax call will be used 
        autosaveAlert("Saving...");
        autosaveInProgress = true;
        $.ajax({
            url: quotes.urls["updateQuoteDetails"],
            type: "json",
            method: "POST",
            data: quote,
            success: function (res) {
                autosaveInProgress = false;
                // if autosaveTimeout is set, it means there's another autosave scheduled,
                // so don't display display any misleading alerts or reload the header table
                if (!autosaveTimeout) {
                    if (res.success) {
                        autosaveAlert("Saved!");
                        if (res.shouldRecalculateFreight) {
                            quotes.calculateFreight(quote.QuoteId, function (freightCost, totAdjMarginPerc, totFloorMarginPerc) {
                                quotes.updateFreightCalcValues(freightCost, totAdjMarginPerc, totFloorMarginPerc);
                                quotes.quoteItemsTable.ajax.reload();
                            });
                        }
                    } else {
                        app.showAlertMessage("Error", res.message, "error");
                    }
                }
            },
            error: function (res) {
                autosaveInProgress = false;
                if (!autosaveTimeout) {
                    app.showAlertMessage("Error", "Quote update error", "error");
                }
            }
        });
    };

    quotes.removeItemFromQuote = function (quoteId, itemId) {
        app.blockUI("Please wait...");
        $.ajax({
            type: 'post',
            url: quotes.urls["removeItemFromQuote"],
            data: {
                quoteId: quoteId,
                itemId: itemId
            },
            dataType: 'json',
            cache: false,
            timeout: 1000000,
            success: function (data) {
                quotes.calculateFreight(quoteId, function (freightCost, totAdjMarginPerc, totFloorMarginPerc) {

                    quotes.updateFreightCalcValues(freightCost, totAdjMarginPerc, totFloorMarginPerc);
                    quotes.quoteItemsTable.ajax.reload();
                });
            },
            error: function (xr, data) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while removing item from the quote", "error");
            }
        });
    };

    quotes.removeItemListFromQuote = function (quoteId, items) {
        bootbox.confirm("Are you sure you want to remove items from quote?", function (result) {
            if (result == true) {
                app.blockUI("Please wait...");
                $.ajax({
                    type: 'post',
                    url: quotes.urls["removeItemListFromQuote"],
                    data: {
                        quoteId: quoteId,
                        items: items
                    },
                    dataType: 'json',
                    cache: false,
                    timeout: 1000000,
                    success: function (data) {
                        quotes.calculateFreight(quoteId, function (freightCost, totAdjMarginPerc, totFloorMarginPerc) {

                            quotes.updateFreightCalcValues(freightCost, totAdjMarginPerc, totFloorMarginPerc);
                            quotes.quoteItemsTable.ajax.reload();
                        });
                    },
                    error: function (xr, data) {
                        app.unblockUI();
                        app.showAlertMessage("Error", "Error occurred while removing items from the quote", "error");
                    }
                });
            }


        });
    };


    quotes.updateFreightCalcValues = function (freightCost, totAdjMarginPerc, totFloorMarginPerc) {
        var valuesNotAvailable = (freightCost === null);

        if (valuesNotAvailable) {
            $('[name="AverageShippingCost"]').val('N/A');
            $('[name="TotAdjMarginPerc"]').text('N/A');
        } else {
            $('[name="AverageShippingCost"]').val(freightCost.formatMoney(2, '.', ','));
            $('[name="TotAdjMarginPerc"]').text(totAdjMarginPerc != null ? totAdjMarginPerc.formatPercent(2) : "N/A");
        }

        $('[name="TotAdjMarginPerc"]').removeClass("acceptable-margin").removeClass("notacceptable-margin");

        if (totAdjMarginPerc == null || valuesNotAvailable || (parseFloat(totAdjMarginPerc) < parseFloat(totFloorMarginPerc))) {
            $("#RequestApprovalBtn").show();
            $('[name="TotAdjMarginPerc"]').addClass("notacceptable-margin");
        } else {
            $("#RequestApprovalBtn").hide();
            $('[name="TotAdjMarginPerc"]').addClass("acceptable-margin");
        }
    }

    quotes.calculateFreight = function (quoteId, callback, errorCallback) {
        app.blockUI("Please wait while Est Net Shipping Cost (Freight) is being calculated...");
        $.ajax({
            url: quotes.urls["calculateFreight"],
            type: "json",
            method: "POST",
            data: { quoteId: quoteId },
            success: function (res) {
                if (res.success) {
                    var freightCost = res.averageShippingCost;
                    var totFloorMarginPerc = res.totFloorMarginPerc;
                    var totAdjMarginPerc = res.totAdjMarginPerc;

                    callback(freightCost, totAdjMarginPerc, totFloorMarginPerc);
                    app.unblockUI();
                    app.showAlertMessage("Success", "Freight Calculation Completed", "success");
                    var theAlert = $('.alert-success');
                    theAlert.fadeOut(5000, function () {
                        theAlert.remove();
                    });
                } else {
                    if (typeof errorCallback === 'function') {
                        errorCallback(res);
                    } else {
                        app.unblockUI();
                        app.showAlert({
                            title: "Freight Calculation Couldn't be completed",
                            message: res.message ||
                                'Please, check that the "From Zip", "Ship To" and item freight class values are valid.'
                        });
                        quotes.quoteItemsTable.ajax.reload();
                    }
                }
            },
            error: function (res) {
                if (typeof errorCallback === 'function') {
                    errorCallback(res);
                } else {
                    app.unblockUI();
                    app.showAlertMessage("Error", res.message || "Freight Calculation Error", "error");
                }
            }
        });
    };

    quotes.customerFacingXls = function (quoteId) {
        app.blockUI("Please wait...");
        $.ajax({
            url: quotes.urls["customerFacingXls"],
            type: "json",
            method: "POST",
            data: { quoteId: quoteId },
            success: function (res) {
                if (res.success) {
                    app.unblockUI();
                    var link = quotes.urls["downloadFile"];
                    link = link.replace("__handle__", res.handle);
                    link = link.replace("__filename__", res.filename);
                    window.location.href = link;
                } else {
                    app.unblockUI();
                    app.showAlertMessage("Error", res.message || "Error Generating Xlsx", "error");
                }
            },
            error: function (res) {
                app.unblockUI();
                app.showAlertMessage("Error", res.message || "Error Generating Xlsx", "error");
            }
        });
    };

    quotes.beforeCreateQuote = function () {
        var customerId = $("#CustomerId").val();
        var quoteName = $("#QuoteName").val();
        if (customerId == "" || quoteName == "") {
            app.showAlertMessage("Error", "Please select Customer and add Quote Name", "error");
            var theAlert = $('.alert-error');
            theAlert.fadeOut(7000, function () {
                theAlert.remove();
            });
            return false;
        } else {
            var form = $("#createQuoteForm");
            form.submit();
        }
        return false;
    }

    quotes.getItemListFromText = function (text) {
        var itemNumbers = [];
        var tmp = text.split("\n");
        tmp.forEach(function (s) {
            var tmp1 = s.split(" ");
            tmp1.forEach(function (p) {
                var tmp2 = p.split(",");
                tmp2.forEach(function (q) {
                    var tmp3 = q.split(";");
                    tmp3.forEach(function (s) {
                        var item = s.trim();
                        if (item != "") {
                            itemNumbers.push(item);
                        }
                    });
                });
            });
        });
        return itemNumbers;
    }

    quotes.validateAndAddItemsFromList = function (quoteId, text) {
        var itemNumbers = quotes.getItemListFromText(text);
        app.blockUI("Please wait...");
        $.ajax({
            url: quotes.urls["validateItemsFromList"],
            type: "json",
            method: "POST",
            data: {
                quoteId: quoteId,
                itemsList: itemNumbers
            },
            success: function (res) {
                var currentHtml = "<textarea cols='5' rows='10' class='form-control'></textarea>";

                if (res.success) {
                    app.unblockUI();
                    $("#validateModalTitle").html("All items are valid");
                    $("#ValidateItemsModal").find('.modal-body').html("Do you want to continue adding items to the Quote?");
                    $("#ValidateItemsModal").modal('show');
                } else {
                    app.unblockUI();
                    var notValid = res.notValid;
                    var message = "";
                    notValid.forEach(function (p) {
                        message += p + "<br/>";
                    });
                    message += "<br/><br/>" + "Do you want to continue adding the valid items to the Quote?";

                    $("#validateModalTitle").html("The following item(s) are not valid item number(s):");
                    $("#ValidateItemsModal").find('.modal-body').html(message);
                    $("#ValidateItemsModal").modal('show');
                }
            },
            error: function (res) {
                app.unblockUI();
                app.showAlertMessage("Error", res.message || "Error adding items", "error");
            }
        });
    }

    quotes.addItemsFromList = function (quoteId, text) {
        var itemNumbers = quotes.getItemListFromText(text);

        app.blockUI("Please wait...");
        $.ajax({
            url: quotes.urls["addItemsFromList"],
            type: "json",
            method: "POST",
            data: {
                quoteId: quoteId,
                itemsList: itemNumbers
            },
            success: function (res) {
                if (res.success) {
                    $("#AddItemsFromListModal").modal('hide');
                    $("#ValidateItemsModal").modal('hide');
                    app.unblockUI();
                    quotes.calculateFreight(quoteId, function (freightCost, totAdjMarginPerc, totFloorMarginPerc) {
                        quotes.updateFreightCalcValues(freightCost, totAdjMarginPerc, totFloorMarginPerc);
                        quotes.quoteItemsTable.ajax.reload();
                        quotes.itemsTable.ajax.reload();
                        app.unblockUI();
                    });
                } else {
                    app.unblockUI();
                    app.showAlertMessage("Error", res.message || "Error adding items", "error");
                }
            },
            error: function (res) {
                app.unblockUI();
                app.showAlertMessage("Error", res.message || "Error adding items", "error");
            }
        });
    }

    quotes.updateCompetitorPrice = function (quoteId, itemId, value) {
        value = value.replace('$', '');
        if (isNaN(value) || value < 0) {
            app.showAlertMessage("Error", "Please enter valid number", "error");
            var theAlert = $('.alert-error');
            theAlert.fadeOut(5000, function () {
                theAlert.remove();
            });
            return;
        } else {
            // ajax call will be used 
            app.blockUI("Please wait...");
            $.ajax({
                url: quotes.urls["updateCompetitorPrice"],
                type: "json",
                method: "POST",
                data: {
                    quoteId: quoteId,
                    itemId: itemId,
                    newValue: value
                },
                success: function (res) {
                    app.unblockUI();
                    quotes.quoteItemsTable.ajax.reload();
                },
                error: function (res) {
                    app.unblockUI();
                    app.showAlertMessage("Error", "Error occurred while updating competitor price", "error");
                }
            });
        }
    }

    quotes.updateCompetitorUoM = function (quoteId, itemId, value) {
        // ajax call will be used 
        app.blockUI("Please wait...");
        $.ajax({
            url: quotes.urls["updateCompetitorUoM"],
            type: "json",
            method: "POST",
            data: {
                quoteId: quoteId,
                itemId: itemId,
                newValue: value
            },
            success: function (res) {
                app.unblockUI();
                quotes.quoteItemsTable.ajax.reload();
            },
            error: function (res) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while updating competitor UoM", "error");
            }
        });
    }
    quotes.revertPriceToPlcPrice = function (quoteId, itemId) {
        // ajax call will be used 
        app.blockUI("Please wait...");
        $.ajax({
            url: quotes.urls["revertPriceToPlcPrice"],
            type: "json",
            method: "POST",
            data: {
                quoteId: quoteId,
                itemId: itemId
            },
            success: function (res) {
                app.unblockUI();
                quotes.quoteItemsTable.ajax.reload();
            },
            error: function (res) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while retriving price to PLC price", "error");
            }
        });
    }

    quotes.updateCompetitorCf = function (quoteId, itemId, value) {
        value = value.replace('$', '');
        if (isNaN(value) || value < 0) {
            app.showAlertMessage("Error", "Please enter valid number", "error");
            var theAlert = $('.alert-error');
            theAlert.fadeOut(5000, function () {
                theAlert.remove();
            });
            return;
        } else {
            // ajax call will be used 
            app.blockUI("Please wait...");
            $.ajax({
                url: quotes.urls["updateCompetitorCf"],
                type: "json",
                method: "POST",
                data: {
                    quoteId: quoteId,
                    itemId: itemId,
                    newValue: value
                },
                success: function (res) {
                    app.unblockUI();
                    quotes.quoteItemsTable.ajax.reload();
                },
                error: function (res) {
                    app.unblockUI();
                    app.showAlertMessage("Error", "Error occurred while updating competitor CF", "error");
                }
            });
        }
    }

    // Private Methods
    function somePrivateMethod(item) {
    }
}(window.quotes = window.quotes || {}, jQuery));