﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTUserProfileConfig : IEntityTypeConfiguration<QTUserProfile>
    {
        public void Configure(EntityTypeBuilder<QTUserProfile> builder)
        {
            builder.ToTable("QTUserProfile");
            builder.HasKey(entity => entity.UserId);

            builder.Property(entity => entity.Company).HasMaxLength(50);
            builder.Property(entity => entity.SalesRepCode).HasMaxLength(50);
            builder.Property(entity => entity.SalesRepName).HasMaxLength(50);
            builder.Property(entity => entity.SalesRepRole).HasMaxLength(50);
            builder.Property(entity => entity.SalesRepRoleDescription).HasMaxLength(50);
            builder.Property(entity => entity.SalesManagerCode).HasMaxLength(50);
            builder.Property(entity => entity.SalesManagerName).HasMaxLength(50);
        }
    }
}
