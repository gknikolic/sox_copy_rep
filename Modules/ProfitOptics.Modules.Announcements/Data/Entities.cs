﻿using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Modules.Announcements.Data.Config;
using ProfitOptics.Modules.Announcements.Data.Domain;

namespace ProfitOptics.Modules.Announcements.Data
{
    public class Entities : DbContext
    {
        public Entities(DbContextOptions<Entities> options) : base(options)
        {
            var listener = this.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }

        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<POUserProfilePicture> POUserProfilePictures { get; set; }
        public virtual DbSet<POAnnouncement> POAnnouncements { get; set; }
        public virtual DbSet<POAnnouncementRead> POAnnouncementReads { get; set; }
        public virtual DbSet<POAnnouncementRole> POAnnouncementRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new AspNetRolesConfig());
            builder.ApplyConfiguration(new AspNetUsersConfig());
            builder.ApplyConfiguration(new AspNetUserRolesConfig());
            builder.ApplyConfiguration(new POUserProfilePictureConfig());
            builder.ApplyConfiguration(new POAnnouncementConfig());
            builder.ApplyConfiguration(new POAnnouncementReadsConfig());
            builder.ApplyConfiguration(new POAnnouncementRolesConfig());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}
