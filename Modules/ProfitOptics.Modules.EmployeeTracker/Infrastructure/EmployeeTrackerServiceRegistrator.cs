﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.EmployeeTracker.Data;
using ProfitOptics.Modules.EmployeeTracker.Factories;
using ProfitOptics.Modules.EmployeeTracker.Services;

namespace ProfitOptics.Modules.EmployeeTracker.Infrastructure
{
    public sealed class EmployeeTrackerServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<IEmployeeTrackerService, EmployeeTrackerService>();
            services.AddScoped<IEmployeeTrackerFactory, EmployeeTrackerFactory>();
        }
    }
}
