﻿using Intuit.Ipp.OAuth2PlatformClient;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Sox.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProfitOptics.Modules.Sox.Services
{
    public class HelperService : IHelperService
    {
        private readonly IUserService _userService;
        private readonly IGroundControlService _groundControlService;
        public HelperService(IUserService userService,
                                     IGroundControlService groundControlService)
        {
            this._userService = userService;
            this._groundControlService = groundControlService;
        }

        public string GetQuickBooksAccessToken(int userId)
        {
            string token = null;
            var claims = _userService.GetUserClaims(userId, "quickbooks_").ToList();
            var userName = string.Empty;
            var user = _userService.FindUserByIdAsync(userId).Result;
            if (user != null)
            {
                userName = user.UserName;
            }

            //TokenResponse tokenResponse = null;
            //var tokenUpdated = false;
            //POSetting refreshTokenPOSetting = null;
            //POSetting refreshTokenExpiresAtPOSetting = null;

            //if (claims != null)
            //{
            //    var access_token = claims.Where(x => x.ClaimType == Enums.QBTokenEnum.quickbooks_access_token.ToString()).FirstOrDefault();
            //    var access_token_expires_at = claims.Where(x => x.ClaimType == Enums.QBTokenEnum.quickbooks_access_token_expires_at.ToString()).FirstOrDefault();

            //    if (access_token != null && access_token_expires_at != null)
            //    {
            //        DateTime expiresAt = DateTime.MinValue;

            //        if (DateTime.TryParse(access_token_expires_at.ClaimValue, out expiresAt) && expiresAt > DateTime.MinValue)
            //        {
            //            if (expiresAt > DateTime.UtcNow)
            //            {
            //                token = access_token.ClaimValue;
            //            }
            //        }
            //    }

            //    if (token == null)
            //    {
            //        string refresh_token = null;
            //        refreshTokenPOSetting = _groundControlService.GetPOSetting(Enums.POSettingsType.QuickBooksRefreshToken);
            //        if (refreshTokenPOSetting != null)
            //        {
            //            refresh_token = refreshTokenPOSetting.Value;
            //        }
            //        refreshTokenExpiresAtPOSetting = _groundControlService.GetPOSetting(Enums.POSettingsType.QuickBooksRefreshTokenExpiresAt);

            //        if (refresh_token != null && refreshTokenExpiresAtPOSetting != null)
            //        {
            //            DateTime expiresAt = DateTime.MinValue;
            //            if (DateTime.TryParse(refreshTokenExpiresAtPOSetting.Value, out expiresAt) && expiresAt > DateTime.MinValue)
            //            {
            //                if (expiresAt > DateTime.UtcNow)
            //                {
            //                    tokenResponse = _groundControlService.RefreshQBAuthorizeToken(refresh_token).Result;
            //                    tokenUpdated = true;
            //                }
            //            }
            //        }
            //    }
            //}

            //if (tokenUpdated && tokenResponse != null)
            //{
            //    var dict = new Dictionary<string, string>();
            //    dict.Add(Enums.QBTokenEnum.quickbooks_access_token.ToString(), tokenResponse.AccessToken);
            //    dict.Add(Enums.QBTokenEnum.quickbooks_access_token_expires_at.ToString(), DateTime.UtcNow.AddSeconds(tokenResponse.AccessTokenExpiresIn).ToString());
            //    _groundControlService.AddOrEditUserClaims(userId, dict);
            //    token = tokenResponse.AccessToken;

            //    if(refreshTokenPOSetting != null && 
            //        refreshTokenExpiresAtPOSetting != null &&
            //        !refreshTokenPOSetting.Value.Equals(tokenResponse.RefreshToken))
            //    {
            //        _groundControlService.AddOrUpdatePOSetting(Enums.POSettingsType.QuickBooksRefreshToken, tokenResponse.RefreshToken, userName);
            //        _groundControlService.AddOrUpdatePOSetting(Enums.POSettingsType.QuickBooksRefreshTokenExpiresAt, DateTime.UtcNow.AddSeconds(tokenResponse.RefreshTokenExpiresIn).ToString(), userName);
            //    }
            //}

            //return token;

            var userClaims = new List<AspNetUserClaims>();
            claims.ForEach(x =>
            {
                userClaims.Add(new AspNetUserClaims()
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    ClaimType = x.ClaimType,
                    ClaimValue = x.ClaimValue
                });
            });

            return _groundControlService.GetQuickBooksAccessToken(userId, userClaims, userName);
        }

        public void RevokeQBAccessTokenAsync(int userId)
        {
            var token = GetQuickBooksAccessToken(userId);
            _groundControlService.RevokeQBAccessTokenAsync(token);
            _groundControlService.RemovePOSetting(POSettingsType.QuickBooksRefreshToken);
            _groundControlService.RemovePOSetting(POSettingsType.QuickBooksRefreshTokenExpiresAt);
            _userService.RemoveClaim(Enums.QBTokenEnum.quickbooks_access_token.ToString(), null);
            _userService.RemoveClaim(Enums.QBTokenEnum.quickbooks_access_token_expires_at.ToString(), null);
        }
    }
}
