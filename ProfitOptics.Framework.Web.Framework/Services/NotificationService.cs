﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using ProfitOptics.Framework.Web.Framework.Helpers.Notification;

namespace ProfitOptics.Framework.Web.Framework.Services
{
    public class NotificationService : INotificationService
    {
        public const string NextRequestNotificationType = "NextRequestNotificationType";
        public const string NextRequestNotificationMessage = "NextRequestNotificationMessage";

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ITempDataDictionaryFactory _tempDataDictionaryFactory;

        public NotificationService(IHttpContextAccessor httpContextAccessor, ITempDataDictionaryFactory tempDataDictionaryFactory)
        {
            _httpContextAccessor = httpContextAccessor;
            _tempDataDictionaryFactory = tempDataDictionaryFactory;
        }

        public void SetNextRequestNotification(NotificationType notificationType, string message)
        {
            SetNextRequestNotifications(notificationType, new List<string>() { message });
        }

        public void SetNextRequestNotifications(NotificationType notificationType, IEnumerable<string> messages)
        {
            ITempDataDictionary tempData = GetTempData();

            tempData[NextRequestNotificationType] = notificationType;
            tempData[NextRequestNotificationMessage] = messages.ToList();
        }

        public NotificationType? GetNotificationType()
        {
            ITempDataDictionary tempData = GetTempData();

            return (NotificationType)tempData[NextRequestNotificationType];
        }

        public IList<string> GetNotificationMessages()
        {
            ITempDataDictionary tempData = GetTempData();

            return tempData[NextRequestNotificationMessage] as IList<string>;
        }

        public bool HasNotification()
        {
            if (GetNotificationMessages() != null && GetNotificationType() != null)
            {
                return true;
            }

            return false;
        }

        private ITempDataDictionary GetTempData()
        {
            HttpContext httpContext = _httpContextAccessor.HttpContext;
            ITempDataDictionary tempData = _tempDataDictionaryFactory.GetTempData(httpContext);

            return tempData;
        }
    }
}