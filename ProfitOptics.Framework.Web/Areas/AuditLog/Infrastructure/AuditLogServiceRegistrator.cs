﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Areas.AuditLog.Data;
using ProfitOptics.Framework.Web.Areas.AuditLog.Services;
using ProfitOptics.Framework.Web.Framework.Infrastructure;

namespace ProfitOptics.Framework.Web.Areas.AuditLog.Infrastructure
{
    public sealed class AuditLogServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<IAuditLogService, AuditLogService>();

            var auditLogService = (AuditLogService)services.BuildServiceProvider().GetService(typeof(IAuditLogService));
            auditLogService.UpdateAuditLogFriendlyNameData();
        }
    }
}
