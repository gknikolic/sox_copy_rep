﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;
using ProfitOptics.Modules.Quotes.Data;
using ProfitOptics.Modules.Quotes.Factories;
using ProfitOptics.Modules.Quotes.Helpers;
using ProfitOptics.Modules.Quotes.Services;

namespace ProfitOptics.Modules.Quotes.Infrastructure
{
    public sealed class QuotesServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<ISalesRepService, SalesRepService>();
            services.AddScoped<IQuoteService, QuoteService>();
            services.AddScoped<IQuoteFactory, QuoteFactory>();
            services.AddScoped<IUserAssignment, QuoteUserAssignment>();
        }
    }
}