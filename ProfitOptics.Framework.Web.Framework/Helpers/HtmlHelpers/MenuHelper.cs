﻿using System;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ProfitOptics.Framework.Web.Framework.Helpers.HtmlHelpers
{
    public static class MenuExtensions
    {
        public static IDisposable MenuHolder(this IHtmlHelper htmlHelper)
        {
            return new DisposableHolder(
                () => htmlHelper.ViewContext.Writer.Write("<ul class='sidebar-menu'>"),
                () => htmlHelper.ViewContext.Writer.Write("</ul>")
            );
        }

        public static IDisposable AreaItem(this IHtmlHelper htmlHelper, string id, string label, string icon)
        {
            return new DisposableHolder(
                () => htmlHelper.ViewContext.Writer.Write("<li class='treeview' id='" + id + "'><a href='javascript:void(0)'><i class='fa " + icon + "'></i><span>" + label + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a><ul class='treeview-menu'>"),
                () => htmlHelper.ViewContext.Writer.Write("</ul></li>")
            );
        }

        public static IHtmlContent ActionItem(this IHtmlHelper htmlHelper, string id, string label, string icon, string url)
        {
            TagBuilder li = new TagBuilder("li");
            TagBuilder a = new TagBuilder("a");
            TagBuilder i = new TagBuilder("i");
            TagBuilder span = new TagBuilder("span");

            li.Attributes.Add("id", id);
            a.Attributes.Add("href", url);
            i.AddCssClass("fa " + icon);
            span.InnerHtml.SetContent(label);

            a.InnerHtml.AppendHtml(GetString(i) + GetString(span));
            li.InnerHtml.AppendHtml(GetString(a));

            return li;
        }

        public static string GetString(IHtmlContent content)
        {
            var writer = new System.IO.StringWriter();
            content.WriteTo(writer, HtmlEncoder.Default);
            return writer.ToString();
        }
    }

    class DisposableHolder : IDisposable
    {
        private Action end;

        public DisposableHolder(Action begin, Action end)
        {
            this.end = end;
            begin();
        }

        public void Dispose()
        {
            end();
        }
    }
}
