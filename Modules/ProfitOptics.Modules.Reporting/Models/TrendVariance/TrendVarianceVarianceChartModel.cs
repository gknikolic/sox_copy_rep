﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVarianceVarianceChartModel
    {
        public int Year { get; set; }

        public List<VarianceChartMonthlyData> MonthlyData { get; set; }

        public class VarianceChartMonthlyData
        {
            public int Month { get; set; }

            public decimal Sales { get; set; }

            public decimal GrossProfit { get; set; }

            public decimal GrossProfitPercent { get; set; }
        }
    }
}