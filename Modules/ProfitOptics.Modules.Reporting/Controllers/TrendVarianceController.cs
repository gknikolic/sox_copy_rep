﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Factories.TrendVariance;
using ProfitOptics.Modules.Reporting.Models.TrendVariance;
using ProfitOptics.Modules.Reporting.Services.TrendVariance;

namespace ProfitOptics.Modules.Reporting.Controllers
{
    [Authorize(Roles = "admin, reporting")]
    public class TrendVarianceController : BaseReportingController
    {
        private readonly ITrendVarianceModelFactory _trendVarianceModelFactory;
        private readonly ITrendVarianceExportService _trendVarianceExportService;

        public TrendVarianceController(ITrendVarianceModelFactory trendVarianceModelFactory, ITrendVarianceExportService trendVarianceExportService)
        {
            _trendVarianceModelFactory = trendVarianceModelFactory;
            _trendVarianceExportService = trendVarianceExportService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            TrendVarianceViewModel vm = _trendVarianceModelFactory.CreateTrendVarianceViewModel(GetCurrentUserId());

            return View(vm);
        }
        
        [HttpPost]
        public IActionResult GetYearOverYearVariances([ModelBinder(typeof(DataTablesBinder))]IDataTablesRequest request)
        {
            List<TrendVarianceYearOverYearVarianceModel> models = _trendVarianceModelFactory.CreateYearOverYearVarianceModels(GetCurrentUserId(), request);

            return LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpPost]
        public IActionResult GetVariancesByTypeForParameters([ModelBinder(typeof(DataTablesBinder))]IDataTablesRequest request,
            TrendVarianceVarianceByTypeRequestParameters parameters)
        {
            PagedList<TrendVarianceVarianceByTypeModel> models = _trendVarianceModelFactory.CreateVarianceModels(GetCurrentUserId(),
                request, parameters);

            return LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpGet]
        public IActionResult ExportVariancesByTypeForParameters([FromQuery]TrendVarianceVarianceByTypeRequestParameters parameters)
        {
            MemoryStream memoryStream = _trendVarianceExportService.ExportVariancesByType(GetCurrentUserId(), parameters);

            string title = $"{Enum.GetName(typeof(ViewByType), parameters.ViewByType)} {Enum.GetName(typeof(ComparisonType), parameters.ComparisonType)}";
            
            return File(memoryStream, ExcelFile.ExcelContentType, $"{title} - {DateTime.Now:MM-dd-yyyy}.xlsx");
        }

        [HttpGet]
        public IActionResult GetVariancesForChart()
        {
            return Json(_trendVarianceModelFactory.CreateSalesHistoryForMonthModels(GetCurrentUserId()));
        }

        [HttpGet]
        public IActionResult CustomerDrillThroughForBillTo(int id, ComparisonType comparisonType,
            DateTime? comparisonStartDate, DateTime? comparisonEndDate)
        {
            TrendVarianceBillToCustomerDrillThroughViewModel vm = _trendVarianceModelFactory.CreateCustomerDrillThroughViewModelForBillToId(
                GetCurrentUserId(), id, comparisonType, comparisonStartDate, comparisonEndDate, Url);
            
            return View("CustomerDrillThrough", vm);
        }

        [HttpPost]
        public IActionResult GetItemCategoryVariancesForBillTo([ModelBinder(typeof(DataTablesBinder))]IDataTablesRequest request,
            TrendVarianceBillToDrillThroughVarianceParameters parameters)
        {
            PagedList<TrendVarianceVarianceByTypeModel> models = _trendVarianceModelFactory
                .CreateVarianceModelsForBillToParameters(GetCurrentUserId(), request, parameters);

            return LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpGet]
        public IActionResult CustomerDrillThroughForShipTo(int id, ComparisonType comparisonType,
            DateTime? comparisonStartDate, DateTime? comparisonEndDate)
        {
            TrendVarianceShipToCustomerDrillThroughViewModel vm = _trendVarianceModelFactory.CreateCustomerDrillThroughViewModelForShipToId(
                GetCurrentUserId(), id, comparisonType, comparisonStartDate, comparisonEndDate, Url);

            return View("CustomerDrillThrough", vm);
        }

        [HttpPost]
        public IActionResult GetItemCategoryVariancesForShipTo([ModelBinder(typeof(DataTablesBinder))]IDataTablesRequest request,
            TrendVarianceShipToDrillThroughVarianceParameters parameters)
        {
            PagedList<TrendVarianceVarianceByTypeModel> models = _trendVarianceModelFactory
                .CreateVarianceModelsForShipToParameters(GetCurrentUserId(), request, parameters);

            return LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpGet]
        public IActionResult ExportItemCategoryVariancesForCustomer(bool isBillTo, int customerId, string customerName,
            ComparisonType comparisonType, DateTime? comparisonStartDate, DateTime? comparisonEndDate)
        {
            MemoryStream memoryStream = _trendVarianceExportService.ExportCustomerDrillThroughVariances(GetCurrentUserId(), isBillTo,
                customerId, comparisonType, comparisonStartDate, comparisonEndDate);

            return File(memoryStream, ExcelFile.ExcelContentType, $"{customerName} {DateTime.Now:MM-dd-yyyy}.xlsx");
        }

        [HttpGet]
        public IActionResult ItemCategoryDrillThroughForBillTo(int id, int itemCategoryId, ComparisonType comparisonType,
            DateTime? comparisonStartDate, DateTime? comparisonEndDate)
        {
            TrendVarianceBillToItemCategoryDrillThroughViewModel vm = _trendVarianceModelFactory
                .CreateItemCategoryDrillThroughViewModelForItemCategoryIdAndBillToId(GetCurrentUserId(),
                    id,
                    itemCategoryId,
                    comparisonType,
                    comparisonStartDate,
                    comparisonEndDate,
                    Url);

            return View("ItemCategoryDrillThrough", vm);
        }

        [HttpPost]
        public IActionResult GetItemVariancesForItemCategoryAndBillTo([ModelBinder(typeof(DataTablesBinder))]IDataTablesRequest request,
            TrendVarianceBillToItemCategoryDrillThroughParameters parameters)
        {
            PagedList<TrendVarianceVarianceForItemModel> models = _trendVarianceModelFactory
                .CreateItemVarianceModelsForBillToItemCategoryParameters(GetCurrentUserId(), request, parameters);

            return LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpPost]
        public IActionResult GetItemVariancesForChartForItemCategoryAndBillTo(int itemCategoryId, int billToId)
        {
            return Json(_trendVarianceModelFactory.CreateItemCategoryChartModels(GetCurrentUserId(), itemCategoryId, billToId));
        }

        [HttpGet]
        public IActionResult ItemCategoryDrillThroughForShipTo(int id, int itemCategoryId, ComparisonType comparisonType,
            DateTime? comparisonStartDate, DateTime? comparisonEndDate)
        {
            TrendVarianceShipToItemCategoryDrillThroughViewModel vm = _trendVarianceModelFactory
                .CreateItemCategoryDrillThroughViewModelForItemCategoryIdAndShipToId(GetCurrentUserId(),
                    id, 
                    itemCategoryId,
                    comparisonType,
                    comparisonStartDate, 
                    comparisonEndDate,
                    Url);

            return View("ItemCategoryDrillThrough", vm);
        }

        [HttpPost]
        public IActionResult GetItemVariancesForItemCategoryAndShipTo([ModelBinder(typeof(DataTablesBinder))]IDataTablesRequest request,
            TrendVarianceShipToItemCategoryDrillThroughParameters parameters)
        {
            PagedList<TrendVarianceVarianceForItemModel> models = _trendVarianceModelFactory
                .CreateItemVarianceModelsForShipToItemCategoryParameters(GetCurrentUserId(), request, parameters);

            return LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpPost]
        public IActionResult GetItemVariancesForChartForItemCategoryAndShipTo(int itemCategoryId, int shipToId)
        {
            return Json(_trendVarianceModelFactory.CreateItemCategoryChartModels(GetCurrentUserId(), itemCategoryId, shipToId: shipToId));
        }
    }
}
