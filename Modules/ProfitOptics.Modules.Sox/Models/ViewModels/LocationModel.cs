﻿using ProfitOptics.Framework.DataLayer.GroundControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class LocationModel
    {
        public GCLocation GCLocaton { get; set; }
        public GCLocationInfo GCLocationInfo { get; set; }
    }
}
