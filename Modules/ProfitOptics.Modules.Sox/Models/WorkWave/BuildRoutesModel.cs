﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class BuildRoutesModel
	{
		[JsonProperty("from")]
		public string From { get; set; }
		[JsonProperty("to")]
		public string To { get; set; }
	}
}
