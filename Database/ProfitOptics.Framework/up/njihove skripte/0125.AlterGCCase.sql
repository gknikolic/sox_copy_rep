USE [Sox]

ALTER TABLE [Sox].[dbo].[GCCase] DROP COLUMN [QBEstimateId];
ALTER TABLE [Sox].[dbo].[GCCase] ADD [GCInvoiceId] INT NULL;

GO

ALTER TABLE [dbo].[GCCase]  WITH CHECK ADD  CONSTRAINT [FK_GCCase_GCInvoice] FOREIGN KEY([GCInvoiceId])
REFERENCES [dbo].[GCInvoice] ([Id])
GO
ALTER TABLE [dbo].[GCCase] CHECK CONSTRAINT [FK_GCCase_GCInvoice]
GO