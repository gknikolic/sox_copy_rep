﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCEstimateLineConfig : IEntityTypeConfiguration<GCEstimateLine>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCEstimateLine> builder)
        {
            builder.ToTable("GCEstimateLine");
            builder.HasKey(entity => entity.Id);
        }
    }
}
