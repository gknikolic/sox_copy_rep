﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class EstimateLineDetail
    {
        public GeneralRef ItemRef { get; set; }
        public decimal UnitPrice { get; set; }
        public int Qty { get; set; }
        public GeneralValue TaxCodeRef { get; set; }
        public DateTime? ServiceDate { get; set; }
    }
}
