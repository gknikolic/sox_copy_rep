﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.DataBrowser.Helpers;
using ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder;
using ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Enums;
using ProfitOptics.Modules.DataBrowser.Models;
using ProfitOptics.Modules.DataBrowser.Services;

namespace ProfitOptics.Modules.DataBrowser.Factories
{
    public class DataBrowserModelFactory : IDataBrowserModelFactory
    {
        #region Fields

        private readonly IDataBrowserConfigurationService _dataBrowserConfigurationService;
        private readonly IDataBrowserValidatorService _dataBrowserValidator;
        private readonly SelectListItem _emptyListItem = new SelectListItem
        {
            Selected = true,
            Text = string.Empty,
            Value = string.Empty
        };
        #endregion

        #region Ctor

        public DataBrowserModelFactory(IDataBrowserConfigurationService dataBrowserConfigurationService,
                                       IDataBrowserValidatorService dataBrowserValidator)
        {
            this._dataBrowserConfigurationService = dataBrowserConfigurationService;
            this._dataBrowserValidator = dataBrowserValidator;
        }

        #endregion

        #region Methods

        public DataBrowserViewModel PrepareDataBrowserViewModel()
        {
            var databases = new List<SelectListItem>
            {
                new SelectListItem {Selected = true, Text = string.Empty, Value = string.Empty}
            };

            databases.AddRange(_dataBrowserConfigurationService.GetAllowedDatabases().Select(x => new SelectListItem
            {
                Value = x,
                Text = x
            }));

            var model = new DataBrowserViewModel
            {
                Databases = databases
            };

            return model;
        }

        public IEnumerable<TableModel> PrepareTables(string databaseName, string searchQuery)
        {
            IEnumerable<TableModel> tables = GetTables(databaseName);

            if (!string.IsNullOrWhiteSpace(searchQuery))
            {
                tables = tables.Where(x => x.Name.ToLower().Contains(searchQuery.ToLower().Trim()));
            }

            return tables.Distinct().OrderBy(x => x.Name);
        }

        public FilterViewModel PrepareFilterViewModel(string databaseName, string tableName)
        {
            var conditions = new List<SelectListItem>
            {
                _emptyListItem,
                new SelectListItem {Text = "=", Value = Comparison.Equals.ToString()},
                new SelectListItem {Text = "!=", Value = Comparison.NotEquals.ToString()},
                new SelectListItem {Text = ">", Value = Comparison.GreaterThan.ToString()},
                new SelectListItem {Text = ">=", Value = Comparison.GreaterOrEquals.ToString()},
                new SelectListItem {Text = "<", Value = Comparison.LessThan.ToString()},
                new SelectListItem {Text = "=<", Value = Comparison.LessOrEquals.ToString()},
                new SelectListItem {Text = "LIKE", Value = Comparison.Like.ToString()},
                new SelectListItem {Text = "NOT LIKE", Value = Comparison.NotLike.ToString()},
                new SelectListItem {Text = "IS NULL", Value = Comparison.IsNull.ToString()},
                new SelectListItem {Text = "IS NOT NULL", Value = Comparison.IsNotNull.ToString()},
            };

            var logicOperators = new List<SelectListItem>
            {
                _emptyListItem,
                new SelectListItem {Text = "AND", Value = LogicOperator.And.ToString()},
                new SelectListItem {Text = "OR", Value = LogicOperator.Or.ToString()}
            };

            using (SqlConnection conn = new SqlConnection(_dataBrowserConfigurationService.GetConnectionString().ConnectionString))
            {
                conn.Open();

                bool valid = _dataBrowserValidator.ValidateDatabaseName(databaseName) &&
                             _dataBrowserValidator.ValidateTableName(databaseName, tableName);

                if (valid)
                {
                    IEnumerable<ColumnModel> columnModels = GetColumnModel(databaseName, tableName, conn);
                    var columns = new List<SelectListItem> { _emptyListItem };
                    columns.AddRange(columnModels.Select(x => new SelectListItem { Text = x.Name, Value = x.Name }));
                    var viewModel = new FilterViewModel
                    {
                        Conditions = conditions,
                        LogicOperators = logicOperators,
                        Columns = columns
                    };

                    return viewModel;
                }
            }

            throw new DataBrowserException();
        }

        public IEnumerable<ColumnModel> PrepareTableDataColumns(string databaseName, string tableName)
        {
            using (SqlConnection conn = new SqlConnection(_dataBrowserConfigurationService.GetConnectionString().ConnectionString))
            {
                conn.Open();

                bool valid = _dataBrowserValidator.ValidateDatabaseName(databaseName) &&
                             _dataBrowserValidator.ValidateTableName(databaseName, tableName);
                if (valid)
                {
                    IEnumerable<ColumnModel> columnModels = GetColumnModel(databaseName, tableName, conn);

                    return columnModels;
                }
            }

            throw new DataBrowserException("Data is not valid");
        }

        public DataBrowserPaginationResult PrepareTableData(IDataTablesRequest request, TableQueryModel tableQueryModel)
        {
            DataBrowserPaginationResult paginationResult = new DataBrowserPaginationResult();

            using (SqlConnection conn = new SqlConnection(_dataBrowserConfigurationService.GetConnectionString().ConnectionString))
            {
                conn.Open();

                conn.ChangeDatabase(FormatDatabaseName(tableQueryModel.DatabaseName));

                bool valid = _dataBrowserValidator.ValidateTableQuery(tableQueryModel);
                if (valid)
                {
                    var queryBuilder = new DataBrowserQueryBuilder(tableQueryModel, true);
                    var tableData = new DataBrowserTableData(conn, queryBuilder);

                    try
                    {
                        paginationResult = tableData.GetPaginationResult();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                return paginationResult;
            }
        }

        public DataBrowserResult PrepareWhereStatement(TableQueryModel tableQueryModel)
        {
            var dataBrowserResult = new DataBrowserResult();

            using (SqlConnection conn = new SqlConnection(_dataBrowserConfigurationService.GetConnectionString().ConnectionString))
            {
                conn.Open();


                bool valid = _dataBrowserValidator.ValidateTableQuery(tableQueryModel);
                if (valid)
                {
                    var queryBuilder = new DataBrowserQueryBuilder(tableQueryModel);

                    string whereStatement = queryBuilder.BuildWhereStatement();

                    dataBrowserResult.WhereStatement = whereStatement;
                }
                else
                {
                    dataBrowserResult.Status = ResultStatus.ERROR.ToString();
                    dataBrowserResult.Message = "Unable to generate WHERE statement. Input data is not valid.";
                }
            }

            return dataBrowserResult;
        }

        public DataBrowserDownloadModel PrepareDataBrowserDownloadModel(TableQueryModel tableQueryModel)
        {
            string id = Guid.NewGuid().ToString().Replace("-", "");

            DataBrowserDownloadModel model = null;

            using (SqlConnection conn = new SqlConnection(_dataBrowserConfigurationService.GetConnectionString().ConnectionString))
            {
                conn.Open();

                conn.ChangeDatabase(FormatDatabaseName(tableQueryModel.DatabaseName));

                bool valid = _dataBrowserValidator.ValidateTableQuery(tableQueryModel);

                if (valid)
                {
                    var queryBuilder = new DataBrowserQueryBuilder(tableQueryModel);
                    var tableData = new DataBrowserTableData(conn, queryBuilder);

                    int count = tableData.GetCount();

                    bool downloadWarning = count > _dataBrowserConfigurationService.GetMinWarningDownloadRecords() &&
                                           count <= _dataBrowserConfigurationService.GetMaxAllowedDownloadRecords();
                    bool tooManyRecords = count > _dataBrowserConfigurationService.GetMaxAllowedDownloadRecords();

                    model = new DataBrowserDownloadModel
                    {
                        Id = id,
                        DownloadWarning = downloadWarning,
                        TooManyRecords = tooManyRecords,
                        Count = count.ToString("N0")
                    };
                }
            }

            return model;
        }

        #endregion

        #region Util

        private IEnumerable<TableModel> GetTables(string databaseName)
        {
            using (SqlConnection conn = new SqlConnection(_dataBrowserConfigurationService.GetConnectionString().ConnectionString))
            {
                conn.Open();

                bool valid = _dataBrowserValidator.ValidateDatabaseName(databaseName);
                if (!valid)
                {
                    throw new DataBrowserException("Invalid request.");
                }

                return _dataBrowserConfigurationService.GetTablesFromDatabase(databaseName);
            }
        }

        private IEnumerable<ColumnModel> GetColumnModel(string databaseName, string tableName, SqlConnection conn)
        {
            var dataBrowserColumns = new DataBrowserColumns(conn, databaseName);
            IEnumerable<DataColumn> dataColumns = dataBrowserColumns.GetColumns(tableName);

            IEnumerable<ColumnModel> result = dataColumns.Select(col => new ColumnModel
            {
                Name = col.ColumnName,
                Type = col.DataType.Name
            });

            return result;
        }

        private string FormatDatabaseName(string databaseName)
        {
            if (databaseName.Contains('[') && databaseName.Contains(']'))
            {
                databaseName = databaseName.Substring(1, databaseName.Length - 2);
            }

            return databaseName;
        }

        #endregion
    }
}
