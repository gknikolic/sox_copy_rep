﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Models.Components;
using ProfitOptics.Modules.Showcase.Factories;

namespace ProfitOptics.Modules.Showcase.Controllers
{
    [Authorize]
    [Area("Showcase")]
    public class HierarchyChartsController : BaseController
    {
        private readonly IShowcaseFactory _showcaseFactory;

        public HierarchyChartsController(IShowcaseFactory showcaseFactory)
        {
            _showcaseFactory = showcaseFactory;
        }

        public IActionResult Index()
        {
            // Generate tree
            var chart = _showcaseFactory.GenerateExampleChartNode();

            var returnModel = new HierarchyChartModel
            {
                Data = chart,
                Button1Text = "Edit",
                Button2Text = "+",
                Button3Text = "Profile",
                OnNodeButton1Callback = "leftNodeButtonClick",
                OnNodeButton2Callback = "middleNodeButtonClick",
                OnNodeButton3Callback = "rightNodeButtonClick"
            };

            return View(returnModel);
        }
    }
}