  USE [Sox]

BEGIN

  ALTER TABLE [Sox].[dbo].[WWRoute] ALTER COLUMN Date DateTime NULL;

    ALTER TABLE [Sox].[dbo].[WWRouteStep] ALTER COLUMN Type NVARCHAR(200) NULL;
  	EXEC sp_rename 'Sox.dbo.GCCase.WWRouteId', 'WWPickupRouteId', 'COLUMN';

	ALTER TABLE [Sox].[dbo].[GCCase] ADD WWDeliveryRouteId INT NULL;	
	
	  ALTER TABLE [dbo].[GCCase]  WITH CHECK ADD  CONSTRAINT [FK_GCCase_WWDeliveryRoute] FOREIGN KEY([WWDeliveryRouteId])
REFERENCES [dbo].[WWRoute] ([Id])
	
END