﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Core.System;
using ProfitOptics.Modules.EmailParser.Models;
using ProfitOptics.Modules.Sox.Services;
using Serilog;

namespace ProfitOptics.Modules.EmailParser.Controllers
{
    [Area("EmailParser")]
	[Route("EmailParser/Callback")]
    public class CallBackController : Controller
    {
        private readonly Common _settings;
        private readonly IReportingService _reportingService;

        public CallBackController(Common settings, IReportingService reportingService )
        {
            _settings = settings;
            _reportingService = reportingService;
        }
        [AllowAnonymous]
        [HttpGet]
		[Route("Test")]
		public IActionResult Test()
		{
            return Ok();
		}

		[AllowAnonymous]
		[HttpPost]
		[Route("ParseBasReport")]
		//[Route("EmailParser/CallBack/ParseBasReport/{key?}")]
		public HttpResponseMessage ParseBasReport([FromQuery] string id)
		{
			try
			{
				var logName = "basreportcallback_";
				Log.Logger = new LoggerConfiguration().
				WriteTo.File(Path.Combine((new IO(null)).GetAssemblyDir(), $"basreportlog/{logName}.log"), rollingInterval: RollingInterval.Day).
				WriteTo.Console().
				CreateLogger();
				Log.Information("[Reporting callback] Callback hit");
				//Check the key
				var secretKey = _settings.EmailParseKey;
				if (secretKey != id)
					return new HttpResponseMessage(HttpStatusCode.Forbidden);

				var inboundEmail = InboundEmailExtractor.ExtractInboundEmail(Request);
				if (inboundEmail != null)
				{
					Log.Information("[Reporting callback] New Email Received from {0}", inboundEmail.From);
					if (inboundEmail.AttachmentFiles != null && inboundEmail.AttachmentFiles.Count > 0)
					{
						Log.Information("[Reporting callback] Number of attachments: {0}", inboundEmail.AttachmentFiles.Count);
						foreach (var file in inboundEmail.AttachmentFiles)
						{
							Log.Information("[Reporting callback] Processing attachment: {0}", file.FileName);
							if (file.FileName.Contains("Report"))
							{
								using (var stream = file.OpenReadStream())
								{
									_reportingService.ParseBasReport(stream);
									Log.Information("[Reporting callback] Processing attachment: {0} FINISHED", file.FileName);
								}
							}
						}
					}
				}
				return new HttpResponseMessage(HttpStatusCode.OK);
			}
			catch(Exception ex)
			{
				var response = new HttpResponseMessage(HttpStatusCode.BadRequest);
				response.ReasonPhrase = ex.Message;
				Log.Error("[Reporting callback] Processing Failed: {0}", ex.Message);
				return response;
			}
			
		}
	}
}
