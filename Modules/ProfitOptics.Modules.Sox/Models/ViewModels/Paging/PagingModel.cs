﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels.Paging
{
	public class PagingModel
	{
		const int maxPageSize = 100;
		public int PageNumber { get; set; } = 1;

		public string SearchValue { get; set; }

		private int _pageSize = 10;
		public int PageSize
		{
			get
			{
				return _pageSize;
			}
			set
			{
				_pageSize = (value > maxPageSize) ? maxPageSize : value;
			}
		}
	}
}
