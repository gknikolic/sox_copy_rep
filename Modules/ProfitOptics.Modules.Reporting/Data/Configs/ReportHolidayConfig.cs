﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportHolidayConfig : IEntityTypeConfiguration<ReportHoliday>
    {
        public void Configure(EntityTypeBuilder<ReportHoliday> builder)
        {
            builder.ToTable(nameof(ReportHoliday));
            builder.HasKey(h => h.Id);

            builder.Property(h => h.Name).HasMaxLength(100).IsRequired();
            builder.Property(h => h.Date).IsRequired();
            builder.Property(h => h.Country).HasMaxLength(50).IsRequired();
        }
    }
}
