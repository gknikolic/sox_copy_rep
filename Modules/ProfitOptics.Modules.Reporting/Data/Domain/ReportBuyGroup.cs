using System;
using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportBuyGroup : IFilterableEntity
    {
        public ReportBuyGroup()
        {
            ReportSalesHistories = new HashSet<ReportSalesHistory>();
        }

        [Select2Id]
        public int Id { get; set; }

        public string Code { get; set; }

        [Select2Text]
        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        
        public virtual ICollection<ReportSalesHistory> ReportSalesHistories { get; set; }
    }
}
