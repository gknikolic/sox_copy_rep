﻿using System;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class DashboardHourPreviewItemModel
	{
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public int NumOfBlueCases { get; set; }
		public int NumOfPurpleCases { get; set; }
		public int NumOfRedCases { get; set; }
		public int NumOfYellowCases { get; set; }
		public int NumOfOrangeCases { get; set; }
		public int NumOfGreenCases { get; set; }
		public int NumOfGrayCases { get; set; }
	}


}
