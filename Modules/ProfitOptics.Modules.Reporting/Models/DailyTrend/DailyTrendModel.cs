﻿using System;

namespace ProfitOptics.Modules.Reporting.Models.DailyTrend
{
    public class DailyTrendModel
    {
        public DateTime InvoiceDate { get; set; }

        public string DateString => InvoiceDate.ToString("MM/dd/yyyy");

        public decimal Sales { get; set; }

        public decimal GrossProfit { get; set; }

        public decimal GrossMarginPercent { get; set; }
    }
}