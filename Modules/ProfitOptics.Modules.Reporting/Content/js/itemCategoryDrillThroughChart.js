﻿let itemCategoryDrillThroughChart = (function () {
    const settings = {
        colors: ['#505262', '#718ada', '#64d774'],
        barSeriesPrefix: "CY",
        lineSeriesPrefix: "GP% CY",
        containerSelector: "",
        dataUrl: ""
    };

    function init(containerSelector, dataUrl) {
        settings.containerSelector = containerSelector;
        settings.dataUrl = dataUrl;
    }

    function getDataAndDrawChart(customerId, customerName, customerNum, itemCategoryId, itemCategoryName, isCorporate) {
        showOverlay();
        
        $.ajax({
            url: settings.dataUrl,
            data: {
                itemCategoryId: itemCategoryId,
                billToId: customerId,
                shipToId: customerId
            },
            type: "POST",
            dataType: "json",
            success: function (data) {
                drawChart(customerName, customerNum, itemCategoryName, data, isCorporate);

                hideOverlay();
            },
            error: function (err) {
                //drawEmptyChart(isAdmin);
                hideOverlay();
            }
        });
    }
    
    function showOverlay() {
        $(settings.containerSelector).parents(".row").find(".table-overlay").show();
    }

    function hideOverlay() {
        $(settings.containerSelector).parents(".row").find(".table-overlay").hide();
    }

    function drawChart(customerName, customerNum, itemCategoryName, dataPoints, isCorporate) {
        Highcharts.chart(settings.containerSelector.replace("#", ""), {
            lang: {
                thousandsSep: ","
            },
            credits: {
                enabled: false
            },
            title: {
                text: getChartTitle(customerName, customerNum, itemCategoryName),
                useHTML: true
            },
            xAxis: {
                categories: constructCategories(dataPoints),
                min: 0,
                crosshair: true
            },
            yAxis: [
                {
                    labels: {
                        format: "${value:,.0f}",
                        formatter: function () {
                            return "$" + Highcharts.numberFormat(this.value, 0, '.', ',');
                        }
                    },
                    title: {
                        text: "Sales"
                    }
                },
                {
                    labels: {
                        format: '{value} %'
                    },
                    min: 0,
                    title: {
                        text: 'GP%'
                    },
                    opposite: true
                }
            ],
            series: constructSeries(dataPoints)
        });
    }

    function getChartTitle(customerName, customerNum, itemCategoryName) {
        return customerName + " (" + customerNum + ") - " + itemCategoryName;
    }

    function constructCategories(dataPoints) {
        var categories = [];

        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        $.each(dataPoints, function (_, val) {
            const year = String(val.YearMonth).substring(2, 4);

            const month = String(val.YearMonth).substring(4, 6);

            categories.push(monthNames[parseInt(month) - 1] + " " + year);
        });

        return categories;
    }

    function constructSeries(dataPoints) {
        const series = [];

        series.push(constructBarSeries(dataPoints));

        series.push(constructLineSeries(dataPoints));

        return series;
    }

    function constructBarSeries(dataPoints) {
        var data = [];

        $.each(dataPoints, function(_, val) {
            data.push(val.MonthlySales);
        });

        return {
            type: 'column',
            yAxis: 0,
            name: 'Sales',
            tooltip: {
                pointFormatter: function () {
                    return this.series.name + ': ' + "$" + Highcharts.numberFormat(this.y, 0, '.', ',');
                }
            },
            color: settings.colors[1],
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            data: data
        };
    }

    function constructLineSeries(dataPoints) {
        var data = [];

        $.each(dataPoints, function(_, val) {
            data.push(parseFloat(parseFloat(val.MonthlyGrossProfitPercent * 100.0).toFixed(1)));
        });

        return {
            type: 'line',
            yAxis: 1,
            name: 'GP%',
            tooltip: {
                valueSuffix: '%',
                shared: false
            },
            color: settings.colors[2],
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            data: data
        };
    }

    return {
        init: init,
        render: getDataAndDrawChart
    };
})();