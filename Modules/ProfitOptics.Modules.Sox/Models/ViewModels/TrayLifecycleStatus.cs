﻿using System;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayLifecycleStatus
	{
        public string Title { get; set; }
        public DateTime Timestamp { get; set; }
		public bool IsFinished { get; set; }
		public string User { get; set; }
	}
}
