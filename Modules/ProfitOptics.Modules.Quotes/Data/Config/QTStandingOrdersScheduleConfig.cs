﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTStandingOrdersScheduleConfig : IEntityTypeConfiguration<QTStandingOrdersSchedule>
    {
        public void Configure(EntityTypeBuilder<QTStandingOrdersSchedule> builder)
        {
            builder.ToTable("QTStandingOrdersSchedule");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Company).HasMaxLength(50);
            builder.Property(entity => entity.CustID).HasMaxLength(50);
            builder.Property(entity => entity.CustName).HasMaxLength(50);
            builder.Property(entity => entity.PartNum).HasMaxLength(50);
            builder.Property(entity => entity.StandingOrderCode).HasMaxLength(50);
            builder.Property(entity => entity.OrderPattern).HasMaxLength(50);
            builder.Property(entity => entity.OrderWeek).HasMaxLength(50);
            builder.Property(entity => entity.ShipToName).HasMaxLength(50);
            builder.Property(entity => entity.ShipToNum).HasMaxLength(50);
            builder.Property(entity => entity.PONumber).HasMaxLength(50);

        }
    }
}
