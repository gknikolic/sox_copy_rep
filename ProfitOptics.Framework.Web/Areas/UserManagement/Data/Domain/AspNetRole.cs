﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Data.Domain
{
    public class AspNetRole
    {
        public AspNetRole()
        {
            AspNetUserRoles = new HashSet<AspNetUserRoles>();
        }

        public int Id { get; set; }

        public string Name { get; set; }        

        public virtual ICollection<AspNetUserRoles> AspNetUserRoles { get; set; }
    }
}
