namespace ProfitOptics.Framework.Web.RestApi.Resources
{
    public class QueryResource
    {
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public string FilterColumn { get; set; }
        public string FilterValue { get; set; }
    }
}