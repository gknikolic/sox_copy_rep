﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueTransitions
    {
        public string id { get; set; }

        public string name { get; set; }

        public IssueTransitionsTo to { get; set; }

        public bool hasScreen { get; set; }

        public bool isGlobal { get; set; }

        public bool isInitial { get; set; }

        public bool isConditional { get; set; }
    }
}