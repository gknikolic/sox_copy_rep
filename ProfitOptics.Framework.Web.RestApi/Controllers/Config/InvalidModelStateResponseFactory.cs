using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.RestApi.Resources;
using ProfitOptics.Framework.Web.RestApiExtensions;

namespace ProfitOptics.Framework.Web.RestApiControllers.Config
{
    public static class InvalidModelStateResponseFactory
    {
        public static IActionResult ProduceErrorResponse(ActionContext context)
        {
            var errors = context.ModelState.GetErrorMessages();
            var response = new ErrorResource(messages: errors);
            
            return new BadRequestObjectResult(response);
        }
    }
}