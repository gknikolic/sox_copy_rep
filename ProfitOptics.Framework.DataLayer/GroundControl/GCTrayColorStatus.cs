﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCTrayColorStatus
    {
        public int Id { get; set; }
        public int GCColorStatusTypeId { get; set; }
        public int GCTrayId { get; set; }
        public DateTime Timestamp { get; set; }
        public bool IsActiveStatus { get; set; }
        public bool? IsLocked { get; set; }

        public virtual GCColorStatusType GCColorStatusType { get; set; }
        public virtual GCTray GCTray { get; set; }
    }
}
