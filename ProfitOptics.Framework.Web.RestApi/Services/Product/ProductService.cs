using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.Web.RestApi.DataLayer;
using ProfitOptics.Framework.Web.RestApi.Models;
using ProfitOptics.Framework.Web.RestApi.Models.Product;
using ProfitOptics.Framework.Web.RestApiExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.Services.Product
{
    public class ProductService : IProductService
    {
        private readonly ApiEntitiesContext _context;

        public ProductService(ApiEntitiesContext context)
        {
            _context = context;
        }

        public async Task<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Product>> ListAsync(ProductQuery query)
        {
            IQueryable<ProfitOptics.Framework.Web.RestApi.DataLayer.Product> queryable = _context.Products
                                                .Include(p => p.DistributorProducts)
                                                .Include(p => p.CustomerProducts)
                                                .AsNoTracking();

            if (query.CustomerId.HasValue && query.CustomerId > 0)
            {
                queryable = queryable.Where(p => p.CustomerProducts.Any(cp => cp.CustomerId == query.CustomerId.Value));
            }

            if (query.DistributorId.HasValue && query.DistributorId > 0)
            {
                queryable = queryable.Where(p => p.DistributorProducts.Any(cp => cp.DistributorId == query.DistributorId.Value));
            }

            int totalItems = await queryable.CountAsync();

            List<ProfitOptics.Framework.Web.RestApi.DataLayer.Product> products = await queryable
                .ApplyFiltering(query)
                .ApplySort(query)
                .ApplyPagination(query)
                .ToListAsync();

            return new QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Product>
            {
                Items = products,
                TotalItems = totalItems,
            };
        }

        public async Task<List<ProfitOptics.Framework.Web.RestApi.DataLayer.Product>> ListAsyncByIds(List<int> ids)
        {
            IQueryable<ProfitOptics.Framework.Web.RestApi.DataLayer.Product> queryable = _context.Products
                                                .Where(d => ids.Contains(d.Id))
                                                .AsNoTracking();

            List<ProfitOptics.Framework.Web.RestApi.DataLayer.Product> products = await queryable.ToListAsync();

            return products;
        }

        public async Task<ProductResponse> SaveAsync(ProfitOptics.Framework.Web.RestApi.DataLayer.Product product)
        {
            try
            {
                /*
                 Notice here we have to check if the distributor ID is valid before adding the product, to avoid errors.
                */
                if (product.DistributorProducts.Count > 0)
                {
                    var distributorIds = product.DistributorProducts.Select(dp => dp.DistributorId).Distinct().ToList();

                    var existingDistributors = _context.Customers
                                                .Where(d => distributorIds.Contains(d.Id))
                                                .AsNoTracking();

                    if (existingDistributors == null)
                        return new ProductResponse("Invalid distributors.");

                    if (existingDistributors.Count() != distributorIds.Count())
                        return new ProductResponse("Could not find all the distributors.");
                }

                if (product.CustomerProducts.Count > 0)
                {
                    var customerIds = product.CustomerProducts.Select(cp => cp.CustomerId).Distinct().ToList();

                    var existingCustomers = _context.Customers
                                                .Where(d => customerIds.Contains(d.Id))
                                                .AsNoTracking();

                    if (existingCustomers == null)
                        return new ProductResponse("Invalid customers.");

                    if (existingCustomers.Count() != customerIds.Count())
                        return new ProductResponse("Could not find all the customers.");
                }

                await _context.Products.AddAsync(product);
                await _context.SaveChangesAsync();

                return new ProductResponse(product);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new ProductResponse($"An error occurred when saving the product: {ex.Message}");
            }
        }

        public async Task<ProductResponse> UpdateAsync(int id, ProfitOptics.Framework.Web.RestApi.DataLayer.Product product)
        {
            var existingProduct = await _context.Products.FindAsync(id);

            if (existingProduct == null)
                return new ProductResponse("Product not found.");

            if (product.DistributorProducts.Count > 0)
            {
                var distributorIds = product.DistributorProducts.Select(dp => dp.DistributorId).Distinct().ToList();

                var existingDistributors = _context.Customers
                                            .Where(d => distributorIds.Contains(d.Id))
                                            .AsNoTracking();

                if (existingDistributors == null)
                    return new ProductResponse("Invalid distributors.");

                if (existingDistributors.Count() != distributorIds.Count())
                    return new ProductResponse("Could not find all the distributors.");
            }

            if (product.CustomerProducts.Count > 0)
            {
                var customerIds = product.CustomerProducts.Select(cp => cp.CustomerId).Distinct().ToList();

                var existingCustomers = _context.Customers
                                            .Where(d => customerIds.Contains(d.Id))
                                            .AsNoTracking();

                if (existingCustomers == null)
                    return new ProductResponse("Invalid customers.");

                if (existingCustomers.Count() != customerIds.Count())
                    return new ProductResponse("Could not find all the customers.");
            }

            existingProduct.Name = product.Name;
            existingProduct.UnitOfMeasurement = product.UnitOfMeasurement;
            existingProduct.QuantityInPackage = product.QuantityInPackage;

            try
            {
                _context.Products.Update(existingProduct);
                await _context.SaveChangesAsync();

                return new ProductResponse(existingProduct);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new ProductResponse($"An error occurred when updating the product: {ex.Message}");
            }
        }

        public async Task<ProductResponse> DeleteAsync(int id)
        {
            var existingProduct = await _context.Products.FindAsync(id);

            if (existingProduct == null)
                return new ProductResponse("Product not found.");

            try
            {
                _context.Products.Remove(existingProduct);
                await _context.SaveChangesAsync();

                return new ProductResponse(existingProduct);
            }
            catch (Exception ex)
            {
                // Do some logging stuff
                return new ProductResponse($"An error occurred when deleting the product: {ex.Message}");
            }
        }
    }
}