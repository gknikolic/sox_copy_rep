﻿$(document).ready(function () {
    $('#gcInvoices').DataTable();
    $('.select2-customers').select2();

    $('#customerList').on('change', function () {
        window.location.href = location.origin + location.pathname + '?id=' + $(this).val();
    });
});

function PullTraysOrCasesInvoiceInfo(customerId) {
    $.blockUI();

    $.ajax({
        type: "POST",
        url: "../Sox/Accountings/PullTraysOrCasesInvoiceInfo",
        data: {
            customerId: customerId, month: $('#lastThreeMonthsList :selected').val() },
        dataType: "json",
        success: function (response) {
            if (response.success) {
                app.showAlert(response.message);
                setTimeout(function () {
                    location.reload();
                }, 2500);
            } else {
                $.unblockUI();
                app.showAlert({
                    title: "Error",
                    message: response.message
                });
            }
        },
        error: function (response) {
            var error = 400;
            $.unblockUI();
        }
    });
} 

function PushInvoiceDataToQB(customerId) {
    $.blockUI();
    $.ajax({
        type: "POST",
        url: "../Sox/Accountings/PushInvoiceDataToQB",
        data: { customerId: customerId },
        dataType: "json",
        success: function (response) {
            if (response.success) {
                app.showAlert(response.message);
                setTimeout(function () {
                    location.reload();
                }, 2500);
            } else {
                $.unblockUI();
                app.showAlert({
                    title: "Error",
                    message: response.message
                });
            }
        },
        error: function (response) {
            var error = 400;
            $.unblockUI();
        }
    });
}

function RemoveGCInvoiceLine(gcInvoiceLineId) {
    $.ajax({
        type: "POST",
        url: "../Sox/Accountings/RemoveGCInvoiceLine",
        data: { gcInvoiceLineId: gcInvoiceLineId },
        dataType: "json",
        success: function (response) {
            app.showAlert(response.message);
            setTimeout(function () {
                location.reload();
            }, 2500);
        },
        error: function (response) {
            var error = 400;
        }
    });
} 