﻿using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Modules.CrossReference.Data.Config;
using ProfitOptics.Modules.CrossReference.Data.Domain;

namespace ProfitOptics.Modules.CrossReference.Data
{
    public class Entities : DbContext
    {
        public Entities(DbContextOptions<Entities> options) : base(options)
        {
            var listener = this.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }

        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<CrtCompatibilityOption> CrtCompatibilityOptions { get; set; }
        public virtual DbSet<CrtCompetitor> CrtCompetitors { get; set; }
        public virtual DbSet<CrtCrossReferenceChangeHistory> CrtCrossReferenceChangeHistories { get; set; }
        public virtual DbSet<CrtCrossReferenceRecord> CrtCrossReferenceRecords { get; set; }
        public virtual DbSet<CrtCrossReferenceVote> CrtCrossReferenceVotes { get; set; }
        public virtual DbSet<QTItem> QTItems { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new AspNetUserConfig());
            builder.ApplyConfiguration(new CrtCompatibilityOptionConfig());
            builder.ApplyConfiguration(new CrtCompetitorConfig());
            builder.ApplyConfiguration(new CrtCrossReferenceChangeHistoryConfig());
            builder.ApplyConfiguration(new CrtCrossReferenceRecordConfig());
            builder.ApplyConfiguration(new CrtCrossReferenceVoteConfig());
            builder.ApplyConfiguration(new QTItemConfig());

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}
