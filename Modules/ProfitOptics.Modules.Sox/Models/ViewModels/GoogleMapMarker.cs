﻿namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class GoogleMapMarker
	{
		public string Title { get; set; }
		public float Lat { get; set; }
		public float Long { get; set; }
	}


}
