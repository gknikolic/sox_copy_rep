﻿using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class MarginBridgeFilterModel
    {
        public int UserId { get; set; }

        public ProductLevelType ProductSelectionLevel { get; set; }

        public LocationLevelType LocationSelectionLevel { get; set; }

        public MarginBridgeType MarginBridgeType { get; set; }

        public ReportFilterModel ReportFitlerModel { get; set; }
    }
}
