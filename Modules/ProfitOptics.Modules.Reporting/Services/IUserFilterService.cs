﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Services
{
    public interface IUserFilterService
    {
        /// <summary>
        /// Gets the user filter with the specified identifier.
        /// </summary>
        /// <param name="userFilterId"></param>
        /// <returns></returns>
        ReportUserFilter GetUserFilterById(int userFilterId);
        
        /// <summary>
        /// Gets the default filter for the specified user identifier.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ReportUserFilter GetDefaultReportFilterForUserId(int userId);

        /// <summary>
        /// Gets all of the report filters for the specified user identifier.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="loadDefaultFilter"></param>
        /// <returns></returns>
        IEnumerable<ReportUserFilter> GetAllReportFiltersForUserId(int userId, bool loadDefaultFilter = false);

        /// <summary>
        /// Adds or updates the specified user filter.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userFilter"></param>
        /// <param name="filterId"></param>
        /// <returns></returns>
        bool AddOrUpdateReportFilter(int userId, SaveReportFilterModel userFilter, out int filterId);

        /// <summary>
        /// Deletes the user filter with the specified identifier.
        /// </summary>
        /// <param name="userFilterId"></param>
        bool DeleteUserFilter(int userFilterId);

        /// <summary>
        /// Renames the specified filters.
        /// </summary>
        /// <param name="filtersToRename"></param>
        /// <returns></returns>
        bool BulkRenamedUserFilters(IList<RenameFilterModel> filtersToRename);
    }
}
