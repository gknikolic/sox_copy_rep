﻿using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;

namespace ProfitOptics.Modules.Reporting.Services.RsmPerformance
{
    public interface IRsmMissingRevenueService
    {
        SalesChannelQuotaPerformanceDataModel GetSalesChannelQuotaPerformanceModelForSalesRepId(string salesChannelName,
            MonthDetails monthDetails,
            int salesRepId = 0,
            int year = 0,
            int month = 0);

        ProductFamilyQuotaPerformanceDataModel GetProductFamilyQuotaPerformanceModelForSalesRepId(string productFamilyName,
            MonthDetails monthDetails,
            int salesRepId = 0,
            int year = 0,
            int month = 0);

        ItemCategoryQuotaPerformanceDataModel GetItemCategoryQuotaPerformanceModelForSalesRepId(string itemCategoryName,
            MonthDetails monthDetails,
            int salesRepId = 0,
            int year = 0,
            int month = 0);

        SalesRepRegionQuotaPerformanceDataModel GetSalesRepGetRegionQuotaPerformanceDataModelForSalesRepName(string salesRepName,
            MonthDetails monthDetails,
            int year = 0,
            int month = 0,
            int currentQuarter = 0);
    }
}