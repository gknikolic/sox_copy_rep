﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System;
using ProfitOptics.Framework.DataLayer.GroundControl;
using System.Linq;
using ProfitOptics.Modules.Sox.Models;
using System.Collections.Generic;
using TimeZoneConverter;
using System.Diagnostics;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Enums;

namespace ProfitOptics.Modules.Sox.Services
{
	public class ReportingService : IReportingService
	{
		private readonly Framework.DataLayer.Entities _context;

		public ReportingService(Framework.DataLayer.Entities context)
		{
			_context = context;
		}

		public void ParseBasReport(Stream stream)
		{
			ETLLogs etlLog = new ETLLogs();
			etlLog.Source = ProfitOptics.Modules.Sox.Helpers.Constants.REPORTING_MODULE;
			etlLog.FileName = "no file";
			etlLog.Username = "no username";
			etlLog.Status = ETLStatusType.OK.ToString();
			etlLog.Message = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE;
			etlLog.RowCount = 0;
			etlLog.ActionType = (int)ReportParsing.ParseBASReportFromEmail;
			etlLog.Description = "Parsing email from BAS Report";
			var stopwatch = new Stopwatch();
			stopwatch.Start();
			int rowCount = 0;
			try
			{
				
				string currentLine;
				bool ReadHeaders = true;
				int timestampIndex = 0;
				BASReportLineConfig deconConfig = new BASReportLineConfig();
				BASReportLineConfig sterileConfig = new BASReportLineConfig();
				BASReportLineConfig prepAdnPackConfig = new BASReportLineConfig();
				var deconLocation = _context.GCLocations.Where(x => x.Code == (int)GCLocationType.DeconRoom).FirstOrDefault();
				if (deconLocation != null)
				{
					deconConfig.Location = deconLocation;
				}
				var sterileLocation = _context.GCLocations.Where(x => x.Code == (int)GCLocationType.SterileRoom).FirstOrDefault();
				if (sterileLocation != null)
				{
					sterileConfig.Location = sterileLocation;
				}
				var prepLocation = _context.GCLocations.Where(x => x.Code == (int)GCLocationType.PrepAndPackRoom).FirstOrDefault();
				if (prepLocation != null)
				{
					prepAdnPackConfig.Location = prepLocation;
				}
				using (StreamReader sr = new StreamReader(stream))
				{
					while ((currentLine = sr.ReadLine()) != null)
					{
						rowCount++;
						if (ReadHeaders)
						{
							ReadHeaders = false;
							var headers = new List<string>();
							if (currentLine.Contains(","))
							{
								headers = currentLine.Split(',').ToList();
							}
							else if (currentLine.Contains('\t'))
							{
								string sep = "\t";
								headers = currentLine.Split(sep.ToCharArray()).ToList();
							}
							for (int i = 0; i < headers.Count; i++)
							{
								if (headers[i].Contains("timestamp"))
								{
									timestampIndex = i;
								}
								else if (headers[i].Contains("Decon"))
								{
									if (headers[i].Contains("ACPH"))
									{
										deconConfig.ACPHIndex = i;
									}
									else if (headers[i].Contains("Hum"))
									{
										deconConfig.HumidityIndex = i;
									}
									else if (headers[i].Contains("Temp"))
									{
										deconConfig.TempIndex = i;
									}
									else if (headers[i].Contains("Press"))
									{
										deconConfig.PressureIndex = i;
									}
								}
								else if (headers[i].Contains("Prep"))
								{
									if (headers[i].Contains("ACPH"))
									{
										prepAdnPackConfig.ACPHIndex = i;
									}
									else if (headers[i].Contains("Hum"))
									{
										prepAdnPackConfig.HumidityIndex = i;
									}
									else if (headers[i].Contains("Temp"))
									{
										prepAdnPackConfig.TempIndex = i;
									}
									else if (headers[i].Contains("Press"))
									{
										prepAdnPackConfig.PressureIndex = i;
									}
								}
								else if (headers[i].Contains("Sterile"))
								{
									if (headers[i].Contains("ACPH"))
									{
										sterileConfig.ACPHIndex = i;
									}
									else if (headers[i].Contains("Hum"))
									{
										sterileConfig.HumidityIndex = i;
									}
									else if (headers[i].Contains("Temp"))
									{
										sterileConfig.TempIndex = i;
									}
									else if (headers[i].Contains("Press"))
									{
										sterileConfig.PressureIndex = i;
									}
								}
							}
						}
						else
						{
							var values = new List<string>();
							if (currentLine.Contains(",")) //if CSV file
							{
								values = currentLine.Split(',').ToList();
							}
							else if (currentLine.Contains('\t')) //if tab separated file
							{
								string sep = "\t";
								values = currentLine.Split(sep.ToCharArray()).ToList();
							}
							var timestampVal = values[timestampIndex];
							var convertedTimestamp = new DateTime();
							if (timestampVal.Contains("CST"))
							{
								timestampVal = timestampVal.Replace("CST", "");
								convertedTimestamp = DateTime.Parse(timestampVal).AddHours(6);// get date in UTC
							}
							else if (timestampVal.Contains("CDT"))
							{
								timestampVal = timestampVal.Replace("CDT", "");
								convertedTimestamp = DateTime.Parse(timestampVal).AddHours(5); // get date in UTC
							}
							else if (timestampVal.Contains("CET"))
							{
								timestampVal = timestampVal.Replace("CET", "");
								convertedTimestamp = DateTime.Parse(timestampVal).AddHours(-1); //get date in UTC
							}
							CreateOrUpdateLocationInfo(values, convertedTimestamp, deconConfig);

							CreateOrUpdateLocationInfo(values, convertedTimestamp, prepAdnPackConfig);

							CreateOrUpdateLocationInfo(values, convertedTimestamp, sterileConfig);
						}
					}
				}
			}
			catch (Exception ex)
			{
				etlLog.Status = ETLStatusType.Error.ToString();
				etlLog.Message = ex.Message;
				etlLog.Description = ex.StackTrace;
			}
			finally
			{
				stopwatch.Stop();
				etlLog.Description += $" | Time elapsed: { stopwatch.Elapsed }";
				etlLog.ProcessingTime = DateTime.UtcNow;
				etlLog.RowCount = rowCount;
				_context.ETLLogs.Add(etlLog);
				_context.SaveChanges();
			}
		}

		public void CreateOrUpdateLocationInfo(List<string> values, DateTime timestamp, BASReportLineConfig config)
		{
			var locationInfo = _context.GCLocationInfos.Where(x => x.Timestamp == timestamp && x.GCLocationId == config.Location.Id).FirstOrDefault();
			if (locationInfo == null) //if locationInfo for this timestamp doesnt exist in db, create new record
			{
				GCLocationInfo model = new GCLocationInfo()
				{
					Timestamp = timestamp,
					GCLocationId = config.Location.Id,
					ACPH = float.Parse(values[config.ACPHIndex]),
					Pressure = float.Parse(values[config.PressureIndex]),
					Temperature = float.Parse(values[config.TempIndex]),
					Humidity = float.Parse(values[config.HumidityIndex]),
				};
				_context.GCLocationInfos.Add(model);
				_context.SaveChanges();
			}
			else
			{
				if (locationInfo.Temperature == null)
				{
					locationInfo.Temperature = float.Parse(values[config.TempIndex]);
				}

				if (locationInfo.Pressure == null)
				{
					locationInfo.Pressure = float.Parse(values[config.PressureIndex]);
				}

				if (locationInfo.Humidity == null)
				{
					locationInfo.Humidity = float.Parse(values[config.HumidityIndex]);
				}

				if (locationInfo.ACPH == null)
				{
					locationInfo.ACPH = float.Parse(values[config.ACPHIndex]);
				}

				_context.GCLocationInfos.Update(locationInfo);
				_context.SaveChanges();
			}

		}

		public void UploadLocationInfoData(IFormFile file)
		{
			//ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

			using (StreamReader sr = new StreamReader(file.OpenReadStream()))
			{
				//find for which location is this file
				GCLocation location = null;
				if (file.FileName.Contains("Decon"))
				{
					location = _context.GCLocations.Where(x => x.Code == (int)GCLocationType.DeconRoom).FirstOrDefault();
				}
				else if (file.FileName.Contains("Prep"))
				{
					location = _context.GCLocations.Where(x => x.Code == (int)GCLocationType.PrepAndPackRoom).FirstOrDefault();
				}
				else if (file.FileName.Contains("Sterile"))
				{
					location = _context.GCLocations.Where(x => x.Code == (int)GCLocationType.SterileRoom).FirstOrDefault();
				}
				if (location == null) //if location is null, that means that we do not reconize this report
				{
					return;
				}
				string currentLine;
				int timestampIndex = 0;
				int humidityIndex = 0;
				int tempIndex = 0;
				int ACPHIndex = 0;
				int pressureIndex = 0;
				bool ReadHeaders = true;
				var model = new GCLocationInfo();
				bool notFirstDate = false; //for first row with data (because it does not have previous)
				while ((currentLine = sr.ReadLine()) != null)
				{
					if (ReadHeaders)
					{
						ReadHeaders = false;
						var headers = new List<string>();
						if (currentLine.Contains(","))
						{
							headers = currentLine.Split(',').ToList();
						}
						else if (currentLine.Contains('\t'))
						{
							string sep = "\t";
							headers = currentLine.Split(sep.ToCharArray()).ToList();
						}
						for (int i = 0; i < headers.Count; i++)
						{
							if (headers[i].Contains("Timestamp"))
							{
								timestampIndex = i;
							}
							else if (headers[i].Contains("ACPH"))
							{
								ACPHIndex = i;
							}
							else if (headers[i].Contains("Humidity"))
							{
								humidityIndex = i;
							}
							else if (headers[i].Contains("Temperature"))
							{
								tempIndex = i;
							}
							else if (headers[i].Contains("Pressure"))
							{
								pressureIndex = i;
							}
						}
					}
					else
					{
						var values = new List<string>();
						if (currentLine.Contains(",")) //if CSV file
						{
							values = currentLine.Split(',').ToList();
						}
						else if (currentLine.Contains('\t')) //if tab separated file
						{
							string sep = "\t";
							values = currentLine.Split(sep.ToCharArray()).ToList();
						}
						for (int i = 0; i < values.Count; i++)
						{
							if (values[i] != String.Empty)
							{
								if (i == timestampIndex)
								{
									var timestampVal = values[i];
									var date = new DateTime();
									if (timestampVal.Contains("CST"))
									{
										timestampVal = timestampVal.Replace("CST", "");
										date = DateTime.Parse(timestampVal).AddHours(6);// get date in UTC
									}
									else if (timestampVal.Contains("CDT"))
									{
										timestampVal = timestampVal.Replace("CDT", "");
										date = DateTime.Parse(timestampVal).AddHours(5); // get date in UTC
									}
									else if (timestampVal.Contains("CET"))
									{
										timestampVal = timestampVal.Replace("CET", "");
										date = DateTime.Parse(timestampVal).AddHours(-1); //get date in UTC
									}
									if (date != model.Timestamp && notFirstDate)
									{
										var dbLocationInfo = _context.GCLocationInfos.Where(x => x.Timestamp == model.Timestamp && x.GCLocationId == location.Id).FirstOrDefault();
										if (dbLocationInfo == null) //if this timestamp doesnt exist in db, create new record
										{
											model.GCLocationId = location.Id;
											_context.GCLocationInfos.Add(model);
											_context.SaveChanges();
										}
										else //if it exists, update where values are null so not to override existing values
										{
											if (dbLocationInfo.Temperature == null)
											{
												dbLocationInfo.Temperature = model.Temperature;
											}

											if (dbLocationInfo.Pressure == null)
											{
												dbLocationInfo.Pressure = model.Pressure;
											}

											if (dbLocationInfo.Humidity == null)
											{
												dbLocationInfo.Humidity = model.Humidity;
											}

											if (dbLocationInfo.ACPH == null)
											{
												dbLocationInfo.ACPH = model.ACPH;
											}

											_context.GCLocationInfos.Update(dbLocationInfo);
											_context.SaveChanges();
										}
										model = new GCLocationInfo(); //clear model

									}
									notFirstDate = true;
									model.Timestamp = date;
								}
								else if (i == ACPHIndex)
								{
									model.ACPH = float.Parse(values[i]);
								}
								else if (i == tempIndex)
								{
									model.Temperature = float.Parse(values[i]);
								}
								else if (i == humidityIndex)
								{
									model.Humidity = float.Parse(values[i]);
								}
								else if (i == pressureIndex)
								{
									model.Pressure = float.Parse(values[i]);
								}
							}

						}


					}
				}
				//last one wont be saved in while loop so this is just for last timestamp
				var dbLocationInfoForLast = _context.GCLocationInfos.Where(x => x.Timestamp == model.Timestamp && x.GCLocationId == location.Id).FirstOrDefault();
				if (dbLocationInfoForLast == null) //if this timestamp doesnt exist in db, create new record
				{
					model.GCLocationId = location.Id;
					_context.GCLocationInfos.Add(model);
					_context.SaveChanges();
				}
				else //if it exists, update it
				{
					if (dbLocationInfoForLast.Temperature == null)
					{
						dbLocationInfoForLast.Temperature = model.Temperature;
					}

					if (dbLocationInfoForLast.Pressure == null)
					{
						dbLocationInfoForLast.Pressure = model.Pressure;
					}

					if (dbLocationInfoForLast.Humidity == null)
					{
						dbLocationInfoForLast.Humidity = model.Humidity;
					}

					if (dbLocationInfoForLast.ACPH == null)
					{
						dbLocationInfoForLast.ACPH = model.ACPH;
					}
					_context.GCLocationInfos.Update(dbLocationInfoForLast);
					_context.SaveChanges();
				}
			}
		}
	}
}
