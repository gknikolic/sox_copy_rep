using AutoMapper;
using ProfitOptics.Framework.Web.RestApi.Models;
using ProfitOptics.Framework.Web.RestApi.Models.Auth;
using ProfitOptics.Framework.Web.RestApi.Resources;
using ProfitOptics.Framework.Web.RestApi.Resources.Auth;
using ProfitOptics.Framework.Web.RestApi.Resources.Customer;
using ProfitOptics.Framework.Web.RestApi.Resources.Distributor;
using ProfitOptics.Framework.Web.RestApi.Resources.Product;
using ProfitOptics.Framework.Web.RestApiExtensions;

namespace ProfitOptics.Framework.Web.RestApi.Mapping
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            // Auth
            CreateMap<AccessToken, AccessTokenResource>()
                .ForMember(a => a.AccessToken, opt => opt.MapFrom(a => a.Token))
                .ForMember(a => a.RefreshToken, opt => opt.MapFrom(a => a.RefreshToken.Token))
                .ForMember(a => a.Expiration, opt => opt.MapFrom(a => a.Expiration));

            // Distributor
            CreateMap<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor, DistributorResource>();
            CreateMap<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor>, QueryResultResource<DistributorResource>>();

            // Customer
            CreateMap<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer, CustomerResource>();
            CreateMap<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer>, QueryResultResource<CustomerResource>>();

            // Product
            CreateMap<ProfitOptics.Framework.Web.RestApi.DataLayer.Product, ProductResource>()
                .ForMember(src => src.UnitOfMeasurement,
                           opt => opt.MapFrom(src => src.UnitOfMeasurement.ToDescriptionString()));
            CreateMap<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Product>, QueryResultResource<ProductResource>>();
        }
    }
}