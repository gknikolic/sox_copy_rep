﻿using Flurl.Http;
using Intuit.Ipp.OAuth2PlatformClient;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using OfficeOpenXml.FormulaParsing.LexicalAnalysis;
using ProfitOptics.Framework.Core.Flurl;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Helpers;
using ProfitOptics.Modules.Sox.Models.QuickBooks;
using RestSharp;
using RestSharp.Extensions;
using Serilog;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Infrastructure.Clients
{
    public class QuickBooksClient : IQuickBooksClient
    {
        #region Fields

        private const string CUSTOMER_READ_ALL = "Select * from Customer";
        private const string ITEMS_READ_ALL = "Select * from Item";
        private const string INVOICE_READ_ALL = "Select * from Invoice";

        private readonly ProfitOptics.Framework.Core.Settings.QuickBooksOptions _quickBooksOptions;

        #endregion Fields

        #region Constructor

        public QuickBooksClient(ProfitOptics.Framework.Core.Settings.QuickBooksOptions quickBooksOptions)
        {
            _quickBooksOptions = quickBooksOptions;
        }

        #endregion Constructor

        #region Methods

        #region Helper Methods

        private async Task<T> GetAsync<T>(string partialUrl, string token, string body = null)
        {
            if(token == null)
            {
                return default;
            }

            T result;

            result = await _quickBooksOptions.GetUrl(partialUrl)
            .WithOAuthBearerToken(token)
            .WithHeader("Accept", "application/json")
            .WithHeader("application/text", body)
            .GetAsync()
            .ReceiveJson<T>();
            
            return result;
        }

        private async Task<T> PostAsync<T>(string partialUrl, string token, object body, RequestContentType requestContentType) where T : new()
        {
            if (token == null)
            {
                return default;
            }

            switch (requestContentType)
            {
                case RequestContentType.Json:
                    //return await _quickBooksOptions.GetUrl(partialUrl)
                    //.WithOAuthBearerToken(token)
                    //.WithHeader("Accept", "application/json")
                    //.WithHeader("Content-Type", "application/json")
                    //.PostJsonAsync(body)
                    //.ReceiveJson<T>();

                    var url = _quickBooksOptions.GetUrl(partialUrl);
                    var client = new RestClient(url);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Authorization", "Bearer " + token);
                    request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);
                    IRestResponse response = client.Execute<T>(request);
                    Console.WriteLine(response.Content);
                    return JsonConvert.DeserializeObject<T>(response.Content);
                case RequestContentType.Text:   
                default:
                    var content = new StringContent(body as string, Encoding.UTF8, "application/text");

                    //var resultString = await _quickBooksOptions.GetUrl(partialUrl)
                    //    .WithOAuthBearerToken(token)
                    //    .WithHeader("Accept", "application/json")
                    //    .PostAsync(content)
                    //    .ReceiveString();

                    return await _quickBooksOptions.GetUrl(partialUrl)
                        .WithOAuthBearerToken(token)
                        .WithHeader("Accept", "application/json")
                        .PostAsync(content)
                        .ReceiveJson<T>();
            }
        }

        public ProfitOptics.Framework.Core.Settings.QuickBooksOptions GetOptions()
        {
            return _quickBooksOptions;
        }

        #endregion Helper Methods

        #region Authorization Methods

        public async Task<TokenResponse> RefreshAuthorizeToken(string refreshToken)
        {
            TokenResponse response = null;

            DiscoveryResponse doc;
            DiscoveryClient discoveryClient;

            if (!string.IsNullOrEmpty(_quickBooksOptions.DiscoveryUrl) && !string.IsNullOrEmpty(_quickBooksOptions.ClientId) && !string.IsNullOrEmpty(_quickBooksOptions.ClientSecret))
            {
                discoveryClient = new DiscoveryClient(_quickBooksOptions.DiscoveryUrl);
                doc = discoveryClient.GetAsync().Result;

                if (doc.StatusCode == HttpStatusCode.OK)
                {
                    var tokenEndpoint = doc.TokenEndpoint;

                    var tokenClient = new TokenClient(tokenEndpoint, _quickBooksOptions.ClientId, _quickBooksOptions.ClientSecret);
                    response = await tokenClient.RequestRefreshTokenAsync(refreshToken);
                }
            }

            return response;
        }

        public async Task<TokenRevocationResponse> RevokeAccessToken(string token)
        {
            TokenRevocationResponse response = null;
            DiscoveryResponse doc;
            DiscoveryClient discoveryClient;

            if (!string.IsNullOrEmpty(_quickBooksOptions.DiscoveryUrl) && !string.IsNullOrEmpty(_quickBooksOptions.ClientId) && !string.IsNullOrEmpty(_quickBooksOptions.ClientSecret))
            {
                discoveryClient = new DiscoveryClient(_quickBooksOptions.DiscoveryUrl);
                doc = await discoveryClient.GetAsync();

                if (doc.StatusCode == HttpStatusCode.OK)
                {
                    var revokeClient = new TokenRevocationClient(doc.RevocationEndpoint, _quickBooksOptions.ClientId, _quickBooksOptions.ClientSecret);
                    response = await revokeClient.RevokeAccessTokenAsync(token);

                }
            }

            return response;
        }

        public string GetAuthorizeUrl(string scope)
        {
            var state = Guid.NewGuid().ToString("N");

            var authorizeUrl = string.Empty;
            var url = string.Empty;

            DiscoveryResponse doc;
            DiscoveryClient discoveryClient;

            if (!string.IsNullOrEmpty(_quickBooksOptions.DiscoveryUrl) && !string.IsNullOrEmpty(_quickBooksOptions.ClientId) && !string.IsNullOrEmpty(_quickBooksOptions.ClientSecret))
            {
                discoveryClient = new DiscoveryClient(_quickBooksOptions.DiscoveryUrl);
                doc = discoveryClient.GetAsync().Result;

                if (doc.StatusCode == HttpStatusCode.OK)
                {
                    authorizeUrl = doc.AuthorizeEndpoint;

                    //Make Authorization request
                    var request = new AuthorizeRequest(authorizeUrl);

                    url = request.CreateAuthorizeUrl(
                       clientId: _quickBooksOptions.ClientId,
                       responseType: OidcConstants.AuthorizeResponse.Code,
                       scope: scope,
                       redirectUri: _quickBooksOptions.RedirectUrl,
                       state: state);
                }
            }

            return url;
        }

        public QBUpdateTokenResponse UpdateToken(string code)
        {
            QBUpdateTokenResponse updateTokenResponse = null;

            DiscoveryResponse doc;
            DiscoveryClient discoveryClient;

            if (!string.IsNullOrEmpty(_quickBooksOptions.DiscoveryUrl) && !string.IsNullOrEmpty(_quickBooksOptions.ClientId) && !string.IsNullOrEmpty(_quickBooksOptions.ClientSecret))
            {
                discoveryClient = new DiscoveryClient(_quickBooksOptions.DiscoveryUrl);
                doc = discoveryClient.GetAsync().Result;

                if (doc.StatusCode == HttpStatusCode.OK)
                {
                    var tokenEndpoint = doc.TokenEndpoint;

                    var tokenClient = new TokenClient(tokenEndpoint, _quickBooksOptions.ClientId, _quickBooksOptions.ClientSecret);
                    TokenResponse response = tokenClient.RequestTokenFromCodeAsync(code, _quickBooksOptions.RedirectUrl).Result;

                    if (response != null)
                    {
                        //TODO: map this:
                        updateTokenResponse = new QBUpdateTokenResponse()
                        {
                            AccessToken = response.AccessToken,
                            AccessTokenExpiresIn = response.AccessTokenExpiresIn,
                            ErrorDescription = response.ErrorDescription,
                            RefreshToken = response.RefreshToken,
                            TokenType = response.TokenType,
                            IdentityToken = response.IdentityToken,
                            HttpErrorReason = response.AccessToken,
                            RefreshTokenExpiresIn = response.RefreshTokenExpiresIn
                        };
                    }
                }
            }

            return updateTokenResponse;
        }

        #endregion Authorization Methods

        #region Customer Methods

        public Customer GetCustomer(int id, string token)
        {
            Customer customer = null;

            try
            {
                customer = GetAsync<CustomerResponse>($"customer/{id}", token).Result.Customer;
            }
            catch (Exception e)
            {

            }

            return customer;
        }

        public List<Customer> GetCustomerList(string token)
        {
            List<Customer> customers = null;

            try
            {
                customers = PostAsync<CustomerListResponse>($"query", token, CUSTOMER_READ_ALL, RequestContentType.Text).Result.QueryResponse.Customers;
            }
            catch (Exception e)
            {

            }

            return customers;
        }

        public EditCustomerResponse EditCustomer(Customer customer, string token)
        {
            EditCustomerResponse result = null;

            try
            {
                var customerToCreate = CreateRequestObjFromCustomer(customer);

                result = PostAsync<EditCustomerResponse>($"customer", token, customerToCreate, RequestContentType.Json).Result;
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public EditCustomerResponse SetCustomerActiveStatus(int customerId, bool isActive, string token, string syncToken)
        {
            EditCustomerResponse result = null;

            try
            {
                var requestObj = new
                {
                    domain = "QBO",
                    sparse = true,
                    Id = customerId,
                    SyncToken = syncToken ?? "0",
                    Active = isActive
                };

                result = PostAsync<EditCustomerResponse>($"customer", token, requestObj, RequestContentType.Json).Result;
            }
            catch (Exception e)
            {

            }

            return result;
        }

        private object CreateRequestObjFromCustomer(Customer customer)
        {
            dynamic result = new ExpandoObject();
            result.DisplayName = customer.DisplayName;
            result.Notes = customer.Notes;
            result.GivenName = customer.GivenName;
            result.MiddleName = customer.MiddleName;
            result.FamilyName = customer.FamilyName;
            result.BillAddr = new
            {
                customer.BillAddr.Line1,
                customer.BillAddr.City,
                customer.BillAddr.Country,
                customer.BillAddr.CountrySubDivisionCode,
                customer.BillAddr.PostalCode
            };
            result.PrimaryPhone = new
            {
                customer.PrimaryPhone.FreeFormNumber
            };
            result.AlternatePhone = new
            {
                customer.AlternatePhone.FreeFormNumber
            };
            if (customer.PrimaryEmailAddr != null && !string.IsNullOrWhiteSpace(customer.PrimaryEmailAddr.Address))
            {
                result.PrimaryEmailAddr = new
                {
                    customer.PrimaryEmailAddr.Address
                };
            }

            if (customer.Id.HasValue && customer.Id.Value > 0)
            {
                result.Id = customer.Id.Value;
            }

            if (customer.SyncToken != null)
            {
                result.SyncToken = customer.SyncToken;
            }

            return result as object;

            ////var result = customer.ToDynamic();
            //if (customer.Id.HasValue && customer.Id.Value > 0)
            //{
            //    result.Id = customer.Id.Value;
            //}
            //return result as object;
        }

        #endregion Customer Methods

        #region Vendor Methods

        private object CreateRequestObjFromVendor(Vendor vendor)
        {
            dynamic result = new ExpandoObject();
            result.DisplayName = vendor.DisplayName;
            result.BillAddr = new
            {
                vendor.BillAddr.Line1,
                vendor.BillAddr.City,
                vendor.BillAddr.Country,
                vendor.BillAddr.CountrySubDivisionCode,
                vendor.BillAddr.PostalCode
            };
            result.PrimaryPhone = new
            {
                vendor.PrimaryPhone.FreeFormNumber
            };
            if (vendor.PrimaryEmailAddr != null && !string.IsNullOrWhiteSpace(vendor.PrimaryEmailAddr.Address))
            {
                result.PrimaryEmailAddr = new
                {
                    vendor.PrimaryEmailAddr.Address
                };
            }
            if (vendor.WebAddr != null && !string.IsNullOrWhiteSpace(vendor.WebAddr.URI))
            {
                result.WebAddr = new
                {
                    vendor.WebAddr.URI
                };
            }

            if (vendor.Id.HasValue && vendor.Id.Value > 0)
            {
                result.Id = vendor.Id.Value;
            }

            if (vendor.SyncToken != null)
            {
                result.SyncToken = vendor.SyncToken;
            }

            return result as object;
        }

        public EditVendorResponse EditVendor(Vendor vendor, string token)
        {
            EditVendorResponse result = null;

            try
            {
                var vendorToCreate = CreateRequestObjFromVendor(vendor);

                result = PostAsync<EditVendorResponse>($"vendor", token, vendorToCreate, RequestContentType.Json).Result;
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public Vendor GetVendor(int id, string token)
        {
            Vendor vendor = null;

            try
            {
                vendor = GetAsync<VendorResponse>($"vendor/{id}", token).Result.Vendor;
            }
            catch (Exception e)
            {

            }

            return vendor;
        }

        #endregion Vendor Methods

        #region Items

        public Item GetItem(string sku, string token)
        {
            Item item = null;
            List<Item> items = null;

            try
            {
                var query = $"{ITEMS_READ_ALL} where Sku = '{sku}'";
                items = PostAsync<ItemQueryResponse>($"query", token, query, RequestContentType.Text).Result.QueryResponse.Items;

                if(items != null && items.Count > 0)
                {
                    item = items.First();
                }
            }
            catch (Exception e)
            {

            }

            return item;
        }

        public EditItemResponse EditItem(Item qbItem, string token)
        {
            EditItemResponse result = null;

            try
            {
                var itemToCreate = CreateRequestObjFromItem(qbItem);

                result = PostAsync<EditItemResponse>($"item", token, itemToCreate, RequestContentType.Json).Result;
            }
            catch (Exception e)
            {

            }

            return result;
        }

        private object CreateRequestObjFromItem(Item item)
        {
            var result = item.ToDynamic();
            //if (estimate.Id.HasValue && estimate.Id.Value > 0)
            //{
            //    result.Id = estimate.Id.Value;
            //}
            return result as object;
        }

        #endregion Items

        #region Estimates

        public EditEstimateResponse EditEstimate(CreateEstimate estimate, string token)
        {
            EditEstimateResponse result = null;

            try
            {
                var estimateToCreate = CreateRequestObjFromEstimate(estimate);
                result = PostAsync<EditEstimateResponse>($"estimate", token, estimateToCreate, RequestContentType.Json).Result;
            }
            catch (Exception e)
            {

            }

            return result;
        }

        private object CreateRequestObjFromEstimate(CreateEstimate estimate)
        {
            var result = estimate.ToDynamic();
            //if (estimate.Id.HasValue && estimate.Id.Value > 0)
            //{
            //    result.Id = estimate.Id.Value;
            //}
            return result as object;
        }

        public Estimate CreateEstimate(GCCase gcCase, string token, out string description)
        {
            description = string.Empty;
            Estimate createdEstimate = null;

            if (gcCase.GCTrays != null && gcCase.GCTrays.Count > 0)
            {
                var qbItemList = new List<ProfitOptics.Modules.Sox.Models.QuickBooks.Item>();
                decimal totalAmt = 0;
                decimal amount = 0;
                int lineItemNum = 1;

                var estimate = new ProfitOptics.Modules.Sox.Models.QuickBooks.CreateEstimate()
                {
                    EstimateLines = new List<ProfitOptics.Modules.Sox.Models.QuickBooks.EstimateLine>(),
                    TxnTaxDetail = new ProfitOptics.Modules.Sox.Models.QuickBooks.TaxDetail()
                    {
                        TotalTax = 0
                    },
                    CustomerRef = new ProfitOptics.Modules.Sox.Models.QuickBooks.GeneralRef()
                    {
                        Value = gcCase.GCCustomer.QBCustomerId.HasValue ? gcCase.GCCustomer.QBCustomerId.ToString() : string.Empty,
                        Name = gcCase.GCCustomer.CustomerName
                    },
                    CustomerMemo = new ProfitOptics.Modules.Sox.Models.QuickBooks.GeneralValue()
                    {
                        Value = "Thank you for your business and have a great day!"
                    },
                    TotalAmt = 0,
                    ApplyTaxAfterDiscount = false,
                    PrintStatus = "NeedToPrint",
                    EmailStatus = "NotSet"
                };
                var estimateLines = new List<ProfitOptics.Modules.Sox.Models.QuickBooks.EstimateLine>();

                int? qbEstimateId = null;
                description += $"Case reference: {gcCase.CTCase.CaseReference}, containing {(gcCase.GCTrays != null ? gcCase.GCTrays.Count : 0)} Trays | ";

                foreach (var gcTray in gcCase.GCTrays.ToList())
                {
                    if (gcTray.CTContainerType != null)
                    {
                        // Saving a QB call if item already pulled:
                        ProfitOptics.Modules.Sox.Models.QuickBooks.Item qbItem = qbItemList.Where(x => x.Sku == gcTray.CTContainerType.ProcurementReferenceId).FirstOrDefault();

                        if (qbItem == null)
                        {
                            qbItem = GetItem(gcTray.CTContainerType.ProcurementReferenceId, token);
                            if (qbItem != null)
                            {
                                qbItemList.Add(qbItem);
                            }
                        }

                        if (qbItem == null)
                        {
                            var errorMsg = $"No item in Quickbooks for SKU = {gcTray.CTContainerType.ProcurementReferenceId}!";
                            description += errorMsg;
                            Log.Error(errorMsg);
                            throw new Exception(errorMsg);
                        }
                        else
                        {
                            var qty = 1;

                            var estimateLine = new ProfitOptics.Modules.Sox.Models.QuickBooks.EstimateLine()
                            {
                                LineNum = lineItemNum,
                                Description = $"Case Refrence: {gcCase.CTCase.CaseReference}",
                                Amount = qty * qbItem.UnitPrice,
                                DetailType = Helpers.Constants.QBO_SALES_ITEM_LINE_DETAIL,
                                SalesItemLineDetail = new ProfitOptics.Modules.Sox.Models.QuickBooks.EstimateLineDetail()
                                {
                                    ItemRef = new ProfitOptics.Modules.Sox.Models.QuickBooks.GeneralRef()
                                    {
                                        Value = qbItem.Id.ToString(),
                                        Name = gcTray.CTCaseContainer.ContainerName
                                    },
                                    UnitPrice = qbItem.UnitPrice,
                                    Qty = qty,
                                    TaxCodeRef = new ProfitOptics.Modules.Sox.Models.QuickBooks.GeneralValue()
                                    {
                                        Value = "NON"
                                    }
                                }
                            };
                            lineItemNum++;
                            amount += estimateLine.Amount;
                            estimateLines.Add(estimateLine);
                        }
                    }
                }

                var subTotalLineDetail = new ProfitOptics.Modules.Sox.Models.QuickBooks.EstimateLine()
                {
                    Amount = amount,
                    DetailType = "SubTotalLineDetail",
                    SubTotalLineDetail = new object()
                };
                estimateLines.Add(subTotalLineDetail);

                var discountLineDetail = new ProfitOptics.Modules.Sox.Models.QuickBooks.EstimateLine()
                {
                    Amount = 0,
                    DetailType = "DiscountLineDetail",
                    DiscountLineDetail = new ProfitOptics.Modules.Sox.Models.QuickBooks.DiscountLineDetail()
                    {
                        PercentBased = true,
                        DiscountPercent = 0,
                        DiscountAccountRef = null
                    }
                };
                estimateLines.Add(discountLineDetail);

                estimate.EstimateLines = estimateLines;
                estimate.TotalAmt = totalAmt;

                var response = EditEstimate(estimate, token);
                if (response.Estimate != null && response.Estimate.Id.HasValue)
                {
                    createdEstimate = response.Estimate;
                    qbEstimateId = response.Estimate.Id;
                }
                else if (response.Fault != null)
                {
                    var responseError = response.Fault.Errors.First().Message;
                    Log.Error(responseError);
                    throw new Exception(response.Fault.Errors.First().Message);
                }

                //QBEstimateId is removed from GCCase
                //gcCase.QBEstimateId = qbEstimateId;
                description += $"Successfully submited estimate for CaseReference: {gcCase.CTCase.CaseReference}, returned: qbEstimateId: {qbEstimateId} || ";
            }

            return createdEstimate;
        }

        #endregion Estimates

        #region Invoices

        public EditInvoiceResponse CreateInvoice(Invoice invoice, string token)
        {
            EditInvoiceResponse result = null;

            try
            {
                var invoiceToCreate = CreateRequestObjFromCreateInvoice(invoice);
                result = PostAsync<EditInvoiceResponse>($"invoice", token, invoiceToCreate, RequestContentType.Json).Result;
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public EditInvoiceResponse EditInvoice(Invoice invoice, string token)
        {
            EditInvoiceResponse result = null;

            try
            {
                var invoiceToEdit = (invoice.ToDynamic()) as object;
                result = PostAsync<EditInvoiceResponse>($"invoice", token, invoiceToEdit, RequestContentType.Json).Result;
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public Invoice GetInvoice(int qbCustomerId, string tnxDate, string token)
        {
            Invoice invoice = null;
            try
            {
                var query = $"{INVOICE_READ_ALL} where CustomerRef = '{qbCustomerId}' and TxnDate = '{tnxDate}'";
                List<Invoice> invoices = PostAsync<InvoiceQueryResponse>($"query", token, query, RequestContentType.Text).Result.QueryResponse.Invoices;
                if (invoices != null && invoices.Count > 0)
                {
                    invoice = invoices.First();
                }
            }
            catch (Exception e)
            {

            }

            return invoice;
        }

        private object CreateRequestObjFromCreateInvoice(Invoice invoice)
        {
            dynamic result = new ExpandoObject();
            result.CustomerRef = new { value = invoice.CustomerRef.Value };
            result.TxnDate = invoice.TxnDate;
            result.DueDate = invoice.DueDate;
            result.DocNumber = invoice.DocNumber;
            result.Line = invoice.InvoiceLines.ToDynamic();
            return result as object;
        }

        #endregion Invoices

        #endregion Methods
    }
}
