﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OtpSharp;
using ProfitOptics.Framework.Web.Areas.Account.Models;
using ProfitOptics.Framework.Web.Areas.Account.Services;
using ProfitOptics.Framework.Web.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ProfitOptics.Framework.Web.Framework.Services;
using Wiry.Base32;
using IAuthenticationService = ProfitOptics.Framework.Web.Framework.Services.IAuthenticationService;
using Microsoft.AspNetCore.Routing;

namespace ProfitOptics.Framework.Web.Areas.Account.Factories
{
    public class AccountModelFactory : IAccountModelFactory
    {
        private readonly IAccountManagementService _accountManagementService;
        private readonly LinkGenerator _linkGenerator;
        private readonly IUserService _userService;
        private readonly IAuthenticationService _authenticationService;

        public AccountModelFactory(IAccountManagementService accountManagementService,
            LinkGenerator linkGenerator,
            IUserService userService,
            IAuthenticationService authenticationService)
        {
            this._accountManagementService = accountManagementService;
            this._linkGenerator = linkGenerator;
            this._userService = userService;
            this._authenticationService = authenticationService;
        }

        public LoginModel PrepareLoginModel(string returnUrl)
        {
            return new LoginModel
            {
                ReturnUrl = returnUrl
            };
        }

        public async Task<ExternalLoginListModel> PrepareExternalLoginListAsync(string returnUrl, List<ExternalLoginProviderModel> externalLoginProviders, string externalLoginCallbackUrl)
        {
            var providers = await _authenticationService.GetExternalLoginProvidersAsync();
            return new ExternalLoginListModel
            {
                ReturnUrl = returnUrl,
                ExternalLoginCallbackUrl = externalLoginCallbackUrl ?? _linkGenerator.GetPathByAction("ExternalLoginCallback", "Account", new { area = "Account" }),
                ExternalLoginProviders = externalLoginProviders ?? providers.Select(schema => new ExternalLoginProviderModel
                {
                    Name = schema.Name,
                    DisplayName = schema.DisplayName
                }).ToList()
            };
        }

        public SendCodeModel PrepareSendCodeModel(string returnUrl, bool rememberMe, IList<string> twoFactorProviders)
        {
            return new SendCodeModel(returnUrl, rememberMe, twoFactorProviders);
        }

        public VerifyCodeModel PrepareVerifyCodeModel(string provider, string returnUrl, bool rememberMe)
        {
            return new VerifyCodeModel(returnUrl, rememberMe, provider);
        }

        /// <summary>
        /// Prepares a model for account management page
        /// </summary>
        /// <param name="message">Enum type for status message</param>
        /// <returns>Account management model</returns>
        public async Task<AccountManagementModel> PrepareAccountManagementModelAsync(ManageMessageId? message)
        {
            var currentUser = await _accountManagementService.GetCurrentUserAsync();

            AccountManagementModel model = new AccountManagementModel
            {
                StatusMessage = _accountManagementService.ProcessStatusMessage(message),
                SecurityStatusMessage = _accountManagementService.ProcessSecurityStatusMessage(message),
                HasPassword = await _userService.HasPasswordAsync(currentUser),
                PhoneNumber = await _userService.GetPhoneNumberAsync(currentUser),
                TwoFactor = await _userService.GetTwoFactorEnabledAsync(currentUser),
                IsGoogleAuthenticatorEnabled = currentUser.IsGoogleAuthenticatorEnabled,                
                BrowserRemembered = await _authenticationService.IsTwoFactorClientRememberedAsync(currentUser.Id),
                FirstName = currentUser.FirstName,
                LastName = currentUser.LastName
            };
            return model;
        }

        public AddPhoneNumberModel PrepareAddPhoneNumberModel()
        {
            return new AddPhoneNumberModel();
        }

        public VerifyPhoneNumberModel PrepareVerifyPhoneNumberModel(string phoneNumber)
        {
            return new VerifyPhoneNumberModel
            {
                PhoneNumber = phoneNumber
            };
        }

        public async Task<GoogleAuthenticatorModel> PrepareGoogleAuthenticatorModelAsync()
        {
            var user = await _accountManagementService.GetCurrentUserAsync();
            if (user == null)
            {
                return null;
            }
            var secretKey = KeyGeneration.GenerateRandomKey(20);
            var barcodeUrl = $"{KeyUrl.GetTotpUrl(secretKey, user.UserName)}&issuer=ProfitOptics.Framework";

            return new GoogleAuthenticatorModel
            {
                SecretKey = Base32Encoding.Standard.GetString(secretKey),
                BarcodeUrl = HttpUtility.UrlEncode(barcodeUrl)
            };
        }

        public async Task<ManageLoginsModel> PrepareManageLoginsModelAsync(ManageMessageId messageId)
        {
            var user = await _accountManagementService.GetCurrentUserAsync();
            var currentLogins = await _userService.GetExternalLoginsForUser(user);
            var usedProviders = currentLogins.Select(x => x.LoginProvider).ToList();
            var allProviders = await _authenticationService.GetExternalLoginProvidersAsync();
            return new ManageLoginsModel
            {
                StatusMessage = _accountManagementService.ProcessStatusMessage(messageId),
                CurrentLogins = PrepareCurrentLoginsModels(currentLogins),
                OtherLogins = PrepareUnusedLoginProviders(allProviders, usedProviders),
                ShowRemoveButton = await _userService.HasPasswordAsync(user) || (currentLogins != null && currentLogins.Count > 1),
                LinkLoginCallbackUrl = _linkGenerator.GetPathByAction("LinkLoginCallback", "Manage", new { area = "Account" })
            };
        }

        private IList<ExternalLoginProviderModel> PrepareUnusedLoginProviders(IList<AuthenticationScheme> allProviders, List<string> usedProviders)
        {
            if (allProviders == null || allProviders.Any())
            {
                return null;
            }

            usedProviders = usedProviders ?? new List<string>();
            return allProviders
                .Where(x => !usedProviders.Contains(x.Name))
                .Select(x => new ExternalLoginProviderModel
                {
                    DisplayName = x.DisplayName,
                    Name = x.Name
                }).ToList();
        }

        private IList<ExternalLoginManageInfo> PrepareCurrentLoginsModels(IList<IdentityUserLogin<int>> currentLogins)
        {
            return currentLogins.Select(x => new ExternalLoginManageInfo
            {
                LoginProvider = x.LoginProvider,
                ProviderDisplayName = x.ProviderDisplayName,
                ProviderKey = x.ProviderKey
            }).ToList();
        }
    }
}