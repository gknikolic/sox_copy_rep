using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Framework.Web.RestApi.Resources.Distributor
{
    public class SaveDistributorResource
    {
        [Required]
        [MaxLength(150)]
        public string Name { get; set; }
    }
}