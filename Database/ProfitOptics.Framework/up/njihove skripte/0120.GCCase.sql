USE [Sox]

ALTER TABLE [Sox].[dbo].[GCCase] DROP COLUMN IsCanceled;
ALTER TABLE [Sox].[dbo].[GCCase] ADD [CanceledAtGCLifecycleStatusTypeId] INT NULL;

GO

ALTER TABLE [dbo].[GCCase]  WITH CHECK ADD  CONSTRAINT [FK_GCCase_GCLifecycleStatusType] FOREIGN KEY([CanceledAtGCLifecycleStatusTypeId])
REFERENCES [dbo].[GCLifecycleStatusType] ([Id])
GO
ALTER TABLE [dbo].[GCCase] CHECK CONSTRAINT [FK_GCCase_GCLifecycleStatusType]
GO
