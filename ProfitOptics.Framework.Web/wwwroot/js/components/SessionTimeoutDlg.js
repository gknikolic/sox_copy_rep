﻿let SessionManager = (function() {
    let settings = {
        timeoutPeriod: 0,
        timeoutQuant: 1000,
        sessionTimeoutWarningInSeconds: 60,
        refreshSessionUrl: "",
        expiredUrl: "",
        lockscreenLoginUrl: "",
        timer: null,
        sessionExpireAlert: null,
        startTimeLocalStorageReference: 'sessionStart'
    };

    let numberOfSecondsRemaining = function() {
        let sessionStart = localStorage.getItem(settings.startTimeLocalStorageReference);
        let msSinceSessionStart = Date.now() - sessionStart;
        return (settings.timeoutPeriod - msSinceSessionStart) / 1000;
    };

    let timedOut = function() {
        return numberOfSecondsRemaining() <= 0;
    };

    let isTimeToShowSessionExpireAlert = function() {
        return numberOfSecondsRemaining() < settings.sessionTimeoutWarningInSeconds;
    };

    let showSessionExpireAlert = function() {
        if (settings.sessionExpireAlert !== null) {
            //session expire alert is already shown to the user
            return;
        }
        settings.sessionExpireAlert = app.showAlert({
            'message': "Your session is about to expire, refresh and continue?",
            'callback': function () {
                app.blockUI("");
                if (settings.timer != null) {
                    clearTimeout(settings.timer);
                }
                $.ajax({
                    type: "POST",
                    url: settings.refreshSessionUrl,
                    success: function (data) {
                        startCount();
                        app.unblockUI();
                    },
                    error: function () {
                        continueCount();
                        app.unblockUI();
                    }
                });
            },
            buttons: {
                'ok': {
                    label: 'Refresh',
                    className: 'btn-default pull-right'
                }
            }
        });
    };

    let hideSessionExpireAlert = function() {
        if (settings.sessionExpireAlert !== null) {
            settings.sessionExpireAlert.modal("hide");
            settings.sessionExpireAlert = null;
        }
    };

    let startCount = function() {
        if (settings.timer !== null) {
            clearTimeout(settings.timer);
        }
        localStorage.setItem(settings.startTimeLocalStorageReference, Date.now());
        hideSessionExpireAlert();
        continueCount();
    };

    let continueCount = function() {
        settings.timer = setTimeout(timeoutCheck, settings.timeoutQuant);
    };

    let timeoutCheck = function() {
        if (timedOut()) {
            // No response from the user still
            if (settings.timer != null) {
                clearTimeout(settings.timer);
            }
            app.blockUI("");
            hideSessionExpireAlert();
            $.ajax({
                type: "POST",
                url: settings.expiredUrl,
                success: function (data) {
                    //sets the redirect url
                    $('.lockscreen-item input[name="ReturnUrl"]').val(window.location);

                    // adding .lockscreen class to document.body hides everything and shows the lockscreen overlay
                    $('body').addClass('lockscreen');
                    $('.lockscreen-credentials').on('submit', handleLockscreenLogin);
                    app.unblockUI();                    

                    // if a user logs in via a different tab,
                    // detect that and remove the losckscreen
                    checkIfSessionRestarted();
                },
                error: function () {
                    // if the ajax call fails, this will initiate a redirection to the login page
                    window.location = settings.loginPageUrl;
                }
            });
            return;
        } else if (isTimeToShowSessionExpireAlert()) {
            showSessionExpireAlert();
        } else {
            // this hides the sessionExpireAlert in case
            // the session was refreshed in a different tab
            hideSessionExpireAlert();
        }
        continueCount();
    };

    let removeLockscreenAndStartCount = function() {
        // start the session timer count
        startCount();
        // remove lockscreen
        $('body').removeClass('lockscreen');
        $('.lockscreen-login-error').text("");
    };

    let checkIfSessionRestarted = function() {
        // checking if the timer was restarted,
        // for example, if a user logs in via a different tab
        if (timedOut()) {
            // check again in after some time has passed
            setTimeout(checkIfSessionRestarted, 5000);
        } else {
            removeLockscreenAndStartCount();
        }
    }

    let handleLockscreenLogin = function(e) {
        e.preventDefault();
        app.blockUI("");
        $('.lockscreen-login-error').text("");
        var lockscreenForm = $('.lockscreen-credentials');
        $.ajax({
            type: "POST",
            url: settings.lockscreenLoginUrl,
            data: {
                Username: lockscreenForm.find('#Username').val(),
                Password: lockscreenForm.find('#Password').val(),
                RememberMe: false
            },
            success: function (data) {
                if (typeof data !== "undefined" && data !== null) {
                    if (data.redirect) {
                        window.location = data.url;
                    }
                    else if (data.success) {
                        removeLockscreenAndStartCount();
                    } else {
                        $('.lockscreen-login-error').text(data.message);
                    }
                } else {
                    $('.lockscreen-login-error').text("A problem occurred. Please, try again later");
                }
                app.unblockUI();
            },
            error: function (data) {
                // show error on page
                $('.lockscreen-login-error').text("A problem occurred. Please, try again later");
                app.unblockUI();
            }
        });
        return false;
    };

    let initSessionManager = function(timeout, refreshSessionUrl, expiredUrl, lockscreenLoginUrl, loginPageUrl) {
        settings.timeoutPeriod = timeout;
        settings.refreshSessionUrl = refreshSessionUrl;
        settings.expiredUrl = expiredUrl;
        settings.lockscreenLoginUrl = lockscreenLoginUrl;
        settings.loginPageUrl = loginPageUrl;

        startCount();
    };

    return {
        Init: initSessionManager
    };
})();
