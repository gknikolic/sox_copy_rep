using ProfitOptics.Framework.Web.RestApi.DataLayer;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.RestApi.Resources.Customer
{
    public class CustomerResource
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Address { get; set; }

        public DistributorProduct Distributor { get; set; }

        public List<CustomerProduct> CustomerProducts { get; set; }
    }
}