﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTBILoadResultIndicatorConfig : IEntityTypeConfiguration<CTBILoadResultIndicator>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTBILoadResultIndicator> builder)
        {
            builder.ToTable("CTBILoadResultIndicator");
            builder.HasKey(entity => entity.Id);
        }
    }
}
