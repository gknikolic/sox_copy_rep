﻿(function (crossReferenceRecord, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    // Public properties
    crossReferenceRecord.somePublicProperty = {};

    // Public Methods
    crossReferenceRecord.RecordsomePublicMethod = function () {
    };
    crossReferenceRecord.itemsList = [];

    $(".startExporting").on('click', function () {
        $(".actionBtn").hide();
        $(".startExporting").hide();
        $(".isItemSelected").show();
        $(".clearSelection").show();
        $(".createExportList").show();
        $(".cancelExporting").show();
    });
    $(".createExportList").on("click", function (e) {
        if (crossReferenceRecord.itemsList.length > 0) {
            crossReferenceRecord.exportList();
            hideExportButtons();
        } else {
            app.showAlertMessage("Error", "Please select Items", "error");
            var theAlert = $('.alert-error');
            theAlert.fadeOut(7000, function () {
                theAlert.remove();
            });
        }
    });
    $(".clearSelection").on("click", function (e) {
        $(".isItemSelected").prop('checked', false);
        crossReferenceRecord.itemsList = [];
    });
    $(".cancelExporting").on("click", function (e) {
        $(".isItemSelected").prop('checked', false);
        crossReferenceRecord.itemsList = [];
        hideExportButtons();
    });

    $(".ItemId").on("change", function () {
        validateCompetitorMedcoItemCombination($(this).attr("name"));
    });
    $(".CompetitorId").on("change", function () {
        validateCompetitorMedcoItemCombination($(this).attr("name"));
    });
    $(".CompetitorItemNumber").on("keyup", function () {
        validateCompetitorMedcoItemCombination($(this).attr("name"));
    });

    var hideExportButtons = function () {
        $(".createExportList").hide();
        $(".isItemSelected").hide();
        $(".clearSelection").hide();
        $(".cancelExporting").hide();
        $(".startExporting").show();
        $(".actionBtn").show();
    }

    var validateCompetitorMedcoItemCombination = function (fieldName) {
        var competitorItemNumber = $(".CompetitorItemNumber").val() || "";
        var medcoItemNumberId = $(".ItemId").val() || "";
        var competitorId = $(".CompetitorId").val() || "";
        var id = parseInt($(".Id").val() || 0, 10);

        removeErrorMessages();

        if (competitorItemNumber === "" || medcoItemNumberId === "" || competitorId === "") {
            return true;
        }

        $.ajax({
            type: 'post',
            url: '/CrossReference/CrossReference/CheckCompetitorMedcoItemCombination',
            data: {
                id: id,
                competitorId: competitorId,
                competitorItemNumber: competitorItemNumber,
                medcoItemNumberId: medcoItemNumberId
            },
            dataType: 'html',
            cache: false,
            timeout: 1000000,
            success: function (data) {
                handleValidationResponse(JSON.parse(data).isValid, fieldName);
            },
            error: function (xr, data) {
                app.showAlertMessage("Error", "Error occurred while validationg competitor item number!", "error");
            }
        });
    };

    var handleValidationResponse = function (isValid, fieldName) {
        if (isValid) {
            enableButton();
            return true;
        } else {
            var errorMessage = "This competitor item and Medco item already live in the database!";
            addUnobtrusiveJsError(fieldName, errorMessage);
            disableButton();
        }
    };

    var addUnobtrusiveJsError = function (fieldName, errorMessage) {
        $('[data-valmsg-for="' + fieldName + '"]')
            .attr('class', 'field-validation-error')
            .html('<span id="' + fieldName + '-error" class="">' + errorMessage + '</span>');
    }

    var removeErrorMessages = function () {
        var fields = ["ItemId", "CompetitorId", "CompetitorItemNumber"];
        fields.forEach(function (fieldName) {
            $('[data-valmsg-for="' + fieldName + '"]')
                .attr('class', 'field-validation-error')
                .html('');
        });
    };

    var disableButton = function () {
        $(".modal-footer").find(".btn-primary").attr("disabled", true)
    };

    var enableButton = function () {
        $(".modal-footer").find(".btn-primary").attr("disabled", false);
    };

    var modalElements = {
        add: {
            buttonClass: "addRecord",
            titleClass: "addItemTitle"
        },
        edit: {
            buttonClass: "editRecord",
            titleClass: "editItemTitle"
        }
    };

    crossReferenceRecord.initSelectLists = function () {
        $(".CompetitorId").select2({
            placeholder: "Any(select a competitor to view only their products)"
           
        });

        $(".CompetitorIdFilter").select2({
            placeholder: "Any(select a competitor to view only their products)",
            allowClear: true
        });

        $(".CompatibilityOptionId").select2({
            placeholder: "Any(select compatibility)"
        });

        $(".CompatibilityOptionIdFilter").select2({
            placeholder: "Any(select compatibility)",
            allowClear: true
        });

        $(".ItemId").select2({
            placeholder: "Search for Medco Item Number/Description /MPN",
            ajax: {
                url: '/CrossReference/CrossReference/ItemLookupAutocomplete',
                dataType: 'json',
                type: "POST",
                quietMillis: 50,
                data: function (term) {
                    return {
                        term: term.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.items
                    };
                },
                dropdownParent: $('#CrossRefIntel-Modal'),
                cache: true
            },
            minimumInputLength: 1,
            dropdownCssClass: 'bigdrop',
            templateResult: formatResult,
            templateSelection: formatResultSelection,
            escapeMarkup: function (t) { return t; }
        });

        $(".ItemId").on("change", function () {
            $(".ItemDescription").val($(this).text());
        });
    };

    crossReferenceRecord.exportList = function () {
        app.blockUI();
        $.ajax({
            url: '/CrossReference/CrossReference/ExportItemList',
            type: "json",
            method: "POST",
            data: {
                crossReferenceIds: crossReferenceRecord.itemsList
            },
            success: function (res) {
                app.unblockUI();
                var link = '/CrossReference/CrossReference/DownloadFile?handle=__handle__&filename=__filename__';
                link = link.replace("__handle__", res.handle);
                link = link.replace("__filename__", res.filename);
                window.location.href = link;
                crossReferenceTable.ajax.reload();
                crossReferenceRecord.itemsList = [];
            }, error: function (res) {
                app.unblockUI();
                crossReferenceTable.ajax.reload();
            }
        });
    }

    function formatResult(result) {
        if (result.loading != true) {
            result = "<div class='row'><div class='col-sm-4'><b>" + result.id + "</b></div><div class='col-sm-8'>"
                + result.itemDescription + "</div></div><div class='row'><div class='col-sm-6'>AltItemMedco: "
                + (result.alternateItemNumberMedco != null ? result.alternateItemNumberMedco : "N\A") + "</div>"
                + "<div class='col-sm-6'>MPN: " + (result.mpn != null ? result.mpn : "N\A") + "</div></div>";
            return result;
        }
        return result.text;
    }

    function formatResultSelection(result) {
        return result.text;
    }

    crossReferenceRecord.attachDeleteEventHandler = function () {
        $("#DeleteCrossRefIntel-Modal").on('shown.bs.modal', function (e) {
            var id = $(e.relatedTarget).data("id");
            var thisModal = $(this);

            $(".deleteRecord").on("click", function (e) {
                e.stopImmediatePropagation();
                deleteCrossReferenceRecord(id);
            });
        });
    };

    crossReferenceRecord.attachAddEditEventHandlers = function () {

        $("#CrossRefIntel-Modal").on("show.bs.modal", function (e) {
            var row = e.relatedTarget.closest('tr');
            var id = $(e.relatedTarget).data("id");

            if (typeof id !== "undefined") {
                row.Id = id;
                showHideElements("edit");
                populateModal(row, $(this));
            } else {
                clearModal(row, $(this));
                showHideElements("add");
            }

            $(".editRecord").on("click", function (event) {
                event.stopImmediatePropagation();
                editCrossReferenceRecord();
            });

            $(".addRecord").on("click", function (event) {
                event.stopImmediatePropagation();
                addCrossReferenceRecord();
            });
        });
    };

    crossReferenceRecord.attachFilterEventHandlers = function () {
        $(".applyFilter").on("click", function (e) {
            e.preventDefault();
            crossReferenceTable.draw();
        });

        $(".clearFilter").on("click", function (e) {
            e.preventDefault();
            clearFilterFields();
            crossReferenceTable.draw();
        });

        function clearFilterFields() {
            $(".CompetitorIdFilter").val(null).trigger("change");
            $(".CompatibilityOptionIdFilter").val(null).trigger("change");
            $("#competitorItem").val('');
        };
    };

    crossReferenceRecord.attachCommentHandler = function () {
        $("#crossReference").find('[data-toggle="popover"]').on("click", function () {
            var row = $(this).closest('tr');
            var notes = crossReferenceTable.row(row).data().Notes;
            bootbox.alert(notes);
        });
    };

    crossReferenceRecord.attachVoteHandlers = function () {
        $('.thumbs-up-vote').on("click", function () {
            //var crossReferenceRecordId = $(this).data("id");
            console.log($(this).data("id"));
            app.blockUI();
            $.ajax({
                url: '/CrossReference/CrossReference/ThumbsUpClick',
                type: "json",
                method: "POST",
                data: {
                    crossReferenceId: $(this).data("id")
                },
                success: function (res) {
                    app.unblockUI();
                    crossReferenceTable.ajax.reload();
                }, error: function (res) {
                    app.unblockUI();
                    crossReferenceTable.ajax.reload();
                }
            });
        });
        $('.thumbs-down-vote').on("click", function () {
            //var crossReferenceRecordId = $(this).data("id");
            console.log($(this).data("id"));
            app.blockUI();
            $.ajax({
                url: '/CrossReference/CrossReference/ThumbsDownClick',
                type: "json",
                method: "POST",
                data: {
                    crossReferenceId: $(this).data("id")
                },
                success: function (res) {
                    app.unblockUI();
                    crossReferenceTable.ajax.reload();
                }, error: function (res) {
                    app.unblockUI();
                    crossReferenceTable.ajax.reload();
                }
            });
        });
    };

    var addCrossReferenceRecord = function () {
        crossReference.addCrossReferenceRecord(serializeItemForm(), function () {
            reloadTable("submit");
        });
    };

    var editCrossReferenceRecord = function () {
        var itemData = serializeItemForm();
        itemData.Id = $("input[name='Id']").val();

        crossReference.editCrossReferenceRecordForApproval(itemData, function () {
            reloadTable("submit");
        });
    };

    var deleteCrossReferenceRecord = function (id) {
        crossReference.removeCrossReferenceRecordForApproval(id, function (data) {
            reloadTable("delete");
        });
    };

    var reloadTable = function (action) {
        app.unblockUI();
        crossReferenceTable.ajax.reload();
        action == "submit" ? $("#CrossRefIntel-Modal").modal("hide") : $("#DeleteCrossRefIntel-Modal").modal("hide");
    };

    var serializeItemForm = function () {
        var modalForm = $('#addItemForm');
        return modalForm.serializeObject();
    };

    var populateModal = function (row, modal) {
        var data = crossReferenceTable.row(row).data();
        data.Id = row.Id;
        for (attr in data) {
            if (attr == "ItemId") {
                modal.find('.' + attr + '').empty().append("<option value = " + data.ItemId + ">" + data.ItemNumber + "/MPN</option>").trigger("change.select2");
            } else {
                modal.find('.' + attr + '').val(data[attr]).trigger("change.select2");
            }
        };
    };

    var clearModal = function (row, modal) {
        var data = crossReferenceTable.row(row).data();
        for (attr in data) {
            modal.find('.' + attr + '').val('').trigger("change.select2");
        };
    };

    var showHideElements = function (action) {
        var actions = {
            "edit": function () {
                hideElement(modalElements.add.buttonClass);
                hideElement(modalElements.add.titleClass);
                showElement(modalElements.edit.buttonClass);
                showElement(modalElements.edit.titleClass);
            },

            "add": function () {
                showElement(modalElements.add.buttonClass);
                showElement(modalElements.add.titleClass);
                hideElement(modalElements.edit.buttonClass);
                hideElement(modalElements.edit.titleClass);
            }
        };
        return actions[action]();
    };


    var hideElement = function (elementClass) {
        $('.' + elementClass + '').hide();
    };

    var showElement = function (elementClass) {
        $('.' + elementClass + '').show();
    };



    // Private Methods
    function somePrivateMethod(item) {

    }
}(window.crossReferenceRecord = window.crossReferenceRecord || {}, jQuery));