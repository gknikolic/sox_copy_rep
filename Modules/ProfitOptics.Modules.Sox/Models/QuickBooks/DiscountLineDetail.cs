﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class DiscountLineDetail
    {
        public bool PercentBased { get; set; }
        public decimal DiscountPercent { get; set; }
        public GeneralRef DiscountAccountRef { get; set; }
    }
}
