﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class Fault
    {
        [JsonProperty("Error")]
        public List<Error> Errors { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
