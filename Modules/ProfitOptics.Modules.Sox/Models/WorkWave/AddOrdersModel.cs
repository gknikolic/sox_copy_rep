﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class AddOrdersModel
	{
		[JsonProperty("orders")]
		public List<Order> Orders { get; set; }
	}
}
