using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTHierarchyRegion")]
    public partial class QTHierarchyRegion
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? ZoneId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
