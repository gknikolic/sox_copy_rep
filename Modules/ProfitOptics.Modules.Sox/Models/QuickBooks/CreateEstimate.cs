﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class CreateEstimate
    {
        [JsonProperty("Line")]
        public List<EstimateLine> EstimateLines { get; set; }

        public TaxDetail TxnTaxDetail { get; set; }

        public GeneralRef CustomerRef { get; set; }

        public GeneralValue CustomerMemo { get; set; }

        public decimal TotalAmt { get; set; }

        public bool ApplyTaxAfterDiscount { get; set; }

        public string PrintStatus { get; set; }

        public string EmailStatus { get; set; }

        //public DateTime? TxnDate { get; set; }

        //public GeneralRef CurrencyRef { get; set; }

        //public string TxnStatus { get; set; }
    }
}
