namespace ProfitOptics.Framework.Web.RestApi.Models.Product
{
    public class ProductResponse : BaseResponse<ProfitOptics.Framework.Web.RestApi.DataLayer.Product>
    {
        public ProductResponse(ProfitOptics.Framework.Web.RestApi.DataLayer.Product product) : base(product) { }

        public ProductResponse(string message) : base(message) { }
    }
}