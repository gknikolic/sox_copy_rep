﻿let customerDrillThroughChart = (function () {
    let settings = {
        containerSelector: "chart-container",
        comparisonDropdownSelector: "",
        colors: ['#64d774', '#db7466'],
        comparisonDropdownSelectedValueSelector: "",
        dateRangePickerSelector: "",
        redirectUrl: "",
        formatStringPositive: "",
        formatStringNegative: "",
        customDateRangeComparisonValue: -1
    };

    function init(containerSelector, comparisonDropdownSelector, dateRangePickerSelector, redirectUrl, formatStringPositive, formatStringNegative, customerDateRangeComparisonValue) {
        settings.containerSelector = containerSelector;
        settings.comparisonDropdownSelector = comparisonDropdownSelector;
        settings.comparisonDropdownSelectedValueSelector = comparisonDropdownSelector + " option:selected";
        settings.dateRangePickerSelector = dateRangePickerSelector;
        settings.redirectUrl = redirectUrl;
        settings.formatStringPositive = formatStringPositive;
        settings.formatStringNegative = formatStringNegative;
        settings.customDateRangeComparisonValue = customerDateRangeComparisonValue;
    }

    function getDataAndDrawChart(customerId, customerName, customerNum, data, isCorporate) {
        showOverlay();

        drawChart(customerId, customerName, customerNum, data, isCorporate);

        hideOverlay();
    }
    
    function showOverlay() {
        $(settings.containerSelector).parents(".row").find(".table-overlay").show();
    }

    function hideOverlay() {
        $(settings.containerSelector).parents(".row").find(".table-overlay").hide();
    }

    function drawChart(customerId, customerName, customerNum, dataPoints, isCorporate) {
        let redirectLinkDictionary = constructRedirectLinkDictionary(customerId, dataPoints);

        Highcharts.chart(settings.containerSelector.replace("#", ""), {
            lang: {
                thousandsSep: ","
            },
            credits: {
                enabled: false
            },
            title: {
                text: getChartTitle(customerName, customerNum),
                useHTML: true
            },
            xAxis: {
                categories: constructCategories(dataPoints),
                labels: {
                    formatter: function () {
                        const redirectUrl = redirectLinkDictionary[this.value];

                        return "<a href='" + redirectUrl + "'>" + this.value + "</a>";
                    },
                    useHTML: true
                },
                crosshair: true
            },
            yAxis: [{
                labels: {
                    format: "${value:,.0f}",
                    formatter: function () {
                        return "$" + Highcharts.numberFormat(this.value, 0, '.', ',');
                    }
                },
                title: {
                    text: "Variance Sales"
                }
            }],
            plotOptions: {
                column: {
                    zones: [{
                        value: 0, // Values up to 10 (not including) ...
                        color: settings.colors[1]
                    }, {
                        color: settings.colors[0]
                        }],
                    color: settings.colors[1],
                    pointPadding: 0.2,
                    borderWidth: 0,
                    cursor: "pointer",
                    events: {
                        click: function (event) {
                            var url = settings.redirectUrl;
                            url += "?id=" + customerId;
                            url += "&comparisonType=" + $(settings.comparisonDropdownSelectedValueSelector).val();
                            url += "&itemCategoryId=" + event.point.id;

                            if ($(settings.comparisonDropdownSelector).val() === settings.customDateRangeComparisonValue) {
                                url += "&comparisonStartDate=" + $(settings.dateRangePickerSelector).data("daterangepicker").startDate.format("MM/DD/YYYY") +
                                    "&comparisonEndDate=" + $(settings.dateRangePickerSelector).data("daterangepicker").endDate.format("MM/DD/YYYY");
                            }

                            window.open(
                                url,
                                "_blank"
                            );
                        }
                    }
                }
            },
            series: constructBarSeries(dataPoints, isCorporate),
            tooltip: {
                distance: 1
            }
        });
    }

    function getChartTitle(customerName, customerNum) {
        return customerName + " (" + customerNum + ") - " + $(settings.comparisonDropdownSelectedValueSelector).text();
    }

    function constructCategories(dataPoints) {
        const categories = [];

        $.each(dataPoints, function (_, val) {
            categories.push(val.Name);
        });

        return categories;
    }

    function constructRedirectLinkDictionary(customerId, dataPoints) {
        var dict = {};

        $.each(dataPoints, function (_, val) {
            let url = settings.redirectUrl;
            url += "?id=" + customerId;
            url += "&comparisonType=" + $(settings.comparisonDropdownSelectedValueSelector).val();
            url += "&itemCategoryId=" + val.Id;

            if ($(settings.comparisonDropdownSelector).val() === settings.customDateRangeComparisonValue) {
                url += "&comparisonStartDate=" + $(settings.dateRangePickerSelector).data("daterangepicker").startDate.format("DD/MM/YYYY") +
                    "&comparisonEndDate=" + $(settings.dateRangePickerSelector).data("daterangepicker").endDate.format("DD/MM/YYYY");
            }

            dict[val.Name] = url;
        });

        return dict;
    }

    function constructBarSeries(dataPoints, isCorporate) {
        let series = [];

        var values = [];
        $.each(dataPoints, function (_, val) {
            values.push({
                y: val.VarianceSales,
                varianceGrossProfit: helpers.formatMoney(val.VarianceGrossProfit, 0, 0, settings.formatStringPositive, settings.formatStringNegative),
                varianceSalesFormatted: helpers.formatMoney(val.VarianceSales, 0, 0, settings.formatStringPositive, settings.formatStringNegative),
                isCorporate: isCorporate,
                id: val.Id,
                dataLabels: {
                    enabled: true,
                    className: "highlight",
                    formatter: function () {
                        return "";
                    },
                    useHTML: true
                }
            });
        });

        series.push({
            type: "column",
            yAxis: 0,
            name: "Item Categories",
            tooltip: {
                valueSuffix: "$",
                shared: false,
                pointFormat: "<span>\u25CF</span> Variance Sales: <b>{point.varianceSalesFormatted}</b><br/>"
            },
            data: values,
        });

        return series;
    }

    return {
        init: init,
        render: getDataAndDrawChart
    };
})();