﻿using System;

namespace ProfitOptics.Modules.ManualEtl.Models.Alerting
{
    public class EtlLogStatusViewModel
    {
        public string Status { get; set; }
        public string TableName { get; set; }
        public DateTime ProcessingTime { get; set; }
    }
}