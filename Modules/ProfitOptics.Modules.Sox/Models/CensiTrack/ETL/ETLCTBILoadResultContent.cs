﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTBILoadResultContent
    {
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("container")]
        public string Container { get; set; }

        [JsonProperty("updatedBy")]
        public string UpdatedBy { get; set; }

        [JsonProperty("lastUpdate")]
        public DateTime LastUpdate { get; set; }

        [JsonProperty("lastContainer")]
        public string LastContainer { get; set; }

        [JsonProperty("lastLocation")]
        public string LastLocation { get; set; }

        [JsonProperty("sort")]
        public string Sort { get; set; }

        [JsonProperty("biotestFlag")]
        public string BiotestFlag { get; set; }

        [JsonProperty("assetId")]
        public long AssetId { get; set; }

        [JsonProperty("assetType")]
        public string AssetType { get; set; }

        [JsonProperty("caseReference")]
        public string CaseReference { get; set; }
    }
}
