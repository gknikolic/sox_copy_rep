﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Services.TrendVariance
{
    public class VarianceGroupKey
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }
    }

    public class VarianceGroupedModel
    {
        public VarianceGroupKey Key { get; set; }

        public IList<ReportSalesHistory> Sales { get; set; }
     }
}