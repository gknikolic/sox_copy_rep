﻿using System;
using Microsoft.Extensions.Caching.Memory;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    public class MemoryCacheHelper : IMemoryCacheHelper
    {
        private readonly IMemoryCache _cache;

        public MemoryCacheHelper(IMemoryCache cache)
        {
            _cache = cache;
        }

        public T Get<T>(string key, Func<T> acquire, int cacheTimeInMinutes)
        {
            return _cache.GetOrCreate(key, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromMinutes(cacheTimeInMinutes);
                return acquire();
            });
        }

        public void Set<T>(string key, T data, int cacheTimeInMinutes)
        {
            var options = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(cacheTimeInMinutes));

            _cache.Set(key, data, options);
        }
    }
}