﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Framework.Identity.Domain
{
    public class ApplicationRole : IdentityRole<int>
    {
        public ICollection<ApplicationUserRole> UserRoles { get; set; }
    }
}