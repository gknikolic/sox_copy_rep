﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using System.Reflection;

namespace ProfitOptics.Framework.Core.Data
{
    public class DelimitedFile
    {
        public static string DelimitedFileContentType { get { return "text/csv"; } }

        public static void ExportToDelimitedFile<T>(string outputFileName, List<T> data, bool writeHeader = true, string delimeter = ",")
            where T : IDataRecord
        {
            using (var stream = new FileStream(outputFileName, FileMode.Create, FileAccess.ReadWrite))
            {
                ExportDelimitedFile(stream, data, writeHeader, delimeter);
            }
        }

        public static void ExportToDelimitedFile<T>(Stream outputStream, List<T> data, bool writeHeader = true, string delimeter = ",")
           where T : IDataRecord
        {
            ExportDelimitedFile(outputStream, data, writeHeader, delimeter);
        }

        public static DataImportResult<T> LoadFromDelimitedFile<T>(string fileName, bool hasHeader, string delimeter = ",")
            where T : IDataRecord
        {
            var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            return LoadFromDelimitedFile<T>(stream, hasHeader, delimeter);
        }

        public static DataImportResult<T> LoadFromDelimitedFile<T>(Stream inputStream, bool hasHeader, string delimeter = ",")
            where T : IDataRecord
        {
            using (var reader = new StreamReader(inputStream))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.Delimiter = delimeter;
                csv.Configuration.HasHeaderRecord = hasHeader;

                var toReturn = new DataImportResult<T>();

                var properties = new Dictionary<int, PropertyInfo>();
                var myType = typeof(T);
                var allProperties = myType.GetProperties();

                foreach (var property in allProperties)
                {
                    var dataFieldAttribute = property.GetCustomAttributes(true).OfType<DataFieldAttribute>().FirstOrDefault() as DataFieldAttribute;
                    if (dataFieldAttribute != null)
                        properties.Add(dataFieldAttribute.Position, property);
                }

                var currentColumn = 0;
                var rowNum = 0;

                if (hasHeader) csv.ReadHeader();

                try
                {
                    while (csv.Read())
                    {
                        var objT = Activator.CreateInstance<T>();

                        foreach (var currentIndex in properties.Keys)
                        {
                            currentColumn = currentIndex;
                            var value = csv.GetField(currentIndex - 1);
                            // Handle special cases
                            properties[currentIndex].SetValue(objT, GetSafePropertyValue(properties[currentIndex].PropertyType, value));
                        }

                        toReturn.Data.Add(objT);
                    }
                }
                catch (Exception ex)
                {
                    toReturn.Errors.Add(new DataImportError(rowNum, currentColumn, ex.Message));
                }

                return toReturn;
            }
        }

        private static void ExportDelimitedFile<T>(Stream stream, List<T> data, bool writeHeader, string delimeter)
            where T : IDataRecord
        {
            using (var writer = new StreamWriter(stream))
            using (var csv = new CsvWriter(writer))
            {
                csv.Configuration.Delimiter = delimeter;
                csv.Configuration.HasHeaderRecord = writeHeader;

                var reader = DataRecordHelpers.ToDataReader(data);
                var fieldFormats = DataRecordHelpers.GetPropertiesMetadata<T>().Select(p => p.Attribute.FormatString).ToList();

                if (writeHeader)
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        var name = reader.GetName(i);
                        csv.WriteField(name);
                    }

                    csv.NextRecord();
                }

                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        var format = fieldFormats[i];

                        if (!string.IsNullOrEmpty(format))
                        {
                            var value = reader.GetValue(i);
                            csv.WriteField(string.Format(format, value));
                        }
                        else
                        {
                            var value = reader.GetValue(i);
                            csv.WriteField(value);
                        }
                    }

                    csv.NextRecord();
                }
            }
        }

        private static object GetSafePropertyValue(Type type, object value)
        {
            // Handle all kinds of data types
            if (type == typeof(DateTime))
            {
                if (value == null)
                    return DateTime.MinValue;
                else if (value is double)
                    return DateTime.FromOADate((double)value);
                else if (value is string)
                    return DateTime.Parse((string)value);
                else
                    return Convert.ToDateTime(value);
            }
            else
            {
                var nonNullableType = Nullable.GetUnderlyingType(type) ?? type;
                var safeValue = (value == null) ? null : Convert.ChangeType(value, nonNullableType);

                return safeValue;
            }
        }
    }
}
