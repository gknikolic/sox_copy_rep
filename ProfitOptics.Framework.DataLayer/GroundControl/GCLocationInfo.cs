﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.GroundControl
{
    public class GCLocationInfo
    {
        public int Id { get; set; }
        public DateTime? Timestamp { get; set; }
        public float? Temperature { get; set; }
        public float? Humidity { get; set; }
        public float? Pressure { get; set; }
        public float? ACPH { get; set; }
        public int? GCLocationId { get; set; }

        public GCLocation GCLocation { get; set; }
    }
}
