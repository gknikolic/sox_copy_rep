﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProfitOptics.Modules.Reporting.Models.CustomerReport
{
    public class CustomerReportModel
    {
        public string CustomerName { get; set; }
        public List<MonthlySalesModel> MonthlySales { get; set; }
        public decimal LtmSales => MonthlySales.Sum(x => x.Sales);
        public decimal PreviousLtmSales { get; set; }
        public decimal PreviousLtmSalesAndLtmSalesDifference => PreviousLtmSales - LtmSales;
        public decimal LtmSalesPercent => PreviousLtmSales == decimal.Zero ? decimal.Zero : (LtmSales - PreviousLtmSales) / Math.Abs(PreviousLtmSales);
    }
}