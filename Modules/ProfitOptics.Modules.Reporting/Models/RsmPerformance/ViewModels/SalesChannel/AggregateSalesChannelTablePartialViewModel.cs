﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.PartialTableViewModels;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.SalesChannel
{
    public class AggregateSalesChannelTablePartialViewModel : TablePartialViewModelBase
    {
        public List<SalesChannelQuotaPerformanceDataModel> SalesChannels { get; set; }
    }
}