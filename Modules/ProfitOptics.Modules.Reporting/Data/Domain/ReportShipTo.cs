using System;
using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportShipTo : IFilterableEntity
    {
        public ReportShipTo()
        {
            ReportSalesHistories = new HashSet<ReportSalesHistory>();
        }

        [Select2Id]
        public int Id { get; set; }

        public int? SalesRepId { get; set; }

        public int BillToId { get; set; }
        
        public string ShipToNum { get; set; }
        
        public string ShipToName { get; set; }

        [Select2Text]
        public string FullShipToName => $"{ShipToName} ({ShipToNum})";

        public string Address { get; set; }
        
        public string City { get; set; }
        
        public string State { get; set; }
        
        public string Zip { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual ReportBillTo ReportBillTo { get; set; }

        public virtual ReportHierarchySalesRep ReportHierarchySalesRep { get; set; }
        
        public virtual ICollection<ReportSalesHistory> ReportSalesHistories { get; set; }
    }
}
