﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class EEDataPodBarcodes
	{
		[JsonProperty("barcodes")]
		public List<EEPodBarcodes> Barcodes { get; set; }
	}


}
