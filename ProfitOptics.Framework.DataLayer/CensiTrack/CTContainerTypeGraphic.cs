﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerTypeGraphic
    {
        public int Id { get; set; }
        public int CTContainerTypeId { get; set; }
        public int? GCImageId { get; set; }
        public string ImageName { get; set; }
        public long ContainerTypeId { get; set; }
        public long GraphicId { get; set; }
        public bool IsGlobal { get; set; }
        public int UsageFlags { get; set; }
        public long ReferenceId { get; set; }
        public bool IsAssembly { get; set; }
        public bool IsDecontam { get; set; }
        public bool IsSterilization { get; set; }
        public string Path { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual GCImage GCImage { get; set; }
    }
}
