﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueTransitionsStatusCategory
    {
        public string self { get; set; }

        public string key { get; set; }

        public string name { get; set; }

        public string id { get; set; }
    }
}