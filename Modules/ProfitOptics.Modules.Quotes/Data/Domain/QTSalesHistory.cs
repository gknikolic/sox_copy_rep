using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTSalesHistory")]
    public partial class QTSalesHistory
    {
        public string Company { get; set; }

        public string BillToNum { get; set; }

        public string ShipToNum { get; set; }

        public string ItemNum { get; set; }

        public int InvoiceNum { get; set; }

        public int InvoiceLineNum { get; set; }

        public DateTime? DateInv { get; set; }

        public string InvoiceType { get; set; }

        public int? PackNum { get; set; }

        public int? PackLine { get; set; }

        public decimal? QtyOrder { get; set; }

        public decimal QtyShip { get; set; }

        public int? OrderNum { get; set; }

        public int? OrderLineNum { get; set; }

        public string OrderUM { get; set; }

        public decimal ExtPrice { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal? UnitCost { get; set; }

        public bool? StandingOrder { get; set; }
    }
}
