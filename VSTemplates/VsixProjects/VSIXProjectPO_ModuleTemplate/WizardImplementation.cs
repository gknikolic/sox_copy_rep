﻿using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using System;
using System.Collections.Generic;

namespace VSIXProjectPO_ModuleTemplate
{
    class WizardImplementation : IWizard
    {
        public void BeforeOpeningFile(ProjectItem projectItem)
        {
            
        }

        public void ProjectFinishedGenerating(Project project)
        {
            
        }

        public void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
            
        }

        public void RunFinished()
        {
            
        }

        public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            var safeprojectname = replacementsDictionary.ContainsKey("$safeprojectname$") ? replacementsDictionary["$safeprojectname$"].ToString() : string.Empty;

            if (string.IsNullOrEmpty(safeprojectname)) throw new ArgumentNullException("Could not find $safeprojectname$ attribute in the replacementsDictionary, RunStarted method of EmptyModuleWizard.");

            var splittedValues = safeprojectname.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            replacementsDictionary.Add("$shortmodulename$", splittedValues[splittedValues.Length - 1]); // adding just the module name, without ProfitOptics.Modules prefix
        }

        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }
    }
}
