﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class CustomerListResponse : ResponseBase
    {
        public CustomerListContainer QueryResponse { get; set; }
    }
}
