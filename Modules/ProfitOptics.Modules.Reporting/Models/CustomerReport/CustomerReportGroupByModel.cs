﻿namespace ProfitOptics.Modules.Reporting.Models.CustomerReport
{
    public class CustomerReportGroupByModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerNum { get; set; }
    }
}