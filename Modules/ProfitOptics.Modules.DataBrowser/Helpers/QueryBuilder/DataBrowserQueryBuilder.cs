﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.Collections.Generic;
using System.Data.Common;
using Microsoft.Data.SqlClient;
using System.Linq;
using ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Clauses;
using ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Enums;
using ProfitOptics.Modules.DataBrowser.Models;

namespace ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder
{
    public class DataBrowserQueryBuilder
    {
        #region Fields

        private readonly TableQueryModel queryModel;
        private readonly bool useColumns;

        #endregion

        #region Ctor

        public DataBrowserQueryBuilder(TableQueryModel queryModel, bool useColumns = false)
        {
            this.queryModel = queryModel;
            this.useColumns = useColumns;
        }

        #endregion

        #region Methods

        public SqlCommand BuildDataQueryCommand(bool paging = true)
        {
            var queryBuilder = new SelectQueryBuilder();
            if (IsGroupBy(queryModel.GroupByColumns))
            {
                // If we have grouping we can only select and order by the columns in the grouping
                FromTable(queryBuilder, queryModel.TableName)
                    .SelectColumns(queryBuilder, queryModel.Columns.Intersect(queryModel.GroupByColumns))
                    .SelectCount(queryBuilder)
                    .GroupBy(queryBuilder, queryModel.GroupByColumns)
                    .OrderBy(queryBuilder, GetSortColumn(queryModel.GroupByColumns, queryModel.SortColumnName), queryModel.SortDirection)
                    .SetOffset(queryBuilder, paging, queryModel.DisplayStart, queryModel.DisplayLength);
            }
            else
            {
                FromTable(queryBuilder, queryModel.TableName)
                    .SelectColumns(queryBuilder, queryModel.Columns)
                    .OrderBy(queryBuilder, queryModel.SortColumnName, queryModel.SortDirection)
                    .SetOffset(queryBuilder, paging, queryModel.DisplayStart, queryModel.DisplayLength);
            }

            AddWhere(queryBuilder);

            return queryBuilder.BuildCommand();
        }

        public DbCommand BuildCountQueryCommand()
        {
            var queryBuilder = new SelectQueryBuilder();
            if (IsGroupBy(queryModel.GroupByColumns))
            {
                var innerQueryBuilder = new SelectQueryBuilder();

                FromTable(innerQueryBuilder, queryModel.TableName)
                    .SelectColumns(innerQueryBuilder, queryModel.Columns.Intersect(queryModel.GroupByColumns))
                    .GroupBy(innerQueryBuilder, queryModel.GroupByColumns)
                    .AddWhere(innerQueryBuilder);

                var innerQuery = $"({innerQueryBuilder.BuildQuery()}) groups";

                FromTable(queryBuilder, innerQuery)
                    .SelectCount(queryBuilder);

            }
            else
            {
                FromTable(queryBuilder, queryModel.TableName)
                    .SelectCount(queryBuilder)
                    .AddWhere(queryBuilder);

            }

            return queryBuilder.BuildCommand();
        }

        public string BuildWhereStatement()
        {
            var queryBuilder = new SelectQueryBuilder();
            FromTable(queryBuilder, queryModel.TableName);

            AddWhere(queryBuilder);

            return queryBuilder.WhereStatement.BuildWhereStatement().Trim();
        }

        #endregion

        #region Util

        private DataBrowserQueryBuilder SelectCount(SelectQueryBuilder queryBuilder)
        {
            queryBuilder.SelectCount();
            return this;
        }

        private DataBrowserQueryBuilder GroupBy(SelectQueryBuilder queryBuilder, IEnumerable<string> groupByColumns)
        {
            if (groupByColumns != null && groupByColumns.Any())
            {
                queryBuilder.GroupBy(groupByColumns.ToArray());
            }
            return this;
        }

        private DataBrowserQueryBuilder SetOffset(SelectQueryBuilder queryBuilder, bool paging, int? displayStart, int? displayLength)
        {
            if (paging && displayStart.HasValue && displayLength.HasValue)
            {
                queryBuilder.AddOffset(displayStart.Value, displayLength.Value);
            }
            return this;
        }

        private DataBrowserQueryBuilder FromTable(SelectQueryBuilder queryBuilder, string tableName)
        {
            queryBuilder.SelectFromTable(tableName);
            return this;
        }

        private DataBrowserQueryBuilder OrderBy(SelectQueryBuilder queryBuilder, string sortColumnName, string sortDirection)
        {
            if (!string.IsNullOrEmpty(sortColumnName))
            {
                queryBuilder.AddOrderBy(sortColumnName, sortDirection == "asc" ? Sorting.Ascending : Sorting.Descending);
            }
            else
            {
                // Even if no ordering is provided we want to order by select null so we can have pagination
                queryBuilder.AddOrderBy(string.Empty, Sorting.Ascending);
            }
            return this;
        }

        private DataBrowserQueryBuilder SelectColumns(SelectQueryBuilder queryBuilder, IEnumerable<string> columns)
        {
            if (useColumns && columns != null && columns.Any())
            {
                queryBuilder.SelectColumns(columns.ToArray());
            }
            else
            {
                queryBuilder.SelectAllColumns();
            }
            return this;
        }

        private DataBrowserQueryBuilder AddWhere(SelectQueryBuilder queryBuilder)
        {
            if (queryModel.UseWhereStatement)
            {
                queryBuilder.WhereStatement = new DynamicWhereStatement(queryModel.WhereStatement); ;
            }
            else
            {
                ConvertConditionsToWhereClauses(queryBuilder, queryModel.Conditions);
            }
            return this;
        }

        private void ConvertConditionsToWhereClauses(SelectQueryBuilder queryBuilder, IEnumerable<ConditionModel> conditions)
        {
            if (conditions != null)
            {
                IEnumerable<ConditionModel> validConditions = conditions.Where(IsConditionValid);

                if (validConditions.Any())
                {
                    ConditionModel firstCondition = validConditions.First();

                    int level = 1;
                    if (firstCondition.ComparisonOperator.Value == Comparison.IsNotNull ||
                        firstCondition.ComparisonOperator.Value == Comparison.IsNull)
                    {
                        firstCondition.Value = null;
                    }
                    queryBuilder.AddWhere(firstCondition.ColumnName, firstCondition.ComparisonOperator.Value, firstCondition.Value, level);

                    foreach (ConditionModel condition in validConditions.Skip(1))
                    {
                        if (condition.LogicOperator.Value == LogicOperator.Or)
                        {
                            level++;
                        }

                        if (condition.ComparisonOperator.Value == Comparison.IsNotNull ||
                            condition.ComparisonOperator.Value == Comparison.IsNull)
                        {
                            condition.Value = null;
                        }
                        queryBuilder.AddWhere(condition.ColumnName, condition.ComparisonOperator.Value, condition.Value, level);
                    }
                }
            }
        }
        private static bool IsGroupBy(IEnumerable<string> groupByColumns)
        {
            return groupByColumns != null && groupByColumns.Any();
        }

        private static string GetSortColumn(IEnumerable<string> groupByColumns, string sortColumnName)
        {
            return IsGroupBy(groupByColumns) ? groupByColumns.Where(column => column == sortColumnName).SingleOrDefault() : sortColumnName;
        }

        private static bool IsConditionValid(ConditionModel condition, int index)
        {
            bool valid = true;

            if (string.IsNullOrEmpty(condition.ColumnName) || !condition.ComparisonOperator.HasValue)
            {
                valid = false;
            }

            if (index != 0 && !condition.LogicOperator.HasValue)
            {
                valid = false;
            }

            return valid;
        }

        #endregion
    }
}