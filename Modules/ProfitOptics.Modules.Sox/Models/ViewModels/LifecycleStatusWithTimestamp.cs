﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class LifecycleStatusWithTimestamp
    {
        public GCLifecycleStatusType LifecycleStatus { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
