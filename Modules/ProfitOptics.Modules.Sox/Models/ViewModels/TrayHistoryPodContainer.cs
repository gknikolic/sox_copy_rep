﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayHistoryPodContainer {
		public string Note { get; set; }
		public string Vehicle { get; set; }
		public string Driver { get; set; }
		public string AdditionalNote { get; set; }
		public TrayHistoryPod Signature { get; set; }
		public List<TrayHistoryPod> Pictures { get; set; }
		public List<TrayHistoryBarcode> Barcodes { get; set; }
		public bool ShowVehicleAndDriver { get; set; }
		public bool ShowNote { get; set; }
		public bool ShowSignature { get; set; }
		public bool ShowPictures { get; set; }
		public bool ShowBarcodes { get; set; }
		public bool ShowAdditionalNote { get; set; }
	}
}
