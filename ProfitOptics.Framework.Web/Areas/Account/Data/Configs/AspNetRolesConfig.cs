﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.Web.Areas.Account.Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Data.Configs
{
    public class AspNetRolesConfig : IEntityTypeConfiguration<AspNetRoles>
    {
        public void Configure(EntityTypeBuilder<AspNetRoles> builder)
        {
            builder.ToTable(nameof(AspNetRoles));
            builder.HasKey(role => role.Id);

            builder.HasIndex(role => role.Name).HasName("RoleNameIndex").IsUnique();

            builder.Property(role => role.Name).HasMaxLength(256).IsRequired();
        }
    }
}
