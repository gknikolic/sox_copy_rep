﻿namespace ProfitOptics.Framework.Web.Framework.Identity
{
    public static class TwoFactorProviderNames
    {
        public const string Phone = "Phone";
        public const string Email = "Email";
        public const string GoogleAuthenticator = "Google Authenticator";
    }
}