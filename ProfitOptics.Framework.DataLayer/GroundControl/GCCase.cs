﻿using ProfitOptics.Framework.DataLayer.WorkWave;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public partial class GCCase
    {
        public int Id { get; set; }
        public string Title { get; set; } //CaseRef
        public int CTCaseId { get; set; }
        public string OutgoingOrderId { get; set; }
        public string IncomingOrderId { get; set; }
        public string OutgoingRequestId { get; set; }
        public string IncomingRequestId { get; set; }
        public int? GCCustomerId { get; set; }
        public int? GCVendorId { get; set; }
        public int? GCCaseStatusId { get; set; }

        public string VendorName { get; set; }
        public int? WWPickupRouteId { get; set; }
        public int? WWDeliveryRouteId { get; set; }
        public string ProcedureName { get; set; }
        //public int? QBEstimateId { get; set; }
        public int? GCInvoiceId { get; set; }
        
        public DateTime DueTimestamp { get; set; }
        public int? CanceledAtGCLifecycleStatusTypeId { get; set; }

        public bool IsCanceled { get { return CanceledAtGCLifecycleStatusTypeId.HasValue && CanceledAtGCLifecycleStatusTypeId.Value > 0; } }
		public bool IsWWCanceled { get; set; }
		public virtual ICollection<GCSet> Sets { get; set; }
        public virtual CTCase CTCase { get; set; }
        public virtual GCCustomer GCCustomer { get; set; }  
        public virtual GCVendor GCVendor { get; set; }
        public virtual GCCaseStatus GCCaseStatus { get; set; }
        public virtual WWRoute WWPickupRoute { get; set; } 
        public virtual WWRoute WWDeliveryRoute { get; set; }
        public virtual GCLifecycleStatusType CanceledAtGCLifecycleStatusType { get; set; }
        public virtual GCInvoice GCInvoice { get; set; }

        public virtual ICollection<GCCasesLifecycleStatusTypes> GCCasesLifecycleStatusTypes { get; set; }
        public virtual ICollection<GCTray> GCTrays { get; set; }

    }
}
