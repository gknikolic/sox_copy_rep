﻿using System;
using ProfitOptics.Modules.Reporting.Attributes;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge.ImportExportData
{
    public class SalesHistoryIE
    {
        [FieldTitle("LocationName")]
        public string LocationName { get; set; }

        [FieldTitle("ProductNumber")]
        public string ProductNumber { get; set; }

        [FieldTitle("InvoiceCost")]
        public decimal InvoiceCost { get; set; }

        [FieldTitle("InvoiceAmount")]
        public decimal InvoiceAmount { get; set; }

        [FieldTitle("InvoiceDT")]
        public DateTime InvoiceDT { get; set; }

        [FieldTitle("QuantityShipped")]
        public decimal QuantityShipped { get; set; }

        [FieldTitle("RebatedCost")]
        public decimal RebatedCost { get; set; }

        [FieldTitle("InvoiceNumber")]
        public string InvoiceNumber { get; set; }

        public object this[string propertyName]
        {
            get => GetType().GetProperty(propertyName)?.GetValue(this, null);
            set => GetType().GetProperty(propertyName)?.SetValue(this, value, null);
        }
    }
}