USE [Sox]

BEGIN
 
CREATE TABLE GCCustomer
(
	[Id] INT PRIMARY KEY IDENTITY(1,1),
	[FacilityId] [nvarchar](50) NOT NULL,
	[FacilityCode] [nvarchar](50) NOT NULL,
	[DeliveryStreet] [nvarchar](200) NULL,
	[DeliveryCity] [nvarchar](200) NOT NULL,
	[DeliveryZip] [nvarchar](50) NOT NULL,
	[ServiceTime] INT NOT NULL,
	[DeliveryStart] [nvarchar](50) NOT NULL,
	[DeliveryEnd] [nvarchar](50) NOT NULL,
	[DeliverNotes] [nvarchar](1000) NULL,
	[DeliverAccessCode] [nvarchar](50) NULL,
	[DeliverPhoneNumber] [nvarchar](50) NULL,
	[DeliverName] [nvarchar](50) NULL,
)

 END
