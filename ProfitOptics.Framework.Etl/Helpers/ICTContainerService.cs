﻿using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Etl.Models;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public interface ICTContainerService
    {
        //void AddOrEditGCVendors(List<Framework.DataLayer.GCVendor> gCVendors);

        void AddOrEditCTContainerTypes(List<Framework.DataLayer.CTContainerType> ctContainerTypes, long serviceId);

        void AddOrEditCTContainerItems(List<Framework.DataLayer.CTContainerItem> ctContainerItems, long containerId);
        void AddOrEditCTContainerTypeActuals(List<Framework.DataLayer.CTContainerTypeActual> ctContainerTypeActuals, long containerTypeId, int serverOffsetInMinsFromUTC);
        void AddOrEditCTContainerServices(List<Framework.DataLayer.CTContainerService> ctContainerServices);
        //void AddOrEditCTContainerTypeActualHistoryItemsAndGCTrays(List<Framework.DataLayer.CTContainerTypeActualHistoryItem> ctContainerTypeActualHistoryItems, long serviceId, int serverOffsetInMinsFromUTC, bool? isStopLifecycleHistory = false, bool? updateLapCountAndLocation = false);
        void AddOrEditCTContainerTypeGraphics(List<CTContainerTypeGraphic> lists, long containerTypeId);
        //List<CTContainerTypeGraphic> GetCTContainerTypeGraphic(bool? isWithoutImage);
        //List<CTAssemblyGraphic> GetCTAssemblyGraphics(bool? isWithoutImage);
        //void UpdateCTContainerTypeGraphic(int ctContainerTypeGraphicId, byte[] ctContainerTypeImage);
        void SyncTimestamps(int serverOffsetInMinsFromUTC);
        List<long> GetContainerAssemblyActualIds();
        void SyncLCForPastCases();

        /// <summary>
        /// Cycles through completed cases and looks for trays that are in Lifecycle: Sterilize or Sterile Staged and moves them to Shippment Staged
        /// </summary>
        void UpdateLCForCompletedCases(int serverOffsetInMinsFromUTC);
        void UpdateColorStatusForGCCasesAndGCTrays(List<CTCase> ctCasesToUse);
        void SyncColorForPastCases();
        void AddCTAssemblyGraphic(ETLCTAssemblyGraphic assemblyGraphic, int statusTypeId, long legacyId);
        void AddCTAssemblyCountSheet(ETLCTContainerAssemblyCountSheetItem countSheet, string setName, int statusTypeId);
        List<GCTraysLifecycleStatusTypes> GetAssemblyStatuses();
        void UpdateCTAssemblyGraphicsGraphic(int id, byte[] imageBytes);
        //void UpdateTrayInductionSynchronization(string runAnyway);
        void CreateGCImageThumbnails();
        void CancelOrDeleteCTCanceledCases(List<string> caseReferences, int serverOffsetInMinsFromUTC);
        void CompleteBrokenTrayHistory(List<int> gcTrayIds, bool scanAllTrays, int serverOffsetInMinsFromUTC);
        void UpdateGCTrayLapCount(bool? allTrays = false);
        void UpdateHistoryItemAndLapCount(int serverOffsetInMinsFromUTC, bool? allHistoryItems = false);
        void UpdateGCTrayLifecycles(bool updateAllTrays);
    }
}
