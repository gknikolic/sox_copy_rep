﻿namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportRegionQuota
    {
        public int Id { get; set; }

        public int SalesRepId { get; set; }

        public int RegionId { get; set; }

        public string Month { get; set; }

        public int Year { get; set; }

        public decimal Revenue { get; set; }

        public virtual ReportHierarchyRegion ReportHierarchyRegion { get; set; }

        public virtual ReportHierarchySalesRep ReportHierarchySalesRep { get; set; }
    }
}
