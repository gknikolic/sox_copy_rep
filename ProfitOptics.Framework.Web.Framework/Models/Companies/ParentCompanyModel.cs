﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Models
{
    public class ParentCompanyModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name ="Title")]
        public string Title { get; set; }

        public List<ChildCompanyModel> ChildCompanies { get; set; }
		public bool HasVendors { get; set; }
		public bool HasCustomers { get; set; }

		public ParentCompanyModel()
		{
            this.ChildCompanies = new List<ChildCompanyModel>();
		}
    }
}
