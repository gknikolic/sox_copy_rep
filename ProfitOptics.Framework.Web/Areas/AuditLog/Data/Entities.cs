﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Web.Areas.AuditLog.Data.Config;
using ProfitOptics.Framework.Web.Areas.AuditLog.Data.Domain;
using System.Diagnostics;

namespace ProfitOptics.Framework.Web.Areas.AuditLog.Data
{
    public class Entities : DbContext
    {
        public Entities(DbContextOptions<Entities> options) : base(options)
        {
            var listener = this.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }

        public virtual DbSet<POActionAudit> POActionAudits { get; set; }
        public virtual DbSet<POActionAuditFriendlyName> POActionAuditFriendlyNames { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new POActionAuditConfig());
            builder.ApplyConfiguration(new POActionAuditFriendlyNameConfig());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}