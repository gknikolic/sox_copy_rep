﻿using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Framework.Services
{
    public interface ITwoFactorResolver
    {
        /// <summary>
        /// Sends a verification code to user
        /// </summary>
        /// <param name="code">Verification code</param>
        /// <param name="user">User</param>
        /// <returns>Is sending successful</returns>
        Task<bool> SendCode(string code, ApplicationUser user);

        /// <summary>
        /// Returns name of the 2FA provider
        /// </summary>
        /// <returns></returns>
        string GetProviderName();
    }
}
