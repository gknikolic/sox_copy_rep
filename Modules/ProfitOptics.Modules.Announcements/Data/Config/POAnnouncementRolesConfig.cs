﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Announcements.Data.Domain;

namespace ProfitOptics.Modules.Announcements.Data.Config
{
    public class POAnnouncementRolesConfig : IEntityTypeConfiguration<POAnnouncementRole>
    {
        public void Configure(EntityTypeBuilder<POAnnouncementRole> builder)
        {
            builder.ToTable("POAnnouncementRoles");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.AnnouncementId);
            builder.Property(entity => entity.RoleId);

            builder.HasOne(entity => entity.POAnnouncement)
               .WithMany(p => p.POAnnouncementRoles)
               .HasForeignKey(entity => entity.AnnouncementId);

            builder.HasOne(entity => entity.Role)
                .WithMany(p => p.POAnnouncementsRoles)
                .HasForeignKey(entity => entity.AnnouncementId);
        }
    }
}
