USE [Sox]

BEGIN

CREATE TABLE [dbo].[GCLocations]
(
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50),
	[MappingTitle] [varchar](50),
	[Code] [int]
)

CREATE TABLE [dbo].[GCLocationInfos]
(
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Timestamp] datetime,
	[Temperature] decimal,
	[Humidity] decimal,
	[ACPH] decimal,
	[Pressure] decimal,
	[GCLocationId] [int],
	FOREIGN KEY ([GCLocationId]) REFERENCES [dbo].[GCLocations] ([Id])
)

END