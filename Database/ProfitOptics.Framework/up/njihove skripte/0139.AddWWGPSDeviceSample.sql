USE [Sox]

CREATE TABLE [dbo].[WWGPSDeviceSample]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[Timestamp]  [datetime] NOT NULL,
	[Latitude]  BIGINT NOT NULL,
	[Longitude]  BIGINT NOT NULL,
	[Heading]  INT NOT NULL,
	[SpeedMs]  INT NOT NULL
CONSTRAINT [PK_WWGPSDeviceSample] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO