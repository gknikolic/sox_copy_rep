﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class TicketStatusCategoryResponse
    {
        public string Self { get; set; }
        public int Id { get; set; }
        public string Key { get; set; }
        public string ColorName { get; set; }
        public string Name { get; set; }
    }

    public class TicketStatusResponse
    {
        public string Self { get; set; }
        public string Description { get; set; }
        public string IconUrl { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public TicketStatusCategoryResponse StatusCategory { get; set; }
    }
}
