using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.Web.Areas.AuditLog.Data.Domain
{
    [Table("POActionAudit")]
    public partial class POActionAudit
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Url { get; set; }

        public string Status { get; set; }

        public string IpAddress { get; set; }

        public string Browser { get; set; }

        public DateTime CreateTime { get; set; }

        public string Area { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string ReflectedType { get; set; }

        public string ReflectedActionName { get; set; }
    }
}