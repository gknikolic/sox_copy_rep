﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    /// <summary>
    /// Represents the service registrator interface.
    /// </summary>
    public interface IServiceRegistrator
    {
        /// <summary>
        /// Registers the services on application startup.
        /// </summary>
        /// <param name="services">The service collection.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="connectionStrings">The connection strings configuration.</param>
        void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null);
    }
}
