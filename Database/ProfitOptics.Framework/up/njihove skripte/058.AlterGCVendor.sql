USE [Sox]

BEGIN TRANSACTION

	ALTER TABLE [dbo].[GCVendor] ADD LegacyId INT NULL;
	ALTER TABLE [dbo].[GCVendor] ADD CreatedAtUtc DATETIME NULL;
	ALTER TABLE [dbo].[GCVendor] ADD ModifiedAtUtc DATETIME NULL;
	ALTER TABLE [dbo].[GCVendor] ADD QBVendorId INT NULL;

COMMIT TRANSACTION