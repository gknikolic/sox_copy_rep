﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class VerifyCodeModel
    {

        public VerifyCodeModel()
        {

        }

        public VerifyCodeModel(string returnUrl, bool rememberMe, string provider)
        {
            ReturnUrl = returnUrl;
            RememberMe = rememberMe;
            Provider = provider;
        }

        [Required]
        public string Provider { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
        [Required]
        public string Code { get; set; }
        [Display(Name = "Remember Browser")]
        public bool RememberBrowser { get; set; }
    }
}
