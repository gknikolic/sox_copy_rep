﻿using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum PeriodType
    {
        [Display(Name = "")]
        None,

        [Display(Name = "first")]
        First,

        [Display(Name = "second")]
        Second,
    }
}