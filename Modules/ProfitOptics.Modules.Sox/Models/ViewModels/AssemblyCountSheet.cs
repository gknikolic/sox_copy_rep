﻿using ProfitOptics.Framework.DataLayer;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class AssemblyCountSheet
	{
		public List<CTAssemblyCountSheet> AssemblyCountSheets { get; set; }
	}
}
