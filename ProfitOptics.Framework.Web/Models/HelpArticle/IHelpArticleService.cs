﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ProfitOptics.Framework.Web.Models.HelpArticle
{
    public interface IHelpArticleService
    {
        void AddHelpArticle(HelpArticleModel article);

        void EditHelpArticle(HelpArticleModel helpArticleModel);

        void DeleteHelpArticle(int id = 0);

        HelpArticleListModel GetHelpArticleListModel(HelpArticleFilterModel filter);

        IEnumerable<HelpArticleGroupedByTagModel> GetHelpArticlesGroupedByTag();

        string[] GetHelpArticleTags(bool allowTagsOfDeletedArticles = true);

        string[] GetHelpArticleNames();

        POHelpArticle ConvertHelpArticleModelToEntity(HelpArticleModel model);
    }
}
