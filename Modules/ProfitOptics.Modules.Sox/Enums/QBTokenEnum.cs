﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Enums
{
    public enum QBTokenEnum
    {
        quickbooks_access_token = 1,
        quickbooks_refresh_token = 2,
        quickbooks_access_token_expires_at = 3,
        quickbooks_refresh_token_expires_at = 4
    }
}
