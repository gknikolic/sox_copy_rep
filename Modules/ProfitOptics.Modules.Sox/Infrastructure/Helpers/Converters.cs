﻿
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Infrastructure
{
	public static class Converters
	{
		/// <summary>
		/// Returns date in yyyyMMdd format
		/// </summary>
		/// <param name="dateTime">Date to convert</param>
		/// <returns></returns>
		public static string ToWorkWaveDate(this DateTime dateTime)
		{
			return dateTime.ToString("yyyyMMdd");
		}

		public static string GetCustomerAddress(this GCCustomer customer)
		{
			var result = customer.DeliveryStreet + ", " + customer.DeliveryCity + ", " + customer.DeliveryState + " " + customer.DeliveryZip + ", USA";
			return result;
		}

		public static DateTime TrimMinutesAndSeconds(this DateTime dt)
		{
			return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, 0, 0, 0, dt.Kind);
		}

		public static DateTime TrimHoursMinutesSeconds(this DateTime dt)
		{
			return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0, dt.Kind);
		}

		public static void RenderCaseEvent(this Models.ViewModels.CalendarCaseEvent CaseEvent)
		{
			//string result = "<div class=\"fc-custom-content\" onclick=\"ShowEventDetails(`" + CaseEvent.Start+ "`)\"><div class=\"pull-left\"><span class=\"fc-custom-title\">" + CaseEvent.Title+"</span>";
			//if(CaseEvent.CasesLeft>0)
			//{
			//	//result= "<span class=\"fc-custom-more\">+"+CaseEvent.CasesLeft+" more</span></div><div class=\"pull-right\"><span class=\"fc-custom-notify\">"+CaseEvent.NumOfNotifications+"</span></div><div class=\"clearfix\"></div></div>";
			//	result = "<div class=\"fc-custom-content\" onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`)\"><div class=\"pull-left\">";
			//	//result += "<span style=\"font - size: 20px; font-weight: bold; text-align: center; color: black\" class=\"fc-custom-more\">+</span></div><div class=\"pull-right\"><span class=\"fc-custom-notify\">" + CaseEvent.NumOfNotifications + "</span></div><div class=\"clearfix\"></div></div>";
			//	result += "<span  class=\"fc-custom-title\">+</span></div><div class=\"pull-right\"><span class=\"fc-custom-notify\">" + CaseEvent.NumOfNotifications + "</span></div><div class=\"clearfix\"></div></div>";
			//}
			//else
			//{
			//	result += "</div><div class=\"pull-right\"><span class=\"fc-custom-notify\">" + CaseEvent.NumOfNotifications + "</span></div><div class=\"clearfix\"></div></div>";
			//}

			//string result = "<div onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`)\"><span class=\"fc-custom-notify fc-status-1\">" + CaseEvent.NumOfBlueCases + "</span><span class=\"fc-custom-notify fc-status-2\">" + CaseEvent.NumOfRedCases + "</span><span class=\"fc-custom-notify fc-status-3\">" + CaseEvent.NumOfPurpleCases + "</span><span class=\"fc-custom-notify fc-status-4\">" + CaseEvent.NumOfOrangeCases + "</span><span class=\"fc-custom-notify fc-status-5\">" + CaseEvent.NumOfYellowCases + "</span><span class=\"fc-custom-notify fc-status-6\">" + CaseEvent.NumOfGreenCases + "</span></div>";
			//for RED cases just insert: <span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`red`)\" class=\"fc-custom-notify fc-status-2\">" + CaseEvent.NumOfRedCases + "</span>

			//string result = "<div><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`blue`)\" class=\"fc-custom-notify fc-status-1\">" + CaseEvent.NumOfBlueCases + "</span><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`purple`)\" class=\"fc-custom-notify fc-status-3\">" + CaseEvent.NumOfPurpleCases + "</span><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`orange`)\" class=\"fc-custom-notify fc-status-4\">" + CaseEvent.NumOfOrangeCases + "</span><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`yellow`)\" class=\"fc-custom-notify fc-status-5\">" + CaseEvent.NumOfYellowCases + "</span><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`green`)\" class=\"fc-custom-notify fc-status-6\">" + CaseEvent.NumOfGreenCases + "</span></div>";
			string result = "<div><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`gray`, this)\" class=\"fc-custom-notify fc-status-7\">" + CaseEvent.NumOfGrayCases + "</span><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`red`, this)\" class=\"fc-custom-notify fc-status-2\">" + CaseEvent.NumOfRedCases + "</span><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`purple`)\" class=\"fc-custom-notify fc-status-3\">" + CaseEvent.NumOfPurpleCases + "</span><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`orange`)\" class=\"fc-custom-notify fc-status-4\">" + CaseEvent.NumOfOrangeCases + "</span><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`yellow`)\" class=\"fc-custom-notify fc-status-5\">" + CaseEvent.NumOfYellowCases + "</span><span onclick=\"ShowEventDetails(`" + CaseEvent.Start + "`, " + "`green`)\" class=\"fc-custom-notify fc-status-6\">" + CaseEvent.NumOfGreenCases + "</span></div>";

			CaseEvent.EventHtml = new Microsoft.AspNetCore.Html.HtmlString(result);
		}

		public static DateTime ConvertSecondsToDateTime(string seconds)
        {
			TimeSpan time = TimeSpan.FromSeconds(Convert.ToDouble(seconds));
			DateTime dateTime = DateTime.Today.Add(time);
			//string displayTime = dateTime.ToString("hh:mm:tt");

			return dateTime;
		}

        public static string ConvertDateTimeToSeconds(DateTime dateTime)
        {
			var midnightTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0);
			TimeSpan time = dateTime - midnightTime;
			var seconds = time.TotalSeconds.ToString();

			return seconds;
		}

		public static string NormalizeTitle(this string title)
		{
			var result = title.Replace(' ', '-').Replace(")", "").Replace("(", "");
			return result;
		}
    }
}
