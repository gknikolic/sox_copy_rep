﻿let varianceChart = (function () {
    const settings = {
        dataUrl: "",
        colors: ['#505262', '#718ada', '#64d774'],
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        grossProfitSeriesPrefix: "GP% ",
        containerSelector: "",
        checkboxSelector: "",
        data: [],
        salesSeries: [],
        grossProfitPercentSeries: [],
        isCorporate: false,
        viewBySalesSelector: ""
    };

    function showOverlay() {
        $(settings.containerSelector).parents(".row").find(".table-overlay").show();
    }

    function hideOverlay() {
        $(settings.containerSelector).parents(".row").find(".table-overlay").hide();
    }

    function checkBoxOnChange() {
        const chart = $(settings.containerSelector).highcharts();
        const seriesLength = chart.series.length;

        if ($(settings.checkboxSelector).prop("checked")) {
            for (let i = 0; i < settings.grossProfitPercentSeries.length; i++) {
                chart.addSeries(settings.grossProfitPercentSeries[i]);
            }
        }
        else {
            for (let i = seriesLength - 1; i >= 0; i--) {
                if (chart.series[i].name.indexOf(settings.grossProfitSeriesPrefix) > -1) {
                    chart.series[i].remove();
                }
            }
        }
    }

    function initCheckBoxClickEvent() {
        $(settings.checkboxSelector).unbind();

        $(settings.checkboxSelector).prop("checked", false);

        $(settings.checkboxSelector).change(checkBoxOnChange);
    }

    function fillInternalStore(data) {
        settings.data = [];

        $.each(data,
            function (i, val) {
                settings.data.push(val);
            });

        settings.barSeries = getSalesSeries();
        settings.grossProfitPercentSeries = getGrossProfitPercentSeries();
    }

    function getSalesSeries() {
        const series = [];
        
        $.each(settings.data, function(i, val) {
            series.push({
                type: 'column',
                yAxis: 0,
                name: val.Year.toString(),
                tooltip: {
                    pointFormatter: function () {
                        return this.series.name + ': ' + helpers.formatMoney(this.y, 0, null);
                    }
                },
                color: settings.colors[i],
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                data: getSalesSeriesDataPoints(val.MonthlyData)
            });
        });

        return series;
    }

    function viewBySales() {
        return $(settings.viewBySalesSelector).hasClass("btn-primary");
    }

    function getSalesSeriesDataPoints(monthlyData) {
        const dataPoints = [];
        for (let i = 1; i <= 12; i++) {
            if (monthlyData.filter(function(el) {
                    return el.Month === i;
                }).length ===
                0) {
                dataPoints.push(0);
            } else {
                const dataPoint = monthlyData.filter(function(el) {
                    return el.Month === i;
                })[0];

                const valueToParse = viewBySales() ? dataPoint.Sales : dataPoint.GrossProfit;

                const parsedValue = parseFloat(valueToParse);

                dataPoints.push(parsedValue);
            }
        }

        return dataPoints;
    }

    function getGrossProfitPercentSeries() {
        const series = [];

        $.each(settings.data, function(i, val) {
            series.push({
                type: 'line',
                yAxis: 1,
                name: settings.grossProfitSeriesPrefix + val.Year,
                tooltip: {
                    valueSuffix: '%',
                    shared: false
                },
                color: settings.colors[i],
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                data: getGrossProfitPercentSeriesDataPoints(val.MonthlyData)
            });
        });

        settings.lineSeries = series;

        return series;
    }

    function getGrossProfitPercentSeriesDataPoints(monthlyData) {
        const dataPoints = [];

        for (let i = 1; i <= 12; i++) {
            if (monthlyData.filter(function (el) {
                    return el.Month === i;
                }).length ===
                0) {
                dataPoints.push(0);
            } else {
                dataPoints.push(parseFloat((monthlyData.filter(function (el) {
                    return el.Month === i;
                })[0].GrossProfitPercent * 100).toFixed(2)));
            }
        }

        return dataPoints;
    }

    function drawChart() {
        Highcharts.chart(settings.containerSelector.replace("#", ""), {
            credits: {
                enabled: false
            },
            title: {
                text: ""
            },
            xAxis: {
                categories: settings.categories,
                min: 0,
                crosshair: true
            },
            yAxis: [{
                //primary
                labels: {
                    formatter: function () {
                        return helpers.formatMoney(this.value, 0, null);
                    }
                },
                min: 0,
                title: {
                    text: ""
                }
            }, {
                //secondary
                labels: {
                    format: "{value}%"
                },
                title: {
                    text: settings.isCorporate ? "GP%" : ""
                },
                opposite: true
            }],
            series: getSalesSeries()
        });
    }

    const init = function(dataUrl, isCorporate, containerSelector, checkboxSelector, viewBySalesSelector) {
        settings.dataUrl = dataUrl;
        settings.isCorporate = isCorporate;
        settings.containerSelector = containerSelector;
        settings.checkboxSelector = checkboxSelector;
        settings.viewBySalesSelector = viewBySalesSelector;
    };

    const getDataAndDrawChart = function() {
        showOverlay();

        $.ajax({
            url: settings.dataUrl,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                fillInternalStore(data);

                initCheckBoxClickEvent();
                
                drawChart();

                hideOverlay();
            },
            error: function (err) {
                hideOverlay();
                //drawEmptyChart(isAdmin);
            }
        });
    };

    return {
        init: init,
        render: getDataAndDrawChart
    };
})();