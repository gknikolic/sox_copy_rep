﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class TrayPickupModel
    {
        public IEnumerable<SelectListItem> GCCustomers { get; set; }
    }
}
