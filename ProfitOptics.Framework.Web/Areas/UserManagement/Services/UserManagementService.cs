﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Email;
using ProfitOptics.Framework.Web.Areas.UserManagement.Data;
using ProfitOptics.Framework.Web.Areas.UserManagement.Models;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Framework.Web.Models.Common;
using ProfitOptics.Framework.Web.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;
using POUserProfile = ProfitOptics.Framework.Web.Framework.Identity.Domain.POUserProfile;
using System.Net;
using ProfitOptics.Framework.Web.Framework.Models;
using System.Collections.Generic;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using ProfitOptics.Modules.Sox.Services;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Services
{
    public class UserManagementService : IUserManagementService
    {
        #region Fields

        private readonly Entities _entities;
        private readonly IServiceProvider _serviceProvider;
        //private readonly IUserAssignment _userAssignment;
        private readonly IEmailer _emailer;
        private IUserAssignmentService _userAssignmentService;
        private readonly IUrlHelper Url;
        private readonly IUserService _userService;
        private readonly IGroundControlService _groundControlService;

        #endregion

        #region Ctor

        public UserManagementService(Entities entities,
            IServiceProvider serviceProvider,
            //IUserAssignment userAssignment,
            IEmailer emailer,
            IUserAssignmentService userAssignmentService,
            IUrlHelper url,
            IUserService userService,
            IGroundControlService groundControlService)
        {
            this._serviceProvider = serviceProvider;
            this._entities = entities;
            //this._userAssignment = userAssignment;
            this._emailer = emailer;
            this._userAssignmentService = userAssignmentService;
            this.Url = url;
            this._userService = userService;
            this._groundControlService = groundControlService;
        }

        #endregion

        #region Methods

        public async Task<UserManagementServiceResult> SaveUser(ApplicationUserModel model)
        {
            var user = await _userService.FindUserByIdAsync(model.Id);
            if (user == null)
            {
                return UserManagementServiceResult.Failed("User not found.");
            }


            var updateResult = await UpdateUserAsync(user, model);
            if (!updateResult.Succeeded)
            {
                return updateResult;
            }

            if (model.SelectedParentCompanies != null)
            {
                _userService.EditUsersParentCompanies(model.Id, model.SelectedParentCompanies.Select(x => x));
            }


            var rolesResult = _userService.UpdateUserRolesAsync(user, model.SelectedRoles).Result;


            if (model.SelectedParentCompanies != null)
            {
                #region Edit Company Permissions
                List<string> viewPermissions = new List<string>();
                List<string> editPermissions = new List<string>();
                List<string> addPermissions = new List<string>();
                List<string> deletePermissions = new List<string>();

                if (model.SelectedParentCompanies.Count > 0)
                {
                    foreach (var companyPermission in model.ChildCompanyPermissions)
                    {
                        var companyPermissionPair = companyPermission.Split("_").ToList();
                        if (companyPermissionPair != null && companyPermissionPair.Count == 2)
                        {
                            int companyId = 0;
                            if (int.TryParse(companyPermissionPair[1], out companyId) && companyId > 0)
                            {
                                Enum.TryParse(companyPermissionPair[0], out Framework.Enums.CompanyPermissionType companyPermissionType);

                                switch (companyPermissionType)
                                {
                                    case Framework.Enums.CompanyPermissionType.CompanyPermissionTypeView:
                                        viewPermissions.Add(companyId.ToString());
                                        break;
                                    case Framework.Enums.CompanyPermissionType.CompanyPermissionTypeEdit:
                                        editPermissions.Add(companyId.ToString());
                                        break;
                                    case Framework.Enums.CompanyPermissionType.CompanyPermissionTypeAdd:
                                        addPermissions.Add(companyId.ToString());
                                        break;
                                    case Framework.Enums.CompanyPermissionType.CompanyPermissionTypeDelete:
                                        deletePermissions.Add(companyId.ToString());
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }

                var dict = new Dictionary<string, string>();
                dict.Add(Framework.Enums.CompanyPermissionType.CompanyPermissionTypeView.ToString(), string.Join(',', viewPermissions));
                dict.Add(Framework.Enums.CompanyPermissionType.CompanyPermissionTypeEdit.ToString(), string.Join(',', editPermissions));
                dict.Add(Framework.Enums.CompanyPermissionType.CompanyPermissionTypeAdd.ToString(), string.Join(',', addPermissions));
                dict.Add(Framework.Enums.CompanyPermissionType.CompanyPermissionTypeDelete.ToString(), string.Join(',', deletePermissions));
                _groundControlService.AddOrEditUserClaims(model.Id, dict);
                #endregion
            }


            return UserManagementServiceResult.Success;
        }

        private async Task<UserManagementServiceResult> UpdateUserAsync(ApplicationUser applicationUser, ApplicationUserModel applicationUserModel)
        {
            //Update user details
            if (applicationUser == null)
            {
                throw new ArgumentNullException(nameof(applicationUser));
            }

            applicationUser.UserName = applicationUserModel.UserName;
            applicationUser.FirstName = applicationUserModel.FirstName;
            applicationUser.LastName = applicationUserModel.LastName;
            applicationUser.Email = applicationUserModel.UserName;

            if (!string.IsNullOrEmpty(applicationUserModel.Phone))
            {
                applicationUser.PhoneNumber = applicationUserModel.Phone;
            }

            if (!string.IsNullOrEmpty(applicationUserModel.Title))
            {
                if (applicationUser.POUserProfile == null)
                {
                    applicationUser.POUserProfile = new POUserProfile
                    {
                        Id = applicationUser.Id
                    };
                }

                applicationUser.POUserProfile.Title = applicationUserModel.Title;
            }
            else if (applicationUser.POUserProfile != null)
                applicationUser.POUserProfile.Title = null;


            var updateResult = await _userService.UpdateUserAsync(applicationUser);
            if (!updateResult.Succeeded)
            {
                return new UserManagementServiceResult(updateResult);
            }

            //Set user roles
            if (applicationUserModel.Roles.Count() > 0)
            {
                var roleNames = applicationUserModel.Roles.Where(x => x.IsUserInRole).Select(x => x.Name).ToList();
                var updateRolesResult = await _userService.UpdateUserRolesAsync(applicationUser, roleNames);
                return new UserManagementServiceResult(updateRolesResult);
            }
            return new UserManagementServiceResult();
        }

        public async Task<UserManagementServiceResult> ToggleUserEnabledAsync(int userId)
        {
            var user = await _userService.FindUserByIdAsync(userId);
            if (user == null)
            {
                return UserManagementServiceResult.Failed("User not found!");
            }

            user.IsEnabled = !user.IsEnabled;
            var result = await _userService.UpdateUserAsync(user);
            return new UserManagementServiceResult(result);
        }

        public async Task<UserManagementServiceResult> ToggleUserLockAsync(int userId)
        {
            var user = await _userService.FindUserByIdAsync(userId);

            if (user == null)
            {
                return UserManagementServiceResult.Failed("User not found!");
            }

            IdentityResult result;
            if (await _userService.IsLockedOutAsync(user))
            {
                result = await _userService.SetLockoutEndDateAsync(user, DateTimeOffset.UtcNow);
            }
            else
            {
                result = await _userService.SetLockoutEndDateAsync(user, DateTimeOffset.UtcNow.AddYears(100));
            }
            return new UserManagementServiceResult(result);
        }

        public async Task<PasswordResetServiceResult> AjaxResetPassword(int userId, string requestScheme)
        {
            var user = await _userService.FindUserByIdAsync(userId);
            if (user == null)
            {
                return UserManagementServiceResult.Failed<PasswordResetServiceResult>("User not found.");
            }

            var passwordResetToken = await _userService.GeneratePasswordResetTokenAsync(user);
            var callbackUrl = Url.RouteUrl(POFrameworkDefaults.PasswordResetRouteName,
                new { email = user.Email, code = WebUtility.UrlEncode(passwordResetToken) }, requestScheme);
            return PasswordResetServiceResult.Success(callbackUrl, user.Email);
        }

        //public ParentsWithChildCompanies Cast(List<int> ids)
        //{
        //    ParentsWithChildCompanies companies = new ParentsWithChildCompanies();

        //    ParentsWithChildCompaniesModel companiesModel = _userService.GetChildrenForGCParentCompany(ids);
        //}

        #endregion
    }
}
