﻿namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayHistoryBarcode
	{
		public string Barcode { get; set; }
		public string Status { get; set; }
	}
}
