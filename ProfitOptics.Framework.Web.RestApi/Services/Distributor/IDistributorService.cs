using ProfitOptics.Framework.Web.RestApi.Models;
using ProfitOptics.Framework.Web.RestApi.Models.Distributor;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.Services.Distributor
{
    public interface IDistributorService
    {
        Task<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor>> ListAsync(DistributorQuery query);
        Task<List<ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor>> ListAsyncByIds(List<int> ids);
        Task<DistributorResponse> SaveAsync(ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor distributor);
        Task<DistributorResponse> UpdateAsync(int id, ProfitOptics.Framework.Web.RestApi.DataLayer.Distributor distributor);
        Task<DistributorResponse> DeleteAsync(int id);
    }
}