﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class CTContainerTypeActualHistoryItemConfig : IEntityTypeConfiguration<CTContainerTypeActualHistoryItem>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTContainerTypeActualHistoryItem> builder)
        {
            builder.ToTable("CTContainerTypeActualHistoryItem");
            builder.HasKey(entity => entity.Id);
        }
    }
}
