﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Enums
{
    public enum RequestContentType
    {
        Json = 1,
        Text = 2
    }
}
