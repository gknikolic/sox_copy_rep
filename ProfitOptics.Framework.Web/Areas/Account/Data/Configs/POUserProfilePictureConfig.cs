﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.Web.Areas.Account.Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Data.Configs
{
    public class POUserProfilePictureConfig : IEntityTypeConfiguration<POUserProfilePicture>
    {
        public void Configure(EntityTypeBuilder<POUserProfilePicture> builder)
        {
            builder.HasKey(e => e.UserId);

            builder.ToTable("POUserProfilePicture");

            builder.Property(e => e.UserId).ValueGeneratedNever();

            builder.Property(e => e.Data).IsRequired();
        }
    }
}
