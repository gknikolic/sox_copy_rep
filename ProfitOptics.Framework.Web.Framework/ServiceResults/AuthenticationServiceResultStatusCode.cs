﻿namespace ProfitOptics.Framework.Web.Framework.ServiceResults
{
    public enum AuthenticationServiceResultStatusCode
    {
        Success,
        Failed,
        UserNotFound,
        EmailNotConfirmed
    }
}
