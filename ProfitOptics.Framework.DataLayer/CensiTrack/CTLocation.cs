﻿using System;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTLocation
    {
        public int Id { get; set; }
        public int CTLocationTypeId { get; set; }
        public int? CTFacilityId { get; set; }
        public int? GCCustomerId { get; set; }
        public string LocationName { get; set; }
        public long LocationId { get; set; }
        public long? AreaId { get; set; }
        public long? BuildingId { get; set; }
        public long FacilityId { get; set; }
        public string LocationType { get; set; }
        public string SterilizationMethod { get; set; }
        public DateTime? LocationInventoryDate { get; set; }
        public string LocationInventoryUser { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }


        public virtual CTLocationType CTLocationType { get; set; }
    }
}
