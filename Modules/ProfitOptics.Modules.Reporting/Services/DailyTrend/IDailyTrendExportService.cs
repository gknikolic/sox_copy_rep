﻿using System;
using System.IO;

namespace ProfitOptics.Modules.Reporting.Services.DailyTrend
{
    public interface IDailyTrendExportService
    {
        /// <summary>
        /// Exports the daily trends for the specified parameters to an excel document.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        MemoryStream ExportDailyTrendsExcel(int userId, DateTime startDate, DateTime endDate);
    }
}
