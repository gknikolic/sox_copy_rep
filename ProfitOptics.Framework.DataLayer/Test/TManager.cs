﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.Test
{
    public class TManager
    {
        public TManager()
        {
            Employees = new HashSet<TEmployee>();
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Title { get; set; }

        public virtual ICollection<TEmployee> Employees { get; set; }
    }
}
