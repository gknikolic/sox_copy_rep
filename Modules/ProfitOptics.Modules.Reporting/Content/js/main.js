﻿var Main = function () {
    var levelArray = [];
    var Private = {

        Methods: {

            InitializeFirstTree: function () {
                $("#jstree")
                    .bind('loaded.jstree', function (event, data) {
                        var treeData = $('#jstree').jstree(true).get_json('#', { flat: false });
                        var checkedNodesVal = $("#checkedNodes").val();
                        if (checkedNodesVal != "") {
                            var checkedNodes = JSON.parse(checkedNodesVal);

                            locationNodes = checkedNodes[0];
                            for (i = 0; i < locationNodes.length; i++) {
                                $("#jstree").jstree("select_node", locationNodes[i]);

                            }
                        } else {
                            $("#jstree").jstree("check_all");
                        }
                        $(this).show();
                    })
                    .jstree({
                        "checkbox": {
                            "keep_selected_style": false
                        },
                        "plugins": ["checkbox"],
                        "core": {
                            "themes": {
                                "icons": false
                            },
                            expand_selected_onload: false,
                        }
                    })
                    // .jstree("check_all")
                    .jstree("open_node", $("#jstree li[data-rootnode]:first-of-type"));
            },

            InitializeSecondTree: function () {
                $("#jstree1")
                    .bind('loaded.jstree', function (event, data) {
                        var checkedNodesVal = $("#checkedNodes").val();
                        if (checkedNodesVal != "") {
                            var checkedNodes = JSON.parse(checkedNodesVal);

                            productNodes = checkedNodes[1];
                            for (i = 0; i < productNodes.length; i++) {
                                $("#jstree1").jstree("select_node", productNodes[i]);
                            }
                        } else {
                            $("#jstree1").jstree("check_all");
                        }

                        $(this).show();
                    })
                    .jstree({
                        "checkbox": {
                            "keep_selected_style": false
                        },
                        "plugins": ["checkbox"],
                        "core": {
                            "themes": {
                                "icons": false
                            },
                            expand_selected_onload: false,
                        }
                    })
                    .jstree("open_node", $("#jstree1 li[data-rootnode]:first-of-type"));
            },

            InitializeThirdTree: function () {
                $("#jstree3")
                    .jstree({
                        "checkbox": {
                            "keep_selected_style": false
                        },
                        "plugins": ["checkbox"],
                        "core": {
                            "themes": {
                                "icons": false
                            },
                            expand_selected_onload: false,
                        }
                    })
                    .bind('ready.jstree', function () {
                        $(this).show();
                    });
            },

            InitializeFourthTree: function () {
                $("#jstree4")
                    .jstree({
                        "checkbox": {
                            "keep_selected_style": false
                        },
                        "plugins": ["checkbox"],
                        "core": {
                            "themes": {
                                "icons": false
                            },
                            expand_selected_onload: false,
                        },
                    })
                    .bind('ready.jstree', function () {
                        //$(this).show();
                    });
                //.jstree("open_node", $("#jstree4 li[data-rootnode]:first-of-type"))
                //.jstree("open_node", $("#jstree4 li[data-rootnode]:last-of-type"));
            },

            OnloadAccordion: function () {
                $(".product-selection-levels input").prop("checked", false);
                $(".location-selection-levels input").prop("checked", false);

                var checkedLocationLevel = $("#checkedLocationLevel").val();
                if (checkedLocationLevel != "") {
                    $(".location-selection-levels input[value='" + checkedLocationLevel + "']").prop("checked", true);

                } else {
                    $(".location-selection-levels input").prop("checked", false);
                }

                var checkedProductLevel = $("#checkedProductLevel").val();
                if (checkedProductLevel != "") {
                    $(".product-selection-levels input[value='" + checkedProductLevel + "']").prop("checked", true);

                } else {
                    $(".product-selection-levels input").prop("checked", false);
                }

                $("#accordionFilters").accordion({
                    heightStyle: "content",

                });

                $("#accordionFilters h3").each(function (i) {
                    if (i != 0) {
                        $(this).addClass("ui-state-disabled");
                    }
                });
            },

            LoadTable: function () {
                Private.Methods.DrawRedrawStatisticsTable();
            },

            DrawRedrawStatisticsTable: function () {
                var priceClass = "";
                var costClass = "";
                var volumeClass = "";
                var productMixClass = "";
                var rebateRateClass = "";
                var rebateVolClass = "";
                var rebateProdMixClass = "";
                var totalImpactClass = "";
                var canDrillThrough = $("#canDrillThrough").val();

                var url = $("#getStatisticsDataTable").val();

                tblStatistics = $("#tblStatisticsTable").dataTable({
                    "sAjaxSource": url,
                    "aLengthMenu": [10, 25, 50, 100],
                    "iDisplayLength": 10,
                    "bFilter": true,
                    "bProcessing": false,
                    "bPaginate": true,
                    "bServerSide": true,
                    "bStateSave": true,
                    //"orderable": false,
                    //"info": false,
                    "sDom": '<"pull-left"><"pull-right">t<"pull-left"l><"pull-right"ip>',
                    "sPaginationType": "full_numbers",
                    "aaSorting": [[0, 'asc']],
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        blockUI("Getting statistics table data...");

                        var newAoData = {
                            sEcho: getAoDataVal(aoData, "sEcho"),
                            iDisplayLength: getAoDataVal(aoData, "iDisplayLength"),
                            iDisplayStart: getAoDataVal(aoData, "iDisplayStart"),
                            sColumnName: getAoDataVal(aoData, "mDataProp_" + getAoDataVal(aoData, "iSortCol_0")),
                            sSortColDir: getAoDataVal(aoData, "sSortDir_0"),
                            sSearch: getAoDataVal(aoData, "sSearch")
                        };

                        $.postJSON(sSource, 
                              newAoData,
                         function (data) {
                            fnCallback(data);
                            $("#Period1Sales").text("$ " + ((data.aaSummaryRow.Period1Sales).toFixed(0)).replace(/(\d)(?=(\d{3})+$)/g, '$1,'));
                            $("#Period2Sales").text("$ " + (data.aaSummaryRow.Period2Sales).toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,'));
                            $("#TotalStartMargin").text((data.aaSummaryRow.StartMargin * 100).toFixed(2) + "%");
                            $("#TotalPrice").text((data.aaSummaryRow.Price * 100).toFixed(3) + "%");
                            $("#TotalCost").text((data.aaSummaryRow.Cost * 100).toFixed(3) + "%");
                            $("#TotalVolume").text((data.aaSummaryRow.Volume * 100).toFixed(3) + "%");
                            $("#TotalProductMix").text((data.aaSummaryRow.ProductMix * 100).toFixed(3) + "%");
                            $("#TotalRebateRate").text((data.aaSummaryRow.RebateRate * 100).toFixed(3) + "%");
                            $("#TotalRebateVol").text((data.aaSummaryRow.RebateVol * 100).toFixed(3) + "%");
                            $("#TotalRebateProdMix").text((data.aaSummaryRow.RebateProdMix * 100).toFixed(3) + "%");
                            $("#TotalTotalImpact").text((data.aaSummaryRow.TotalImpact * 100).toFixed(3) + "%");
                            $("#TotalEndMargin").text((data.aaSummaryRow.EndMargin * 100).toFixed(2) + "%");

                            $.unblockUI();
                        });
                    },

                    "aoColumnDefs": [
                        {
                            "fnRender": function (oObj) {
                                return oObj;
                            },
                        }],

                    "aoColumns": [
                        {
                            "mData": "Result.Description",
                            "mRender": function (data, display, row) {
                                if (canDrillThrough === "True") {
                                    return "<a href='javascript:;' style='max-width: 175px;display: block;overflow: hidden;text-overflow: ellipsis;' >" + row.Description + "</a>" + "<input class='caption-link' type='hidden' value='" + row.Caption + "'/>";
                                }
                                else { return "<span style='max-width: 175px;display: block;overflow: hidden;text-overflow: ellipsis;' >" + row.Description + "</span><input class='caption-link' type='hidden' value='" + row.Caption + "'/>"; }
                            },
                            "sClass": "text-left brd-sepoarator-light",
                            "sWidth": "220px"
                        },
                        {
                            "mData": "Result.Period1Sales",
                            "mRender": function (data, display, row) {
                                return "$  " + row.Period1Sales.toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                            },
                            "sClass": "text-right",
                            "sWidth": "100px"
                        },
                        {
                            "mData": "Result.Period2Sales",
                            "mRender": function (data, display, row) {
                                return "$  " + row.Period2Sales.toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                            },
                            "sClass": "text-right",
                            "sWidth": "100px"
                        },
                        {
                            "mData": "Result.StartMargin",
                            "mRender": function (data, display, row) {
                                return (row.StartMargin * 100).toFixed(2) + " %";
                            },
                            "sClass": "text-right",
                            "sWidth": "100px"
                        },
                        {
                            "mData": "Result.Price",
                            "mRender": function (data, display, row) {
                                return (row.Price * 100).toFixed(3) + " %";
                            },
                            "sClass": "text-right",
                            "sWidth": "100px"
                        },
                        {
                            "mData": "Result.Cost",
                            "mRender": function (data, display, row) {
                                return (row.Cost * 100).toFixed(3) + " %";
                            },
                            "sClass": "text-right",
                            "sWidth": "100px"
                        },
                        {
                            "mData": "Result.Volume",
                            "mRender": function (data, display, row) {
                                return (row.Volume * 100).toFixed(3) + " %";
                            },
                            "sClass": "text-right",
                            "sWidth": "100px"
                        },
                        {
                            "mData": "Result.ProductMix",
                            "mRender": function (data, display, row) {
                                return (row.ProductMix * 100).toFixed(3) + " %";
                            },
                            "sClass": "text-right",
                            "sWidth": "110px"
                        },
                        {
                            "mData": "Result.RebateRate",
                            "mRender": function (data, display, row) {
                                return (row.RebateRate * 100).toFixed(3) + " %";
                            },
                            "sClass": "text-right",
                            "sWidth": "110px"
                        },
                        {
                            "mData": "Result.RebateVol",
                            "mRender": function (data, display, row) {
                                return (row.RebateVol * 100).toFixed(3) + " %";
                            },
                            "sClass": "text-right",
                            "sWidth": "110px"
                        },
                        {
                            "mData": "Result.RebateProdMix",
                            "mRender": function (data, display, row) {
                                return (row.RebateProdMix * 100).toFixed(3) + " %";
                            },
                            "sClass": "text-right",
                            "sWidth": "110px"
                        },
                        {
                            "mData": "Result.TotalImpact",
                            "mRender": function (data, display, row) {
                                return (row.TotalImpact * 100).toFixed(3) + " %";
                            },
                            "sClass": "text-right",
                            "sWidth": "100px"
                        },
                        {
                            "mData": "Result.EndMargin",
                            "mRender": function (data, display, row) {
                                return (row.EndMargin * 100).toFixed(2) + " %";
                            },
                            "sClass": "text-right",
                            "sWidth": "110px"
                        }
                    ],
                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        priceClass = aData.Price > 0 ? "arrow-up" : aData.Price < 0 ? "arrow-down" : "";
                        costClass = aData.Cost > 0 ? "arrow-up" : aData.Cost < 0 ? "arrow-down" : "";
                        volumeClass = aData.Volume > 0 ? "arrow-up" : aData.Volume < 0 ? "arrow-down" : "";
                        productMixClass = aData.ProductMix > 0 ? "arrow-up" : aData.ProductMix < 0 ? "arrow-down" : "";
                        rebateRateClass = aData.RebateRate > 0 ? "arrow-up" : aData.RebateRate < 0 ? "arrow-down" : "";
                        rebateVolClass = aData.RebateVol > 0 ? "arrow-up" : aData.RebateVol < 0 ? "arrow-down" : "";
                        rebateProdMixClass = aData.RebateProdMix > 0 ? "arrow-up" : aData.RebateProdMix < 0 ? "arrow-down" : "";
                        totalImpactClass = aData.TotalImpact > 0 ? "arrow-up" : aData.TotalImpact < 0 ? "arrow-down" : "";

                        if (aData.Description.length > 25) {
                            $('td:eq(0)', nRow).prop("title", aData.Description);
                        }
                        $('td:eq(4)', nRow).addClass(priceClass);
                        $('td:eq(5)', nRow).addClass(costClass);
                        $('td:eq(6)', nRow).addClass(volumeClass);
                        $('td:eq(7)', nRow).addClass(productMixClass);
                        $('td:eq(8)', nRow).addClass(rebateRateClass);
                        $('td:eq(9)', nRow).addClass(rebateVolClass);
                        $('td:eq(10)', nRow).addClass(rebateProdMixClass);
                        $('td:eq(11)', nRow).addClass(totalImpactClass);
                    }
                });
            },

            DrawDataBridgeTable: function () {

                var url = $("#getDataBridgeTable").val();

                if ($.fn.dataTable.isDataTable('#tblDataBridgeTable')) {
                    $(".getDatabridge").hide();
                    $(".downloadRawDataBridgeXlsx").show();
                    $(".data-bridge .middle-content").slideDown(500);
                    $.unblockUI();
                }
                else {
                    tblStatistics = $("#tblDataBridgeTable").dataTable({
                        "sAjaxSource": url,
                        "ordering": false,
                        //"aLengthMenu": [10, 25, 50, 100],
                        //"iDisplayLength": 10,
                        "bFilter": false,
                        bSort: false,
                        "bProcessing": false,
                        "bPaginate": false,
                        "bServerSide": true,
                        "bStateSave": true,
                        bInfo: false,
                        "sDom": '<"pull-left"><"pull-right">t<"pull-left"l><"pull-right"ip>',
                        "sPaginationType": "full_numbers",
                        "aaSorting": [[0, 'asc']],
                        "fnServerData": function (sSource, aoData, fnCallback) {
                            $.ajaxSetup({ timeout: 5000000 });
                            $.postJSON(sSource, {}, function (data) {
                                data.aaData = data.Items;

                                fnCallback(data);

                                $(".getDatabridge").hide();
                                $(".downloadRawDataBridgeXlsx").show();
                                $(".data-bridge .middle-content").show();

                                $.unblockUI();
                            });
                        },

                        "aoColumnDefs": [
                            {
                                "fnRender": function (oObj) {
                                    return oObj;
                                },
                            }],

                        "aoColumns": [
                            {
                                "mData": "Result.Description",
                                "mRender": function (data, display, row) {
                                    return row.Caption;
                                },
                                "sClass": "text-left brd-sepoarator-light",
                                "sWidth": "129px"
                            },
                            {
                                "mData": "Result.Period1Sales",
                                "mRender": function (data, display, row) {
                                    return "$  " + row.Period1Sales.toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                                },
                                "sClass": "text-right",
                                "sWidth": "69px"
                            },
                            {
                                "mData": "Result.Period1RebatedCost",
                                "mRender": function (data, display, row) {
                                    return "$  " + row.Period1RebatedCost.toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                                },
                                "sClass": "text-right",
                                "sWidth": "69px"
                            },
                            {
                                "mData": "Result.StartMargin",
                                "mRender": function (data, display, row) {
                                    return (row.StartMargin * 100).toFixed(2) + " %";
                                },
                                "sClass": "text-right",
                                "sWidth": "90px"
                            },
                            {
                                "mRender": function (data, display, row) {
                                    return "";
                                },
                                "sWidth": "51px"
                            },
                            {
                                "mRender": function (data, display, row) {
                                    return "";
                                },
                                "sWidth": "60px"
                            },
                            {
                                "mRender": function (data, display, row) {
                                    return "";
                                },
                                "sWidth": "63px"
                            },
                            {
                                "mRender": function (data, display, row) {
                                    return "";
                                },
                                "sWidth": "84px"
                            },
                            {
                                "mRender": function (data, display, row) {
                                    return "";
                                },
                                "sWidth": "81px"
                            },
                            {
                                "mRender": function (data, display, row) {
                                    return "";
                                },
                                "sWidth": "75px"
                            },
                            {
                                "mData": "Result.Period2Sales",
                                "mRender": function (data, display, row) {
                                    return "$  " + row.Period2Sales.toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                                },
                                "sClass": "text-right",
                                "sWidth": "106px"
                            },
                            {
                                "mData": "Result.Period2RebatedCost",
                                "mRender": function (data, display, row) {
                                    return "$  " + row.Period2RebatedCost.toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                                },
                                "sClass": "text-right",
                                "sWidth": "87px"
                            },
                            {
                                "mData": "Items.EndMargin",
                                "mRender": function (data, display, row) {
                                    return (row.EndMargin * 100).toFixed(2) + " %";
                                },
                                "sClass": "text-right",
                                "sWidth": "78px"
                            }
                        ],
                        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                            var tblDataBridgeTable = $('#tblDataBridgeTable').dataTable();
                            var data = tblDataBridgeTable.fnGetData();

                            if (iDisplayIndex == 0 || iDisplayIndex == data.length - 1) {
                                $(nRow).css("font-weight", "bold");
                            } else {
                                $(nRow).find("td:first-child").attr('style', 'padding-left: 30px !important');
                            }
                        }
                    });
                }


            },
        },

        Events: {

            OnChangeUploadData: function () {
                $("#productsUpload").change(function () {
                    $("#productsFile").text($(this).val().split('\\').pop());
                    $("#productsFile").attr("title", $(this).val());
                    $("#productsError").hide();

                    var uploadButton = $(this).parent().find(".button-red");
                    uploadButton.val(uploadButton.val().replace("Upload", "Replace"));
                });

                $("#locationsUpload").change(function () {
                    $("#locationsFile").text($(this).val().split('\\').pop());
                    $("#locationsFile").attr("title", $(this).val());
                    $("#locationsError").hide();

                    var uploadButton = $(this).parent().find(".button-red");
                    uploadButton.val(uploadButton.val().replace("Upload", "Replace"));
                });

                $("#salesUpload").change(function () {
                    $("#salesFile").text($(this).val().split('\\').pop());
                    $("#salesFile").attr(("title", $(this).val()));
                    $("#salesError").hide();

                    var uploadButton = $(this).parent().find(".button-red");
                    uploadButton.val(uploadButton.val().replace("Upload", "Replace"));
                });


                if ($("#showChangeData").val() == "True") {
                    $("#filterSetupDialog").modal("show");
                }

                if ($("#successUpload").val() == "True") {
                    $("#okDialog .modal-title").text("Success");
                    $("#okDialog .modal-body").text("Data uploaded successfully.");
                    $("#okDialog").modal("show");
                } else if ($("#successUpload").val() == "False") {
                    $("#okDialog .modal-title").text("Error");
                    $("#okDialog .modal-body").text("There was an error uploading data.");
                    $("#okDialog").modal("show");
                }

            },


            OnDownloadExcelData: function () {
                $(".downloadExcel").click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();

                    $("#submitDownloadType").val($(this).parent().find("input[type=hidden]").val());
                    $("#downloadForm").submit();

                });
            },

            OnCollapsedTabClick: function () {
                $(".data-bridge .collapsed-tab").click(function () {
                    $(this).parents(".middle-content").slideUp(500, function () {
                        $(".getDatabridge").show();
                        $(".downloadRawDataBridgeXlsx").hide();
                    });
                });
            },

            OnDataBridgeButtonClick: function () {
                $(".getDatabridge").click(function () {
                    $.blockUI("Getting Data Bridge data...");
                    Private.Methods.DrawDataBridgeTable();
                });
            },

            ApplyCheckedNodes: function () {
                var treeArrayLocationItems = [];
                var treeArrayProductsItems = [];


                $(".apply-checked-nodes").on("click", function () {
                    $.blockUI("Getting statistics data...");
                    var treeData = $('#jstree3').jstree(true).get_json('#', { flat: false });

                    treeArrayProductsItems = [];
                    $.each(treeData, function (i) {
                        Public.ChildrenProducts(treeData[i]);
                        treeArrayProductsItems.push(Public.ChildrenProducts(treeData[i]));
                    });

                    treeArrayLocationItems = [];
                    $.each(treeData, function (i) {
                        Public.ChildrenLocations(treeData[i]);
                        treeArrayLocationItems.push(Public.ChildrenLocations(treeData[i]));
                    });

                    //Ajax to controller action
                    var applyTableFilters = $("#applyFilter").val();
                    $.ajax({
                        type: "POST",
                        url: applyTableFilters,
                        data: {
                            products: [treeArrayProductsItems[1]],
                            locations: [treeArrayLocationItems[0]],
                        },
                        success: function (data) {

                            $.unblockUI();

                            if (data.Success) {
                                location.reload();
                            } else {

                                $.unblockUI();
                                $(".filter-error-info").show().text(data.ErrorMessage);
                                setTimeout(function () {
                                    $(".filter-error-info").hide().text('');
                                }, 5000);
                            }
                        },
                        error: function () {

                            $.unblockUI();
                            $(".filter-error-info").show().text("Error sending data!");
                            setTimeout(function () {
                                $(".filter-error-info").hide().text('');
                            }, 5000);
                        }
                    });
                });
            },

            UnApplyCheckedNodes: function () {

                $(".reset-checked-nodes").on("click", function () {
                    $.blockUI("Getting statistics data...");

                    //Ajax to controller action
                    var unapplyTableFilters = $("#unapplyFilter").val();
                    $.ajax({
                        type: "POST",
                        url: unapplyTableFilters,
                        success: function (data) {

                            $.unblockUI();

                            if (data.Success) {
                                location.reload();
                            } else {

                                $.unblockUI();
                                $(".filter-error-info").show().text(data.ErrorMessage);
                                setTimeout(function () {
                                    $(".filter-error-info").hide().text('');
                                }, 5000);
                            }
                        },
                        error: function () {

                            $.unblockUI();
                            $(".filter-error-info").show().text("Error sending data!");
                            setTimeout(function () {
                                $(".filter-error-info").hide().text('');
                            }, 5000);
                        }
                    });
                });
            },

            //PrefillData: function () {
            //    $(".prefill-data").on("click", function () {
            //        $(".first-period .datepicker.from").val("02/01/2016");
            //        $(".first-period .datepicker.to").val("02/02/2016");
            //        $(".second-period .datepicker.from").val("04/01/2016");
            //        $(".second-period .datepicker.to").val("04/30/2016");
            //        $("#jstree").jstree("select_node", "j1_3");
            //        $("#jstree").jstree("select_node", "j1_23");
            //        $("#jstree").jstree("select_node", "j2_72");
            //        $(".location-selection-levels input[type='radio']:last-of-type").prop("checked", true);
            //        $(".product-selection-levels input[type='radio']:last-of-type").prop("checked", true);
            //        $(".apply-selected-filters").prop("disabled", false);
            //    });
            //},

        },
    };

    var Public = {
        ChildrenProducts: function (node) {
            var obj = {
                "Checked": node.state.selected,
                "Code": node.data.code,
                "RowId": node.data.rowid,
                "FilterType": node.data.productfiltertype,
                "Description": node.data.description,
                "Items": []
            };

            if (node.children.length > 0) {
                $.each(node.children, function (i) {

                    var childrenObj = Public.ChildrenProducts(this);

                    if (childrenObj.Checked === true) {
                        obj.Checked = true;
                    }

                    obj.Items.push(childrenObj);
                });
            }

            return obj;
        },

        ChildrenLocations: function (node) {
            var obj = {
                "Checked": node.state.selected,
                "Code": node.data.code,
                "FilterType": node.data.locationfiltertype,
                "RowId": node.data.rowid,
                "Description": node.data.description,
                "Items": []
            };

            if (node.children.length > 0) {
                $.each(node.children, function (i) {

                    var childrenObj = Public.ChildrenLocations(this);

                    if (childrenObj.Checked === true) {
                        obj.Checked = true;
                    }

                    obj.Items.push(childrenObj);
                });
            }

            return obj;
        },

        GetMinLevel: function () {
            /*Create instance of tree to get level from each node*/
            var instLocation = $("#jstree").jstree({
                'contextmenu': {
                    "items": function ($node) {
                        var tree = $("#jstree").jstree(true);
                        return {
                            create: {
                                'label': "Create",
                                'action': function (obj) {
                                    $node = tree.create_node($node);
                                    tree.edit($node);
                                }
                            },
                        };
                    }
                }
            });

            var instProduct = $("#jstree1").jstree({
                'contextmenu': {
                    "items": function ($node) {
                        var tree = $("#jstree1").jstree(true);
                        return {
                            create: {
                                'label': "Create",
                                'action': function (obj) {
                                    $node = tree.create_node($node);
                                    tree.edit($node);
                                }
                            },
                        };
                    }
                }
            });

            /*Count selected nodes to find level from*/
            var selectedLocationNodes = $("#jstree").jstree(true).get_selected();
            var selectedProductNodes = $("#jstree1").jstree(true).get_selected();

            /*create array of levels from selected nodes*/
            var levelLocation = [];
            var levelProduct = [];

            for (var i = 0; i < selectedLocationNodes.length; i++) {
                levelLocation.push(instLocation.get_path(selectedLocationNodes[i]).length - 1);
            }

            for (var i = 0; i < selectedProductNodes.length; i++) {
                levelProduct.push(instProduct.get_path(selectedProductNodes[i]).length - 1);
            }

            /*Set initial level to deepest*/
            var minLocationLevel = 5;
            var minProductLevel = 5;

            for (var i = 0; i < levelLocation.length; i++) {
                if (minLocationLevel > levelLocation[i]) {
                    minLocationLevel = levelLocation[i];
                }
            }

            for (var i = 0; i < levelProduct.length; i++) {
                if (minProductLevel > levelProduct[i]) {
                    minProductLevel = levelProduct[i];
                }
            }

            Tree.DisableHierachyFilters(minLocationLevel, minProductLevel);
        },

        Init: function () {
            Private.Methods.InitializeFirstTree();
            Private.Methods.InitializeSecondTree();
            Private.Methods.InitializeThirdTree();
            Private.Methods.InitializeFourthTree();
            Private.Methods.OnloadAccordion();
            Private.Events.ApplyCheckedNodes();
            Private.Events.UnApplyCheckedNodes();
            //Private.Events.PrefillData();
            Private.Methods.LoadTable();
            Private.Events.OnDataBridgeButtonClick();
            Private.Events.OnCollapsedTabClick();
            Private.Events.OnChangeUploadData();
            Private.Events.OnDownloadExcelData();

        }
    };
    return Public;
}();

Main.Init();