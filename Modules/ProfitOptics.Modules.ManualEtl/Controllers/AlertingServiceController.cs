﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Modules.ManualEtl.Models.Alerting;
using ProfitOptics.Modules.ManualEtl.Services;

namespace ProfitOptics.Modules.ManualEtl.Controllers
{
    [Area("ManualEtl")]
    public class AlertingServiceController : BaseController
    {
        private readonly IManualEtlService _manualEtlService;

        public AlertingServiceController(IManualEtlService manualEtlService)
        {
            _manualEtlService = manualEtlService;
        }

        [HttpPost]
        public async Task<IActionResult> SendPortalQaCheckReport()
        {
            if (!HttpContext.Request.IsLocal())
            {
                return Unauthorized();
            }

            var responseString = new StreamReader(Request.Body)
                    .ReadToEnd();

            var model = JsonConvert.DeserializeObject<DbCheckViewModel>(responseString);

            if (model == null)
            {
                return Json(false);
            }

            var result = false;

            if (model.DbTableStatuses.Any() || model.EtlStatuses.Any())
            {
                try
                {
                    var emailBody = await this.RenderViewAsync("_DbCheckEmailTemplate", model);
                    var emails = _manualEtlService.GetAdminEmails();

                    result = await _manualEtlService.SendPortalQaCheckReport(emails, emailBody);
                }
                catch (Exception e)
                {
                    result = false;
                }
            }

            return Json(result);
        }
    }
}