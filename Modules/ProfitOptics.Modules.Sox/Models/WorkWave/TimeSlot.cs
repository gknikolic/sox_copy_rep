﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class TimeSlot
	{
		[JsonProperty("pickupTimeWindows")]
		public List<TimeWindow> PickupTimeWindows { get; set; }
		[JsonProperty("deliveryTimeWindows")]
		public List<TimeWindow> DeliveryTimeWindows { get; set; }
		[JsonProperty("date")]
		public string Date { get; set; }
	}
}
