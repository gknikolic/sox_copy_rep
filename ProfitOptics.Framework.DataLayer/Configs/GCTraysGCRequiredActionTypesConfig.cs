﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class GCTraysGCRequiredActionTypesConfig : IEntityTypeConfiguration<GCTraysGCRequiredActionTypes>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCTraysGCRequiredActionTypes> builder)
        {
            builder.ToTable("GCTrayGCRequiredActionTypes");
            builder.HasKey(entity => entity.Id);
        }
    }
}
