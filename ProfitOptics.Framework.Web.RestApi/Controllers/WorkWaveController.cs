﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Intuit.Ipp.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProfitOptics.Modules.Sox.Infrastructure;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Models.WorkWave;
using ProfitOptics.Modules.Sox.Services;
using Serilog;

namespace ProfitOptics.Framework.Web.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkWaveController : ControllerBase
    {
        IWorkWaveClient workWaveClient;
        IWorkWaveService workWaveService;
        ISoxLogger soxLogger;
        private readonly ILogger<WorkWaveController> _nlogger;

        public WorkWaveController(IWorkWaveClient workWaveClient,
                                  IWorkWaveService workWaveService,
                                  ISoxLogger soxLogger,
                                  ILogger<WorkWaveController> nlogger)
        {
            this.workWaveClient = workWaveClient;
            this.workWaveService = workWaveService;
            this.soxLogger = soxLogger;
            this._nlogger = nlogger;
        }



        /// <summary>
        /// Test method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("test")]
        public async Task<StatusCodeResult> Test()
        {
            try
            {
                //await workWaveClient.GetOrdersFromWorWaveAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }

        [HttpPost]
        [Route("response")]
        public async Task<StatusCodeResult> WorkWaveResponseAsync(/*[FromBody] CallbackResponse response*/ object res)
        {
            try
            {
                var response = JsonConvert.DeserializeObject<CallbackResponse>(res.ToString());

                await workWaveClient.HandleWorkwaweResponseAsync(response);

                soxLogger.WWInformation(res.ToString());
                _nlogger.LogInformation(res.ToString());
                var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

                return Ok();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        [Route("setcallback")]
        public async Task<StatusCodeResult> SetCallbackUrlAsync([FromBody] SetCallbackUrl model)
        {
            try
            {
                var result = await workWaveClient.SetCallbackUrl(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }

        [HttpGet]
        [Route("createorder/{caseId}")]
        public async Task<StatusCodeResult> CreateOrderAsync(int caseId)
        {
            try
            {
                var result = await workWaveService.AddOrderToWWAsync(caseId);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }

        [HttpGet]
        [Route("territories")]
        public async Task<StatusCodeResult> GetTerritoriesAsync()
        {
            try
            {
                var result = await workWaveClient.GetTerritories();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }
    }
}
