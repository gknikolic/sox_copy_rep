﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCTrayConfig : IEntityTypeConfiguration<GCTray>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCTray> builder)
        {
            builder.ToTable("GCTray");
            builder.HasKey(entity => entity.Id);

            //builder.HasOne<CTCaseContainer>(x => x.CTCaseContainer)
            //    .WithOne(x => x.GCTray)
            //    .HasForeignKey<GCTray>(x => x.CTCaseContainerId)
            //    .HasConstraintName("FK_GCTray_CTCaseContainer");
        }
    }
}
