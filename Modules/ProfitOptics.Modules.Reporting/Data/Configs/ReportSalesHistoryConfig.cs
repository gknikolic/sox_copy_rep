﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportSalesHistoryConfig : IEntityTypeConfiguration<ReportSalesHistory>
    {
        public void Configure(EntityTypeBuilder<ReportSalesHistory> builder)
        {
            builder.ToTable(nameof(ReportSalesHistory));
            builder.HasKey(s => s.Id);

            builder.Property(s => s.UnitPrice).HasColumnType("decimal(19,4)");
            builder.Property(s => s.UnitCost).HasColumnType("decimal(19,4)");
            builder.Property(s => s.ExtPrice).HasColumnType("decimal(19,4)").IsRequired();
            builder.Property(s => s.ExtCost).HasColumnType("decimal(19,4)").IsRequired();
            builder.Property(s => s.Qty).HasColumnType("decimal(19,4)").IsRequired();
            builder.Property(s => s.RebateRate).HasColumnType("decimal(19,4)").IsRequired();
            builder.Property(s => s.StartDate).IsRequired();
            builder.Property(s => s.SOPType).HasMaxLength(20);
            builder.Property(s => s.SOPNumber).HasMaxLength(30);
            builder.Property(s => s.Comment1).HasMaxLength(100);
            builder.Property(s => s.Comment2).HasMaxLength(100);
            builder.Property(s => s.Comment3).HasMaxLength(100);
            builder.Property(s => s.Comment4).HasMaxLength(100);
        }
    }
}
