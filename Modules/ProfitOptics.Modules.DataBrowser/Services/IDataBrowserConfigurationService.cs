﻿using System.Collections.Generic;
using System.Configuration;
using ProfitOptics.Modules.DataBrowser.Models;

namespace ProfitOptics.Modules.DataBrowser.Services
{
    public interface IDataBrowserConfigurationService
    {
        ConnectionStringSettings GetConnectionString();

        IEnumerable<string> GetAllowedDatabases();

        int GetMaxAllowedDownloadRecords();

        int GetMinWarningDownloadRecords();

        IEnumerable<string> GetAllowedTablesForDatabase(string database);

        IEnumerable<TableModel> GetTablesFromDatabase(string database);
    }
}
