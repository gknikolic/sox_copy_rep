﻿namespace ProfitOptics.Framework.Web.Framework.ServiceResults
{
    public class SingleLogoutServiceResult : AuthenticationServiceResult
    {
        public string RedirectUrl { get; set; }

        public new static SingleLogoutServiceResult Failed(string redirectUrl) => new SingleLogoutServiceResult
        {
            Status = AuthenticationServiceResultStatusCode.Failed,
            RedirectUrl = redirectUrl,
            Error = null
        };

        public new static SingleLogoutServiceResult Success() => new SingleLogoutServiceResult
        {
            Error = null,
            RedirectUrl = null,
            Status = AuthenticationServiceResultStatusCode.Success
        };
    }
}
