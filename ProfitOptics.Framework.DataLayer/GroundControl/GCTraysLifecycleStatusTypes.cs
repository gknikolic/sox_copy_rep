﻿using ProfitOptics.Framework.DataLayer.WorkWave;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCTraysLifecycleStatusTypes
    {
        public int Id { get; set; }
        public int LifecycleStatusTypeId { get; set; }
        public int GCTrayId { get; set; }
        public DateTime Timestamp { get; set; }
        public bool IsActiveStatus { get; set; }
        public string User { get; set; }
        public int? WWPodContainerId { get; set; }
        public DateTime TimestampOriginal { get; set; }

		public GCLifecycleStatusType LifecycleStatusType { get; set; }
        public GCTray GCTray { get; set; }
        public WWPodContainer WWPodContainer { get; set; }
    }
}
