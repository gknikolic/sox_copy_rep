﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class CaseDetailsModel
	{
        public GCCaseModel GCCase { get; set; }
   
        public List<LifecycleStatusModel> LifecycleStatuses { get; set; }
		public List<LifecycleCategory> LifecycleCategories { get; set; }
        public List<LifecycleCategory> PreviousLifecycleCategories { get; set; }
        public int MaxOrderNumber { get; set; }
        public string Color { get; set; }
		//public System.Array LifecycleStatuses { get; set; }
		public List<NoteGroup> NoteGroups { get; set; }
        public string QBCusomerExternalUrl { get; set; }

        public AddColorCommentModal AddColorCommentModal { get; set; }
		public CaseDetailsTraysModel CaseDetailsTraysModel { get; set; }
	}
}
