﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
	[Table("WWRoute")]
	public partial class WWRoute
	{

		public int Id { get; set; }

		public string WWRouteId { get; set; }

		public int Revision { get; set; }

		public DateTime Date { get; set; }

		public int VehicleId { get; set; }

		public int? DriverId { get; set; }

		//public List<int> VehicleViolations { get; set; }
		public virtual ICollection<WWRouteStep> Steps { get; set; }

		public virtual WWVehicle Vehicle { get; set; }

		public virtual WWDriver Driver { get; set; }
	}
}
