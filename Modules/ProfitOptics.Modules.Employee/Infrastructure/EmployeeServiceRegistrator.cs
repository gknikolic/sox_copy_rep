﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.Employee.Data;
using ProfitOptics.Modules.Employee.Factories;
using ProfitOptics.Modules.Employee.Services;

namespace ProfitOptics.Modules.Employee.Infrastructure
{
    public sealed class EmployeeServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<ProfitOptics.Framework.DataLayer.Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IEmployeeFactory, EmployeeFactory>();
        }
    }
}
