﻿using System;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class DashboardDetailedHourPreviewItemModel
	{
		public string ColorClass { get; set; }
		public string CaseReference { get; set; }
		public int CaseId { get; set; }
		public DateTime DueTimestamp { get; set; }
		public string CustomerName { get; set; }
	}


}
