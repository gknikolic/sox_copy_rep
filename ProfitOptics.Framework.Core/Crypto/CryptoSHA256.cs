﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ProfitOptics.Framework.Core.Crypto
{
    public class CryptoSHA256
    {
        public static string HashFromString(string customerString)
        {
            var sb = new StringBuilder();

            using (var hash = SHA256.Create())
            {
                var enc = Encoding.UTF8;
                var result = hash.ComputeHash(enc.GetBytes(customerString));

                foreach (Byte b in result)
                    sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }
    }
}
