﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueTransitionsResult
    {
        public string expand { get; set; }

        public List<IssueTransitions> transitions { get; set; }
    }
}