﻿using ProfitOptics.Framework.DataLayer;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public interface ICTProductService
    {
        //void AddOrEditCTProducts(List<CTProduct> ctProducts);
        void AddOrEditCTVendors(List<CTVendor> ctVendors);
    }
}
