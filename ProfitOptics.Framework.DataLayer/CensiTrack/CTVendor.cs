﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
	public class CTVendor
    {
        public int Id { get; set; }
        public long LegacyId { get; set; }
        public string Name { get; set; }
        public string ContactName { get; set; }
        public int VendorTypes { get; set; }
        public string FriendlyName { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
    }
}
