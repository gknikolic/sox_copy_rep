USE [Sox]

ALTER TABLE [dbo].[GCLifecycleStatusType]  WITH CHECK ADD CONSTRAINT [FK_GCLifecycleStatusType_WWPodContainer] FOREIGN KEY([WWPodContainerId])
REFERENCES [dbo].[WWPodContainer] ([Id]);

GO
