using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.Web.Areas.AuditLog.Data.Domain
{
    [Table("POActionAuditFriendlyName")]
    public partial class POActionAuditFriendlyName
    {
        public int Id { get; set; }

        public string ReflectedType { get; set; }

        public string ReflectedActionName { get; set; }

        public string FriendlyName { get; set; }
    }
}