﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer
{
    public partial class GCSet
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public int CaseId { get; set; }

        public virtual GCCase Case { get; set; }
        public virtual ICollection<GCContainer> Containers { get; set; }

    }
}
