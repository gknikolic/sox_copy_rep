﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using System;
using System.Linq;
using System.Reflection;
using System.Text;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.Anotations;
using Microsoft.Extensions.DependencyInjection;

namespace Microsoft.AspNetCore.Mvc
{
    public static class AjaxRenderer
    {
        public const string ModulePrefix = "ProfitOptics.Modules.";

        public static IHtmlContent RenderAjaxActions(this IHtmlHelper htmlHelper)
        {
            var sb = new StringBuilder();

            var controllerName = htmlHelper.ViewContext.RouteData.Values["controller"] + "Controller";

            var moduleName = ModulePrefix + htmlHelper.ViewContext.RouteData.Values["area"];

            var asm = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(item => item.GetName().Name == moduleName) ?? Assembly.GetExecutingAssembly();
            
            var types = asm.GetTypes()
                .Where(type => typeof(Controller).IsAssignableFrom(type) && type.Name == controllerName)
                .SelectMany(type => type.GetMethods())
                .Where(method => method.IsPublic && !method.IsDefined(typeof(NonActionAttribute)));

            var methods = types.ToArray();

            var found = false;
            sb.AppendLine("<script>");
            foreach (var mInfo in methods)
            {
                var attrs = Attribute.GetCustomAttributes(mInfo, typeof(Attribute), false);

                foreach (var attr in attrs)
                {
                    var ajaxCallAttribute = attr as AjaxCallAttribute;
                    if (ajaxCallAttribute != null)
                    {
                        var act = RenderActionToString(mInfo, htmlHelper.ViewContext, ajaxCallAttribute.Method, ajaxCallAttribute.DataType);
                        sb.AppendLine(act);
                        found = true;

                    }
                }

            }
            sb.AppendLine("</script>");

            var contentBuilder = new HtmlContentBuilder();

            return found ? contentBuilder.SetHtmlContent(sb.ToString()) : contentBuilder.SetHtmlContent(string.Empty);
        }

        public static string RenderActionToString(MethodInfo mInfo, ViewContext viewContext, string method, string dataType)
        {
            StringBuilder sb = new StringBuilder();

            var areaName = viewContext.RouteData.Values["area"] != null ? viewContext.RouteData.Values["area"].ToString() : "";
            var controllerName = viewContext.RouteData.Values["controller"].ToString();

            Type cType = mInfo.ReflectedType;
            string cNs = mInfo.ReflectedType.Namespace;
            string cName = mInfo.ReflectedType.Name;
            string name = mInfo.Name;

            var urlHelperFactory = viewContext.HttpContext.RequestServices.GetRequiredService<IUrlHelperFactory>();
            var urlHelper = urlHelperFactory.GetUrlHelper(viewContext);

            var url = urlHelper.Action(name, controllerName, new { Area = areaName });

            string aUrl = url;

            var config = $"{{dataType:'{dataType}', type: '{method}'}}";

            sb.AppendLine("app." + name + "=function(data) {");
            sb.AppendLine("return app.postJson('" + aUrl + "',data, " + config + ");");
            sb.AppendLine("}");

            return sb.ToString();
        }
    }
}
