﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using Serilog;

namespace ProfitOptics.Framework.Web.Framework.Helpers.CustomAttributes
{
    public class HandleExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            var exception = new Exception(filterContext.Exception.Message);
            exception.Ship(filterContext.HttpContext);

            var isAjaxRequest = filterContext.HttpContext.Request != null && filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";

            if (isAjaxRequest)
            {
                var response = filterContext.HttpContext.Response;

                response.Clear();
                response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                var innerEx = filterContext.Exception.InnerException != null ? " - " + filterContext.Exception.InnerException.Message : "";
                response.ContentType = "application/json";
                filterContext.ExceptionHandled = true;

                var Log = new LoggerConfiguration()
                                 //.Enrich.WithExceptionDetails()
                                 .MinimumLevel.Debug()
                                 .WriteTo.File(@"AppData\ExceptionLogs\log-.log", rollingInterval: RollingInterval.Day)
                                 .CreateLogger();

                Log.Error(filterContext.Exception, "");


                //var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

                response.WriteAsync(JsonConvert.SerializeObject(new { Status = "Error", Message = filterContext.Exception.Message + innerEx }));
            }
        }
    }
}