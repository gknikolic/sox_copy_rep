﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCContainerTypeConfig : IEntityTypeConfiguration<GCContainerType>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCContainerType> builder)
        {
            builder.ToTable("GCContainerType");
            builder.HasKey(entity => entity.Id);
        }
    }
}
