﻿using System;
using System.Linq;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Factories;
using ProfitOptics.Modules.Reporting.Models.DailyTrend;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Services.DailyTrend
{
    public class DailyTrendDataService : IDailyTrendDataService
    {
        private readonly IReportFilterModelFactory _reportFilterModelFactory;
        private readonly ISalesHistoryService _salesHistoryService;
        private readonly Entities _context;

        public DailyTrendDataService(IReportFilterModelFactory reportFilterModelFactory, ISalesHistoryService salesHistoryService, Entities context)
        {
            _reportFilterModelFactory = reportFilterModelFactory;
            _salesHistoryService = salesHistoryService;
            _context = context;
        }

        /// <inheritdoc />
        public PagedList<IGrouping<DateTime, ReportSalesHistory>> GetSalesHistoriesGroupedByDate(int userId, DateRangeDto dateRange)
        {
            ReportFilterModel filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            var sales = _salesHistoryService
                .GetFilteredSalesHistories(filter)
                .Where(s => s.InvoiceDate >= dateRange.StartDate &&
                            s.InvoiceDate <= dateRange.EndDate)
                .ToLookup(s => s.InvoiceDate);

            return new PagedList<IGrouping<DateTime, ReportSalesHistory>>(sales.AsQueryable());
        }

        /// <inheritdoc />
        public PagedList<ReportSalesHistory> GetSalesHistoriesForDate(int userId, IDataTablesRequest request, DateTime date)
        {
            ReportFilterModel filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            IQueryable<ReportSalesHistory> sales = _salesHistoryService
                .GetFilteredSalesHistories(filter)
                .Where(s => s.InvoiceDate >= date &&
                            s.InvoiceDate <= date)
                .OrderByDescending(s => s.InvoiceDate);

            return new PagedList<ReportSalesHistory>(sales, request.Start, request.Length);
        }

        /// <inheritdoc />
        public PagedList<ReportSalesHistory> GetSalesHistoriesForBillToIdAndDate(int userId, IDataTablesRequest request, int billToId, DateTime date)
        {
            if (billToId <= 0)
            {
                return  new PagedList<ReportSalesHistory>();
            }

            ReportFilterModel filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            IQueryable<ReportSalesHistory> sales = _salesHistoryService
                .GetFilteredSalesHistories(filter)
                .Where(s => s.BillToId == billToId &&
                    s.InvoiceDate >= date &&
                    s.InvoiceDate <= date)
                .OrderByDescending(s => s.InvoiceDate);

            return new PagedList<ReportSalesHistory>(sales, request.Start, request.Length);
        }

        /// <inheritdoc />
        public PagedList<IGrouping<DrillThroughTypeKey, ReportSalesHistory>> GetSalesHistoriesGroupedByType(int userId, IDataTablesRequest request,
            DailyTrendDrillThroughType drillThroughType, DateTime date)
        {
            ReportFilterModel filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            IQueryable<ReportSalesHistory> filteredSalesHistories = _salesHistoryService.GetFilteredSalesHistories(filter);

            IQueryable<IGrouping<DrillThroughTypeKey, ReportSalesHistory>> sales;
            switch (drillThroughType)
            {
                case DailyTrendDrillThroughType.BillTo:
                    sales = from salesHistory in filteredSalesHistories
                        from billTo in _context.ReportBillTo.Where(b => b.Id == salesHistory.BillToId)
                        where salesHistory.InvoiceDate >= date && salesHistory.InvoiceDate <= date
                        group salesHistory by new DrillThroughTypeKey
                        {
                            GroupCode = billTo.BillToNum,
                            GroupName = billTo.BillToName
                        };
                    break;
                case DailyTrendDrillThroughType.ShipTo:
                    sales = from salesHistory in filteredSalesHistories
                        from shipTo in _context.ReportShipTo.Where(s => s.Id == salesHistory.ShipToId)
                        where salesHistory.InvoiceDate >= date && salesHistory.InvoiceDate <= date
                        group salesHistory by new DrillThroughTypeKey
                        {
                            GroupCode = shipTo.ShipToNum,
                            GroupName = shipTo.ShipToName
                        };
                    break;
                case DailyTrendDrillThroughType.CustomerClass:
                    sales = from salesHistory in filteredSalesHistories
                        from customerClass in _context.ReportCustomerClasses.Where(c => c.Id == salesHistory.CustomerClassId)
                        where salesHistory.InvoiceDate >= date && salesHistory.InvoiceDate <= date
                        group salesHistory by new DrillThroughTypeKey
                        {
                            GroupCode = customerClass.Code,
                            GroupName = customerClass.Name
                        };
                    break;
                case DailyTrendDrillThroughType.ItemCategory:
                    sales = from salesHistory in filteredSalesHistories
                        from itemCategory in _context.ReportItemCategory.Where(i => i.Id == salesHistory.ItemCategoryId)
                        where salesHistory.InvoiceDate >= date && salesHistory.InvoiceDate <= date
                        group salesHistory by new DrillThroughTypeKey
                        {
                            GroupCode = itemCategory.Code,
                            GroupName = itemCategory.Name
                        };
                    break;
                case DailyTrendDrillThroughType.SalesRep:
                    sales = from salesHistory in filteredSalesHistories
                        from salesRep in _context.ReportHierarchySalesRep.Where(s => s.Id == salesHistory.SalesRepId)
                        where salesHistory.InvoiceDate >= date && salesHistory.InvoiceDate <= date
                        group salesHistory by new DrillThroughTypeKey
                        {
                            GroupCode = salesRep.Name,
                            GroupName = salesRep.Name
                        };
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(drillThroughType), drillThroughType, null);
            }
            
            return new PagedList<IGrouping<DrillThroughTypeKey, ReportSalesHistory>>(sales, request.Start, request.Length);
        }

        /// <inheritdoc />
        public ReportBillTo GetBillToForBillToNum(string billToNum)
        {
            if (string.IsNullOrWhiteSpace(billToNum))
            {
                return null;
            }

            return _context.ReportBillTo.SingleOrDefault(billTo => billTo.BillToNum == billToNum);
        }
    }
}