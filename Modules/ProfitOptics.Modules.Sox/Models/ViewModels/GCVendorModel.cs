﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCVendorModel
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public bool Active { get; set; }
        public int? QBVendorId { get; set; }
        [Required]
        [StringLength(2, MinimumLength = 1, ErrorMessage = "Maximum 2 characters")]
        public string Abbreviation { get; set; }

        public int? SelectedCompany { get; set; }
        public IEnumerable<SelectListItem> GCParentCompanies { get; set; }
    }
}
