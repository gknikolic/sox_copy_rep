﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCEstimateLine
    {
        public int Id { get; set; }
        public int GCEstimateId { get; set; }
        public int LineNum { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string DetailType { get; set; }
        public string SalesItemValue { get; set; }
        public string SalesItemName { get; set; }
        public decimal SalesItemUnitPrice { get; set; }
        public int SalesItemQty { get; set; }
        public string SalesItemTaxCodeValue { get; set; }
        public bool DiscountPercentBased { get; set; }
        public decimal DiscountPercent { get; set; }
        public string DiscountAccountName { get; set; }
        public string DiscountAccountValue { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual GCEstimate GCEstimate { get; set; }
        
    }
}
