﻿using System;
using System.Linq;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.DailyTrend;

namespace ProfitOptics.Modules.Reporting.Services.DailyTrend
{
    public interface IDailyTrendDataService
    {
        /// <summary>
        /// Gets the sales histories for the specified parameters group by date.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        PagedList<IGrouping<DateTime, ReportSalesHistory>> GetSalesHistoriesGroupedByDate(
            int userId, DateRangeDto dateRange);

        /// <summary>
        /// Gets the sales histories for the specified date.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        PagedList<ReportSalesHistory> GetSalesHistoriesForDate(int userId, IDataTablesRequest request, DateTime date);

        /// <summary>
        /// Gets the sales histories for the specified date.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="billToId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        PagedList<ReportSalesHistory> GetSalesHistoriesForBillToIdAndDate(int userId, IDataTablesRequest request, int billToId, DateTime date);

        /// <summary>
        /// GetSearchResultModel sales histories grouped by type.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="drillThroughType"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        PagedList<IGrouping<DrillThroughTypeKey, ReportSalesHistory>> GetSalesHistoriesGroupedByType(
            int userId, IDataTablesRequest request, DailyTrendDrillThroughType drillThroughType, DateTime date);

        /// <summary>
        /// Gets the bill to with the specified bill to num.
        /// </summary>
        /// <param name="billToNum"></param>
        /// <returns></returns>
        ReportBillTo GetBillToForBillToNum(string billToNum);
    }
}
