﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;

namespace ProfitOptics.Modules.Reporting.Services.RsmPerformance
{
    public class RsmPerformanceService : IRsmPerformanceService
    {
        #region Fields

        private readonly Entities _context;
        private readonly ReportingSettings _reportingSettings;
        private readonly IRsmMissingRevenueService _missingRevenueService;
        private IEnumerable<string> AllowedItemCategories => _reportingSettings.AllowedItemCategories.Split(',');
        private IEnumerable<string> AllowedSalesChannels => _reportingSettings.AllowedSalesChannels.Split(',');
        private IEnumerable<string> AllowedProductFamilies => _reportingSettings.AllowedProductFamilies.Split(',');
        private IEnumerable<string> AllowedSalesReps => _reportingSettings.AllowedSalesReps.Split(",");
        private IEnumerable<string> AllowedRegions => _reportingSettings.AllowedRegions.Split(",");

        #endregion Fields

        #region Ctor

        public RsmPerformanceService(Entities context, ReportingSettings reportingSettings
        , IRsmMissingRevenueService missingRevenueService)
        {
            _context = context;
            _reportingSettings = reportingSettings;
            _missingRevenueService = missingRevenueService;
        }

        #endregion Ctor

        #region Item Category/Family Name Helpers

        public List<ItemCategoryQuotaPerformanceDataModel> OrderItemsAccordingToAppSetting(List<ItemCategoryQuotaPerformanceDataModel> items,
           MonthDetails monthDetails = null,
           int salesRepId = 0,
           int year = 0,
           int month = 0)
        {
            // Order the items in the resulting list according to the app setting.
            items = AllowedItemCategories
                .Select(name =>
                {
                    ItemCategoryQuotaPerformanceDataModel itemCategory = items.FirstOrDefault(x => x.ItemCategoryName == name);

                    if (itemCategory != null)
                    {
                        return itemCategory;
                    }

                    ItemCategoryQuotaPerformanceDataModel defaultItemCategory = _missingRevenueService.GetItemCategoryQuotaPerformanceModelForSalesRepId(name,
                        monthDetails,
                        salesRepId,
                        year,
                        month);

                    return defaultItemCategory;
                })
                .ToList();

            return items;
        }

        /// <summary>
        /// Adds the totals row for item categories.
        /// </summary>
        /// <param name="itemCategories">The item categories.</param>
        /// <param name="itemCategoryName">The name of the "total" item category.</param>
        /// <returns></returns>
        public List<ItemCategoryQuotaPerformanceDataModel> AddTotalsRowForItemCategory(
            List<ItemCategoryQuotaPerformanceDataModel> itemCategories, string itemCategoryName = "Total")
        {
            if (itemCategories.Any())
            {
                itemCategories.Add(new ItemCategoryQuotaPerformanceDataModel
                {
                    ItemCategoryName = itemCategoryName,
                    MtdActualRevenue = itemCategories.Sum(x => x.MtdActualRevenue),
                    MtdActualUnits = itemCategories.Sum(x => x.MtdActualUnits),
                    MtdTotalMonthQuota = itemCategories.Sum(x => x.MtdTotalMonthQuota),
                    ProjectedAvgRevPerDay = itemCategories.Sum(x => x.ProjectedAvgRevPerDay),
                    ProjectedRevRemainingDays = itemCategories.Sum(x => x.ProjectedRevRemainingDays),
                    YtdActualRevenue = itemCategories.Sum(x => x.YtdActualRevenue),
                    YtdActualUnits = itemCategories.Sum(x => x.YtdActualUnits),
                    YtdQuota = itemCategories.Sum(x => x.YtdQuota)
                });
            }

            return itemCategories;
        }

        #endregion Item Category/Family Name Helpers

        #region Sales Channel/Customer Class Helpers

        public List<SalesChannelQuotaPerformanceDataModel> OrderItemsAccordingToAppSetting(List<SalesChannelQuotaPerformanceDataModel> items,
           MonthDetails monthDetails = null,
           int salesRepId = 0,
           int year = 0,
           int month = 0)
        {
            // Order the items in the resulting list according to the app setting.
            items = AllowedSalesChannels
                .Select(name =>
                {
                    SalesChannelQuotaPerformanceDataModel salesChannel = items.FirstOrDefault(x => x.SalesChannelName == name);

                    if (salesChannel != null)
                    {
                        return salesChannel;
                    }

                    SalesChannelQuotaPerformanceDataModel defaultSalesChannel = _missingRevenueService.GetSalesChannelQuotaPerformanceModelForSalesRepId(name,
                        monthDetails,
                        salesRepId,
                        year,
                        month);

                    return defaultSalesChannel;
                })
                .ToList();

            return items;
        }

        public List<SalesChannelChartData> OrderItemsAccordingToAppSetting(List<SalesChannelChartData> items)
        {
            // Order the items in the resulting list according to the app setting.
            items = AllowedSalesChannels
                .Select(name => items.FirstOrDefault(x => x.SalesChannelName == name) ?? new SalesChannelChartData
                {
                    SalesChannelName = name
                })
                .ToList();

            return items;
        }

        /// <summary>
        /// Adds the totals row for sales channels.
        /// </summary>
        /// <param name="salesChannels">The sales channels.</param>
        /// <param name="channelName">The name of the "total" sales channel.</param>
        /// <returns></returns>
        public List<SalesChannelQuotaPerformanceDataModel> AddTotalsRowForSalesChannel(
            List<SalesChannelQuotaPerformanceDataModel> salesChannels, string channelName = "Total")
        {
            if (salesChannels.Any())
            {
                salesChannels.Add(new SalesChannelQuotaPerformanceDataModel
                {
                    SalesChannelName = channelName,
                    MtdActualRevenue = salesChannels.Sum(x => x.MtdActualRevenue),
                    MtdTotalMonthQuota = salesChannels.Sum(x => x.MtdTotalMonthQuota),
                    MtdDailyQuota = salesChannels.Sum(x => x.MtdDailyQuota),
                    ProjectedAvgRevPerDay = salesChannels.Sum(x => x.ProjectedAvgRevPerDay),
                    ProjectedRevRemainingDays = salesChannels.Sum(x => x.ProjectedRevRemainingDays),
                    YtdActualRevenue = salesChannels.Sum(x => x.YtdActualRevenue),
                    YtdQuota = salesChannels.Sum(x => x.YtdQuota)
                });
            }

            return salesChannels;
        }

        #endregion Sales Channel/Customer Class Helpers

        #region Product Family/Vendor Helpers

        public List<ProductFamilyQuotaPerformanceDataModel> OrderItemsAccordingToAppSetting(List<ProductFamilyQuotaPerformanceDataModel> items,
          MonthDetails monthDetails = null,
          int salesRepId = 0,
          int year = 0,
          int month = 0)
        {
            // Order the items in the resulting list according to the app setting.
            items = AllowedProductFamilies
                .Select(name =>
                {
                    ProductFamilyQuotaPerformanceDataModel productFamily = items.FirstOrDefault(x => x.ProductFamilyName == name);

                    if (productFamily != null)
                    {
                        return productFamily;
                    }

                    ProductFamilyQuotaPerformanceDataModel defaultProductFamily = _missingRevenueService.GetProductFamilyQuotaPerformanceModelForSalesRepId(name,
                        monthDetails,
                        salesRepId,
                        year,
                        month);

                    return defaultProductFamily;
                })
                .ToList();

            return items;
        }

        public List<ProductFamilyChartData> OrderItemsAccordingToAppSetting(List<ProductFamilyChartData> items)
        {
            // Order the items in the resulting list according to the app setting.
            items = AllowedProductFamilies
                .Select(name => items.FirstOrDefault(x => x.ProductFamilyName == name) ?? new ProductFamilyChartData { ProductFamilyName = name })
                .ToList();

            return items;
        }

        /// <summary>
        /// Adds the totals row for product families.
        /// </summary>
        /// <param name="productFamilies">The product families.</param>
        /// <param name="productFamilyName">The "totals" product category name.</param>
        /// <returns></returns>
        public List<ProductFamilyQuotaPerformanceDataModel> AddTotalsRowForProductFamily(List<ProductFamilyQuotaPerformanceDataModel> productFamilies, string productFamilyName = "Total")
        {
            if (productFamilies.Any())
            {
                productFamilies.Add(new ProductFamilyQuotaPerformanceDataModel
                {
                    ProductFamilyName = productFamilyName,
                    MtdActualRevenue = productFamilies.Sum(x => x.MtdActualRevenue),
                    MtdActualUnits = productFamilies.Sum(x => x.MtdActualUnits),
                    MtdTotalMonthQuota = productFamilies.Sum(x => x.MtdTotalMonthQuota),
                    ProjectedAvgRevPerDay = productFamilies.Sum(x => x.ProjectedAvgRevPerDay),
                    ProjectedRevRemainingDays = productFamilies.Sum(x => x.ProjectedRevRemainingDays),
                    YtdActualRevenue = productFamilies.Sum(x => x.YtdActualRevenue),
                    YtdActualUnits = productFamilies.Sum(x => x.YtdActualUnits),
                    YtdQuota = productFamilies.Sum(x => x.YtdQuota)
                });
            }

            return productFamilies;
        }

        #endregion Product Family/Vendor Helpers

        #region Region Table Helpers

        /// <summary>
        /// Adds missing sales reps if either the sales or the quotes data is missing
        /// </summary>
        /// <param name="tableData">Region table data</param>
        /// <param name="monthDetails">Current month details</param>
        /// <param name="year">The year filter.</param>
        /// <param name="month">The month filter.</param>
        /// <param name="currentQuarter">Current quarter number</param>
        /// <returns></returns>
        public List<SalesRepRegionQuotaPerformanceDataModel> AddMissingData(
            List<SalesRepRegionQuotaPerformanceDataModel> tableData, MonthDetails monthDetails, int year, int month,
            int currentQuarter)
        {
            tableData = AllowedSalesReps
                .Select(name =>
                {
                    SalesRepRegionQuotaPerformanceDataModel record = tableData.FirstOrDefault(x => x.SalesRepData.SalesRepName == name);

                    if (record != null)
                    {
                        return record;
                    }

                    SalesRepRegionQuotaPerformanceDataModel defaultRecord = _missingRevenueService.GetSalesRepGetRegionQuotaPerformanceDataModelForSalesRepName(name,
                        monthDetails,
                        year,
                        month,
                        currentQuarter);

                    return defaultRecord;
                })
                .ToList();

            return tableData;
        }

        /// <summary>
        /// Adds ranking info for sales reps
        /// </summary>
        /// <param name="salesRepData">Region Table Data</param>
        /// <param name="rankTotals">Determines if only "Total" rows should be ranked</param>
        /// <returns></returns>
        public List<SalesRepRegionQuotaPerformanceDataModel> ApplyRankings(List<SalesRepRegionQuotaPerformanceDataModel> salesRepData, bool rankTotals = false)
        {
            List<SalesRepRegionQuotaPerformanceDataModel> rankedSalesRepData = salesRepData
                .Where(x => (!rankTotals || x.SalesRepData.SalesRepId == 0) && x.SalesRepData.YtdQuota != 0)
                .ToList();

            rankedSalesRepData = ApplyRanking(rankedSalesRepData, x => x.SalesRepData.MtdPercentToQuotaTrend, (x, rank) =>
            {
                x.SalesRepData.MtdRanking = rank + 1;

                return x;
            });

            rankedSalesRepData = ApplyRanking(rankedSalesRepData, x => x.SalesRepData.QtdPercentToQuotaTrend, (x, rank) =>
            {
                x.SalesRepData.QtdRanking = rank + 1;

                return x;
            });

            rankedSalesRepData = ApplyRanking(rankedSalesRepData, x => x.SalesRepData.YtdPercentToQuotaTrend, (x, rank) =>
            {
                x.SalesRepData.YtdRanking = rank + 1;

                return x;
            });

            return rankedSalesRepData;
        }

        /// <summary>
        /// Adds rankings for selected criteria
        /// </summary>
        /// <param name="queryable">Region Table Data</param>
        /// <param name="rankByFunc">The function that determines the ranking column</param>
        /// <param name="setRankFunc">The function that sets the ranks</param>
        /// <returns></returns>
        private List<SalesRepRegionQuotaPerformanceDataModel> ApplyRanking(List<SalesRepRegionQuotaPerformanceDataModel> queryable,
            Func<SalesRepRegionQuotaPerformanceDataModel, decimal> rankByFunc,
            Func<SalesRepRegionQuotaPerformanceDataModel, int, SalesRepRegionQuotaPerformanceDataModel> setRankFunc)
        {
            queryable = queryable.OrderByDescending(rankByFunc)
                .Select(setRankFunc)
                .ToList();

            return queryable;
        }

        /// <summary>
        /// Adds Total columns for each region, and the total for all regions
        /// </summary>
        /// <param name="tableData">Region Table Data</param>
        /// <returns></returns>
        public List<SalesRepRegionQuotaPerformanceDataModel> AddTotals(List<SalesRepRegionQuotaPerformanceDataModel> tableData)
        {
            if (tableData.Count == 0)
            {
                return tableData;
            }

            //Add totals for each region
            List<SalesRepRegionQuotaPerformanceDataModel> salesRepTotalsForRegion = tableData.GroupBy(x => x.RegionId)
                .Select(salesRepGrouping => new SalesRepRegionQuotaPerformanceDataModel
                {
                    RegionId = salesRepGrouping.Key,
                    RegionName = tableData.FirstOrDefault(x => x.RegionId == salesRepGrouping.Key)?.RegionName,
                    SalesRepData = new RegionQuotaPerformanceSalesRepModel
                    {
                        SalesRepId = 0,
                        SalesRepName = "Total",
                        MtdRevenue = salesRepGrouping.Sum(x => x.SalesRepData.MtdRevenue),
                        MtdQuota = salesRepGrouping.Sum(x => x.SalesRepData.MtdQuota),
                        QtdRevenue = salesRepGrouping.Sum(x => x.SalesRepData.QtdRevenue),
                        QtdQuota = salesRepGrouping.Sum(x => x.SalesRepData.QtdQuota),
                        YtdRevenue = salesRepGrouping.Sum(x => x.SalesRepData.YtdRevenue),
                        YtdQuota = salesRepGrouping.Sum(x => x.SalesRepData.YtdQuota),
                        PreviousYtdRevenue = salesRepGrouping.Sum(x => x.SalesRepData.PreviousYtdRevenue),
                    }
                })
                .ToList();

            //Rank totals only.
            salesRepTotalsForRegion = ApplyRankings(salesRepTotalsForRegion, true);

            //Join Sales reps with totals
            List<SalesRepRegionQuotaPerformanceDataModel> result = tableData.GroupBy(x => x.RegionId)
                .Select(g =>
                {
                    List<SalesRepRegionQuotaPerformanceDataModel> regionData = g.ToList();

                    regionData.Add(salesRepTotalsForRegion.Single(x => x.RegionId == g.Key));

                    return regionData;
                })
                .Aggregate((current, next) =>
                {
                    current.AddRange(next);

                    return current;
                }).ToList();

            //Add total for all regions
            result.Add(new SalesRepRegionQuotaPerformanceDataModel
            {
                RegionId = 0,
                RegionName = "Total Region",
                SalesRepData = new RegionQuotaPerformanceSalesRepModel
                {
                    SalesRepId = 0,
                    SalesRepName = "Total",
                    MtdRevenue = salesRepTotalsForRegion.Sum(x => x.SalesRepData.MtdRevenue),
                    MtdQuota = salesRepTotalsForRegion.Sum(x => x.SalesRepData.MtdQuota),
                    QtdRevenue = salesRepTotalsForRegion.Sum(x => x.SalesRepData.QtdRevenue),
                    QtdQuota = salesRepTotalsForRegion.Sum(x => x.SalesRepData.QtdQuota),
                    YtdRevenue = salesRepTotalsForRegion.Sum(x => x.SalesRepData.YtdRevenue),
                    YtdQuota = salesRepTotalsForRegion.Sum(x => x.SalesRepData.YtdQuota),
                    PreviousYtdRevenue = salesRepTotalsForRegion.Sum(x => x.SalesRepData.PreviousYtdRevenue),
                }
            });

            return result;
        }

        /// <summary>
        /// Converts the SalesRepRegionQuotaPerformanceDataModel to RegionQuotaPerformanceDataModel
        /// </summary>
        /// <param name="tableData">Region Table Data</param>
        /// <returns></returns>
        public List<RegionQuotaPerformanceDataModel> ConvertAndGroupSalesReps(List<SalesRepRegionQuotaPerformanceDataModel> tableData)
        {
            var groupModel = new List<RegionQuotaPerformanceDataModel>();

            if (tableData.Count == 0)
            {
                return groupModel;
            }

            //Group by region and order by MTD ranking
            foreach (IGrouping<string, SalesRepRegionQuotaPerformanceDataModel> item in tableData.GroupBy(x => x.RegionName).ToList())
            {
                groupModel.Add(new RegionQuotaPerformanceDataModel
                {
                    RegionQuotaPerformanceData = item.OrderBy(x => x.SalesRepData.SalesRepName.Equals("Total"))?.ThenBy(x => x.SalesRepData.MtdRanking)?.ToList(),
                    RegionLeader = item.Where(x => x.SalesRepData.IsRegionLeader)?.FirstOrDefault()?.SalesRepData.SalesRepName ?? string.Empty
                });
            }

            //Ordering logic for regions
            groupModel = AllowedRegions
                .Select(name =>
                {
                    RegionQuotaPerformanceDataModel record = groupModel.FirstOrDefault(x =>
                        string.Equals(x.RegionQuotaPerformanceData.FirstOrDefault()?.RegionName.ToLower(),
                            name.ToLower(), StringComparison.InvariantCulture));

                    if (record != null)
                    {
                        return record;
                    }

                    throw new ArgumentNullException(nameof(record));
                })
                .ToList();

            return groupModel;
        }

        #endregion Region Table Helpers

        #region Miscelanous

        public void CheckYearAndMonth(ref int year, ref int month)
        {
            if (year <= 0)
            {
                year = DateTime.Now.Year;
            }

            if (month <= 0)
            {
                month = DateTime.Now.Month;
            }
        }

        #endregion Miscelanous
    }
}