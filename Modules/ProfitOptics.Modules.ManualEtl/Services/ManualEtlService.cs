﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Http;
using ProfitOptics.Framework.Core.Email;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Modules.ManualEtl.Data;
using ProfitOptics.Modules.ManualEtl.Data.Domain;
using ProfitOptics.Modules.ManualEtl.Models;

namespace ProfitOptics.Modules.ManualEtl.Services
{
    public class ManualEtlService : IManualEtlService
    {
        #region Fields

        private readonly Entities _context;
        private readonly IEmailer _emailer;
        private readonly ConnectionStrings _connectionStrings;
        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion Fields

        #region Ctor

        public ManualEtlService(Entities context,
            IEmailer emailer,
            ConnectionStrings connectionStrings,
            IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _emailer = emailer;
            _connectionStrings = connectionStrings;
            _httpContextAccessor = httpContextAccessor;
        }

        #endregion Ctor

        #region Alerting Helper

        public List<string> GetAdminEmails()
        {
            var emails = _context.AspNetUsers.Where(x => x.AspNetUserRoles.Select(r => r.Role.Name.ToLower() == "admin" || r.Role.Name.ToLower() == "superadmin").Any()).Select(x => x.Email).ToList();

            return emails;
        }

        public async Task<bool> SendPortalQaCheckReport(List<string> emails, string body)
        {
            try
            {
                if (!emails.Any() || string.IsNullOrWhiteSpace(body))
                {
                    return false;
                }

                using (var message = new MailMessage { IsBodyHtml = true })
                {
                    message.Subject = "ProfitOptics Framework - Portal QA Checks";

                    message.Body = body;

                    await _emailer.SendEmailAsync(emails, message.Subject, message.Body, message.IsBodyHtml);
                }

                return true;
            }
            catch (Exception e)
            {
                e.Ship(_httpContextAccessor.HttpContext);
                return false;
            }
        }

        #endregion Alerting Helper

        #region Manual Etl

        public IQueryable<ETLLog> GetEtlLogsForSource(string source)
        {
            var result = from log in _context.EtlLogs
                         where source == "ALL" || log.Source == source
                         orderby log.ProcessingTime descending
                         select log;

            return result;
        }

        public IQueryable<ETLLog> GetEtlLogs()
        {
            return _context.EtlLogs;
        }

        #endregion Manual Etl

        #region Manual Upload

        public ManualUploadFileTypeModel GetFileDetails(ManualUploadFileConfigModel configModel)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionStrings.AreaEntities))
                {
                    connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = GetCommandText(configModel.TableName);

                        using (var reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                return ManualUploadFileTypeModel.Default(configModel.FileTypeId, configModel.FileTypeName);
                            }

                            while (reader.Read())
                            {
                                return new ManualUploadFileTypeModel
                                {
                                    FileTypeId = configModel.FileTypeId,
                                    FileTypeName = configModel.FileTypeName,
                                    LastImportTime = Convert.ToDateTime(reader["ETLLoadDate"]),
                                    LastImportUsername = Convert.ToString(reader["ETLUsername"]),
                                    RecordCount = Convert.ToInt32(reader["Cnt"])
                                };
                            }
                        }
                    }

                    connection.Close();

                    return null;
                }
            }
            catch (Exception e)
            {
                e.Ship(_httpContextAccessor.HttpContext);
                return null;
            }
        }

        private static string GetCommandText(string tableName)
        {
            var commandText =
                $@"SELECT TOP 1 ETLUsername, ETLLoadDate, COUNT(*) AS Cnt
                FROM {tableName}
                GROUP BY ETLUsername, ETLLoadDate
                ORDER BY ETLLoadDate DESC";

            return commandText;
        }

        #endregion Manual Upload
    }
}