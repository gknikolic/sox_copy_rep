﻿using ProfitOptics.Framework.Core.Data;

namespace ProfitOptics.Modules.Reporting.Models.BusinessReview
{
    public class BusinessReviewExcelModel : IDataRecord
    {
        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerNum { get; set; }

        public int CustomerClassId { get; set; }

        [DataField("ProductFamily", "", 1)]
        public string ProductFamily { get; set; }

        [DataField("ModelName", "", 2)]
        public string ModelName { get; set; }

        [DataField("Code", "", 3)]
        public string Code { get; set; }

        [DataField("PrevPrevYearRevenue", "", 4)]
        public decimal PrevPrevYearRevenue { get; set; }

        [DataField("PrevYearRevenue", "", 5)]
        public decimal PrevYearRevenue { get; set; }

        [DataField("PrevPrevYTDRevenue", "", 6)]
        public decimal PrevPrevYTDRevenue { get; set; }

        [DataField("PrevYTDRevenue", "", 7)]
        public decimal PrevYTDRevenue { get; set; }

        [DataField("CurrentYTDRevenue", "", 8)]
        public decimal CurrentYTDRevenue { get; set; }

        [DataField("Growth", "", 9)]
        public decimal Growth { get; set; }

        [DataField("PrevPrevYearQty", "", 10)]
        public decimal PrevPrevYearQty { get; set; }

        [DataField("PrevYearQty", "", 11)]
        public decimal PrevYearQty { get; set; }

        public decimal PrevYTDQty { get; set; }

        [DataField("CurrentYearQty", "", 12)]
        public decimal CurrentYTDQty { get; set; }

        [DataField("ASP", "", 13)]
        public decimal ASP { get; set; }
    }
}