﻿using ProfitOptics.Framework.DataLayer.GroundControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models
{
	public class BASReportLineConfig
	{
		public GCLocation Location { get; set; }
		public int HumidityIndex = 0;
		public int TempIndex = 0;
		public int ACPHIndex = 0;
		public int PressureIndex = 0;
	}
}
