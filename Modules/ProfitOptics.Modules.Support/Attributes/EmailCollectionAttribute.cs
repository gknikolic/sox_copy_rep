﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace ProfitOptics.Modules.Support.Attributes
{
    public class EmailCollectionAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is IList list)
            {
                var reg = new Regex(@"[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}");

                foreach (string val in list)
                {
                    if (!string.IsNullOrEmpty(val) && !reg.Match(val).Success)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
