﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Models.ReleaseNotes
{
    public class ReleaseNotesViewModel
    {
        public List<ReleaseNotesModel> List { get; set; }
    }
}