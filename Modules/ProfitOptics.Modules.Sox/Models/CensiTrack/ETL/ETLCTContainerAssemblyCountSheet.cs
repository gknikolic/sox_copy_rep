﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
	public class ETLCTContainerAssemblyCountSheet
	{
        [JsonProperty("sets")]
        public List<ETLCTContainerAssemblyCountSheetSet> Sets { get; set; }

        [JsonProperty("items")]
        public List<ETLCTContainerAssemblyCountSheetItem> Items { get; set; }

        [JsonProperty("footers")]
        public List<ETLCTContainerAssemblyCountSheetFooter> Footers { get; set; }
    }
    public class ETLCTContainerAssemblyCountSheetFooter
    {
        [JsonProperty("setId")]
        public long SetId { get; set; }

        [JsonProperty("defaultTimestamp")]
        public DateTimeOffset? DefaultTimestamp { get; set; }

        [JsonProperty("updateTimestamp")]
        public DateTimeOffset? UpdateTimestamp { get; set; }

        [JsonProperty("definitionUser")]
        public string DefinitionUser { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("location")]
        public object Location { get; set; }

        [JsonProperty("assemblyUser")]
        public string AssemblyUser { get; set; }
    }

    public class ETLCTContainerAssemblyCountSheetItem
    {
        [JsonProperty("setId")]
        public long SetId { get; set; }

        [JsonProperty("itemName")]
        public string ItemName { get; set; }

        [JsonProperty("vendorName")]
        public string VendorName { get; set; }

        [JsonProperty("modelNumber")]
        public string ModelNumber { get; set; }

        [JsonProperty("requiredCount")]
        public int RequiredCount { get; set; }

        [JsonProperty("actualCount")]
        public int ActualCount { get; set; }
    }

    public class ETLCTContainerAssemblyCountSheetSet
    {
        [JsonProperty("setId")]
        public long SetId { get; set; }

        [JsonProperty("setName")]
        public string SetName { get; set; }
        [JsonProperty("assemblyComment")]
        public string AssemblyComment { get; set; }

	}

}
