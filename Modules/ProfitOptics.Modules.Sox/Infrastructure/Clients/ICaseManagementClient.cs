﻿using ProfitOptics.Modules.Sox.Models.ViewModels;
using System;

namespace ProfitOptics.Modules.Sox.Infrastructure.Clients
{
	public interface ICaseManagementClient
	{
		void ChangeGCCaseLifecycleStatus(ManageGCCaseLifecycleStatuses model);
		LifecycleStatusWithTimestamp GetActiveLifecycleStatusAndTimestampForTray(int trayId);
		void UpdateGCCaseLifecycleStatus(int gcCaseId);
		void ChangeTrayLifecycleStatusByBarcode(int caseId, int statusId, string barcode, string user, DateTime? timestamp = null);
	}
}