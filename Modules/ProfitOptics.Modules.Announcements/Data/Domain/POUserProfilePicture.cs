﻿namespace ProfitOptics.Modules.Announcements.Data.Domain
{
    public class POUserProfilePicture
    {
        public int UserId { get; set; }
        public byte[] Data { get; set; }
    }
}