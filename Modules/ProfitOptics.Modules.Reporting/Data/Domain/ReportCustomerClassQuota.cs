﻿namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportCustomerClassQuota
    {
        public int Id { get; set; }

        public int SalesRepId { get; set; }

        public int CustomerClassId { get; set; }

        public string Month { get; set; }

        public int Year { get; set; }

        public decimal Revenue { get; set; }

        public virtual ReportCustomerClass ReportCustomerClass { get; set; }

        public virtual ReportHierarchySalesRep ReportHierarchySalesRep { get; set; }
    }
}
