﻿using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum ViewByType
    {
        [Display(Name = "Bill To")]
        BillTo = 0,

        [Display(Name = "Customer Class")]
        CustomerClass = 1,

        [Display(Name = "Item Category")]
        ItemCategory = 2,

        [Display(Name = "Vendor")]
        Vendor = 3,

        [Display(Name = "Ship To")]
        ShipTo = 4
    }
}