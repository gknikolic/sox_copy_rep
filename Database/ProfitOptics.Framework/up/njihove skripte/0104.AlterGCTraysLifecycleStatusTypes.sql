USE [Sox]

BEGIN
	ALTER TABLE [dbo].[GCTraysLifecycleStatusTypes] DROP COLUMN TimestampUtc;
	ALTER TABLE [dbo].[GCTraysLifecycleStatusTypes] ADD TimestampOriginal DATETIME NULL;
END
GO
UPDATE [dbo].[GCTraysLifecycleStatusTypes] SET TimestampOriginal = Timestamp;
BEGIN
	ALTER TABLE [dbo].[GCTraysLifecycleStatusTypes] ALTER COLUMN TimestampOriginal DATETIME NOT NULL;
END
--ALTER TABLE [dbo].[GCTraysLifecycleStatusTypes] DROP COLUMN TimestampOriginal;
--ALTER TABLE [dbo].[GCTraysLifecycleStatusTypes] ADD TimestampUtc DATETIME NULL;

BEGIN
	ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem] DROP COLUMN UpdateTimestampUtc;
	ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem] ADD UpdateTimestampOriginal DATETIME NULL;
END
GO
UPDATE [dbo].[CTContainerTypeActualHistoryItem] SET UpdateTimestampOriginal = UpdateTimestamp;
BEGIN
	ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem] ALTER COLUMN UpdateTimestampOriginal DATETIME NOT NULL;
END
--ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem] DROP COLUMN UpdateTimestampOriginal;
--ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem] ADD UpdateTimestampUtc DATETIME NULL;

BEGIN
	ALTER TABLE [dbo].[CTCaseContainer] DROP COLUMN DueTimestampUtc;
	ALTER TABLE [dbo].[CTCaseContainer] ADD DueTimestampOriginal DATETIME NULL;
	
	ALTER TABLE [dbo].[CTCaseContainer] DROP COLUMN EndTimestampUtc;
	ALTER TABLE [dbo].[CTCaseContainer] ADD EndTimestampOriginal DATETIME NULL;
	
	ALTER TABLE [dbo].[CTCaseContainer] DROP COLUMN UpdateTimestampUtc;
	ALTER TABLE [dbo].[CTCaseContainer] ADD UpdateTimestampOriginal DATETIME NULL;
	
	ALTER TABLE [dbo].[CTCaseContainer] DROP COLUMN MultipleBasisUtc;
	ALTER TABLE [dbo].[CTCaseContainer] ADD MultipleBasisOriginal DATETIME NULL;
END
GO
UPDATE [dbo].[CTCaseContainer] SET DueTimestampOriginal = DueTimestamp;
UPDATE [dbo].[CTCaseContainer] SET EndTimestampOriginal = EndTimestamp;
UPDATE [dbo].[CTCaseContainer] SET UpdateTimestampOriginal = UpdateTimestamp;
UPDATE [dbo].[CTCaseContainer] SET MultipleBasisOriginal = MultipleBasis;
BEGIN
	ALTER TABLE [dbo].[CTCaseContainer] ALTER COLUMN DueTimestampOriginal DATETIME NOT NULL;
END
--ALTER TABLE [dbo].[CTCaseContainer] DROP COLUMN DueTimestampOriginal;
--ALTER TABLE [dbo].[CTCaseContainer] ADD DueTimestampUtc DATETIME NULL;
--ALTER TABLE [dbo].[CTCaseContainer] DROP COLUMN EndTimestampOriginal;
--ALTER TABLE [dbo].[CTCaseContainer] ADD EndTimestampUtc DATETIME NULL;
--ALTER TABLE [dbo].[CTCaseContainer] DROP COLUMN UpdateTimestampOriginal;
--ALTER TABLE [dbo].[CTCaseContainer] ADD UpdateTimestampUtc DATETIME NULL;
--ALTER TABLE [dbo].[CTCaseContainer] DROP COLUMN MultipleBasisOriginal;
--ALTER TABLE [dbo].[CTCaseContainer] ADD MultipleBasisUtc DATETIME NULL;

BEGIN
	ALTER TABLE [dbo].[CTBILoadResult] DROP COLUMN LoadDateUtc;
	ALTER TABLE [dbo].[CTBILoadResult] ADD LoadDateOriginal DATETIME NULL;
	
	ALTER TABLE [dbo].[CTBILoadResult] DROP COLUMN InsertTimestampUtc;
	ALTER TABLE [dbo].[CTBILoadResult] ADD InsertTimestampOriginal DATETIME NULL;
	
	ALTER TABLE [dbo].[CTBILoadResult] DROP COLUMN UpdateTimestampUtc;
	ALTER TABLE [dbo].[CTBILoadResult] ADD UpdateTimestampOriginal DATETIME NULL;
	
	ALTER TABLE [dbo].[CTBILoadResult] DROP COLUMN LoadTimestampUtc;
	ALTER TABLE [dbo].[CTBILoadResult] ADD LoadTimestampOriginal DATETIME NULL;
END
GO
UPDATE [dbo].[CTBILoadResult] SET LoadDateOriginal = LoadDate;
UPDATE [dbo].[CTBILoadResult] SET InsertTimestampOriginal = InsertTimestamp;
UPDATE [dbo].[CTBILoadResult] SET UpdateTimestampOriginal = UpdateTimestamp;
UPDATE [dbo].[CTBILoadResult] SET LoadTimestampOriginal = LoadTimestamp;
--ALTER TABLE [dbo].[CTBILoadResult] DROP COLUMN LoadDateOriginal;
--ALTER TABLE [dbo].[CTBILoadResult] ADD LoadDateUtc DATETIME NULL;
--ALTER TABLE [dbo].[CTBILoadResult] DROP COLUMN InsertTimestampOriginal;
--ALTER TABLE [dbo].[CTBILoadResult] ADD InsertTimestampUtc DATETIME NULL;
--ALTER TABLE [dbo].[CTBILoadResult] DROP COLUMN UpdateTimestampOriginal;
--ALTER TABLE [dbo].[CTBILoadResult] ADD UpdateTimestampUtc DATETIME NULL;
--ALTER TABLE [dbo].[CTBILoadResult] DROP COLUMN LoadTimestampOriginal;
--ALTER TABLE [dbo].[CTBILoadResult] ADD LoadTimestampUtc DATETIME NULL;

BEGIN
	ALTER TABLE [dbo].[CTBILoadResultContent] DROP COLUMN LastUpdateUtc;
	ALTER TABLE [dbo].[CTBILoadResultContent] ADD LastUpdateOriginal DATETIME NULL;
END
GO
UPDATE [dbo].[CTBILoadResultContent] SET LastUpdateOriginal = LastUpdate;
BEGIN
	ALTER TABLE [dbo].[CTBILoadResultContent] ALTER COLUMN LastUpdateOriginal DATETIME NOT NULL;
END
--ALTER TABLE [dbo].[CTBILoadResultContent] DROP COLUMN LastUpdateOriginal;
--ALTER TABLE [dbo].[CTBILoadResultContent] ADD LastUpdateUtc DATETIME NULL;

BEGIN
	ALTER TABLE [dbo].[CTBILoadResultIndicator] DROP COLUMN InsertTimestampUtc;
	ALTER TABLE [dbo].[CTBILoadResultIndicator] ADD InsertTimestampOriginal DATETIME NULL;
	
	ALTER TABLE [dbo].[CTBILoadResultIndicator] DROP COLUMN UpdateTimestampUtc;
	ALTER TABLE [dbo].[CTBILoadResultIndicator] ADD UpdateTimestampOriginal DATETIME NULL;
END
GO
UPDATE [dbo].[CTBILoadResultIndicator] SET InsertTimestampOriginal = InsertTimestamp;
UPDATE [dbo].[CTBILoadResultIndicator] SET UpdateTimestampOriginal = UpdateTimestamp;
--ALTER TABLE [dbo].[CTBILoadResultIndicator] DROP COLUMN InsertTimestampOriginal;
--ALTER TABLE [dbo].[CTBILoadResultIndicator] ADD InsertTimestampUtc DATETIME NULL;
--ALTER TABLE [dbo].[CTBILoadResultIndicator] DROP COLUMN UpdateTimestampOriginal;
--ALTER TABLE [dbo].[CTBILoadResultIndicator] ADD UpdateTimestampUtc DATETIME NULL;

BEGIN
	ALTER TABLE [dbo].[CTBILoadResultGraphic] DROP COLUMN UpdateTimestampUtc;
	ALTER TABLE [dbo].[CTBILoadResultGraphic] ADD UpdateTimestampOriginal DATETIME NULL;
END
GO
UPDATE [dbo].[CTBILoadResultGraphic] SET UpdateTimestampOriginal = UpdateTimestamp;
BEGIN
	ALTER TABLE [dbo].[CTBILoadResultGraphic] ALTER COLUMN UpdateTimestampOriginal DATETIME NOT NULL;
END
--ALTER TABLE [dbo].[CTBILoadResultGraphic] DROP COLUMN UpdateTimestampOriginal;
--ALTER TABLE [dbo].[CTBILoadResultGraphic] ADD UpdateTimestampUtc DATETIME NULL;