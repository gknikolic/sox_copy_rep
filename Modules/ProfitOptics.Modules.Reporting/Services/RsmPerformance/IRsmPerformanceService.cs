﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;

namespace ProfitOptics.Modules.Reporting.Services.RsmPerformance
{
    public interface IRsmPerformanceService
    {
        void CheckYearAndMonth(ref int year, ref int month);

        #region Item Category/Family Name Helpers

        List<ItemCategoryQuotaPerformanceDataModel> OrderItemsAccordingToAppSetting(
            List<ItemCategoryQuotaPerformanceDataModel> items,
            MonthDetails monthDetails = null,
            int salesRepId = 0,
            int year = 0,
            int month = 0);

        List<ItemCategoryQuotaPerformanceDataModel> AddTotalsRowForItemCategory(
            List<ItemCategoryQuotaPerformanceDataModel> itemCategories, string itemCategoryName = "Total");

        #endregion Item Category/Family Name Helpers

        #region Sales Channel/Customer Class Helpers

        List<SalesChannelQuotaPerformanceDataModel> OrderItemsAccordingToAppSetting(
            List<SalesChannelQuotaPerformanceDataModel> items,
            MonthDetails monthDetails = null,
            int salesRepId = 0,
            int year = 0,
            int month = 0);

        List<SalesChannelChartData> OrderItemsAccordingToAppSetting(List<SalesChannelChartData> items);

        List<SalesChannelQuotaPerformanceDataModel> AddTotalsRowForSalesChannel(
            List<SalesChannelQuotaPerformanceDataModel> salesChannels, string channelName = "Total");

        #endregion Sales Channel/Customer Class Helpers

        #region Product Family/Vendor Helpers

        List<ProductFamilyQuotaPerformanceDataModel> OrderItemsAccordingToAppSetting(
            List<ProductFamilyQuotaPerformanceDataModel> items,
            MonthDetails monthDetails = null,
            int salesRepId = 0,
            int year = 0,
            int month = 0);

        List<ProductFamilyChartData> OrderItemsAccordingToAppSetting(List<ProductFamilyChartData> items);

        List<ProductFamilyQuotaPerformanceDataModel> AddTotalsRowForProductFamily(
            List<ProductFamilyQuotaPerformanceDataModel> productFamilies, string productFamilyName = "Total");

        #endregion Product Family/Vendor Helpers

        #region Region Table Helpers

        List<SalesRepRegionQuotaPerformanceDataModel> AddMissingData(
            List<SalesRepRegionQuotaPerformanceDataModel> tableData, MonthDetails monthDetails, int year, int month,
            int currentQuarter);

        List<SalesRepRegionQuotaPerformanceDataModel>
            AddTotals(List<SalesRepRegionQuotaPerformanceDataModel> tableData);

        List<RegionQuotaPerformanceDataModel> ConvertAndGroupSalesReps(
            List<SalesRepRegionQuotaPerformanceDataModel> tableData);

        List<SalesRepRegionQuotaPerformanceDataModel> ApplyRankings(
            List<SalesRepRegionQuotaPerformanceDataModel> salesRepData, bool rankTotals = false);

        #endregion Region Table Helpers
    }
}