﻿namespace ProfitOptics.Framework.Web.Framework.Enums
{
    public enum CompanyPermissionType
    {
        CompanyPermissionTypeView = 1,
        CompanyPermissionTypeEdit = 2,
        CompanyPermissionTypeAdd = 3,
        CompanyPermissionTypeDelete = 4
    }
}
