﻿using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Helpers;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class SignInJsonResult
    {
        private readonly IStringResourceRepository _stringResourceRepository;

        public SignInJsonResult(IStringResourceRepository stringResourceRepository)
        {
            this._stringResourceRepository = stringResourceRepository;
        }
        public object IsLockedOut => new { success = false, message = _stringResourceRepository.GetResource(ResourceKeys.MessagesSignInResultLockout) };
        public object IsNotAllowed => new { success = false, message = _stringResourceRepository.GetResource(ResourceKeys.MessagesSignInResultNotApproved) };
        public object Failed => new { success = false, message = _stringResourceRepository.GetResource(ResourceKeys.MessagesSignInResultFailed) };
        public object Succeeded => new { success = true };
        public object Redirect(string url)
        {
            return new { redirect = true, url = url };
        }

        public object Message(string message)
        {
            return new {success = false, message = message};
        }
    }
}
