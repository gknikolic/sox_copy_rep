﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using ProfitOptics.Framework.Core;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Web.Framework.Helpers.CustomAttributes;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Modules;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    public class ApplicationEngine : IEngine
    {
        private IServiceProvider _serviceProvider { get; set; }

        public void Initialize(IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            var hostingEnvironment = provider.GetRequiredService<IWebHostEnvironment>();
            CommonHelper.DefaultFileProvider = new ApplicationFileProvider(hostingEnvironment);

            var mvcCoreBuilder = services.AddMvcCore();
            ModuleManager.Initialize(mvcCoreBuilder.PartManager);
        }

        public T Resolve<T>() where T : class
        {
            return (T) GetServiceProvider().GetRequiredService(typeof(T));
        }

        public object Resolve(Type type)
        {
            return GetServiceProvider().GetRequiredService(type);
        }

        public IEnumerable<T> ResolveAll<T>()
        {
            return (IEnumerable<T>)GetServiceProvider().GetServices(typeof(T));
        }

        public object ResolveUnregistered(Type type)
        {
            Exception innerException = null;
            foreach (var constructor in type.GetConstructors())
            {
                try
                {
                    //try to resolve constructor parameters
                    var parameters = constructor.GetParameters().Select(parameter =>
                    {
                        var service = Resolve(parameter.ParameterType);
                        if (service == null)
                            throw new Exception("Unknown dependency");
                        return service;
                    });

                    //all is ok, so create instance
                    return Activator.CreateInstance(type, parameters.ToArray());
                }
                catch (Exception ex)
                {
                    innerException = ex;
                }
            }

            throw new Exception("No constructor was found that had all the dependencies satisfied.", innerException);
        }

        protected IServiceProvider GetServiceProvider()
        {
            var accessor = ServiceProvider.GetService<IHttpContextAccessor>();
            var context = accessor.HttpContext;
            return context?.RequestServices ?? ServiceProvider;
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth, ElmahIo elmahIo)
        {
            // Database configuration
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            //ErrorHandling
            services.AddElmahIo(o =>
            {
                o.ApiKey = elmahIo.ApiKey;
                o.LogId = new Guid(elmahIo.LogId);
            });

            //Common
            services.AddResponseCaching();

            services.AddResponseCompression(options =>
            {
                options.Providers.Add<BrotliCompressionProvider>();
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;
            });

            services.Configure<BrotliCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Optimal;
            });

            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Optimal;
            });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(auth.SessionTimeout);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });

            //Authentication
            services.AddIdentityCore<ApplicationUser>(options => { });
            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.Tokens.EmailConfirmationTokenProvider = "emailconf";
                options.Tokens.AuthenticatorTokenProvider = "Google Authenticator";
                options.SignIn.RequireConfirmedEmail = true;
            })
            .AddEntityFrameworkStores<ApplicationUserDbContext>()
            .AddDefaultTokenProviders()
            .AddTokenProvider<EmailConfirmationTokenProvider<ApplicationUser>>("emailconf")
            .AddTokenProvider<GoogleAuthenticatorTokenProvider>("Google Authenticator");

            services.AddAuthorization();

            services.Configure<SecurityStampValidatorOptions>(options =>
            {
                options.ValidationInterval = TimeSpan.FromDays(150);
            });

            services.Configure<DataProtectionTokenProviderOptions>(opt =>
            {
                opt.TokenLifespan = TimeSpan.FromHours(3);
            });

            services.Configure<EmailConfirmationTokenProviderOptions>(opt =>
            {
                opt.TokenLifespan = TimeSpan.FromDays(2);
            });

            services.Configure<PasswordHasherOptions>(options =>
            {
                //Used for backwards compatibility
                options.CompatibilityMode = PasswordHasherCompatibilityMode.IdentityV2;
                options.IterationCount = 100000;
            });

            services.ConfigureApplicationCookie(opt =>
            {
                opt.LoginPath = POFrameworkDefaults.LoginPath;
                opt.LogoutPath = POFrameworkDefaults.LogoutPath;
                opt.AccessDeniedPath = POFrameworkDefaults.AccessDenied;
                opt.ExpireTimeSpan = TimeSpan.FromMinutes(auth.SessionTimeout);
                opt.SlidingExpiration = true;
            });

            services.Configure<IdentityOptions>(o =>
            {
                o.Password.RequiredLength = 8;
                o.Password.RequireDigit = true;
                o.Password.RequireNonAlphanumeric = true;
                o.Password.RequireUppercase = false;
                o.Password.RequireLowercase = false;
            });

            if (auth.EnableGoogleAuth)
            {
                services.AddAuthentication().AddGoogle("google", options =>
                {
                    options.ClientId = auth.GoogleClientId;
                    options.ClientSecret = auth.GoogleClientSecret;
                    options.SignInScheme = IdentityConstants.ExternalScheme;
                    options.UserInformationEndpoint = "https://www.googleapis.com/oauth2/v2/userinfo";
                    options.ClaimActions.MapJsonKey("image", "picture");
                });
            }

            services.AddSaml(configuration.GetSection("SAML"));

            //MVC

            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new ModuleViewLocationExpander());
            });

            services.Configure<MvcRazorRuntimeCompilationOptions>(options => {
               // options.FileProviders.Clear();
                options.FileProviders.Add(new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "Modules")));
                options.FileProviders.Add(new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "Areas")));
            });

            services.AddControllersWithViews(options =>
            {
                options.Filters.Add(new HandleExceptionAttribute());
                options.SuppressAsyncSuffixInActionNames = false;
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            })
            .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_3_0);
            services.AddRazorPages().AddRazorRuntimeCompilation();

            var typeFinder = new WebAppTypeFinder();
            var serviceRegistratorTypes = typeFinder.FindClassesOfType<IServiceRegistrator>();

            var instances = serviceRegistratorTypes.Select(x => (IServiceRegistrator)Activator.CreateInstance(x));
            foreach (var instance in instances)
            {
                instance.RegisterServices(services, configuration, connectionStrings, auth);
            }

            services.AddScoped<IApplicationFileProvider, ApplicationFileProvider>();
            services.AddSingleton<IEngine, ApplicationEngine>();
            services.AddScoped<ITypeFinder, WebAppTypeFinder>();

            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            var assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.FullName == args.Name);
            if (assembly != null)
                return assembly;

            //get assembly from TypeFinder
            var tf = Resolve<ITypeFinder>();
            assembly = tf.GetAssemblies().FirstOrDefault(a => a.FullName == args.Name);
            return assembly;
        }

        public virtual IServiceProvider ServiceProvider => _serviceProvider;
    }
}
