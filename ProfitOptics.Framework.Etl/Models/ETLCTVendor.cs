﻿using Newtonsoft.Json;

namespace ProfitOptics.Framework.Etl.Models
{
    public class ETLCTVendor
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("contactName")]
        public string ContactName { get; set; }

        [JsonProperty("vendorTypes")]
        public int VendorTypes { get; set; }

        [JsonProperty("friendlyName")]
        public string FriendlyName { get; set; }
    }
}
