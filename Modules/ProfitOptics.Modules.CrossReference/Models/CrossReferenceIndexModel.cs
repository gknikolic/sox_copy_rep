﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.CrossReference.Models
{
    public class CrossReferenceIndexModel
    {
        public List<CompetitorModel> Competitors { get; set; }

        public List<CompatibilityOptionModel> CompatibilityOptions { get; set; }
    }
}
