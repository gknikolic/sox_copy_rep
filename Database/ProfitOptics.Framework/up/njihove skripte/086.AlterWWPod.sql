USE [Sox]

ALTER TABLE [Sox].[dbo].[WWPod] ADD GCImageId INT NULL;


ALTER TABLE [dbo].[WWPod]  WITH CHECK ADD CONSTRAINT [WWPod_GCImage] FOREIGN KEY([GCImageId])
REFERENCES [dbo].[GCImage] ([Id])
GO