﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssuesResult
    {
        public int? id { get; set; }

        public string expand { get; set; }

        public string self { get; set; }

        public string key { get; set; }

        public IssueField fields { get; set; }
    }
}