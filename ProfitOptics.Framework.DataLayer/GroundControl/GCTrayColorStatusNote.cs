﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCTrayColorStatusNote
    {
        public int Id { get; set; }
        public int GCTrayColorStatusId { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public bool RequiresResolution { get; set; }
        public DateTime? ResolvedAtUtc { get; set; }
        public string CreatedBy { get; set; }

        public virtual GCTrayColorStatus GCTrayColorStatus { get; set; }
    }
}
