﻿namespace ProfitOptics.Framework.Core.Settings
{
    public class PdfPrinter
    {
        public string Url { get; set; }
        public string AppCode { get; set; }
        public string AppKey { get; set; }
    }
}
