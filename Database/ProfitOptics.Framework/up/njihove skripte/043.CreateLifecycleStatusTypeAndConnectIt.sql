USE [Sox]

BEGIN

  CREATE TABLE [dbo].[LifecycleStatusType]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50),
    [Order] int	
  )
  
  CREATE TABLE [dbo].[GCTraysLifecycleStatusTypes]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[LifecycleStatusTypeId] int,
    [GCTrayId] int,
	[Timestamp] datetime,
	FOREIGN KEY ([LifecycleStatusTypeId]) REFERENCES [dbo].[LifecycleStatusType] ([Id]),
	FOREIGN KEY ([GCTrayId]) REFERENCES [dbo].[GCTray] ([Id])
  )
  
  CREATE TABLE [dbo].[GCCasesLifecycleStatusTypes]
  (
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[LifecycleStatusTypeId] int,
    [GCCaseId] int,
	[Timestamp] datetime,
	FOREIGN KEY ([LifecycleStatusTypeId]) REFERENCES [dbo].[LifecycleStatusType] ([Id]),
	FOREIGN KEY ([GCCaseId]) REFERENCES [dbo].[GCCase] ([Id])
  )

END