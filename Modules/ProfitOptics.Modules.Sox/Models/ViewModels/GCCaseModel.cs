﻿using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.DataLayer.WorkWave;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCCaseModel
    {
        public int Id { get; set; }
        public string Title { get; set; } 
        public int CTCaseId { get; set; }
        public string OutgoingOrderId { get; set; }
        public string IncomingOrderId { get; set; }
        public string OutgoingRequestId { get; set; }
        public string IncomingRequestId { get; set; }
        public string InstrumentTrays { get; set; }
        public int GCCustomerId { get; set; }
        public int GCCaseStatusId { get; set; }
        public string ProcedureName { get; set; }
        public int? WWPickupRouteId { get; set; }
        public int? WWDeliveryRouteId { get; set; }
        public string VendorName { get; set; }
        public string CaseColor { get; set; }
        public int? QBEstimateId { get; set; }
        public int TrayCount { get; set; }
        public string CustomerName { get; set; }
		public bool IsLoaner { get; set; }
        public DateTime DueTimestamp { get; set; }
        public bool IsCanceled { get; set; }
		public CaseScheduleColorType CaseScheduleColorType { get; set; }
		public GCLifecycleStatusType LifecycleStatusType { get; set; }
        public virtual ICollection<GCSet> Sets { get; set; }
        public virtual CTCase CTCase { get; set; }
        public virtual GCCustomer GCCustomer { get; set; }
        public virtual WWRoute WWPickupRoute { get; set; }
        public virtual WWRoute WWDeliveryRoute { get; set; }
        public virtual GCCaseStatus GCCaseStatus { get; set; }
    }
}
