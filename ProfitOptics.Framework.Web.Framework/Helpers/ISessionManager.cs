﻿namespace ProfitOptics.Framework.Web.Framework.Helpers
{
    public interface ISessionManager
    {
        void SetValue<T>(SessionKeys key, T value);
        bool IsSet(SessionKeys key);
        T GetValue<T>(SessionKeys key);
        void ClearSession();
    }
}
