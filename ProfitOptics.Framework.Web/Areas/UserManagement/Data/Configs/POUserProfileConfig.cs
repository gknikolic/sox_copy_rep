﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.Web.Areas.UserManagement.Data.Domain;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Data.Configs
{
    public class POUserProfileConfig : IEntityTypeConfiguration<POUserProfile>
    {
        public void Configure(EntityTypeBuilder<POUserProfile> builder)
        {
            builder.ToTable("POUserProfile");
            builder.HasKey(user => user.Id);

            builder.Property(user => user.Title).HasMaxLength(50);

            builder.HasOne(user => user.AspNetUser).
                WithOne(p => p.POUserProfile)
                .HasForeignKey<POUserProfile>(user => user.Id);
        }
    }
}
