﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Framework.Helpers.DataTables
{
    public abstract class DataTablesJsonBinder : IModelBinder
    {
        /// <summary>
        /// GetSearchResultModel's the JSON parameter name to retrieve data. 
        /// You may override this to change to your parameter.
        /// </summary>
        protected virtual string JSON_PARAMETER_NAME { get { return "json"; } }
        /// <summary>
        /// Binds a new model with the DataTables request parameters.
        /// </summary>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public object BindModel(ModelBindingContext bindingContext)
        {
            var request = bindingContext.HttpContext.Request;

            // If the request type does not contains "JSON", it's supposed not to be a JSON request so we skip.
            if (!IsJsonRequest(request))
                throw new ArgumentException("You must provide a JSON request and set it's content type to match a JSON request content type.");

            // Desserializes the JSON request using the .Net Json implementation.
            return Deserialize(bindingContext.ValueProvider.GetValue(JSON_PARAMETER_NAME).FirstValue);
        }
        /// <summary>
        /// Checks if a request is a JsonRequest or not. 
        /// You may override this to check for other values or indicators.
        /// </summary>
        /// <param name="request">The HttpRequestBase object representing the MVC request.</param>
        /// <returns>True if the ContentType contains "json", False otherwise.</returns>
        public virtual bool IsJsonRequest(HttpRequest request)
        {
            return request.ContentType.ToLower().Contains("json");
        }
        /// <summary>
        /// When overriden, deserializes the JSON data into a DataTablesRequest object.
        /// </summary>
        /// <param name="jsonData">The JSON data to be deserialized.</param>
        /// <returns>The DataTablesRequest object.</returns>
        protected abstract IDataTablesRequest Deserialize(string jsonData);

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var result = BindModel(bindingContext);
            bindingContext.Result = ModelBindingResult.Success(result);
            return Task.CompletedTask;
        }
    }
}
