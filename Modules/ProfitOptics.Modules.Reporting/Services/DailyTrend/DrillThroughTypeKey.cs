﻿namespace ProfitOptics.Modules.Reporting.Services.DailyTrend
{
    public class DrillThroughTypeKey
    {
        public string GroupCode { get; set; }

        public string GroupName { get; set; }
    }
}