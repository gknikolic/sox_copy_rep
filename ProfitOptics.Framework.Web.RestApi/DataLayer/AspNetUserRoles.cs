﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.DataLayer
{
    public partial class AspNetUserRoles
    {
        public AspNetUserRoles()
        {
        }

        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
