﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class ListOrders
	{
		[JsonProperty("orders")]
		public Dictionary<string, Order> Orders { get; set; }

	}
}
