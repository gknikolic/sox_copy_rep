﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class StatusResult
    {
        public string Message { get; set; }
        public bool Success { get; set; }
        public bool ShowModal { get; set; }
    }
}
