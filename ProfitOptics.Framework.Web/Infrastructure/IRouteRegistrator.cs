﻿using Microsoft.AspNetCore.Routing;

namespace ProfitOptics.Framework.Web.Infrastructure
{
    public interface IRouteRegistrator
    {
        void RegisterRoutes(IEndpointRouteBuilder routeBuilder);
    }
}
