﻿using ProfitOptics.Modules.Sox.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCLocationInfoModel
    {
        public int Id { get; set; }
        public DateTime? Timestamp { get; set; }
        public float Temperature { get; set; }
        public float Humidity { get; set; }
        public float Pressure { get; set; }
        public float ACPH { get; set; }
        public int GCLocationId { get; set; }
		public string ConvertedTimestamp { get { return Timestamp != null ? Timestamp.GetLocalDateTimeString() : ""; } }
		//public string LocationTitle { get; set; }
	}
}
