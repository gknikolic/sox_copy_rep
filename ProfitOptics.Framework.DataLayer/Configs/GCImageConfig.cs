﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCImageConfig : IEntityTypeConfiguration<GCImage>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCImage> builder)
        {
            builder.ToTable("GCImage");
            builder.HasKey(entity => entity.Id);

            builder.HasOne(x => x.GCParentImage)
                .WithMany(x => x.GCChildImages)
                .HasForeignKey(x => x.ParentId)
                .IsRequired(false);
        }
    }
}
