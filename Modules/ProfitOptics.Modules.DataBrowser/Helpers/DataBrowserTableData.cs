﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Data.SqlClient;
using ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder;
using ProfitOptics.Modules.DataBrowser.Models;

namespace ProfitOptics.Modules.DataBrowser.Helpers
{
    public class DataBrowserTableData
    {
        #region Fields

        private readonly SqlConnection conn;
        private readonly DataBrowserQueryBuilder queryBuilder;

        #endregion

        #region Ctor

        public DataBrowserTableData(SqlConnection conn, DataBrowserQueryBuilder queryBuilder)
        {
            this.conn = conn;
            this.queryBuilder = queryBuilder;
        }

        #endregion

        #region Methods

        public DataBrowserPaginationResult GetPaginationResult()
        {
            int count = GetCount();
            var paginationResult = new DataBrowserPaginationResult
            {
                TotalFilteredCount = Math.Min(count, 1000), // showing only top 1000 results, per JAG-44
                TotalCount = count,
                Data = GetData()
            };

            return paginationResult;
        }

        public int GetCount()
        {
            DbCommand countCommand = queryBuilder.BuildCountQueryCommand();
            countCommand.Connection = conn;
            countCommand.CommandTimeout = 60;

            var count = (int) countCommand.ExecuteScalar();

            return count;
        }

        #endregion

        #region Util

        private List<object> GetData()
        {
            SqlCommand dataCommand = queryBuilder.BuildDataQueryCommand();
            dataCommand.Connection = conn;

            //SqlDataReader reader = dataCommand.ExecuteReader();
            var data = new List<object>();
            //while (reader.Read())
            //{
            //    var dict = new Dictionary<string, Object>();

            //    for (int i = 0; i < reader.FieldCount; i++)
            //    {
            //        dict.Add(reader.GetName(i), reader.GetFieldValue<object>(i));
            //    }

            //    data.Add(dict);
            //}

            var dataSet = new DataSet();
            var dataAdapter = new SqlDataAdapter(dataCommand);

            dataAdapter.SelectCommand.CommandTimeout = 60;

            dataAdapter.Fill(dataSet);

            DataTable table = dataSet.Tables[0];
            DataRowCollection rows = table.Rows;
            DataColumnCollection columns = table.Columns;

            data = new List<object>();
            foreach (DataRow row in rows)
            {
                var dict = new Dictionary<string, Object>();

                foreach (DataColumn column in columns)
                {
                    dict.Add(column.ColumnName, row[column]);
                }

                data.Add(dict);
            }

            return data;
        }

        #endregion
    }
}