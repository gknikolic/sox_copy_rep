﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Services;

namespace ProfitOptics.Modules.Sox.Infrastructure
{
    public sealed class ApiServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            //services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));
            var res = StartupConfiguration.ConfigureStartupConfig<WorkWaveOptions>(services, configuration);
            StartupConfiguration.ConfigureStartupConfig<GlobalOptions>(services, configuration);
            StartupConfiguration.ConfigureStartupConfig<CensiTracOptions>(services, configuration);
            StartupConfiguration.ConfigureStartupConfig<MonnitOptions>(services, configuration);
            StartupConfiguration.ConfigureStartupConfig<BoxstormOptions>(services, configuration);
            //services.AddSingleton<WorkWaveOptions>();
            services.AddDbContext<Framework.DataLayer.Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));
            services.AddScoped<IWorkWaveClient, WorkWaveClient>();
            services.AddScoped<IWorkWaveService, WorkWaveService>();

            services.AddScoped<IQuickBooksClient, QuickBooksClient>();
            services.AddScoped<ICaseManagementClient, CaseManagementClient>();
            services.AddScoped<IGroundControlService, GroundControlService>();
            services.AddScoped<IMonnitClient, MonnitClient>();
            services.AddScoped<IMonnitService, MonnitService>();
            services.AddScoped<IBoxstormClient, BoxstormClient>();
            services.AddScoped<IBoxstormService, BoxstormService>();
            //services.AddScoped<IUserService, UserService>();

            services.AddSingleton<ISoxLogger, SoxLogger>();
        }
    }
}
