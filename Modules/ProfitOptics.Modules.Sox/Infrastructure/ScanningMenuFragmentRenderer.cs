﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Web.Framework.Helpers.HtmlHelpers;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Razor.Language.TagHelperMetadata;

namespace ProfitOptics.Modules.Sox.Infrastructure
{
    public class ScanningMenuFragmentRenderer : IMenuFragmentRenderer
    {
        public bool Validate(Framework.Core.Settings.Common settings, ClaimsPrincipal user)
        {
            return string.IsNullOrWhiteSpace(settings.DisabledModules) ||
                   !settings.DisabledModules.Contains("Sox") ||
                   user.IsInRole("admin") ||
                   user.IsInRole("superadmin");
        }

        /// <inheritdoc />
        public Task<IHtmlContent> RenderMenuFragmentAsync(IHtmlHelper htmlHelper)
        {
            return htmlHelper.PartialAsync(PartialViewHelper.BuildModulePartialViewPath(Assembly.GetExecutingAssembly(), "/Views/Shared/Scanning/_Menu.Fragment.cshtml"));
        }

        /// <inheritdoc />
        public int Order => 12;
    }
}
