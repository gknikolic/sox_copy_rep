﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Web.Infrastructure;

namespace ProfitOptics.Framework.Web.Helpers.Extensions
{
    public static class MvcBuilderExtensions
    {
        public static IMvcBuilder LoadFrameworkModules(this IMvcBuilder mvcBuilder, IModuleManager moduleManager, IWebHostEnvironment hostingEnvironment)
        {
            var assemblyList = moduleManager.GetExternalModules(hostingEnvironment);

            foreach (var assembly in assemblyList)
            {
                mvcBuilder = mvcBuilder.AddApplicationPart(assembly);
            }

            return mvcBuilder;
        }
    }
}
