USE [Sox]

BEGIN

 	ALTER TABLE [Sox].[dbo].[CTCaseContainer] DROP COLUMN StandardTime;
	ALTER TABLE [Sox].[dbo].[CTCaseContainer] ADD StandardTime INT NULL;
	
	ALTER TABLE [Sox].[dbo].[CTCaseContainer] DROP COLUMN ExpediteTime;
	ALTER TABLE [Sox].[dbo].[CTCaseContainer] ADD ExpediteTime INT NULL;
	
	ALTER TABLE [Sox].[dbo].[CTCaseContainer] DROP COLUMN StatTime;
	ALTER TABLE [Sox].[dbo].[CTCaseContainer] ADD StatTime INT NULL;
	
END