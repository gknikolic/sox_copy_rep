﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class StatusUpdateExecutionEvents
	{
		[JsonProperty("events")]
		public List<ExecutionEventStatusUpdate> Events { get; set; }

		[JsonProperty("strict")]
		public bool Strict { get; set; }
	}


}
