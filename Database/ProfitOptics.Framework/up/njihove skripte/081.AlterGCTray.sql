USE [Sox]

ALTER TABLE [Sox].[dbo].[GCTray] ADD [Barcode] NVARCHAR(100) NULL;
ALTER TABLE [Sox].[dbo].[GCTray] ADD [IsActiveStatus] BIT NULL;

GO
