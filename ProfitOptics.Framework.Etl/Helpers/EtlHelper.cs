﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using MailKit.Net.Imap;
using ProfitOptics.Framework.Core.System;
using Serilog;
using WinSCP;
using Microsoft.CodeAnalysis.Options;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public class EtlHelper : IEtlHelper
    {
        private readonly Settings _settings;

        const int ERROR_SHARING_VIOLATION = 32;
        const int ERROR_LOCK_VIOLATION = 33;

        public EtlHelper(Settings settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Downloads files from the specified folder on the IMAP server.
        /// </summary>
        /// <param name="destinationFolder"></param>
        /// <param name="folder"></param>
        /// <param name="moveProcessedToFolder"></param>
        public void DownloadFileImap(string destinationFolder, string folder = "INBOX", string moveProcessedToFolder = "ARCHIVE")
        {
            using (var imapClient = new ImapClient())
            {
                imapClient.Connect(_settings.ImapServer, _settings.ImapPort, _settings.ImapUseSSL);
                imapClient.Authenticate(_settings.ImapUsername, _settings.ImapPassword);

                var messages = imapClient.GetFolder(folder);

                Log.Information($"Getting messages from {folder}.");
                var totalMessages = messages.Count;

                if (totalMessages == 0) return;

                foreach (var mailMessage in messages)
                {
                    Log.Information($"Getting message with subject {mailMessage.Subject} and date {mailMessage.Date.ToString(CultureInfo.InvariantCulture)}.");

                    foreach (var attachment in mailMessage.Attachments)
                    {
                        Log.Information($"Saving file {attachment.ContentDisposition?.FileName} to directory {destinationFolder}.");
                        attachment.WriteTo(Path.Combine(destinationFolder, attachment.ContentDisposition?.FileName));
                    }

                    if (string.IsNullOrEmpty(moveProcessedToFolder))
                    {
                        continue;
                    }

                    Log.Information($"Moving message to {moveProcessedToFolder}");

                    var processedMessageFolder = imapClient.GetFolder(folder);
                    messages.MoveTo(MailKit.UniqueId.Parse(mailMessage.MessageId), processedMessageFolder);
                }

                imapClient.Disconnect(true);
            }
        }

        /// <summary>
        /// Executes SQL scripts from the specified folder, or just a single script if the filename is provided.
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="fileName"></param>
        public void ExecuteSql(string folder, string fileName = "")
        {
            if (string.IsNullOrEmpty(_settings.SqlFolder))
            {
                _settings.SqlFolder = Path.Combine((new IO(null)).GetAssemblyDir(), "SQL");
            }

            using (var connection = new SqlConnection(_settings.DefaultConnection))
            {
                connection.Open();

                // Get all the files that we should execute
                var files = new List<string>();

                if (string.IsNullOrWhiteSpace(fileName))
                {
                    var fileNames = Directory.GetFiles(Path.Combine(_settings.SqlFolder, folder));
                    files.AddRange(fileNames);
                }
                else
                {
                    files.Add(Path.Combine(_settings.SqlFolder, folder, fileName));
                }

                files = files.OrderBy(p => p).ToList();

                foreach (var file in files)
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandTimeout = 0;
                        try
                        {
                            command.CommandText = File.ReadAllText(file);

                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"Error executing script from file {file}", ex);
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Checks if the source <see cref="DataTable"/> fits the destination <see cref="DataTable"/>.
        /// </summary>
        /// <param name="dtSource"></param>
        /// <param name="dtDestination"></param>
        public void CheckIfFits(DataTable dtSource, DataTable dtDestination)
        {
            for (var i = 0; i < dtSource.Rows.Count; i++)
            {
                try
                {
                    dtDestination.Rows.Add(dtSource.Rows[i].ItemArray);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Error at row {i}: " + ex.Message, ex);
                }
            }
        }

        /// <summary>
        /// Gets the table structure for the spcified database table name.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public DataTable GetTableStructure(string tableName)
        {
            using (var connection = new SqlConnection(_settings.DefaultConnection))
            {
                connection.Open();
                var dt = new DataTable();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = $"SELECT * FROM {tableName} WHERE 1=0";
                    dt.Load(command.ExecuteReader());
                }

                return dt;
            }
        }

        /// <summary>
        /// Loadas the specified <see cref="DataTable"/> data to the specified database table.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="data"></param>
        /// <param name="truncate"></param>
        /// <param name="skipColumns"></param>
        public void LoadTable(string tableName, DataTable data, bool truncate, string[] skipColumns = null)
        {
            // Remove some columns from the source data table
            if (skipColumns != null)
            {
                foreach (var columnToRemove in skipColumns)
                {
                    for (var i = 0; i < data.Columns.Count; i++)
                    {
                        if (data.Columns[i].ColumnName == columnToRemove)
                        {
                            data.Columns.RemoveAt(i);
                            break;
                        }
                    }
                }
            }

            // Bulk copy
            using (var connection = new SqlConnection(_settings.DefaultConnection))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    if (truncate)
                    {
                        command.CommandText = $"TRUNCATE TABLE {tableName}";
                        command.ExecuteNonQuery();
                    }
                }

                using (var bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.BulkCopyTimeout = 0;
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.WriteToServer(data);
                }
            }
        }

        /// <summary>
        /// Logs the processed file to the database.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="source"></param>
        /// <param name="fileName"></param>
        /// <param name="processingTime"></param>
        /// <param name="status"></param>
        /// <param name="rowCount"></param>
        /// <param name="message"></param>
        /// <param name="actionType"></param>
        /// <param name="options"></param>
        /// <param name="description"></param>
        public void LogProcessedAction(string username, string source, string fileName, string processingTime, string status, int rowCount, string message = null, int? actionType = null, string options = null, string description = "")
        {
            using (var connection = new SqlConnection(_settings.DefaultConnection))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = @"
                    INSERT INTO [ETLLog] ([Source],[FileName],[Username],[ProcessingTime],[Status],[Message],[RowCount],[ActionType],[Options],[Description]) 
                    VALUES (@source, @fileName, @username, @processingTime, @status, @message, @rowCount, @actionType, @options, @description)";

                    command.Parameters.Add(new SqlParameter("source", source));
                    command.Parameters.Add(new SqlParameter("fileName", fileName));
                    command.Parameters.Add(new SqlParameter("username", username));
                    command.Parameters.Add(new SqlParameter("processingTime", processingTime));
                    command.Parameters.Add(new SqlParameter("status", status));
                    command.Parameters.Add(new SqlParameter("message", string.IsNullOrEmpty(message) ? "OK" : message));
                    command.Parameters.Add(new SqlParameter("rowCount", rowCount));
                    command.Parameters.Add(new SqlParameter("actionType", actionType));
                    command.Parameters.Add(new SqlParameter("options", options));
                    command.Parameters.Add(new SqlParameter("description", description));

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Downloads file from SFTP and retruns a list of downloaded files
        /// </summary>
        /// <param name="hostname"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="sshhostkeyfingerprint"></param>
        /// <param name="sourceFolder"></param>
        /// <param name="destinationFolder"></param>
        /// <param name="deleteFile"></param>
        /// <returns></returns>
        public List<string> DownloadFileSftp(string hostname, string username, string password, string sshhostkeyfingerprint, string sourceFolder, string destinationFolder, bool deleteFile)
        {
            var downloadedFiles = new List<string>();
            using (var session = new Session())
            {
                session.Open(new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = hostname,
                    UserName = username,
                    Password = password,
                    PortNumber = 22,
                    SshHostKeyFingerprint = sshhostkeyfingerprint
                });

                if (!Directory.Exists(destinationFolder)) Directory.CreateDirectory(destinationFolder);

                var transferOptions = new TransferOptions { TransferMode = TransferMode.Binary };
                var dir = session.ListDirectory(sourceFolder);

                foreach (var file in dir.Files.Where(x => !x.IsDirectory))
                {
                    var resultFileName = Path.Combine(destinationFolder, file.Name);
                    var transferResult = session.GetFiles(file.FullName, resultFileName, deleteFile, transferOptions);
                    transferResult.Check();

                    downloadedFiles.Add(resultFileName);
                }
            }

            return downloadedFiles;
        }

        /// <summary>
        /// Uploads a file to SFTP
        /// </summary>
        /// <param name="hostname">Hostname</param>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="sshhostkeyfingerprint">SSH host key finger print, starts with something like ssh-dss 1024 ErL+, ends with =</param>
        /// <param name="localPath">Local file path, e.g. c:\\tmp\\file.ext</param>
        /// <param name="remotePath">Remote file path, e.g. /folder/file.ext</param>
        public void UploadFileSftp(string hostname, string username, string password, string sshhostkeyfingerprint, string localPath, string remotePath)
        {
            using (var session = new Session())
            {
                session.Open(new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = hostname,
                    UserName = username,
                    Password = password,
                    PortNumber = 22,
                    SshHostKeyFingerprint = sshhostkeyfingerprint
                });

                TransferOperationResult transferResult = session.PutFiles(localPath, remotePath, options: new TransferOptions { TransferMode = TransferMode.Binary });
                transferResult.Check();
            }
        }

        /// <summary>
        /// Deletets a file to specific file on the sftp server.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="ftpFileUrl"></param>
        public void DeleteFileSFtp(string hostname, string username, string password, string sshhostkeyfingerprint, string fileName)
        {
            using (var session = new Session())
            {
                session.Open(new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = hostname,
                    UserName = username,
                    Password = password,
                    PortNumber = 22,
                    SshHostKeyFingerprint = sshhostkeyfingerprint
                });

                session.RemoveFile(fileName);
            }
        }

        /// <summary>
        /// Download file from the specified FTP URL. 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="sourceFileUrl"></param>
        /// <param name="destinationFolder"></param>
        /// <param name="fileName"></param>
        /// <param name="deleteFile">If set to true, downloaded file is deleted after downloading.</param>
        public void DownloadFileFtp(string username, string password, string sourceFileUrl, string destinationFolder, string fileName, bool deleteFile)
        {
            var client = new WebClient { Credentials = new NetworkCredential(username, password) };
            client.DownloadFile(sourceFileUrl, destinationFolder + @"\" + fileName);
            if (deleteFile)
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(sourceFileUrl);
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                request.Credentials = new NetworkCredential(username, password);
                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Downloads all files from the specified FTP directory.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="ftpDirectoryUrl"></param>
        /// <param name="destinationFolder"></param>
        /// <param name="deleteFiles">If set to true, downloaded files are deleted after downloading.</param>
        public List<string> DownloadAllFilesFromDirectoryFtp(string username, string password, string ftpDirectoryUrl, string destinationFolder, bool deleteFiles)
        {
            try
            {
                //List all items in the ftp directory 
                var requestDetail = (FtpWebRequest)WebRequest.Create(ftpDirectoryUrl);
                requestDetail.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                requestDetail.Credentials = new NetworkCredential(username, password);
                var responseDetail = (FtpWebResponse)requestDetail.GetResponse();
                var readerDetail = new StreamReader(responseDetail.GetResponseStream());

                var request = (FtpWebRequest)WebRequest.Create(ftpDirectoryUrl);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(username, password);
                var response = (FtpWebResponse)request.GetResponse();
                var reader = new StreamReader(response.GetResponseStream());

                var directories = new List<string>();

                var line = reader.ReadLine();
                var lineDetails = readerDetail.ReadLine();

                //Filter out directories
                while (!string.IsNullOrEmpty(line))
                {
                    if (!lineDetails.Contains("<DIR>"))
                        directories.Add(line);
                    line = reader.ReadLine();
                    lineDetails = readerDetail.ReadLine();
                }

                reader.Close();
                response.Close();

                readerDetail.Close();
                responseDetail.Close();

                //Iterate through items and download them one by one
                foreach (var item in directories)
                {
                    DownloadFileFtp(username, password, ftpDirectoryUrl + item, destinationFolder, item, deleteFiles);
                }
                return directories;
            }
            catch (Exception e)
            {
                throw new Exception("An error occured while downloading files from ftp directory. Error message: " + e.Message);
            }
        }

        /// <summary>
        /// Gets the file name from the specified URL.
        /// </summary>
        /// <param name="fileUrl"></param>
        /// <returns></returns>
        public string GetFileNameFromUrl(string fileUrl)
        {
            var result = fileUrl.Split('/');
            return result[result.Length - 1];
        }

        /// <summary>
        /// Deletes a file on the local file system.
        /// </summary>
        /// <param name="filePath"></param>
        public void DeleteFile(string filePath)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);
        }

        /// <summary>
        /// Creates a file and writes text to it.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="textToWrite"></param>
        public void WriteToFileLocal(string fileName, string textToWrite)
        {
            //delete the existing file
            DeleteFile(fileName);

            //write to file
            var sw = File.CreateText(fileName);

            sw.Write(textToWrite);
            sw.Flush();
            sw.Close();
        }

        /// <summary>
        /// Checks if files are locked and moves all of them from one location to another and returns the final file paths
        /// </summary>
        /// <param name="sourceDirectory"></param>
        /// <param name="destinationDirectory"></param>
        /// <returns></returns>
        public List<string> CopyAllFilesLocalWithLockCheck(string sourceDirectory, string destinationDirectory)
        {
            List<string> movedFiles = new List<string>();

            if (Directory.Exists(sourceDirectory) && Directory.Exists(destinationDirectory))
            {
                var discoveredFiles = Directory.GetFiles(sourceDirectory);

                foreach (var file in discoveredFiles)
                {
                    var retryCount = 0;
                    var success = false;

                    while (!success)
                    {
                        if (CanReadFile(file))
                        {
                            File.Copy(file, Path.Combine(destinationDirectory, Path.GetFileName(file)));
                            movedFiles.Add(Path.Combine(destinationDirectory, Path.GetFileName(file)));
                            success = true;
                        }
                        else if (retryCount++ < _settings.CopyFilesRetryCount)
                            System.Threading.Thread.Sleep(_settings.CopyFilesRetryWaitTime);
                        else
                            throw new Exception("Exceeded the maximum number of copy attempts.");
                    }
                }
            }

            return movedFiles;
        }

        /// <summary>
        /// Delete all files form a Directory.
        /// </summary>
        /// <param name="DirectoryPath"></param>
        public void CleanDirectory(string DirectoryPath)
        {
            if (Directory.Exists(DirectoryPath))
                foreach (var file in Directory.GetFiles(DirectoryPath))
                    File.Delete(file);
        }

        /// <summary>
        /// Unpackes a zip file and returns the file name
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string UpackZipFile(string directoryPath, string fileName)
        {
            ZipFile.ExtractToDirectory(Path.Combine(directoryPath, fileName), directoryPath);

            return Directory.GetFiles(directoryPath).FirstOrDefault(x => !x.ToLower().EndsWith(".zip"));
        }

        private static bool IsFileLocked(Exception exception)
        {
            int errorCode = Marshal.GetHRForException(exception) & ((1 << 16) - 1);
            return errorCode == ERROR_SHARING_VIOLATION || errorCode == ERROR_LOCK_VIOLATION;
        }

        private static bool CanReadFile(string filePath)
        {
            //Try-Catch so we dont crash the program and can check the exception
            try
            {
                //The "using" is important because FileStream implements IDisposable and
                //"using" will avoid a heap exhaustion situation when too many handles  
                //are left undisposed.
                using (FileStream fileStream = File.Open(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    if (fileStream != null)
                        fileStream.Close();  //This line is me being overly cautious, fileStream will never be null unless an exception occurs...
                }
            }
            catch (IOException ex)
            {
                if (IsFileLocked(ex))
                {
                    return false;
                }
                else
                    throw new Exception("An error occured while trying to open the file. Error message: " + ex.Message);
            }

            return true;
        }
    }
}
