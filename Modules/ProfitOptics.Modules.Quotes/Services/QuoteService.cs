﻿using System;
using System.Collections.Generic;
using System.Linq;
using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.TokenizationSearch;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Quotes.Data;
using ProfitOptics.Modules.Quotes.Data.Domain;
using ProfitOptics.Modules.Quotes.Helpers;
using ProfitOptics.Modules.Quotes.Helpers.Exceptions;
using ProfitOptics.Modules.Quotes.Models;
using ProfitOptics.Modules.Quotes.Models.FreightCalculation;

namespace ProfitOptics.Modules.Quotes.Services
{
    public class QuoteService : IQuoteService
    {
        #region Fields

        private readonly Entities _context;
        private readonly ISalesRepService _salesRepService;

        #endregion Fields

        #region Ctor

        public QuoteService(Entities context, ISalesRepService salesRepService)
        {
            _context = context;
            _salesRepService = salesRepService;
        }

        #endregion Ctor

        #region Create Quotes

        public int? CloneQuote(int quoteId, string cloneName, int customerId)
        {
            var quote = _context.QTQuotes.AsNoTracking().FirstOrDefault(t => t.Id == quoteId);

            if (quote == null) return null;

            var qtItems = _context.QTQuoteItems.Where(t => t.QuoteId == quoteId).ToList();
            quote.CustomerBillToId = customerId;
            quote.Name = cloneName;
            quote.Id = 0;

            var newQuote = _context.QTQuotes.Add(quote);
            _context.SaveChanges();

            foreach (var qtItem in qtItems)
            {
                qtItem.QuoteId = newQuote.Entity.Id;
            }

            _context.QTQuoteItems.AddRange(qtItems);

            _context.SaveChanges();

            return newQuote.Entity.Id;
        }

        public int CreateQuote(CreateQuoteModel quote)
        {
            var createdQuote = _context.QTQuotes.Add(new QTQuote
            {
                CustomerBillToId = quote.CustomerBillToId,
                CustomerShipToId = quote.CustomerShipToId,
                UserId = quote.UserId,
                Name = quote.QuoteName,
                Description = quote.QuoteDescription,
                Status = "In Process",
                LastModifiedDate = DateTime.UtcNow,
                StartDate = DateTime.UtcNow,
                FreightPaymentType = "statlab"
            });

            _context.SaveChanges();

            return createdQuote.Entity.Id;
        }

        public void DeleteQuote(int quoteId)
        {
            var quote = _context.QTQuotes.SingleOrDefault(x => x.Id == quoteId);

            if (quote != null)
            {
                quote.EndDate = DateTime.UtcNow;
                quote.LastModifiedDate = DateTime.UtcNow;

                _context.SaveChanges();
            }
        }

        public void BulkDeleteQuotes(string quoteName)
        {
            _context.QTQuotes.Where(q => q.Name == quoteName).BatchDelete();
        }

        public void UnarchiveQuote(int quoteId)
        {
            var quote = _context.QTQuotes.SingleOrDefault(x => x.Id == quoteId);

            if (quote != null)
            {
                quote.EndDate = null;
                quote.LastModifiedDate = DateTime.UtcNow;

                _context.SaveChanges();
            }
        }

        #endregion Create Quotes

        #region Customers

        public IQueryable<QTBillTo> GetQtBillTos(int userId)
        {
            var isAdmin = IsUserAdmin(userId);
            var isManager = IsUserManager(userId);
            var salesRepCode = _salesRepService.GetSalesRepCode(userId);
            var managerSalesRepCodes = _salesRepService.GetSalesRepCodesForManager(salesRepCode);

            var records = from c in _context.QTBillToes
                from s in _context.QTShipToes.Where(p => p.CustomerBillToId == c.Id).DefaultIfEmpty()
                where isAdmin
                      || (isManager && managerSalesRepCodes.Contains(salesRepCode))
                      || s.SalesRepCode == salesRepCode
                select c;

            return records;
        }
        
        public List<SelectListItem> CustomersToDropdown()
        {
            var records = (from c in _context.QTBillToes
                           select new SelectListItem
                           {
                               Value = c.Id.ToString(),
                               Text = c.BillToName
                           }).ToList();

            return records;
        }

        public List<SelectListItem> ShipToDropdown(int userId, int quoteId)
        {
            var quote = _context.QTQuotes.Find(quoteId);

            if (quote == null)
            {
                return new List<SelectListItem>();
            }

            var billToId = quote.CustomerBillToId;

            var records = (
                from s in _context.QTShipToes
                where s.CustomerBillToId == billToId
                select new SelectListItem
                {
                    Value = s.Id + "",
                    Text = s.ShipToNum + " (" + s.Address1 + ", " + s.City + ", " + s.State + " " + s.Zip + ")",
                    Selected = quote.CustomerShipToId == s.Id
                }).ToList();

            return records;
        }

        public string GetShipToZip(int shipToId)
        {
            var zipCode = (from qtShipTo in _context.QTShipToes
                where qtShipTo.Id == shipToId
                select qtShipTo.Zip).SingleOrDefault();

            return zipCode;
        }

        #endregion Customers

        #region Customer Pricing

        public IEnumerable<SelectListItem> GetPricingListForDropdown(int quoteId)
        {
            var quote = _context.QTQuotes.Find(quoteId);

            var billToNum = _context.QTBillToes.Find(quote.CustomerBillToId).BillToNum;

            var shipToNum = quote.CustomerShipToId != null ? _context.QTShipToes.Find(quote.CustomerShipToId).ShipToNum : "";

            var plc = (from p in _context.QTCustomerPricings.Where(x => x.BillToNum == billToNum
                       && (x.ShipToNum == shipToNum || x.ShipToNum == ""))
                       from b in _context.QTBillToPriceLists.Where(x => x.Company == p.Company && x.BillToNum == p.BillToNum
                                     && x.ShipToNum == p.ShipToNum && x.SeqNum == p.SeqNum
                                     && x.PriceListCode == p.PriceListCode)
                       select new
                       {
                           p.PriceListCode,
                           p.SeqNum,
                           b.PriceListDescription
                       } into g1
                       group g1 by new { g1.PriceListCode, g1.SeqNum, g1.PriceListDescription }
                       into g2
                       select new
                       {
                           g2.Key.PriceListCode,
                           g2.Key.SeqNum,
                           g2.Key.PriceListDescription,
                           NumOfItems = g2.Count()
                       }).Distinct();

            var results = plc
                .OrderBy(m => m.PriceListCode)
                .Select(q => new SelectListItem
                {
                    Value = q.PriceListCode,
                    Selected = false,
                    Text = "PLC: " + q.PriceListCode + " | Description: " + q.PriceListDescription
                            + " | Items: " + q.NumOfItems + " | SeqNum: " + q.SeqNum
                }).ToList();

            return results;
        }

        public IQueryable<QTCustomerPricing> GetCustomerPriceList(string plcCode)
        {
            var records = from p in _context.QTCustomerPricings.Where(x => x.PriceListCode == plcCode)
                select p;

            return records;
        }

        #endregion Customer Pricing

        #region Items

        public IQueryable<QTItem> GetQtItems(int quoteId, bool filterByBillToCustomerId = false, bool filterQuoteItems = true, bool filterByEndDate = true)
        {
            var classesToExclude = Constants.GetClassesToExclude();

            var records = from i in _context.QTItems.Where(x =>
                    x.Inactive == false
                    && x.OnHold == false
                    && !x.ItemDescription.ToLower().Contains("inactiv")
                    && (x.WebSaleable != false || x.RawSaleable != false || !x.ClassId.StartsWith("RAW"))
                    && !classesToExclude.Contains(x.ClassId))
                select i;

            if (filterByEndDate)
            {
                records = records.Where(x => x.EndDate == null);
            }

            if (filterByBillToCustomerId)
            {
                var quote = _context.QTQuotes.Find(quoteId);
                var customerId = _context.QTBillToes.Find(quote.CustomerBillToId)?.CustomerId;
                records = records.Where(x => x.CustomCustID == "" || x.CustomCustID == null || x.CustomCustID == customerId);
            }

            if (filterQuoteItems)
            {
                var quoteItems = _context.QTQuoteItems.Where(x => x.QuoteId == quoteId).Select(c => c.ItemId).ToList();
                records = records.Where(x => !quoteItems.Contains(x.Id));
            }

            return records;
        }

        public IQueryable<ItemModel> GetItemsData(IDataTablesRequest request, int quoteId, string filter)
        {
            var qtItems = GetQtItems(quoteId, true);
            var quote = _context.QTQuotes.FirstOrDefault(x => x.Id == quoteId);
            var quoteBillToNum = _context.QTBillToes.FirstOrDefault(x => x.Id == quote.CustomerBillToId)?.BillToNum.ToString();

            var soldItems = _context.QTSalesHistories.Where(t => t.BillToNum == quoteBillToNum).Select(t => t.ItemNum).ToList();

            var records = from i in qtItems.Where(y => (filter != "PreviouslySold" || soldItems.Contains(y.ItemNum))
                                                            && (filter != "NewItems" || !soldItems.Contains(y.ItemNum)))
                from s in _context.QTShipToItemSummaries.Where(x => x.ItemNum == i.ItemNum
                                                                    && x.Company == i.Company && x.CustomerBillToId == quote.CustomerBillToId
                                                                    && x.CustomerShipToId == quote.CustomerShipToId).DefaultIfEmpty()
                from inv in _context.QTInventories.Where(x => x.ItemNum == i.ItemNum && x.Company == i.Company
                                                                                     && ((x.Plant == "BAL" && quote.OriginZipCode == 21046) ||
                                                                                         (x.Plant == "MfgSys" && quote.OriginZipCode == 75069))).DefaultIfEmpty()
                let billTo12MoQty = _context.QTShipToItemSummaries.Where(x => x.ItemNum == i.ItemNum
                                                                              && x.Company == i.Company && x.CustomerBillToId == quote.CustomerBillToId).Sum(d => d.Frequency12Mo)
                select new ItemModel
                {
                    ItemId = i.Id,
                    ItemNum = i.ItemNum,
                    ItemDescription = i.ItemDescription,
                    ItemVendor = i.VendorName,
                    FreightClass = i.FreightClass,
                    ItemPrivateLabel = i.PrivateLabel == true,
                    ItemQa = (int) inv.OnHand,
                    ItemQoo = (int) inv.MfgOnOrder,
                    ItemUoM = i.SellingUOM,
                    ShipTo12MoQty = s.Frequency12Mo,
                    BillTo12MoQty = billTo12MoQty
                };

            return records;
        }

        public IQueryable<ItemModel> GetStandingOrderQtItems(int quoteId, int orderNum)
        {
            var qtItems = GetQtItems(quoteId);
            var quote = _context.QTQuotes.Find(quoteId);
            var billTo = _context.QTBillToes.Find(quote.CustomerBillToId);
            var billToNum = billTo.BillToNum;

            var records = (from i in qtItems
                           from p in _context.QTStandingOrdersSchedules.Where(x => x.CustNum == billToNum && x.PartNum == i.ItemNum && x.OrderNum == orderNum)
                           from s in _context.QTShipToItemSummaries.Where(x =>
                                   x.ItemNum == i.ItemNum && x.Company == i.Company && x.CustomerBillToId == quote.CustomerBillToId && x.CustomerShipToId == quote.CustomerShipToId)
                               .DefaultIfEmpty()
                           from inv in _context.QTInventories.Where(x => x.ItemNum == i.ItemNum && x.Company == i.Company
                                                                                                && ((x.Plant == "BAL" && quote.OriginZipCode == 21046) ||
                                                                                                    (x.Plant == "MfgSys" && quote.OriginZipCode == 75069)))
                               .DefaultIfEmpty()
                           let billTo12MoQty = _context.QTShipToItemSummaries.Where(x => x.ItemNum == i.ItemNum
                                                                                         && x.Company == i.Company && x.CustomerBillToId == quote.CustomerBillToId).Sum(d => d.Frequency12Mo)

                           select new ItemModel
                           {
                               ItemId = i.Id,
                               ItemNum = i.ItemNum,
                               ItemDescription = i.ItemDescription,
                               BillTo12MoQty = billTo12MoQty,
                               ShipTo12MoQty = s.Frequency12Mo,
                               ItemUoM = i.SellingUOM,
                               ItemVendor = i.VendorName,
                               ItemPrivateLabel = i.PrivateLabel == true,
                               FreightClass = i.FreightClass,
                               ItemQa = (int)inv.OnHand,
                               ItemQoo = (int)inv.MfgOnOrder
                           }).Distinct();

            return records;
        }

        #endregion Items

        #region Print

        public List<PrintModel.PrintQuoteItems> GetPrintQuoteItemsForQuote(int userId, int quoteId)
        {
            var itemList = GetItemsListForQuote(userId, quoteId);

            var items = (from i in itemList
                         select new PrintModel.PrintQuoteItems
                         {
                             ItemId = i.ItemId,
                             ItemNum = i.ItemNum,
                             ItemDescription = i.ItemDescription,
                             ItemUoM = i.ItemUoM,
                             FreightClass = i.FreightClass,
                             Units = i.Units,
                             MoQty = i.MoQty,
                             LastDate = i.LastDate,
                             LastGpPct = i.LastGpPct,
                             LastPrice = i.LastPrice,
                             WeightProduct = i.WeightProduct,
                             WeightTotal = i.WeightTotal,
                             CurrentCost = i.CurrentCost,
                             CurrentPrice = i.CurrentPrice,
                             CurrentSource = i.CurrentSource,
                             CurrentC = i.CurrentC,
                             CurrentB = i.CurrentB,
                             CurrentGmPercent = i.CurrentGmPercent,
                             QuotePrice = i.QuotePrice,
                             QuoteGmPercent = i.QuoteGmPercent,
                             PriceGuidanceFloor = i.PriceGuidanceFloor,
                             PriceGuidanceTarget = i.PriceGuidanceTarget,
                             PriceGuidanceList = i.PriceGuidanceList,
                             QuoteExtendedCost = i.QuoteExtendedCost,
                             QuoteExtendedRevenue = i.QuoteExtendedRevenue,
                             CompetitorPrice = i.CompetitorPrice,
                             CompetitorUoM = i.CompetitorUoM,
                             CompetitorCf = i.CompetitorCf,
                             CompetitorPriceVarDollar = i.CompetitorPriceVarDollar,
                             CompetitorExtPriceVarDollar = i.CompetitorExtPriceVarDollar,
                             QuoteExtendedMBeforeFreight = i.QuoteExtendedMBeforeFreight,
                             QuoteExtendedNetShippingProfit =
                                i.QuoteExtendedNetShippingRevenue - i.QuoteExtendedNetShippingCost,
                             QuoteExtendedAdjustedMarginDollar = i.QuoteExtendedAdjustedMarginDollar,
                             QuoteExtendedAdjustedMarginPercent = i.QuoteExtendedAdjustedMarginPercent,
                             FloorRevenue = i.FloorRevenue,
                             FloorCost = i.FloorCost,
                             FloorMDollar = i.FloorMDollar,
                             FloorMPercent = i.FloorMPercent
                         });

            return items.ToList();
        }

        public List<ItemModel> GetItemsForQuote(int userId, int quoteId)
        {
            var qtItems = GetQtItems(quoteId, true, filterQuoteItems: false);

            var itemList = (from i in qtItems
                            from qi in _context.QTQuoteItems.Where(x => x.ItemId == i.Id && x.QuoteId == quoteId)
                            select new ItemModel
                            {
                                ItemNum = i.ItemNum,
                                ItemDescription = i.ItemDescription,
                                Units = qi.Qty,
                                ItemUoM = i.SellingUOM,
                                CurrentPrice = qi.UnitPrice,
                                QuotePrice = qi.Qty * qi.UnitPrice
                            });

            return itemList.ToList();
        }

        public PrintModel.CustomerFacingExtendedModel GetDataForPrint(int userId, int quoteId)
        {
            PrintModel.CustomerFacingExtendedModel result = new PrintModel.CustomerFacingExtendedModel();

            var quote = _context.QTQuotes.Find(quoteId);
            var billTo = _context.QTBillToes.Find(quote.CustomerBillToId);
            var shipTo = _context.QTShipToes.Find(quote.CustomerShipToId);
            var shipToSalesRepCode = shipTo?.SalesRepCode;

            var qtUserProfile = _context.QTUserProfiles.FirstOrDefault(x => x.SalesRepCode == shipToSalesRepCode);

            var salesRepDetails = new AspNetUser();
            var managerDetails = new AspNetUser();

            if (qtUserProfile != null)
            {
                salesRepDetails = _context.AspNetUsers.SingleOrDefault(q => q.Id == qtUserProfile.Id);
                managerDetails = _context.AspNetUsers.SingleOrDefault(q => q.Id == qtUserProfile.SalesManagerUserId);
            }
            else
            {
                var user = _context.AspNetUsers.SingleOrDefault(q => q.Id == userId);
                salesRepDetails.FirstName = user?.FirstName;
                salesRepDetails.LastName = user?.LastName;
                managerDetails.FirstName = "";
                managerDetails.LastName = "";
            }

            result.Date = DateTime.UtcNow;
            result.SalesRepName = salesRepDetails?.FirstName + " " + salesRepDetails?.LastName;
            result.SalesRepEmail = salesRepDetails?.Email;
            result.SalesRepPhone = "";

            result.ManagerName = managerDetails?.FirstName + " " + managerDetails?.LastName;
            result.ManagerEmail = managerDetails?.Email;
            result.ManagerPhone = "";

            result.HeaderComment = quote.HeaderComments;
            result.QuoteToAddress1 = shipTo != null ? shipTo.BillToName : billTo.BillToName;
            result.QuoteToAddress2 = shipTo != null ? shipTo.Address1 : billTo.Address1;
            result.QuoteToAddress3 = shipTo != null ? shipTo.City + " " + shipTo.State + " " + shipTo.Zip
                                                    : billTo.City + " " + billTo.State + " " + billTo.Zip;

            result.CustomerNumber = billTo.CustomerId;

            result.Items = GetItemsForQuote(userId, quoteId);
            result.TotalAmount = result.Items.Sum(x => x.QuotePrice);

            return result;
        }

        #endregion Print

        #region Quote Detalis

        public QuoteDetailsModel GetQuoteDetailsData(int quoteId, int userId)
        {
            var isAdmin = IsUserAdmin(userId);
            var isManager = IsUserManager(userId);
            var userProfileCode = _salesRepService.GetSalesRepCode(userId);
            var managerSalesRepIds = _salesRepService.GetSalesRepIdsForManager(userProfileCode);

            var model = (from p in _context.QTQuotes
                         from s in _context.QTShipToes.Where(t => t.Id == p.CustomerShipToId).DefaultIfEmpty()
                         where p.Id == quoteId
                                 && (isAdmin
                                 || (isManager && managerSalesRepIds.Contains(p.UserId))
                                 || p.UserId == userId)
                         select new QuoteDetailsModel
                         {
                             QuoteId = p.Id,
                             QuoteName = p.Name,
                             QuoteDescription = p.Description,
                             PresentedToCustomer = p.PresentedToCustomer,
                             CustomerBillToId = p.CustomerBillToId,
                             Status = p.Status,
                             ValidThroughDate = p.ValidThroughDate,
                             HeaderComments = p.HeaderComments,
                             FooterComments = p.FooterComments,
                             ContactEmail = p.ContactEmail,
                             CustomerReference = p.CustomerReference,
                             NewBusiness = p.NewBusiness ?? false,
                             EstimatedFirstOrderDate = p.EstimatedFirstOrderDate,
                             Probability = p.Probability,
                             ShippingCost = p.ShippingCost,
                             TaxAmount = p.TaxAmount,
                             AverageShippingCost = p.AverageShippingCost,
                             OriginZipCode = p.OriginZipCode,
                             ShipToNum = s == null ? string.Empty : s.ShipToNum,
                             ShipToId = p.CustomerShipToId,
                             ShipToZipOverride = p.ShipToZipOverride,
                             IsArchived = p.EndDate != null,
                             OrderFrequency = p.OrderFrequency,
                             FreightCapAmount = p.FreightCapAmount,
                             FreightPaymentType = p.FreightPaymentType,
                             ShippingMethod = p.ShippingMethod
                         }).SingleOrDefault();

            var customer = (from c in _context.QTBillToes.Where(x => x.Id == model.CustomerBillToId)
                            from s in _context.QTShipToes.Where(p => p.CustomerBillToId == c.Id)
                            select new
                            {
                                CustomerNum = c.BillToNum,
                                c.CustomerId,
                                CustomerName = c.BillToName,
                                FromZip = c.Zip,
                                ToZip = s.Zip
                            }).FirstOrDefault();

            if (model != null)
            {
                var tmp = GetTotAjdAndFloorMarginPct(
                    userId, quoteId,
                    model.AverageShippingCost, model.FreightCapAmount, model.FreightPaymentType);

                model.CustomerNum = customer?.CustomerNum;
                model.CustomerId = customer?.CustomerId;
                model.CustomerName = customer?.CustomerName;
                model.TotAdjMarginPerc = tmp.TotAdjMarginPerc;
                model.TotFloorMarginPerc = tmp.TotFloorMarginPerc;
            }

            return model;
        }

        #endregion Quote Detalis

        #region Quote Header

        public object GetQuoteHeader(int userId, int quoteId)
        {
            var isAdmin = IsUserAdmin(userId);
            var isManager = IsUserManager(userId);
            var userProfileCode = _salesRepService.GetSalesRepCode(userId);
            var managerSalesRepIds = _salesRepService.GetSalesRepIdsForManager(userProfileCode);

            var quote = (from p in _context.QTQuotes
                         from s in _context.QTShipToes.Where(t => t.Id == p.CustomerShipToId).DefaultIfEmpty()
                         where p.Id == quoteId
                                 && (isAdmin
                                 || (isManager && managerSalesRepIds.Contains(p.UserId))
                                 || p.UserId == userId)
                         select new QuoteHeaderModel
                         {
                             Id = p.Id,
                             QuoteName = p.Name,
                             QuoteDescription = p.Description,

                             PresentedToCustomer = p.PresentedToCustomer,
                             Status = p.Status,
                             ValidThroughDate = p.ValidThroughDate,
                             HeaderComments = p.HeaderComments,
                             FooterComments = p.FooterComments,
                             ContactEmail = p.ContactEmail,
                             CustomerReference = p.CustomerReference,
                             NewBusiness = p.NewBusiness ?? false,
                             EstimatedFirstOrderDate = p.EstimatedFirstOrderDate,
                             Probability = p.Probability,
                             ShippingCost = p.ShippingCost,
                             TaxAmount = p.TaxAmount,
                             AverageShippingCost = p.AverageShippingCost,
                             OriginZipCode = p.OriginZipCode,
                             ShipToNum = s == null ? string.Empty : s.ShipToNum,
                             CustomerShipToId = p.CustomerShipToId,
                             ShipToZipOverride = p.ShipToZipOverride,
                             IsArchived = p.EndDate != null,
                             FreightPaymentType = p.FreightPaymentType,
                             FreightCapAmount = p.FreightCapAmount
                         }).SingleOrDefault();

            return quote;
        }

        #endregion Quote Header

        #region Quote Items

        public IQueryable<ItemModel> GetItemsValuesForFreightCalc(int userId, int quoteId)
        {
            var isAdmin = IsUserAdmin(userId);
            var isManager = IsUserManager(userId);
            var userProfileCode = _salesRepService.GetSalesRepCode(userId);
            var managerSalesRepIds = _salesRepService.GetSalesRepIdsForManager(userProfileCode);

            var qtItems = GetQtItems(quoteId, true, filterQuoteItems: false);

            var items = (from q in _context.QTQuotes
                         from qi in _context.QTQuoteItems.Where(x => x.QuoteId == q.Id && x.EndDate == null).DefaultIfEmpty()
                         from i in qtItems.Where(x => x.Id == qi.ItemId).DefaultIfEmpty()
                         where qi.QuoteId == quoteId
                                && (isAdmin
                                || (isManager && managerSalesRepIds.Contains(q.UserId))
                                || q.UserId == userId)
                         select new ItemModel
                         {
                             QuoteExtendedCost = (decimal)(qi.Qty * i.SalesCost),
                             QuoteExtendedRevenue = (decimal)(qi.Qty * qi.UnitPrice),
                             FloorRevenue = (decimal)(qi.Qty * i.SellingFloorPrice),
                             FloorMDollar = (decimal)(qi.Qty * i.SellingFloorPrice - qi.Qty * i.SalesCost)
                         });

            return items;
        }

        public List<ItemModel> GetItemsListForQuote(int userId, int quoteId)
        {
            var isAdmin = IsUserAdmin(userId);
            var isManager = IsUserManager(userId);
            var userProfileCode = _salesRepService.GetSalesRepCode(userId);
            var managerSalesRepIds = _salesRepService.GetSalesRepIdsForManager(userProfileCode);
            
            var qtItems = GetQtItems(quoteId, true, filterQuoteItems: false);

            var query = (from q in _context.QTQuotes.ToList()
                         from b in _context.QTBillToes.Where(x => x.Id == q.CustomerBillToId).DefaultIfEmpty()
                         from sh in _context.QTShipToes.Where(x => x.Id == q.CustomerShipToId).DefaultIfEmpty()
                         from qi in _context.QTQuoteItems.Where(x => x.QuoteId == q.Id && x.EndDate == null).DefaultIfEmpty()
                         from i in qtItems.Where(x => qi != null && x.Id == qi.ItemId).DefaultIfEmpty()
                         from s in _context.QTShipToItemSummaries
                            .Where(x => i != null && x.ItemNum == i.ItemNum
                                 && i.Company == x.Company
                                 && x.CustomerBillToId == q.CustomerBillToId
                                 && x.CustomerShipToId == q.CustomerShipToId)
                             .DefaultIfEmpty()
                         let pricingValues = _context.QTCustomerPricings.Where(x => i != null && x.ItemNum == i.ItemNum && x.Company == i.Company
                             && x.BillToNum == b.BillToNum && (sh == null || x.ShipToNum == sh.ShipToNum || x.ShipToNum == "")
                             && x.StartDate <= DateTime.UtcNow && DateTime.UtcNow <= x.EndDate).OrderBy(n => n.SeqNum).FirstOrDefault()
                         where (qi != null && qi.QuoteId == quoteId) 
                                && (isAdmin
                                || (isManager && managerSalesRepIds.Contains(q.UserId))
                                || q.UserId == userId)
                         select new ItemModel
                         {
                             QuoteAverageShippingCost = q?.AverageShippingCost,
                             FreightPaymentType = q?.FreightPaymentType,
                             FreightCap = q?.FreightCapAmount,

                             QuoteItemId = qi.Id,
                             ItemId = i.Id,
                             LineNumber = qi.LineNumber,
                             ItemNum = i?.ItemNum,
                             ItemDescription = i?.ItemDescription,
                             ItemUoM = i?.SellingUOM,
                             ItemStartDate = qi?.StartDate,
                             FreightClass = i?.FreightClass,
                             Units = qi?.Qty,
                             MoQty = s?.Frequency3Mo ?? 0,
                             LastPrice = s?.LastPrice,
                             LastDate = s?.LastDate,
                             LastGpPct = s?.LastGPPct,
                             WeightProduct = i.UnitNetWeight,
                             WeightTotal = qi?.Qty != null && i?.UnitNetWeight != null ? qi.Qty.Value * i.UnitNetWeight.Value : decimal.Zero,

                             CurrentCost = i?.SalesCost,
                             CurrentPrice = i?.SellingListPrice,
                             CurrentSource = pricingValues?.PriceListCode,
                             CurrentC = pricingValues?.CurrentContract == true,
                             CurrentB = pricingValues?.CurrentBlitzedEnable == true,
                             PlcPrice = pricingValues?.BasePrice,
                             CurrentGmPercent = i?.SellingListPrice != 0 ? (i?.SellingListPrice - i?.SalesCost) / i?.SellingListPrice : 0,

                             QuotePrice = qi?.UnitPrice,
                             QuoteGmPercent = qi?.UnitPMPct,

                             PriceGuidanceFloor = i?.SellingFloorPrice,
                             //todo: we should probably change price guidance values
                             PriceGuidanceTarget = (i?.SellingFloorPrice + i?.SellingListPrice) / 2,
                             PriceGuidanceList = i?.SellingListPrice,

                             //Quote Extended Cost = Unit * CurrentCost
                             QuoteExtendedCost = qi?.Qty != null && i?.SalesCost != null ? qi.Qty.Value * i.SalesCost.Value : decimal.Zero,
                             //Quote Extended Revenue = Unit * QuotePrice
                             QuoteExtendedRevenue = (decimal)(qi?.Qty * qi?.UnitPrice),

                             CompetitorPrice = qi?.CompetitorPrice,
                             CompetitorUoM = qi?.CompetitorUoM,
                             CompetitorCf = qi?.CompetitorCF,
                             CompetitorNormalizedPrice = qi?.CompetitorPrice * qi?.CompetitorCF,
                             CompetitorPriceVarDollar = qi?.CompetitorPrice * qi?.CompetitorCF - qi?.UnitPrice,
                             CompetitorExtPriceVarDollar = (qi?.CompetitorPrice * qi?.CompetitorCF - qi?.UnitPrice) * qi?.Qty,

                             // not sure about this
                             FloorRevenue = (decimal)(qi?.Qty * i?.SellingFloorPrice)
                         });
            var items = query.ToList();

            var quoteTotalWeight = items.Sum(z => z.WeightTotal);
            foreach (var item in items)
            {
                // Guidance Flor GM % = (GuidanceFlor$ - CurrentUnit Cost) / GuidanceFlor$
                item.PriceGuidanceFloorGmPercent = item.PriceGuidanceFloor != 0
                    ? (item.PriceGuidanceFloor - item.CurrentCost) / item.PriceGuidanceFloor
                    : null;

                // Guidance List GM% = (GuidanceList$ - CurrentUnit Cost) / GuidanceList$
                item.PriceGuidanceListGmPercent = item.PriceGuidanceList != 0
                    ? (item.PriceGuidanceList - item.CurrentCost) / item.PriceGuidanceList
                    : null;

                // Guidance List GM% = (GuidanceTarget$ - CurrentUnit Cost) / GuidanceTarget
                item.PriceGuidanceTargetGmPercent = item.PriceGuidanceTarget != 0
                    ? (item.PriceGuidanceTarget - item.CurrentCost) / item.PriceGuidanceTarget
                    : null;

                // Quote Extended M$ Before Freight = QuoteExtendedRevenue - QuoteExtendedCost
                item.QuoteExtendedMBeforeFreight = item.QuoteExtendedRevenue - item.QuoteExtendedCost;

                var freightCost = item.QuoteAverageShippingCost ?? Decimal.Zero;
                // Quote Extended Net Shipping comes from the Est Net Shipping Cost (aka freight cost)
                item.QuoteExtendedNetShippingCost = quoteTotalWeight != 0
                    ? (item.WeightTotal / quoteTotalWeight) * freightCost
                    : 0;

                var freightPaymentType = item.FreightPaymentType;
                if (freightPaymentType == "statlab")
                {
                    item.QuoteExtendedNetShippingRevenue = Decimal.Zero;
                }
                else if (freightPaymentType == "customer")
                {
                    item.QuoteExtendedNetShippingRevenue = 2.1m * item.QuoteExtendedNetShippingCost;
                }
                else if (freightPaymentType == "split")
                {
                    var freightCap = item.FreightCap ?? Decimal.Zero;
                    var freightCapRevenue = Math.Min(freightCap, 2.1m * freightCost);
                    item.QuoteExtendedNetShippingRevenue = quoteTotalWeight != 0
                        ? (item.WeightTotal / quoteTotalWeight) * freightCapRevenue
                        : 0;
                }

                // Quote Extended Adjusted Margin$ = M$ Before Freight - Net Shipping Cost + Net Shipping Revenue
                item.QuoteExtendedAdjustedMarginDollar = item.QuoteExtendedMBeforeFreight
                    - item.QuoteExtendedNetShippingCost
                    + item.QuoteExtendedNetShippingRevenue;

                // Quote Extended Adjusted Margin% = Quote Extended Adjusted Margin$ / Quote Extended Revenue
                item.QuoteExtendedAdjustedMarginPercent = item.QuoteExtendedRevenue != 0
                    ? item.QuoteExtendedAdjustedMarginDollar / item.QuoteExtendedRevenue
                    : null;

                // FloorCost = Quote Extended Cost
                item.FloorCost = item.QuoteExtendedCost;

                // FloorMDollar = FloorRevenue - FloorCost
                item.FloorMDollar = item.FloorRevenue - item.QuoteExtendedCost;

                // FloorMPercent = FloorMDollar / FloorRevenue
                item.FloorMPercent = item.FloorRevenue != 0
                    ? (item.FloorRevenue - item.QuoteExtendedCost) / item.FloorRevenue
                    : (decimal?)null;
            }
            return items;
        }

        public void UpdateLineNumbers(int quoteItemId, int currentLineNumber, int newLineNumber)
        {
            var movedUp = newLineNumber > currentLineNumber;
            var greaterValue = movedUp ? newLineNumber : currentLineNumber;
            var lesserValue = movedUp ? currentLineNumber : newLineNumber;

            var movedQuoteItem = _context.QTQuoteItems.Single(i => i.Id == quoteItemId && i.LineNumber == currentLineNumber);
            var quoteItemsToUpdate = _context.QTQuoteItems.Where(i => i.LineNumber >= lesserValue && i.LineNumber <= greaterValue && i.LineNumber != currentLineNumber);

            movedQuoteItem.LineNumber = newLineNumber;

            foreach (var item in quoteItemsToUpdate)
            {
                item.LineNumber = movedUp ? item.LineNumber - 1 : item.LineNumber + 1;
            }

            _context.SaveChanges();
        }

        public QuoteItemModel GetQuoteItemModel(int userId, int quoteId)
        {
            var quote = _context.QTQuotes.Find(quoteId);
            var customer1 = (from c in _context.QTBillToes.Where(x => x.Id == quote.CustomerBillToId)
                             from s in _context.QTShipToes.Where(p => p.CustomerBillToId == c.Id)
                             select new
                             {
                                 CustomerNum = c.BillToNum,
                                 c.CustomerId,
                                 CustomerName = c.BillToName,
                                 FromZip = c.Zip,
                                 ToZip = s.Zip
                             }).FirstOrDefault();

            var tmp = GetTotAjdAndFloorMarginPct(
                userId, quoteId, quote.AverageShippingCost, 0, "");

            var model = new QuoteItemModel
            {
                QuoteId = quoteId,
                QuoteName = quote.Name,
                CustomerNum = customer1?.CustomerNum,
                CustomerId = customer1?.CustomerId,
                CustomerName = customer1?.CustomerName,
                Notes = quote.Description,
                FromZip = quote.OriginZipCode,
                ShipToId = quote.CustomerShipToId,
                //ToZip = customer1?.ToZip,
                AverageShippingCost = quote.AverageShippingCost,
                TotAdjMarginPerc = tmp.TotAdjMarginPerc,
                TotFloorMarginPerc = tmp.TotFloorMarginPerc,
                IsArchived = (quote.EndDate != null),
                OrderFrequency = quote.OrderFrequency
            };

            return model;
        }

        public decimal GetBasePrice(int quoteId, int itemId)
        {
            var quote = _context.QTQuotes.Find(quoteId);
            var item = _context.QTItems.Find(itemId);
            var billToNum = _context.QTBillToes.Find(quote.CustomerBillToId).BillToNum;
            var shipTo = _context.QTShipToes.Find(quote.CustomerShipToId);
            var shipToNum = shipTo != null ? shipTo.ShipToNum : "";

            var pricingSql = _context.QTCustomerPricings
                .Where(x => x.Company == item.Company && x.BillToNum == billToNum
                            && (x.ShipToNum == shipToNum || x.ShipToNum == "")
                            && x.ItemNum == item.ItemNum
                            && x.StartDate <= DateTime.UtcNow && x.EndDate >= DateTime.UtcNow)
                .OrderBy(x => x.SeqNum);
            var bestPrice = pricingSql.FirstOrDefault();

            return bestPrice?.BasePrice ?? decimal.Zero;
        }
        
        public void AddItemsToQuote(int quoteId, List<int> itemIds)
        {
            var newQuoteItems = new List<QTQuoteItem>();
            const int defaultQty = 1;
            var quoteItems = _context.QTQuoteItems.Where(i => i.QuoteId == quoteId).ToList().OrderBy(i => i.LineNumber);
            var lineNumberList = quoteItems.Select(i => i.LineNumber).ToList();
            var lastLineNumber = lineNumberList.Any() ? lineNumberList.OrderBy(i => i).Last() + 1 : 1;
            foreach (var itemId in itemIds)
            {
                var item = _context.QTItems.Find(itemId);
                var price = GetBasePrice(quoteId, itemId);

                newQuoteItems.Add(new QTQuoteItem
                {
                    ItemId = itemId,
                    LineNumber = lastLineNumber,
                    QuoteId = quoteId,
                    Qty = defaultQty,
                    UnitCost = item.SalesCost,
                    UnitPrice = price,
                    UnitPMPct = price != 0 ? (price - item.SalesCost) / price : 0,
                    ExtCost = item.SalesCost * defaultQty,
                    ExtPrice = price * defaultQty,
                    StartDate = DateTime.UtcNow
                });

                lastLineNumber++;
            }

            _context.QTQuoteItems.AddRange(newQuoteItems);

            _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

            _context.SaveChanges();
        }

        public void AddItemsFromList(int quoteId, List<string> itemNums)
        {
            var quoteItems = _context.QTQuoteItems.Where(i => i.QuoteId == quoteId);
            var idList = quoteItems.Select(i => i.ItemId).ToList();
            var lineNumberList = quoteItems.Select(i => i.LineNumber).ToList();
            var lastLineNumber = lineNumberList.Any() ? lineNumberList.OrderBy(i => i).Last() + 1 : 1;

            var itemsToAdd = new List<QTQuoteItem>();
            const int defaultQty = 1;
            foreach (var itemNum in itemNums)
            {
                var item = _context.QTItems.FirstOrDefault(x => x.ItemNum == itemNum && !idList.Contains(x.Id));

                if (item != null)
                {
                    var price = GetBasePrice(quoteId, item.Id);

                    itemsToAdd.Add(new QTQuoteItem
                    {
                        ItemId = item.Id,
                        LineNumber = lastLineNumber,
                        QuoteId = quoteId,
                        Qty = defaultQty,
                        UnitCost = item.SalesCost,
                        UnitPrice = price,
                        UnitPMPct = price != 0 ? (price - item.SalesCost) / price : 0,
                        ExtCost = item.SalesCost * defaultQty,
                        ExtPrice = price * defaultQty,
                        StartDate = DateTime.UtcNow
                    });

                    lastLineNumber++;
                }
            }

            _context.QTQuoteItems.AddRange(itemsToAdd);

            _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

            _context.SaveChanges();
        }

        public List<string> ValidateItemsFromList(int quoteId, List<string> itemNums)
        {

            var qtItems = GetQtItems(quoteId, true, filterQuoteItems: false, filterByEndDate: false);

            var notValid = (from itemNum in itemNums
                            let item = qtItems.FirstOrDefault(x => x.ItemNum == itemNum)
                            where item == null
                            select itemNum).ToList();
            return notValid;
        }

        public void AddItemToQuote(int quoteId, string itemNum)
        {
            var quoteItems = _context.QTQuoteItems.Where(i => i.QuoteId == quoteId);
            var item = _context.QTItems.FirstOrDefault(x => x.ItemNum == itemNum);
            var alreadyExists = _context.QTQuoteItems.Any(x => x.QuoteId == quoteId && x.ItemId == item.Id);
            var lineNumberList = quoteItems.Select(i => i.LineNumber).ToList();
            var lastLineNumber = lineNumberList.Any() ? lineNumberList.OrderBy(i => i).Last() + 1 : 1;

            if (item != null && !alreadyExists)
            {
                const int defaultQty = 1;

                var price = GetBasePrice(quoteId, item.Id);

                _context.QTQuoteItems.Add(new QTQuoteItem
                {
                    ItemId = item.Id,
                    LineNumber = lastLineNumber,
                    QuoteId = quoteId,
                    Qty = defaultQty,
                    UnitCost = item.SalesCost,
                    UnitPrice = price,
                    UnitPMPct = price != 0 ? (price - item.SalesCost) / price : 0,
                    ExtCost = item.SalesCost * defaultQty,
                    ExtPrice = price * defaultQty,
                    StartDate = DateTime.UtcNow
                });

                _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

                _context.SaveChanges();
            }
        }

        public void RemoveItemFromQuote(int quoteId, int itemId)
        {
            var quoteItem = _context.QTQuoteItems.FirstOrDefault(x => x.QuoteId == quoteId && x.ItemId == itemId);
            if (quoteItem != null)
            {
                _context.QTQuoteItems.Remove(quoteItem);

                _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

                _context.SaveChanges();
            }
        }

        public void RemoveItemListFromQuote(int quoteId, List<int> items)
        {
            List<QTQuoteItem> itemsToDelete = _context.QTQuoteItems
                .Where(x => x.QuoteId == quoteId && items.Contains((int)x.ItemId))
                .ToList();

            _context.QTQuoteItems.RemoveRange(itemsToDelete);

            _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

            _context.SaveChanges();
        }

        public void UpdateItemQty(int quoteId, int itemId, int units)
        {
            var quoteItem = _context.QTQuoteItems.FirstOrDefault(x => x.QuoteId == quoteId && x.ItemId == itemId);
            if (quoteItem != null)
            {
                quoteItem.Qty = units;

                _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

                _context.SaveChanges();
            }
        }

        public void UpdateQuoteItemPrice(int quoteId, int itemId, decimal newValue)
        {
            var quoteItem = _context.QTQuoteItems.FirstOrDefault(x => x.QuoteId == quoteId && x.ItemId == itemId);
            if (quoteItem != null)
            {
                quoteItem.UnitPrice = newValue;
                quoteItem.UnitPMPct = newValue != 0 ? (quoteItem.UnitPrice - quoteItem.UnitCost) / newValue : 0;

                _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

                _context.SaveChanges();
            }
        }

        public void UpdateQuoteItemGmPerc(int quoteId, int itemId, decimal newGpPercValue)
        {
            var quoteItem = _context.QTQuoteItems.FirstOrDefault(x => x.QuoteId == quoteId && x.ItemId == itemId);
            if (quoteItem != null)
            {
                var item = _context.QTItems.FirstOrDefault(x => x.Id == itemId);
                if (item != null)
                {
                    var percent = newGpPercValue / 100;
                    var priceValue = percent != 1 ? item.SalesCost / (1 - percent) : item.SalesCost * 10;
                    quoteItem.UnitPrice = priceValue;
                    quoteItem.UnitPMPct = percent;

                    _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

                    _context.SaveChanges();
                }
            }
        }

        public void UpdateCompetitorPrice(int quoteId, int itemId, decimal newValue)
        {
            var quoteItem = _context.QTQuoteItems.FirstOrDefault(x => x.QuoteId == quoteId && x.ItemId == itemId);
            if (quoteItem != null)
            {
                quoteItem.CompetitorPrice = newValue;

                _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

                _context.SaveChanges();
            }
        }

        public void UpdateCompetitorUoM(int quoteId, int itemId, string newValue)
        {
            var quoteItem = _context.QTQuoteItems.FirstOrDefault(x => x.QuoteId == quoteId && x.ItemId == itemId);
            if (quoteItem != null)
            {
                quoteItem.CompetitorUoM = newValue;

                _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

                _context.SaveChanges();
            }
        }

        public void UpdateCompetitorCf(int quoteId, int itemId, decimal newValue)
        {
            var quoteItem = _context.QTQuoteItems.FirstOrDefault(x => x.QuoteId == quoteId && x.ItemId == itemId);
            if (quoteItem != null)
            {
                quoteItem.CompetitorCF = newValue;

                _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

                _context.SaveChanges();
            }
        }

        public void RevertPriceToPlcPrice(int quoteId, int itemId)
        {
            var price = GetBasePrice(quoteId, itemId);
            var quoteItem = _context.QTQuoteItems.FirstOrDefault(x => x.QuoteId == quoteId && x.ItemId == itemId);
            if (quoteItem != null)
            {
                var item = _context.QTItems.Find(itemId);

                quoteItem.UnitPrice = price;
                quoteItem.UnitPMPct = price != 0 ? (price - item.SalesCost) / price : 0;
                _context.QTQuotes.Find(quoteId).LastModifiedDate = DateTime.UtcNow;

                _context.SaveChanges();
            }
        }

        public void ReplaceItemInQuote(int quoteId, string oldItemNum, string newItemNum)
        {
            var oldItem = _context.QTItems.FirstOrDefault(x => x.ItemNum == oldItemNum);
            var newItem = _context.QTItems.FirstOrDefault(x => x.ItemNum == newItemNum);
            var quoteItem = _context.QTQuoteItems.FirstOrDefault(x => x.QuoteId == quoteId && x.ItemId == oldItem.Id);
            if (quoteItem != null && newItem != null)
            {
                var price = GetBasePrice(quoteId, newItem.Id);

                quoteItem.ItemId = newItem.Id;
                quoteItem.Qty = 1;
                quoteItem.UnitPrice = price;
                quoteItem.UnitPMPct = price != 0 ? (price - newItem.SalesCost) / price : 0;
                quoteItem.ExtPrice = price * 1;
                quoteItem.StartDate = DateTime.UtcNow;
                quoteItem.UnitCost = newItem.SalesCost;

                _context.SaveChanges();
            }
        }

        public List<QuoteItemModel.LookupSelect2Data> GetItemAutocompleteData(int quoteId, string term)
        {
            var items = GetQtItems(quoteId, true);

            var results = items.Where(q => q.ItemNum.Contains(term))
                .OrderBy(m => m.ItemNum.ToLower().StartsWith(term.ToLower()) ? 0 : 1)
                .Take(10)
                .AsEnumerable()
                .Select(q => new QuoteItemModel.LookupSelect2Data
                {
                    id = q.ItemNum,
                    slug = q.ItemNum,
                    text = q.ItemNum
                })
                .ToList();

            return results;
        }

        #endregion Quote Items

        #region Quote Model

        public PagedList<QuoteModel> GetQuoteModelData(int userId, bool showArchived, int urlQuoteId, IDataTablesRequest request)
        {

            var qtItems = GetQtItems(0, false, false, false);

            var quoteItems = from qi in _context.QTQuoteItems.Where(x => x.EndDate == null)
                             from i in qtItems.Where(x => x.Id == qi.ItemId)
                             select new QuoteType
                             {
                                 QuoteId = qi.QuoteId,
                                 ExtPrice = qi.ExtPrice,
                                 Qty = qi.Qty,
                                 SalesCost = i.SalesCost,
                                 FloorPrice = i.SellingFloorPrice
                             } into g1
                             group g1 by g1.QuoteId into g
                             select new QuoteItemType
                             {
                                 QuoteId = g.Key,
                                 Count = g.Count(),
                                 ExtPrice = g.Select(p => p.ExtPrice).Sum(),
                                 ExtCost = g.Select(p => p.Qty * p.SalesCost).Sum(),
                                 FloorRavenue = g.Select(x => x.Qty * x.FloorPrice).Sum()
                             };

            var isAdmin = IsUserAdmin(userId);
            var isManager = IsUserManager(userId);
            var userProfileCode = _salesRepService.GetSalesRepCode(userId);
            var managerSalesRepIds = _salesRepService.GetSalesRepIdsForManager(userProfileCode);

            var customerData = (from s in _context.QTShipToItemSummaries
                group s by s.CustomerBillToId into g
                select new CustomerDataType
                {
                    CustomerBillToId = g.Key,
                    Sales3Mo = g.Sum(p => p.Sales3Mo),
                    Cost3Mo = g.Sum(p => p.Cost3Mo),
                    Sales12Mo = g.Sum(p => p.Sales12Mo),
                    Cost12Mo = g.Sum(p => p.Cost12Mo)
                }).ToList();

            var records = (from q in _context.QTQuotes
                           from c in _context.QTBillToes.Where(p => p.Id == q.CustomerBillToId)
                           from s in customerData.Where(p => p.CustomerBillToId == q.CustomerBillToId).DefaultIfEmpty()
                           from qi in quoteItems.Where(p => p.QuoteId == q.Id).DefaultIfEmpty()
                           where ((q.EndDate == null && !showArchived) || (showArchived && q.EndDate != null))
                                && (isAdmin
                                || (isManager && managerSalesRepIds.Contains(q.UserId))
                                || q.UserId == userId)
                           select new QuoteModel
                           {
                               QuoteId = q.Id,
                               QuoteName = q.Name,
                               Notes = q.Description,
                               CustomerNum = c.CustomerId,
                               CustomerName = c.BillToName,
                               Sales3Mo = s.Sales3Mo ?? decimal.Zero,
                               Cost3Mo = s.Cost3Mo ?? decimal.Zero,
                               GPPct3Mo = (s.Sales3Mo ?? decimal.Zero) == 0 ? decimal.Zero : (((s.Sales3Mo - s.Cost3Mo) / s.Sales3Mo) ?? decimal.Zero),
                               Sales12Mo = s.Sales12Mo ?? decimal.Zero,
                               Cost12Mo = s.Cost12Mo ?? decimal.Zero,
                               GPPct12Mo = (s.Sales12Mo ?? decimal.Zero) == 0 ? decimal.Zero : (((s.Sales12Mo - s.Cost12Mo) / s.Sales12Mo) ?? decimal.Zero),
                               CreatedDate = q.StartDate,
                               LastModifiedDate = q.LastModifiedDate ?? q.StartDate,
                               PresentedToCustomer = q.PresentedToCustomer != null,
                               Items = qi.Count ?? 0,
                               Status = q.Status,
                               QuoteExtGPPct = ((qi.ExtPrice ?? decimal.Zero) == decimal.Zero ? decimal.Zero : (qi.ExtPrice - qi.ExtCost) / qi.ExtPrice) ?? decimal.Zero,
                               QuoteRevenue = qi.ExtPrice ?? decimal.Zero,
                               IsArchived = q.EndDate != null,
                               FloorPct = qi.FloorRavenue != 0 ? (qi.FloorRavenue - qi.ExtCost) / qi.FloorRavenue : decimal.Zero
                           });

            if (urlQuoteId != 0)
            {
                records = records.Where(q => q.QuoteId == urlQuoteId);
                return new PagedList<QuoteModel>(records.ToList(), records.Count());
            }

            records = records.ApplySort(request);

            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                var strings = Tokenizer.TokenizeToWords(request.Search.Value);
                foreach (var str in strings)
                {
                    records = records.Where(x =>
                        (x.QuoteName != null && x.QuoteName.Contains(str)) ||
                        (x.Notes != null && x.Notes.Contains(str)) ||
                        (x.CustomerNum != null && x.CustomerNum.Contains(str)) ||
                        (x.CustomerName != null && x.CustomerName.Contains(str)) ||
                        (x.Status != null && x.Status.Contains(str))
                    );
                }
            }

            var paged = records.ApplyPagination(request).ToList();

            var totalCount = records.Count();
            return new PagedList<QuoteModel>(paged, totalCount);
        }

        #endregion Quote Model

        #region Standing Orders Model

        public IEnumerable<SelectListItem> GetStandingOrderDropdown(int quoteId)
        {
            var quote = _context.QTQuotes.Find(quoteId);
            var billTo = _context.QTBillToes.Find(quote.CustomerBillToId);
            var billToNum = billTo.BillToNum;

            var qtItems = GetQtItems(quoteId);

            var orders = (from p in _context.QTStandingOrdersSchedules.Where(x => x.CustNum == billToNum)
                          from i in qtItems.Where(x => p.PartNum == x.ItemNum)
                          select new
                          {
                              p.OrderNum,
                              p.OrderPattern,
                              p.OrderWeek,
                              p.OrderStartDate,
                              p.PartNum
                          } into g1
                          group g1 by new { g1.OrderNum, g1.OrderPattern, g1.OrderWeek, g1.OrderStartDate }
                          into g2
                          select new
                          {
                              g2.Key.OrderNum,
                              g2.Key.OrderPattern,
                              g2.Key.OrderWeek,
                              g2.Key.OrderStartDate,
                              NumOfItems = g2.Count()
                          }).ToList();

            var results = orders
                .OrderBy(m => m.OrderNum)
                .Select(q => new SelectListItem
                {
                    Value = q.OrderNum + "",
                    Selected = false,
                    Text = "Order Num: " + q.OrderNum + " | # of Items: " + q.NumOfItems
                        + " | Order Pattern: " + q.OrderPattern
                        + " | Order Week: " + q.OrderWeek
                        + " | Order Start Date: " + (q.OrderStartDate != null ? q.OrderStartDate.Value.ToString("d") : "")
                }).ToList();
            return results;
        }
        
        #endregion Standing Orders Model

        #region Update Quote Details Model

        public bool UpdateQuoteDetails(UpdateQuoteDetailsModel quote)
        {
            if (quote.ShipToZipOverride != null)
            {
                quote.CustomerShipToId = null;
            }

            var q = _context.QTQuotes.Find(quote.QuoteId);

            var shouldRecalculateFreight =
                quote.OriginZipCode != q.OriginZipCode
                || quote.CustomerShipToId != q.CustomerShipToId
                || quote.ShipToZipOverride != q.ShipToZipOverride
                || quote.FreightPaymentType != q.FreightPaymentType
                || quote.FreightCapAmount != q.FreightCapAmount;

            q.Description = quote.QuoteDescription;
            q.OriginZipCode = quote.OriginZipCode;
            q.OrderFrequency = quote.OrderFrequency;
            q.FreightPaymentType = quote.FreightPaymentType;
            q.FreightCapAmount = quote.FreightPaymentType == "split" ?
                quote.FreightCapAmount : Decimal.Zero;
            q.CustomerShipToId = quote.CustomerShipToId;
            q.ShipToZipOverride = quote.ShipToZipOverride;
            q.ShippingMethod = quote.ShippingMethod;

            q.LastModifiedDate = DateTime.UtcNow;

            _context.SaveChanges();

            return shouldRecalculateFreight;
        }

        #endregion Update Quote Details Model

        #region Update Quote Model

        public bool UpdateQuote(UpdateQuoteModel quote)
        {
            var q = _context.QTQuotes.Find(quote.QuoteId);

            var shouldRecalculateFreight = (quote.CustomerShipToId != q.CustomerShipToId);

            q.Name = quote.QuoteName;
            q.Description = quote.QuoteDescription;
            q.PresentedToCustomer = quote.PresentedToCustomer;
            q.Status = quote.Status;
            q.ValidThroughDate = quote.ValidThroughDate;
            q.HeaderComments = quote.HeaderComments;
            q.FooterComments = quote.FooterComments;
            q.ContactEmail = quote.ContactEmail;
            q.CustomerReference = quote.CustomerReference;
            q.NewBusiness = quote.NewBusiness;
            q.EstimatedFirstOrderDate = quote.EstimatedFirstOrderDate;
            q.Probability = quote.Probability;
            q.ShippingCost = quote.ShippingCost;
            q.TaxAmount = quote.TaxAmount;
            q.CustomerShipToId = quote.CustomerShipToId;
            q.ShippingMethod = quote.ShippingMethod;

            q.LastModifiedDate = DateTime.UtcNow;

            _context.SaveChanges();

            return shouldRecalculateFreight;
        }

        #endregion Update Quote Model

        #region Freight Calculation Model

        public QuoteItemModel CalculateFreightCost(QuoteHeaderModel data, int userId)
        {
            decimal? freightCost = null;

            try
            {
                if (data.OriginZipCode == null)
                {
                    throw new FreightCostNotAvailableException("Invalid Origin Zip Code (\"From Zip\" value)");
                }

                if (data.CustomerShipToId == null && data.ShipToZipOverride == null)
                {
                    throw new FreightCostNotAvailableException("Invalid Ship To ID or Ship To Zip Override (\"Ship To\" or \"To Zip\" value)");
                }

                var model = new FreightCalculationModel
                {
                    DestinationZipCode = GetZipCode(data),
                    OriginZipCode = data.OriginZipCode,
                    FreightList = new List<FreightEntryModel>()
                };
                
                var qtItems = GetQtItems(data.Id, true, false, false);

                var quoteItems = (from qi in _context.QTQuoteItems.Where(x=> x.QuoteId == data.Id)
                                  join ci in qtItems on qi.ItemId equals ci.Id
                                  select new FreightEntryModel
                                  {
                                      Class = ci.FreightClass ?? 0,
                                      WeightInPounds = (qi.Qty ?? 0) * (ci.UnitNetWeight ?? 0)
                                  }).ToList();

                if (quoteItems.Count == 0)
                {
                    throw new FreightCostNotAvailableException("No items found for quote");
                }

                var freightClasses = quoteItems.Select(q => q.Class).Distinct();

                foreach (var freightClass in freightClasses)
                {
                    if (freightClass == 0)
                    {
                        var quoteWeightForFreightClassZero =
                            quoteItems.Where(q => q.Class == (decimal)0.0)
                                .Sum(q => q.WeightInPounds);
                        model.FreightList.Add(new FreightEntryModel
                        {
                            Class = (decimal)50.0,
                            // by requirements, weight per class gets rounded to the integer above
                            WeightInPounds = Math.Ceiling(quoteWeightForFreightClassZero)
                        });
                        continue;
                    }

                    var orderWeightForFreightClass =
                        quoteItems.Where(q => q.Class == freightClass)
                            .Sum(q => q.WeightInPounds);
                    model.FreightList.Add(new FreightEntryModel
                    {
                        Class = freightClass,
                        // by requirements, weight per class gets rounded to the integer above
                        WeightInPounds = Math.Ceiling(orderWeightForFreightClass)
                    });
                }
                freightCost = CalculateFreightCost(model);
            }
            catch (FreightCostNotAvailableException)
            {
            }

            UpdateQuoteShippingCost(data.Id, freightCost);

            // calculate totAdjMarginPerc and totFloorMarginPerc
            var cap = data.FreightCapAmount;
            var freightPaymentType = data.FreightPaymentType;
            var result = GetTotAjdAndFloorMarginPct(userId, data.Id, freightCost, cap, freightPaymentType);

            return result;
        }

        public QuoteItemModel GetTotAjdAndFloorMarginPct(int userId, int quoteId, decimal? freightCost, decimal? freightCapAmount, string freightPaymentType)
        {
            var tmp = GetItemsValuesForFreightCalc(userId, quoteId);
            decimal? totAdjMarginPerc = null;
            decimal? totFloorMarginPerc = null;
            decimal freightCap = freightCapAmount ?? Decimal.Zero;
            if (freightCost != null)
            {
                if (tmp.Any())
                {
                    var quoteExtendedRevenueTotal = tmp.Sum(x => x.QuoteExtendedRevenue);
                    var quoteQuoteExtendedCost = tmp.Sum(x => x.QuoteExtendedCost);
                    var quoteExtendedMBeforeFreight = quoteExtendedRevenueTotal - quoteQuoteExtendedCost;

                    // logic based on: https://profitoptics.atlassian.net/browse/COWBOYS-247
                    decimal? quoteExtendedAdjustedMarginDollar;
                    if (freightPaymentType == "statlab")
                    {
                        quoteExtendedAdjustedMarginDollar = quoteExtendedMBeforeFreight - freightCost;
                    }
                    else if (freightPaymentType == "customer")
                    {
                        quoteExtendedAdjustedMarginDollar = quoteExtendedMBeforeFreight
                            - freightCost + 2.1m * freightCost;
                    }
                    else if (freightPaymentType == "split")
                    {
                        quoteExtendedAdjustedMarginDollar =
                            quoteExtendedMBeforeFreight - freightCost + Math.Min(freightCap, 2.1m * (decimal)freightCost);
                    }
                    else
                    {
                        // same behavior as for "statlab" freight payment type
                        quoteExtendedAdjustedMarginDollar = quoteExtendedMBeforeFreight - freightCost;
                    }

                    totAdjMarginPerc = tmp.Any(x => x.QuoteExtendedRevenue == 0) ? (decimal?)null :
                        Convert.ToDecimal(quoteExtendedRevenueTotal > 0 ?
                        quoteExtendedAdjustedMarginDollar / quoteExtendedRevenueTotal : 0);

                    var floorMarginDollar = tmp.Sum(z => z.FloorMDollar);
                    var floorTotalRevenue = tmp.Sum(z => z.FloorRevenue);
                    totFloorMarginPerc = Convert.ToDecimal(floorTotalRevenue > 0 ?
                        floorMarginDollar / floorTotalRevenue : 0);
                }
                else
                {
                    totAdjMarginPerc = 0;
                    totFloorMarginPerc = 0;
                }
            }

            var result = new QuoteItemModel
            {
                AverageShippingCost = freightCost,
                TotAdjMarginPerc = totAdjMarginPerc,
                TotFloorMarginPerc = totFloorMarginPerc
            };
            return result;
        }

        public decimal CalculateFreightCost(FreightCalculationModel model)
        {
            var upsRatesForZipCode = GetUpsRates(
                originZip: model.OriginZipCode,
                destinationZip: model.DestinationZipCode);
            var minFreightClass = model.FreightList.Min(x => x.Class); //used for phantom weight
            var maxFreightClass = model.FreightList.Max(x => x.Class);
            var minChargeForMaxFreightClass = upsRatesForZipCode
                .Where(x => x.Class == maxFreightClass)
                .Select(x => x.MinChg)
                .FirstOrDefault();
            var weightPerShipment = model.FreightList.Sum(x => x.WeightInPounds);

            var phantomWeight = GetPhantomWeightAmount(weightPerShipment);

            var total = (decimal)0;
            var totalWithPhantomWeight = (decimal)0;
            foreach (var m in model.FreightList)
            {
                if (m.Class == minFreightClass)
                {
                    totalWithPhantomWeight +=
                        CalculateCostForSingleFreightEntry(upsRatesForZipCode, weightPerShipment + phantomWeight,
                            new FreightEntryModel
                            {
                                Class = m.Class,
                                WeightInPounds = m.WeightInPounds + phantomWeight
                            });
                }
                else
                {
                    totalWithPhantomWeight +=
                        CalculateCostForSingleFreightEntry(upsRatesForZipCode, weightPerShipment + phantomWeight, m);
                }
                total += CalculateCostForSingleFreightEntry(upsRatesForZipCode, weightPerShipment, m);
            }
            total = Math.Min(total, totalWithPhantomWeight);

            var totalAfterMinChargeCheck = Math.Max(total, minChargeForMaxFreightClass);

            var freightCost = totalAfterMinChargeCheck * FreightCalculationModel.FuelSurchargeFactor;

            var additionalUpsSurcharge = GetUpsAdditionalSurcharge(model.DestinationZipCode);
            if (additionalUpsSurcharge != null)
            {
                freightCost += (decimal)additionalUpsSurcharge;
            }

            return freightCost;
        }

        public decimal CalculateCostForSingleFreightEntry(List<QTUpsRate> upsRateData, decimal weightPerShipment, FreightEntryModel freightEntry)
        {
            var upsRateRow = upsRateData.FirstOrDefault(x => x.Class == freightEntry.Class);
            if (upsRateRow == null)
            {
                throw new Exception("Invalid Freight Class: " + freightEntry.Class);
            }

            decimal weightCostCalculation;
            decimal weightInPounds = freightEntry.WeightInPounds;

            if (weightPerShipment < 500)
            {
                weightCostCalculation = weightInPounds * upsRateRow.L5 / 100;
            }
            else if (weightPerShipment < 1000)
            {
                weightCostCalculation = weightInPounds * upsRateRow.M5C / 100;
            }
            else if (weightPerShipment < 2000)
            {
                weightCostCalculation = weightInPounds * upsRateRow.M1M / 100;
            }
            else if (weightPerShipment < 5000)
            {
                weightCostCalculation = weightInPounds * upsRateRow.M2M / 100;
            }
            else if (weightPerShipment < 10000)
            {
                weightCostCalculation = weightInPounds * upsRateRow.M5M / 100;
            }
            else
            {
                weightCostCalculation = weightInPounds * upsRateRow.M10M / 100;
            }

            return weightCostCalculation;
        }

        public int GetZipCode(QuoteHeaderModel data)
        {
            string zipCode;
            if (data.ShipToZipOverride != null)
            {
                int zip;
                if (int.TryParse(data.ShipToZipOverride, out zip))
                {
                    var zipCodeExists = _context.QTUpsRates.Any(t => !(t.Ldzip > zip) && !(t.Hdzip < zip));
                    if (!zipCodeExists)
                    {
                        throw new Exception("Invalid Zip code entered!");
                    }
                }
                else
                {
                    throw new Exception("Invalid Zip code entered!");
                }

                zipCode = data.ShipToZipOverride;
            }
            else
            {
                var shipTo = _context.QTShipToes.FirstOrDefault(q => q.Id == data.CustomerShipToId);
                if (shipTo == null)
                {
                    throw new Exception("Invalid Ship-To Reference (\"Ship To\" zip Code value)");
                }
                zipCode = shipTo.Zip;
            }

            // only the part before the dash is needed. if there's no dash this still works
            var zipCodeBeforeDash = zipCode.Split('-')[0];

            return Convert.ToInt32(zipCodeBeforeDash);
        }

        public decimal GetPhantomWeightAmount(decimal weight)
        {
            if (weight < 500) return 500 - weight;
            if (weight < 1000) return 1000 - weight;
            if (weight < 2000) return 2000 - weight;
            if (weight < 5000) return 5000 - weight;
            if (weight < 10000) return 10000 - weight;
            return 0;
        }

        public List<QTUpsRate> GetUpsRates(int? originZip, int destinationZip)
        {
            return _context.QTUpsRates
               .Where(q => q.OriginZip == originZip && q.Hdzip >= destinationZip && q.Ldzip <= destinationZip)
               .ToList();
        }

        public decimal? GetUpsAdditionalSurcharge(int destinationZip)
        {
            return _context.QTUpsAdditionalSurcharges
               .Where(q => q.Hdzip >= destinationZip && q.Ldzip <= destinationZip)
               .Select(q => q.Surcharge)
               .FirstOrDefault();
        }

        public void UpdateQuoteShippingCost(int quoteId, decimal? freightCost)
        {
            var quote = _context.QTQuotes.Find(quoteId);
            if (quote == null)
            {
                throw new Exception("Invalid Quote Data");
            }
            quote.AverageShippingCost = freightCost;

            quote.LastModifiedDate = DateTime.UtcNow;

            _context.SaveChanges();
        }

        #endregion Freight Calculation Model

        #region UserRoles

        private bool IsUserAdmin(int userId)
        {
            var userRoles = _context.AspNetUsers.Find(userId).AspNetUserRoles;

            var result = userRoles.Any(x => x.RoleId == 1);

            return result;
        }

        private bool IsUserManager(int userId)
        {
            var userRoles = _context.AspNetUsers.Find(userId).AspNetUserRoles;

            var result = userRoles.Any(x => x.RoleId == 20);

            return result;
        }

        #endregion UserRoles

        #region Nested Classes

        private class QuoteType
        {
            public int? QuoteId { get; set; }
            public decimal? ExtPrice { get; set; }
            public int? Qty { get; set; }
            public decimal? SalesCost { get; set; }
            public decimal? FloorPrice { get; set; }
        }

        private class CustomerDataType
        {
            public int CustomerBillToId { get; set; }
            public decimal? Sales3Mo { get; set; }
            public decimal? Cost3Mo { get; set; }
            public decimal? Sales12Mo { get; set; }
            public decimal? Cost12Mo { get; set; }
        }

        private class QuoteItemType
        {
            public int? QuoteId { get; set; }
            public int? Count { get; set; }
            public decimal? ExtPrice { get; set; }
            public decimal? ExtCost { get; set; }
            public decimal? FloorRavenue { get; set; }
        }

        #endregion Nested Classes
    }
}