using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.RestApi.Resources.Distributor
{
    public class DistributorResource
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer> Customers { get; set; }

        public List<ProfitOptics.Framework.Web.RestApi.DataLayer.DistributorProduct> DistributorProducts { get; set; }
    }
}