﻿namespace ProfitOptics.Modules.Sox.Models
{
	public enum BarcodeStatusType
	{
		SCANNED,
		UNREADABLE, 
		MISSING_BARCODE,
		MISSING_PACKAGE
	}


}
