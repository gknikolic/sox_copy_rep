﻿/**
 * Used to encapsulate page logic. Check out Showcase/Misc for example of how page object is extended
 */
(function (page, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    page.OpenAnnouncement = function (Id) {
        var data = { Id: Id };
        $.ajax({
            type: "POST",
            url: "/Announcements/Announcement/AnnouncementPartial",
            dataType: "html",
            data: data,
            cache: false,
            timeout: 100000,
            success: function (data) {
                $("#AnnouncementDialogHolder").html(data);
                $("#AnnouncementDialog").modal({ show: true });
                setTimeout("page.MarkAnnouncementAsRead(" + Id + ")", 1000);
            }
        });
    };

    page.MarkAnnouncementAsRead = function (Id) {
        var data = { Id: Id };
        $.ajax({
            type: "POST",
            url: "/Announcements/Announcement/MarkAnnouncementAsRead",
            dataType: "html",
            data: data,
            cache: false,
            timeout: 100000,
            success: function (data) {
                $("#dropdownAnnouncement").replaceWith(data);
            }
        });
    };

    // Public properties
    page.somePublicProperty = []; 

    // Public Methods
    page.somePublicMethod = function () {
    };

    // Private Methods
    function someMethod(item) {
    }
}(window.page = window.page || {}, jQuery));