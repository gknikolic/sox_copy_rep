using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Support.Data.Domain
{
    [Table("JiraSupportNotification")]
    public class JiraSupportNotification
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string IssueId { get; set; }

        public string ProjectId { get; set; }
    }
}