﻿using ProfitOptics.Modules.DataBrowser.Models;

namespace ProfitOptics.Modules.DataBrowser.Services
{
    public interface IDataBrowserValidatorService
    {
        bool ValidateDatabaseName(string databaseName);

        bool ValidateTableName(string databaseName, string tableName);

        bool ValidateTableQuery(TableQueryModel tableQueryModel);
    }
}
