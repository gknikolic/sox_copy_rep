﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCSystemAlertTypeConfig : IEntityTypeConfiguration<GCSystemAlertType>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCSystemAlertType> builder)
        {
            builder.ToTable("GCSystemAlertType");
            builder.HasKey(entity => entity.Id);
        }
    }
}
