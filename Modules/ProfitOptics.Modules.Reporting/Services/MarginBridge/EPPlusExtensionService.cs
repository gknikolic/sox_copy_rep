﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using log4net;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Modules.Reporting.Attributes;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Services.MarginBridge
{
    public class EPPlusExtensionService : IEPPlusExtensionService
    {
        #region Fields

        private static ILog log = LogManager.GetLogger(typeof(EPPlusExtensionService));

        #endregion Fields

        #region Util

        private OperationStatus ValidateSalesExcel<T>(Stream file, int worksheetIndex, List<string> locationNames, List<string> productNumbers)
        {
            using (var pck = new ExcelPackage())
            {
                pck.Load(file);

                var ws = pck.Workbook.Worksheets[worksheetIndex];
                var toColumn = GetValidProperties(typeof(T)).Length;
                var myProperties = GetValidProperties(typeof(T));
                var excelObjDifference = myProperties.Length - ws.Dimension.Columns;
                for (var rowNum = 1; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var currentColumn = 0;
                    try
                    {
                        var wsRow = ws.Cells[rowNum, 1, rowNum, toColumn];
                        if (rowNum == 1)
                        {
                            //header validation
                            for (var i = 1; i <= myProperties.Length - excelObjDifference; i++)
                            {
                                var currentValue = wsRow[rowNum, i].Text;
                                var columnName = GetColumnName(i, ExcelFileType.Sales);
                                if (!currentValue.Equals(columnName))
                                {
                                    return new OperationStatus { Status = false, Message = string.Format("Sales Error: {0} column title is not valid", columnName) };
                                }
                            }
                        }
                        else
                        {
                            for (int colNum = 1; colNum <= myProperties.Length - excelObjDifference; colNum++)
                            {
                                currentColumn = colNum;
                                var cellValue = wsRow[rowNum, colNum].Text;

                                if (string.IsNullOrWhiteSpace(cellValue))
                                    return new OperationStatus
                                    {
                                        Status = false,
                                        Message = String.Format("Sales Error: {0} is missing value in row {1}", GetColumnName(currentColumn, ExcelFileType.Sales), rowNum)
                                    };
                                if (currentColumn == 1 && !locationNames.Contains(cellValue))
                                    return new OperationStatus
                                    {
                                        Status = false,
                                        Message = String.Format("Sales Error: Location Name in row {0} does not exist in locations list", rowNum)
                                    };
                                if (currentColumn == 2 && !productNumbers.Contains(cellValue))
                                    return new OperationStatus
                                    {
                                        Status = false,
                                        Message = String.Format("Sales Error: Product Number in row {0} does not exist in products list", rowNum)
                                    };
                                try
                                {
                                    var propertyType = myProperties[colNum - 1 + excelObjDifference].PropertyType;
                                    var converter = TypeDescriptor.GetConverter(propertyType);
                                    converter.ConvertFromString(cellValue);
                                }
                                catch (Exception)
                                {
                                    return new OperationStatus
                                    {
                                        Status = false,
                                        Message = String.Format("Sales Error: {0} data type in row {1} is not valid", GetColumnName(currentColumn, ExcelFileType.Sales), rowNum)
                                    };
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Failed parsing input file at row " + rowNum + ", column " + currentColumn + ". " + ex.Message, ex);
                    }
                }

                return new OperationStatus { Status = true };
            }
        }

        private string GetColumnName(int columnNumber, ExcelFileType fileType)
        {
            if (fileType == ExcelFileType.Locations)
            {
                switch (columnNumber)
                {
                    case 1:
                        return "Location Level 1";

                    case 2:
                        return "Location Level 2";

                    case 3:
                        return "Location Level 3";

                    case 4:
                        return "Location Level 4";

                    case 5:
                        return "Location Level 5";

                    case 6:
                        return "Location Level 6";

                    case 7:
                        return "Location Name";
                }
            }
            else if (fileType == ExcelFileType.Products)
            {
                switch (columnNumber)
                {
                    case 1:
                        return "Product Level 1";

                    case 2:
                        return "Product Level 2";

                    case 3:
                        return "Product Level 3";

                    case 4:
                        return "Product Level 4";

                    case 5:
                        return "Product Level 5";

                    case 6:
                        return "Product Level 6";

                    case 7:
                        return "Product Number";
                }
            }
            else
            {
                switch (columnNumber)
                {
                    case 1:
                        return "LocationName";

                    case 2:
                        return "ProductNumber";

                    case 3:
                        return "InvoiceCost";

                    case 4:
                        return "InvoiceAmount";

                    case 5:
                        return "InvoiceDT";

                    case 6:
                        return "QuantityShipped";

                    case 7:
                        return "RebatedCost";

                    case 8:
                        return "InvoiceNumber";
                }
            }

            return "Unknown column name";
        }

        #endregion

        #region Methods

        public List<T> LoadFromExcelFile<T>(string fileName, int worksheetIndex)
        {
            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return LoadFromExcelFile<T>(stream, worksheetIndex);
            }
        }

        public List<T> LoadFromExcelFile<T>(Stream file, int worksheetIndex)
        {
            using (var pck = new ExcelPackage())
            {
                pck.Load(file);

                var toReturn = new List<T>();

                var ws = pck.Workbook.Worksheets[worksheetIndex];
                var toColumn = GetValidProperties(typeof(T)).Count();
                var myType = typeof(T);
                var myProperties = GetValidProperties(typeof(T));
                for (var rowNum = 2; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var currentColumn = 0;
                    try
                    {
                        var objT = Activator.CreateInstance<T>();
                        var wsRow = ws.Cells[rowNum, 1, rowNum, toColumn];
                        for (int i = 1; i <= myProperties.Count(); i++)
                        {
                            currentColumn = i;

                            // Handle special cases
                            var cellValue = wsRow[rowNum, i].Text;
                            if (cellValue == "")
                                myProperties[i - 1].SetValue(objT, null);
                            else if (myProperties[i - 1].PropertyType == typeof(DateTime) && cellValue == "0")
                                myProperties[i - 1].SetValue(objT, DateTime.MinValue);
                            else
                                myProperties[i - 1].SetValue(objT,
                                    GetSafePropertyValue(myProperties[i - 1].PropertyType, cellValue));
                        }

                        toReturn.Add(objT);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(
                            "Failed parsing input file at row " + rowNum + ", column " + currentColumn + ". " +
                            ex.Message, ex);
                    }
                }

                return toReturn;
            }
        }

        public List<string> GetColumnNames(string fileName, int worksheetIndex)
        {
            log.Info("Entered GetColumnNames(string fileName, int worksheetIndex)");
            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return GetColumnNames(stream, worksheetIndex);
            }
        }

        public List<string> GetColumnNames(Stream file, int worksheetIndex)
        {
            log.Info("Entered GetColumnNames(Stream file, int worksheetIndex)");
            using (var pck = new ExcelPackage())
            {
                List<string> listColumnNames = new List<string>();
                log.Info("Loading file into pck");
                pck.Load(file);
                log.Info("Getting worksheet from pck");
                var ws = pck.Workbook.Worksheets[worksheetIndex];

                log.Info("Reading column information");
                if (ws.Dimension != null && ws.Dimension.End != null)
                {
                    int lastColumnNumber = ws.Dimension.End.Column;
                    var wsRow = ws.Cells[1, 1, 1, lastColumnNumber];
                    for (var columnIndex = 1; columnIndex <= lastColumnNumber; columnIndex++)
                    {
                        string columnName = wsRow[1, columnIndex].Text;
                        listColumnNames.Add(columnName);
                    }
                }

                return listColumnNames;
            }
        }

        public List<string> GetWorksheetNames(string fileName)
        {
            log.Info("Entered GetWorksheetNames(string fileName)");
            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return GetWorksheetNames(stream);
            }
        }

        public List<string> GetWorksheetNames(Stream file)
        {
            log.Info("Entered GetColumnInfo(Stream file)");
            using (var pck = new ExcelPackage())
            {
                List<string> listWorksheetNames = new List<string>();
                log.Info("Loading file into pck");
                pck.Load(file);
                log.Info("Loading worksheet names");
                int worksheetCount = pck.Workbook.Worksheets.Count;
                for (int i = 1; i <= worksheetCount; i++)
                {
                    listWorksheetNames.Add(pck.Workbook.Worksheets[i].Name);
                }
                return listWorksheetNames;
            }
        }

        public void WriteToExcelFile<T>(string fileName, string sheetName, List<T> values, bool autoFormatTable)
        {
            System.IO.File.WriteAllBytes(fileName, WriteToExcel<T>(sheetName, values, autoFormatTable));
        }

        public void WriteToExcelFile<T>(string fileName, string sheetName, List<T> values, List<string> titles, bool autoFormatTable)
        {
            System.IO.File.WriteAllBytes(fileName, WriteToExcel<T>(sheetName, values, titles, autoFormatTable));
        }

        public byte[] WriteToExcel<T>(string sheetName, List<T> values, bool autoFormatTable)
        {
            PropertyInfo[] members = GetValidProperties(typeof(T));
            List<string> titles = members.Select(p => p.GetCustomAttributes().OfType<FieldTitleAttribute>().Single().Name).ToList();
            List<FieldTitleAttribute> attributes = members.Select(p => p.GetCustomAttributes().OfType<FieldTitleAttribute>().Single()).ToList();
            using (var pck = new ExcelPackage())
            {
                WriteToExcelHelper<T>(pck, sheetName, values, autoFormatTable, members, titles, attributes);
                return pck.GetAsByteArray();
            }
        }

        public byte[] WriteToExcel<T>(string sheetName, List<T> values, List<string> titles, bool autoFormatTable)
        {
            PropertyInfo[] members = GetValidProperties(typeof(T));
            List<string> titlesBackup = members.Select(p => p.GetCustomAttributes().OfType<FieldTitleAttribute>().Single().Name).ToList();
            List<FieldTitleAttribute> attributes = members.Select(p => p.GetCustomAttributes().OfType<FieldTitleAttribute>().Single()).ToList();
            if (titles.Count == titlesBackup.Count)
            {
                using (var pck = new ExcelPackage())
                {
                    WriteToExcelHelper<T>(pck, sheetName, values, autoFormatTable, members, titles, attributes);
                    return pck.GetAsByteArray();
                }
            }
            else
            {
                using (var pck = new ExcelPackage())
                {
                    WriteToExcelHelper<T>(pck, sheetName, values, autoFormatTable, members, titlesBackup, attributes);
                    return pck.GetAsByteArray();
                }
            }
        }

        public byte[] AppendToExcel<T>(byte[] existingDoc, string sheetName, List<T> values, bool autoFormatTable)
        {
            PropertyInfo[] members = GetValidProperties(typeof(T));
            List<string> titles = members.Select(p => p.GetCustomAttributes().OfType<FieldTitleAttribute>().Single().Name).ToList();
            List<FieldTitleAttribute> attributes = members.Select(p => p.GetCustomAttributes().OfType<FieldTitleAttribute>().Single()).ToList();
            using (var documentStream = new MemoryStream(existingDoc))
            using (var pck = new ExcelPackage(documentStream))
            {
                WriteToExcelHelper<T>(pck, sheetName, values, autoFormatTable, members, titles, attributes);
                return pck.GetAsByteArray();
            }
        }

        public byte[] WriteToExcel<T>(Dictionary<string, List<T>> valuesInSheets, bool autoFormatTable)
        {
            PropertyInfo[] members = GetValidProperties(typeof(T));
            List<string> titles = members.Select(p => p.GetCustomAttributes().OfType<FieldTitleAttribute>().Single().Name).ToList();
            List<FieldTitleAttribute> attributes = members.Select(p => p.GetCustomAttributes().OfType<FieldTitleAttribute>().Single()).ToList();
            using (var pck = new ExcelPackage())
            {
                foreach (var sheetName in valuesInSheets.Keys)
                    WriteToExcelHelper<T>(pck, sheetName, valuesInSheets[sheetName], autoFormatTable, members, titles, attributes);

                return pck.GetAsByteArray();
            }
        }

        public void WriteToExcelHelper<T>(ExcelPackage pck, string sheetName, List<T> values, bool autoFormatTable,
            PropertyInfo[] members, List<string> titles, List<FieldTitleAttribute> attributes,
            bool wrapStringCells = false)
        {
            var ws = pck.Workbook.Worksheets.Add(sheetName);

            for (int i = 0; i < members.Length; i++)
            {
                ws.Cells[1, i + 1].Value = titles[i];

                if (autoFormatTable)
                {
                    ws.Cells[1, i + 1].Style.Font.Bold = true;
                    ws.Cells[1, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(149, 0, 47));

                    switch (attributes[i].TitleStyle)
                    {
                        case TitleStyle.Style1:
                            ws.Cells[1, i + 1].Style.Font.Color.SetColor(System.Drawing.Color.White);
                            break;

                        case TitleStyle.Style2:
                            ws.Cells[1, i + 1].Style.Font.Color.SetColor(System.Drawing.Color.LightGray);
                            break;
                    }
                }
            }

            if (values?.Any() ?? false)
            {
                var style = OfficeOpenXml.Table.TableStyles.None;
                //if (autoFormatTable)
                //    style = OfficeOpenXml.Table.TableStyles.Light1;

                // Now write, but only members that have FieldTitleAttribute
                ws.Cells["A2"].LoadFromCollection(values, false, style, BindingFlags.Default, members);

                // Set formatting on cells
                for (var i = 0; i < members.Length; i++)
                {
                    if (members[i].PropertyType == typeof(DateTime) || members[i].PropertyType == typeof(DateTime?))
                    {
                        ws.Cells[2, i + 1, values.Count + 1, i + 1].Style.Numberformat.Format = "MM/dd/yyyy";
                        //ws.Column(i+1).Style.Numberformat.Format = "dd/MM/yyyy";
                    }
                    else if (members[i].PropertyType == typeof(string))
                    {
                        ws.Cells[2, i + 1, values.Count + 1, i + 1].Style.Numberformat.Format = "@";
                        ws.Cells[2, i + 1, values.Count + 1, i + 1].Style.WrapText = wrapStringCells;
                        //ws.Column(i + 1).Style.Numberformat.Format = "@";
                    }
                    else if (members[i].PropertyType == typeof(decimal) || members[i].PropertyType == typeof(decimal?) || members[i].PropertyType == typeof(double) || members[i].PropertyType == typeof(double?))
                    {
                        switch (attributes[i].ColumnType)
                        {
                            case ColumnType.Dollars:
                                ws.Cells[2, i + 1, values.Count + 1, i + 1].Style.Numberformat.Format = "$#,###;($#,###);;\"NaN\"";
                                break;
                            case ColumnType.DollarsInThousands:
                                ws.Cells[2, i + 1, values.Count + 1, i + 1].Style.Numberformat.Format = "$#,##0,\"k\"; -$#,##0,\"k\"";
                                break;
                            case ColumnType.Percent:

                                ws.Cells[2, i + 1, values.Count + 1, i + 1].Style.Numberformat.Format = "0.00%;-0.00%;;\"NaN\"";
                                break;
                            case ColumnType.PercentThree:
                                ws.Cells[2, i + 1, values.Count + 1, i + 1].Style.Numberformat.Format = "0.000%;-0.000%;;\"NaN\"";
                                break;
                            case ColumnType.PercentFour:
                                ws.Cells[2, i + 1, values.Count + 1, i + 1].Style.Numberformat.Format = "0.0000%;-0.0000%;;\"NaN\"";
                                break;
                        }
                    }
                    else if (members[i].PropertyType == typeof(int) || members[i].PropertyType == typeof(int?))
                    {
                        if (attributes[i].ColumnType == ColumnType.Count)
                        {
                            ws.Cells[2, i + 1, values.Count + 1, i + 1].Style.Numberformat.Format = "#,##0";
                        }
                    }
                }

                /*
                // Patch
                XmlDocument tabXml = ws.Tables[0].TableXml;
                if (tabXml != null && tabXml.HasChildNodes && tabXml.ChildNodes.Count > 1)
                {
                    foreach (XmlNode tableNode in tabXml.ChildNodes)
                    {
                        if (tableNode.Name == "table")
                        {
                            if (tableNode.Attributes != null && tableNode.Attributes["ref"] != null)
                            {
                                tableNode.Attributes["ref"].Value = string.Format("A1:Z{0}", values.Count + 1);
                            }

                            if (tableNode.HasChildNodes)
                            {
                                bool autoFilterNodeFound = false;
                                foreach (XmlNode autoFilterNode in tableNode.ChildNodes)
                                {
                                    if (autoFilterNode.Name == "autoFilter")
                                    {
                                        autoFilterNodeFound = true;
                                        if (autoFilterNode.Attributes != null && autoFilterNode.Attributes["ref"] != null)
                                        {
                                            autoFilterNode.Attributes["ref"].Value = string.Format("A1:Z{0}", values.Count + 1);
                                        }
                                    }
                                }

                                if (!autoFilterNodeFound)
                                {
                                    XmlNode newNode = tabXml.CreateNode(XmlNodeType.Element, "autoFilter", null);
                                    XmlAttribute newAttribute = tabXml.CreateAttribute("ref");
                                    newNode.Attributes.Append(newAttribute);
                                    newNode.Attributes["ref"].Value = string.Format("A1:Z{0}", values.Count + 1);
                                    tableNode.AppendChild(newNode);
                                }
                            }
                        }
                    }
                }
                */

                // Patch
                if (ws.Tables != null && ws.Tables.Count > 0)
                {
                    XmlDocument tabXml = ws.Tables[0].TableXml;
                    if (tabXml != null && tabXml.HasChildNodes && tabXml.ChildNodes.Count > 1)
                    {
                        foreach (XmlNode tableNode in tabXml.ChildNodes)
                        {
                            if (tableNode.Name == "table")
                            {
                                foreach (XmlNode tableColumnsNode in tableNode.ChildNodes)
                                {
                                    if (tableColumnsNode.Name == "tableColumns")
                                    {
                                        if (tableColumnsNode.ChildNodes != null && tableColumnsNode.ChildNodes.Count == titles.Count)
                                        {
                                            for (int i = 0; i < tableColumnsNode.ChildNodes.Count; i++)
                                            {
                                                if (tableColumnsNode.ChildNodes[i].Name == "tableColumn")
                                                {
                                                    if (tableColumnsNode.ChildNodes[i].Attributes != null && tableColumnsNode.ChildNodes[i].Attributes["id"] != null && tableColumnsNode.ChildNodes[i].Attributes["name"] != null)
                                                    {
                                                        string id = tableColumnsNode.ChildNodes[i].Attributes["id"].Value;
                                                        tableColumnsNode.ChildNodes[i].Attributes["name"].Value = titles[i];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            ws.Cells[ws.Dimension.Address].AutoFitColumns();
        }

        public PropertyInfo[] GetValidProperties(Type type)
        {
            var propertyInfos = type.GetProperties().Where(p => p.GetCustomAttributes(true).OfType<FieldTitleAttribute>().Any()).ToArray();

            return propertyInfos;
        }

        public object GetSafePropertyValue(Type type, string value)
        {
            var nonNullableType = Nullable.GetUnderlyingType(type) ?? type;
            var safeValue = value == null ? null : Convert.ChangeType(value, nonNullableType);

            return safeValue;
        }

        public MemoryStream GetClonedExcelTemplateStream(string templatePath)
        {
            using (var stream = new FileStream(templatePath, FileMode.Open))
            {
                var templateMermoryStream = new MemoryStream();
                stream.CopyTo(templateMermoryStream);
                stream.Close();

                return templateMermoryStream;
            }
        }

        #endregion Methods

    }
}
