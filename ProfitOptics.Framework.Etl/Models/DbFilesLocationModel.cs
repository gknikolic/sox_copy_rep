﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Models
{
    public class DbFilesLocationModel
    {
        public string MdfLocation { get; set; }
        public string LogLocation { get; set; }
    }
}
