USE [Sox]

ALTER TABLE [Sox].[dbo].[CTCaseContainerHistorical] ADD [DueTimestampOriginal] DATETIME2 NULL;
ALTER TABLE [Sox].[dbo].[CTCaseContainerHistorical] ADD [EndTimestampOriginal] DATETIME2 NULL;
ALTER TABLE [Sox].[dbo].[CTCaseContainerHistorical] ADD [UpdateTimestampOriginal] DATETIME2 NULL;
ALTER TABLE [Sox].[dbo].[CTCaseContainerHistorical] ADD [MultipleBasisOriginal] DATETIME2 NULL;

GO