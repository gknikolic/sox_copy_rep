﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.ViewComponents
{
    public class HeaderWidget : ViewComponent
    {
        private readonly IServiceProvider _serviceProvider;

        public HeaderWidget(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public virtual async Task<IViewComponentResult> InvokeAsync()
        {
            var headerWidgetComponent = _serviceProvider.GetServices<IHeaderWidget>()
                .OrderByDescending(x => x.OrderNumber()).FirstOrDefault();

            if (headerWidgetComponent != null)
            {
                return await headerWidgetComponent.InvokeAsync();
            }

            return new ViewViewComponentResult();

            //return View();
        }
    }
}
