USE [SoxQATest]
ALTER TABLE [dbo].[CTFacility] ADD CreatedAtUtc DateTime NOT NULL;
ALTER TABLE [dbo].[CTFacility] ADD ModifiedAtUtc DateTime NULL;