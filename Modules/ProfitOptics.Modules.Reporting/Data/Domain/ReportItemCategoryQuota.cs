﻿namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportItemCategoryQuota
    {
        public int Id { get; set; }

        public int SalesRepId { get; set; }

        public int ItemCategoryId { get; set; }

        public string Month { get; set; }

        public int Year { get; set; }

        public decimal Revenue { get; set; }

        public virtual ReportHierarchySalesRep ReportHierarchySalesRep { get; set; }

        public virtual ReportItemCategory ReportItemCategory { get; set; }
    }
}
