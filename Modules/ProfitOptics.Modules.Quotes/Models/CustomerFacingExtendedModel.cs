﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Quotes.Models
{
    public partial class PrintModel
    {
        public class CustomerFacingExtendedModel
        {
            public DateTime? Date { get; set; }
            public string SalesRepName { get; set; }
            public string SalesRepEmail { get; set; }
            public string SalesRepPhone { get; set; }

            public string ManagerName { get; set; }
            public string ManagerEmail { get; set; }
            public string ManagerPhone { get; set; }

            public string HeaderComment { get; set; }
            public string QuoteToAddress1 { get; set; }
            public string QuoteToAddress2 { get; set; }
            public string QuoteToAddress3 { get; set; }
            public string CustomerNumber { get; set; }
            public decimal? TotalAmount { get; set; }
            public List<ItemModel> Items { get; set; }
            public bool IsNotExtended { get; set; }
        }
    }
}