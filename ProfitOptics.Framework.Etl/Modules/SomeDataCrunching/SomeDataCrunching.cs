﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using Microsoft.Extensions.Logging;
using ProfitOptics.Framework.Etl.DataLayer;
using ProfitOptics.Framework.Etl.Helpers;
using Serilog;

namespace ProfitOptics.Framework.Etl.Modules.SomeDataCrunching
{
    [Verb("some_data_crunching", HelpText = "An example to demonstrate multiple commands that ETL can handle.")]
    public class SomeDataCrunchingOptions : IOptions
    {
        [Option('i', "input", Required = true, HelpText = "Input file.")]
        public string InputFile { get; set; }

        [Option('o', "output", Required = true, HelpText = "Output file.")]
        public string OutputFile { get; set; }
    }

    class SomeDataCrunching : IModule
    {
        private readonly IEtlHelper _etlHelper;
        private readonly Entities _data;
        private readonly Settings _settings;
        public SomeDataCrunching(IEtlHelper etlHelper, Entities data, Settings settings)
        {
            _etlHelper = etlHelper;
            _data = data;
            _settings = settings;
        }

        public int Execute(IOptions opts)
        {
            // Getting command line parameters
            var options = opts as SomeDataCrunchingOptions;

            // Execute some EF query
            //var results = _data.AspNetUser.Where(...standard ef linq...).ToList();

            // Execute some raw sql query from a folder.
            _etlHelper.ExecuteSql("SomeDataCrunching", "SomeDataCrunchingQueries.sql"); // This file is set to "Copy Always" during build. All SQL files are in the <root>/SQL folder

            // Logging actions as we go          
            Log.Information("test");

            // Or simply throw exception that will be caught up upstream, logged and then notified via task scheduler
            // throw new Exception("Something is messed up.");

            // We are good.
            return 0;
        }
    }
}
