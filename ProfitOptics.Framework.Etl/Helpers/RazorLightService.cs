﻿using RazorLight;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public class RazorLightService : IRazorLightService
    {
        private IRazorLightEngine _engine;
        private readonly string currentDirectory;

        public RazorLightService()
        {
            currentDirectory = Path.Combine(Environment.CurrentDirectory, "Modules", "RazorLight", "Templates");
            InitRazorLightEngine();
        }

        /// <summary>
        /// Compiles and renders the template
        /// </summary>
        /// <param name="template">Template name</param>
        /// <param name="model">Template model</param>
        /// <returns>Rendered template as a string</returns>
        public async Task<string> RenderTemplate(string template, object model)
        {
            var result = await _engine.CompileRenderAsync(Path.Combine(currentDirectory, template), model);
            return result;
        }

        private void InitRazorLightEngine()
        {
            _engine = new RazorLightEngineBuilder()
              .UseFileSystemProject(currentDirectory)
              .UseMemoryCachingProvider()
              .Build();            
        }
    }
}
