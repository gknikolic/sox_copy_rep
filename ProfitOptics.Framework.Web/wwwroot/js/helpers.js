﻿(function (helpers, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    // Public properties
    helpers.somePublicProperty = false;

    // Public Methods
    helpers.encodeHtml = function(data) {
        return $("<div/>").text(data).html();
    };

    helpers.decodeHtml = function (data) {
        return $("<div/>").html(data).text();
    };

    helpers.formatMoney = function(value, decimals, defaultValue, formatStringPositive, formatStringNegative) {
        if (value == null) {
            if (defaultValue == null)
                return "";

            value = defaultValue;
        }

        var formatString = "{0}";
        if (value < 0 && formatStringNegative != null)
            formatString = formatStringNegative;
        else if (formatStringPositive != null)
            formatString = formatStringPositive;

        return String.format(formatString, value.formatMoney(decimals));
    };

    helpers.formatNumber = function(value, decimals, defaultValue, formatStringPositive, formatStringNegative) {
        if (value == null) {
            if (defaultValue == null)
                return "";

            value = defaultValue;
        }

        var formatString = "{0}";
        if (value < 0 && formatStringNegative != null)
            formatString = formatStringNegative;
        else if (formatStringPositive != null)
            formatString = formatStringPositive;

        return String.format(formatString, value.formatNumber(decimals));
    };

    helpers.formatPercent = function(value, decimals, defaultValue, formatStringPositive, formatStringNegative) {
        if (value == null) {
            if (defaultValue == null)
                return "";

            value = defaultValue;
        }

        var formatString = "{0}";
        if (value < 0 && formatStringNegative != null)
            formatString = formatStringNegative;
        else if (formatStringPositive != null)
            formatString = formatStringPositive;

        return String.format(formatString, value.formatPercent(decimals));
    };

    helpers.getTotalFromArray = function (arr, propName) {
        var total = 0;

        $.each(arr,
            function (i, val) {
                total += val[propName];
            });

        return total;
    };

    // Private Methods
    function someMethod(item) {
    }
}(window.helpers = window.helpers || {}, jQuery));