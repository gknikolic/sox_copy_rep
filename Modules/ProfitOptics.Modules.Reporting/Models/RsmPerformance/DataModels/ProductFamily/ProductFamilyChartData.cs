﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily
{
    public class ProductFamilyChartData : GenericChartData
    {
        public int ProductFamilyId { get; set; }
        public string ProductFamilyName { get; set; }
    }
}