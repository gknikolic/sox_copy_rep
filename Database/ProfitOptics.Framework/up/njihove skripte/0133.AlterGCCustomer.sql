USE [Sox]

ALTER TABLE [Sox].[dbo].[GCCustomer] ADD [APEmail] NVARCHAR(100) NULL;
ALTER TABLE [Sox].[dbo].[GCCustomer] ADD [APPhone] NVARCHAR(100) NULL;
ALTER TABLE [Sox].[dbo].[GCCustomer] ADD [APContactName] NVARCHAR(100) NULL;

GO