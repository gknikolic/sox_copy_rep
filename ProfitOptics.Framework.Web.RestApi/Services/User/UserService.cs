﻿using ProfitOptics.Framework.Web.RestApi.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.Web.RestApi.Services.User
{
    public class UserService : IUserService
    {
        private readonly ApiEntitiesContext _context;

        public UserService(ApiEntitiesContext context)
        {
            _context = context;
        }

        public async Task<ProfitOptics.Framework.Web.RestApi.DataLayer.AspNetUsers> FindByUsernameAsync(string username)
        {
            return await _context.AspNetUsers.Where(p => p.UserName == username).SingleOrDefaultAsync();
        }

        public async Task<List<string>> GetUserRoles(string username)
        {
            return await (from u in _context.AspNetUsers
                          from ur in _context.AspNetUserRoles.Where(p => p.UserId == u.Id)
                          from r in _context.AspNetRoles.Where(p => p.Id == ur.RoleId)
                          where u.UserName == username
                          select r.Name).ToListAsync();
        }
    }
}
