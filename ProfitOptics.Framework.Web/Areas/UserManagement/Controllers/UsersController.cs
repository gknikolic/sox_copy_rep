﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Email;
using ProfitOptics.Framework.Web.Areas.UserManagement.Factories;
using ProfitOptics.Framework.Web.Areas.UserManagement.Services;
using ProfitOptics.Framework.Web.Helpers.Extensions;
using ProfitOptics.Framework.Web.Models.Common;
using System.Threading.Tasks;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Areas.Account.Models;
using ProfitOptics.Framework.Web.Areas.UserManagement.Models;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Helpers.Anotations;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Models;
using IAuthenticationService = ProfitOptics.Framework.Web.Framework.Services.IAuthenticationService;
using IUserService = ProfitOptics.Framework.Web.Framework.Services.IUserService;
using LargeJsonResult = ProfitOptics.Framework.Web.Framework.Models.LargeJsonResult;
using System.Net;
using System.Linq;
using System;
using System.Collections.Generic;
using ProfitOptics.Modules.Sox.Services;
using ProfitOptics.Modules.Sox.Factories;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Modules.Sox.Models.ViewModels;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Controllers
{
    [Area("UserManagement")]
    [Authorize(Roles = "superadmin,admin")]
    public class UsersController : BaseController
    {
        #region Fields

        private readonly IUserManagementModelFactory _userManagementModelFactory;
        private readonly IUserService _userService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IEmailer _emailer;
        private readonly Common _commonSettings;
        private readonly IStringResourceRepository _stringResourceRepository;
        private readonly IUserManagementService _userManagementService;
        private readonly ISoxService _soxService;
        private readonly ISoxFactory _soxFactory;
        private readonly QuickBooksOptions _quickBooksOptions;

        #endregion

        #region Constructor 

        public UsersController(IUserManagementModelFactory userManagementModelFactory,
            IUserService userService,
            IEmailer emailer,
            IStringResourceRepository stringResourceRepository,
            IUserManagementService userManagementService,
            IAuthenticationService authenticationService,
            Common commonSettings,
            ISoxService _soxService,
            ISoxFactory _soxFactory,
            QuickBooksOptions quickBooksOptions)
        {
            this._userManagementModelFactory = userManagementModelFactory;
            this._userService = userService;
            this._emailer = emailer;
            this._stringResourceRepository = stringResourceRepository;
            this._userManagementService = userManagementService;
            this._authenticationService = authenticationService;
            this._commonSettings = commonSettings;
            this._soxService = _soxService;
            this._soxFactory = _soxFactory;
            _quickBooksOptions = quickBooksOptions;
        }

        #endregion

        #region Users list and user actions

        public IActionResult Index(AddUserModel registerModel)
        {
            registerModel.RegisterModelRoles = _userManagementModelFactory.PrepareUserRolesSelectListItems();

            var gcParentCompanies = _userService.GetGCParentCompanies();

            registerModel.GCParentCompanies = gcParentCompanies;

            //accept ShowModal info from SaveUser action and forward to view via ViewBag
            if (TempData["showModal"] != null)
                ViewBag.ShowModel = TempData["showModal"];
            else
                ViewBag.ShowModel = false;

            return View(registerModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult GetAllUsers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request)
        {
            var models = _userManagementModelFactory.CreateUserModels(request);
            return new LargeJsonResult(models.ToDataTablesResponse(request));
        }

        [HttpGet]
        public async Task<IActionResult> ImpersonateUser(int userId)
        {
            await _authenticationService.StartImpersonatingSessionAsync(userId);

            return RedirectToAction("Index", "Home", new { area = "" });
        }

        [HttpGet]
        public async Task<IActionResult> FinishImpersonatingSession()
        {
            if (HttpContext.User.IsImpersonating())
            {
                var userPrincipal = await _authenticationService.FinishImpersonatingSessionAsync();
                if (userPrincipal == null)
                {
                    ModelState.AddModelError("", "There was an error finishing your impersonating session. Please, log in to continue.");
                    Redirect("/login");
                }
            }
            return RedirectToAction("Index", "Home", new { area = "" });
        }

        [HttpPost]
        [AjaxCall]
        public async Task<IActionResult> AjaxToggleUserEnabled(int userId)
        {
            var result = await _userManagementService.ToggleUserEnabledAsync(userId);
            if (result.Succeeded)
            {
                return Json(new
                {
                    Status = "ok"
                });
            }

            return Json(new
            {
                Status = "error",
                Message = result.Error
            });
        }

        [HttpPost]
        [AjaxCall]
        public async Task<IActionResult> AjaxToggleUserLockAsync(int userId)
        {
            var result = await _userManagementService.ToggleUserLockAsync(userId);

            return Json(new
            {
                Status = "ok"
            });
        }

        [HttpPost]
        [AjaxCall]
        public async Task<IActionResult> AjaxResetPassword(int userId)
        {
            var passwordResetResult = await _userManagementService.AjaxResetPassword(userId, Request.Scheme);
            if (!passwordResetResult.Succeeded)
            {
                return Json(new
                {
                    Status = "error",
                    Message = passwordResetResult.Error
                });
            }

            var emailBody = await this.RenderViewAsync<string>("_EmailResetPassword", passwordResetResult.Url, partial: true);
            var emailSubject = _stringResourceRepository.GetResource(ResourceKeys.EmailSubjectsResetPassword);
            var sent = await _emailer.SendEmailAndWriteToDbAsync(passwordResetResult.EmailTo, emailSubject, emailBody,
                isHtml: true);
            var status = sent ? "ok" : "error";

            return Json(new
            {
                Status = status
            });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> AddNewUser(AddUserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                userModel.ShouldShowModal = true;

                return RedirectToAction("Index", userModel);
            }

            //if (userModel.Email != null)
            //{
            //    var user = _userService.FindUserByEmailAsync(userModel.Email);
            //    if(user != null)
            //    {
            //        //return 
            //    }
            //}

            var createUserResult = await _authenticationService.RegisterUserAsync(userModel.FirstName, userModel.LastName,
                userModel.Email, string.Empty, userModel.PhoneNumber, Request.Scheme);

            if (createUserResult.Succeeded)
            {
                await _userService.EnableAndAddRolesToUser(userModel.Email, userModel.SelectedUserRoles);
            }
            else
            {
                ModelState.AddModelError("", createUserResult.Error);

                return RedirectToAction("Index", userModel);
            }

            var appUser = _userService.FindUserByEmailAsync(userModel.Email).Result;
            var token = await _userService.GeneratePasswordResetTokenAsync(appUser);

            if (userModel.SelectedGCParentCompanies != null && userModel.SelectedGCParentCompanies.Count > 0)
            {
                _userService.AddUserToGCParentCompany(appUser.Id, userModel.SelectedGCParentCompanies.Select(x => Int32.Parse(x)));
            }

            var url = Url.RouteUrl(POFrameworkDefaults.AddPasswordRouteName,
                new { userId = appUser.Id, code = WebUtility.UrlEncode(token), email = userModel.Email }, Request.Scheme);

            var emailBody = await this.RenderViewAsync<string>("_EmailConfirmationRegistration", url, partial: true);

            _emailer.SendEmailAndWriteToDb(userModel.Email, _stringResourceRepository.GetResource(ResourceKeys.EmailSubjectsConfirmEmail), emailBody, isHtml: true);

            return RedirectToAction("Index", new AddUserModel());
        }

        [AllowAnonymous]
        public IActionResult AddPassword(string code, string email)
        {
            return View();
        }
        public IActionResult CompanyManagement()
        {
            ParentsWithChildCompaniesModel model = new ParentsWithChildCompaniesModel();

            var parentCompanies = _userService.GetParentCompanies();
            var parentCompaniesIds = parentCompanies.Select(x => x.Id).ToList();

            var parentCompaniesWithChildren = _userService.GetChildrenForGCParentCompany(parentCompaniesIds);

            //var companiesList = parentCompanies.ToList();

            model.ParentCompanies = new List<ParentCompanyModel>();

            for (int i = 0; i < parentCompanies.Count(); i++)
            {
                ParentCompanyModel parentCompany = new ParentCompanyModel();

                parentCompany.Id = parentCompaniesWithChildren.ParentCompanies[i].Id;
                parentCompany.Title = parentCompaniesWithChildren.ParentCompanies[i].Title;
                parentCompany.ChildCompanies = parentCompaniesWithChildren.ParentCompanies[i].ChildCompanies;

                model.ParentCompanies.Add(parentCompany);
            }

            return View(model);
        }

        public IActionResult PartnerManagement()
        {
            ParentsWithChildCompaniesModel model = new ParentsWithChildCompaniesModel();
            var vendors = _soxService.GetGCVendorsSelectList();
            var parentCompanies = _userService.GetParentCompanies();
            var parentCompaniesIds = parentCompanies.Select(x => x.Id).ToList();

            var parentCompaniesWithChildren = _userService.GetChildrenForGCParentCompany(parentCompaniesIds);

            //var companiesList = parentCompanies.ToList();

            model.ParentCompanies = new List<ParentCompanyModel>();

            for (int i = 0; i < parentCompanies.Count(); i++)
            {
                ParentCompanyModel parentCompany = new ParentCompanyModel();

                parentCompany.Id = parentCompaniesWithChildren.ParentCompanies[i].Id;
                parentCompany.Title = parentCompaniesWithChildren.ParentCompanies[i].Title;
                parentCompany.ChildCompanies = parentCompaniesWithChildren.ParentCompanies[i].ChildCompanies;

                foreach (var cCompany in parentCompany.ChildCompanies)
                {
                    if (cCompany.GCCustomerId != null)
                    {
                        var customer = _soxService.GetCustomer((int)cCompany.GCCustomerId);
                        cCompany.GCCustomer = customer;
                        parentCompany.HasCustomers = true;
                        
                    }
                    else if(cCompany.GCVendorId != null)
                    {
                         var vendor = _soxService.GetGCVendor((int)cCompany.GCVendorId);
                         cCompany.Vendor = vendor;
                         parentCompany.HasVendors = true;
                         
                    }
                }

                model.ParentCompanies.Add(parentCompany);
                
                
            }

            foreach (var pCompany in model.ParentCompanies)
            {

            }
            model.QBCusomerExternalUrl = _quickBooksOptions.InstanceUrl;
            return View(model);
        }

        [HttpGet]
        public IActionResult EditVendorDialog(int vendorId)
        {
            var companies = _userService.GetGCParentCompanies();
            UpdateVendorModel model = new UpdateVendorModel();
            model.Vendor = _soxFactory.PrepareGCVendorModel(vendorId);
            model.Vendor.GCParentCompanies = companies;
            //if (model.Customer.QBBillingType == 0)
            //{
            //    model.Customer.QBBillingType = (int)ProfitOptics.Modules.Sox.Enums.QBBillingType.Tray;
            //}

            return PartialView("Modules/ProfitOptics.Modules.Sox/Views/Sox/_UpdateVendorFromCompanyModal.cshtml", model);
        }

        [HttpPost]
        public IActionResult UpdateVendor(UpdateVendorModel model)
        {
            _soxService.UpdateVendor(model);

            return RedirectToAction("PartnerManagement");
        }

        [HttpGet]
        public IActionResult EditCustomerDialog(int customerId)
        {
            var companies = _userService.GetGCParentCompanies();
            UpdateCustomerModel model = new UpdateCustomerModel();
            model.Customer = _soxFactory.PrepareGCCustomerModel(customerId);
            model.Customer.GCParentCompanies = companies;
            if (model.Customer.QBBillingType == 0)
            {
                model.Customer.QBBillingType = (int)ProfitOptics.Modules.Sox.Enums.QBBillingType.Tray;
            }

            return PartialView("Modules/ProfitOptics.Modules.Sox/Views/Sox/_UpdateCustomerFromCompanyModal.cshtml", model);
        }

        [HttpPost]
        public IActionResult UpdateCustomer(UpdateCustomerModel model)
        {
            _soxService.UpdateCustomer(model);

            return RedirectToAction("PartnerManagement");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> AddPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var token = System.Net.WebUtility.UrlDecode(model.Code);
            await _userService.ConfirmEmail(model.Email, token);
            var result = await _authenticationService.PasswordResetAsync(model.Email, token, model.Password);

            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation");
            }

            ModelState.AddModelError("", result.Error);

            return View(model);
        }

        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        #endregion

        #region Update user dialog

        [HttpPost]
        [AjaxCall(DataType = AjaxCallDataType.Html)]
        public async Task<IActionResult> AjaxUpdateUserDialog(int userId)
        {
            var model = await _userManagementModelFactory.PrepareApplicationUserModelAsync(userId);
            return PartialView("_UpdateUserDialog", model);
        }

        [HttpPost]
        [AjaxCall(DataType = AjaxCallDataType.Json)]
        public async Task<IActionResult> SaveUser(ApplicationUserModel model)
        {
            //Hack:
            ModelState.Remove("model.SelectedParentCompanies");

            if (ModelState.IsValid)
            {
                await _userManagementService.SaveUser(model);
            }

            AddUserModel usersModel = new AddUserModel();
            //usersModel.ShowModal = true;
            //usersModel.Message = "User successful edited!";

            //when user is edited, forward info to Index action to show modal
            TempData["showModal"] = true;

            return Json(new
            {
                Status = "ok"
            });
            //return RedirectToAction("Index", usersModel);
     
        }

        [HttpPost]
        public IActionResult GetParentCompanyWithChildren(int parentCompanyId, string childCompanyPermissionValues)
        {
            var model = _userService.GetParentCompanyWithChilder(parentCompanyId, childCompanyPermissionValues);
            return PartialView("Areas/UserManagement/Views/Users/Components/CompanyPermissionTableRow.cshtml", model);
        }

        [HttpGet]
        [AjaxCall(DataType = AjaxCallDataType.Html)]
        public IActionResult AjaxUpdateCompanyDialog(int parentCompanyId)
        {
            ParentCompanyModel model = _userManagementModelFactory.PrepareParentCompanyModel(parentCompanyId);
            return PartialView("Areas/UserManagement/Views/Companies/_UpdateParentCompanyDialog.cshtml", model);
        }

        //[HttpPost]
        public IActionResult EditParentCompany(ParentCompanyModel model)
        {
            if (ModelState.IsValid)
            {
                _userService.EditParentCompany(model);
            }
            return RedirectToAction("PartnerManagement");
        }

        [HttpGet]
        public IActionResult AddParentCompanyDialog()
        {
            return PartialView("Areas/UserManagement/Views/Companies/_AddNewParentCompanyDialog.cshtml");
        }

        [HttpGet]
        public IActionResult DeleteParentCompanyDialog(int parentCompanyId)
        {
            ViewBag.ParentCompanyId = parentCompanyId;

            var list = new List<int>();
            list.Add(parentCompanyId);

            var companies = _userService.GetChildrenForGCParentCompany(list);

            if (companies.ParentCompanies[0].ChildCompanies.Count() > 0)
            {
                ViewBag.HasChildren = true;
            }
            else
            {
                ViewBag.HasChildren = false;
            }

            return PartialView("Areas/UserManagement/Views/Companies/_DeleteParentCompanyDialog.cshtml");
        }

        [HttpPost]
        public IActionResult AddNewParentCompany(ParentCompanyModel model)
        {
            if (ModelState.IsValid)
            {
                _userService.AddParentCompany(model);
            }
            return RedirectToAction("PartnerManagement");
        }

        [HttpPost]
        public IActionResult DeleteParentCompany(ParentCompanyModel model)
        {
            _userService.DeleteParentCompany(model.Id);

            return RedirectToAction("PartnerManagement");
        }

        [HttpGet]
        [AjaxCall(DataType = AjaxCallDataType.Html)]
        public IActionResult AjaxUpdateChildCompanyDialog(int childCompanyId)
        {
            ChildCompanyModel model = _userManagementModelFactory.PrepareChildCompanyModel(childCompanyId);

            return PartialView("Areas/UserManagement/Views/Companies/_UpdateChildCompanyDialog.cshtml", model);
        }

        [HttpPost]
        public IActionResult EditChildCompany(ChildCompanyModel model)
        {
            if (ModelState.IsValid)
            {
                _userService.EditChildCompany(model);
            }
            return RedirectToAction("CompanyManagement");
        }

        [HttpGet]
        public IActionResult AddChildCompanyDialog(int parentCompanyId)
        {
            ChildCompanyModel model = new ChildCompanyModel()
            {
                ParentCompanyId = parentCompanyId
            };

            var customers = _soxService.GetAllGCCustomers();
            model.Customers = customers.Select(customer => new SelectListItem
            {
                Text = customer.CustomerName,
                Value = customer.Id.ToString()
            });

            var vendors = _soxService.GetVendors();
            model.Vendors = vendors.Select(vendor => new SelectListItem
            {
                Text = vendor.Title,
                Value = vendor.Id.ToString()
            });

            return PartialView("Areas/UserManagement/Views/Companies/_AddNewChildCompanyDialog.cshtml", model);
        }

        [HttpPost]
        public IActionResult AddNewChildCompany(ChildCompanyModel model)
        {
            if (ModelState.IsValid)
            {
                _userService.AddChildCompany(model);
            }
            return RedirectToAction("CompanyManagement");
        }

        [HttpGet]
        public IActionResult DeleteChildCompany(int childCompanyId)
        {
            _userService.DeleteChildCompany(childCompanyId);

            return RedirectToAction("CompanyManagement");
        }
        #endregion
    }
}
