﻿var Statistics = function () {

    var Private = {

        Methods: {
            Calculate: function (url, clickedLink, groupByType, level) {
                blockUI("Getting statistics data...");
                $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            groupByType: groupByType,
                            level: level,
                            value: clickedLink,
                            //pageNum: pageNum,
                            //itemsPerPage: itemsPerPage
                        },
                        success: function (data) {
                            $.unblockUI();

                            if (data.Success) {
                                location.reload();
                            } else {
                                if (data.RedirectToIndex) {
                                    $.unblockUI();
                                    blockUI("Your session has expired. Please wait while we redirect you to the first page.");
                                    var url = $("#goBackToIndexPage").val();
                                    window.location.href = url;
                                }
                                $(".error-info").show().text(data.ErrorMessage);
                                setTimeout(function () {
                                    $(".error-info").hide().text('');
                                }, 5000);
                            }
                        },
                        error: function () {
                            $.unblockUI();
                            $(".error-info").show().text("Error sending data!");
                            setTimeout(function () {
                                $(".error-info").hide().text('');
                            }, 5000);
                        }
                    });
            },

            GroupMaxLevel: function (url, groupByType) {
                blockUI("Getting statistics data...");
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        groupByType: groupByType
                    },
                    success: function (data) {
                        $.unblockUI();

                        if (data.Success) {
                            $(tblStatisticsTable).DataTable().ajax.reload();
                            renderHicharts();
                        } else {
                            if (data.RedirectToIndex) {
                                $.unblockUI();
                                blockUI("Your session has expired. Please wait while we redirect you to the first page.");
                                var url = $("#goBackToIndexPage").val();
                                window.location.href = url;
                            }
                            $.unblockUI();
                            $(".error-info").show().text(data.ErrorMessage);
                            setTimeout(function () {
                                $(".error-info").hide().text('');
                            }, 5000);
                        }
                    },
                    error: function () {
                        $.unblockUI();
                        $(".error-info").show().text("Error sending data!");
                        setTimeout(function () {
                            $(".error-info").hide().text('');
                        }, 5000);
                    }
                });
            },

            ExpandSelectedLocationFilters: function () {
                var locationBreadcrumbs = ".locations-breadcrumbs";
                var firstRootNode = '#jstree3 li[data-rootnode]:first-of-type';
                var allLocations = "All Locations";

                Private.Methods.ExpandSelectedFilters(locationBreadcrumbs, allLocations, firstRootNode);
            },

            ExpandSelectedProductFilters: function () {
                var productBreadcrumbs = ".products-breadcrumbs";
                var lastRootNode = '#jstree3 li[data-rootnode]:last-of-type';
                var allProducts = "All Products";

                Private.Methods.ExpandSelectedFilters(productBreadcrumbs, allProducts, lastRootNode);
            },

            ExpandSelectedFilters: function (breadcrumbs, allNodes, rootNode) {
                $(".breadcrumbs " + breadcrumbs + " span").each(function () {
                    var breadcrumbItem = $(this).find("a").text();
                    var breadcrumbItemSelected = $(this).hasClass("selected");

                    if (breadcrumbItem !== allNodes) {
                        $("#jstree3 .node-name").each(function () {
                            if (breadcrumbItem == $(this).text()) {
                                var id = $(this).parents(":eq(1)").attr("id");
                                $("#jstree3").jstree("open_node", $("#" + id));
                            }
                        })

                        if (breadcrumbItemSelected) {
                            return false;
                        }
                    } else {
                        $("#jstree3").jstree("open_node", $(rootNode));

                        if (breadcrumbItemSelected) {
                            return false;
                        }
                    }
                });
            },

            CheckAllNodes: function () {
                $(".selected-tree-nodes").jstree("check_all");
                $(".initial-tree-nodes").jstree("check_all");
            }
        },

        Events: {
            ViewBySelection: function () {
                $(".groupByClass").on("click", function () {
                    $(this).siblings().removeClass("selected");
                    $(this).addClass("selected");

                    var url = $("#calculateResults").val();
                    var clickedLink = null;
                    var groupByType = $(this).text();
                    var level;
                    //var itemsPerPage = parseInt($(".items-per-page").val());

                    if (groupByType == "Location") {
                        level = parseInt($("#locationLevel").val());
                    } else if (groupByType == "Product") {
                        level = parseInt($("#productLevel").val());
                    }
                    
                    Private.Methods.Calculate(url, clickedLink, groupByType, level);
                });
            },

            GroupByMaxLevel: function () {
                $(".groupByMaxClass").on("click", function () {
                    $(this).siblings().removeClass("selected");
                    $(this).addClass("selected");

                    var url = $("#groupByMaxLevelUrl").val();
                    var groupByType = $(this).text();
                    //var itemsPerPage = parseInt($(".items-per-page").val());

                    if ($(this).attr('id') == "allLocations") {
                        groupByType = "Location";
                        var locationType = $(".location-selection-levels input:checked").attr('id');
                        $(".table-caption").text(locationType);
                    } else if ($(this).attr('id') == "allProducts") {
                        var productType = $(".product-selection-levels input:checked").attr('id');
                        $(".table-caption").text(productType);
                        groupByType = "Product";
                    } 

                    Private.Methods.GroupMaxLevel(url, groupByType);
                });
            },

            BreadcrumbsClick: function() {
                $(".breadcrumbs span a").on("click", function () {

                    var parent = $(this).parents(":eq(1)");
                    var url = $("#calculateResults").val();
                    var value = $(this).text();
                    var level = parseInt($(this).attr("data-level"));
                    //var itemsPerPage = parseInt($(".items-per-page").val());

                    if (parent.hasClass("products-breadcrumbs")) {
                        var groupByType = "Product";
                    } else {
                        var groupByType = "Location";
                    }

                    Private.Methods.Calculate(url, value, groupByType, level);
                });
            },

            GoBackToIndexPage: function () {
                $(".change-dataset-filters").on("click", function () {
                    var url = $("#goBackToIndexPage").val();
                    window.location.href = url;
                });
            },

            TableLinkClick: function () {
                $("body").on("click", "#tblStatisticsTable a", function () {
                    var url = $("#calculateResults").val();
                    var clickedLink = $(this).next("input").val();
                    var groupByType = $("#groupBy").val();
                    var level;
                    //var itemsPerPage = parseInt($(".items-per-page").val());

                    if (groupByType == "Location") {
                        level = parseInt($("#locationLevel").val()) + 1;
                    } else if (groupByType == "Product") {
                        level = parseInt($("#productLevel").val()) + 1;
                    }
                    
                    Private.Methods.Calculate(url, clickedLink, groupByType, level);
                });
            },

            DownloadButtonClick: function() {
                $(".downloadXlsx").on("click", function () {
                    var url = $("#checkDownloadStatusUrl").val();
                    var redirectUrl = $("#downloadXlsxUrl").val();
                    blockUI("Preparing data...");
                    var checkDownload = setInterval(function() {
                        $.ajax({
                            type: "POST",
                            url: url,
                            success: function(data) {
                                if (data.Downloaded === true) {
                                    $.unblockUI();
                                    clearInterval(checkDownload);
                                }
                            },
                            error: function() {

                            }
                        });
                    }, 500);
                    window.location = redirectUrl;
                });
            },

            DownloadRawButtonClick: function() {
                $(".downloadRawXlsx").on("click", function() {
                    var url = $("#checkDownloadStatusUrl").val();
                    var redirectUrl = $("#downloadRawXlsxUrl").val();
                    blockUI("Preparing data...");
                    var checkDownload = setInterval(function() {
                        $.ajax({
                            type: "POST",
                            url: url,
                            success: function(data) {
                                if (data.Downloaded === true) {
                                    $.unblockUI();
                                    clearInterval(checkDownload);
                                }
                            },
                            error: function() {

                            }
                        });
                    }, 1000);
                    window.location = redirectUrl;
                });
            },

            DownloadRawDatabridgeButtonClick: function () {
                $(".downloadRawDataBridgeXlsx").on("click", function () {
                    var url = $("#checkDownloadStatusUrl").val();
                    var redirectUrl = $("#downloadRawDatabridgeXlsxUrl").val();
                    blockUI("Preparing data...");
                    var checkDownload = setInterval(function () {
                        $.ajax({
                            type: "POST",
                            url: url,
                            success: function (data) {
                                if (data.Downloaded === true) {
                                    $.unblockUI();
                                    clearInterval(checkDownload);
                                }
                            },
                            error: function () {

                            }
                        });
                    }, 1000);
                    window.location = redirectUrl;
                });
            },

            SortingTable: function () {
                $("#tblStatisticsTable thead th").on("click", function () {
                    var clickedHeader = $(this);
                    var sortingDirection = clickedHeader.attr("data-asc");
                    var sortingDirection = clickedHeader.attr("data-caption");
                    var url = $("#applyOrderfilter").val();

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            orderByColumn: sortingDirection,
                            orderDirection: sortingDirection,
                        },
                        success: function (data) {
                            $.unblockUI();

                            if (data.Success) {
                                location.reload();
                            } else {
                                
                            }
                        },
                        error: function () {

                        }
                    });
                });
            },

            PredefinedFiltersClick: function () {
                $(".compare-periods-header").on("click", function () {
                    var currentHeader = $(this);
                    currentHeader.toggleClass("selected");
                    if (currentHeader.hasClass("selected")) {
                        currentHeader.closest(".tab-filter").siblings(":not('button')").hide();
                        currentHeader.show();
                        currentHeader.siblings().show();
                    } else {
                        currentHeader.closest(".tab-filter").siblings().show();
                        currentHeader.show();
                        currentHeader.siblings().hide();
                    }
                });
            },
        }
    };

    var Public = {
        Init: function () {
            Private.Methods.ExpandSelectedLocationFilters();
            Private.Methods.ExpandSelectedProductFilters();
            Private.Events.ViewBySelection();
            Private.Events.GroupByMaxLevel();
            Private.Events.GoBackToIndexPage();
            Private.Events.TableLinkClick();
            Private.Events.BreadcrumbsClick();
            Private.Events.DownloadButtonClick();
            Private.Events.DownloadRawButtonClick();
            Private.Events.DownloadRawDatabridgeButtonClick();
            Private.Events.PredefinedFiltersClick();
            //Private.Events.SortingTable();
        }
    };
    return Public;
}();

