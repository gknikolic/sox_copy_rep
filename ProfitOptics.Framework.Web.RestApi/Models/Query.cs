using System;

namespace ProfitOptics.Framework.Web.RestApi.Models
{
    public class Query
    {
        public int Page { get; protected set; }
        public int ItemsPerPage { get; protected set; }
        public string SortColumn { get; protected set; }
        public string SortDirection { get; protected set; }
        public string FilterColumn { get; protected set; }
        public string FilterValue { get; protected set; }

        public Query(int page, int itemsPerPage, string sortColumn, string sortDirection, string filterColumn, string filterValue)
        {
            Page = page;
            ItemsPerPage = itemsPerPage;

            if (Page <= 0)
            {
                Page = 1;
            }

            if (ItemsPerPage <= 0)
            {
                ItemsPerPage = 10;
            }

            SortColumn = sortColumn;
            SortDirection = !string.IsNullOrEmpty(sortDirection) ? sortDirection.ToLower() : sortDirection;

            if ((!string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortDirection)) || (string.IsNullOrEmpty(SortColumn) && !string.IsNullOrEmpty(SortDirection)))
            {
                throw new ArgumentException("Sorting options were not properly initialized.");
            }

            if (!string.IsNullOrEmpty(SortDirection) && SortDirection.ToLower() != "asc" && SortDirection != "desc")
            {
                throw new ArgumentException(@"Sorting direction format issue. Allowed values are ""asc"" or ""desc"".");
            }

            FilterColumn = filterColumn;
            FilterValue = !string.IsNullOrEmpty(filterValue) ? filterValue.ToLower() : filterValue;

            if ((!string.IsNullOrEmpty(FilterColumn) && string.IsNullOrEmpty(FilterValue)) || (string.IsNullOrEmpty(FilterColumn) && !string.IsNullOrEmpty(FilterValue)))
            {
                throw new ArgumentException("Filtering options were not properly initialized.");
            }
        }

        
    }
}