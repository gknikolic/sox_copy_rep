﻿using System;

namespace ProfitOptics.Framework.DataLayer
{
    public partial class POActionAudit
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Url { get; set; }
        public string Status { get; set; }
        public string IpAddress { get; set; }
        public string Browser { get; set; }
        public DateTime CreateTime { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string ReflectedType { get; set; }
        public string ReflectedActionName { get; set; }
    }
}
