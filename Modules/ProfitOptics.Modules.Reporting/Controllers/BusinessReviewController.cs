﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Core.System;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Factories.BusinessReview;

namespace ProfitOptics.Modules.Reporting.Controllers
{
    [Authorize(Roles = "admin, reporting")]
    public class BusinessReviewController : BaseReportingController
    {
        #region Fields

        private readonly IBusinessReviewModelFactory _businessReviewModelFactory;
        private readonly IPrinter _printer;
        private readonly ITempFile _tempFile;

        #endregion

        #region Ctor

        public BusinessReviewController(IBusinessReviewModelFactory businessReviewModelFactory,
                                        IPrinter printer,
                                        ITempFile tempFile)
        {
            _businessReviewModelFactory = businessReviewModelFactory;
            _printer = printer;
            _tempFile = tempFile;
        }

        #endregion

        #region Methods

        // GET: Reporting/BusinessReview
        public IActionResult Index()
        {
            var currentUserId = GetCurrentUserId();

            var model = _businessReviewModelFactory.PrepareBusinessReviewViewModel(currentUserId);

            return View(model);
        }

        [HttpPost]
        public IActionResult GetBusinessReviewDropdownDataForReport(BusinessReviewReportType reportType, string search, int page = 0, int take = int.MaxValue)
        {
            var userId = GetCurrentUserId();

            var model = _businessReviewModelFactory.PrepareBusinessReviewDropdownModelForReportType(userId, reportType, search, page, take);

            return Json(new
            {
                model.results,
                model.count_filtered,
            });
        }

        public IActionResult DownloadBusinessReviewExcel(int customerId, BusinessReviewReportType reportType, string notes)
        {
            var model = _businessReviewModelFactory.PrepareBusinessReviewReportModel(customerId, reportType, notes)
;
            return File(model.Stream, model.ContentType, model.FileName);
        }

        public async Task<IActionResult> DownloadBusinessReviewPdf(int customerId, BusinessReviewReportType reportType, string notes)
        {
            var model = _businessReviewModelFactory.PrepareBusinessReviewReportModel(customerId, reportType, notes);

            model.FileName = model.FileName.Replace("xlsx", "pdf");

            string reportFileName = _tempFile.GetTempFileForUser(GetCurrentUserId(), model.FileName);

            using (MemoryStream ms = await _printer.ConvertToPDF(model.Stream, "Report for customer:" + customerId, "xls"))
            using (FileStream fs = System.IO.File.Create(reportFileName))
            {
                ms.Seek(0, SeekOrigin.Begin);
                ms.CopyTo(fs);
            }

            var filePreviewUrl = Url.Action("DownloadFileForUser", "BaseReporting", new { area = "Reporting", fileName = model.FileName, downloadOrPreview = "preview" }, Request.Scheme);

            return PartialView("_PDFPreview", filePreviewUrl);
        }

        #endregion
    }
}
