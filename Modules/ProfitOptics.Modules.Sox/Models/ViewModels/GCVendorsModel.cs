﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCVendorsModel
    {
        public List<GCVendorModel> Vendors { get; set; }
        public string QBCusomerExternalUrl { get; set; }
    }
}
