﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data.Config
{
    public class QTHierarchySalesRepConfig : IEntityTypeConfiguration<QTHierarchySalesRep>
    {
        public void Configure(EntityTypeBuilder<QTHierarchySalesRep> builder)
        {
            builder.ToTable("QTHierarchySalesRep");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Name).HasMaxLength(50).IsRequired();
        }
    }
}
