﻿(function (page, $, undefined) {

    app.activeMenus = ["menuAdmin", "menuAdminAnnouncements"]; // List all menus that should be activated when this page is opened

    $("#AnnouncementForm").validate({
        rules: {
            "Title": { required: true },
            "ValidUntil": { required: true }
        },
        messages: {
            "Title": { required: "Please Provide Title" },
            "ValidUntil": { required: "Please Provide Valid Until" }
        },
        showErrors: function (errorMap, errorList) {
            app.formValidationErrorHandler(this, errorMap, errorList)
        }
    });

})(window.page = window.page || {}, jQuery);