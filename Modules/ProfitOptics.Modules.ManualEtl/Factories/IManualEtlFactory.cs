﻿using System.Collections.Generic;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.ManualEtl.Models;

namespace ProfitOptics.Modules.ManualEtl.Factories
{
    public interface IManualEtlFactory
    {
        PagedList<EtlLogDataModel> GetEtlLogDataModels(IDataTablesRequest request, string source);

        ManualUploadViewModel GetManualUploadViewModel();

        List<DropdownItemModel> GetEtlSourceDropdownList();
    }
}