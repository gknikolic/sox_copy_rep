﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Framework.Web.Helpers.Extensions;
using ProfitOptics.Framework.Web.Models;
using ProfitOptics.Framework.Web.Services;

namespace ProfitOptics.Framework.Web.Factories
{
    public class LayoutModelFactory : ILayoutModelFactory
    {
        private readonly HttpContext HttpContext;
        private readonly IUserService _userService;

        public LayoutModelFactory(IHttpContextAccessor httpContextAccessor,
           IUserService userService)
        {
            this.HttpContext = httpContextAccessor.HttpContext;
            this._userService = userService;
        }

        public async Task<HeaderAccountMenuModel> PrepareHeaderAccountMenuModelAsync()
        {
            return new HeaderAccountMenuModel
            {
                FullName = HttpContext.User.GetFullName(),
                MemeberSince = HttpContext.User.GetRegistrationDateUtc() > DateTime.MinValue ? HttpContext.User.GetRegistrationDateUtc().ToShortDateString() : "Forever",
                IsImpersonating = HttpContext.User.IsImpersonating(),
                Base64ProfilePicture = Convert.ToBase64String(await _userService.GetProfilePictureAsync(HttpContext.User.GetUserId()))
            };
        }

        public async Task<SideBarModel> PrepareSideBarModelAsync()
        {
            return new SideBarModel
            {
                FullName = HttpContext.User.GetFullName(),
                Base64ProfilePicture = Convert.ToBase64String(await _userService.GetProfilePictureAsync(HttpContext.User.GetUserId()))
            };
        }
    }
}
