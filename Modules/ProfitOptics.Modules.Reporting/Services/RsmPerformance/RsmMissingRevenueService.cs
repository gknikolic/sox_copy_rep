﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ItemCategory;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.RegionTable;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel;

namespace ProfitOptics.Modules.Reporting.Services.RsmPerformance
{
    public class RsmMissingRevenueService : IRsmMissingRevenueService
    {
        #region Fields

        private readonly Entities _context;
        private readonly IDaysOffService _daysOffService;

        #endregion Fields

        #region Ctor

        public RsmMissingRevenueService(Entities context, IDaysOffService daysOffService)
        {
            _context = context;
            _daysOffService = daysOffService;
        }

        #endregion Ctor

        #region Methods

        public SalesChannelQuotaPerformanceDataModel GetSalesChannelQuotaPerformanceModelForSalesRepId(string salesChannelName,
            MonthDetails monthDetails,
            int salesRepId = 0,
            int year = 0,
            int month = 0)
        {
            ReportCustomerClass salesChannel = _context.ReportCustomerClasses.Single(x => x.Name == salesChannelName);

            List<ReportCustomerClassQuota> quotas = _context.ReportCustomerClassQuotas
                .Where(x => (year == 0 || x.Year == year) &&
                            (salesRepId == 0 || x.SalesRepId == salesRepId) &&
                            x.CustomerClassId == salesChannel.Id)
                .ToList();

            var model = new SalesChannelQuotaPerformanceDataModel
            {
                SalesChannelId = salesChannel.Id,
                SalesChannelName = salesChannel.Name,
                MtdActualRevenue = 0.0m,
                MtdTotalMonthQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) : 0.0m,
                MtdDailyQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? (monthDetails.WorkDaysCount == 0 ? 0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) / monthDetails.WorkDaysCount) : 0.0m,
                ProjectedAvgRevPerDay = 0.0m,
                ProjectedRevRemainingDays = 0.0m,
                YtdActualRevenue = 0.0m,
                YtdQuota = quotas.Any() ? (monthDetails.PreviousMonthsNames.Any() ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month)).Sum(x => x.Revenue) : 0.0m) +
                    (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                        month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                        (monthDetails.WorkDaysCount == 0 ? 0 :
                            quotas.Where(z => z.Month == monthDetails.CurrentMonthName)
                                .Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount :
                        0.0m)
                    : 0.0m
            };

            return model;
        }

        public ProductFamilyQuotaPerformanceDataModel GetProductFamilyQuotaPerformanceModelForSalesRepId(string productFamilyName,
            MonthDetails monthDetails,
            int salesRepId = 0,
            int year = 0,
            int month = 0)
        {
            // Will throw if nothing is found, which is desired.
            ReportVendor productFamily = _context.ReportVendor.Single(x => x.Name == productFamilyName);

            List<ReportVendorQuota> quotas = _context.ReportVendorQuotas
                .Where(x => (year == 0 || x.Year == year) &&
                            (salesRepId == 0 || x.SalesRepId == salesRepId) &&
                            x.VendorId == productFamily.Id)
                .ToList();

            var model = new ProductFamilyQuotaPerformanceDataModel
            {
                ProductFamilyId = productFamily.Id,
                ProductFamilyName = productFamily.Name,
                MtdActualRevenue = 0.0m,
                MtdTotalMonthQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) : 0.0m,
                MtdDailyQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? (monthDetails.WorkDaysCount == 0 ? 0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) / monthDetails.WorkDaysCount) : 0.0m,
                ProjectedAvgRevPerDay = 0.0m,
                ProjectedRevRemainingDays = 0.0m,
                YtdActualRevenue = 0.0m,
                YtdQuota = quotas.Any() ? (monthDetails.PreviousMonthsNames.Any() ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month)).Sum(x => x.Revenue) : 0.0m) +
                                          (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                              month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                              (monthDetails.WorkDaysCount == 0 ? 0 :
                                                  quotas.Where(z => z.Month == monthDetails.CurrentMonthName)
                                                      .Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount :
                                              0.0m)
                    : 0.0m
            };

            return model;
        }

        public ItemCategoryQuotaPerformanceDataModel GetItemCategoryQuotaPerformanceModelForSalesRepId(string itemCategoryName,
            MonthDetails monthDetails,
            int salesRepId = 0,
            int year = 0,
            int month = 0)
        {
            // Will throw if nothing is found, which is desired.
            ReportItemCategory itemCategory = _context.ReportItemCategory.Single(x => x.Name == itemCategoryName);

            List<ReportItemCategoryQuota> quotas = _context.ReportItemCategoryQuotas
                .Where(x => (year == 0 || x.Year == year) &&
                            (salesRepId == 0 || x.SalesRepId == salesRepId) &&
                            x.ItemCategoryId == itemCategory.Id)
                .ToList();

            var model = new ItemCategoryQuotaPerformanceDataModel
            {
                ItemCategoryId = itemCategory.Id,
                ItemCategoryName = itemCategory.Name,
                MtdActualRevenue = 0.0m,
                MtdTotalMonthQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) : 0.0m,
                MtdDailyQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ? (monthDetails.WorkDaysCount == 0 ? 0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(x => x.Revenue) / monthDetails.WorkDaysCount) : 0.0m,
                ProjectedAvgRevPerDay = 0.0m,
                ProjectedRevRemainingDays = 0.0m,
                YtdActualRevenue = 0.0m,
                YtdQuota = quotas.Any() ? (monthDetails.PreviousMonthsNames.Any() ? quotas.Where(x => monthDetails.PreviousMonthsNames.Contains(x.Month)).Sum(x => x.Revenue) : 0.0m) +
                                          (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                                              month < DateTime.Now.Month ? quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                                              (monthDetails.WorkDaysCount == 0 ? 0 :
                                                  quotas.Where(z => z.Month == monthDetails.CurrentMonthName)
                                                      .Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount :
                                              0.0m)
                    : 0.0m
            };

            return model;
        }

        public  SalesRepRegionQuotaPerformanceDataModel GetSalesRepGetRegionQuotaPerformanceDataModelForSalesRepName(string salesRepName,
           MonthDetails monthDetails,
           int year = 0,
           int month = 0,
           int currentQuarter = 0)
        {
            // Will throw if nothing is found, which is desired.
            ReportHierarchySalesRep salesRep = _context.ReportHierarchySalesRep.Single(x => x.Name == salesRepName);

            // Will throw if nothing is found, which is desired.
            ReportUserMapping mapping = _context.ReportUserMapping.Single(x => x.SalesRepId == salesRep.Id);

            bool isRegionLeader = mapping.ZoneId == null && mapping.RegionId != null;

            List<ReportRegionQuota> quotas = _context.ReportRegionQuotas
                .Where(x => (year == 0 || x.Year == year) &&
                            x.SalesRepId == salesRep.Id)
                .ToList();

            var model = new SalesRepRegionQuotaPerformanceDataModel
            {
                RegionId = salesRep.RegionId ?? 0,
                RegionName = salesRep.ReportHierarchyRegion.Name,
                SalesRepData = new RegionQuotaPerformanceSalesRepModel
                {
                    SalesRepId = salesRep.Id,
                    SalesRepName = salesRep.Name ?? string.Empty,
                    IsRegionLeader = isRegionLeader,
                    MtdRevenue = 0.0m,
                    MtdQuota = quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                        month < DateTime.Now.Month ?
                        quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) :
                        (monthDetails.WorkDaysCount == 0 ? 0 :
                        quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m,
                    QtdRevenue = 0.0m,
                    QtdQuota = quotas.Any(z => _daysOffService.GetQuarterNumber(z.Month) == currentQuarter) ?
                        quotas.Where(z => _daysOffService.GetQuarterNumber(z.Month) == currentQuarter && monthDetails.PreviousMonthsNames.Contains(z.Month))
                        .Sum(x => x.Revenue) + (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                        (monthDetails.WorkDaysCount == 0 ?
                        0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m) : 0.0m,
                    YtdRevenue = 0.0m,
                    YtdQuota = quotas.Any(z => z.Year == year) ? (monthDetails.PreviousMonthsNames.Any() ?
                        quotas.Where(z => monthDetails.PreviousMonthsNames.Contains(z.Month))
                        .Sum(x => x.Revenue) : 0.0m) +
                        (quotas.Any(z => z.Month == monthDetails.CurrentMonthName) ?
                        (monthDetails.WorkDaysCount == 0 ?
                        0 : quotas.Where(z => z.Month == monthDetails.CurrentMonthName).Sum(q => q.Revenue) / monthDetails.WorkDaysCount) * monthDetails.WorkDaysToDateCount : 0.0m) : 0.0m,
                    PreviousYtdRevenue = 0.0m,
                }
            };

            return model;
        }

        #endregion Methods
    }
}