﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Framework.DataLayer.Test;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    class TEmployeeSkillsConfig : IEntityTypeConfiguration<TEmployeeSkills>
    {
        public void Configure(EntityTypeBuilder<TEmployeeSkills> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.SkillId);
            builder.Property(x => x.EmployeeId);

            //builder.HasKey(x => new { x.EmployeeId, x.SkillId });
        }
    }
}
