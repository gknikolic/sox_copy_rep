﻿using System.Collections.Generic;
using Atlassian.Jira;
using ProfitOptics.Modules.Support.Models.JiraRestModels;
using IssuePriority = Atlassian.Jira.IssuePriority;
using IssueType = Atlassian.Jira.IssueType;

namespace ProfitOptics.Modules.Support.Services
{
    public interface ISupportMemoryCacheService
    {
        List<JiraUserFromApi> GetJiraUsersForProject(string projectKey);

        List<Project> GetProjects();

        string GetFirstProjectKey();

        BoardConfiguration GetBoardConfiguration(string projectKey);

        List<ComponentResult> GetComponentsForProject(string selectedProjectKey);

        List<IssueType> GetIssueTypesForProject(string projectKey);

        List<IssuePriority> GetPrioritiesForProject(string projectKey);
    }
}