USE [Sox]

EXEC sp_rename '[Sox].[dbo].[WWPictures]', 'WWPicture'
EXEC sp_rename '[Sox].[dbo].[WWSignatures]', 'WWSignature'

EXEC sp_rename '[Sox].[dbo].[WWPodContainer].[WWPodId]', 'NoteId', 'COLUMN';

ALTER TABLE [Sox].[dbo].[WWSignature] ADD  [WWPodContainerId] INT NULL;

ALTER TABLE [Sox].[dbo].[WWSignature] WITH CHECK ADD  CONSTRAINT [FK_WWSignatures_WWPodContainer] FOREIGN KEY([WWPodContainerId])
REFERENCES [dbo].[WWPodContainer] ([Id])

ALTER TABLE [Sox].[dbo].[WWPicture] ADD  [WWPodContainerId] INT NULL;

ALTER TABLE [Sox].[dbo].[WWPicture] WITH CHECK ADD  CONSTRAINT [FK_WWPictures_WWPodContainer] FOREIGN KEY([WWPodContainerId])
REFERENCES [dbo].[WWPodContainer] ([Id])

GO
