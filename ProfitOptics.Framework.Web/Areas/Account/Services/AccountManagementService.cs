﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using OtpSharp;
using ProfitOptics.Framework.Core.SMS;
using ProfitOptics.Framework.Web.Areas.Account.Models;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Framework.Web.Helpers;
using System.IO;
using System.Threading.Tasks;
using Wiry.Base32;
using IAuthenticationService = ProfitOptics.Framework.Web.Framework.Services.IAuthenticationService;
using ISessionManager = ProfitOptics.Framework.Web.Framework.Helpers.ISessionManager;

namespace ProfitOptics.Framework.Web.Areas.Account.Services
{
    public class AccountManagementService : IAccountManagementService
    {
        private readonly IStringResourceRepository _stringResourceRepository;
        private readonly ITwilio _twilio;
        private readonly IUserService _userService;
        private readonly IAuthenticationService _authenticationService;
        private readonly HttpContext HttpContext;
        private readonly ISessionManager _sessionManager;

        public AccountManagementService(IStringResourceRepository stringResourceRepository,
            ITwilio twilio,
            IUserService userService,
            IAuthenticationService authenticationService,
            IHttpContextAccessor httpContextAccessor,
            ISessionManager sessionManager)
        {
            this._stringResourceRepository = stringResourceRepository;
            this._twilio = twilio;
            this._userService = userService;
            this._authenticationService = authenticationService;
            this.HttpContext = httpContextAccessor.HttpContext;
            this._sessionManager = sessionManager;
        }

        public async Task<ApplicationUser> GetCurrentUserAsync()
        {
            var userId = HttpContext.User.GetUserId();
            return await _userService.FindUserByIdAsync(userId);
        }

        public async Task<AccountManagementServiceResult> ChangePhoneNumberAsync(string phoneNumber, string code)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return AccountManagementServiceResult.UserNotFound();

            }
            var changeResult = await _userService.ChangePhoneNumberAsync(user, phoneNumber, code);
            return new AccountManagementServiceResult(changeResult);
        }

        public async Task DisableGoogleAuthenticatorAsync()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                user.GoogleAuthenticatorSecretKey = null;
                user.IsGoogleAuthenticatorEnabled = false;
                await _userService.UpdateUserAsync(user);
            }
        }

        public string ProcessSecurityStatusMessage(ManageMessageId? message)
        {
           switch (message)
            {
                case ManageMessageId.ChangePasswordSuccess:
                    return _stringResourceRepository.GetResource(ResourceKeys.MessagesAccountManagementChangePasswordSuccess);
                case ManageMessageId.SetPasswordSuccess:
                    return _stringResourceRepository.GetResource(ResourceKeys.MessagesAccountManagementSetPasswordSuccess);
                case ManageMessageId.SetTwoFactorSuccess:
                    return _stringResourceRepository.GetResource(ResourceKeys.MessagesAccountManagementSetTwoFactorSuccess);
                case ManageMessageId.AddPhoneSuccess:
                    return _stringResourceRepository.GetResource(ResourceKeys.MessagesAccountManagementAddPhoneSuccess);
                case ManageMessageId.RemovePhoneSuccess:
                    return _stringResourceRepository.GetResource(ResourceKeys.MessagesAccountManagementRemovePhoneSuccess);
                default:
                    return string.Empty;
            }
        }


        /// <summary>
        /// Gets a string resource message for given message identificator
        /// </summary>
        /// <param name="message">Message enum</param>
        /// <returns>Message as a text</returns>
        public string ProcessStatusMessage(ManageMessageId? message)
        {
            switch (message)
            {
                case ManageMessageId.ProfileUpdated:
                    return _stringResourceRepository.GetResource(ResourceKeys.MessagesAccountManagementProfileUpdated);
                case ManageMessageId.RemoveLoginSuccess:
                    return _stringResourceRepository.GetResource(ResourceKeys.MessagesAccountManagementRemoveLoginSuccess);
                case ManageMessageId.Error:
                    return _stringResourceRepository.GetResource(ResourceKeys.MessagesGenericError);
                default:
                    return string.Empty;               
            }
        }

        public async Task<AccountManagementServiceResult> SendPhoneVerificationCodeAsync(string number)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return AccountManagementServiceResult.UserNotFound();
            }

            var code = await _userService.GenerateChangePhoneNumberTokenAsync(user, number);

            if (code == null)
            {
                var error = _stringResourceRepository.GetResource(ResourceKeys.MessagesGenericError);
                return AccountManagementServiceResult.Failed(error);
            }

            var smsBody =
                string.Format(_stringResourceRepository.GetResource(ResourceKeys.TemplatesSMSPhoneVerification), code);
            if (!_twilio.Send(number, smsBody))
            {
                var error = _stringResourceRepository.GetResource(ResourceKeys
                    .MessagesAccountManagementPhoneVerificationSendingFailed);
                return AccountManagementServiceResult.Failed(error);
            }
            return AccountManagementServiceResult.Success;            
        }

        public async Task<AccountManagementServiceResult> SetAccountGoogleAuthenticatorAsync(string secretKey)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return AccountManagementServiceResult.UserNotFound();
            }
            
            var result = await _userService.SetAccountGoogleAuthenticatorAsync(user, secretKey);
            return new AccountManagementServiceResult(result);
        }

        public async Task<AccountManagementServiceResult> ToggleTwoFactorAuthenticationAsync()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return AccountManagementServiceResult.Success;
            }

            var isEnabled = await _userService.IsTwoFactorAuthenticationEnabledAsync(user);
            var identityResult = await _userService.SetTwoFactorAuthenticationEnabledAsync(user, !isEnabled);
            return new AccountManagementServiceResult(identityResult);
        }

        public async Task<GoogleAuthenticatorEnabledResult> IsGoogleAuthenticatorEnabledAsync()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return AccountManagementServiceResult.UserNotFound<GoogleAuthenticatorEnabledResult>();
            }
            bool isGoogleAuthenticatorEnabled = await _userService.IsGoogleAuthenticatorEnabledAsync(user);
            return GoogleAuthenticatorEnabledResult.Success(isGoogleAuthenticatorEnabled);
        }

        public async Task<AccountManagementServiceResult> ChangePasswordAsync(string currentPassword, string newPassword)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return AccountManagementServiceResult.UserNotFound();
            }

            var result = await _userService.ChangePasswordAsync(user, currentPassword, newPassword);
            return new AccountManagementServiceResult(result);
        }

        public async Task<AccountManagementServiceResult> AddExternalLoginAsync(AuthenticateResult response)
        {
            if (response == null)
            {
                var error = _stringResourceRepository.GetResource(ResourceKeys.MessagesGenericError);
                return AccountManagementServiceResult.Failed(error);
            }

            var externalLoginDetails = _authenticationService.GetExternalLoginDetails(response);
            if (externalLoginDetails != null)
            {

            }
            var currentUser = await GetCurrentUserAsync();
            if (currentUser == null)
            {
                return AccountManagementServiceResult.UserNotFound();
            }

            currentUser = await _authenticationService.SaveExternalLoginAsync(externalLoginDetails, currentUser);
            if (currentUser == null)
            {
                var error = "There was an error adding external login.";
                return AccountManagementServiceResult.Failed(error);
            }
            return AccountManagementServiceResult.Success;
        }

        public async Task<AccountManagementServiceResult> RemoveLoginAsync(string loginProvider, string providerKey)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return AccountManagementServiceResult.UserNotFound();
            }
            IdentityResult result = await _userService.RemoveLoginAsync(user, loginProvider, providerKey);
            return new AccountManagementServiceResult(result);
        }

        public async Task<AccountManagementServiceResult> RemovePhoneNumberAsync()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return AccountManagementServiceResult.UserNotFound();
            }

            IdentityResult result = await _userService.RemovePhoneNumberAsync(user);
            return new AccountManagementServiceResult(result);
        }

        public async Task<AccountManagementServiceResult> SetPasswordAsync(string password)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return AccountManagementServiceResult.UserNotFound();
            }

            IdentityResult result = await _userService.SetPasswordAsync(user, password);
            return new AccountManagementServiceResult(result);
        }

        public async Task<AccountManagementServiceResult> UpdateAccountInfoForCurrentUserAsync(AccountManagementModel model)
        {
            var currentUser = await GetCurrentUserAsync();

            currentUser.FirstName = model.FirstName;
            currentUser.LastName = model.LastName;

            var result = await _userService.UpdateUserAsync(currentUser);            
            if (!result.Succeeded)
            {
                return new AccountManagementServiceResult(result);
            }

            await _authenticationService.RefreshAsync();

            if (model.ProfilePhoto != null)
            {
                byte[] profilePhotoBytes = null;
                using (var memoryStream = new MemoryStream())
                {
                    await model.ProfilePhoto.CopyToAsync(memoryStream);
                    profilePhotoBytes = Core.System.ImageHelper.Resize(memoryStream, true);
                }

                var profilePictureResult = await _userService.UpdateProfilePictureAsync(currentUser, profilePhotoBytes);

                if (profilePictureResult < 1)
                {
                    return AccountManagementServiceResult.Failed("There was an error adding profile picture");                
                }
                _sessionManager.SetValue(SessionKeys.ProfilePicture, profilePhotoBytes);
            }

            return AccountManagementServiceResult.Success;
        }

        public bool VerifyGoogleAuthenticator(GoogleAuthenticatorModel model)
        {
            var secretKey = Base32Encoding.Standard.ToBytes(model.SecretKey);
            var otp = new Totp(secretKey);
            return otp.VerifyTotp(model.Code, out _, new VerificationWindow(2, 2));
        }
    }
}
