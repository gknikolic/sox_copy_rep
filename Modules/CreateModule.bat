@echo off 

echo === WARNING ===
echo This script is depricated.
echo To add a new module, please follow the instructions from the wiki page:
echo https://bitbucket.org/profitoptics/profitoptics.framework.core/wiki/Modules

pause
