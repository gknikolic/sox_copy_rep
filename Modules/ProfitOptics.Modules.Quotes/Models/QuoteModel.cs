﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class QuoteModel
    {
        public int QuoteId { get; set; }
        public string QuoteName { get; set; }
        public string Notes { get; set; }
        public string CustomerNum { get; set; }
        public string CustomerName { get; set; }
        public decimal Sales3Mo { get; set; }
        public decimal Cost3Mo { get; set; }
        public decimal GPPct3Mo { get; set; }
        public decimal Sales12Mo { get; set; }
        public decimal Cost12Mo { get; set; }
        public decimal GPPct12Mo { get; set; }
        public int SalesChange => CalculateChange(Sales3Mo, Sales12Mo, 0.1);
        public int GPPctChange => CalculateChange(GPPct3Mo, GPPct12Mo, 0.1);
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public bool PresentedToCustomer { get; set; }
        public int Items { get; set; }
        public decimal? FloorPct { get; set; }
        public string Status { get; set; }
        public decimal QuoteExtGPPct { get; set; }
        public decimal QuoteRevenue { get; set; }
        public bool IsArchived { get; set; }

        private static int CalculateChange(decimal newValue, decimal oldValue, double threshold)
        {
            if (oldValue == decimal.Zero)
            {
                if (newValue == decimal.Zero)
                    return 0;

                return (int)(newValue / Math.Abs(newValue));
            }

            var v = (newValue - oldValue) / oldValue;
            return v != 0 ? (int)(v / Math.Abs(v)) : 0;
        }
    }
}


