﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.System;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Helpers.Anotations;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Factories;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;
using ProfitOptics.Modules.Reporting.Services;

namespace ProfitOptics.Modules.Reporting.Controllers
{
    [Area("Reporting")]
    public class BaseReportingController : BaseController
    {
        private IReportFilterModelFactory _reportFilterModelFactory;
        private IUserFilterService _userFilterService;

        private IReportFilterModelFactory ReportFilterModelFactory => _reportFilterModelFactory ??
            (_reportFilterModelFactory = HttpContext.RequestServices.GetService<IReportFilterModelFactory>());

        private IUserFilterService UserFilterService => _userFilterService ??
            (_userFilterService = HttpContext.RequestServices.GetService<IUserFilterService>());

        [HttpGet]
        public IActionResult GetFilterOptions(FilterType filterType, string search, int page, int take)
        {
            return Json(ReportFilterModelFactory.CreateFilterOptions(GetCurrentUserId(), filterType, search, page, take));
        }

        [HttpGet]
        [AjaxCall(AjaxCallMethod.Get, AjaxCallDataType.Json)]
        public IActionResult LoadDefaultFilters()
        {
            int currentUserId = GetCurrentUserId();

            SelectedFiltersModel selectedFilters = ReportFilterModelFactory.CreateDefaultSelectedFiltersModel(currentUserId);

            return Json(new { success = true, filters = selectedFilters });
        }

        [HttpPost]
        [AjaxCall(AjaxCallMethod.Post, AjaxCallDataType.Json)]
        public IActionResult LoadUserFilter(int userFilterId)
        {
            return Json(new
            {
                filters = ReportFilterModelFactory
                    .CreateSelectedFiltersModelForUserFilterId(GetCurrentUserId(), userFilterId)
            });
        }

        [HttpPost]
        [AjaxCall(AjaxCallMethod.Post, AjaxCallDataType.Json)]
        public IActionResult AddOrUpdateUserFilter(SaveReportFilterModel saveReportFilterModel)
        {
            bool success = UserFilterService.AddOrUpdateReportFilter(GetCurrentUserId(), saveReportFilterModel, out int filterId);

            return Json(new {success, filterId});
        }

        [HttpPost]
        [AjaxCall(AjaxCallMethod.Post, AjaxCallDataType.Json)]
        public IActionResult DeleteUserFilter(int userFilterId)
        {
            return Json(new { success = UserFilterService.DeleteUserFilter(userFilterId) });
        }

        [HttpPost]
        [AjaxCall(AjaxCallMethod.Post, AjaxCallDataType.Json)]
        public IActionResult BulkRenameUserFilters(List<RenameFilterModel> filtersToRename)
        {
            return Json(new { success = UserFilterService.BulkRenamedUserFilters(filtersToRename) });
        }

        public IActionResult LargeJsonResult(object data)
        {
            return new LargeJsonResult(data)
            {
                MaxJsonLength = int.MaxValue
            };
        }

        public IActionResult DownloadFileForUser(string fileName, string downloadOrPreview)
        {
            // File is authorized by it's location. It is located in the temp folder in the directory that is named by user id
            var userId = GetCurrentUserId();

            var tmpFile = HttpContext.RequestServices.GetService<ITempFile>();

            var filepath = tmpFile.GetTempFileForUser(userId, fileName);

            var filedata = System.IO.File.ReadAllBytes(filepath);

            new FileExtensionContentTypeProvider().TryGetContentType(filepath, out string contentType);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = fileName,
                Inline = downloadOrPreview == "preview" ? true : false,
            };

            return File(filedata, contentType);
        }
    }
}
