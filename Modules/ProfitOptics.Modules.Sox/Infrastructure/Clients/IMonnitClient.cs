﻿using ProfitOptics.Modules.Sox.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Modules.Sox.Infrastructure.Clients
{
	public interface IMonnitClient
	{
		public Task<List<MonnitSensor>> GetSensors();
		public Task<bool> MonnitLogonAsync();
	}
}
