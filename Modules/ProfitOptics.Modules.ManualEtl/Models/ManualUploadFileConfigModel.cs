﻿namespace ProfitOptics.Modules.ManualEtl.Models
{
    public class ManualUploadFileConfigModel
    {
        public string FileTypeId { get; set; }
        public string FileTypeName { get; set; }
        public string TableName { get; set; }
    }
}