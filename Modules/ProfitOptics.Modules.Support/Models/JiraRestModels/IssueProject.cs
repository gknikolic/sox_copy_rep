﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueProject
    {
        public string key { get; set; }

        public string name { get; set; }
    }
}