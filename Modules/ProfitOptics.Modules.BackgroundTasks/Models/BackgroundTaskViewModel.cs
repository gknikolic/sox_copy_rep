﻿namespace ProfitOptics.Modules.BackgroundTasks.Models
{
    public class BackgroundTaskViewModel
    {
        public string Description { get; set; }

        public decimal Progress { get; set; }

        public string ProgressPercent => Progress.ToString("P");
    }
}