﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class ProcedureTypesModel
    {
        public string Title { get; set; }
        public List<ProcedureTypeModel> ProcedureTypes { get; set; }
    }
}
