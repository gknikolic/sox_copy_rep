USE [SoxQATest]

BEGIN
 
CREATE TABLE CTFacility
(
	[Id] [INT] PRIMARY KEY IDENTITY(1,1),
	[LegacyId] [BIGINT] NOT NULL,
	[ProcessingFacilityId] [BIGINT] NULL,
	[Name] [nvarchar](200) NOT NULL,
	[CompositeFacilityKey] [nvarchar](200) NULL,
	[IsProcessingFacility] [bit] NULL
)

END
