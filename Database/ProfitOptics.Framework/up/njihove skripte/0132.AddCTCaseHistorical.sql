USE [Sox]

CREATE TABLE CTCaseHistorical
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[CTCaseId] [INT] NOT NULL,
	[CaseReference] [NVARCHAR](50) NOT NULL,
	[DueTimestamp] [DateTime] NOT NULL,
	[DestinationName] [NVARCHAR](200) NULL,
	[CaseDoctor] [NVARCHAR](200) NULL,
	[CaseCartId] [BIGINT] NULL,
	[CaseTime] [INT] NULL,
	[CaseStatus] [NVARCHAR](50) NULL,
	[IsCartComplete] [BIT] NULL,
	[CartStatus] [NVARCHAR](100) NULL,
	[CartStatusId] [BIGINT] NULL,
	[CaseCartName] [NVARCHAR](200) NULL,
	[CaseCartLocationName] [NVARCHAR](200) NULL,
	[IsCanceled] [BIT] NULL,
	[Shipped] [BIT] NULL,
	[PreviousDueTimestamp] [DateTime] NULL,
	[PreviousDestinationName] [NVARCHAR](200) NULL,
	[ScheduleId] [INT] NULL,
	[ExpediteTime] [DateTime] NULL,
	[DueInMinutes] [NUMERIC](18,2) NULL,
	[DueInHoursAndMinutes] [NVARCHAR](10) NULL,
	[PreviousDueInMinutes] [NUMERIC](18,2) NULL,
	[PreviousDueInHoursAndMinutes] [NVARCHAR](10) NULL,
	[Changed] [NVARCHAR](20) NULL,
	[Priority] [BIT] NULL,
	[DateTimeNow] [DateTime] NULL,
	[EstimatedTime] [NVARCHAR](50) NULL,
	[DestinationFacilityId] [BIGINT] NULL,
	[CreatedAtUtc] DateTime NOT NULL
CONSTRAINT [PK_CTCaseHistorical] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
