﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportVendorConfig : IEntityTypeConfiguration<ReportVendor>
    {
        public void Configure(EntityTypeBuilder<ReportVendor> builder)
        {
            builder.ToTable(nameof(ReportVendor));
            builder.HasKey(v => v.Id);

            builder.Property(v => v.Name).HasMaxLength(50).IsRequired();
            builder.Property(v => v.StartDate).IsRequired();

            builder.HasMany(v => v.ReportSalesHistories)
                .WithOne(s => s.ReportVendor)
                .HasForeignKey(s => s.VendorId)
                .IsRequired(false);

            builder.HasMany(v => v.ReportQuotas)
                .WithOne(s => s.ReportVendor)
                .HasForeignKey(s => s.ProductFamilyId);
        }
    }
}
