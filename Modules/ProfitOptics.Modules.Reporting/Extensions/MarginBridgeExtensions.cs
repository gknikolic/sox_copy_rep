﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Extensions
{
    public static class MarginBridgeExtensions
    {
        public const string NLProductsString = "NL";
        public const string NLProductsRenameString = "NLNL";

        public static readonly List<int> OverrideSalesIds = new List<int> { 1 };
        public static readonly List<int> CustomerContractSalesIds = new List<int> { 2, 3 };
        public static readonly List<int> PromotionSalesIds = new List<int> { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
        public static readonly List<int> CustomerExceptionSalesIds = new List<int> { 16, 17 };
        public static readonly List<int> OtherSalesIds = new List<int> { 18, 19, 22 };
        public static readonly List<int> MatrixSalesIds = new List<int> { 20, 21, 23, 24 };

        public static List<int> GetAllLeavesIds(this List<ProductHierarchyFilterModel> filter)
        {
            return GetAllLeavesIdsInternal(filter);
        }

        public static List<int> GetAllLeavesIds(this List<LocationHierarchyFilterModel> filter)
        {
            return GetAllLeavesIdsInternal(filter);
        }

        public static List<T> CopyList<T>(this List<T> lst)
        {
            List<T> lstCopy = new List<T>();
            foreach (var item in lst)
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, item);
                    stream.Position = 0;
                    lstCopy.Add((T)formatter.Deserialize(stream));
                }
            }
            return lstCopy;
        }

        public static List<ProductHierarchyFilterModel> SelectChecked(this List<ProductHierarchyFilterModel> products)
        {
            var result = new List<ProductHierarchyFilterModel>();

            var checkedProducts = products.Where(p => p.Checked).ToList();

            foreach (var product in checkedProducts)
            {
                if (product.Items != null)
                {
                    product.Items = SelectChecked(product.Items);
                }

                result.Add(product);
            }

            return result;
        }

        public static List<LocationHierarchyFilterModel> SelectChecked(this List<LocationHierarchyFilterModel> locations)
        {
            var result = new List<LocationHierarchyFilterModel>();

            var checkedProducts = locations.Where(p => p.Checked).ToList();

            foreach (var location in checkedProducts)
            {
                if (location.Items != null)
                {
                    location.Items = SelectChecked(location.Items);
                }

                result.Add(location);
            }

            return result;
        }

        public static IEnumerable<ProductHierarchyFilterModel> CloneAndUncheckAll(this List<ProductHierarchyFilterModel> products)
        {
            foreach (var product in products)
            {
                var productFilterModel = new ProductHierarchyFilterModel
                {
                    FilterType = product.FilterType,
                    Code = product.Code,
                    Description = product.Description,
                    RowId = product.RowId,
                    Checked = false
                };

                if (product.Items != null)
                {
                    productFilterModel.Items = CloneAndUncheckAll(product.Items).ToList();
                }

                yield return productFilterModel;
            }
        }

        public static IEnumerable<LocationHierarchyFilterModel> CloneAndUncheckAll(this List<LocationHierarchyFilterModel> products)
        {
            foreach (var product in products)
            {
                var locationFilterModel = new LocationHierarchyFilterModel
                {
                    FilterType = product.FilterType,
                    Code = product.Code,
                    Description = product.Description,
                    RowId = product.RowId,
                    Checked = false
                };

                if (product.Items != null)
                {
                    locationFilterModel.Items = CloneAndUncheckAll(product.Items).ToList();
                }

                yield return locationFilterModel;
            }
        }

        public static void UncheckAll(this List<ProductHierarchyFilterModel> products)
        {
            foreach (var product in products)
            {
                product.Checked = false;
                if (product.Items != null)
                {
                    UncheckAll(product.Items);
                }
            }
        }

        public static void UncheckAll(this List<LocationHierarchyFilterModel> locations)
        {
            foreach (var location in locations)
            {
                location.Checked = false;
                if (location.Items != null)
                {
                    UncheckAll(location.Items);
                }
            }
        }

        public static void CheckAll(this List<ProductHierarchyFilterModel> products)
        {
            foreach (var product in products)
            {
                product.Checked = true;
                if (product.Items != null)
                {
                    CheckAll(product.Items);
                }
            }
        }

        public static void CheckAll(this List<LocationHierarchyFilterModel> locations)
        {
            foreach (var location in locations)
            {
                location.Checked = true;
                if (location.Items != null)
                {
                    CheckAll(location.Items);
                }
            }
        }

        public static string GetLocationKeyColumnName(this LocationLevelType filterType)
        {
            switch (filterType)
            {
                case LocationLevelType.ShipTo:
                    return "ShipTo";
                case LocationLevelType.BillTo:
                    return "BillTo";
                case LocationLevelType.Territory:
                    return "Territory";
                case LocationLevelType.Region:
                    return "Region";
                case LocationLevelType.Zone:
                    return "Zone";
                default:
                    throw new Exception("Unknown location filter type");
            }
        }

        public static string GetProductKeyColumnName(this ProductLevelType filterType)
        {
            switch (filterType)
            {
                case ProductLevelType.Product:
                    return "Item";
                case ProductLevelType.ProductCategory:
                    return "ItemCategory";
                default:
                    throw new Exception("Unknown product filter type");
            }
        }

        public static string GetLocationLevelTypeButtonText(this LocationLevelType filterType)
        {
            switch (filterType)
            {
                case LocationLevelType.AllLocations:
                    return "Locations";
                case LocationLevelType.ShipTo:
                    return "Ship To";
                case LocationLevelType.BillTo:
                    return "Bill To";
                case LocationLevelType.Territory:
                    return "Territory";
                case LocationLevelType.Region:
                    return "Region";
                case LocationLevelType.Zone:
                    return "Zone";
                default:
                    throw new Exception("Unknown location filter type");
            }
        }

        public static string GetProductLevelTypeButtonText(this ProductLevelType filterType)
        {
            switch (filterType)
            {
                case ProductLevelType.AllProducts:
                    return "Products";
                case ProductLevelType.Product:
                    return "Item";
                case ProductLevelType.ProductCategory:
                    return "Item Category";
                default:
                    throw new Exception("Unknown product filter type");
            }
        }

        private static List<int> GetAllLeavesIdsInternal(IEnumerable<ProductHierarchyFilterModel> hierarchyFilter, List<int> response = null)
        {
            var result = response ?? new List<int>();
            var checkedProducts = hierarchyFilter.Where(p => p.Checked);

            foreach (var product in checkedProducts)
            {
                if (product.FilterType == ProductLevelType.ProductCategory)
                {
                    var list = product.Items.Where(i => i.Checked).Select(i => i).ToList();
                    result.AddRange(list.Select(item => item.RowId).ToList());
                }
                else
                {
                    if (product.Items != null)
                    {
                        result = GetAllLeavesIdsInternal(product.Items, result);
                    }
                }
            }

            return result;
        }

        private static List<int> GetAllLeavesIdsInternal(IEnumerable<LocationHierarchyFilterModel> hierarchyFilter, List<int> response = null)
        {
            var result = response ?? new List<int>();

            var checkedLocations = hierarchyFilter.Where(p => p.Checked);

            foreach (var location in checkedLocations)
            {
                if (location.FilterType == LocationLevelType.Zone)
                {
                    var list = location.Items.Where(i => i.Checked).Select(i => i).ToList();
                    result.AddRange(list.Select(item => item.RowId).ToList());
                }
                else
                {
                    if (location.Items != null)
                    {
                        result = GetAllLeavesIdsInternal(location.Items, result);
                    }
                }
            }

            return result;
        }
    }

    public static class KeyValuePairExtensions
    {
        public static bool IsNull<T, TU>(this KeyValuePair<T, TU> pair)
        {
            return pair.Equals(new KeyValuePair<T, TU>());
        }
    }
}
