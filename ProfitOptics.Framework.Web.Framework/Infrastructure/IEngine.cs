﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    public interface IEngine
    {
        void Initialize(IServiceCollection services);
        T Resolve<T>() where T : class;
        object Resolve(Type type);
        IEnumerable<T> ResolveAll<T>();
        object ResolveUnregistered(Type type);

        void ConfigureServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth, ElmahIo elmahIo);
    }
}
