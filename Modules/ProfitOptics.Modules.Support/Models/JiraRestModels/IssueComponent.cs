﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueComponent
    {
        public string self { get; set; }

        public string name { get; set; }

        public int? id { get; set; }
    }
}