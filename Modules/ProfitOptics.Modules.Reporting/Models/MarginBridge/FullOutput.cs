﻿namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class FullOutput
    {
        public double Price { get; set; }
        public double Cost { get; set; }
        public double Volume { get; set; }
        public double ProductMix { get; set; }
        public double RebateRate { get; set; }
        public double RebateVolume { get; set; }
        public double RebateProductMix { get; set; }

        //public string LocationBusinessUnitNew { get; set; }
        //public string LocationDivisionName { get; set; }
        //public string LocationSubRegionNameNew { get; set; }
        //public string LocationAreaName { get; set; }
        //public string LocationName { get; set; }

        public string ShipToId { get; set; }
        public string BillToId { get; set; }
        public string TerritoryId { get; set; }
        public string RegionId { get; set; }
        public string ZoneId { get; set; }

        public string ProductId { get; set; }
        public string ProductCategoryId { get; set; }

        public string ShipTo { get; set; }
        public string BillTo { get; set; }
        public string Territory { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }

        public string Product { get; set; }
        public string ProductCategory { get; set; }

        //public string ProductFocusCore { get; set; }
        //public string ProductSuperBusinessUnit { get; set; }
        //public string ProductSuperGroup { get; set; }
        //public string ProductGroup { get; set; }
        //public string ProductSubgroup { get; set; }
        //public string ProductNumber { get; set; }

        public decimal StartInvoiceAmount { get; set; }
        public decimal StartRebatedCost { get; set; }
        public decimal EndInvoiceAmount { get; set; }
        public decimal EndRebatedCost { get; set; }
    }
}