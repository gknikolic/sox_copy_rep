namespace ProfitOptics.Modules.Announcements.Data.Domain
{
    public partial class POAnnouncementRole
    {
        public int Id { get; set; }

        public int AnnouncementId { get; set; }

        public int RoleId { get; set; }

        public virtual POAnnouncement POAnnouncement { get; set; }

        public virtual AspNetRole Role { get; set; }
    }
}
