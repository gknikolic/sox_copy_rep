USE [Sox]

CREATE TABLE GCLifecycleStatusTypeCategory
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[Title] [NVARCHAR](100) NULL,
	
CONSTRAINT [PK_GCLifecycleStatusTypeCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Sox].[dbo].[GCLifecycleStatusType] ADD Code INT NULL;
ALTER TABLE [Sox].[dbo].[GCLifecycleStatusType] ADD GCLifecycleStatusTypeCategoryId INT NULL;


ALTER TABLE [dbo].[GCLifecycleStatusType]  WITH CHECK ADD  CONSTRAINT [GCLifecycleStatusType_GCLifecycleStatusTypeCategory] FOREIGN KEY([GCLifecycleStatusTypeCategoryId])
REFERENCES [dbo].[GCLifecycleStatusTypeCategory] ([Id])
GO
