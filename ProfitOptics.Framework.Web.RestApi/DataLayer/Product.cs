using ProfitOptics.Framework.Web.RestApi.Models.Product;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.Web.RestApi.DataLayer
{
    public class Product
    {
        public Product()
        {
            this.DistributorProducts = new HashSet<DistributorProduct>();

            this.CustomerProducts = new HashSet<CustomerProduct>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        [Required]
        [MaxLength(50, ErrorMessage = "Length must be less then 50 characters")]
        public string Name { get; set; }
        public short QuantityInPackage { get; set; }
        public EUnitOfMeasurement UnitOfMeasurement { get; set; }

        public decimal? Price { get; set; }

        public virtual ICollection<DistributorProduct> DistributorProducts { get; set; }

        public virtual ICollection<CustomerProduct> CustomerProducts { get; set; }
    }
}