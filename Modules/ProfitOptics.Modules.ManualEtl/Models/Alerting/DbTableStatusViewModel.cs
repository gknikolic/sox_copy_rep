﻿namespace ProfitOptics.Modules.ManualEtl.Models.Alerting
{
    public class DbTableStatusViewModel
    {
        public string TableName { get; set; }
        public string ConstraintBroken { get; set; }
        public int RecordsThatBrokeConstraintCount { get; set; }
    }
}