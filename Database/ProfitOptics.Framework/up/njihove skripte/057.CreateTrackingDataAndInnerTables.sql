use [Sox]

begin
  
  create table [dbo].[WWPod]
  (
	[Id] [int] primary key identity(1,1) not null,
	[Sec] int,
	[Text] varchar(50),
	[Token] varchar(20)
  )
  
  create table [dbo].[WWSignatures]
  (
	[Id] [int] primary key identity(1,1) not null,
	[SignatureType] int,
	[WWPodId] int,
	
	constraint [FK_WWSignatures_WWPod] foreign key([WWPodId])
    references [dbo].[WWPod] ([Id])
  )
  
  create table [dbo].[WWPictures]
  (
	[Id] [int] primary key identity(1,1) not null,
	[Url] varchar(50),
	[WWPodId] int,	
	
	constraint [FK_WWPictures_WWPod] foreign key([WWPodId])
    references [dbo].[WWPod] ([Id])
  )
  
  create table [dbo].[WWPodContainer]
  (
	[Id] [int] primary key identity(1,1) not null,
	[WWPodId] int,
	
	constraint [FK_WWPodContainer_WWPod] foreign key([WWPodId])
    references [dbo].[WWPod] ([Id])
  )
  
  create table [dbo].[WWTrackingData]
  (
	[Id] [int] primary key identity(1,1) not null,
	[WWDriverId] int,
    [WWVehicleId] int,
    [TimeInSec] int,
    [Status] int,
    [StatusSec] int,
    [WWPodContainerId] int,	
	
	constraint [FK_WWTrackingData_WWDriver] foreign key([WWDriverId])
    references [dbo].[WWDriver] ([Id]),
	
	constraint [FK_WWTrackingData_WWVehicle] foreign key([WWVehicleId])
    references [dbo].[WWVehicle] ([Id]),
	
	constraint [FK_WWTrackingData_WWPodContainer] foreign key([WWPodContainerId])
    references [dbo].[WWPodContainer] ([Id])
  )
  
  alter table [Sox].[dbo].[WWRouteStep]
  add [WWTrackingDataId] int,
  
      constraint [FK_WWRouteStep_WWTrackingData] foreign key([WWTrackingDataId])
      references [dbo].[WWTrackingData] ([Id]);

end