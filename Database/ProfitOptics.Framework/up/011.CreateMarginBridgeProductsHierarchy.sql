USE [ProfitOptics.Framework]
GO

/****** Object:  Table [dbo].[MarginBridge_ProductsHierarchy]    Script Date: 23.9.2019. 12:23:26 ******/

 IF exists(SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MarginBridge_ProductsHierarchy' AND TABLE_CATALOG = 'ProfitOptics.Framework')
 BEGIN
 Drop Table MarginBridge_ProductsHierarchy
 END

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MarginBridge_ProductsHierarchy](
	[ProductHierarchyID] [int] IDENTITY(1,1) NOT NULL,
	[ProductLevel1] [varchar](255) NULL,
	[ProductLevel2] [varchar](255) NULL,
	[ProductLevel3] [varchar](255) NULL,
	[ProductLevel4] [varchar](255) NULL,
	[ProductLevel5] [varchar](255) NULL,
	[ProductLevel6] [varchar](255) NULL,
	[ProductNumber] [varchar](255) NULL,
 CONSTRAINT [PK_MarginBridge_ProductsHierarchy] PRIMARY KEY CLUSTERED 
(
	[ProductHierarchyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

