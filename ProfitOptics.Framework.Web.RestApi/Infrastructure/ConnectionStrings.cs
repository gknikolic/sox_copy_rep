﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApiInfrastructure
{
    public class ConnectionStrings
    {
        public string ApiEntities { get; set; }
        public string SoxEntities { get; set; }
    }
}
