﻿using System;
using System.Globalization;

namespace ProfitOptics.Modules.CrossReference.Models
{
    public partial class CrossReferenceRecordModel
    {
        public int Id { get; set; }

        public int SubmittedById { get; set; }

        public string SubmittedByName { get; set; }

        public int CompetitorId { get; set; }

        public string CompetitorName { get; set; }

        public string CompetitorItemNumber { get; set; }

        public string CompetitorItemDescription { get; set; }

        public string CompetitorWebsiteUrl { get; set; }

        public int ItemId { get; set; }

        public string ItemNumber { get; set; }

        public string AlternativeItemNumber { get; set; }

        public string ItemDescription { get; set; }

        public string ItemUom { get; set; }

        public int CompatibilityOptionId { get; set; }

        public string CompatibilityOptionName { get; set; }

        public int ThumbsUpCount { get; set; }

        public int ThumbsDownCount { get; set; }

        public DateTime? StartDate { get; set; }

        public string StartDateString => StartDate.HasValue ? StartDate.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) : "";

        public bool CurrentUserThumbsUp { get; set; }

        public bool CurrentUserThumbsDown { get; set; }

        public string Notes { get; set; }
    }
}