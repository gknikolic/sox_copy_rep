﻿using System.Collections.Generic;

namespace ProfitOptics.Framework.DataLayer
{
	public class GCLifecycleStatusTypeCategory
	{
		public int Id { get; set; }

        public string Title { get; set; }
		public int Order { get; set; }
		public bool? IsDisabledInTimeline { get; set; }
		public virtual ICollection<GCLifecycleStatusType> GCLifecycleStatusTypes { get; set; }
	}
}
