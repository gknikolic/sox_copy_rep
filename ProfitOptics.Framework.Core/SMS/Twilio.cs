﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace ProfitOptics.Framework.Core.SMS
{
    public class Twilio : ITwilio
    {
        private readonly Settings.Twilio _settings;

        public Twilio(Settings.Twilio settings)
        {
            _settings = settings;
        }

        public bool Send(string destinationPhoneNumber, string text)
        {
            
            try
            {
                TwilioClient.Init(_settings.SMSAccountIdentification, _settings.SMSAccountPassword);
                var message = MessageResource.Create(
                    body: text,
                    from: new PhoneNumber(_settings.SMSAccountFrom),
                    to: new PhoneNumber(destinationPhoneNumber)
                );

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }            
        }
    }
}
