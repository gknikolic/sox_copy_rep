﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCChildCompany
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? GCParentCompanyId { get; set; }
        public GCParentCompany GCParentCompany { get; set; }
        public int? GCVendorId { get; set; }
        public int? GCCustomerId { get; set; }
        public GCVendor GCVendor { get; set; }
        public GCCustomer GCCustomer { get; set; }
    }
}
