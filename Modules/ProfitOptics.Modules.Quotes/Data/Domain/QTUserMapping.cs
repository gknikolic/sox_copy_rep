using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTUserMapping")]
    public partial class QTUserMapping
    {
        public int UserId { get; set; }

        public int? CorporateId { get; set; }

        public int? ZoneId { get; set; }

        public int? RegionId { get; set; }

        public int? SalesRepId { get; set; }
    }
}
