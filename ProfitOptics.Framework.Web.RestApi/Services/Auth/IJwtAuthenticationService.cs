﻿using ProfitOptics.Framework.Web.RestApi.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.Services.Auth
{
    public interface IJwtAuthenticationService
    {
        Task<TokenResponse> CreateAccessTokenAsync(string username, string password);
        Task<TokenResponse> RefreshTokenAsync(string refreshToken, string username);
        void RevokeRefreshToken(string refreshToken);
    }
}
