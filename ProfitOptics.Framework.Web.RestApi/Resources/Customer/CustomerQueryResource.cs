namespace ProfitOptics.Framework.Web.RestApi.Resources.Customer
{
    public class CustomerQueryResource : QueryResource
    {
        public int? DistributorId { get; set; }
        public int? ProductId { get; set; }
    }
}