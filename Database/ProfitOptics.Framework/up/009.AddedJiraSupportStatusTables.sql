USE [ProfitOptics.Framework]
 IF NOT EXISTS(SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'JiraSupportTicketStatusGroup' AND TABLE_CATALOG = 'ProfitOptics.Framework')
 BEGIN
 
CREATE TABLE JiraSupportTicketStatusGroup
(
	Id int primary key,
	Caption varchar(100) not null	
)
INSERT INTO JiraSupportTicketStatusGroup(Id, Caption)
VALUES
(1, 'Ready'),
(2, 'In Progress'),
(3, 'Testing'),
(4, 'Done')

CREATE TABLE JiraSupportTicketStatus
(
	Id int identity,
	JiraStatusId varchar(100) NOT NULL,
	StatusName varchar(100) NOT NULL UNIQUE,
	StatusGroupId int,
	CONSTRAINT FK_TicketStatusGroup FOREIGN KEY(StatusGroupId) REFERENCES JiraSupportTicketStatusGroup(Id)
)
 END
