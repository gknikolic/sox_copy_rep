﻿namespace ProfitOptics.Modules.Reporting.Models.DailyTrend
{
    public class DailyTrendDrillThroughAllViewModel
    {
        public bool IsCorporate { get; set; }

        public string DrillThroughDate { get; set; }
    }
}
