﻿using ProfitOptics.Framework.Web.Areas.Account.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.Account.Factories
{
    public interface IAccountModelFactory
    {
        /// <summary>
        /// Prepares log in model
        /// </summary>
        /// <param name="returnUrl">Url that user should be redirected to, upon sucessful login</param>
        /// <returns>Log in model</returns>
        LoginModel PrepareLoginModel(string returnUrl);

        /// <summary>
        /// Prepares a model for account management page
        /// </summary>
        /// <param name="message">Enum type for status message</param>
        /// <returns>Account management model</returns>
        Task<AccountManagementModel> PrepareAccountManagementModelAsync(ManageMessageId? message);
        
        /// <summary>
        /// Prepares view model for two factor authentication
        /// </summary>
        /// <param name="returnUrl">Url to be redirected to after verification</param>
        /// <param name="rememberMe">Indicates if user should be remebered for future visits</param>
        /// <param name="twoFactorProviders">List of all available 2FA providers for the user</param>
        /// <returns>SendCode view model</returns>
        SendCodeModel PrepareSendCodeModel(string returnUrl, bool rememberMe, IList<string> twoFactorProviders);

        /// <summary>
        /// Prepared the 2FA verification view model
        /// </summary>
        /// <param name="provider">Selected 2FA provider</param>
        /// <param name="returnUrl">Url to be redirected to after verification</param>
        /// <param name="rememberMe">Indicates if user should be remebered for future visits</param>
        /// <returns>VerifyCode view model</returns>
        VerifyCodeModel PrepareVerifyCodeModel(string provider, string returnUrl, bool rememberMe);

        /// <summary>
        /// Prepares AddPhoneNumber view model
        /// </summary>
        /// <returns>AddPhoneNumber view model</returns>
        AddPhoneNumberModel PrepareAddPhoneNumberModel();

        /// <summary>
        /// Prepares VerifyPhoneNumber view model
        /// </summary>
        /// <param name="phoneNumber">Phone number that is being verified</param>
        /// <returns>VerifyPhoneNumber view model</returns>
        VerifyPhoneNumberModel PrepareVerifyPhoneNumberModel(string phoneNumber);

        /// <summary>
        /// Prepares GoogleAuthenticator view model
        /// </summary>
        /// <param name="user">User on which we want to activate Google Authenticator</param>
        /// <returns>GoogleAuthenticator view model</returns>
        Task<GoogleAuthenticatorModel> PrepareGoogleAuthenticatorModelAsync();

        /// <summary>
        /// Prepares an external logins providers list
        /// </summary>
        /// <param name="returnUrl">Url to be redirected to after sucessful login</param>
        /// <param name="externalLoginProviders">List of available external login providers</param>
        /// <param name="externalLoginCallbackUrl">Url to action that is going to process sucessful external login</param>
        /// <returns>ExternalLoginList model</returns>
        Task<ExternalLoginListModel> PrepareExternalLoginListAsync(string returnUrl, List<ExternalLoginProviderModel> externalLoginProviders, string externalLoginCallbackUrl);

        /// <summary>
        /// Prepares a ManageLogins view model
        /// </summary>
        /// <param name="user">User for which we are checking external logins</param>
        /// <param name="messageId">Status message for previous form action</param>
        /// <returns>ManageLogins view model</returns>
        Task<ManageLoginsModel> PrepareManageLoginsModelAsync(ManageMessageId messageId);
    }
}
