﻿using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Modules.Support.Data.Config;
using ProfitOptics.Modules.Support.Data.Domain;

namespace ProfitOptics.Modules.Support.Data
{
    public class Entities : DbContext
    {
        public Entities(DbContextOptions<Entities> options) : base(options)
        {
            var listener = this.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }

        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<JiraSupportNotification> JiraSupportNotifications { get; set; }
        public virtual DbSet<JiraSupportGlobalNotification> JiraSupportGlobalNotifications { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new AspNetUserConfig());
            builder.ApplyConfiguration(new JiraSupportNotificationConfig());
            builder.ApplyConfiguration(new JiraSupportGlobalNotificationConfig());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }
    }
}
