using AutoMapper;
using ProfitOptics.Framework.Web.RestApi.DataLayer;
using ProfitOptics.Framework.Web.RestApi.Models.Customer;
using ProfitOptics.Framework.Web.RestApi.Models.Distributor;
using ProfitOptics.Framework.Web.RestApi.Models.Product;
using ProfitOptics.Framework.Web.RestApi.Resources.Customer;
using ProfitOptics.Framework.Web.RestApi.Resources.Distributor;
using ProfitOptics.Framework.Web.RestApi.Resources.Product;
using System.Collections.Generic;
using System.Linq;

namespace ProfitOptics.Framework.Web.RestApi.Mapping
{
    public class ResourceToModelProfile : Profile
    {
        public ResourceToModelProfile()
        {
            // Distributor
            CreateMap<SaveDistributorResource, Distributor>();
            CreateMap<DistributorQueryResource, DistributorQuery>();

            // Product
            CreateMap<SaveProductResource, Product>()
                .ForMember(src => src.UnitOfMeasurement, opt => opt.MapFrom(src => (EUnitOfMeasurement)src.UnitOfMeasurement))
                .ForMember(src => src.DistributorProducts, opt => opt.MapFrom(src => CreateDistributorProducts(src.DistributorIds)))
                .ForMember(src => src.CustomerProducts, opt => opt.MapFrom(src => CreateCustomerProductsFromCustomers(src.CustomerIds)));
            CreateMap<ProductQueryResource, ProductQuery>();

            // Customer
            CreateMap<SaveCustomerResource, Customer>()
                .ForMember(src => src.CustomerProducts, opt => opt.MapFrom(src => CreateCustomerProductsFromProducts(src.ProductIds)));
            CreateMap<CustomerQueryResource, CustomerQuery>();
        }

        private static List<DistributorProduct> CreateDistributorProducts(IList<int> distributorIds)
        {
            IList<DistributorProduct> distributorProducts = new List<DistributorProduct>();

            foreach (int id in distributorIds)
            {
                distributorProducts.Add(new DistributorProduct
                {
                    DistributorId = id
                });
            }

            return distributorProducts.ToList();
        }

        private static List<CustomerProduct> CreateCustomerProductsFromCustomers(IList<int> customerIds)
        {
            IList<CustomerProduct> customerProducts = new List<CustomerProduct>();

            foreach (int id in customerIds)
            {
                customerProducts.Add(new CustomerProduct
                {
                    CustomerId = id
                });
            }

            return customerProducts.ToList();
        }

        private static List<CustomerProduct> CreateCustomerProductsFromProducts(IList<int> productIds)
        {
            IList<CustomerProduct> customerProducts = new List<CustomerProduct>();

            foreach (int id in productIds)
            {
                customerProducts.Add(new CustomerProduct
                {
                    ProductId = id
                });
            }

            return customerProducts.ToList();
        }
    }
}