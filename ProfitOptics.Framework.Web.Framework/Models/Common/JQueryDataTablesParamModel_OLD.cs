﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Framework.Models.Common
{
    /// <summary>
    /// Class that encapsulates most common parameters sent by DataTables plugin
    /// </summary>
    public class JQueryDataTablesParamModel_OLD
    {
        /// <summary>
        /// Request sequence number sent by DataTable,
        /// same value must be returned in response
        /// </summary>       
        public string sEcho { get; set; }

        /// <summary>
        /// Text used for filtering
        /// </summary>
        public string sSearch { get; set; }

        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>
        public int iDisplayLength { get; set; }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>
        public int iDisplayStart { get; set; }

        /// <summary>
        /// Number of columns in table
        /// </summary>
        public int iColumns { get; set; }

        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>
        public int iSortingCols { get; set; }

        /// <summary>
        /// Comma separated list of column names
        /// </summary>
        public string sColumnName { get; set; }

        /// <summary>
        /// Sorting column index
        /// </summary>
        public int iSortCol { get; set; }

        /// <summary>
        /// Sorting column direction
        /// </summary>
        public string sSortColDir { get; set; }

        /// <summary>
        /// Sorting column direction
        /// </summary>
        public bool bPaginate { get; set; }
    }

    public class PaginationResult_OLD<T>
    {
        public int TotalCount { get; set; }
        public List<T> Result { get; set; }

        public T SummaryRow { get; set; }
    }
}
