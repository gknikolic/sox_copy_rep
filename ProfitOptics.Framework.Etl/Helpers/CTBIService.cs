﻿using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Modules.Sox.Helpers;
using ProfitOptics.Modules.Sox.Services;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public class CTBIService : ICTBIService
    {
        private readonly Entities _context;
        private readonly IGroundControlService _groundControlService;

        public CTBIService(Entities context, IGroundControlService groundControlService)
        {
            _context = context;
            _groundControlService = groundControlService;
        }

        public bool AddOrEditCTBILoadResult(CTBILoadResult item, int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            bool isImportSuccessfull = true;

            var existingBILoadResult = _context.CTBILoadResults.Where(x => x.LoadId == item.LoadId).FirstOrDefault();

            int hrsBack = -6;

            if (existingBILoadResult != null && 
                existingBILoadResult.IsImportSuccessfull.HasValue && 
                existingBILoadResult.IsImportSuccessfull.Value &&
                existingBILoadResult.CreatedAtUtc < DateTime.UtcNow.AddHours(hrsBack)) // Load Results in past 'hrsBack' hrs just in case
            {
                Log.Information($"-Found CTBILoadResult Items for loadId: {item.LoadId}, older than {hrsBack * -1}hrs, import is COMPLETE");
                isImportSuccessfull = true;
            }
            else
            {
                if (existingBILoadResult == null)
                {
                    Log.Information($"-NOT Found CTBILoadResult Items for loadId: {item.LoadId}, older than {hrsBack * -1}hrs, import Continues");
                    item.IsImportSuccessfull = false;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    //item.LocationName = item.Location;
                    item.LoadDate = (item.LoadDateOriginal.HasValue && item.LoadDateOriginal.Value > DateTime.MinValue) ? item.LoadDateOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    item.InsertTimestamp = (item.InsertTimestampOriginal.HasValue && item.InsertTimestampOriginal.Value > DateTime.MinValue) ? item.InsertTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    item.UpdateTimestamp = (item.UpdateTimestampOriginal.HasValue && item.UpdateTimestampOriginal.Value > DateTime.MinValue) ? item.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    item.LoadTimestamp = (item.LoadTimestampOriginal.HasValue && item.LoadTimestampOriginal.Value > DateTime.MinValue) ? item.LoadTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    _context.CTBILoadResults.Add(item);
                }
                else
                {
                    Log.Information($"-Found CTBILoadResult Items for loadId: {item.LoadId}, NOT older than {hrsBack * -1}hrs, import Continues");
                    existingBILoadResult.LoadBarcode = item.LoadBarcode;
                    existingBILoadResult.Location = item.Location;
                    existingBILoadResult.LocationId = item.LocationId;
                    existingBILoadResult.LocationName = item.LocationName;
                    existingBILoadResult.LocationMethodId = item.LocationMethodId;
                    existingBILoadResult.LocationMethod = item.LocationMethod;
                    existingBILoadResult.LocationType = item.LocationType;
                    existingBILoadResult.LoadDateOriginal = item.LoadDateOriginal;
                    existingBILoadResult.LoadDate = (item.LoadDateOriginal.HasValue && item.LoadDateOriginal.Value > DateTime.MinValue) ? item.LoadDateOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existingBILoadResult.InsertTimestampOriginal = item.InsertTimestampOriginal;
                    existingBILoadResult.InsertTimestamp = (item.InsertTimestampOriginal.HasValue && item.InsertTimestampOriginal.Value > DateTime.MinValue) ? item.InsertTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existingBILoadResult.UpdateTimestampOriginal = item.UpdateTimestampOriginal;
                    existingBILoadResult.UpdateTimestamp = (item.UpdateTimestampOriginal.HasValue && item.UpdateTimestampOriginal.Value > DateTime.MinValue) ? item.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existingBILoadResult.LoadTimestampOriginal = item.LoadTimestampOriginal;
                    existingBILoadResult.LoadTimestamp = (item.LoadTimestampOriginal.HasValue && item.LoadTimestampOriginal.Value > DateTime.MinValue) ? item.LoadTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existingBILoadResult.UpdateUser = item.UpdateUser;
                    existingBILoadResult.Operator = item.Operator;
                    existingBILoadResult.SterilizationCycle = item.SterilizationCycle;
                    existingBILoadResult.CaseReference = item.CaseReference;
                    existingBILoadResult.Reason = item.Reason;
                    existingBILoadResult.Notes = item.Notes;
                    existingBILoadResult.ResultFlag = item.ResultFlag;
                    existingBILoadResult.Alarm = item.Alarm;
                    existingBILoadResult.IsLastLoad = item.IsLastLoad;
                    existingBILoadResult.SterilantBatch = item.SterilantBatch;
                    existingBILoadResult.MaxTemperature = item.MaxTemperature;
                    existingBILoadResult.ExposureTime = item.ExposureTime;
                    existingBILoadResult.ModifiedAtUtc = DateTime.UtcNow;
                }

                _context.SaveChanges();

                isImportSuccessfull = false;
            }

            return isImportSuccessfull;
        }

        public void AddOrEditCTBILoadResultContents(List<CTBILoadResultContent> contents, long loadId, int serverOffsetInMinsFromUTC)
        {
            Log.Information($"--Editing {contents.Count} CTBILoadResultContents LoadId: {loadId}...");

            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            contents.ForEach(item =>
            {
                Log.Information($"---Editing Content with AssetID (LegacyID): {item.AssetId}, Id: {item.CTContainerTypeActualId} CTBILoadResultContents LoadId: {loadId}...");

                var ctBILoadResult = _context.CTBILoadResults.Where(x => x.LoadId == loadId).FirstOrDefault();

                if (ctBILoadResult == null)
                {
                    Log.Error($"No CTBILoadResult found for LoadId: {loadId}.");
                    throw new Exception();
                }

                var ctContainerTypeActual = _context.CTContainerTypeActuals.Where(x => x.LegacyId == item.AssetId).FirstOrDefault();

                if (ctContainerTypeActual == null)
                {
                    Log.Error($"No CTContainerTypeActual found for LegacyId: {item.AssetId}.");
                    throw new Exception();
                }
                else
                {
                    Log.Information($"---Found CTContainerTypeActual for LegacyId: {item.AssetId}, Id: {item.CTContainerTypeActualId}");
                }

                var existing = _context.CTBILoadResultContents.Where(x => x.LoadId == loadId && x.AssetId == item.AssetId).FirstOrDefault();

                if (existing == null)
                {
                    Log.Information($"---NO CTBILoadResultContent Found for LegacyId: {item.AssetId}, Id: {item.CTContainerTypeActualId}, Creating now!");
                    item.CTBILoadResultId = ctBILoadResult.Id;
                    item.CTContainerTypeActualId = ctContainerTypeActual.Id;
                    item.LoadId = loadId;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    item.LastUpdate = item.LastUpdateOriginal.AddMinutes(minsToAdd);
                    _context.CTBILoadResultContents.Add(item);
                }
                else
                {
                    Log.Information($"---Found CTBILoadResultContent for LegacyId: {item.AssetId}, Id: {item.CTContainerTypeActualId}, Updating now!");
                    existing.CTBILoadResultId = ctBILoadResult.Id;
                    existing.CTContainerTypeActualId = ctContainerTypeActual.Id;
                    existing.LoadId = item.LoadId;
                    existing.Quantity = item.Quantity;
                    existing.Description = item.Description;
                    existing.Container = item.Container;
                    existing.UpdatedBy = item.UpdatedBy;
                    existing.LastUpdateOriginal = item.LastUpdateOriginal;
                    existing.LastUpdate = item.LastUpdateOriginal.AddMinutes(minsToAdd);
                    existing.LastUpdateOriginal = item.LastUpdateOriginal;
                    existing.LastContainer = item.LastContainer;
                    existing.LastLocation = item.LastLocation;
                    existing.Sort = item.Sort;
                    existing.BiotestFlag = item.BiotestFlag;
                    existing.AssetId = item.AssetId;
                    existing.AssetType = item.AssetType;
                    existing.CaseReference = item.CaseReference;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void AddOrEditCTBILoadResultIndicators(List<CTBILoadResultIndicator> indicators, long loadId, int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            var ctBILoadResult = _context.CTBILoadResults.Where(x => x.LoadId == loadId).FirstOrDefault();

            if (ctBILoadResult == null)
            {
                Log.Error($"No CTBILoadResult found for LoadId: {loadId}.");
                throw new Exception();
            }

            Log.Information($"--Preparing to edit CTBILoadResultIndicators Items for loadId: {loadId}...");

            var existingIndicators = _context.CTBILoadResultIndicators.Where(x => x.LoadId == loadId).ToList();
            if(existingIndicators != null && existingIndicators.Count > 0)
            {
                Log.Information($"--Found {existingIndicators.Count} CTBILoadResultIndicators Items for loadId: {loadId}...");
                Log.Information($"--REMOVING {existingIndicators.Count} CTBILoadResultIndicators Items for loadId: {loadId}...");
                _context.CTBILoadResultIndicators.RemoveRange(existingIndicators);
                _context.SaveChanges();
            }

            indicators.ForEach(item =>
            {
                Log.Information($"--Processing LoadIndicatorId: {item.LoadIndicatorId}...");

                var existing = _context.CTBILoadResultIndicators.Where(x => x.LoadId == loadId && x.LoadIndicatorId == item.LoadIndicatorId).FirstOrDefault();

                if (existing == null)
                {
                    Log.Information($"--NOT found for LoadIndicatorId: {item.LoadIndicatorId}, adding now...");
                    item.CTBILoadResultId = ctBILoadResult.Id;
                    item.LoadId = loadId;
                    if (item.InsertTimestampOriginal.HasValue && item.InsertTimestampOriginal.Value > DateTime.MinValue) item.InsertTimestamp = item.InsertTimestampOriginal.Value.AddMinutes(minsToAdd);
                    if (item.UpdateTimestampOriginal.HasValue && item.UpdateTimestampOriginal.Value > DateTime.MinValue) item.UpdateTimestamp = item.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd);
                    item.CreatedAtUtc = DateTime.UtcNow;
                    _context.CTBILoadResultIndicators.Add(item);
                }
                else
                {
                    Log.Information($"--FOUND for LoadIndicatorId: {item.LoadIndicatorId}, updating now...");
                    existing.CTBILoadResultId = ctBILoadResult.Id;
                    existing.IndicatorType = item.IndicatorType;
                    existing.IndicatorName = item.IndicatorName;
                    existing.PassLabel = item.PassLabel;
                    existing.IncubationFlag = item.IncubationFlag;
                    existing.DisplayOrder = item.DisplayOrder;
                    existing.DefaultProduct = item.DefaultProduct;
                    existing.SterilizerGroupId = item.SterilizerGroupId;
                    existing.IndicatorProduct = item.IndicatorProduct;
                    existing.IndicatorLot = item.IndicatorLot;
                    existing.ResultFlag = item.ResultFlag;
                    existing.InsertTimestampOriginal = item.InsertTimestampOriginal;
                    existing.InsertTimestamp = (item.InsertTimestampOriginal.HasValue && item.InsertTimestampOriginal.Value > DateTime.MinValue) ? item.InsertTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existing.UpdateTimestampOriginal = item.UpdateTimestampOriginal;
                    existing.UpdateTimestamp = (item.UpdateTimestampOriginal.HasValue && item.UpdateTimestampOriginal.Value > DateTime.MinValue) ? item.UpdateTimestampOriginal.Value.AddMinutes(minsToAdd) : (DateTime?)null;
                    existing.StartUser = item.StartUser;
                    existing.UserId = item.UserId;
                    existing.Quantity = item.Quantity;
                    existing.Exclude = item.Exclude;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void AddOrEditCTBILoadResultGraphics(List<CTBILoadResultGraphic> graphics, long loadId, int serverOffsetInMinsFromUTC)
        {
            int minsToAdd = ProfitOptics.Modules.Sox.Helpers.Helpers.GetServerOffsetInMinsFromUTC(serverOffsetInMinsFromUTC);

            Log.Information($"--Preparing to edit CTBILoadResultGraphics Items for loadId: {loadId}...");
            graphics.ForEach(item =>
            {
                Log.Information($"--Processing GraphicsID: {item.GraphicId}...");
                var ctBILoadResult = _context.CTBILoadResults.Where(x => x.LoadId == loadId).FirstOrDefault();

                if (ctBILoadResult == null)
                {
                    Log.Error($"No CTBILoadResult found for LoadId: {loadId}.");
                    throw new Exception();
                }
                
                var existing = _context.CTBILoadResultGraphics.Where(x => x.LoadId == loadId && x.GraphicId == item.GraphicId).FirstOrDefault();

                if (existing == null)
                {
                    Log.Information($"--NOT found for GraphicId: {item.GraphicId}, adding now...");
                    item.CTBILoadResultId = ctBILoadResult.Id;
                    item.LoadId = loadId;
                    item.GCImageId = null;
                    item.CreatedAtUtc = DateTime.UtcNow;
                    item.UpdateTimestamp = item.UpdateTimestampOriginal.AddMinutes(minsToAdd);
                    _context.CTBILoadResultGraphics.Add(item);
                }
                else
                {
                    Log.Information($"--FOUND for GraphicId: {item.GraphicId}, updating now...");
                    existing.CTBILoadResultId = ctBILoadResult.Id;
                    existing.IsGlobal = item.IsGlobal;
                    existing.FileName = item.FileName;
                    existing.Path = item.Path;
                    existing.UserName = item.UserName;
                    existing.UpdateTimestamp = item.UpdateTimestampOriginal.AddMinutes(minsToAdd);
                    existing.UseLocalImage = item.UseLocalImage;
                    existing.ModifiedAtUtc = DateTime.UtcNow;
                }
            });

            _context.SaveChanges();
        }

        public void AddOrEditCTBILoadResults(List<CTBILoadResult> ctBILoadResults, int serverOffsetInMinsFromUTC)
        {
            ctBILoadResults.ForEach(item =>
            {
                AddOrEditCTBILoadResult(item, serverOffsetInMinsFromUTC);
            });
        }

        //public List<CTBILoadResultGraphic> GetCTLoadResultGraphics(bool? isLoadMeadiaWithoutImage)
        //{
        //    var result = new List<CTBILoadResultGraphic>();

        //    var query = _context.CTBILoadResultGraphics.Where(x => 1 == 1);

        //    if (isLoadMeadiaWithoutImage.HasValue)
        //    {
        //        if (isLoadMeadiaWithoutImage.Value == true)
        //        {
        //            query = query.Where(x => !x.GCImageId.HasValue);
        //        }
        //        else
        //        {
        //            query = query.Where(x => x.GCImageId.HasValue);
        //        }
        //    }

        //    result = query.ToList();

        //    return result;
        //}

        public void UpdateCTLoadResultGraphic(int id, byte[] ctBILoadResultImage)
        {
            var graphic = _context.CTBILoadResultGraphics.Find(id);

            if (graphic != null)
            {
                var gcImage = new GCImage()
                {
                    Data = ctBILoadResultImage,
                    Origin = Constants.CT_ABBREVIATION_TWO_CHAR,
                    Title = graphic.FileName,
                    CreatedAtUtc = DateTime.UtcNow
                };

                graphic.GCImage = gcImage;
                _groundControlService.CreateThumbnailsForGCImage(gcImage);

                _context.SaveChanges();
            }
        }


        public void UpdateCTLoadResultImportStatus(long loadId, bool isImportSuccessfull)
        {
            var loadResult = _context.CTBILoadResults.Where(x => x.LoadId == loadId).FirstOrDefault();
            if(loadResult != null)
            {
                loadResult.IsImportSuccessfull = isImportSuccessfull;
                _context.SaveChanges();
            }
        }
    }
}
