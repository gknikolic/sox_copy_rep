﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class EntityPropertyKeys
    {
        public List<EntityPropertyKey> keys { get; set; }
    }
}