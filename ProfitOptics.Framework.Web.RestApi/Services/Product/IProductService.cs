using ProfitOptics.Framework.Web.RestApi.Models;
using ProfitOptics.Framework.Web.RestApi.Models.Product;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace ProfitOptics.Framework.Web.RestApi.Services.Product
{
    public interface IProductService
    {
        Task<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Product>> ListAsync(ProductQuery query);
        Task<List<ProfitOptics.Framework.Web.RestApi.DataLayer.Product>> ListAsyncByIds(List<int> ids);
        Task<ProductResponse> SaveAsync(ProfitOptics.Framework.Web.RestApi.DataLayer.Product product);
        Task<ProductResponse> UpdateAsync(int id, ProfitOptics.Framework.Web.RestApi.DataLayer.Product product);
        Task<ProductResponse> DeleteAsync(int id);
    }
}