namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportUserMapping
    {
        public int UserId { get; set; }

        public int? CorporateId { get; set; }

        public int? ZoneId { get; set; }

        public int? RegionId { get; set; }

        public int? SalesRepId { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        public virtual ReportHierarchyCorporate ReportHierarchyCorporate { get; set; }

        public virtual ReportHierarchyZone ReportHierarchyZone { get; set; }

        public virtual ReportHierarchyRegion ReportHierarchyRegion { get; set; }

        public virtual ReportHierarchySalesRep ReportHierarchySalesRep { get; set; }
    }
}
