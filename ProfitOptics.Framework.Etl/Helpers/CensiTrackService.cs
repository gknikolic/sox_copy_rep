﻿using Flurl;
using Flurl.Http;
using ProfitOptics.Framework.Etl.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Twilio.Http;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public class CensiTrackService : ICensiTrackService
    {
        private static string CensitrackApiEndpoint = "http://{0}.censis.net/mvc/api/";
        private const string CENSITRACK_LOGIN_URL = "login/login";

        public CensiTrackService()
        {

        }

        //public async Task<ETLCTLoginResponseModel> Login(ETLCTLoginModel model)
        //{
        //    var responseModel = new ETLCTLoginResponseModel()
        //    {
        //        LoginSuccessful = false,
        //        StatusCode = 204,
        //    };

        //    Log.Information($"Loging user: { model.UserId }");
        //    try
        //    {
        //        var response = await string.Format(CensitrackApiEndpoint, model.ClientId).AppendPathSegment(CENSITRACK_LOGIN_URL).PostJsonAsync(model);
        //        responseModel.LoginSuccessful = responseModel.StatusCode == response.StatusCode;
        //        Log.Information($"Response: { response.ResponseMessage }");
        //    }
        //    catch (Exception e)
        //    {
        //        Log.Error(e.Message);
        //        responseModel.Message = e.Message;
        //    }

        //    return responseModel;
        //}

        public async Task<List<ETLCTCase>> GetCTCases(int clientId, long? facilityId = 0)
        {
            // Example:
            // http://1906.censis.net/mvc/api/schedules/case-statuses?facilityId=10000000&showReprocessingOnly=false

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("schedules", "case-statuses")
            .SetQueryParams(new { facilityId = facilityId })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTCase>>();

            Log.Information($"Fetched {result.Count} CT cases.");

            return result;
        }

        public async Task<List<ETLCTCase>> GetCTCasesV2(int clientId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/external/distribution-center/all-case-statuses

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("external", "distribution-center", "all-case-statuses")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTCase>>();

            Log.Information($"Fetched {result.Count} CT cases.");

            return result;
        }

        //public async Task<List<ETLCTFacility>> GetCTFacilities(int clientId)
        //{
        //    // Example
        //    // http://1906.censis.net/mvc/api/schedules/facilities

        //    var result = await string.Format(CensitrackApiEndpoint, clientId)
        //    .AppendPathSegments("schedules", "facilities")
        //    //.WithOAuthBearerToken("my_oauth_token")
        //    .GetAsync()
        //    .ReceiveJson<ETLCTFacilitiesResponse>();

        //    Log.Information($"Fetched {result.ScheduleFacilities.Count} CT facilities.");

        //    return result.ScheduleFacilities;
        //}

        public async Task<List<Models.ETLCTContainerService>> GetCTContainerServices(int clientId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/services

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "services")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<Models.ETLCTContainerService>>();

            Log.Information($"Fetched {result.Count} CT Container Services.");

            return result;
        }

        public async Task<List<Models.ETLCTContainerType>> GetCTContainerTypesByService(int clientId, long facilityId, long serviceId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/types/by-service/10000002?facilityId=10000000&includeDeleted=false&includeMedia=true

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "types", "by-service", serviceId)
            .SetQueryParams(new { facilityId, includeDeleted = false, includeMedia = true })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<Models.ETLCTContainerType>>();

            Log.Information($"Fetched {result.Count} CT Container Types for FacilityId: {facilityId} & ServiceId: {serviceId}.");

            return result;
        }

        //public async Task<List<ETLCTProduct>> GetCTProducts(int clientId)
        //{
        //    // Example
        //    // http://1906.censis.net/mvc/api/products/search?$count=true

        //    var result = await string.Format(CensitrackApiEndpoint, clientId)
        //    .AppendPathSegments("products", "search?$count=true")
        //    //.SetQueryParams(new { $count, includeDeleted = false, includeMedia = true })
        //    //.WithOAuthBearerToken("my_oauth_token")
        //    .GetAsync()
        //    .ReceiveJson<ETLCTProductsResponse>();

        //    Log.Information($"Fetched {result.Items.Count} CT Products.");

        //    return result.Items;
        //}

        public async Task<List<ETLCTVendor>> GetCTVendors(int clientId)
        {
            // Example
            // http://1906.censis.net/mvc/api/vendor/

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegment("vendor")
            //.SetQueryParams(new { $count, includeDeleted = false, includeMedia = true })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTVendor>>();

            Log.Information($"Fetched {result.Count} CT Vendors.");

            return result;
        }

        public async Task<List<ETLCTContainerItem>> GetCTContainerTypeItems(int clientId, long containerId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/types/10000056/count-sheet

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "types", containerId, "count-sheet")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTContainerItem>>();

            Log.Information($"Fetched {result.Count} CT ContainerItems.");

            return result;
        }

        public async Task<List<ETLCTCaseContainer>> GetCTCaseContainers(int clientId, long facilityId, string caseReference, bool? showReprocessingOnly = true)
        {
            // Example:
            //http://1906.censis.net/mvc/api/schedules/case-containers?caseReference=11111111&facilityId=0&showReprocessingOnly=false

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("schedules", "case-containers")
            //.SetQueryParams(new { caseReference, facilityId, showReprocessingOnly })
            .SetQueryParams(new { caseReference })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTCaseContainer>>();

            Log.Information($"Fetched {result.Count} CT Containers for CaseReference: { caseReference }.");

            return result;
        }

        public async Task<List<ETLCTContainerTypeActual>> GetCTContainerTypeActualssByContainerTypes(int clientId, long containerTypeId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/by-type/10000069?includeDeleted=false

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "by-type", containerTypeId)
            .SetQueryParams(new { includeDeleted = false })
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<Models.ETLCTContainerTypeActual>>();

            Log.Information($"Fetched {result.Count} CT ContainerTypeActuals for ContainerTypeID: { containerTypeId }.");

            return result;
        }

        //public async Task<List<ETLCTLocationType>> GetCTLocationTypes(int clientId)
        //{
        //    // Example:
        //    // http://1906.censis.net/mvc/api/admin/locations/location-types

        //    var result = await string.Format(CensitrackApiEndpoint, clientId)
        //    .AppendPathSegments("admin", "locations", "location-types")
        //    //.WithOAuthBearerToken("my_oauth_token")
        //    .GetAsync()
        //    .ReceiveJson<List<Models.ETLCTLocationType>>();

        //    Log.Information($"Fetched {result.Count} CT LocationTypes.");

        //    return result;
        //}

        //public async Task<ETLCTLocationsResponse> GetCTLocations(int clientId)
        //{
        //    // Example:
        //    // http://1906.censis.net/mvc/api/admin/locations/location-initial-data

        //    var result = await string.Format(CensitrackApiEndpoint, clientId)
        //    .AppendPathSegments("admin", "locations", "location-initial-data")
        //    //.WithOAuthBearerToken("my_oauth_token")
        //    .GetAsync()
        //    .ReceiveJson<Models.ETLCTLocationsResponse>();

        //    Log.Information($"Fetched {result.Locations.Count} CT Locations.");

        //    return result;
        //}

        //public async Task<ETLCTContainerTypeActualHistoryResponse> GetCTContainerTypeActualHistoryItems(int clientId, long actualId)
        //{
        //    // Example:
        //    // http://1906.censis.net/mvc/api/containers/1010/history?pageSize=30

        //    var result = await string.Format(CensitrackApiEndpoint, clientId)
        //    .AppendPathSegments("containers", actualId, "history")
        //    .SetQueryParams(new { pageSize = 100000000 })
        //    //.WithOAuthBearerToken("my_oauth_token")
        //    .GetAsync()
        //    .ReceiveJson<ETLCTContainerTypeActualHistoryResponse>();

        //    Log.Information($"Fetched {(result.ETLCTContainerTypeActualHistoryItems == null ? 0 : result.ETLCTContainerTypeActualHistoryItems.Count)} CT ETLCTContainerTypeActualHistoryItems.");

        //    return result;
        //}

        public async Task<List<ETLCTContainerTypeGraphic>> GetCTContainerTypeGraphic(int clientId, long containerTypeId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/graphics/1033

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "graphics", containerTypeId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTContainerTypeGraphic>>();

            Log.Information($"Fetched {result.Count} CT ContainerTypeGraphics.");

            return result;
        }

        //public async Task<byte[]> GetCTGraphicImage(int clientId, long graphicId)
        //{
        //    // Example:
        //    // http://1906.censis.net/mvc/api/graphics/product-image?GraphicId=1013&IsGlobal=false

        //    var result = await string.Format(CensitrackApiEndpoint, clientId)
        //    .AppendPathSegments("graphics", "product-image")
        //    .SetQueryParams(new { GraphicId = graphicId, IsGlobal = false })
        //    //.WithOAuthBearerToken("my_oauth_token")
        //    .GetAsync()
        //    .ReceiveBytes();

        //    Log.Information($"Fetched CT ContainerTypeGraphicImage.");

        //    return result;
        //}

        public async Task<List<ETLCTBILoadResult>> GetCTBILoadResults(int clientId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/load-results/filter

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("load-results", "filter")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<Models.ETLCTBILoadResult>>();

            Log.Information($"Fetched {result.Count} CT BI LoadResults");

            return result;
        }

        public async Task<ETLCTBILoadResultContentResponse> GetCTBILoadResultContents(int clientId, long loadId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/sterilization/load-contents/1035

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("sterilization", "load-contents", loadId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTBILoadResultContentResponse>();

            Log.Information($"Fetched {result.LoadItems.Count} CT BI LoadResult Items for LoadId: {loadId}");

            return result;
        }

        public async Task<ETLCTBILoadResultIndicatorResponse> GetCTBILoadResultIndicators(int clientId, long loadId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/load-results/indicators/1035

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("load-results", "indicators", loadId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTBILoadResultIndicatorResponse>();

            Log.Information($"Fetched {result.RequiredIndicators.Count} CT BI LoadResult Indicators for LoadId: {loadId}");

            return result;
        }

        public async Task<List<ETLCTBILoadResultGraphic>> GetCTBILoadResultGraphics(int clientId, long loadId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/graphics/indicators/1035/graphics

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("graphics", "indicators", loadId, "graphics")
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<List<ETLCTBILoadResultGraphic>>();

            Log.Information($"Fetched {result.Count} CT BI LoadResult Graphics for LoadId: {loadId}");

            return result;
        }

        //public async Task<byte[]> GetCTLoadResultGraphicImage(int clientId, long graphicId)
        //{
        //    // Example:
        //    // http://1906.censis.net/mvc/api/graphics/indicators/graphics/1001

        //    var result = await string.Format(CensitrackApiEndpoint, clientId)
        //    .AppendPathSegments("graphics", "indicators", "graphics", graphicId)
        //    //.WithOAuthBearerToken("my_oauth_token")
        //    .GetAsync()
        //    .ReceiveBytes();

        //    Log.Information($"Fetched CT LoadResultGraphicImage.");

        //    return result;
        //}

        public async Task<ETLCTContainerType> GetCTContainerTypeItem(int clientId, long legacyId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/types/1045

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "types", legacyId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerType>();

            Log.Information($"Fetched CT ETLCTContainerType.");

            return result;
        }

        public async Task<ETLCTContainerAssembly> GetCTContainerAssemblyForActual(int clientId, long legacyId)
		{
            // Example:
            // http://1906.censis.net/mvc/api/containers-assembly/1198

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers-assembly", legacyId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerAssembly>();

            Log.Information($"Fetched CT ETLCTContainerType.");

            return result;
        }

        public async Task<ETLCTCurrentAssemblyGraphics> GetCTContainerAssemblyGraphics(int clientId, long containerTypeId, Guid censisSetHistoryId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers-assembly/graphics?censisSetHistoryId=2125c490-7987-40b8-8cea-8c84dc566340&containerTypeId=1124

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers-assembly", "graphics")
            //.WithOAuthBearerToken("my_oauth_token")
            .SetQueryParams(new { censisSetHistoryId, containerTypeId })
            .GetAsync()
            .ReceiveJson<ETLCTCurrentAssemblyGraphics>();

            Log.Information($"Fetched CT ETLCTAssemblyGraphics.");

            return result;
        }

        public async Task<ETLCTContainerAssemblyCountSheet> GetCTContainerAssemblyCountSheets(int clientId, long containerTypeId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/count-sheet/set/1124

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("count-sheet", "set",containerTypeId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerAssemblyCountSheet>();

            Log.Information($"Fetched CT ETLCTAssemblyGraphics.");

            return result;
        }

        public async Task<ETLCTContainerType> GetCTContainerTypesByService(int clientId, long containerTypeId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/types/1249

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "types", containerTypeId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerType>();

            Log.Information($"Fetched CT ETLCTContainerType.");

            return result;
        }

        public async Task<ETLCTContainerService> GetCTContainerServiceByLegacyId(int clientId, long serviceId)
        {
            // Example:
            // http://1906.censis.net/mvc/api/containers/services/1002

            var result = await string.Format(CensitrackApiEndpoint, clientId)
            .AppendPathSegments("containers", "services", serviceId)
            //.WithOAuthBearerToken("my_oauth_token")
            .GetAsync()
            .ReceiveJson<ETLCTContainerService>();

            Log.Information($"Fetched CT ETLCTContainerService.");

            return result;
        }

        public async Task<ETLCTContainerTypeActual> GetCTContainerTypesActualByService(int clientId, long containerId)
        {
            return null;
        }
    }
}
