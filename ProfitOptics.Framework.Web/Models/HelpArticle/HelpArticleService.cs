﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;

namespace ProfitOptics.Framework.Web.Models.HelpArticle
{
    public class HelpArticleService : IHelpArticleService
    {
        private readonly Entities _entities;

        public HelpArticleService(Entities entities)
        {
            _entities = entities;
        }

        public void AddHelpArticle(HelpArticleModel article)
        {
            var helpArticleEntity = ConvertHelpArticleModelToEntity(article);

            helpArticleEntity.StartDate = DateTime.UtcNow;

            _entities.POHelpArticles.Add(helpArticleEntity);

            _entities.SaveChanges();
        }

        public void EditHelpArticle(HelpArticleModel helpArticleModel)
        {
            var helpArticleEntity = GetHelpArticleEntity(helpArticleModel.Id);

            if (helpArticleEntity == null)
            {
                return;
            }

            helpArticleEntity.Name = helpArticleModel.Name;
            helpArticleEntity.Content = helpArticleModel.Content;
            helpArticleEntity.RelatedUrls = helpArticleModel.RelatedUrls;
            helpArticleEntity.Tags = helpArticleModel.Tags;
            helpArticleEntity.ModifiedDate = DateTime.UtcNow;

            _entities.SaveChanges();
        }

        public void DeleteHelpArticle(int id = 0)
        {
            var helpArticleEntity = GetHelpArticleEntity(id);

            if (helpArticleEntity == null)
            {
                return;
            }

            helpArticleEntity.EndDate = DateTime.UtcNow;

            _entities.SaveChanges();
        }

        public HelpArticleListModel GetHelpArticleListModel(HelpArticleFilterModel filter)
        {
            var articles = FilterHelpArticles(filter);

            int totalArticlesCount = GetTotalHelpArticleCount(filter);

            var result = new HelpArticleListModel
            {
                Data = articles,
                TotalArticlesCount = totalArticlesCount
            };

            return result;
        }

        private int GetTotalHelpArticleCount(HelpArticleFilterModel filter)
        {
            int result = _entities.POHelpArticles
                .Where(GetHelpArticleFilterPredicate(filter))
                .Count();

            return result;
        }

        private List<HelpArticleModel> FilterHelpArticles(HelpArticleFilterModel filter)
        {
            var articles = _entities.POHelpArticles
                .Where(GetHelpArticleFilterPredicate(filter))
                .Select(GetHelpArticleModelPredicate())
                .OrderByDescending(x => x.ModifiedDate ?? x.StartDate)
                .Skip(filter.Skip)
                .Take(filter.Take)
                .ToList();

            return articles;
        }

        private Expression<Func<POHelpArticle, bool>> GetHelpArticleFilterPredicate(HelpArticleFilterModel filter)
        {
            Expression<Func<POHelpArticle, bool>> predicate = x => x.EndDate == null && x.RelatedUrls.Contains(filter.CurrentUrl) &&
            (string.IsNullOrEmpty(filter.SearchValue) || x.Name.ToLower().Contains(filter.SearchValue.ToLower()) | x.Tags.ToLower().Contains(filter.SearchValue.ToLower()));

            return predicate;
        }

        public IEnumerable<HelpArticleGroupedByTagModel> GetHelpArticlesGroupedByTag()
        {
            var tags = GetHelpArticleTags(false);

            var result = tags.Select(tag => new HelpArticleGroupedByTagModel
            {
                Tag = tag,
                Articles = GetHelpArticlesByTag(tag)
            }).AsEnumerable();

            return result;
        }

        public string[] GetHelpArticleTags(bool allowTagsOfDeletedArticles = true)
        {
            var relevantArticles = allowTagsOfDeletedArticles ? _entities.POHelpArticles : _entities.POHelpArticles.Where(x => x.EndDate == null);

            var articleTags = string.Join(",", relevantArticles.Select(x => x.Tags)).Split(',').Distinct().Where(x => !string.IsNullOrEmpty(x)).ToArray();

            return articleTags;
        }

        private IEnumerable<HelpArticleModel> GetHelpArticlesByTag(string tag)
        {
            var result = _entities.POHelpArticles
                .Where(x => x.EndDate == null && x.Tags.Contains(tag))
                .Select(GetHelpArticleModelPredicate())
                .AsEnumerable();

            return result;
        }

        private Expression<Func<POHelpArticle, HelpArticleModel>> GetHelpArticleModelPredicate()
        {
            Expression<Func<POHelpArticle, HelpArticleModel>> predicate = x => new HelpArticleModel
            {
                Id = x.Id,
                Name = x.Name,
                Content = x.Content,
                RelatedUrls = x.RelatedUrls,
                Tags = x.Tags,
                StartDate = x.StartDate,
                ModifiedDate = x.ModifiedDate,
                EndDate = x.EndDate
            };

            return predicate;
        }

        private POHelpArticle GetHelpArticleEntity(int id = 0)
        {
            var article = _entities.POHelpArticles.Find(id);
            return article;
        }

        public string[] GetHelpArticleNames()
        {
            var articleNames = _entities.POHelpArticles.Select(x => x.Name).Distinct().ToArray();

            return articleNames;
        }

        public POHelpArticle ConvertHelpArticleModelToEntity(HelpArticleModel model)
        {
            var result = new POHelpArticle
            {
                Id = model.Id,
                Name = model.Name,
                Content = model.Content,
                RelatedUrls = model.RelatedUrls,
                StartDate = model.StartDate,
                ModifiedDate = model.ModifiedDate,
                Tags = model.Tags
            };

            return result;
        }
    }
}