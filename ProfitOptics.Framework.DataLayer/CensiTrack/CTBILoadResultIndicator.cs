﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTBILoadResultIndicator
    {
        public int Id { get; set; }
        public int CTBILoadResultId { get; set; }
        public long LoadIndicatorId { get; set; }
        public long LoadId { get; set; }
        public int IndicatorType { get; set; }
        public string IndicatorName { get; set; }
        public string PassLabel { get; set; }
        public string FailLabel { get; set; }
        public string IncubationFlag { get; set; }
        public int DisplayOrder { get; set; }
        public string DefaultProduct { get; set; }
        public string SterilizerGroupId { get; set; }
        public string IndicatorProduct { get; set; }
        public string IndicatorLot { get; set; }
        public string ResultFlag { get; set; }
        public DateTime? InsertTimestamp { get; set; }
        public string StartUser { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public string UserId { get; set; }
        public int? Quantity { get; set; }
        public bool Exclude { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
        public DateTime? InsertTimestampOriginal { get; set; }
        public DateTime? UpdateTimestampOriginal { get; set; }
    }
}
