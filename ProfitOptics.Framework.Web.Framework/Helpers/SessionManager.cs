﻿using System;
using Microsoft.AspNetCore.Http;

namespace ProfitOptics.Framework.Web.Framework.Helpers
{
    public class SessionManager : ISessionManager
    {
        private readonly ISession _session;

        public SessionManager(IHttpContextAccessor httpContextAccessor)
        {
            this._session = httpContextAccessor.HttpContext.Session;
        }

        public T GetValue<T>(SessionKeys key)
        {
            var keyValue = key.ToString();
            try
            {
                if (typeof(T) == typeof(byte[]))
                {
                    return (T)(object)_session.Get(keyValue);
                }
                else if (typeof(T) == typeof(int))
                {
                    return (T)(object)_session.GetInt32(keyValue);
                }
                else if (typeof(T) == typeof(bool))
                {
                    return (T)(object)bool.Parse(_session.GetString(keyValue));
                }
                else if (typeof(T) == typeof(double))
                {
                    return (T)(object)double.Parse(_session.GetString(keyValue));
                }
                else if (typeof(T) == typeof(float))
                {
                    return (T)(object)float.Parse(_session.GetString(keyValue));
                }
                else if (typeof(T) == typeof(short))
                {
                    return (T)(object)short.Parse(_session.GetString(keyValue));
                }
                else if (typeof(T) == typeof(char))
                {
                    return (T)(object)char.Parse(_session.GetString(keyValue));
                }
                else if (typeof(T) == typeof(DateTime))
                {
                    return (T)(object)DateTime.Parse(_session.GetString(keyValue));
                }
                else
                {
                    return (T)(object)_session.GetString(keyValue);
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Wrong type or type not supported.");
            }            
        }

        public void ClearSession()
        {
            _session.Clear();;
        }

        public bool IsSet(SessionKeys key)
        {
            var keyValue = key.ToString();
            return _session.IsAvailable && _session.TryGetValue(keyValue, out _);
        }

        public void SetValue<T>(SessionKeys key, T value)
        {
            var keyValue = key.ToString();
            try
            {
                if (typeof(T) == typeof(byte[]))
                {
                    _session.Set(keyValue, (byte[])(object)value);
                }
                else if (typeof(T) == typeof(int))
                {
                    _session.SetInt32(keyValue, (int)(object)value);
                }
                else
                {
                    _session.SetString(keyValue, value.ToString());
                }
            }
            catch (Exception ex)
            {                
                throw new ArgumentException("Wrong type or type not supported.");                
            }
        }
    }
}
