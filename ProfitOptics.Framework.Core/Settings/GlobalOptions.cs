﻿namespace ProfitOptics.Framework.Core.Settings
{
	public class GlobalOptions
	{
		public string CENTRAL_TIMEZONE_OFFSET_IN_MINS { get; set; }
	}
}
