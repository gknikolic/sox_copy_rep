﻿using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Modules.BackgroundTasks.Data.Configs;
using ProfitOptics.Modules.BackgroundTasks.Data.Domain;

namespace ProfitOptics.Modules.BackgroundTasks.Data
{
    public class Entities : DbContext
    {
        public Entities(DbContextOptions<Entities> options) : base(options)
        {
            var listener = this.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }

        public DbSet<AspNetUser> AspNetUsers { get; set; }
        public DbSet<BackgroundTask> BackgroundTasks { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new AspNetUserConfig());
            builder.ApplyConfiguration(new BackgroundTaskConfig());
        }
    }
}