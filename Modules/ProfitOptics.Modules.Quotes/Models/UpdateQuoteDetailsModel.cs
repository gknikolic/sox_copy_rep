﻿using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class UpdateQuoteDetailsModel
    {
        public int QuoteId { get; set; }

        [MaxLength(250, ErrorMessage = "Quote description is limited to 250 characters.")]
        public string QuoteDescription { get; set; }

        public int? OriginZipCode { get; set; }
        public string OrderFrequency { get; set; }
        public decimal? FreightCapAmount { get; set; }
        public string FreightPaymentType { get; set; }
        public int? CustomerShipToId { get; set; }
        public string ShipToZipOverride { get; set; }
        public string ShippingMethod { get; set; }
    }
}