USE [Sox]

CREATE TABLE [dbo].[WWGPSDevice]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[Label]  NVARCHAR(200) NULL,
	[Category]  NVARCHAR(200) NULL
CONSTRAINT [PK_WWGPSDevice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WWVehicle] ADD WWGPSDeviceId INT NULL;

ALTER TABLE [dbo].[WWVehicle]  WITH CHECK ADD CONSTRAINT [FK_WWVehicle_WWGPSDevice] FOREIGN KEY([WWGPSDeviceId])
REFERENCES [dbo].[WWGPSDevice] ([Id])
GO
