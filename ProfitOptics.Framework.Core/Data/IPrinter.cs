﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ProfitOptics.Framework.Core.Settings;

namespace ProfitOptics.Framework.Core.Data
{
    public interface IPrinter
    {
        /// <summary>
        /// Converts xls,pptx or docx file into pdf.
        /// </summary>
        /// <param name="inputfile">MemoryStream of the file that will be converted.</param>
        /// <param name="outputFileName">Name of the converted file.</param>
        /// <param name="fileExtension">File extension of the file that is converted. It can be xls, pptx, docx.</param>
        /// <returns></returns>
        Task<MemoryStream> ConvertToPDF(MemoryStream inputfile, string outputFileName, string fileExtension);
    }

}
