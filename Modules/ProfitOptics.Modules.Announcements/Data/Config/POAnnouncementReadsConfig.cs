﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Announcements.Data.Domain;

namespace ProfitOptics.Modules.Announcements.Data.Config
{
    public class POAnnouncementReadsConfig : IEntityTypeConfiguration<POAnnouncementRead>
    {
        public void Configure(EntityTypeBuilder<POAnnouncementRead> builder)
        {
            builder.ToTable("POAnnouncementRead");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.UserId);
            builder.Property(entity => entity.ReadDate).HasColumnType("datetime");
            builder.Property(entity => entity.AnnouncementId);

            builder.HasOne(entity => entity.POAnnouncement)
                .WithMany(p => p.POAnnouncementReads)
                .HasForeignKey(entity => entity.AnnouncementId);

            builder.HasOne(entity => entity.AspNetUser)
                .WithMany(p => p.POAnnouncementReads)
                .HasForeignKey(entity => entity.UserId);
        }
    }
}
