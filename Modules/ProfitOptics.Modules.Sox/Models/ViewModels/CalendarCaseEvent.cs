﻿using Microsoft.AspNetCore.Html;
using System;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class CalendarCaseEvent
	{
		public int CaseId { get; set; }
		public string Title { get; set; }
		public int CasesLeft { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public DateTime EventDate { get; set; }
		public string EventColor { get; set; }

		public HtmlString EventHtml { get; set; }
		//public int NumOfNotifications { get; set; }
		public string EventHtmlString { get; set; }
		public int NumOfBlueCases { get; set; }
		public int NumOfPurpleCases { get; set; }
		public int NumOfRedCases { get; set; }
		public int NumOfYellowCases { get; set; }
		public int NumOfOrangeCases { get; set; }
		public int NumOfGreenCases { get; set; }
		public int NumOfGrayCases { get; set; }
	}


}
