﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Quotes.Services
{
    public interface ISalesRepService
    {
        string GetSalesRepCode(int userId);

        List<int?> GetSalesRepIdsForManager(string managerCode);

        List<string> GetSalesRepCodesForManager(string managerCode);
    }
}
