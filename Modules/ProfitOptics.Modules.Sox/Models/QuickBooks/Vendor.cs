﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class Vendor
    {
        public int? Id { get; set; }
        public Address BillAddr { get; set; }
        public string TaxIdentifier { get; set; }
        public string AcctNum { get; set; }
        public string Title { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string Suffix { get; set; }
        public string CompanyName { get; set; }
        public string DisplayName { get; set; }
        public string PrintOnCheckName { get; set; }
        public Phone PrimaryPhone { get; set; }
        public Phone Mobile { get; set; }
        public Email PrimaryEmailAddr { get; set; }
        public WebAddr WebAddr { get; set; }
        public string SyncToken { get; set; }
    }
}
