﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class BoardDetails
    {
        public int? Id { get; set; }

        public string Self { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}