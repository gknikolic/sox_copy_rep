﻿var enableDialog = true;


(function (userManagement, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    // Public properties
    userManagement.somePublicProperty = {};

    // Public Methods
    userManagement.somePublicMethod = function () {
    };

    // Private Methods
    function somePrivateMethod(item) {
    }
}(window.userManagement = window.userManagement || {}, jQuery));

app.activeMenus = ["menuAdmin", "menuAdminUserManagement"]; // List all menus that should be activated when this page is opened

var dataTableUserManagement = {};
var dataTable = null;
$(document).ready(function () {
    $("body").on("click", ".applyToAll", function () {
        var roles = new Array();
        var currentRole = $(this).data("role");
        var checkedRoles = $(".checkbox-userInRole:checked");
        for (var i = 0; i < checkedRoles.length; i++) {
            roles[i] = checkedRoles.eq(i).data("role");
        }
        var zoneName;
        var regionName;
        var salesRepName;
        if ($(this).parent().parent().children(".selectedZone").children(".select2").val() != null)
            zoneName = $(this).parent().parent().children(".selectedZone").children(".select2 ").eq(0).select2('data')[0].text;
        if ($(this).parent().parent().children(".selectedRegion").children(".select2").val() != null)
            regionName = $(this).parent().parent().children(".selectedRegion").children(".select2 ").eq(0).select2('data')[0].text;
        if ($(this).parent().parent().children(".selectedSalesRep").children(".select2").val() != null)
            salesRepName = $(this).parent().parent().children(".selectedSalesRep").children(".select2 ").eq(0).select2('data')[0].text;
        var errors = [];

        roles.splice(roles.indexOf(currentRole), 1);
        var numberOfRoles = roles.length;

        if (numberOfRoles > 0) {
            for (var i = 0; i < numberOfRoles; i++) {
                if ($(".selectedZone select.select2[data-role='" + roles[i] + "'] option:contains('" + zoneName + "')").length == 0
                    && $(".selectedRegion select.select2[data-role='" + roles[i] + "'] option:contains('" + regionName + "')").length == 0
                    && $(".selectedSalesRep select.select2[data-role='" + roles[i] + "'] option:contains('" + salesRepName + "')").length == 0) {
                    errors.push(roles[i]);
                }

                $(".selectedZone select.select2[data-role='" + roles[i] + "'] option:contains('" + zoneName + "')").prop("selected", true).trigger("change");
                $(".selectedRegion select.select2[data-role='" + roles[i] + "'] option:contains('" + regionName + "')").prop("selected", true).trigger("change");
                $(".selectedSalesRep select.select2[data-role='" + roles[i] + "'] option:contains('" + salesRepName + "')").prop("selected", true).trigger("change");
            }
            errors.splice(errors.indexOf("admin"), 1);
            if (errors.length > 0)
                bootbox.alert("Warning: Unable to apply the hierarchy to the following roles: " + errors.toString() + ".");
        }
    });

    $("body").on("change", ".checkbox-userInRole", function () {
        var role = $(this).data("role");
        if ($(this).is(":checked"))
            $(".select2[data-role='" + role + "']").select2().enable(true);
        else
            $(".select2[data-role='" + role + "']").select2().enable(false);
    });

    //$('.select2-user-roles').on('change', function (e) {
    //    var role = $(this).find('option:selected').text();
    //    SetCompanyListVisibility($("#AddNewUserDialog"), role);
    //});
    var $parent = $("#AddNewUserDialog");
    RegisterCompanyListChange($parent);
    RegisterUserRoleChange($parent);
})


var dt = null;
$(document).ready(function () {
    dataTable = $('#dt-table').on('preXhr.dt', function (e, settings, data) {
        // callback before ajax call
        $('.table-overlay').show();
    }).on('xhr.dt', function (e, settings, json, xhr) {
        // callback after ajax call

        $('div.dataTables_filter input').off('keyup.DT input.DT');
        var searchDelay = null;
        $('div.dataTables_filter input').on('keyup.DT input.DT', function () {
            var search = $('div.dataTables_filter input').val();
            clearTimeout(searchDelay);
            searchDelay = setTimeout(function () {
                if (search != null) {
                    dataTable.search(search).draw();
                }
            }, 500);
        });

        $('.table-overlay').hide();
    }).DataTable({
        lengthChange: true,
        autoWidth: false,
        processing: true,
        serverSide: true,
        searching: true,
        ordering: true,
        paging: true,
        info: true,
        pageLength: 50,
        lengthMenu: [3, 10, 25, 50, 100, 250, 500],
        ajax: {
            url: $("#usersListUrl").val(),
            data: function (data) {
            },
            type: "POST"
        },
        language: {
            processing: ''
        },
        createdRow: function (row, data, dataIndex) {
            if (data.IsLocked) {
                $(row).addClass('red-text');
            }
        },
        columns: [{
            data: "Username",
            orderable: true,
            render: function (data, type, full, meta) {
                return "<span class='datacell'>" + (full.Username != null ? full.Username : "") + "</span>";
            }
        }, {
            data: "FirstName",
            orderable: true,
            className: "text-left",
            render: function (data, type, full, meta) {
                return "<span class='datacell'>" + (full.FirstName != null ? full.FirstName : "") + "</span>";
            }
        }, {
            data: "LastName",
            orderable: true,
            className: "text-left",
            render: function (data, type, full, meta) {
                return "<span class='datacell'>" + (full.LastName != null ? full.LastName : "") + "</span>";
            }
        }, {
            data: "Email",
            orderable: true,
            render: function (data, type, full, meta) {
                return "<span class='datacell'>" + (full.Email != null ? full.Email : "") + "</span>";
            }
        }, {
            data: "PhoneNumber",
            orderable: true,
            render: function (data, type, full, meta) {
                return "<span class='datacell'>" + (full.PhoneNumber != null ? full.PhoneNumber : "") + "</span>";
            }
        }, {
            data: "RolesString",
            orderable: false,
            render: function (data, type, full, meta) {
                return "<span class='datacell'>" + (full.RolesString != null ? full.RolesString : "") + "</span>";
            }
        }, {
            data: "IsLocked",
            orderable: true,
            className: "text-center",
            render: function (data, type, full, meta) {
                return "<span class='datacell'>" + (full.IsLocked != null ? full.IsLocked : "") + "</span>";
            }
        }, {
            data: null,
            orderable: false,
            className: "text-center",
            render: function (data, type, full, meta) {
                // Update Roles, Unlock, Login as User, Reset Password
                return '<div class="btn-group btn-group-xs">' +
                    '    <button title="Edit User" type="button" class="btn btn-default" data-id="' + full.Id + '" onclick="page.onEditUserClicked(this);"><span class="glyphicon glyphicon-tags"></span></button>' +
                    (full.IsLocked ? '<button title="Unlock User" type="button" class="btn btn-default" data-id="' + full.Id + '" onclick="page.onUnlockClicked(this);"><span class="fa fa-lg fa-unlock"></span></button>' :
                        '<button title="Lock User" type="button" class="btn btn-default" data-id="' + full.Id + '" onclick="page.onLockClicked(this);"><span class="fa fa-lg fa-lock"></span></button>') +
                    '    <button title="Login as User" type="button" class="btn btn-default" data-id="' + full.Id + '" onclick="page.onLoginAsUserClicked(this);"><span class="glyphicon glyphicon-new-window"></span></button>' +
                    '    <button title="Reset Password" type="button" class="btn btn-default" data-id="' + full.Id + '" data-username="' + full.Username + '" onclick="page.onResetPasswordClicked(this);"><span class="glyphicon glyphicon-off"></span></button>' +
                    (full.IsEnabled ? '<button title="Delete User" type="button" class="btn btn-default" data-id="' + full.Id + '" onclick="page.onDisableClicked(this);"><span class="fa fa-lg fa-trash"></span></button>' :
                        '<button title="Enable User" type="button" class="btn btn-default" data-id="' + full.Id + '" onclick="page.onEnableClicked(this);"><span class="fa fa-lg fa-ban "></span></button>') +
                    '</div>';
            }
        }]
    });

    dt = dataTable;
});

// Encapsulate page logic. Check out page.js for example of public and private methods and properties
(function (page, $, undefined) {
    page.onLoginAsUserClicked = function (el) {
        var userId = $(el).data("id");
        window.location.href = $("#impersonateUserUrl").val() + userId;
    };

    page.onEditUserClicked = function (el) {
        if (enableDialog == false) {
            return;
        }
        enableDialog = false;

        var userId = $(el).data("id");
        var data = { userId: userId };
        app.AjaxUpdateUserDialog(data).done(function (returnData) {
            enableDialog = true;
            $("#UpdateUserDialogHolder").html(returnData);
            
            var $parentDialog = $("#UpdateUserDialog");
            $('#hdnUserDialogName').val('UpdateUserDialog');

            $parentDialog.modal({ show: true });

            RegisterSelect2Dropdowns($parentDialog);
            var role = $parentDialog.find($('#SelectedRoles option:selected')).text();
            SetCompanyListVisibility($parentDialog, role);
            RegisterUserRoleChange($parentDialog);
            RegisterCompanyListChange($parentDialog);
            $parentDialog.find('.companyList').trigger('change');

            RegisterUpdateUser();

            if (returnData.Status == "ok") {
                app.showNotification("Success!");
                dt.ajax.reload();
            }
        }).fail(function (response) {
            enableDialog = true;
        });
    };
    page.onUnlockClicked = function (el) {
        bootbox.confirm({
            message: "Are you sure you want to unlock the user?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    page.UnlockUser(el);
                }
            }
        });
    }

    page.onDisableClicked = function (el) {
        //if (enableDialog == false) {
        //    return;
        //}
        //enableDialog = false;

        bootbox.confirm({
            message: "Are you sure you want to delete the user?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    page.DisableUser(el);
                    //enableDialog = true;
                }
            }
        });
    }

    page.DisableUser = function (el) {
        var userId = $(el).data("id");
        var data = { userId: userId };
        app.AjaxToggleUserEnabled(data).done(function (returnData) {
            if (returnData.Status == "ok") {
                app.showNotification("Success!");
                dt.ajax.reload();
            }
        });
    };

    page.onEnableClicked = function (el) {
        page.EnableUser(el);
    };

    page.EnableUser = function (el) {
        var userId = $(el).data("id");
        var data = { userId: userId };
        app.AjaxToggleUserEnabled(data).done(function (returnData) {
            if (returnData.Status == "ok") {
                app.showNotification("Success!");
                dt.ajax.reload();
            }
        });
    };

    page.UnlockUser = function (el) {
        var userId = $(el).data("id");
        var data = { userId: userId };
        app.AjaxToggleUserLockAsync(data).done(function (returnData) {
            if (returnData.Status == "ok") {
                app.showNotification("Success!");
                dt.ajax.reload();
            }
        });
    };

    page.onLockClicked = function (el) {
        bootbox.confirm({
            message: "Are you sure you want to lock the user?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    page.LockUserAsync(el);
                }
            }
        });
    };

    page.LockUserAsync = function (el) {
        var userId = $(el).data("id");
        var data = { userId: userId };
        app.AjaxToggleUserLockAsync(data).done(function (returnData) {
            if (returnData.Status == "ok") {
                app.showNotification("Success!");
                dt.ajax.reload();
            }
        });
    }

    page.onResetPasswordClicked = function (el) {

        var username = $(el).data("username");
        if (!confirm("Are you sure you want to reset password for " + username + "?")) {
            return;
        }

        var userId = $(el).data("id");
        var data = { userId: userId };

        app.AjaxResetPassword(data).done(function (returnData) {
            if (returnData.Status == "ok") {
                app.showNotification("Success!");
            }
        });
    };

}(window.page = window.page || {}, jQuery));

$("#applyFiltersButton").click(function () {
    FilterTable(true);
});


function FilterTable(resetPaging) {
    $("#dt-table").DataTable().ajax.reload(null, resetPaging);
}

$(document).ready(function () {
    $("#filterRole").select2({
        width: '100%'
    });
    $("#filterUsername").select2({
        width: '100%'
    });
    $("#addNewUserButton").click(function () {
        //if (enableDialog == false) {
        //    return;
        //}
        //enableDialog = false;

        $("#AddNewUserDialog").modal({
            show: true
            //callback: function () {
            //    enableDialog = true;
            //}
        });
    });
});

app.activeMenus = ["menuAdmin", "menuDataBrowser"];
function GetChildrenForGCParentCompany(parentCompanyId) {
    app.blockUI('Loading...');

    var childCompanyPermissionValues = $('#ChildCompanyPermissionValues').val();

    $.ajax({
        type: "json",
        method: "POST",
        url: "Users/GetParentCompanyWithChildren",
        data: { parentCompanyId: parentCompanyId, childCompanyPermissionValues: childCompanyPermissionValues },
        success: function (response) {
            var dialogName = $('#hdnUserDialogName').val();
            $("#" + dialogName + " #companyPermissionsTable > tbody:first").append(response);
            RegisterPersmissionChecks();

            $('.childCompanyCb').trigger('change');
        },
        error: function (response) {
            var error = 400;
        },
        complete: function () {
            $.unblockUI();
        }

    });
}


function UpdateParentCompany(companyId) {
    if (enableDialog == false) {
        return;
    }
    enableDialog = false;
    var parentCompanyId = 0;
    if (companyId == null) {
        parentCompanyId = $('#parentDiv').find('.active').find('input').val();
    }
    else {
        parentCompanyId = companyId;
	}
   

    $.ajax({
        type: "GET",
        url: "AjaxUpdateCompanyDialog",
        data: { parentCompanyId: parentCompanyId },
        contentType: 'application/json',
        dataType: "html",
        success: function (response) {
            $("#UpdateParentCompanyDialogHolder").html(response);
            $("#UpdateParentCompanyDialog").modal({ show: true });
            enableDialog = true;
        },
        error: function (response) {
            var error = 400;
            enableDialog = true;
        }
    });
}

function AddParentCompany() {
    if (enableDialog == false) {
        return;
    }
    enableDialog = false;

    $.ajax({
        type: "GET",
        url: "AddParentCompanyDialog",
        contentType: 'application/json',
        dataType: "html",
        success: function (response) {
            $("#AddParentCompanyDialogHolder").html(response);
            $("#AddNewParentCompanyDialog").modal({ show: true });
            enableDialog = true;
        },
        error: function (response) {
            var error = 400;
            enableDialog = true;
        }
    });
}

//function DeleteParentCompany() {

//    var parentCompanyId = $(".active").find('input').val();

//    if (window.confirm("Are you sure?")) {
//        $.ajax({
//            type: "GET",
//            url: "DeleteParentCompany",
//            data: { parentCompanyId: parentCompanyId },
//            contentType: 'application/json',
//            success: function (response) {
//                location.reload();
//            },
//            error: function (response) {
//                var error = 400;
//            }
//        });
//    }
   
//}

function DeleteParentCompany(companyId) {
    if (enableDialog == false) {
        return;
    }
    enableDialog = false;
    var parentCompanyId = 0;
    if (companyId == null) {
        parentCompanyId = $(".active").find('input').val();
    }
    else {
        parentCompanyId = companyId;
    }


    $.ajax({
        type: "GET",
        url: "DeleteParentCompanyDialog",
        contentType: 'application/json',
        data: { parentCompanyId: parentCompanyId},
        dataType: "html",
        success: function (response) {
            $("#DeleteParentCompanyDialogHolder").html(response);
            $("#DeleteParentCompanyDialog").modal({ show: true });
            enableDialog = true;
        },
        error: function (response) {
            var error = 400;
            enableDialog = true;
        }
    });

}

function UpdateChildCompany() {

    var parentCompanyId = $('#parentDiv').find('.active').find('input').val();
    var childCompanyId = $('#childDiv-' + parentCompanyId).find('.active').find('input').val();

    $.ajax({
        type: "GET",
        url: "AjaxUpdateChildCompanyDialog",
        data: { childCompanyId: childCompanyId },
        contentType: 'application/json',
        dataType: "html",
        success: function (response) {
            $("#UpdateChildCompanyDialogHolder").html(response);
            $("#UpdateChildCompanyDialog").modal({ show: true });
           
        },
        error: function (response) {
            var error = 400;
        }
    });
}

function AddChildCompany() {

    var parentCompanyId = $('#parentDiv').find('.active').find('input').val();

    $.ajax({
        type: "GET",
        url: "AddChildCompanyDialog",
        data: { parentCompanyId: parentCompanyId },
        contentType: 'application/json',
        dataType: "html",
        success: function (response) {
            $("#AddChildCompanyDialogHolder").html(response);
            $("#AddNewChildCompanyDialog").modal({ show: true });
          
        },
        error: function (response) {
            var error = 400;
        }
    });
}

function DeleteChildCompany() {

    var parentCompanyId = $('#parentDiv').find('.active').find('input').val();
    var childCompanyId = $('#childDiv-' + parentCompanyId).find('.active').find('input').val();

    if (window.confirm("Are you sure?")) {
        $.ajax({
            type: "GET",
            url: "DeleteChildCompany",
            data: { childCompanyId: childCompanyId },
            //data: "parentCompanyId=" + parentCompanyId,
            //data: JSON.stringify({ parentCompanyId: parentCompanyId }),
            contentType: 'application/json',
            success: function (response) {
                location.reload();
            },
            error: function (response) {
                var error = 400;
            }
        });
    }

}

function RegisterSelect2Dropdowns($parent) {
    $parent.find($(".select2-user-roles")).removeAttr("multiple");
    $parent.find($(".select2-user-roles")).select2({
        //closeOnSelect: false,
        dropdownParent: $parent,
        placeholder: "Select user role",
        allowClear: false
    });

    $parent.find($(".select2-user-parentCompanies")).select2({
        closeOnSelect: false,
        dropdownParent: $parent,
        placeholder: "Select companies",
        allowClear: false
    });
}

function SetCompanyListVisibility($parent, role) {
    if (role === 'vendoruser' || role === 'customeruser') {
        $parent.find($('.companyList')).attr('disabled', false);
    } else {
        $parent.find($('.companyList')).val('');
        $parent.find($('.companyList')).trigger('change');
        $parent.find($('.companyList')).attr('disabled', 'disabled');
        $parent.find($('#ChildCompanyPermissionValues')).val('');
    }
}

function RegisterUserRoleChange($parent) {
    $parent.find($('.select2-user-roles')).on('change', function (e) {
        var role = $(this).find('option:selected').text();
        SetCompanyListVisibility($parent, role);
    });
}

function RegisterCompanyListChange($parent) {
    var companyListIds = [];
    $parent.find($('.companyList')).on('change', function (e) {
        var selectedValues = $(this).val();
        if (selectedValues == null) {
            selectedValues = [];
        }
        if (selectedValues.length > companyListIds.length) { //new copmany is added to list
            var selectedVal = selectedValues.filter(x => !companyListIds.includes(x));
            $.each(selectedVal, function (i, e) {
                GetChildrenForGCParentCompany(e);
            });
            //GetChildrenForGCParentCompany(selectedVal[0]);

        }
        else { //company is deleted from list
            var deselectedVal = companyListIds.filter(x => !selectedValues.includes(x));
            var trclass = ".comp_" + deselectedVal[0];
            $(trclass).remove();
        }
        companyListIds = selectedValues;
        if (selectedValues.length == 0) {
            $(".companyPermissionsDiv").hide();
        }
        else {
            $(".companyPermissionsDiv").show();
        }
    });
}

function RegisterPersmissionChecks($parent) {
    $('.parentCompanyCb').unbind('change').bind('change', function(){
        var cbIndex = $(this).parent().index();
        var checked = $(this).prop('checked');
        var parentClass = $(this).parent().parent().prop('class');
        var itemsToIterate = $('.' + parentClass);

        for (var i = 0; i < itemsToIterate.length; i++) {
            if (i > 0) { // skipping first element - that is a parent
                var rowArray = $(itemsToIterate[i]).find('table tr');
                for (var j = 0; j < rowArray.length; j++) {
                    var currentRow = rowArray[j];
                    var cell = $(currentRow).find('td').eq(cbIndex);
                    $(cell).find('input[type=checkbox]').prop("checked", checked);
                }
            }
        }
    });

    $('.childCompanyCb').unbind('change').bind('change', function () {
        $chk = $(this);
        SetParentChkChecked($chk);
    });
}

function SetParentChkChecked($chk) {
    var cbIndex = $chk.parent().index();
    $chk.parent().parent().parent().parent().parent().parent().parent().prev().find('td').eq(cbIndex).find('input[type=checkbox]').prop("checked", false);
    var rowsToIterate = $chk.parent().parent().parent().find('tr');
    var checkCount = 0;
    var totalRows = rowsToIterate.length;
    for (var i = 0; i < totalRows; i++) {
        var cell = $(rowsToIterate[i]).find('td').eq(cbIndex);
        var currentIsChecked = $(cell).find('input[type=checkbox]').prop("checked");
        if (currentIsChecked) {
            checkCount++
        }
    }
    if (checkCount === totalRows) {
        $chk.parent().parent().parent().parent().parent().parent().parent().prev().find('td').eq(cbIndex).find('input[type=checkbox]').prop("checked", true);
    }
}

function RegisterUpdateUser() {
    $('#btnUpdateUser').click(function () {
        var model = GetFormData($('form#updateUser'));
        var selectedPermissions = $('form#updateUser').find('.childCompanyCb:checked');
        model.SelectedParentCompanies = $('#SelectedParentCompanies').val();
        var permissionArray = [];
        for (var i = 0; i < selectedPermissions.length; i++) {
            permissionArray.push($(selectedPermissions[i]).val());
        }
        model.ChildCompanyPermissions = permissionArray;
        var data = { model: model };
        app.SaveUser(data).done(function (returnData) {
            if (returnData.Status === "ok") {
                app.showNotification("Success!");
                dt.ajax.reload();
                $('#UpdateUserDialog').modal('hide');
            }
        });
    });
}

//$('#updateUser').on("submit", function (e) {

//    var model = GetFormData($('form#updateUser'));
//    var selectedPermissions = $('form#updateUser').find('.childCompanyCb:checked');
//    model.SelectedParentCompanies = $('#SelectedParentCompanies').val();
//    var permissionArray = [];
//    for (var i = 0; i < selectedPermissions.length; i++) {
//        permissionArray.push($(selectedPermissions[i]).val());
//    }
//    model.ChildCompanyPermissions = permissionArray;
//    var data = { model: model };
//    app.SaveUser(data).done(function (returnData) {
//        if (returnData.Status === "ok") {
//            app.showNotification("Success!");
//            dt.ajax.reload();
//            $('#UpdateUserDialog').modal('hide');
//        }
//    });
//});

function GetFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}


