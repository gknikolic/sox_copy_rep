﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
    [Table("WWPodContainer")]
    public class WWPodContainer
    {
        public int Id { get; set; }
        public int? NoteId { get; set; }
        public WWPod Note { get; set; }
        public string AdditionalNote { get; set; }
        public virtual ICollection<WWTrackingData> WWTrackingData { get; set; }
        public virtual ICollection<WWSignature> Signatures { get; set; }
        public virtual ICollection<WWPicture> Pictures { get; set; }
        public virtual ICollection<GCTraysLifecycleStatusTypes> GCTraysLifecycleStatusTypes { get; set; }
        public virtual ICollection<WWBarcode> Barcodes { get; set; }
    }
}
