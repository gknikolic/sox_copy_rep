namespace ProfitOptics.Framework.Web.RestApi.Models.Customer
{
    public class CustomerQuery : Query
    {
        public int? DistributorId { get; set; }

        public int? ProductId { get; set; }
        public CustomerQuery(int? distributorId, int? productId, int page, int itemsPerPage, string sortColumn, string sortDirection, string filterColumn, string filterValue)
            : base(page, itemsPerPage, sortColumn, sortDirection, filterColumn, filterValue)
        {
            DistributorId = distributorId;
            ProductId = productId;
        }
    }
}