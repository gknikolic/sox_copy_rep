﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    public interface IHeaderWidget
    {
        Task<IViewComponentResult> InvokeAsync();

        int OrderNumber();
    }
}