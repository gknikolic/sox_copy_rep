﻿using ProfitOptics.Modules.Sox.Models.QuickBooks;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class QbCustomerTestVM
    {
        public string Status { get; set; }
        public List<Customer> Customers { get; set; }
    }
}
