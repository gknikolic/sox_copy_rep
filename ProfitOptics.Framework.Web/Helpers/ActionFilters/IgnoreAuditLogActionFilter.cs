﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using Microsoft.AspNetCore.Mvc.Filters;

namespace ProfitOptics.Framework.Web.Helpers.ActionFilters
{
    public class IgnoreAuditLogActionFilter : ActionFilterAttribute
    {
    }
}