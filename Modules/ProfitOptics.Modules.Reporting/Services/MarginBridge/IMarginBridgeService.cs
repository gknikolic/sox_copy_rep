﻿using System;
using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Models.Common;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Services.MarginBridge
{
    public interface IMarginBridgeService
    {
        /// <summary>
        /// Returns all records from the "ReportSalesHistory" table.
        /// </summary>
        /// <returns></returns>
        List<ReportSalesHistory> GetSalesHistory();

        /// <summary>
        /// Returns filtered data from ReportSalesHistory table wrapped in ReportSalesHistoryModel
        /// </summary>
        /// <param name="filter">Filter which consists of "filter widget", margin bridge type, location type and product type components. </param>
        /// <param name="period">Date range</param>
        /// <param name="periodType"></param>
        /// <returns></returns>
        List<ReportSalesHistoryModel> GetSalesHistory(MarginBridgeFilterModel filter, MarginBridgePeriod period, PeriodType periodType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstPeriodSales"></param>
        /// <param name="secondPeriodSales"></param>
        /// <param name="dataPoints"></param>
        /// <param name="maxProductLevel"></param>
        /// <param name="maxLocationLevel"></param>
        /// <param name="productDbColumnName"></param>
        /// <param name="locationDbColumnName"></param>
        /// <returns></returns>
        List<FullOutput> GetFullOutput(IEnumerable<InputDataPointExt> firstPeriodSales, 
            IEnumerable<InputDataPointExt> secondPeriodSales, List<OutputDataPoint> dataPoints, ProductLevelType maxProductLevel,
            LocationLevelType maxLocationLevel, string productDbColumnName, string locationDbColumnName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="period"></param>
        /// <returns></returns>
        List<ReportSalesHistoryModel> GetRawSalesHistory(MarginBridgeFilterModel filter, MarginBridgePeriod period);

        /// <summary>
        ///
        /// </summary>
        /// <param name="model"></param>
        /// <param name="groupByMaxLevel"></param>
        void CalculateResults(MargineBridgeResponseDataModel model, bool groupByMaxLevel = true);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="firstPeriod"></param>
        /// <param name="secondsPeriod"></param>
        /// <returns></returns>
        DataBridgeRawData GetDataBridgeRawData(MarginBridgeFilterModel filter, MarginBridgePeriod firstPeriod, MarginBridgePeriod secondsPeriod);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="firstPeriod"></param>
        /// <param name="secondsPeriod"></param>
        /// <returns></returns>
        List<DataBridgeItem> GetDataBridgeData(MarginBridgeFilterModel filter, MarginBridgePeriod firstPeriod, MarginBridgePeriod secondsPeriod);

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        string GetEnumDescription(Enum value);

        PaginationResult_OLD<StatisticsResponse> GetStatisticsData(JQueryDataTablesParamModel aoData);
        InputData CreateProcessInputData(MarginBridgeFilterModel filter, MarginBridgePeriod period, PeriodType periodType, ProductLevelType maxProductLevel, LocationLevelType maxLocationLevel);
    }
}