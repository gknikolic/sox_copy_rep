﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerServiceConfig : IEntityTypeConfiguration<CTContainerService>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTContainerService> builder)
        {
            builder.ToTable("CTContainerService");
            builder.HasKey(entity => entity.Id);
        }
    }
}
