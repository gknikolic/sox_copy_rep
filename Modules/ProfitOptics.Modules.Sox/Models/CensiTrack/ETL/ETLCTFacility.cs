﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTFacility
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("processingFacilityId")]
        public long? ProcessingFacilityId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("compositeFacilityKey")]
        public string CompositeFacilityKey { get; set; }

        [JsonProperty("isProcessingFacility")]
        public bool? IsProcessingFacility { get; set; }
    }
}
