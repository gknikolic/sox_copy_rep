﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
	public class ETLCTContainerAssembly
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("containerDetails")]
        public ETLCTContainerAssemblyDetail ContainerDetails { get; set; }

        [JsonProperty("assemblyComment")]
        public string AssemblyComment { get; set; }
    }

   
}
