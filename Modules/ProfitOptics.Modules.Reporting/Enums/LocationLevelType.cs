﻿using System.ComponentModel;

namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum LocationLevelType
    {
        [Description("All Locations")]
        AllLocations = 0,
        [Description("Ship to")]
        ShipTo = 1,
        [Description("Bill to")]
        BillTo = 2,
        [Description("Territory")]
        Territory = 3,
        [Description("Region")]
        Region = 4,
        [Description("Zone")]
        Zone = 5
    }
}