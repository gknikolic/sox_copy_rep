USE [SoxQATest]

BEGIN
 
CREATE TABLE CTCase
(
	[Id] INT PRIMARY KEY IDENTITY(1,1),
	[CaseReference] [nvarchar](50) NOT NULL,
	[DueTimestamp] [datetime] NOT NULL,
	[DestinationName] [nvarchar](100) NULL,
	[CaseDoctor] [nvarchar](200) NULL,
	[CaseStatus] [int] NULL,
	[IsCartComplete] [bit] NULL,
	[CartStatus] [nvarchar](50) NULL,
	[IsCanceled] [bit] NULL,
	[Shipped] [bit] NULL,
	[PreviousDueTimestamp] [datetime] NULL,
	[PreviousDestinationName] [nvarchar](100) NULL,
	[ScheduleId] [int] NULL,
	[DueInMinutes] [numeric](18, 6) NOT NULL,
	[DueInHoursAndMinutes] [nvarchar](10) NOT NULL,
	[PreviousDueInMinutes] [numeric](18, 6) NULL,
	[PreviousDueInHoursAndMinutes] [nvarchar](10) NULL,
	[Changed] [bit] NULL,
	[Priority] [bit] NULL,
	[DateTimeNow] [datetime] NOT NULL,
	[EstimatedTime] [nvarchar](20) NULL
)

 END
