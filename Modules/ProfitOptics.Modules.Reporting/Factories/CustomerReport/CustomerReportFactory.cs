﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Models.CustomerReport;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;
using ProfitOptics.Modules.Reporting.Services;

namespace ProfitOptics.Modules.Reporting.Factories.CustomerReport
{
    public class CustomerReportFactory : ICustomerReportFactory
    {
        #region Fields

        private readonly IMemoryCacheHelper _memoryCacheHelper;
        private readonly ISalesHistoryService _salesHistoryService;
        private readonly ReportingSettings _settings;
        private readonly IUserHierarchyService _userHierarchyService;

        #endregion Fields

        #region Ctor

        public CustomerReportFactory(IMemoryCacheHelper memoryCacheHelper,
            ISalesHistoryService salesHistoryService,
            ReportingSettings reportingSettings,
            IUserHierarchyService userHierarchyService)
        {
            _memoryCacheHelper = memoryCacheHelper;
            _salesHistoryService = salesHistoryService;
            _settings = reportingSettings;
            _userHierarchyService = userHierarchyService;
        }

        #endregion Ctor

        #region Public Methods

        public CustomerReportViewModel GetCustomerReportViewModel(int userId, bool isShipTo = false)
        {
            string tableNameSuffix = isShipTo ? "ShipTos" : "Customers";

            var columnNames = _settings.ReportTableColumnNames.Split(",");

            return new CustomerReportViewModel
            {
                IsCorporate = _userHierarchyService.IsCorporateUser(userId),
                IsShipTo = isShipTo,
                TopCustomers = new CustomerReportTableModel
                {
                    TableName = $"Top {tableNameSuffix}",
                    ColumnNames = columnNames,
                },
                BottomCustomers = new CustomerReportTableModel
                {
                    TableName = $"Top {tableNameSuffix} With the Least Growth",
                    ColumnNames = columnNames,
                }
            };
        }

        public PagedList<CustomerReportModel> GetTopCustomersReportModels(IDataTablesRequest request, ReportFilterModel filter, bool isShipTo)
        {
            var records = GetTopCustomers(filter, isShipTo);

            List<CustomerReportModel> paged = records.ApplyPagination(request).ToList();

            int totalCount = records.Count();

            return new PagedList<CustomerReportModel>(paged, totalCount);
        }

        public PagedList<CustomerReportModel> GetBottomCustomersReportModels(IDataTablesRequest request, ReportFilterModel filter, bool isShipTo)
        {
            var records = GetBottomCustomers(filter, isShipTo);

            List<CustomerReportModel> paged = records.ApplyPagination(request).ToList();

            int totalCount = records.Count();

            return new PagedList<CustomerReportModel>(paged, totalCount);
        }

        #endregion Public Methods

        #region Private Methods

        private IQueryable<CustomerReportModel> GetCachedTopCustomers(string key, Func<IQueryable<CustomerReportModel>> acquire)
        {
            var time = int.TryParse(_settings.CustomerReportCacheTimeInMinutes, out int cacheTime) ? cacheTime : 0;

            IQueryable<CustomerReportModel> records = _memoryCacheHelper.Get(key, acquire, time);

            return records;
        }

        private bool GetTopCustomersPredicate(CustomerReportModel customer)
        {
            //this condition is currently ignored and always return true
            //int numberOfSales = customer.MonthlySalesModel.Count;

            //return customer.MonthlySalesModel[numberOfSales - 1].Sales != decimal.Zero &&
            //       customer.MonthlySalesModel[numberOfSales - 2].Sales != decimal.Zero;

            return true;
        }

        private IQueryable<CustomerReportModel> GetTopCustomers(ReportFilterModel filter, bool isShipTo)
        {
            var key = GetTopCustomersCacheKey(filter, isShipTo);

            var records = GetCachedTopCustomers(key, () => GetCustomerReportModels(filter, isShipTo));
            var topCustomersData = records.Where(GetTopCustomersPredicate);

            return topCustomersData.AsQueryable();
        }

        private IQueryable<CustomerReportModel> GetBottomCustomers(ReportFilterModel filter, bool isShipTo)
        {
            var key = GetTopCustomersCacheKey(filter, isShipTo);

            var records = GetCachedTopCustomers(key, () => GetCustomerReportModels(filter, isShipTo));

            var bottomCustomersData = records
                .Where(x => x.LtmSalesPercent < _settings.MinGrowthDeclinePercentage)
                .OrderByDescending(c => c.PreviousLtmSalesAndLtmSalesDifference);

            return bottomCustomersData.AsQueryable();
        }

        private IQueryable<CustomerReportModel> GetCustomerReportModels(ReportFilterModel filter, bool isShipTo)
        {
            DateTime ltmEndDate = DateTime.Now.Date.AddDays(-1);
            DateTime ltmStartDate = ltmEndDate.AddYears(-1).AddDays(1);

            DateTime previousLtmEndDate = ltmStartDate.Date.AddDays(-1);
            DateTime previousLtmStartDate = ltmStartDate.AddYears(-1);

            List<ReportSalesHistory> filteredSalesHistory =
                _salesHistoryService.GetFilteredSalesHistories(filter).ToList(); // with efcore 3, this approach is no longer viable, do not filter queryable and then do other transformations on it, client side eval is not supported

            var customerReportData = GetCustomerReportData(filteredSalesHistory, isShipTo, ltmEndDate, ltmStartDate, previousLtmEndDate, previousLtmStartDate)
                .OrderByDescending(x => x.LtmSales)
                .ToList();

            FillCustomerReportDataMissingMonths(customerReportData, ltmEndDate, ltmStartDate);

            return customerReportData.AsQueryable();
        }

        private IEnumerable<CustomerReportModel> GetCustomerReportData(IEnumerable<ReportSalesHistory> filteredSalesHistory, bool isShipTo, DateTime ltmEndDate, DateTime ltmStartDate, DateTime previousLtmEndDate, DateTime previousLtmStartDate)
        {
            var customerModels = (from salesHistory in filteredSalesHistory.Where(i => i.InvoiceDate >= previousLtmStartDate && i.InvoiceDate < ltmEndDate)
                    group salesHistory by new
                    {
                        Id = isShipTo ? salesHistory.ShipToId : salesHistory.BillToId,
                        CustomerName = isShipTo ? salesHistory.ReportShipTo.ShipToName : salesHistory.ReportBillTo.BillToName,
                        CustomerNum = isShipTo ? salesHistory.ReportShipTo.ShipToNum : salesHistory.ReportBillTo.BillToNum
                    }
                    into g
                    select g)
                .Select(sales => new CustomerReportModel
                {
                    CustomerName = sales.Key.CustomerName + " (" + sales.Key.CustomerNum + ")",
                    PreviousLtmSales =
                        sales.Any(x => x.InvoiceDate >= previousLtmStartDate && x.InvoiceDate < previousLtmEndDate)
                            ? sales.Where(x => x.InvoiceDate >= previousLtmStartDate && x.InvoiceDate < previousLtmEndDate)
                                .Sum(z => z.ExtPrice)
                            : 0.0m,
                    MonthlySales = sales
                        .Where(x => x.InvoiceDate >= ltmStartDate && x.InvoiceDate < ltmEndDate)
                        .GroupBy(x => x.InvoiceDate.Year * 100 + x.InvoiceDate.Month).Select(x =>
                            new MonthlySalesModel
                            {
                                Month = x.Key,
                                Sales = x.Sum(z => z.ExtPrice)
                            }).ToList()
                });

            return customerModels;
        }

        private void FillCustomerReportDataMissingMonths(IEnumerable<CustomerReportModel> customerReportData, DateTime ltmEndDate, DateTime ltmStartDate)
        {
            foreach (var customer in customerReportData)
            {
                Dictionary<int, MonthlySalesModel> monthlySales = customer.MonthlySales.ToDictionary(p => p.Month, p => p);

                var currentDate = ltmStartDate;

                while (currentDate < ltmEndDate)
                {
                    if (!monthlySales.ContainsKey(currentDate.Year * 100 + currentDate.Month))
                        monthlySales.Add(currentDate.Year * 100 + currentDate.Month, new MonthlySalesModel
                        {
                            Month = currentDate.Year * 100 + currentDate.Month,
                            Sales = decimal.Zero
                        });

                    currentDate = currentDate.AddMonths(1);
                }

                customer.MonthlySales = monthlySales.Values.OrderBy(p => p.Month).ToList();
            }
        }

        private string GetTopCustomersCacheKey(ReportFilterModel filter, bool isShipTo)
        {
            string key = string.Format(_settings.TopCustomersCacheKey, isShipTo, JsonConvert.SerializeObject(filter)) + "-SalesRepId:" + filter.SalesRepId.ToString();

            return key;
        }

        #endregion Private Methods
    }
}