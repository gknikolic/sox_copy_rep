﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCInvoiceConfig : IEntityTypeConfiguration<GCInvoice>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCInvoice> builder)
        {
            builder.ToTable("GCInvoice");
            builder.HasKey(entity => entity.Id);
        }
    }
}
