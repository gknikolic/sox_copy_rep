﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Core.Settings
{
    public class Recaptcha
    {
        public string RecaptchaSiteKey { get; set; }
        public string RecaptchaSecretKey { get; set; }
    }
}
