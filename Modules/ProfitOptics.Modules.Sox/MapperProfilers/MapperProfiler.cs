﻿using AutoMapper;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.DataLayer.GroundControl;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.Sox.Models;
using ProfitOptics.Modules.Sox.Models.CensiTrack.ETL;
using ProfitOptics.Modules.Sox.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.MapperProfilers
{
    public class MapperProfiler : Profile
    {
        public MapperProfiler()
        {
            CreateMap<GCCustomer, GCCustomerModel>().ReverseMap();
            //.ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<GCCase, GCCaseModel>().ReverseMap();

            //CreateMap<CTCaseContainer, TrayModel>().ReverseMap();
            CreateMap<CTContainerTypeModel, TrayModel>().ReverseMap();
            CreateMap<GCTray, TrayModel>().ReverseMap();
            CreateMap<CTContainerType, CTContainerTypeModel>().ReverseMap();

            CreateMap<ETLLogs, ETLLogModel>().ReverseMap();

            CreateMap<GCVendor, GCVendorModel>().ReverseMap();

            CreateMap<Framework.Web.Framework.Identity.Domain.GCChildCompany, ChildCompanyModel>().ReverseMap();

            CreateMap<Framework.Web.Framework.Identity.Domain.GCChildCompany, ChildCompanyModel>().ReverseMap();

            CreateMap<GCLocationInfo, GCLocationInfoModel>().ReverseMap();

            CreateMap<GCProcedureType, ProcedureTypeModel>().ReverseMap();

            CreateMap<CTContainerService, ProcedureModel>()
                .ForMember(dest => dest.ProcedureTypeName, opt => opt.MapFrom(src => src.GCProcedureType.Title))
                .ReverseMap();

            ///Mapping from CensiTrack mapper
            CreateMap<ETLCTCase, Framework.DataLayer.CTCase>();

            CreateMap<ETLCTFacility, Framework.DataLayer.CTFacility>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ETLCTContainerService, Framework.DataLayer.CTContainerService>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ETLCTContainerType, Framework.DataLayer.CTContainerType>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ETLCTContainerTypeActual, Framework.DataLayer.CTContainerTypeActual>()
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ETLCTVendor, Framework.DataLayer.CTVendor>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ETLCTContainerItem, Framework.DataLayer.CTContainerItem>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CTProduct, opt => opt.MapFrom(src => src.Product));

            CreateMap<ETLCTProduct, Framework.DataLayer.CTProduct>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ETLCTContainerTypeGraphic, Framework.DataLayer.CTContainerTypeGraphic>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.GraphicId, opt => opt.MapFrom(src => src.GraphicKey.GraphicId))
                .ForMember(dest => dest.IsGlobal, opt => opt.MapFrom(src => src.GraphicKey.IsGlobal));

            CreateMap<ETLCTCaseContainer, Framework.DataLayer.CTCaseContainer>()
                .ForMember(dest => dest.DueTimestampOriginal, opt => opt.MapFrom(src => src.DueTimestamp))
                .ForMember(dest => dest.EndTimestampOriginal, opt => opt.MapFrom(src => src.EndTimestamp))
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(dest => dest.MultipleBasisOriginal, opt => opt.MapFrom(src => src.MultipleBasis))
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<ETLCTLocationType, Framework.DataLayer.CTLocationType>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.LegacyId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ETLCTLocation, Framework.DataLayer.CTLocation>()
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<ETLCTContainerTypeActualHistoryItem, Framework.DataLayer.CTContainerTypeActualHistoryItem>()
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<ETLCTBILoadResult, Framework.DataLayer.CTBILoadResult>()
                .ForMember(dest => dest.LoadDateOriginal, opt => opt.MapFrom(src => src.LoadDate))
                .ForMember(dest => dest.InsertTimestampOriginal, opt => opt.MapFrom(src => src.InsertTimestamp))
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(dest => dest.LoadTimestampOriginal, opt => opt.MapFrom(src => src.LoadTimestamp))
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<ETLCTBILoadResultContent, Framework.DataLayer.CTBILoadResultContent>()
                .ForMember(dest => dest.LastUpdateOriginal, opt => opt.MapFrom(src => src.LastUpdate))
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<ETLCTBILoadResultIndicator, Framework.DataLayer.CTBILoadResultIndicator>()
                .ForMember(dest => dest.InsertTimestampOriginal, opt => opt.MapFrom(src => src.InsertTimestamp))
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<ETLCTBILoadResultGraphic, Framework.DataLayer.CTBILoadResultGraphic>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.UpdateTimestampOriginal, opt => opt.MapFrom(src => src.UpdateTimestamp))
                .ForMember(dest => dest.GraphicId, opt => opt.MapFrom(src => src.GraphicKey.GraphicId))
                .ForMember(dest => dest.IsGlobal, opt => opt.MapFrom(src => src.GraphicKey.IsGlobal));

            CreateMap<Framework.DataLayer.CTCaseContainer, Framework.DataLayer.CTCaseContainerHistorical>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CTCaseContainerId, opt => opt.MapFrom(src => src.Id));
            ///

            CreateMap<GCSystemAlert, GCSystemAlertModel>()
                .ForMember(dest => dest.GCCaseId, opt => opt.MapFrom(src => src.GCTray.GCCaseId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.GCSystemAlertType.Name))
                .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.GCSystemAlertType.Message))
                .ForMember(dest => dest.CaseReference, opt => opt.MapFrom(src => src.GCTray.GCCase.Title))
                .ForMember(dest => dest.ActualsName, opt => opt.MapFrom(src => src.GCTray.CTContainerTypeActual.Name))
                .ForMember(dest => dest.ActualsParentName, opt => opt.MapFrom(src => src.GCTray.CTContainerTypeActual.ParentName));
        }
    }
}
