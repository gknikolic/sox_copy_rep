﻿namespace ProfitOptics.Modules.Support.Models
{
    public class BacklogIssueModel
    {
        public string Key { get; set; }

        public string Summary { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }

        public string Assignee { get; set; }

        public string Reporter { get; set; }

        public string LocalCreator { get; set; }

        public string ProjectName { get; set; }

        public string Component { get; set; }

        public string Updated { get; set; }

        public string Description { get; set; }

        public string Priority { get; set; }

        public int CommentCount { get; set; }

        public bool CanEdit { get; set; }
    }
}