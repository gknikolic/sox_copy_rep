﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class TrayModel
    {
        public string ServiceName { get; set; }
        public string Name { get; set; }
        public string ParentName { get; set; }
        public string LifecycleStatus { get; set; }
        public int? LifecycleStatusId { get; set; }
        public string ColorStatus { get; set; }
        public int CTContainerTypeActualId { get; set; }
		public string Barcode { get; set; }
        public string CountSheetStatus { get; set; }

        public int Id { get; set; }
        public int CTCaseId { get; set; }
        public long ContainerId { get; set; }
        public long ContainerTypeId { get; set; }
        public string CaseReference { get; set; }
        public long CaseCartId { get; set; }
        public string CaseCartName { get; set; }
        public long DueMinutes { get; set; }
        public DateTime? CaseTime { get; set; }
        public DateTime DueTimestamp { get; set; }
        public string DestinationName { get; set; }
        public DateTime? EndTimestamp { get; set; }
        public string CaseDoctor { get; set; }
        public string ContainerName { get; set; }
        public string ItemName { get; set; }
        public string Status { get; set; }
        public string ScheduleStatusCode { get; set; }
        public string ScheduleStatus { get; set; }
        public DateTime? StandardTime { get; set; }
        public DateTime? ExpediteTime { get; set; }
        public string ItemCode { get; set; }
        public string ItemType { get; set; }
        public string ItemTypeName { get; set; }
        public DateTime? StatTime { get; set; }
        public bool? MultipleBasis { get; set; }
        public string LocationName { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public DateTime? ScheduleCartChanged { get; set; }
        public bool IsModified { get; set; }
        public decimal HoldQuantity { get; set; }
        public long ItemQuantity { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsCartComplete { get; set; }
        public bool Shipped { get; set; }
        public string CaseStatus { get; set; }
        public string StandardTimeInHoursAndMinutes { get; set; }
        public string ExpediteTimeInHoursAndMinutes { get; set; }
        public string StatTimeInHoursAndMinutes { get; set; }
        public decimal DueInMinutes { get; set; }
        public string DueInHoursAndMinutes { get; set; }
        public long? ProcessingFacilityId { get; set; }
        public long? LocationFacilityId { get; set; }
        public long CaseItemNumber { get; set; }
        public long CaseContainerStatus { get; set; }
        public string CurrentLocationName { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public int GCCaseId { get; set; }
        public bool IsGCTrayActiveStatus { get; set; }

        public virtual CTCase CTCase { get; set; }
		public string SKU { get; set; }
        public string VendorName { get; set; }
		public bool IsLoaner { get; set; }
        public DateTime? Timestamp { get; set; }
        public int? ActualLapCount { get; set; }

        public bool WasInPickupStatus { get; set; }

        public string ProcurementReferenceId { get; set; }
        public string LifecycleCategory { get; set; }
        public string Location { get; set; }
    }
}
