﻿namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.SalesChannel
{
    public class SalesChannelChartData : GenericChartData
    {
        public int SalesChannelId { get; set; }
        public string SalesChannelName { get; set; }
    }
}