﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.ManualEtl.Factories;

namespace ProfitOptics.Modules.ManualEtl.Controllers
{
    [Area("ManualEtl")]
    [Authorize(Roles = "admin,superadmin")]
    public class ControlCenterController : BaseController
    {
        private readonly IManualEtlFactory _manualEtlFactory;

        public ControlCenterController(IManualEtlFactory manualEtlFactory)
        {
            _manualEtlFactory = manualEtlFactory;
        }

        public IActionResult Index()
        {
            var model = _manualEtlFactory.GetEtlSourceDropdownList();

            return View(model);
        }

        [HttpPost]
        public IActionResult GetEtlLogData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request)
        {
            try
            {
                var source = Request.Form["source"];

                var data = _manualEtlFactory.GetEtlLogDataModels(request, source);

                return new LargeJsonResult(data.ToDataTablesResponse(request));
            }
            catch (Exception ex)
            {
                return Json("Error");
            }
        }
    }
}