using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Announcements.Data.Domain
{
    public partial class AspNetUser
    {
        public AspNetUser()
        {
            POAnnouncementReads = new HashSet<POAnnouncementRead>();
            POAnnouncements = new HashSet<POAnnouncement>();
            AspNetUserRoles = new HashSet<AspNetUserRoles>();
        }

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public bool Approved { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }

        public string PhoneNumber { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        public bool LockoutEnabled { get; set; }

        public int AccessFailedCount { get; set; }

        public string UserName { get; set; }

        public bool IsGoogleAuthenticatorEnabled { get; set; }

        public string GoogleAuthenticatorSecretKey { get; set; }

        public bool IsEnabled { get; set; }

        public virtual ICollection<POAnnouncementRead> POAnnouncementReads { get; set; }

        public virtual ICollection<POAnnouncement> POAnnouncements { get; set; }

        public virtual ICollection<AspNetUserRoles> AspNetUserRoles { get; set; }
    }
}
