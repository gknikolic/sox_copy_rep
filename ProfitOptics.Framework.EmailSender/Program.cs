﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using ProfitOptics.Framework.Core.Email;
using System;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Core.System;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.EmailSender
{
    class Program
    {

        private static IServiceProvider _serviceProvider;
        
        static void Main(string[] args)
        {
            RegisterServices();

            var ioService = _serviceProvider.GetRequiredService<IIO>();
            var emailService = _serviceProvider.GetService<IEmailer>();
            var data = _serviceProvider.GetService<Entities>();

            var emailToSend = (from s in data.POEmails
                select s).Take(10).ToList();

            foreach (var email in emailToSend)
            {
                try
                {
                    var toList = email.To.Split(';').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                    var attachments = email.POEmailAttachments.Select(a => new EmailAttachment(a.FileName, a.SourceFileName, ioService)).ToList();

                    var result = emailService.SendEmail(toList, email.Subject, email.Body, email.IsHtml, attachments);

                    if (result)
                    {
                        email.SentTime = DateTime.UtcNow;
                    }

                    data.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error while sending email: {e}");
                }
            }

            DisposeServices();
        }

        private static void RegisterServices()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var configuration = builder.Build();

            var services = new ServiceCollection();
            var common = ConfigureSettingsConfig<Common>(services, configuration);
            ConfigureSettingsConfig<Email>(services, configuration);

            services.AddScoped<IIO, IO>();
            services.AddDbContext<Entities>(options => options.UseSqlServer(common.DefaultConnection));
            services.AddScoped<IEmailer, Emailer>();
            _serviceProvider = services.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }

        public static TConfig ConfigureSettingsConfig<TConfig>(IServiceCollection services, IConfiguration configuration) where TConfig : class, new()
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            //create instance of config
            var config = new TConfig();
            var classType = typeof(TConfig);
            //bind it to the appropriate section of configuration
            configuration.Bind(classType.Name, config);

            //and register it as a service
            services.AddSingleton(config);

            return config;
        }
    }
}