﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class AccountingsMainVM
    {
        public string Title { get; set; }
        public string QBExternalUrl { get; set; }
        public bool QBAuthorized { get; set; }
        public int? SelectedCustomer { get; set; }
        public string SelectedCustomerName { get; set; }
        public ProfitOptics.Modules.Sox.Enums.QBBillingType QBBillingType { get; set; }
        public List<SelectListItem> Customers { get; set; }
        public List<GCInvoice> GCInvoices { get; set; }
    }
}
