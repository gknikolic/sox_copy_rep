﻿using ProfitOptics.Framework.Web.Framework.Identity;
using System;
using System.Linq;
using System.Security.Claims;

namespace ProfitOptics.Framework.Web.Framework.Helpers.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetFullName(this ClaimsPrincipal claimsPrincipal)
        {
            var firstName = claimsPrincipal.Claims.FirstOrDefault(x => x.Type == ClaimTypes.GivenName)?.Value;
            var lastName = claimsPrincipal.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Surname)?.Value;

            return $"{firstName ?? string.Empty} {lastName ?? string.Empty}".Trim();
        }

        public static DateTime GetRegistrationDateUtc(this ClaimsPrincipal claimsPrincipal)
        {
            var registrationDateUtcString = claimsPrincipal.Claims.FirstOrDefault(x => x.Type == ApplicationClaimTypes.RegistrationDateUtc)?.Value;
            if (DateTime.TryParse(registrationDateUtcString, out DateTime registrationDateUtc))
            {
                return registrationDateUtc;
            }
            return DateTime.MinValue;
        }

        public static bool IsImpersonating(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims.Any(x => x.Type == ApplicationClaimTypes.OriginalUserId);
        }

        public static int GetUserId(this ClaimsPrincipal claimsPrincipal)
        {
            if (int.TryParse(claimsPrincipal.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value, out int userId))
            {
                return userId;
            }
            return int.MinValue;
        }
    }
}
