﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters;
using System.Text;

namespace ProfitOptics.Modules.Reporting.Extensions
{
    public class Logger
    {
        public enum LogType
        {
            Info,
            Error
        };

        private static readonly string LogPath = @AppDomain.CurrentDomain.BaseDirectory + @"\";
        private static readonly string FilePath = $"{LogPath}Log_{DateTime.UtcNow:MM_dd_yyyy}.txt";
        private static readonly string Separator = new string('_', 64);

        /// <summary>
        /// Writes log to .txt file
        /// </summary>
        /// <param name="logType">Required Info or Error log </param>
        /// <param name="message">Optional note about log</param>
        /// <param name="executionTime">Optional Eg. You can log execution time of a method</param>
        /// <param name="objects">All objects that you want to write in the log</param>
        public static void WriteLog(LogType logType, string message = null, string executionTime = null, params object[] objects)
        {
            CreateLogFile();
            Write(PrepareLogData(logType, message, executionTime, objects));
        }

        private static string PrepareLogData(LogType logType, string message, string executionTime, params object[] objects)
        {
            var builder = new StringBuilder();
            SetHeader(logType, message, executionTime, builder);
            foreach (var obj in objects)
            {
                builder.AppendLine(ParseObject(obj));
            }
            builder.AppendLine(Separator);

            return builder.ToString();
        }

        private static void SetHeader(LogType logType, string message, string executionTime, StringBuilder builder)
        {
            builder.AppendLine(Separator);
            builder.AppendLine($"{logType.ToString()} logged at: {DateTime.UtcNow:yyyy.dd.M HH:mm:ss}");
            if (message != null)
            {
                builder.AppendLine($"Message: {message}");
            }
            if (message != executionTime)
            {
                builder.AppendLine($"Execution Time: {executionTime} ms");
            }
            builder.AppendLine(Separator);
        }

        private static void CreateLogFile()
        {
            using (var tw = new StreamWriter(FilePath, true))
            {
                if (!File.Exists(FilePath))
                {
                    File.Create(FilePath);
                    tw.Close();
                }
            }
        }

        private static void Write(string objectData)
        {
            using (var tw = new StreamWriter(FilePath, append: true))
            {
                tw.Write(objectData);
                tw.Close();
            }
        }

        private static string ParseObject(object target)
        {
            var json = JsonConvert.SerializeObject(target,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    Formatting = Formatting.Indented,
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    TypeNameHandling = TypeNameHandling.All,
                    TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                    CheckAdditionalContent = true
                });

            JToken jt = JToken.Parse(json);
            return jt.ToString();
        }
    }

}
