﻿using System;

namespace ProfitOptics.Framework.Web.Models.LayoutModels
{
    public class MainLayoutViewModel
    {
        public string FullName { get; set; }

        public DateTime? StartTime { get; set; }

        public MainLayoutViewModel()
        {

        }

    }
}
