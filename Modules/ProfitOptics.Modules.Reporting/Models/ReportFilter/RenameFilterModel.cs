﻿namespace ProfitOptics.Modules.Reporting.Models.ReportFilter
{
    public class RenameFilterModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
