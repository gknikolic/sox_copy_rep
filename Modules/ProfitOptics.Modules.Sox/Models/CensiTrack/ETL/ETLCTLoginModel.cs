﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTLoginModel
    {
        public int ClientId { get; set; }
        public long FacilityId { get; set; }
        public string UserId { get; set; }
        public string UserPassword { get; set; }
    }
}
