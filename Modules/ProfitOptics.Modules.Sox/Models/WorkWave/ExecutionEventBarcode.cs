﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class ExecutionEventBarcode : ExecutionEvent
	{
		[JsonProperty("data")]
		public EEDataPodBarcodes Data { get; set; }
	}


}
