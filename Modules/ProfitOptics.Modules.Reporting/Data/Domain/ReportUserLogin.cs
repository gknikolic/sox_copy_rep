using System;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportUserLogin
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public DateTime LoginDateTime { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
    }
}
