﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class CaseDetailsTraysModel
	{
        public List<TrayModel> Trays { get; set; }
		public List<TrayAssemblyPhotos> AssemblyPhotos { get; set; }
		public List<TrayHistorySterilizeBIResultPhotos> LatestBIResults { get; set; }
		public List<TrayHistoryDeliveryPhotos> LatestDeliveryPods { get; set; }
		public int? NumOfPhotos { get; set; }

	}
}
