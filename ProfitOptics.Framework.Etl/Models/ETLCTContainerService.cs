﻿using Newtonsoft.Json;

namespace ProfitOptics.Framework.Etl.Models
{
    public class ETLCTContainerService
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
