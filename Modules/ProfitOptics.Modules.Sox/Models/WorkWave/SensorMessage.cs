﻿using Newtonsoft.Json;
using System;

namespace ProfitOptics.Modules.Sox.Models
{
	public class SensorMessage
    {
        [JsonProperty("sensorID")]
        public long SensorId { get; set; }

        [JsonProperty("sensorName")]
        public string SensorName { get; set; }

        [JsonProperty("applicationID")]
        public long ApplicationId { get; set; }

        [JsonProperty("networkID")]
        public long NetworkId { get; set; }

        [JsonProperty("dataMessageGUID")]
        public Guid DataMessageGuid { get; set; }

        [JsonProperty("state")]
        public float State { get; set; }

        [JsonProperty("messageDate")]
        public DateTime MessageDate { get; set; }

        [JsonProperty("rawData")]
        public string RawData { get; set; }

        [JsonProperty("dataType")]
        public string DataType { get; set; }

        [JsonProperty("dataValue")]
        public string DataValue { get; set; }

        [JsonProperty("plotValues")]
        public string PlotValues { get; set; }

        [JsonProperty("plotLabels")]
        public string PlotLabels { get; set; }

        [JsonProperty("batteryLevel")]
        public float BatteryLevel { get; set; }

        [JsonProperty("signalStrength")]
        public float SignalStrength { get; set; }

        [JsonProperty("pendingChange")]
        public string PendingChange { get; set; }

        [JsonProperty("voltage")]
        public float Voltage { get; set; }
    }
}
