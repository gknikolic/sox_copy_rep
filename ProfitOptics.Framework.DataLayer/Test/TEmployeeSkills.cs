﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.Test
{
    public class TEmployeeSkills
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int SkillId { get; set; }

        public virtual TEmployee Employee { get; set; }
        public virtual TSkill Skill { get; set; }

    }
}
