﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTBILoadResultIndicatorResponse
    {
        [JsonProperty("loadId")]
        public long LoadId { get; set; }

        [JsonProperty("requiredIndicators")]
        public List<ETLCTBILoadResultIndicator> RequiredIndicators { get; set; }
    }
}
