﻿using ProfitOptics.Framework.Core.Data;

namespace ProfitOptics.Modules.Reporting.Models.DailyTrend
{
    public class DailyTrendExportToExcelModel : IDataRecord
    {
        [DataField("Sales Rep", "", 1)]
        public virtual string SalesRep { get; set; }

        [DataField("Date", "", 2)]
        public virtual string Date { get; set; }

        [DataField("Customer Name", "", 3)]
        public virtual string BillToName { get; set; }

        [DataField("Customer Number", "", 4)]
        public virtual string BillToNum { get; set; }

        [DataField("Customer Class", "", 5)]
        public virtual string CustomerClass { get; set; }

        [DataField("Vendor", "", 6)]
        public virtual string VendorName { get; set; }

        [DataField("Ship To Name", "", 7)]
        public virtual string ShipToName { get; set; }

        [DataField("Ship To Num", "", 8)]
        public virtual string ShipToNum { get; set; }

        [DataField("Item Category", "", 9)]
        public virtual string ItemCategory { get; set; }

        [DataField("Item Number", "", 10)]
        public virtual string ItemNum { get; set; }

        [DataField("Quantity", "#,##0", 11, true)]
        public virtual decimal Quantity { get; set; }

        [DataField("Revenue", "$#,##0.00", 12, true)]
        public virtual decimal ExtPrice { get; set; }

        [DataField("Shipping Street", "", 13)]
        public virtual string ShippingStreet { get; set; }

        [DataField("Shipping City", "", 14)]
        public virtual string ShippingCity { get; set; }

        [DataField("Shipping State", "", 15)]
        public virtual string ShippingStateProvince { get; set; }

        [DataField("SOP Type", "", 16)]
        public virtual string SOPType { get; set; }

        [DataField("SOP Number", "", 17)]
        public virtual string SOPNumber { get; set; }

        [DataField("Comment 1", "", 18)]
        public virtual string Comment1 { get; set; }

        [DataField("Comment 2", "", 19)]
        public virtual string Comment2 { get; set; }

        [DataField("Comment 3", "", 20)]
        public virtual string Comment3 { get; set; }

        [DataField("Comment 4", "", 21)]
        public virtual string Comment4 { get; set; }
    }
}
