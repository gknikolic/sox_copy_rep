﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.Collections.Generic;
using ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Enums;

namespace ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder.Clauses
{
    /// <summary>
    ///     Represents a WHERE clause on 1 database column, containing 1 or more comparisons on
    ///     that column, chained together by logic operators: eg (UserID=1 or UserID=2 or UserID>100)
    ///     This can be achieved by doing this:
    ///     WhereClause myWhereClause = new WhereClause("UserID", Comparison.Equals, 1);
    ///     myWhereClause.AddClause(LogicOperator.Or, Comparison.Equals, 2);
    ///     myWhereClause.AddClause(LogicOperator.Or, Comparison.GreaterThan, 100);
    /// </summary>
    public struct WhereClause
    {
        internal List<SubClause> SubClauses; // Array of SubClause
        private Comparison m_ComparisonOperator;
        private string m_FieldName;
        private object m_Value;

        public WhereClause(string field, Comparison firstCompareOperator, object firstCompareValue)
        {
            m_FieldName = field;
            m_ComparisonOperator = firstCompareOperator;
            m_Value = firstCompareValue;
            SubClauses = new List<SubClause>();
        }

        /// <summary>
        ///     Gets/sets the name of the database column this WHERE clause should operate on
        /// </summary>
        public string FieldName
        {
            get { return m_FieldName; }
            set { m_FieldName = value; }
        }

        /// <summary>
        ///     Gets/sets the comparison method
        /// </summary>
        public Comparison ComparisonOperator
        {
            get { return m_ComparisonOperator; }
            set { m_ComparisonOperator = value; }
        }

        /// <summary>
        ///     Gets/sets the value that was set for comparison
        /// </summary>
        public object Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }

        public void AddClause(LogicOperator logic, Comparison compareOperator, object compareValue)
        {
            var NewSubClause = new SubClause(logic, compareOperator, compareValue);
            SubClauses.Add(NewSubClause);
        }

        internal struct SubClause
        {
            public Comparison ComparisonOperator;
            public LogicOperator LogicOperator;
            public object Value;

            public SubClause(LogicOperator logic, Comparison compareOperator, object compareValue)
            {
                LogicOperator = logic;
                ComparisonOperator = compareOperator;
                Value = compareValue;
            }
        }
    }
}