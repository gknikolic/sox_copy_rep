﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class VehicleSettings
	{
		[JsonProperty("available")]
		public bool Available { get; set; }
		[JsonProperty("notes")]
		public string Notes { get; set; }
	}
}
