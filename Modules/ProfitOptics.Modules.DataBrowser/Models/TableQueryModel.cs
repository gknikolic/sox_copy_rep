﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.Collections.Generic;

namespace ProfitOptics.Modules.DataBrowser.Models
{
    public class TableQueryModel
    {
        public string DatabaseName { get; set; }
        public string TableName { get; set; }
        public List<ConditionModel> Conditions { get; set; }
        public IEnumerable<string> GroupByColumns { get; set; }
        public string SortColumnName { get; set; }
        public string SortDirection { get; set; }

        public int? DisplayStart { get; set; }
        public int? DisplayLength { get; set; }

        public bool UseWhereStatement { get; set; }
        public string WhereStatement { get; set; }

        public IEnumerable<string> Columns { get; set; }
    }
}