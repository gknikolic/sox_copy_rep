﻿namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum SelectionFilterType
    {
        FilterByProducts,
        FilterByLocations,
        FilterByClients,
        FilterBySKUs
    }
}