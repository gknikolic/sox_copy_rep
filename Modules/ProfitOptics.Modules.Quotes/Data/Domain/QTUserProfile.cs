using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTUserProfile")]
    public partial class QTUserProfile
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        public string Company { get; set; }

        public string SalesRepCode { get; set; }

        public string SalesRepName { get; set; }

        public string SalesRepRole { get; set; }

        public string SalesRepRoleDescription { get; set; }

        public int? SalesManagerUserId { get; set; }

        public string SalesManagerCode { get; set; }

        public string SalesManagerName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
