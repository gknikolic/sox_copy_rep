﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
	[Table("WWVehicle")]
	public partial class WWVehicle
	{
		public int Id { get; set; }
		public string WWVehicleId { get; set; }
		public string ExternalId { get; set; }
		public bool Available { get; set; }
		public string Notes { get; set; }
		public string DeviceId { get; set; }
		public int? WWGPSDeviceId { get; set; }
		public virtual ICollection<WWTrackingData> WWTrackingData { get; set; }
		public virtual WWGPSDevice WWGPSDevice { get; set; }

	}
}
