using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Announcements.Data.Domain
{
    [Table("POAnnouncementRead")]
    public partial class POAnnouncementRead
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        public int? AnnouncementId { get; set; }

        public DateTime? ReadDate { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        public virtual POAnnouncement POAnnouncement { get; set; }
    }
}
