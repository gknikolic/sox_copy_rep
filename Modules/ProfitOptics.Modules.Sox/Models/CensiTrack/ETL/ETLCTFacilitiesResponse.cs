﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTFacilitiesResponse
    {
        public List<ETLCTFacility> ScheduleFacilities { get; set; }
        public long? DefaultFacilityId { get; set; }
        public long? ProcessingFacilityId { get; set; }

    }
}
