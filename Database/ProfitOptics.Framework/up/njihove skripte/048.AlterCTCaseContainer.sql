USE [Sox]

BEGIN

	ALTER TABLE [Sox].[dbo].[CTCaseContainer] ADD SerialNumber NVARCHAR(50) NULL;
	ALTER TABLE [Sox].[dbo].[CTCaseContainer] ADD VendorName NVARCHAR(100) NULL;
	
END
