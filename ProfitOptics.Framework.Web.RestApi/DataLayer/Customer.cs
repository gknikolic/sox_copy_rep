﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.Web.RestApi.DataLayer
{
    public class Customer
    {
        public Customer()
        {
            this.CustomerProducts = new HashSet<CustomerProduct>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(150, ErrorMessage = "Length must be less then 150 characters")]
        public string Name { get; set; }

        [MaxLength(150, ErrorMessage = "Length must be less then 150 characters")]
        public string Address { get; set; }

        public int DistributorId { get; set; }

        public Distributor Distributor { get; set; }

        public ICollection<CustomerProduct> CustomerProducts { get; set; }
    }
}