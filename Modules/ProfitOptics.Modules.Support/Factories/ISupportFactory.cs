﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Support.Models.ViewModels;
using BacklogIssueModel = ProfitOptics.Modules.Support.Models.BacklogIssueModel;
using CreateIssueViewModel = ProfitOptics.Modules.Support.Models.ViewModels.CreateIssueViewModel;
using IssueDataModel = ProfitOptics.Modules.Support.Models.IssueDataModel;
using IssuesViewModel = ProfitOptics.Modules.Support.Models.ViewModels.IssuesViewModel;

namespace ProfitOptics.Modules.Support.Factories
{
    public interface ISupportFactory
    {
        IssuesViewModel PrepareIssuesViewModel(string suppMsg);

        DataTablesResponse<IssueDataModel> PrepareIssueDataModels(IDataTablesRequest request,
            List<string> statuses, List<string> components, string project, string assignee, string reporter,
            string userEmail);

        DataTablesResponse<BacklogIssueModel> PrepareBacklogIssueDataModels(IDataTablesRequest request,
            List<string> components, string project, string assignee, string reporter);

        /// <summary>
        /// Prepares the update issue model for the issue with the provided issue key.
        /// </summary>
        /// <param name="issueKey"></param>
        /// <returns></returns>
        UpdateIssueViewModel PrepareUpdateIssueModel(string issueKey);

        /// <summary>
        /// Prepares create issue model with the provided support message and referrer url.
        /// </summary>
        /// <param name="supportMessage"></param>
        /// <param name="referrerUrl"></param>
        /// <returns></returns>
        CreateIssueViewModel PrepareCreateIssueModel(string supportMessage, string referrerUrl);

        Select2PagedResult PrepareUsersPagedResult(string projectKey, string searchTerm, int page);

        List<SelectListItem> PrepareAssigneeSelectListModel(string projectKey);

        List<SelectListItem> PrepareComponentsList(string selectedProjectKey, bool addEmptyComponent = false);

        List<SelectListItem> PrepareIssueTypeList(string projectKey);

        List<SelectListItem> PreparePriorityList(string projectKey);

        List<SelectListItem> PrepareProjectsList();

        List<SelectListItem> PrepareIssueList(string projectKey);
    }
}