﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class QuoteHeaderModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Quote name is required.")]
        [MaxLength(50, ErrorMessage = "Quote name is limited to 50 characters.")]
        public string QuoteName { get; set; }

        [MaxLength(250, ErrorMessage = "Quote description is limited to 250 characters.")]
        public string QuoteDescription { get; set; }

        public DateTime? PresentedToCustomer { get; set; }
        public string Status { get; set; }
        public DateTime? ValidThroughDate { get; set; }
        public string HeaderComments { get; set; }
        public string FooterComments { get; set; }
        public string ContactEmail { get; set; }
        public string CustomerReference { get; set; }
        public bool NewBusiness { get; set; }
        public DateTime? EstimatedFirstOrderDate { get; set; }
        public decimal? Probability { get; set; }
        public decimal? ShippingCost { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? AverageShippingCost { get; set; }
        public int? OriginZipCode { get; set; }
        public string ShipToNum { get; set; }
        public int? CustomerShipToId { get; set; }
        public string ShipToZipOverride { get; set; }
        public bool IsArchived { get; set; }

        public decimal? FreightCapAmount { get; set; }
        public string FreightPaymentType { get; set; }
    }
}