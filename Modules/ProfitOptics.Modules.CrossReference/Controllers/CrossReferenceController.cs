﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.CrossReference.Factories;
using ProfitOptics.Modules.CrossReference.Models;
using ProfitOptics.Modules.CrossReference.Services;

namespace ProfitOptics.Modules.CrossReference.Controllers
{
    [Area("CrossReference")]
    [Authorize]
    public class CrossReferenceController : BaseController
    {
        private readonly ICrossReferenceService _crossReferenceService;
        private readonly ICrossReferenceFactory _crossReferenceFactory;
        private readonly IWebHostEnvironment _host;

        public CrossReferenceController(ICrossReferenceService crossReferenceService,
            IWebHostEnvironment host, ICrossReferenceFactory crossReferenceFactory)
        {
            _crossReferenceService = crossReferenceService;
            _crossReferenceFactory = crossReferenceFactory;
            _host = host;
        }

        public IActionResult Index()
        {
            CrossReferenceIndexModel model = _crossReferenceFactory.PrepareIndexModel();

            return View(model);
        }

        public IActionResult ValidCompetitors()
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            List<CompetitorModel> model = _crossReferenceFactory.GetCompetitorModels();

            return View(model);
        }

        public IActionResult ValidCompatibility()
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            List<CompatibilityOptionModel> model = _crossReferenceFactory.GetCompatibilityOptionModels();

            return View(model);
        }

        public IActionResult UserSubmittedCrossReferences()
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            CrossReferenceIndexModel model = _crossReferenceFactory.PrepareIndexModel();

            return View(model);
        }

        public IActionResult BulkImport()
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            return View();
        }

        public IActionResult ExportAllRecords()
        {
            if (!User.IsInRole("admin"))
            {
                return Forbid();
            }

            return View();
        }

        public IActionResult Admin()
        {
            if (!User.IsInRole("admin"))
            {
                return Forbid();
            }

            return View();
        }

        [HttpPost]
        public IActionResult RemoveCompetitor(int competitorId)
        {
            if (!User.IsInRole("admin") && !(User.IsInRole("crossreferencemanager")))
            {
                return Forbid();
            }

            _crossReferenceService.DeleteCompetitor(competitorId);

            List<CompetitorModel> competitorModel = _crossReferenceFactory.GetCompetitorModels();

            return PartialView("_CompetitorTable", competitorModel);
        }

        [HttpPost]
        public IActionResult UpdateCompetitor(int competitorId, string competitorName)
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            _crossReferenceService.UpdateCompetitor(competitorId, competitorName);

            List<CompetitorModel> competitorModel = _crossReferenceFactory.GetCompetitorModels();

            return PartialView("_CompetitorTable", competitorModel);
        }

        [HttpPost]
        public IActionResult CreateCompetitor(string competitorName)
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            _crossReferenceService.CreateCompetitor(competitorName);

            List<CompetitorModel> competitorModel = _crossReferenceFactory.GetCompetitorModels();

            return PartialView("_CompetitorTable", competitorModel);
        }

        [HttpPost]
        public IActionResult CreateCompatibilityOption(string compatibilityName)
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            _crossReferenceService.CreateCompatibilityOption(compatibilityName);

            List<CompatibilityOptionModel> compatibilityOptionsModel = _crossReferenceFactory.GetCompatibilityOptionModels();

            return PartialView("_CompatibilityOptionsTable", compatibilityOptionsModel);
        }

        [HttpPost]
        public IActionResult RemoveCompatibilityOption(int compatibilityId)
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            _crossReferenceService.DeleteCompatibilityOption(compatibilityId);

            List<CompatibilityOptionModel> compatibilityOptionModel = _crossReferenceFactory.GetCompatibilityOptionModels();

            return PartialView("_CompatibilityOptionsTable", compatibilityOptionModel);
        }

        [HttpPost]
        public IActionResult UpdateCompatibilityOption(int compatibilityOptionId, string compatibilityOptionName)
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            _crossReferenceService.UpdateCompatibilityOption(compatibilityOptionId, compatibilityOptionName);

            List<CompatibilityOptionModel> compatibilityOptionModel = _crossReferenceFactory.GetCompatibilityOptionModels();

            return PartialView("_CompatibilityOptionsTable", compatibilityOptionModel);
        }

        [HttpPost]
        public IActionResult GetCrossReferenceRecords([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request,
            int? competitorId, int? compatibilityId, string competitorItem, string searchTerm)
        {
            try
            {
                int userId = GetCurrentUserId();

                PagedList<CrossReferenceRecordModel> data = _crossReferenceFactory
                    .GetCrossReferenceRecordModels(request, competitorId, compatibilityId, competitorItem, userId);

                return new LargeJsonResult(data.ToDataTablesResponse(request));
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult GetCrossReferenceRecordsForApproval([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request, string searchTerm)
        {
            int userId = GetCurrentUserId();

            PagedList<CrossReferenceRecordModel> data = _crossReferenceFactory.GetCrossReferenceRecordModelsForApproval(request, userId);

            return new LargeJsonResult(data.ToDataTablesResponse(request));
        }

        [HttpPost]
        public IActionResult RemoveCrossReferenceRecordForApproval(int id)
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            try
            {
                _crossReferenceService.RemoveCrossReferenceRecordForApproval(id);

                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult ApproveCrossReferenceRecord(int id)
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            try
            {
                _crossReferenceService.ApproveCrossReferenceRecord(id);

                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult EditCrossReferenceRecordForApproval(CrossReferenceRecordModel crossReferenceRecord)
        {
            if (!User.IsInRole("admin") && !User.IsInRole("crossreferencemanager"))
            {
                return Forbid();
            }

            try
            {
                _crossReferenceService.EditCrossReferenceRecordForApproval(crossReferenceRecord);

                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult AddCrossReferenceRecord(CrossReferenceRecordModel crossReferenceRecord)
        {
            try
            {
                int userId = GetCurrentUserId();

                _crossReferenceService.AddCrossReferenceRecord(crossReferenceRecord, userId);

                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        public IActionResult ItemLookupAutocomplete(string term)
        {
            return Json(new
            {
                items = _crossReferenceFactory.GetItemAutocompleteData(term)
            });
        }

        public IActionResult CheckCompetitorMedcoItemCombination(int id, int competitorId, string competitorItemNumber, int medcoItemNumberId)
        {
            try
            {
                bool isValid = id <= 0 ? 
                    _crossReferenceService.CheckCompetitorMedcoItemCombinationOnAdd(competitorId, competitorItemNumber, medcoItemNumberId)
                    : _crossReferenceService.CheckCompetitorMedcoItemCombinationOnEdit(id, competitorId, competitorItemNumber, medcoItemNumberId);

                return Json(new { success = true, message = "Success", isValid }); ;
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        public IActionResult ThumbsUpClick(int crossReferenceId)
        {
            try
            {
                int userId = GetCurrentUserId();
                
                _crossReferenceService.ThumbsUpClick(crossReferenceId,userId);
                
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        public IActionResult ThumbsDownClick(int crossReferenceId)
        {
            try
            {
                int userId = GetCurrentUserId();
                
                _crossReferenceService.ThumbsDownClick(crossReferenceId, userId);
                
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult ExportItemList(List<int?> crossReferenceIds)
        {
            try
            {
                string handle = Guid.NewGuid().ToString();
                
                string fileName = _host.ContentRootPath + $"\\wwwroot\\{handle}.xlsx";
                
                string downloadFileName = $"CrossReferenceItems-{DateTime.UtcNow:MM-dd-yyyy_Hmmss}.xlsx";
                
                List<CrossReferenceExportModel> data = _crossReferenceFactory.GetCrossReferenceExportModelList(crossReferenceIds);

                if (data.Any())
                {
                    ExcelFile.ExportToExcel(fileName, "Sheet1", data, true, true);
                }

                return Json(new { success = true, filename = downloadFileName, handle = handle });
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);

                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpGet]
        public virtual FileContentResult DownloadFile(string handle, string filename, bool inline = false)
        {
            string path = _host.ContentRootPath + $"\\wwwroot\\{handle}{Path.GetExtension(filename)}";
            
            byte[] bytes = System.IO.File.ReadAllBytes(path);
            
            System.IO.File.Delete(path);

            var fileExtensionContentTypeProvider = new FileExtensionContentTypeProvider();

            fileExtensionContentTypeProvider.TryGetContentType(filename, out string contentType);
            
            return new FileContentResult(bytes, contentType) { FileDownloadName = filename };
        }
    }
}