﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Factories;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;
using ProfitOptics.Modules.Reporting.Models.TrendVariance;

namespace ProfitOptics.Modules.Reporting.Services.TrendVariance
{
    public class TrendVarianceDataService : ITrendVarianceDataService
    {
        private readonly ISalesHistoryService _salesHistoryService;
        private readonly IReportFilterModelFactory _reportFilterModelFactory;
        private readonly Entities _context;

        public TrendVarianceDataService(ISalesHistoryService salesHistoryService, IReportFilterModelFactory reportFilterModelFactory, Entities context)
        {
            _salesHistoryService = salesHistoryService;
            _reportFilterModelFactory = reportFilterModelFactory;
            _context = context;
        }

        /// <inheritdoc />
        public IEnumerable<TrendVarianceSalesForMonthModel> GetSalesHistoryForMonths(int userId, DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
            {
                throw new ArgumentOutOfRangeException(nameof(startDate));
            }

            if (userId <= 0)
            {
                return new List<TrendVarianceSalesForMonthModel>();
            }

            ReportFilterModel filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            IQueryable<ReportSalesHistory> filteredSalesHistories = _salesHistoryService.GetFilteredSalesHistoriesForDateRange(filter, startDate, endDate);

            var resultFilteredSalesHistories = filteredSalesHistories.ToList();

            List<IGrouping<int?, ReportSalesHistory>> groupedSalesHistory = (from salesHistory in resultFilteredSalesHistories
                group salesHistory by salesHistory.InvoiceYearMonth
                into grouped select  grouped).ToList();

            List<TrendVarianceSalesForMonthModel> monthlyData = (from salesHistory in groupedSalesHistory
                select new TrendVarianceSalesForMonthModel
                {
                    Year = (int)Math.Floor((salesHistory.Key ?? 0) / 100.0m),
                    Month = (salesHistory.Key ?? 0) % 100,
                    ExtPrice = salesHistory.Sum(z => z.ExtPrice),
                    ExtCost = salesHistory.Sum(z => z.ExtCost),
                }).ToList();

            return monthlyData;
        }

        /// <inheritdoc />
        public PagedList<TrendVarianceVarianceByTypeModel> GetVariancesGroupedByViewByType(int userId,
            IDataTablesRequest request, ViewByType viewByType, TrendVariancePeriodsModel periods)
        {
            if (userId <= 0)
            {
                return new PagedList<TrendVarianceVarianceByTypeModel>();
            }

            ReportFilterModel filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            IQueryable<ReportSalesHistory> filteredSalesHistories =
                _salesHistoryService.GetFilteredSalesHistoriesForDateRange(filter, periods.PreviousPeriodStartDate, periods.CurrentPeriodEndDate);

            int totalCount;
            IEnumerable<TrendVarianceVarianceByTypeModel> items;
            switch (viewByType)
            {
                case ViewByType.BillTo:
                    (items, totalCount) = GetVarianceGroupedByBillTo(request, filteredSalesHistories, periods);
                    break;
                case ViewByType.CustomerClass:
                    (items, totalCount) = GetVarianceGroupedByCustomerClass(request, filteredSalesHistories, periods);
                    break;
                case ViewByType.ItemCategory:
                    (items, totalCount) = GetVarianceGroupedByItemCategory(request, filteredSalesHistories, periods);
                    break;
                case ViewByType.Vendor:
                    (items, totalCount) = GetVarianceGroupedByVendor(request, filteredSalesHistories, periods);
                    break;
                case ViewByType.ShipTo:
                    (items, totalCount) = GetVarianceGroupedByShipTo(request, filteredSalesHistories, periods);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(viewByType), viewByType, null);
            }
            
            return new PagedList<TrendVarianceVarianceByTypeModel>(items, totalCount);
        }

        /// <inheritdoc />
        public PagedList<TrendVarianceVarianceByTypeModel> GetVariancesGroupedByItemCategoryForBillToId(int userId, int billToId,
            IDataTablesRequest request, TrendVariancePeriodsModel periods)
        {
            return GetSalesHistoriesGroupedByItemCategoryForCustomerId(userId, request, periods, billToId: billToId);
        }

        /// <inheritdoc />
        public PagedList<TrendVarianceVarianceByTypeModel> GetVariancesGroupedByItemCategoryForShipToId(int userId, int shipToId, IDataTablesRequest request,
            TrendVariancePeriodsModel periods)
        {
            return GetSalesHistoriesGroupedByItemCategoryForCustomerId(userId, request, periods, shipToId: shipToId);
        }

        private PagedList<TrendVarianceVarianceByTypeModel> GetSalesHistoriesGroupedByItemCategoryForCustomerId(int userId,
            IDataTablesRequest request, TrendVariancePeriodsModel periods, int billToId = 0, int shipToId = 0)
        {
            if (userId <= 0)
            {
                return new PagedList<TrendVarianceVarianceByTypeModel>();
            }

            ReportFilterModel filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            IQueryable<ReportSalesHistory> filteredSalesHistories =
                _salesHistoryService.GetFilteredSalesHistoriesForDateRange(filter, periods.PreviousPeriodStartDate,
                    periods.CurrentPeriodEndDate);

            var itemCategoryQuery = from salesHistory in filteredSalesHistories
                from itemCategory in _context.ReportItemCategory.Where(b => b.Id == salesHistory.ItemCategoryId)
                where (billToId == 0 || salesHistory.BillToId == billToId) && (shipToId == 0 || salesHistory.ShipToId == shipToId) 
                group salesHistory by new {itemCategory.Id, itemCategory.Code, itemCategory.Name};

            IQueryable<TrendVarianceVarianceByTypeModel> itemsQuery = itemCategoryQuery.Select(g => new TrendVarianceVarianceByTypeModel
            {
                Id = g.Key.Id,
                Name = g.Key.Name,
                Code = g.Key.Code,
                PriorSales = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                PriorCost = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                PriorQty = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.Qty
                        : decimal.Zero),
                CurrentSales = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                CurrentCost = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                CurrentQty = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.Qty
                        : decimal.Zero)
            }).ApplySort(request);

            return new PagedList<TrendVarianceVarianceByTypeModel>(itemsQuery, request.Start, request.Length);
        }

        /// <inheritdoc />
        public ReportBillTo GetBillToById(int billToId)
        {
            return billToId <= 0 ? null : _context.ReportBillTo.SingleOrDefault(bt => bt.Id == billToId);
        }

        /// <inheritdoc />
        public ReportShipTo GetShipToById(int shipToId)
        {
            return shipToId <= 0 ? null : _context.ReportShipTo.SingleOrDefault(bt => bt.Id == shipToId);
        }

        /// <inheritdoc />
        public ReportItemCategory GetItemCategoryById(int itemCategoryId)
        {
            return itemCategoryId <= 0 ? null : _context.ReportItemCategory.SingleOrDefault(bt => bt.Id == itemCategoryId);
        }

        /// <inheritdoc />
        public PagedList<TrendVarianceVarianceForItemModel> GetVariancesGroupedByItemForItemCategoryIdAndBillToId(
            int userId, int billToId, int itemCategoryId, IDataTablesRequest request, TrendVariancePeriodsModel periods)
        {
            return GetVariancesGroupedByItemForItemCategoryAndCustomerId(userId, itemCategoryId, request,
                periods, billToId);
        }

        /// <inheritdoc />
        public PagedList<TrendVarianceVarianceForItemModel> GetVariancesGroupedByItemForItemCategoryIdAndShipToId(int userId, int shipToId, int itemCategoryId,
            IDataTablesRequest request, TrendVariancePeriodsModel periods)
        {
            return GetVariancesGroupedByItemForItemCategoryAndCustomerId(userId, itemCategoryId, request,
                periods, shipToId: shipToId);
        }

        /// <inheritdoc />
        public List<ReportSalesHistory> GetSalesHistoriesForDateRange(int userId, DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
            {
                throw new ArgumentOutOfRangeException(nameof(startDate));
            }

            if (userId <= 0)
            {
                return new List<ReportSalesHistory>();
            }

            ReportFilterModel filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            IQueryable<ReportSalesHistory> filteredSalesHistories = _salesHistoryService
                .GetFilteredSalesHistoriesForDateRange(filter, startDate, endDate);

            return filteredSalesHistories.ToList();
        }

        private PagedList<TrendVarianceVarianceForItemModel> GetVariancesGroupedByItemForItemCategoryAndCustomerId(
            int userId, int itemCategoryId, IDataTablesRequest request, TrendVariancePeriodsModel periods,
            int billToId = 0, int shipToId = 0)
        {
            if (userId <= 0)
            {
                return new PagedList<TrendVarianceVarianceForItemModel>();
            }

            ReportFilterModel filter = _reportFilterModelFactory.CreateDefaultReportFilterModelForUserId(userId);

            IQueryable<ReportSalesHistory> filteredSalesHistories =
                _salesHistoryService.GetFilteredSalesHistoriesForDateRange(filter, periods.PreviousPeriodStartDate,
                    periods.CurrentPeriodEndDate);

            var itemQuery = from salesHistory in filteredSalesHistories
                            from item in _context.ReportItem.Where(b => b.Id == salesHistory.ItemId)
                            where salesHistory.ItemCategoryId == itemCategoryId && 
                                  (billToId == 0 || salesHistory.BillToId == billToId) &&
                                  (shipToId == 0 || salesHistory.ShipToId == shipToId)
                            group salesHistory by new { item.Id, item.ItemNum, item.ItemDescription };

            IQueryable<TrendVarianceVarianceForItemModel> variancesQuery = itemQuery.Select(g => new TrendVarianceVarianceForItemModel
            {
                Id = g.Key.Id,
                ItemNum = g.Key.ItemNum,
                ItemDescription = g.Key.ItemDescription,
                PriorSales = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                PriorCost = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                PriorQty = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.Qty
                        : decimal.Zero),
                CurrentSales = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                CurrentCost = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                CurrentQty = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.Qty
                        : decimal.Zero)
            }).ApplySort(request);

            return new PagedList<TrendVarianceVarianceForItemModel>(variancesQuery, request.Start, request.Length);
        } 

        private (IEnumerable<TrendVarianceVarianceByTypeModel> items, int totalCount)  GetVarianceGroupedByBillTo(
            IDataTablesRequest request, IQueryable<ReportSalesHistory> filteredSalesHistories, TrendVariancePeriodsModel periods)
        {
            var salesHistory = filteredSalesHistories.AsNoTracking().ToLookup(s => s.BillToId);
            var billTos = _context.ReportBillTo.AsNoTracking().ToList();

            List<TrendVarianceVarianceByTypeModel> variancesList = new List<TrendVarianceVarianceByTypeModel>();
            foreach (var billTo in billTos)
            {
                variancesList.Add(new TrendVarianceVarianceByTypeModel
                {
                    Id = billTo.Id,
                    Name = billTo.FullBillToName,
                    Code = billTo.BillToNum,
                    PriorSales = salesHistory[billTo.Id].Sum(p =>
                        p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                        p.InvoiceDate < periods.PreviousPeriodEndDate
                            ? p.ExtPrice
                            : decimal.Zero),
                    PriorCost = salesHistory[billTo.Id].Sum(p =>
                        p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                        p.InvoiceDate < periods.PreviousPeriodEndDate
                            ? p.ExtCost
                            : decimal.Zero),
                    PriorQty = salesHistory[billTo.Id].Sum(p =>
                        p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                        p.InvoiceDate < periods.PreviousPeriodEndDate
                            ? p.Qty
                            : decimal.Zero),
                    CurrentSales = salesHistory[billTo.Id].Sum(p =>
                        p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                            ? p.ExtPrice
                            : decimal.Zero),
                    CurrentCost = salesHistory[billTo.Id].Sum(p =>
                        p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                            ? p.ExtCost
                            : decimal.Zero),
                    CurrentQty = salesHistory[billTo.Id].Sum(p =>
                        p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                            ? p.Qty
                            : decimal.Zero)
                });
            }

            int totalCount = variancesList.Count();
            
            List<TrendVarianceVarianceByTypeModel> items = variancesList.AsQueryable()
                .ApplyPagination(request)
                .ToList();

            return (items, totalCount);
        }

        private (IEnumerable<TrendVarianceVarianceByTypeModel> items, int totalCount) GetVarianceGroupedByCustomerClass(
            IDataTablesRequest request, IQueryable<ReportSalesHistory> filteredSalesHistories, TrendVariancePeriodsModel periods)
        {
            var customerClassQuery = from salesHistory in filteredSalesHistories
                from billTo in _context.ReportCustomerClasses.Where(b => b.Id == salesHistory.CustomerClassId)
                group salesHistory by new { billTo.Id, billTo.Code, billTo.Name };

            IQueryable<TrendVarianceVarianceByTypeModel> variancesQuery = customerClassQuery.Select(g => new TrendVarianceVarianceByTypeModel
            {
                Id = g.Key.Id,
                Name = g.Key.Name,
                Code = g.Key.Code,
                PriorSales = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                PriorCost = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                PriorQty = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.Qty
                        : decimal.Zero),
                CurrentSales = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                CurrentCost = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                CurrentQty = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.Qty
                        : decimal.Zero)
            }).ApplySort(request);

            int totalCount = customerClassQuery.Count();

            List<TrendVarianceVarianceByTypeModel> items = variancesQuery
                .ApplyPagination(request)
                .ToList();

            return (items, totalCount);
        }

        private (IEnumerable<TrendVarianceVarianceByTypeModel> items, int totalCount) GetVarianceGroupedByItemCategory(
            IDataTablesRequest request, IQueryable<ReportSalesHistory> filteredSalesHistories, TrendVariancePeriodsModel periods)
        {
            var itemCategoryQuery = from salesHistory in filteredSalesHistories
                from itemCategory in _context.ReportItemCategory.Where(b => b.Id == salesHistory.ItemCategoryId)
                group salesHistory by new { itemCategory.Id, itemCategory.Code, itemCategory.Name };

            IQueryable<TrendVarianceVarianceByTypeModel> variancesQuery = itemCategoryQuery.Select(g => new TrendVarianceVarianceByTypeModel
            {
                Id = g.Key.Id,
                Name = g.Key.Name,
                Code = g.Key.Code,
                PriorSales = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                PriorCost = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                PriorQty = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.Qty
                        : decimal.Zero),
                CurrentSales = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                CurrentCost = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                CurrentQty = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.Qty
                        : decimal.Zero)
            }).ApplySort(request);

            int totalCount = itemCategoryQuery.Count();

            List<TrendVarianceVarianceByTypeModel> items = variancesQuery
                .ApplyPagination(request)
                .ToList();

            return (items, totalCount);
        }

        private (IEnumerable<TrendVarianceVarianceByTypeModel> items, int totalCount) GetVarianceGroupedByVendor(
            IDataTablesRequest request, IQueryable<ReportSalesHistory> filteredSalesHistories, TrendVariancePeriodsModel periods)
        {
            var vendorQuery = from salesHistory in filteredSalesHistories
                from vendor in _context.ReportVendor.Where(b => b.Id == salesHistory.VendorId)
                group salesHistory by new { vendor.Id, vendor.Name };

            IQueryable<TrendVarianceVarianceByTypeModel> variancesQuery = vendorQuery.Select(g => new TrendVarianceVarianceByTypeModel
            {
                Id = g.Key.Id,
                Name = g.Key.Name,
                Code = g.Key.Name,
                PriorSales = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                PriorCost = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                PriorQty = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.Qty
                        : decimal.Zero),
                CurrentSales = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                CurrentCost = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                CurrentQty = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.Qty
                        : decimal.Zero)
            }).ApplySort(request);

            int totalCount = vendorQuery.Count();

            List<TrendVarianceVarianceByTypeModel> items = variancesQuery
                .ApplyPagination(request)
                .ToList();

            return (items, totalCount);
        }

        private (IEnumerable<TrendVarianceVarianceByTypeModel> items, int totalCount) GetVarianceGroupedByShipTo(
            IDataTablesRequest request, IQueryable<ReportSalesHistory> filteredSalesHistories, TrendVariancePeriodsModel periods)
        {
            var shipToQuery = from salesHistory in filteredSalesHistories
                from shipTo in _context.ReportShipTo.Where(b => b.Id == salesHistory.ShipToId)
                group salesHistory by new { shipTo.Id, Code = shipTo.ShipToNum, Name = shipTo.FullShipToName };

            IQueryable<TrendVarianceVarianceByTypeModel> variancesQuery = shipToQuery.Select(g => new TrendVarianceVarianceByTypeModel
            {
                Id = g.Key.Id,
                Name = g.Key.Name,
                Code = g.Key.Code,
                PriorSales = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                PriorCost = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                PriorQty = g.Sum(p =>
                    p.InvoiceDate >= periods.PreviousPeriodStartDate &&
                    p.InvoiceDate < periods.PreviousPeriodEndDate
                        ? p.Qty
                        : decimal.Zero),
                CurrentSales = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtPrice
                        : decimal.Zero),
                CurrentCost = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.ExtCost
                        : decimal.Zero),
                CurrentQty = g.Sum(p =>
                    p.InvoiceDate >= periods.CurrentPeriodStartDate && p.InvoiceDate < periods.CurrentPeriodEndDate
                        ? p.Qty
                        : decimal.Zero)
            }).ApplySort(request);

            int totalCount = shipToQuery.Count();

            List<TrendVarianceVarianceByTypeModel> items = variancesQuery
                .ApplyPagination(request)
                .ToList();

            return (items, totalCount);
        }
    }
}