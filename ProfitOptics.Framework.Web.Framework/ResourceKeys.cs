﻿namespace ProfitOptics.Framework.Web.Framework
{
    public static class ResourceKeys
    {
        public static string MessagesGenericError => "Messages.Generic.Error";
        public static string MessagesAccountRegistrationCaptchaError => "Messages.UserRegistration.CaptchaError";
        public static string MessagesAccountManagementProfileUpdated => "Messages.AccountManagement.ProfileUpdated";
        public static string MessagesAccountManagementChangePasswordSuccess => "Messages.AccountManagement.ChangePasswordSuccess";
        public static string MessagesAccountManagementSetPasswordSuccess => "Messages.AccountManagement.SetPasswordSuccess";
        public static string MessagesAccountManagementSetTwoFactorSuccess => "Messages.AccountManagement.SetTwoFactorSuccess";
        public static string MessagesAccountManagementAddPhoneSuccess => "Messages.AccountManagement.AddPhoneSuccess";
        public static string MessagesAccountManagementRemoveLoginSuccess => "Messages.AccountManagement.RemoveLoginSuccess";        
        public static string MessagesAccountManagementRemovePhoneSuccess => "Messages.AccountManagement.RemovePhoneSuccess";
        public static string MessagesAccountManagementPhoneVerificationSendingFailed => "Messages.AccountManagement.PhoneVerificationSendingFailed";
        public static string MessagesAccountManagementPhoneVerificationConfirmationFailed => "Messages.AccountManagement.PhoneVerificationConfirmationFailed";
        public static string MessagesAccountManagementGoogleAuthenticatorInvalidCode => "Messages.AccountManagement.GoogleAuthenticatorInvalidCode";
        public static string MessagesSignInResultLockout => "Messages.SignInResult.Lockout";
        public static string MessagesSignInResultNotApproved => "Messages.SignInResult.NotApproved";
        public static string MessagesSignInResultFailed => "Messages.SignInResult.Failed";
        public static string MessagesSignInResultDisabled => "Messages.SignInResult.Disabled";
        public static string Messages2FASendingCodeFailed => "Messages.2FA.SendingCodeFailed";
        public static string EmailSubjectsConfirmEmail => "Email.Subjects.ConfirmEmail";
        public static string EmailSubjectsNewUserNotification => "Email.Subjects.NewUserNotification";
        public static string EmailSubjectsResetPassword => "Email.Subjects.ResetPassword";
        public static string TemplatesSMSPhoneVerification => "Templates.SMS.PhoneVerification";
    }
}
