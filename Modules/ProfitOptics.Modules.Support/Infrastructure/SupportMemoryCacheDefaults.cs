﻿namespace ProfitOptics.Modules.Support.Infrastructure
{
    public static class SupportMemoryCacheDefaults
    {
        public static int CacheTimeInMinutes => 30;
    }
}