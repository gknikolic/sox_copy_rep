﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Dynamic;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Extensions;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;
using ProfitOptics.Framework.Web.Framework.Models.Common;

namespace ProfitOptics.Modules.Reporting.Services.MarginBridge
{
    public class MarginBridgeService : IMarginBridgeService
    {
        #region Fields

        private readonly Entities _context;
        private readonly ISalesHistoryService _salesHistoryService;
        private readonly IMarginBridgeSessionService _sessionService;

        #endregion Fields

        #region Ctor

        public MarginBridgeService(Entities context, IMarginBridgeSessionService sessionService, ISalesHistoryService salesHistoryService)
        {
            _context = context;
            _sessionService = sessionService;
            _salesHistoryService = salesHistoryService;
        }

        #endregion Ctor

        #region Methods

        public List<ReportSalesHistory> GetSalesHistory()
        {
            return _context.ReportSalesHistory.ToList();
        }

        public List<ReportSalesHistoryModel> GetSalesHistory(MarginBridgeFilterModel filter, MarginBridgePeriod period, PeriodType periodType)
        {
            var periodSales = _salesHistoryService.GetFilteredSalesHistoriesForDateRange(filter.ReportFitlerModel, period.StartDate, period.EndDate);

            var result = (from ps in periodSales
                        join shipTo in _context.ReportShipTo on ps.ShipToId equals shipTo.Id
                        join billTo in _context.ReportBillTo on ps.BillToId equals billTo.Id
                        join salesRep in _context.ReportHierarchySalesRep on ps.SalesRepId equals salesRep.Id
                        join region in _context.ReportHierarchyRegion on ps.RegionId equals region.Id
                        join zone in _context.ReportHierarchyZone on ps.ZoneId equals zone.Id
                        join item in _context.ReportItem on ps.ItemId equals item.Id
                        join itemCategory in _context.ReportItemCategory on ps.ItemCategoryId equals itemCategory.Id
                        select new ReportSalesHistoryModel
                        {
                            Id = ps.Id,
                            BillToId = ps.BillToId,
                            BuyGroupId = ps.BuyGroupId,
                            Comment1 = ps.Comment1,
                            Comment2 = ps.Comment2,
                            Comment3 = ps.Comment3,
                            Comment4 = ps.Comment4,
                            CustomerClassId = ps.CustomerClassId,
                            EndDate = ps.EndDate,
                            ExtCost = ps.ExtCost,
                            ExtPrice = ps.ExtPrice,
                            InvoiceDate = ps.InvoiceDate,
                            InvoiceYearMonth = ps.InvoiceYearMonth,
                            ItemCategoryId = ps.ItemCategoryId,
                            ItemId = ps.ItemId,
                            Qty = ps.Qty,
                            RebateRate = ps.RebateRate,
                            RegionId = ps.RegionId,
                            SOPNumber = ps.SOPNumber,
                            SOPType = ps.SOPType,
                            SalesRepId = ps.SalesRepId,
                            ShipToId = ps.ShipToId,
                            StartDate = ps.StartDate,
                            UnitCost = ps.UnitCost,
                            UnitPrice = ps.UnitPrice,
                            VendorId = ps.VendorId,
                            ZoneId = ps.ZoneId,
                            ShipTo = shipTo.ShipToName,
                            BillTo = billTo.BillToName,
                            Territory = salesRep.Name,
                            Region = region.Name,
                            Zone = zone.Name,
                            Item = item.ItemNum,
                            ItemCategory = itemCategory.Name
                        }).AsNoTracking().ToList();

            
            if (result.Count == 0)
            {
                throw new Exception("There is no data for the " + periodType + " period.");
            }
            

            return result;
        }

        private List<ReportSalesHistoryStatisticsModel> GetSalesHistoryStatistics(MarginBridgeFilterModel filter, MarginBridgePeriod period, PeriodType periodType)
        {
            var periodSales = _salesHistoryService.GetFilteredSalesHistoriesForDateRange(filter.ReportFitlerModel, period.StartDate, period.EndDate);

            var result = (from ps in periodSales
                          join shipTo in _context.ReportShipTo on ps.ShipToId equals shipTo.Id
                          join billTo in _context.ReportBillTo on ps.BillToId equals billTo.Id
                          join salesRep in _context.ReportHierarchySalesRep on ps.SalesRepId equals salesRep.Id
                          join region in _context.ReportHierarchyRegion on ps.RegionId equals region.Id
                          join zone in _context.ReportHierarchyZone on ps.ZoneId equals zone.Id
                          join item in _context.ReportItem on ps.ItemId equals item.Id
                          join itemCategory in _context.ReportItemCategory on ps.ItemCategoryId equals itemCategory.Id
                          select new ReportSalesHistoryStatisticsModel
                          {
                              Id = ps.Id,
                              BuyGroupId = ps.BuyGroupId,
                              CustomerClassId = ps.CustomerClassId,
                              ExtCost = ps.ExtCost,
                              ExtPrice = ps.ExtPrice,
                              InvoiceDate = ps.InvoiceDate,
                              ItemCategoryId = ps.ItemCategoryId,
                              ItemId = ps.ItemId,
                              Qty = ps.Qty,
                              RebateRate = ps.RebateRate,
                              RegionId = ps.RegionId,
                              SalesRepId = ps.SalesRepId,
                              ShipToId = ps.ShipToId,
                              UnitCost = ps.UnitCost,
                              UnitPrice = ps.UnitPrice,
                              VendorId = ps.VendorId,
                              ZoneId = ps.ZoneId,
                              ShipTo = shipTo.ShipToName,
                              BillTo = billTo.BillToName,
                              Territory = salesRep.Name,
                              Region = region.Name,
                              Zone = zone.Name,
                              Item = item.ItemNum,
                              ItemCategory = itemCategory.Name
                          }).AsNoTracking().ToList();


            if (result.Count == 0)
            {
                throw new Exception("There is no data for the " + periodType.Description() + " period.");
            }


            return result;
        }

        public InputData CreateProcessInputData(MarginBridgeFilterModel filter, MarginBridgePeriod period, PeriodType periodType, ProductLevelType maxProductLevel, LocationLevelType maxLocationLevel)
        {
            var salesHistoryQuery = GetSalesHistoryStatistics(filter, period, periodType);
            var groupingLevelsQuery = from sale in salesHistoryQuery
                                      select new
                                      {
                                          Item = ((int)maxProductLevel >= 1 ? sale.Item : string.Empty),
                                          ItemCategory = ((int)maxProductLevel >= 2 ? sale.ItemCategory : string.Empty),
                                          ShipTo = ((int)maxLocationLevel >= 1 ? sale.ShipTo : string.Empty),
                                          BillTo = ((int)maxLocationLevel >= 2 ? sale.BillTo : string.Empty),
                                          Territory = ((int)maxLocationLevel >= 3 ? sale.Territory : string.Empty),
                                          Region = ((int)maxLocationLevel >= 4 ? sale.Region : string.Empty),
                                          Zone = ((int)maxLocationLevel >= 5 ? sale.Zone : string.Empty),
                                          ExtCost = sale.ExtCost,
                                          ExtPrice = sale.ExtPrice,
                                          Qty = sale.Qty,
                                          RebateRate = sale.RebateRate
                                      };

            // note query is written using LINQ methods instead of LINQ query
            // since EF Core 2.2 runs it on the client-side if otherwise
            var query = groupingLevelsQuery.GroupBy(x => new
            {
                Item = x.Item,
                ItemCategory = x.ItemCategory,
                ShipTo = x.ShipTo,
                BillTo = x.BillTo,
                Territory = x.Territory,
                Region = x.Region,
                Zone = x.Zone,
            }).Select(g => new InputDataPointExt()
            {
                Item = g.Key.Item,
                ItemCategory = g.Key.ItemCategory,
                ShipTo = g.Key.ShipTo,
                BillTo = g.Key.BillTo,
                Territory = g.Key.Territory,
                Region = g.Key.Region,
                Zone = g.Key.Zone,

                Cost = g.Sum(p => p.Qty) == 0 ? 0 : g.Sum(p => p.ExtCost * p.Qty) / g.Sum(p => p.Qty),
                CostExtra = 0,
                Price = g.Sum(p => p.Qty) == 0 ? 0 : g.Sum(p => p.ExtPrice * p.Qty) / g.Sum(p => p.Qty),
                RebateDollars = g.Sum(p => p.Qty) == 0 ? 0 : g.Sum(p => p.RebateRate * p.Qty) / g.Sum(p => p.Qty),
                Qty = g.Sum(p => p.Qty),
                InvoiceAmount = g.Sum(i => i.ExtPrice),
                RebatedCost = g.Sum(i => i.ExtPrice * i.RebateRate)
            });

            return new InputData()
            {
                DataPoints = query.ToList().ToDictionary(p => p.ProductKey + "\t" + p.LocationKey, p => p)
            };
        }

        public List<FullOutput> GetFullOutput(IEnumerable<InputDataPointExt> firstPeriodSales,
            IEnumerable<InputDataPointExt> secondPeriodSales, List<OutputDataPoint> dataPoints, ProductLevelType maxProductLevel,
            LocationLevelType maxLocationLevel, string productDbColumnName, string locationDbColumnName)
        {
            var salesInBothPeriods = firstPeriodSales.Concat(secondPeriodSales);

            var productList = (from sale in salesInBothPeriods
                               select new
                               {
                                   Item = (int)maxProductLevel >= 1 ? sale.Item : string.Empty,
                                   ItemCategory = (int)maxProductLevel >= 2 ? sale.ItemCategory : string.Empty,

                                   ProductKey = ((int)maxProductLevel >= 1 ? sale.Item : string.Empty) + ((int)maxProductLevel >= 2 ? sale.ItemCategory : string.Empty)
                               }).ToList().Distinct();

            var locationList = (from sale in salesInBothPeriods
                                select new
                                {
                                    ShipTo = (int)maxLocationLevel >= 1 ? sale.ShipTo : string.Empty,
                                    BillTo = (int)maxLocationLevel >= 2 ? sale.BillTo : string.Empty,
                                    Territory = (int)maxLocationLevel >= 3 ? sale.Territory : string.Empty,
                                    Region = (int)maxLocationLevel >= 4 ? sale.Region : string.Empty,
                                    Zone = (int)maxLocationLevel >= 5 ? sale.Zone : string.Empty,
                                    LocationKey = ((int)maxLocationLevel >= 1 ? sale.ShipTo: string.Empty) + ((int)maxLocationLevel >= 2 ? sale.BillTo : string.Empty) +
                                                  ((int)maxLocationLevel >= 3 ? sale.Territory : string.Empty) + ((int)maxLocationLevel >= 4 ? sale.Region : string.Empty) + ((int)maxLocationLevel >= 5 ? sale.Zone : string.Empty)
                                }).ToList().Distinct();

            var fullOutput = (from dataPoint in dataPoints
                              join location in locationList on dataPoint.LocationKey equals location.LocationKey
                              join product in productList on dataPoint.ProductKey equals product.ProductKey
                              join firstSales in firstPeriodSales on new { dataPoint.ProductKey, dataPoint.LocationKey } equals new { firstSales.ProductKey, firstSales.LocationKey } into firstPeriod
                              from firstPeriodSale in firstPeriod.DefaultIfEmpty()
                              join secondSales in secondPeriodSales on new { dataPoint.ProductKey, dataPoint.LocationKey } equals new { secondSales.ProductKey, secondSales.LocationKey } into secondPeriod
                              from secondPeriodSale in secondPeriod.DefaultIfEmpty()

                              select new FullOutput
                              {
                                  ShipTo = location.ShipTo,
                                  BillTo = location.BillTo,
                                  Territory = location.Territory,
                                  Region = location.Region,
                                  Zone = location.Zone,
                                  Product = product.Item,
                                  ProductCategory = product.ItemCategory,
                                  Price = dataPoint.Price,
                                  Cost = dataPoint.Cost,
                                  Volume = dataPoint.Volume,
                                  ProductMix = dataPoint.ProductMix,
                                  RebateRate = dataPoint.RebateRate,
                                  RebateProductMix = dataPoint.RebateProductMix,
                                  RebateVolume = dataPoint.RebateVolume,
                                  StartInvoiceAmount = firstPeriodSale?.InvoiceAmount ?? 0,
                                  StartRebatedCost = firstPeriodSale?.RebatedCost ?? 0,
                                  EndInvoiceAmount = secondPeriodSale?.InvoiceAmount ?? 0,
                                  EndRebatedCost = secondPeriodSale?.RebatedCost ?? 0,
                              }).DefaultIfEmpty().ToList();

            return fullOutput;
        }

        public List<ReportSalesHistoryModel> GetRawSalesHistory(MarginBridgeFilterModel filter, MarginBridgePeriod period)
        {
            return GetSalesHistory(filter, period, PeriodType.None);
        }

        public void CalculateResults(MargineBridgeResponseDataModel model, bool groupByMaxLevel = true)
        {
            //UpdateFilterTrees(model);
            PerformCalculations(groupByMaxLevel, model);
        }

        public DataBridgeRawData GetDataBridgeRawData(MarginBridgeFilterModel filter, MarginBridgePeriod firstPeriod, MarginBridgePeriod secondsPeriod)
        {
            var firstPeriodSales = GetSalesHistory(filter, firstPeriod, PeriodType.First);
            var secondPeriodSales = GetSalesHistory(filter, secondsPeriod, PeriodType.Second);

            var dataBridgeRawData = GetDataBridgeRawData(firstPeriodSales, secondPeriodSales);
            return dataBridgeRawData;
        }

        public List<DataBridgeItem> GetDataBridgeData(MarginBridgeFilterModel filter, MarginBridgePeriod firstPeriod, MarginBridgePeriod secondsPeriod)
        {
            var firstPeriodSales = GetSalesHistory(filter, firstPeriod, PeriodType.First);
            var secondPeriodSales = GetSalesHistory(filter, secondsPeriod, PeriodType.Second);

            return GetDataBridgeList(firstPeriodSales, secondPeriodSales);
        }

        public string GetEnumDescription(Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        public PaginationResult_OLD<StatisticsResponse> GetStatisticsData(JQueryDataTablesParamModel aoData)
        {
            var data = _sessionService.Current.StatisticsDataModel.AllResults;

            _sessionService.Current.AddSortData(aoData.sColumnName, aoData.sSortColDir);
            _sessionService.Save(_sessionService.Current);

            if (_sessionService.Current.SortData != null)
            {
                data = _sessionService.Current.StatisticsDataModel.AllResults.AsQueryable()
                    .OrderBy(_sessionService.Current.SortData).ToList();
            }

            var resultData =
                data.Skip(aoData.iDisplayStart)
                    .Take(aoData.iDisplayLength)
                    .ToList();

            var result = new PaginationResult_OLD<StatisticsResponse>
            {
                Result = resultData,
                SummaryRow = _sessionService.Current.StatisticsDataModel.Totals,
                TotalCount = _sessionService.Current.StatisticsDataModel.NumberOfRows
            };

            return result;
        }

        #endregion Methods

        #region Helpers

        private void UpdateFilterTrees(MargineBridgeResponseDataModel model)
        {
            if (model.GroupedBy == GroupByType.Location)
            {
                if (model.LocationLevel == LocationLevelType.AllLocations)
                {
                    model.LocationsHierarchy = model.LocationsHierarchyPermanent;
                }
                else
                {
                    var locationHierarchy = model.LocationsHierarchyPermanent.CloneAndUncheckAll().ToList();
                    model.LocationsHierarchy = locationHierarchy.SelectChecked();
                }
            }
            else
            {
                if (model.ProductLevel == ProductLevelType.AllProducts)
                {
                    model.ProductsHierarchy = model.ProductsHierarchyPermanent;
                }
                else
                {
                    var productHierarchy = model.ProductsHierarchyPermanent.CloneAndUncheckAll().ToList();
                    model.ProductsHierarchy = productHierarchy.SelectChecked();
                }
            }
        }
        
        private void PerformCalculations(bool groupByMaxLevel, MargineBridgeResponseDataModel model)
        {
            var groupByLevel = model.GroupedBy == GroupByType.Location
                ? (int)model.LocationLevel + 1
                : (int)model.ProductLevel + 1;
            var groupByProperty = model.GroupedBy == GroupByType.Location
                ? ((LocationLevelType)groupByLevel).ToString()
                : ((ProductLevelType)groupByLevel).ToString();

            model.CanDrillThrough = model.GroupedBy == GroupByType.Location
                ? (int)model.MaxLocationLevel > (int)model.LocationLevel + 1
                : (int)model.MaxProductLevel > (int)model.ProductLevel + 1;

            if (groupByMaxLevel)
            {
                groupByLevel = model.GroupedBy == GroupByType.Location
                    ? (int)model.MaxLocationLevel
                    : (int)model.MaxProductLevel;
                groupByProperty = model.GroupedBy == GroupByType.Location
                    ? model.MaxLocationLevel.ToString()
                    : model.MaxProductLevel.ToString();
                model.CanDrillThrough = false;
            }

            var responseData = (from data in model.FullOutputData
                                group data by data.GetType().GetProperty(groupByProperty).GetValue(data).ToString()
                into grouped
                                select new StatisticsResponse
                                {
                                    Period1Sales = grouped.Sum(i => i.StartInvoiceAmount),
                                    Period1RebateCost = grouped.Sum(i => i.StartRebatedCost),
                                    Period2Sales = grouped.Sum(i => i.EndInvoiceAmount),
                                    Period2RebateCost = grouped.Sum(i => i.EndRebatedCost),
                                    StartMargin = CalculateMargin(model.MarginBridgeType, grouped.Sum(i => i.StartInvoiceAmount), grouped.Sum(i => i.StartRebatedCost)),
                                    Description = GetNodeDescriptions(model, model.ProductsHierarchyPermanent, grouped.Key),
                                    Caption = grouped.Key,
                                    Price = grouped.Sum(i => i.Price),
                                    Cost = grouped.Sum(i => i.Cost),
                                    Volume = grouped.Sum(i => i.Volume),
                                    ProductMix = grouped.Sum(i => i.ProductMix),
                                    RebateProdMix = grouped.Sum(i => i.RebateProductMix),
                                    RebateRate = grouped.Sum(i => i.RebateRate),
                                    RebateVol = grouped.Sum(i => i.RebateVolume),
                                    TotalImpact = grouped.Sum(i => i.Price) + grouped.Sum(i => i.Cost) + grouped.Sum(i => i.Volume) + grouped.Sum(i => i.ProductMix)
                                                + grouped.Sum(i => i.RebateProductMix) + grouped.Sum(i => i.RebateRate) + grouped.Sum(i => i.RebateVolume),
                                    EndMargin = CalculateMargin(model.MarginBridgeType, grouped.Sum(i => i.EndInvoiceAmount), grouped.Sum(i => i.EndRebatedCost))
                                }).ToList();

            if (model.TotalsPermanent == null)
            {
                var startInvoiceAmount = model.FullOutputData.Sum(i => i.StartInvoiceAmount);
                var startRebatedAmount = model.FullOutputData.Sum(i => i.StartRebatedCost);

                var endInvoiceAmount = model.FullOutputData.Sum(i => i.EndInvoiceAmount);
                var endRebatedAmount = model.FullOutputData.Sum(i => i.EndRebatedCost);

                model.TotalsPermanent = new StatisticsResponse
                {
                    Caption = "Total",
                    Period1Sales = startInvoiceAmount,
                    Period1RebateCost = startRebatedAmount,
                    Period2Sales = endInvoiceAmount,
                    Period2RebateCost = endRebatedAmount,
                    Price = responseData.Sum(i => i.Price),
                    Cost = responseData.Sum(i => i.Cost),
                    Volume = responseData.Sum(i => i.Volume),
                    ProductMix = responseData.Sum(i => i.ProductMix),
                    RebateProdMix = responseData.Sum(i => i.RebateProdMix),
                    RebateRate = responseData.Sum(i => i.RebateRate),
                    RebateVol = responseData.Sum(i => i.RebateVol),
                    TotalImpact = responseData.Sum(i => i.Price) + responseData.Sum(i => i.Cost) + responseData.Sum(i => i.Volume) + responseData.Sum(i => i.ProductMix) + responseData.Sum(i => i.RebateProdMix) +
                        responseData.Sum(i => i.RebateRate) + responseData.Sum(i => i.RebateVol),
                    StartMargin = CalculateMargin(model.MarginBridgeType, startInvoiceAmount, startRebatedAmount),
                    EndMargin = CalculateMargin(model.MarginBridgeType, endInvoiceAmount, endRebatedAmount)
                };
            }

            var responseDataTemp = (from data in model.FullOutputData

                                    select data).ToList();

            model.Totals = new StatisticsResponse
            {
                Period1Sales = responseDataTemp.Sum(i => i.StartInvoiceAmount),
                Period1RebateCost = responseDataTemp.Sum(i => i.StartRebatedCost),
                Period2Sales = responseDataTemp.Sum(i => i.EndInvoiceAmount),
                Period2RebateCost = responseDataTemp.Sum(i => i.EndRebatedCost),
                Caption = "Total",
                Price = responseData.Sum(i => i.Price),
                Cost = responseData.Sum(i => i.Cost),
                Volume = responseData.Sum(i => i.Volume),
                ProductMix = responseData.Sum(i => i.ProductMix),
                RebateProdMix = responseData.Sum(i => i.RebateProdMix),
                RebateRate = responseData.Sum(i => i.RebateRate),
                RebateVol = responseData.Sum(i => i.RebateVol),
                TotalImpact = responseData.Sum(i => i.Price) + responseData.Sum(i => i.Cost) + responseData.Sum(i => i.Volume) + responseData.Sum(i => i.ProductMix) + responseData.Sum(i => i.RebateProdMix) +
                        responseData.Sum(i => i.RebateRate) + responseData.Sum(i => i.RebateVol),
                StartMargin = CalculateMargin(model.MarginBridgeType, responseDataTemp.Sum(i => i.StartInvoiceAmount), responseDataTemp.Sum(i => i.StartRebatedCost)),
                EndMargin = CalculateMargin(model.MarginBridgeType, responseDataTemp.Sum(i => i.EndInvoiceAmount), responseDataTemp.Sum(i => i.EndRebatedCost))
            };

            model.NumberOfRows = responseData.Count;
            model.AllResults = responseData;
            model.HeaderCaption = model.GroupedBy == GroupByType.Location
                ? GetEnumDescription((LocationLevelType)groupByLevel)
                : GetEnumDescription((ProductLevelType)groupByLevel);
        }

        private decimal CalculateMargin(MarginBridgeType marginBridgeType, decimal price, decimal cost)
        {
            switch (marginBridgeType)
            {
                case MarginBridgeType.GPPercentage:
                    return (price != 0) ? (price - cost) / price : 0;

                case MarginBridgeType.GPDollarAmount:
                    return price - cost;

                default:
                    throw new ArgumentOutOfRangeException($"Invalid margin bridge type: {marginBridgeType}");
            }
        }

        private List<string> GetBreadcrumbItems(List<BreadcrumbItem> breadcrumbItems)
        {
            var result = new List<string> { null, null, null, null, null, null, null };
            var counter = 0;
            foreach (var breadcrumbItem in breadcrumbItems.OrderBy(item => item.Level))
            {
                result[counter] = breadcrumbItem.Value;
                counter++;
                if (breadcrumbItem.IsSelected)
                {
                    break;
                }
            }

            return result;
        }

        private string GetNodeDescriptions(MargineBridgeResponseDataModel model, List<ProductHierarchyFilterModel> products, string caption)
        {
            var description = caption;

            if (model.GroupedBy == GroupByType.Location)
            {
                return description;
            }

            if (products != null)
            {
                foreach (var product in products)
                {
                    if (product.Code == caption)
                    {
                        //if (product.FilterType == ProductLevelType.ProductGroup ||
                        //    product.FilterType == ProductLevelType.ProductSubgroup)

                        return product.Description;

                    }
                    else
                    {
                        description = GetNodeDescriptions(model, product.Items, caption);
                        if (description != caption)
                        {
                            return description;
                        }
                    }
                }
            }
            return description;
        }

        private DataBridgeRawData GetDataBridgeRawData(List<ReportSalesHistoryModel> firstPeriodSales, List<ReportSalesHistoryModel> secondPeriodSales)
        {
            var dataBridge = new List<DataBridgeItem>();
            var dataBridgeRawData = new DataBridgeRawData();

            var firstRow = GetDataBridgeItem("BI Data", firstPeriodSales, secondPeriodSales, true);
            dataBridge.Add(firstRow);

            ////if (!includeSDItems)
            ////{
            //    dataBridgeRawData.ShipDebitP1 = firstPeriodSales.Where(i => i.IsShipAndDebit).ToList();
            //    dataBridgeRawData.ShipDebitP2 = secondPeriodSales.Where(i => i.IsShipAndDebit).ToList();
            //    dataBridge.Add(GetDataBridgeItem("Ship/Debit", dataBridgeRawData.ShipDebitP1, dataBridgeRawData.ShipDebitP2, false, firstRow));
            //    firstPeriodSales.RemoveAll(i => i.IsShipAndDebit);
            //    secondPeriodSales.RemoveAll(i => i.IsShipAndDebit);
            ////}

            //dataBridgeRawData.PolitiqueP1 = firstPeriodSales.Where(i => i.IsPolitique).ToList();
            //dataBridgeRawData.PolitiqueP2 = secondPeriodSales.Where(i => i.IsPolitique).ToList();
            //dataBridge.Add(GetDataBridgeItem("Politique Parts (QP)", dataBridgeRawData.PolitiqueP1, dataBridgeRawData.PolitiqueP2, false, firstRow));
            //firstPeriodSales.RemoveAll(i => i.IsPolitique);
            //secondPeriodSales.RemoveAll(i => i.IsPolitique);

            //dataBridgeRawData.InvoicesNoSalesP1 = firstPeriodSales.Where(i => i.InvoiceAmount == 0 && i.QuantityShipped != 0 && i.InvoiceCost != 0).ToList();
            //dataBridgeRawData.InvoicesNoSalesP2 = secondPeriodSales.Where(i => i.InvoiceAmount == 0 && i.QuantityShipped != 0 && i.InvoiceCost != 0).ToList();
            //dataBridge.Add(GetDataBridgeItem("Invoices No Sales", dataBridgeRawData.InvoicesNoSalesP1, dataBridgeRawData.InvoicesNoSalesP2, false, firstRow));
            //firstPeriodSales.RemoveAll(i => i.InvoiceAmount == 0 && i.QuantityShipped != 0 && i.InvoiceCost != 0);
            //secondPeriodSales.RemoveAll(i => i.InvoiceAmount == 0 && i.QuantityShipped != 0 && i.InvoiceCost != 0);

            //dataBridgeRawData.InvoicesNoCostP1 = firstPeriodSales.Where(i => i.InvoiceAmount != 0 && i.QuantityShipped != 0 && i.InvoiceCost == 0).ToList();
            //dataBridgeRawData.InvoicesNoCostP2 = secondPeriodSales.Where(i => i.InvoiceAmount != 0 && i.QuantityShipped != 0 && i.InvoiceCost == 0).ToList();
            //dataBridge.Add(GetDataBridgeItem("Invoices No Cost", dataBridgeRawData.InvoicesNoCostP1, dataBridgeRawData.InvoicesNoCostP2, false, firstRow));
            //firstPeriodSales.RemoveAll(i => i.InvoiceAmount != 0 && i.QuantityShipped != 0 && i.InvoiceCost == 0);
            //secondPeriodSales.RemoveAll(i => i.InvoiceAmount != 0 && i.QuantityShipped != 0 && i.InvoiceCost == 0);

            dataBridge.Add(GetDataBridgeItem("Margin Bridge", firstPeriodSales, secondPeriodSales, true));

            dataBridgeRawData.DataBridge = dataBridge;
            return dataBridgeRawData;
        }

        private List<DataBridgeItem> GetDataBridgeList(List<ReportSalesHistoryModel> firstPeriodSales,
            List<ReportSalesHistoryModel> secondPeriodSales)
        {
            var result = new List<DataBridgeItem>();

            var firstRow = GetDataBridgeItem("BI Data", firstPeriodSales, secondPeriodSales, true);
            result.Add(firstRow);

            ////if (!includeSDItems)
            ////{
            //    result.Add(GetDataBridgeItem("Ship/Debit",
            //        firstPeriodSales.Where(i => i.IsShipAndDebit).ToList(),
            //        secondPeriodSales.Where(i => i.IsShipAndDebit).ToList(), false, firstRow));

            //    firstPeriodSales.RemoveAll(i => i.IsShipAndDebit);
            //    secondPeriodSales.RemoveAll(i => i.IsShipAndDebit);
            ////}

            //result.Add(GetDataBridgeItem("Politique Parts (QP)",
            //    firstPeriodSales.Where(i => i.IsPolitique).ToList(),
            //    secondPeriodSales.Where(i => i.IsPolitique).ToList(), false, firstRow));

            //firstPeriodSales.RemoveAll(i => i.IsPolitique);
            //secondPeriodSales.RemoveAll(i => i.IsPolitique);

            //result.Add(GetDataBridgeItem("Invoices No Sales",
            //    firstPeriodSales.Where(i => i.InvoiceAmount == 0 && i.QuantityShipped != 0 && i.InvoiceCost != 0).ToList(),
            //    secondPeriodSales.Where(i => i.InvoiceAmount == 0 && i.QuantityShipped != 0 && i.InvoiceCost != 0).ToList(), false, firstRow));

            //firstPeriodSales.RemoveAll(i => i.InvoiceAmount == 0 && i.QuantityShipped != 0 && i.InvoiceCost != 0);
            //secondPeriodSales.RemoveAll(i => i.InvoiceAmount == 0 && i.QuantityShipped != 0 && i.InvoiceCost != 0);

            //result.Add(GetDataBridgeItem("Invoices No Cost",
            //    firstPeriodSales.Where(i => i.InvoiceAmount != 0 && i.QuantityShipped != 0 && i.InvoiceCost == 0).ToList(),
            //    secondPeriodSales.Where(i => i.InvoiceAmount != 0 && i.QuantityShipped != 0 && i.InvoiceCost == 0).ToList(), false, firstRow));

            //firstPeriodSales.RemoveAll(i => i.InvoiceAmount != 0 && i.QuantityShipped != 0 && i.InvoiceCost == 0);
            //secondPeriodSales.RemoveAll(i => i.InvoiceAmount != 0 && i.QuantityShipped != 0 && i.InvoiceCost == 0);

            result.Add(GetDataBridgeItem("Margin Bridge", firstPeriodSales, secondPeriodSales, true));

            return result;
        }

        private DataBridgeItem GetDataBridgeItem(string caption, List<ReportSalesHistoryModel> firstPeriodSales,
            List<ReportSalesHistoryModel> secondPeriodSales, bool isFirstRow, DataBridgeItem firstRow = null)
        {
            return new DataBridgeItem((MarginBridgeSessionService)_sessionService)
            {
                FirstRow = firstRow,
                IsFirstRow = isFirstRow,
                Caption = caption,
                Period1Sales = firstPeriodSales.Sum(i => i.ExtPrice),
                Period1RebatedCost = 0,
                Period2Sales = secondPeriodSales.Sum(i => i.ExtPrice),
                Period2RebatedCost = 0
            };
        }

        #endregion Helpers
    }
}