﻿using System.Collections.Generic;
using ProfitOptics.Modules.Support.Data.Domain;

namespace ProfitOptics.Modules.Support.Services
{
    public interface ISupportService
    {
        #region Issue Watcher Manager

        /// <summary>
        /// Adds watch to the user with provided email for a given issueId in the project with the specified identifier.
        /// </summary>
        /// <param Name="email">User email</param>
        /// <param Name="projectId">Project identifier</param>
        /// <param Name="issueId">Issue identifier</param>
        void AddWatch(string email, string projectId, string issueId);

        /// <summary>
        /// Removes watch from the user with provided email for a given issueId in the project with the specified identifier.
        /// </summary>
        /// <param Name="email">User email</param>
        /// <param Name="projectId">Project identifier</param>
        /// <param Name="issueId">Issue identifier</param>
        void RemoveWatch(string email, string projectId, string issueId);

        /// <summary>
        /// Gets names of all watched issues for the project with the given project identifier for a user with the given email.
        /// </summary>
        /// <param Name="email">User email</param>
        /// <param Name="projectId">Project identifier</param>
        /// <returns></returns>
        List<string> GetWatchedIssues(string email, string projectId);

        /// <summary>
        /// Gets emails of all users who are watching the issue with the provided issue identifier in the project with the provided project identifier.
        /// </summary>
        /// <param Name="projectId">Project identifier</param>
        /// <param Name="issueId">Issue identifier</param>
        /// <returns></returns>
        List<string> GetWatchersForIssue(string projectId, string issueId);

        /// <summary>
        /// Checks if the provided email is within the JiraSupportGlobalNotifications.
        /// </summary>
        /// <param Name="email">User email</param>
        /// <returns></returns>
        bool IsGlobalWatcher(string email);

        /// <summary>
        /// Gets all global watchers.
        /// </summary>
        /// <returns></returns>
        List<JiraSupportGlobalNotification> GetGlobalWatchers();

        /// <summary>
        /// Add the given email in "Global Watchers"
        /// </summary>
        /// <param Name="email"></param>
        void AddGlobalWatcher(string email);

        /// <summary>
        /// Remove the provided email from "Global Watchers"
        /// </summary>
        /// <param Name="email"></param>
        void RemoveGlobalWatcher(string email);

        /// <summary>
        /// Removes all global watchers.
        /// </summary>
        void RemoveAllGlobalWatchers();

        /// <summary>
        /// Add or Updates each global watcher from the provided list.
        /// </summary>
        /// <param Name="notificationEmails"></param>
        void AddOrUpdateGlobalWatchers(List<JiraSupportGlobalNotification> notificationEmails);

        #endregion Issue Watcher Manager
    }
}