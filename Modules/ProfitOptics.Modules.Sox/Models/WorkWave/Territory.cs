﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class Territory
	{
		[JsonProperty("id")]
		public Guid Id { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("timeZoneCode")]
		public string TimeZoneCode { get; set; }
		[JsonProperty("languageCode")]
		public string LanguageCode { get; set; }
	}

	public class TerritoriesResponse
	{
		[JsonProperty("territories")]
		public Dictionary<Guid, Territory> Territories { get; set; }
	}
}
