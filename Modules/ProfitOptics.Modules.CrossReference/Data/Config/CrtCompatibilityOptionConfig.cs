﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.CrossReference.Data.Domain;

namespace ProfitOptics.Modules.CrossReference.Data.Config
{
    public class CrtCompatibilityOptionConfig : IEntityTypeConfiguration<CrtCompatibilityOption>
    {
        public void Configure(EntityTypeBuilder<CrtCompatibilityOption> builder)
        {
            builder.ToTable("CrtCompatibilityOption");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.CompatibilityOptionName).HasMaxLength(250).IsRequired();
        }
    }
}
