﻿using Newtonsoft.Json;
using System;

namespace ProfitOptics.Modules.Sox.Models
{
	public class Order
	{
		[JsonProperty("id")]
		public Guid Id { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("eligibility")]
		public Eligibility Eligibility { get; set; }

		[JsonProperty("forceVehicleId")]
		public Guid? ForceVehicleId { get; set; }

		[JsonProperty("priority")]
		public int Priority { get; set; }

		[JsonProperty("loads")]
		public Loads Loads { get; set; }
		[JsonProperty("pickup")]
		public OrderStep Pickup { get; set; }
		[JsonProperty("delivery")]
		public OrderStep Delivery { get; set; }
		[JsonProperty("isService")]
		public bool IsService { get; set; }
	}
}
