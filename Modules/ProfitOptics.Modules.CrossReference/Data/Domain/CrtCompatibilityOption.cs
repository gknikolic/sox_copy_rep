using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.CrossReference.Data.Domain
{
    public partial class CrtCompatibilityOption
    {
        public CrtCompatibilityOption()
        {
            CrtCrossReferenceRecords = new HashSet<CrtCrossReferenceRecord>();
        }

        public int Id { get; set; }

        public string CompatibilityOptionName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual ICollection<CrtCrossReferenceRecord> CrtCrossReferenceRecords { get; set; }
    }
}
