﻿namespace ProfitOptics.Framework.Web.Framework.ServiceResults
{
    public class UserRegistrationResult : AuthenticationServiceResult
    {
        /// <summary>
        /// If user has to confirm the email, this is the link for email confirmation
        /// </summary>
        public string ConfirmEmailUrl { get; set; }

        public static UserRegistrationResult EmailNotConfirmed(string url)
        {
            var result = EmailNotConfirmed<UserRegistrationResult>();
            result.ConfirmEmailUrl = url;
            return result;
        }

        public override bool Succeeded => Status == AuthenticationServiceResultStatusCode.Success ||
                                          Status == AuthenticationServiceResultStatusCode.EmailNotConfirmed;

        public new static UserRegistrationResult Success() => new UserRegistrationResult
        {
            Status = AuthenticationServiceResultStatusCode.Success
        };


    }
}
