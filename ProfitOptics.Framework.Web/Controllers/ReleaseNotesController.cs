﻿using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Models.ReleaseNotes;
using System.Linq;
using Microsoft.AspNetCore.Hosting;

namespace ProfitOptics.Framework.Web.Controllers
{
    public class ReleaseNotesController : Controller
    {
        private readonly IWebHostEnvironment _host;

        public ReleaseNotesController(IWebHostEnvironment hostingEnvironment)
        {
            _host = hostingEnvironment;
        }

        // GET: ReleaseNotes
        public IActionResult Index()
        {
            var model = new ReleaseNotesViewModel();
            model.List = ReleaseNotesModel.LoadReleaseNotes(HttpContext, _host.ContentRootPath).ToList().OrderByDescending(p => p.Date).ToList();
            return View(model);
        }
    }
}
