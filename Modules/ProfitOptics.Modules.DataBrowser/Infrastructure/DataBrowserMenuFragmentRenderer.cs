using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Helpers.HtmlHelpers;
using ProfitOptics.Framework.Web.Framework.Infrastructure;

namespace ProfitOptics.Modules.DataBrowser.Infrastructure
{
    public class DataBrowserMenuFragmentRenderer : IMenuFragmentRenderer
    {
        /// <inheritdoc />
        public bool Validate(Common settings, ClaimsPrincipal user)
        {
            return string.IsNullOrWhiteSpace(settings.DisabledModules) ||
                   !settings.DisabledModules.Contains("DataBrowser") ||
                   user.IsInRole("admin") ||
                   user.IsInRole("superadmin");
        }

        /// <inheritdoc />
        public Task<IHtmlContent> RenderMenuFragmentAsync(IHtmlHelper htmlHelper)
        {
            return htmlHelper.PartialAsync(PartialViewHelper.BuildModulePartialViewPath(Assembly.GetExecutingAssembly(), "/Views/Shared/_Menu.Fragment.cshtml"));
        }

        /// <inheritdoc />
        public int Order => 9;
    }
}
