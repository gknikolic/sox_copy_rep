﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTLocationsResponse
    {
        public List<ETLCTLocation> Locations { get; set; }
    }
}
