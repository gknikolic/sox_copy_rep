﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayHistoryModel
	{
		public List<TrayHistoryCase> Cases { get; set; }
		public List<TrayHistoryTrayLifecycle> TrayPreviousHistory { get; set; }
        public List<TrayPart> TrayParts { get; set; }
		public TrayAssemblyParts TrayAssemblyParts{ get; set; }
		public TrayAssemblyPhotos TrayAssemblyPhotos { get; set; }
		public List<TrayImage> TrayImages { get; set; }
		public string AdditionalNote { get; set; }
		public int CTContainerTypeActualId { get; set; }
		public TrayHistorySterilizeBIResultPhotos LatestBIResults { get; set; }
		public TrayHistoryDeliveryPhotos LatestDeliveryPods { get; set; }
		public List<TrayHistoryTrayLifecycle> VendorHistoryLifecycles { get; set; }
		public int? NumOfPhotos { get; set; }
	}

}
