﻿using Newtonsoft.Json;
using ProfitOptics.Modules.Sox.Models.WorkWave;
using System;

namespace ProfitOptics.Modules.Sox.Models
{
	public class ExecutionEvent
	{
	
		public ExecutionEventType EventType { get; set; }

		[JsonProperty("type")]
		public string Type { get { return EventType.ToString(); } }

		[JsonProperty("orderId")]
		public string OrderId { get; set; }

		public OrderStepType StepType { get; set; }
		[JsonProperty("orderStepType")]
		public string OrderStepType { get { return StepType.ToString(); } }
		[JsonProperty("date")]
		public string Date { get; set; }

		[JsonProperty("vehicleId")]
		public string VehicleId { get; set; }
	}


}
