﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTContainerType
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("serviceId")]
        public long ServiceId { get; set; }

        [JsonProperty("serviceName")]
        public string ServiceName { get; set; }

        [JsonProperty("homeLocationName")]
        public string HomeLocationName { get; set; }

        [JsonProperty("processingLocationName")]
        public string ProcessingLocationName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sterilizationMethodName")]
        public string SterilizationMethodName { get; set; }

        [JsonProperty("modified")]
        public string Modified { get; set; }

        [JsonProperty("isDiscarded")]
        public bool IsDiscarded { get; set; }

        [JsonProperty("automaticActuals")]
        public bool AutomaticActuals { get; set; }


        [JsonProperty("facilityId")]
        public long? FacilityId { get; set; }

        [JsonProperty("sterilizationMethodId")]
        public long? SterilizationMethodId { get; set; }

        [JsonProperty("firstAlternateMethodId")]
        public long? FirstAlternateMethodId { get; set; }

        [JsonProperty("secondAlternateMethodId")]
        public long? SecondAlternateMethodId { get; set; }

        [JsonProperty("thirdAlternateMethodId")]
        public long? ThirdAlternateMethodId { get; set; }

        [JsonProperty("homeLocationId")]
        public long? HomeLocationId { get; set; }

        [JsonProperty("processingLocationId")]
        public long? ProcessingLocationId { get; set; }

        [JsonProperty("physicianName")]
        public string PhysicianName { get; set; }

        [JsonProperty("usesBeforeService")]
        public bool? UsesBeforeService { get; set; }

        [JsonProperty("usageInterval")]
        public string UsageInterval { get; set; }

        [JsonProperty("usageIntervalUnitOfMeasure")]
        public string UsageIntervalUnitOfMeasure { get; set; }

        [JsonProperty("standardAssemblyTime")]
        public string StandardAssemblyTime { get; set; }

        [JsonProperty("weight")]
        public string Weight { get; set; }

        [JsonProperty("processingCost")]
        public string ProcessingCost { get; set; }

        [JsonProperty("procurementReferenceId")]
        public string ProcurementReferenceId { get; set; }

        [JsonProperty("decontaminationInstructions")]
        public string DecontaminationInstructions { get; set; }

        [JsonProperty("assemblyInstructions")]
        public string AssemblyInstructions { get; set; }

        [JsonProperty("sterilizationInstructions")]
        public string SterilizationInstructions { get; set; }

        [JsonProperty("countSheetComments")]
        public string CountSheetComments { get; set; }

        [JsonProperty("priorityId")]
        public long? PriorityId { get; set; }

        [JsonProperty("biologicalTestRequired")]
        public bool? BiologicalTestRequired { get; set; }

        [JsonProperty("classSixTestRequired")]
        public bool? ClassSixTestRequired { get; set; }

        [JsonProperty("assemblyByException")]
        public bool? AssemblyByException { get; set; }

        [JsonProperty("softWrap")]
        public bool? SoftWrap { get; set; }

        [JsonProperty("complexityLevelId")]
        public long? ComplexityLevelId { get; set; }

        [JsonProperty("originalContainerTypeId")]
        public long? OriginalContainerTypeId { get; set; }

        [JsonProperty("vendorId")]
        public long? VendorId { get; set; }

        [JsonProperty("vendorName")]
        public string VendorName { get; set; }
    }
}
