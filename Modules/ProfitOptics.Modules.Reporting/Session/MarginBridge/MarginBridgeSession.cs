﻿using ProfitOptics.Modules.Reporting.Models.MarginBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Session.MarginBridge
{

    public class MarginBridgeSession
    {
        private List<JQueryDataTablesParamModel> _sortingData;

        public List<ProductHierarchyFilterModel> UncheckedProductsHierarchy { get; set; }

        public List<LocationHierarchyFilterModel> UncheckedLocationsHierarchy { get; set; }

        public List<ProductHierarchyFilterModel> ProductsHierarchyInitial { get; set; }

        public List<LocationHierarchyFilterModel> LocationsHierarchyInitial { get; set; }

        public List<List<string>> CheckedNodesInitial { get; set; }

        public MargineBridgeResponseDataModel StatisticsDataModel { get; set; }

        public MarginBridgeType MarginBridgeType { get; set; }

        public string SortData => GetSortData();

        public void AddSortData(string column, string orderDirection)
        {
            if (!string.IsNullOrEmpty(column))
            {
                var invalidCharIndex = column.IndexOf(".", StringComparison.Ordinal);
                if (invalidCharIndex > 0)
                {
                    column = column.Remove(0, invalidCharIndex + 1);
                }

                orderDirection = orderDirection ?? "asc";

                if (orderDirection.ToLower() != "asc")
                {
                    orderDirection = "desc";
                }

                if (_sortingData == null)
                {
                    _sortingData = new List<JQueryDataTablesParamModel>();
                }

                var currentValue = _sortingData.SingleOrDefault(s => string.Equals(s.sColumnName, column, StringComparison.CurrentCultureIgnoreCase));

                if (currentValue != null)
                {
                    _sortingData.Remove(currentValue);
                }

                _sortingData.Insert(0, new JQueryDataTablesParamModel
                {
                    sColumnName = column,
                    sSortColDir = orderDirection.ToUpper()
                });
            }
        }

        public void ClearSortData()
        {
            _sortingData = null;
        }

        private string GetSortData()
        {
            if (_sortingData == null)
            {
                return null;
            }

            var dataList = _sortingData.Select(s => s.sColumnName + " " + s.sSortColDir).ToList();

            var result = string.Join(", ", dataList);

            return result;
        }
    }
}
