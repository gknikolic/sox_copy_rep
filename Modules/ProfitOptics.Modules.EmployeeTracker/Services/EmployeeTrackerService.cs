﻿using ProfitOptics.Modules.EmployeeTracker.Data;

namespace ProfitOptics.Modules.EmployeeTracker.Services
{
    public class EmployeeTrackerService : IEmployeeTrackerService
    {
        #region Fields

        private readonly Entities _context;

        #endregion Fields

        #region Ctor

        public EmployeeTrackerService(Entities context)
        {
            _context = context;
        }

        #endregion Ctor

        #region Methods

        public void EmptyMethod()
        {
            return;
        }

        #endregion 
    }
}