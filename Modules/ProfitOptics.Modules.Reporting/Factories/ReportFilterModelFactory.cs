﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;
using ProfitOptics.Modules.Reporting.Services;

namespace ProfitOptics.Modules.Reporting.Factories
{
    public class ReportFilterModelFactory : IReportFilterModelFactory
    {
        private readonly IUserFilterService _userFilterService;
        private readonly IUserMappingService _userMappingService;
        private readonly Entities _context;
        private readonly IUserHierarchyService _userHierarchyService;

        public ReportFilterModelFactory(IUserFilterService userFilterService, IUserMappingService userMappingService, Entities context, IUserHierarchyService userHierarchyService)
        {
            _userFilterService = userFilterService;
            _userMappingService = userMappingService;
            _context = context;
            _userHierarchyService = userHierarchyService;
        }

        /// <inheritdoc />
        public ReportFilterModel CreateDefaultReportFilterModelForUserId(int userId)
        {
            if (userId <= 0)
            {
                return new ReportFilterModel();
            }

            ReportUserFilter defaultFilter = _userFilterService.GetDefaultReportFilterForUserId(userId);

            return CreateReportFilterModelFromUserFilter(userId, defaultFilter);
        }

        /// <inheritdoc />
        public FilterWidgetViewModel CreateFilterWidgetViewModel(int userId)
        {
            IEnumerable<ReportUserFilter> allFilters = _userFilterService.GetAllReportFiltersForUserId(userId);

            List<UserFilterModel> userFilters = allFilters.Select(f => new UserFilterModel(f.Id, f.Name)).ToList();

            return new FilterWidgetViewModel(userFilters,
                _userHierarchyService.IsCorporateUser(userId),
                _userHierarchyService.IsZoneUser(userId),
                _userHierarchyService.IsRegionUser(userId));
        }

        /// <inheritdoc />
        public SelectedFiltersModel CreateDefaultSelectedFiltersModel(int userId)
        {
            ReportFilterModel defaultFilter = CreateDefaultReportFilterModelForUserId(userId);
            
            return ConvertUserFilterToSelectedFiltersModel(defaultFilter);
        }

        /// <inheritdoc />
        public SelectedFiltersModel CreateSelectedFiltersModelForUserFilterId(int userId, int userFilterId)
        {
            ReportUserFilter userFilter = _userFilterService.GetUserFilterById(userFilterId);

            ReportFilterModel filter = CreateReportFilterModelFromUserFilter(userId, userFilter);

            return ConvertUserFilterToSelectedFiltersModel(filter);
        }

        /// <inheritdoc />
        public Select2PagedResult CreateFilterOptions(int userId, FilterType filterType, string search, int page, int take)
        {
            switch (filterType)
            {
                case FilterType.Zone:
                    return CreateZoneSelect2PagedResult(search, page, take);
                case FilterType.Region:
                    return CreateRegionSelect2PagedResult(search, page, take);
                case FilterType.CustomerClass:
                    return CreateCustomerClassSelect2PagedResult(search, page, take);
                case FilterType.ItemCategory:
                    return CreateItemCategorySelect2PagedResult(search, page, take);
                case FilterType.Vendor:
                    return CreateVendorSelect2PagedResult(search, page, take);
                case FilterType.BuyGroup:
                    return CreateBuyGroupSelect2PagedResult(search, page, take);
                case FilterType.SalesRep:
                    return CreateSalesRepSelect2PagedResult(search, page, take);
                case FilterType.BillTo:
                    return CreateBillToSelect2PagedResult(search, page, take);
                case FilterType.ShipTo:
                    return CreateShipToSelect2PagedResult(search, page, take);
                default:
                    throw new ArgumentOutOfRangeException(nameof(filterType), filterType, null);
            }
        }

        private static List<Select2Model> CreateSelect2Models(IQueryable<IFilterableEntity> filterableEntities, Func<IFilterableEntity, bool> predicate)
        {
            return filterableEntities
                //.Where(fe => predicate(fe))
                .ToSelect2List(true);
        }

        private static Select2PagedResult CreateSelect2PagedResult(IQueryable<object> query, int page, int take)
        {
            return query.Skip((page - 1) * take).Take(take).ToSelect2PagedResult(query.Count());
        }

        private SelectedFiltersModel ConvertUserFilterToSelectedFiltersModel(ReportFilterModel filter)
        {
            return new SelectedFiltersModel
            {
                //FilterZone = CreateSelect2Models(_context.ReportHierarchyZone, fe => filter.FilterZone.Contains(fe.Id)),
                //FilterRegion = CreateSelect2Models(_context.ReportHierarchyRegion, fe => filter.FilterRegion.Contains(fe.Id)),
                //FilterBuyGroup = CreateSelect2Models(_context.ReportBuyGroup, fe => filter.FilterBuyGroup.Contains(fe.Id)),
                //FilterVendor = CreateSelect2Models(_context.ReportVendor, fe => filter.FilterVendor.Contains(fe.Id)),
                //FilterCustomerClass = CreateSelect2Models(_context.ReportCustomerClasses, fe => filter.FilterCustomerClass.Contains(fe.Id)),
                //FilterItemCategory = CreateSelect2Models(_context.ReportItemCategory, fe => filter.FilterItemCategory.Contains(fe.Id)),
                //FilterSalesRep = CreateSelect2Models(_context.ReportHierarchySalesRep, fe => filter.FilterSalesRep.Contains(fe.Id)),
                //FilterBillTo = CreateSelect2Models(_context.ReportBillTo, fe => filter.FilterBillTo.Contains(fe.Id)),
                //FilterShipTo = CreateSelect2Models(_context.ReportShipTo, fe => filter.FilterShipTo.Contains(fe.Id))
                FilterZone = _context.ReportHierarchyZone.Where(x => filter.FilterZone.Contains(x.Id)).ToSelect2List(true),
                FilterRegion = _context.ReportHierarchyRegion.Where(x => filter.FilterRegion.Contains(x.Id)).ToSelect2List(true),
                FilterBuyGroup = _context.ReportBuyGroup.Where(x => filter.FilterBuyGroup.Contains(x.Id)).ToSelect2List(true),
                FilterVendor = _context.ReportVendor.Where(x => filter.FilterVendor.Contains(x.Id)).ToSelect2List(true),
                FilterCustomerClass = _context.ReportCustomerClasses.Where(x => filter.FilterCustomerClass.Contains(x.Id)).ToSelect2List(true),
                FilterItemCategory = _context.ReportItemCategory.Where(x => filter.FilterItemCategory.Contains(x.Id)).ToSelect2List(true),
                FilterSalesRep = _context.ReportHierarchySalesRep.Where(x => filter.FilterSalesRep.Contains(x.Id)).ToSelect2List(true),
                FilterBillTo = _context.ReportBillTo.Where(x => filter.FilterBillTo.Contains(x.Id)).ToSelect2List(true),
                FilterShipTo = _context.ReportShipTo.Where(x => filter.FilterShipTo.Contains(x.Id)).ToSelect2List(true)
            };
        }

        private ReportFilterModel CreateReportFilterModelFromUserFilter(int userId, ReportUserFilter filter)
        {
            if (userId <= 0 || 
                filter == null || 
                string.IsNullOrWhiteSpace(filter.Value))
            {
                return new ReportFilterModel();
            }

            var model = JsonConvert.DeserializeObject<ReportFilterModel>(filter.Value);

            ReportUserMapping userMapping = _userMappingService.GetReportUserMappingForUserId(userId);

            if (userMapping == null)
            {
                if (model.ZoneId == 0) model.ZoneId = null;
                if (model.RegionId == 0) model.RegionId = null;
                if (model.SalesRepId == 0) model.SalesRepId = null;

                return model;
            }

            model.ZoneId = userMapping.ZoneId;
            model.RegionId = userMapping.RegionId;
            model.SalesRepId = userMapping.SalesRepId;

            return model;
        }

        private Select2PagedResult CreateShipToSelect2PagedResult(string search, int page, int take)
        {
            IQueryable<ReportShipTo> shipToQuery = _context.ReportShipTo
                .Where(s => search == null || search == "" ||
                            s.ShipToName.Contains(search) || s.ShipToNum.Contains(search));

            return CreateSelect2PagedResult(shipToQuery, page, take);
        }

        private Select2PagedResult CreateBillToSelect2PagedResult(string search, int page, int take)
        {
            IQueryable<ReportBillTo> billToQuery = _context.ReportBillTo
                .Where(b => search == null || search == "" ||
                            b.BillToName.Contains(search) || b.BillToNum.Contains(search));

            return CreateSelect2PagedResult(billToQuery, page, take);
        }

        private Select2PagedResult CreateSalesRepSelect2PagedResult(string search, int page, int take)
        {
            IQueryable<ReportHierarchySalesRep> salesRepQuery = _context.ReportHierarchySalesRep
                .Where(s => search == null || search == "" || s.Name.Contains(search));

            return CreateSelect2PagedResult(salesRepQuery, page, take);
        }

        private Select2PagedResult CreateBuyGroupSelect2PagedResult(string search, int page, int take)
        {
            IQueryable<ReportBuyGroup> buyGroupQuery = _context.ReportBuyGroup
                .Where(b => search == null || search == "" || b.Name.Contains(search));

            return CreateSelect2PagedResult(buyGroupQuery, page, take);
        }

        private Select2PagedResult CreateVendorSelect2PagedResult(string search, int page, int take)
        {
            IQueryable<ReportVendor> vendorQuery = _context.ReportVendor
                .Where(v => search == null || search == "" || v.Name.Contains(search));

            return CreateSelect2PagedResult(vendorQuery, page, take);
        }

        private Select2PagedResult CreateItemCategorySelect2PagedResult(string search, int page, int take)
        {
            IQueryable<ReportItemCategory> itemCategoryQuery = _context.ReportItemCategory
                .Where(i => search == null || search == "" || i.Name.Contains(search));

            return CreateSelect2PagedResult(itemCategoryQuery, page, take);
        }

        private Select2PagedResult CreateCustomerClassSelect2PagedResult(string search, int page, int take)
        {
            IQueryable<ReportCustomerClass> customerClassQuery = _context.ReportCustomerClasses
                .Where(c => search == null || search == "" || c.Name.Contains(search));

            return CreateSelect2PagedResult(customerClassQuery, page, take);
        }

        private Select2PagedResult CreateRegionSelect2PagedResult(string search, int page, int take)
        {
            IQueryable<ReportHierarchyRegion> regionQuery = _context.ReportHierarchyRegion
                .Where(r => search == null || search == "" || r.Name.Contains(search));

            return CreateSelect2PagedResult(regionQuery, page, take);
        }

        private Select2PagedResult CreateZoneSelect2PagedResult(string search, int page, int take)
        {
            IQueryable<ReportHierarchyZone> zoneQuery = _context.ReportHierarchyZone
                .Where(z => search == null || search == "" || z.Name.Contains(search));

            return CreateSelect2PagedResult(zoneQuery, page, take);
        }
    }
}