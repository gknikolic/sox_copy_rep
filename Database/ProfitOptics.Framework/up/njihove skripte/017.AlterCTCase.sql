USE [SoxQATest]
ALTER TABLE [dbo].[CTCase] ADD CreatedAtUtc DateTime NOT NULL;
ALTER TABLE [dbo].[CTCase] ADD ModifiedAtUtc DateTime NULL;