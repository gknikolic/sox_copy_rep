﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.GroundControl
{
    public enum GCColorStatusType
    {
        Blue = 1,
        Purple = 2,
        Red = 3,
        Orange = 4,
        Yellow = 5,
        Green = 6,
        Gray = 7
    }
}
