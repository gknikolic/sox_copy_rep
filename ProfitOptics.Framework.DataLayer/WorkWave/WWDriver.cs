﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer.WorkWave
{
	[Table("WWDriver")]
	public partial class WWDriver
	{
		public int Id { get; set; }
		public string WWDriverId { get; set; }


		public string Name { get; set; }

		public string Email { get; set; }

		public virtual ICollection<WWTrackingData> WWTrackingData { get; set; }
	}
}
