﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProfitOptics.Modules.Sox.Services
{
	public interface IReportingService
	{
		public void UploadLocationInfoData(IFormFile file);
		public void ParseBasReport(Stream file);
	}
}
