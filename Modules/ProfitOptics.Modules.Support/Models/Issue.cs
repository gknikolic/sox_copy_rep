﻿using System;
using IssuesResult = ProfitOptics.Modules.Support.Models.JiraRestModels.IssuesResult;

namespace ProfitOptics.Modules.Support.Models
{
    public class Issue
    {
        public string Username { get; set; }

        public string Key { get; set; }

        public string Summary { get; set; }

        public string Type { get; set; }

        public bool IsSubtask { get; set; }

        public string Status { get; set; }

        public string AssigneeName { get; set; }

        public string AssigneeEmail { get; set; }

        public string AssigneeDisplayName { get; set; }

        public int? AggregateTimeEstimate { get; set; }

        public string ReporterName { get; set; }

        public string ReporterEmail { get; set; }

        public string ReporterDisplayName { get; set; }

        public int? AggregateTimeOriginalEstimate { get; set; }

        public string ProjectKey { get; set; }

        public string ProjectName { get; set; }

        public string Environment { get; set; }

        public DateTime? Updated { get; set; }

        public string Description { get; set; }

        public string Priority { get; set; }

        public int? TimeEstimate { get; set; }

        public DateTime? DueDate { get; set; }

        public int? TimeOriginalEstimate { get; set; }

        public string FixVersion { get; set; }

        public string Component { get; set; }

        public int CommentCount { get; set; }

        public string LocalCreator { get; set; }

        public Issue(IssuesResult issue)
        {
            this.LocalCreator = issue.fields.labels.Count > 0 ? issue.fields.labels[0] : "Jira";
            this.AggregateTimeEstimate = issue.fields.aggregatetimeestimate;
            this.AggregateTimeOriginalEstimate = issue.fields.aggregatetimeoriginalestimate;
            if (issue.fields.assignee != null)
            {
                this.AssigneeDisplayName = issue.fields.assignee.displayName;
                this.AssigneeEmail = issue.fields.assignee.emailAddress;
                this.AssigneeName = issue.fields.assignee.name;
                this.Username = issue.fields.assignee.displayName;
            }
            else
            {
                this.AssigneeDisplayName = "Unassigned";
                this.AssigneeEmail = "Unassigned";
                this.AssigneeName = "Unassigned";
                this.Username = "Unassigned";
            }

            this.Description = issue.fields.description;
            this.DueDate = issue.fields.duedate;
            this.Environment = issue.fields.environment;
            this.IsSubtask = issue.fields.issuetype.subtask;
            this.Key = issue.key;
            this.Priority = issue.fields.priority.name;
            this.ProjectKey = issue.fields.project.key;
            this.ProjectName = issue.fields.project.name;
            this.ReporterDisplayName = issue.fields.reporter.displayName;
            this.ReporterEmail = issue.fields.reporter.emailAddress;
            this.ReporterName = issue.fields.reporter.name;
            this.Status = issue.fields.status.Name;
            this.Summary = issue.fields.summary;
            this.TimeOriginalEstimate = issue.fields.timeoriginalestimate;
            this.TimeEstimate = issue.fields.timeestimate;
            this.Type = issue.fields.issuetype.name;
            this.Updated = issue.fields.updated;
            var comVal = string.Empty;
            foreach (var com in issue.fields.components)
            {
                comVal += com.name + ",";
            }
            comVal = comVal.TrimEnd(new[] { ',' });
            this.Component = comVal;
            var fixver = string.Empty;
            foreach (var fv in issue.fields.fixVersions)
            {
                fixver += fv.name + ",";
            }
            fixver = fixver.TrimEnd(new[] { ',' });
            this.FixVersion = fixver;
        }

        public Issue(Atlassian.Jira.Issue issue)
        {
        }
    }
}