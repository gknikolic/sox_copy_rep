﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ProfitOptics.Modules.Announcements.Data.Domain;
using ProfitOptics.Modules.Announcements.Models;

namespace ProfitOptics.Modules.Announcements.Services
{
    /// <summary>
    /// Represents the announcement service.
    /// </summary>
    public interface IAnnouncementService
    {
        /// <summary>
        /// Saves the announcement.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="form">The form collection instance.</param>
        void Save(int userId, IFormCollection form);

        /// <summary>
        /// Deletes the announcement with the specified identifier.
        /// </summary>
        /// <param name="id">The announcement identifier.</param>
        void Delete(int id);

        /// <summary>
        /// Marks the announcement with the specified identifier as read.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="id">The announcement identifier.</param>
        void MarkAnnouncementAsRead(int userId, int id);

        /// <summary>
        /// Gets the announcement the specified identifier.
        /// </summary>
        /// <param name="id">The announcement identifier.</param>
        /// <returns>The announcement with the specified identifier if it exists, otherwise null.</returns>
        POAnnouncement GetAnnouncementById(int id);

        /// <summary>
        /// Gets the announcements for the specified user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="getActive">Whether to get only active announcements or not.</param>
        /// <returns>The collection of announcements for the specified user identifier.</returns>
        Task<IEnumerable<POAnnouncement>> GetAnnouncementsForUserIdAsync(int userId, bool getActive = false);

        /// <summary>
        /// Gets the announcements with profile pictures for the specified user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="getActive">Whether to get only active announcements or not.</param>
        /// <returns>The collection of announcements for the specified user identifier.</returns>
        Task<IEnumerable<POAnnouncementWithProfilePicture>> GetAnnouncementsWithProfilePicturesForUserIdAsync(int userId,
            bool getActive = false);

        /// <summary>
        /// Searches the announcements for the specified parameters.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="validOnly">Whether to get only valid announcements or not.</param>
        /// <param name="activeOnly">Whether to get only active announcements or not.</param>
        /// <returns>The collection of announcements for the specified parameters.</returns>
        Task<IEnumerable<POAnnouncement>> SearchAnnouncementsAsync(int? userId = null, bool validOnly = false,
            bool activeOnly = false);

    }
}