﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.GroundControl
{
    public enum GCSystemAlertType
    {
        GCTrayIncompleteCountSheet = 1,
        GCTrayQualityHold = 2,
        GCTraySkippedAssemblyAfterQualityHold = 3,
        GCTraySkippedRequiredStepSink = 4,
        GCTraySkippedRequiredStepSinkLoaner = 5,
        GCTraySkippedRequiredStepWasher = 6,
        GCTraySkippedRequiredStepWasherLoaner = 7,
        GCTraySkippedRequiredStepAssembly = 8,
        GCTraySkippedRequiredStepAssemblyLoaner = 9,
        GCTraySkippedRequiredStepVerification = 10,
        GCTraySkippedRequiredStepSterilize = 11,
        GCTraySkippedRequiredStepSterileStaged = 12,
        GCTraySkippedRequiredStepTransportation = 13,
        GCTraySkippedRequiredStepLoanerInbound = 14,
        GCTraySkippedRequiredStepLoanerOutbound = 15,
        GCTrayVerificationQualityFeedDataMismatch = 16
    }
}
