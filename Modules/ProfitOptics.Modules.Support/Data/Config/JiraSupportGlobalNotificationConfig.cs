﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Support.Data.Domain;

namespace ProfitOptics.Modules.Support.Data.Config
{
    public class JiraSupportGlobalNotificationConfig : IEntityTypeConfiguration<JiraSupportGlobalNotification>
    {
        public void Configure(EntityTypeBuilder<JiraSupportGlobalNotification> builder)
        {
            builder.ToTable("JiraSupportGlobalNotification");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Email).HasMaxLength(255).IsRequired();
        }
    }
}
