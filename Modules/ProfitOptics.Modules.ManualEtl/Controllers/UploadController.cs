﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Modules.ManualEtl.Factories;

namespace ProfitOptics.Modules.ManualEtl.Controllers
{
    [Area("ManualEtl")]
    public class UploadController : BaseController
    {
        private readonly Common _settings;
        private readonly IManualEtlFactory _manualEtlFactory;

        public UploadController(Common settings,
            IManualEtlFactory manualEtlFactory)
        {
            _manualEtlFactory = manualEtlFactory;
            _settings = settings;
        }

        public IActionResult Index()
        {
            var model = _manualEtlFactory.GetManualUploadViewModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Upload(string fileTypeId)
        {
            try
            {
                // TODO: Implemente custom file loading using classes from .Core project.

                //var factory = new FileLoaderFactory();

                //if (factory.TryCreateInstance(fileTypeId, out var loader))
                //{
                //    foreach (var file in Request.Form.Files)
                //    {
                //        var fileContent = Request.Form.Files[file.FileName];
                //        if (fileContent == null || fileContent.Length <= 0)
                //        {
                //            continue;
                //        }

                //        // Save file
                //        var stream = fileContent.OpenReadStream();
                //        var fileName = Path.GetFileName(file.FileName);

                //        if (fileName == null)
                //        {
                //            throw new ArgumentNullException(nameof(fileName), @"File name can not be null");
                //        }
                //        var path = Path.Combine(_settings.TempDirectory, fileName);
                //        using (var fileStream = System.IO.File.Create(path))
                //        {
                //            stream.CopyTo(fileStream);
                //        }

                //        var rowCount = loader.Load(path);

                //        _etlHelper.LogProcessedFile(User.Identity.Name, fileTypeId,
                //            "Uploaded file", DateTime.Now.ToString(CultureInfo.InvariantCulture), "OK", rowCount);
                //    }
                //}
            }
            catch (Exception ex)
            {
                //_etlHelper.LogProcessedFile(User.Identity.Name, fileTypeId,
                //    "Uploaded file", DateTime.Now.ToString(CultureInfo.InvariantCulture), "Error", 0, ex.Message);

                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Error");
            }

            return Json("Ok");
        }
    }
}