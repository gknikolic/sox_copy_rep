﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class ExecutionEventStatusUpdate : ExecutionEvent
	{
		[JsonProperty("data")]
		public EEDataStatusUpdate Data { get; set; }
	}


}
