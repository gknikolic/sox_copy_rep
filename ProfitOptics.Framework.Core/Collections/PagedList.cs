﻿using System.Collections.Generic;
using System.Linq;

namespace ProfitOptics.Framework.Core.Collections
{
    public class PagedList<T> : List<T>
    {
        /// <summary>
        /// Gets or sets the total count.
        /// </summary>
        public int TotalCount { get; set; }
        
        /// <summary>
        /// Gets or sets the totals.
        /// </summary>
        public IDictionary<string, decimal?> Total { get; }

        public PagedList()
        {
            TotalCount = default(int);
        }

        public PagedList(IEnumerable<T> items, int totalCount, IDictionary<string, decimal?> total = null)
        {
            TotalCount = totalCount;

            Total = total;

            AddRange(items);
        }

        public PagedList(IQueryable<T> source, int skip = 0, int take = int.MaxValue)
        {
            TotalCount = source.Count();

            AddRange(source.Skip(skip).Take(take).ToList());
        }
    }
}
