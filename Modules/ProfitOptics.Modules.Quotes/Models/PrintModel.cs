﻿using System;
using ProfitOptics.Framework.Core.Data;

namespace ProfitOptics.Modules.Quotes.Models
{
    public partial class PrintModel
    {
        public class PrintQuoteItems : IDataRecord
        {
            [DataField("Record Id", "", 1)]
            public int ItemId { get; set; }
            [DataField("Part No", "", 2)]
            public string ItemNum { get; set; }
            [DataField("Item Description", "", 3)]
            public string ItemDescription { get; set; }
            [DataField("UOM", "", 4)]
            public string ItemUoM { get; set; }
            [DataField("Freight Class", "", 5)]
            public decimal? FreightClass { get; set; }
            [DataField("Units", "", 6)]
            public int? Units { get; set; }
            [DataField("3 Mo Qty", "", 7)]
            public int? MoQty { get; set; }
            [DataField("Last Price", "", 8)]
            public decimal? LastPrice { get; set; }
            [DataField("Last Date", "", 9)]
            public DateTime? LastDate { get; set; }
            [DataField("Last GP%", "", 10)]
            public decimal? LastGpPct { get; set; }
            [DataField("Product Weight", "", 11)]
            public decimal? WeightProduct { get; set; }
            [DataField("Total Weight", "", 12)]
            public decimal WeightTotal { get; set; }
            [DataField("Current Cost", "#,###", 13)]
            public decimal? CurrentCost { get; set; }
            [DataField("Current Price", "#,###", 14)]
            public decimal? CurrentPrice { get; set; }
            [DataField("Current Price List Code", "", 15)]
            public string CurrentSource { get; set; }
            [DataField("Current Contract", "", 16)]
            public bool? CurrentC { get; set; }
            [DataField("Current Blitzed Enable", "", 17)]
            public bool? CurrentB { get; set; }
            [DataField("Current GM %", "0.0%", 18)]
            public decimal? CurrentGmPercent { get; set; }
            [DataField("Quote Price", "#,###", 19)]
            public decimal? QuotePrice { get; set; }
            [DataField("Quote GM%", "0.0%", 20)]
            public decimal? QuoteGmPercent { get; set; }
            [DataField("Price Guidance Floor", "#,###", 21)]
            public decimal? PriceGuidanceFloor { get; set; }
            [DataField("Price Guidance Target", "#,###", 22)]
            public decimal? PriceGuidanceTarget { get; set; }
            [DataField("Price Guidance List", "#,###", 23)]
            public decimal? PriceGuidanceList { get; set; }
            [DataField("Quote Extended Cost", "#,###", 24)]
            public decimal QuoteExtendedCost { get; set; }
            [DataField("Quote Extended Revenue", "#,###", 25)]
            public decimal QuoteExtendedRevenue { get; set; }
            [DataField("Competitor Price", "#,###", 26)]
            public decimal? CompetitorPrice { get; set; }
            [DataField("Competitor UOM", "#,###", 27)]
            public string CompetitorUoM { get; set; }
            [DataField("Competitor CF", "", 28)]
            public decimal? CompetitorCf { get; set; }
            [DataField("Competitor Price Var $", "#,###", 29)]
            public decimal? CompetitorPriceVarDollar { get; set; }
            [DataField("Competitor Ext Price Var $", "#,###", 30)]
            public decimal? CompetitorExtPriceVarDollar { get; set; }
            [DataField("Quote Ext M$ Before Freight", "#,###", 31)]
            public decimal? QuoteExtendedMBeforeFreight { get; set; }
            [DataField("Quote Extended Net Shipping Profit", "#,###", 32)]
            public decimal? QuoteExtendedNetShippingProfit { get; set; }
            [DataField("QuoteExtendedAdjustedMargin $", "#,###", 33)]
            public decimal? QuoteExtendedAdjustedMarginDollar { get; set; }
            [DataField("Quote Extended Adjusted Margin %", "0.0%", 34)]
            public decimal? QuoteExtendedAdjustedMarginPercent { get; set; }
            [DataField("Floor Revenue", "#,###", 35)]
            public decimal FloorRevenue { get; set; }
            [DataField("Floor Cost", "#,###", 36)]
            public decimal FloorCost { get; set; }
            [DataField("Floor M $", "#,###", 37)]
            public decimal FloorMDollar { get; set; }
            [DataField("Floor M %", "0.0%", 38)]
            public decimal? FloorMPercent { get; set; }
        }
    }
}