﻿using System;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayHistoryDeliveryPhotos
	{
		public TrayHistoryPodContainer PodContainer { get; set; }
		public DateTime Timestamp { get; set; }
		public string Title { get; set; }
		public int NumOfPhotos { get; set; }
	}

	public class PhotoWithCountSheet
	{
		public string ImageSrc { get; set; }
		public bool ShowCountSheet { get; set; }
		public AssemblyCountSheet AssemblyCountSheet { get; set; }
	}

}
