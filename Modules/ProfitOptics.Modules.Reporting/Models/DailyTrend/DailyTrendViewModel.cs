﻿namespace ProfitOptics.Modules.Reporting.Models.DailyTrend
{
    public class DailyTrendViewModel
    { 
        public bool IsCorporate { get; set; }

        public bool IsSalesRep { get; set; }

        public string StartDateDefault { get; set; }

        public string EndDateDefault { get; set; }
    }
}
