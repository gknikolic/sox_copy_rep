﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.ManualEtl.Models.Alerting
{
    public class DbCheckViewModel
    {
        public DbCheckViewModel()
        {
            EtlStatuses = new List<EtlLogStatusViewModel>();
            DbTableStatuses = new List<DbTableStatusViewModel>();
        }

        public List<EtlLogStatusViewModel> EtlStatuses { get; set; }
        public List<DbTableStatusViewModel> DbTableStatuses { get; set; }
    }
}