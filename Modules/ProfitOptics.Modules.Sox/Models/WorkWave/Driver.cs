﻿using Newtonsoft.Json;
using System;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class Driver
	{

		[JsonProperty("id")]
		public string Id { get; set; }


		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("email")]
		public string Email { get; set; }
	}
}
