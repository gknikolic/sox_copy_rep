﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer.Configs
{
    public class CTBILoadResultContentConfig : IEntityTypeConfiguration<CTBILoadResultContent>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTBILoadResultContent> builder)
        {
            builder.ToTable("CTBILoadResultContent");
            builder.HasKey(entity => entity.Id);
        }
    }
}
