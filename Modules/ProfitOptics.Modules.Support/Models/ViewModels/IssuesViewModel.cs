﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ProfitOptics.Modules.Support.Models.ViewModels
{
    public class IssuesViewModel
    {
        public string SupportMessage { get; set; }

        public List<SelectListItem> ProjectSelectList { get; set; }

        public List<SelectListItem> ReporterSelectList { get; set; }

        public List<SelectListItem> AssigneesSelectList { get; set; }

        public string SelectedProjectKey { get; set; }

        public string SelectedAssignee { get; set; }

        public string SelectedReporter { get; set; }

        public List<SelectListItem> AllComponents { get; set; }

        public List<string> SelectedComponents { get; set; }

        public BoardViewModel Board { get; set; }

        public bool HasBacklog { get; set; }
    }

    public class BoardViewModel
    {
        public BoardViewModel()
        {
            Columns = new List<BoardColumnViewModel>();
        }

        public List<BoardColumnViewModel> Columns { get; set; }
    }

    public class BoardColumnViewModel
    {
        public string ColumnName { get; set; }

        public List<string> Statuses { get; set; }
    }

}