﻿namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class GCCasesFilterModel
	{
		public string SelectedCustomer { get; set; }
		//public string SelectedVendor { get; set; }
		public string CaseReference { get; set; }
		public string Surgeon { get; set; }
	}

}
