﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.TrendVariance;

namespace ProfitOptics.Modules.Reporting.Services.TrendVariance
{
    public class TrendVarianceExportService : ITrendVarianceExportService
    {
        private readonly ITrendVarianceDataService _trendVarianceDataService;

        public TrendVarianceExportService(ITrendVarianceDataService trendVarianceDataService)
        {
            _trendVarianceDataService = trendVarianceDataService;
        }

        /// <inheritdoc />
        public MemoryStream ExportCustomerDrillThroughVariances(int userId, bool isBillTo, int customerId, ComparisonType comparisonType,
            DateTime? comparisonStartDate, DateTime? comparisonEndDate)
        {
            var periods = new TrendVariancePeriodsModel(comparisonType, comparisonStartDate, comparisonStartDate);

            PagedList<TrendVarianceVarianceByTypeModel> variancesGroupedByItemCategory = isBillTo
                ? _trendVarianceDataService.GetVariancesGroupedByItemCategoryForBillToId(userId, customerId,
                    DefaultDataTablesRequest.Default,
                    periods)
                : _trendVarianceDataService.GetVariancesGroupedByItemCategoryForShipToId(userId, customerId,
                    DefaultDataTablesRequest.Default, periods);

            List<TrendVarianceExportToExcelModel> trendVarianceExportToExcelModels = 
                ConvertVarianceByTypeModelsToTrendVarianceExportToExcelModels(variancesGroupedByItemCategory, periods);

            MemoryStream excelFile = ExcelFile.ExportToExcelOverStream("Trend Variance",
                trendVarianceExportToExcelModels,
                true,
                true);

            excelFile = ExcelFile.ApplyFilterAndFreezePanes(excelFile, 2, 3, 1);

            return excelFile;
        }

        /// <inheritdoc />
        public MemoryStream ExportVariancesByType(int userId, TrendVarianceVarianceByTypeRequestParameters parameters)
        {
            var periods = new TrendVariancePeriodsModel(parameters.ComparisonType, parameters.StartDate, parameters.EndDate);

            PagedList<TrendVarianceVarianceByTypeModel> pagedData = _trendVarianceDataService.GetVariancesGroupedByViewByType(
                userId, DefaultDataTablesRequest.Default, parameters.ViewByType, periods);

            List<TrendVarianceExportToExcelModel> data = ConvertVarianceByTypeModelsToTrendVarianceExportToExcelModels(pagedData, periods);

            MemoryStream excelFile = ExcelFile.ExportToExcelOverStream("Trend Variance",
                data,
                true,
                true);

            excelFile = ExcelFile.ApplyFilterAndFreezePanes(excelFile, 2, 3, 1);

            return excelFile;
        }

        private static List<TrendVarianceExportToExcelModel> ConvertVarianceByTypeModelsToTrendVarianceExportToExcelModels(
            IEnumerable<TrendVarianceVarianceByTypeModel> itemsByTypeModels, TrendVariancePeriodsModel periods)
        {
            return itemsByTypeModels
                .Select(item => new TrendVarianceExportToExcelModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Code = item.Code,
                    PriorSales = item.PriorSales,
                    PriorCost = item.PriorCost,
                    PriorQty = item.PriorQty,
                    CurrentSales = item.CurrentSales,
                    CurrentCost = item.CurrentCost,
                    CurrentQty = item.CurrentQty
                }).ToList();
        }
    }
}