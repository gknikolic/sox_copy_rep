﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Areas.Account.Data;
using ProfitOptics.Framework.Web.Areas.Account.Factories;
using ProfitOptics.Framework.Web.Areas.Account.Models;
using ProfitOptics.Framework.Web.Areas.Account.Services;
using ProfitOptics.Framework.Web.Framework.Identity;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Framework.Web.Framework.Services;
using IAuthenticationService = ProfitOptics.Framework.Web.Framework.Services.IAuthenticationService;

namespace ProfitOptics.Framework.Web.Areas.Account.Infrastructure
{
    public sealed class AccountServiceRegistrator :IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<IAccountModelFactory, AccountModelFactory>();
            services.AddScoped<IAccountManagementService, AccountManagementService>();
            services.AddScoped<IRecaptchaService, RecaptchaService>();
            services.AddSingleton<SignInJsonResult, SignInJsonResult>();

            var migrationsAssembly = typeof(Startup).Assembly.GetName().Name;

            services.AddDbContext<ApplicationUserDbContext>(options =>
            {
                options.UseSqlServer(connectionStrings.AreaEntities,
                    sql => sql.MigrationsAssembly(migrationsAssembly));
            });

            services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, ApplicationUserClaimsPrincipalFactory>();
            services.AddScoped<ITwoFactorResolver, EMailTwoFactorResolver>();
            services.AddScoped<ITwoFactorResolver, SMSTwoFactorResolver>();
            services.AddScoped<IAuthenticationService, Web.Framework.Services.AuthenticationService>();
            services.AddScoped<IUserService, UserService>();

            
        }
    }
}
