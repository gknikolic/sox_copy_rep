﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ProfitOptics.Framework.Core.Data
{
    public static class ExcelFile
    {
        public static string ExcelContentType => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        /// <summary>
        /// Export DataTable to excel file.
        /// </summary>
        /// <param name="outputFileName"></param>
        /// <param name="outputSheetName"></param>
        /// <param name="dt"></param>
        /// <param name="writeHeader"></param>
        /// <param name="autoFit"></param>
        public static void ExportToExcel(string outputFileName, string outputSheetName, IDataReader dr, bool writeHeader, bool autoFit = false, List<string> columnFormats = null, string startCell = "A1")
        {
            using (var pck = new ExcelPackage())
            {
                if (File.Exists(outputFileName))
                {
                    using (var stream = File.Open(outputFileName, FileMode.Open, FileAccess.Read))
                    {
                        pck.Load(stream);
                    }
                }

                ExcelWorksheet ws = pck.Workbook.Worksheets[outputSheetName];
                if (ws == null)
                    ws = pck.Workbook.Worksheets.Add(outputSheetName);

                // add the content into the Excel file
                ws.Cells[startCell].LoadFromDataReader(dr, writeHeader);

                var x = ws;
                // autofit
                if (autoFit)
                {
                    int finalrow = ws.Dimension.End.Row;
                    ws.Cells[1, 1, finalrow, dr.FieldCount].AutoFitColumns();
                }

                // Apply format
                if (columnFormats != null)
                {
                    if (columnFormats.Count != dr.FieldCount)
                        throw new ArgumentException("Supplied number of items in columnFormats list doesn't correspond to number of fields in data reader.");

                    for (int i = 0; i < dr.FieldCount; i++)
                        ws.Column(i + 1).Style.Numberformat.Format = columnFormats[i];
                }

                if (!File.Exists(outputFileName))
                {
                    var result = pck.GetAsByteArray();
                    File.WriteAllBytes(outputFileName, result);
                }
                else
                {
                    using (var stream = File.OpenWrite(outputFileName))
                    {
                        pck.SaveAs(stream);
                    }
                }
            }
        }

        /// <summary>
        /// Exports data into excel file
        /// </summary>
        /// <typeparam name="T">Type of record that should be exported</typeparam>
        /// <param name="outputFileName">Path to Excel file. If file doesn't exist, it will be created.</param>
        /// <param name="outputSheetName">Name of the Sheet where data will be exported to. If sheet doesn't exist, it will be created. If it exists data formats will be inherited.</param>
        /// <param name="data">Data to export</param>
        /// <param name="writeHeader">Should we write header?</param>
        public static void ExportToExcel<T>(string outputFileName, string outputSheetName, List<T> data, bool writeHeader = true, bool autoFit = false, string startCell = "A1") where T : IDataRecord
        {
            var columnFormats = DataRecordHelpers.GetPropertiesMetadata<T>().Select(p => p.Attribute.FormatString).ToList();
            ExportToExcel(outputFileName, outputSheetName, DataRecordHelpers.ToDataReader<T>(data), writeHeader, autoFit, columnFormats, startCell);
        }

        /// <summary>
        /// Export DataTable to excel file.
        /// </summary>
        /// <param name="outputFileName"></param>
        /// <param name="outputSheetName"></param>
        /// <param name="dt"></param>
        /// <param name="writeHeader"></param>
        /// <param name="autoFit"></param>
        public static MemoryStream ExportToExcelOverStream(string outputSheetName, IDataReader dr, bool writeHeader, bool autoFit = false, List<string> columnFormats = null, string startCell = "A1")
        {
            using (var pck = new ExcelPackage())
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets[outputSheetName];
                if (ws == null)
                    ws = pck.Workbook.Worksheets.Add(outputSheetName);

                // add the content into the Excel file
                ws.Cells[startCell].LoadFromDataReader(dr, writeHeader);

                // autofit
                if (autoFit)
                {
                    int finalrow = ws.Dimension.End.Row;
                    ws.Cells[1, 1, finalrow, dr.FieldCount].AutoFitColumns();
                }

                // Apply format
                if (columnFormats != null)
                {
                    if (columnFormats.Count != dr.FieldCount)
                        throw new ArgumentException("Supplied number of items in columnFormats list doesn't correspond to number of fields in data reader.");

                    for (int i = 0; i < dr.FieldCount; i++)
                        ws.Column(i + 1).Style.Numberformat.Format = columnFormats[i];
                }

                ws.Cells.AutoFitColumns();

                byte[] byteArray = pck.GetAsByteArray();
                MemoryStream stream = new MemoryStream();
                stream.Write(byteArray, 0, byteArray.Length);
                stream.Close();
                return new MemoryStream(stream.ToArray());
            }
        }

        /// <summary>
        /// Exports data into excel file
        /// </summary>
        /// <typeparam name="T">Type of record that should be exported</typeparam>
        /// <param name="outputFileName">Path to Excel file. If file doesn't exist, it will be created.</param>
        /// <param name="outputSheetName">Name of the Sheet where data will be exported to. If sheet doesn't exist, it will be created. If it exists data formats will be inherited.</param>
        /// <param name="data">Data to export</param>
        /// <param name="writeHeader">Should we write header?</param>
        public static MemoryStream ExportToExcelOverStream<T>(string outputSheetName, List<T> data, bool writeHeader = true, bool autoFit = false, string startCell = "A1") where T : IDataRecord
        {
            List<PropertyMetadata> propertiesMetadata = DataRecordHelpers.GetPropertiesMetadata<T>();

            List<string> columnFormats = propertiesMetadata.Select(p => p.Attribute.FormatString).ToList();

            MemoryStream excelFile = ExportToExcelOverStream(outputSheetName, DataRecordHelpers.ToDataReader(data),
                writeHeader, autoFit, columnFormats, startCell);

            List<int> columnsForSubtotal = propertiesMetadata
                .Where(x => x.Attribute.ApplySubtotal)
                .Select(x => x.Attribute.Position).ToList();

            if (columnsForSubtotal.Any())
            {
                excelFile = ApplySubtotals(excelFile, columnsForSubtotal, data.Count);
            }

            return excelFile;
        }


        /// <summary>
        /// Loads DataRecords from XLSX file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName">Source file name</param>
        /// <param name="worksheetIndex"></param>
        /// <param name="startCell">Cell from where should start reading data. This is not header cell.</param>
        /// <returns></returns>
        public static DataImportResult<T> LoadFromExcelFile<T>(string fileName, int worksheetIndex, string startCell = "A1") where T : IDataRecord
        {
            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return LoadFromExcelFile<T>(stream, worksheetIndex, startCell);
            }
        }

        /// <summary>
        /// Loads DataRecords from XLSX file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="inputStream">Input stream</param>
        /// <param name="worksheetIndex">Worksheet index</param>
        /// <param name="startCell">Cell from where should start data reading. This is not header cell.</param>
        /// <returns></returns>
        public static DataImportResult<T> LoadFromExcelFile<T>(Stream inputStream, int worksheetIndex, string startCell = "A1") where T : IDataRecord
        {
            using (var pck = new ExcelPackage())
            {
                pck.Load(inputStream);

                var toReturn = new DataImportResult<T>();

                var ws = pck.Workbook.Worksheets[worksheetIndex];
                var myType = typeof(T);

                // Collect all properties that should be found in excel file
                var allProperties = myType.GetProperties();
                var properties = new Dictionary<int, PropertyInfo>();
                foreach (var property in allProperties)
                {
                    var dataFieldAttribute = property.GetCustomAttributes(true).OfType<DataFieldAttribute>().FirstOrDefault() as DataFieldAttribute;
                    if (dataFieldAttribute != null)
                        properties.Add(dataFieldAttribute.Position, property);
                }

                // Get data from excel sheet
                var endColumn = properties.Keys.Max();

                var sCell = ws.Cells[startCell + ":" + startCell];
                var columnOffset = sCell.Start.Column - 1;

                var startColumn = sCell.Start.Column;
                endColumn += columnOffset;
                var startRow = sCell.Start.Row;

                for (var rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var currentColumn = startColumn;
                    try
                    {
                        var objT = Activator.CreateInstance<T>();
                        var wsRow = ws.Cells[rowNum, startColumn, rowNum, endColumn];

                        foreach (var currentIndex in properties.Keys)
                        {
                            currentColumn = currentIndex + columnOffset;

                            // Handle special cases
                            properties[currentIndex].SetValue(objT, GetSafePropertyValue(properties[currentIndex].PropertyType, wsRow[rowNum, currentColumn].Value));
                        }

                        toReturn.Data.Add(objT);
                    }
                    catch (Exception ex)
                    {
                        toReturn.Errors.Add(new DataImportError(rowNum, currentColumn, ex.Message));
                    }
                }

                return toReturn;
            }
        }

        /// <summary>
        /// Sets freeze panes and auto filter in the provided excel file.
        /// </summary>
        /// <param name="excelFile">Excel file that will be modified.</param>
        /// <param name="freezePaneRow">Freeze cells above this row.</param>
        /// <param name="freezePaneColumn">Freeze cells left of this column.</param>
        /// <param name="worksheet">Targeted worksheet of the excel file.</param>
        /// <param name="filterRange">Optional filter range parameter. If it is not set, auto-filter option will be set on all cells in the first row.</param>
        /// <returns>Modified excel file with freeze panes and auto filter.</returns>
        public static MemoryStream ApplyFilterAndFreezePanes(MemoryStream excelFile, int freezePaneRow, int freezePaneColumn, int worksheet = 0, string filterRange = null)
        {
            using (var pck = new ExcelPackage())
            {
                pck.Load(excelFile);

                ExcelWorksheet ws = pck.Workbook.Worksheets[worksheet];

                if (ws == null)
                {
                    throw new ArgumentNullException(nameof(ws));
                }

                if (filterRange == null)
                {
                    int headerRowId = ws.Cells.Start.Row;

                    int headerRowStartColumn = ws.Cells.Start.Column;

                    int headerRowEndColumn = ws.Cells.Columns;

                    ws.Cells[headerRowId, headerRowStartColumn, headerRowId, headerRowEndColumn].AutoFilter = true;
                }
                else
                {
                    ws.Cells[filterRange].AutoFilter = true;
                }

                ws.View.FreezePanes(freezePaneRow, freezePaneColumn);

                return new MemoryStream(pck.GetAsByteArray());
            }
        }

        private static MemoryStream ApplySubtotals(MemoryStream excelFile, IEnumerable<int> columnsForSubtotal, int offset = 0, bool hasHeaderRow = true, int worksheet = 1)
        {
            if (offset == 0)
            {
                return excelFile;
            }

            int rowStart = hasHeaderRow ? 2 : 1;

            int rowEnd = rowStart + offset - 1;

            foreach (int columnIndex in columnsForSubtotal)
            {
                string columnLetter = ((char)('A' + (char)columnIndex - 1)).ToString();

                string formula = GenerateSubtotalFormula(9, $"{columnLetter}{rowStart}:{columnLetter}{rowEnd}");

                excelFile = ApplyFormula(excelFile, formula, rowEnd + 1, columnIndex, worksheet);
            }

            using (var pck = new ExcelPackage())
            {
                pck.Load(excelFile);

                ExcelWorksheet ws = pck.Workbook.Worksheets[worksheet];

                if (ws == null)
                {
                    throw new ArgumentNullException(nameof(ws));
                }

                ws.Cells[$"A{rowEnd + 1}"].Value = "Total";

                return new MemoryStream(pck.GetAsByteArray());
            }
        }

        public static MemoryStream ApplyFormula(MemoryStream excelFile, string formula, int formulaRow,
            int formulaColumn, int workSheet = 1)
        {
            using (var pck = new ExcelPackage())
            {
                pck.Load(excelFile);

                // For some reason, index of the first worksheet is 1.
                ExcelWorksheet ws = pck.Workbook.Worksheets[workSheet];

                if (ws == null)
                {
                    throw new ArgumentNullException(nameof(ws));
                }

                if (string.IsNullOrWhiteSpace(formula))
                {
                    return excelFile;
                }

                // Setting width to 120 because cells with formulas don't adjust width automatically.
                // Important: width is in inches.
                ws.Column(formulaColumn).Width = 16;

                ws.Cells[formulaRow, formulaColumn].Formula = formula;

                return new MemoryStream(pck.GetAsByteArray());
            }
        }

        public static string GenerateSubtotalFormula(int functionCode, string range)
        {
            if (functionCode <= 0)
            {
                throw new ArgumentException(nameof(functionCode));
            }

            if (string.IsNullOrWhiteSpace(range))
            {
                throw new ArgumentNullException(nameof(range));
            }

            string formula = $"=SUBTOTAL({functionCode},{range})";

            return formula;
        }

        private static object GetSafePropertyValue(Type type, object value)
        {
            // Handle all kinds of data types
            if (type == typeof(DateTime))
            {
                if (value == null)
                {
                    return default(DateTime);
                }
                else if (value is double)
                {
                    return DateTime.FromOADate((double)value);
                }
                else if (value is string)
                {
                    return DateTime.Parse((string)value);
                }
                else
                {
                    return Convert.ToDateTime(value);
                }
            }
            else if (type == typeof(bool))
            {
                if (value is string && value.ToString().ToLower().Equals("yes"))
                {
                    return true;
                }
                else if (value is string && value.ToString().ToLower().Equals("no"))
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(value);
                }
            }
            else
            {
                var nonNullableType = Nullable.GetUnderlyingType(type) ?? type;
                var safeValue = (value == null) ? null : Convert.ChangeType(value, nonNullableType);

                return safeValue;
            }
        }
    }
}