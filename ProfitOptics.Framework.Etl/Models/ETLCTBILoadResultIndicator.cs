﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Models
{
    public class ETLCTBILoadResultIndicator
    {
        [JsonProperty("loadIndicatorId")]
        public long LoadIndicatorId { get; set; }

        [JsonProperty("indicatorType")]
        public int IndicatorType { get; set; }

        [JsonProperty("indicatorName")]
        public string IndicatorName { get; set; }

        [JsonProperty("passLabel")]
        public string PassLabel { get; set; }

        [JsonProperty("failLabel")]
        public string FailLabel { get; set; }

        [JsonProperty("incubationFlag")]
        public string IncubationFlag { get; set; }

        [JsonProperty("displayOrder")]
        public int DisplayOrder { get; set; }

        [JsonProperty("defaultProduct")]
        public string DefaultProduct { get; set; }

        [JsonProperty("sterilizerGroupId")]
        public string SterilizerGroupId { get; set; }

        [JsonProperty("indicatorProduct")]
        public string IndicatorProduct { get; set; }

        [JsonProperty("indicatorLot")]
        public string IndicatorLot { get; set; }

        [JsonProperty("resultFlag")]
        public string ResultFlag { get; set; }

        [JsonProperty("insertTimestamp")]
        public DateTime? InsertTimestamp { get; set; }

        [JsonProperty("startUser")]
        public string StartUser { get; set; }

        [JsonProperty("updateTimestamp")]
        public DateTime? UpdateTimestamp { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("exclude")]
        public bool Exclude { get; set; }
    }
}
