﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Models
{
    public class ETLCTCaseContainer
    {
        [JsonProperty("containerId")]
        public long? ContainerId { get; set; }

        [JsonProperty("containerTypeId")]
        public long? ContainerTypeId { get; set; }

        [JsonProperty("caseReference")]
        public string CaseReference { get; set; }

        [JsonProperty("caseCartId")]
        public long CaseCartId { get; set; }

        [JsonProperty("caseCartName")]
        public string CaseCartName { get; set; }

        [JsonProperty("dueMinutes")]
        public long DueMinutes { get; set; }

        [JsonProperty("caseTime")]
        public int? CaseTime { get; set; }

        [JsonProperty("dueTimestamp")]
        public DateTime DueTimestamp { get; set; }

        [JsonProperty("destinationName")]
        public string DestinationName { get; set; }

        [JsonProperty("endTimestamp")]
        public DateTime? EndTimestamp { get; set; }

        [JsonProperty("caseDoctor")]
        public string CaseDoctor { get; set; }

        [JsonProperty("containerName")]
        public string ContainerName { get; set; }

        [JsonProperty("itemName")]
        public string ItemName { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("scheduleStatusCode")]
        public string ScheduleStatusCode { get; set; }

        [JsonProperty("scheduleStatus")]
        public string ScheduleStatus { get; set; }

        [JsonProperty("standardTime")]
        public int? StandardTime { get; set; }

        [JsonProperty("expediteTime")]
        public int? ExpediteTime { get; set; }

        [JsonProperty("itemCode")]
        public string ItemCode { get; set; }

        [JsonProperty("itemType")]
        public string ItemType { get; set; }

        [JsonProperty("itemTypeName")]
        public string ItemTypeName { get; set; }

        [JsonProperty("statTime")]
        public int? StatTime { get; set; }

        [JsonProperty("multipleBasis")]
        public DateTime? MultipleBasis { get; set; }

        [JsonProperty("locationName")]
        public string LocationName { get; set; }

        [JsonProperty("updateTimestamp")]
        public DateTime? UpdateTimestamp { get; set; }

        [JsonProperty("scheduleCartChanged")]
        public string ScheduleCartChanged { get; set; }

        [JsonProperty("isModified")]
        public bool IsModified { get; set; }

        [JsonProperty("holdQuantity")]
        public string HoldQuantity { get; set; }

        [JsonProperty("itemQuantity")]
        public long ItemQuantity { get; set; }

        [JsonProperty("isCanceled")]
        public bool IsCanceled { get; set; }

        [JsonProperty("isCartComplete")]
        public bool IsCartComplete { get; set; }

        [JsonProperty("shipped")]
        public bool Shipped { get; set; }

        [JsonProperty("caseStatus")]
        public string CaseStatus { get; set; }

        [JsonProperty("standardTimeInHoursAndMinutes")]
        public string StandardTimeInHoursAndMinutes { get; set; }

        [JsonProperty("expediteTimeInHoursAndMinutes")]
        public string ExpediteTimeInHoursAndMinutes { get; set; }

        [JsonProperty("statTimeInHoursAndMinutes")]
        public string StatTimeInHoursAndMinutes { get; set; }

        [JsonProperty("dueInMinutes")]
        public decimal DueInMinutes { get; set; }

        [JsonProperty("dueInHoursAndMinutes")]
        public string DueInHoursAndMinutes { get; set; }

        [JsonProperty("processingFacilityId")]
        public long? ProcessingFacilityId { get; set; }

        [JsonProperty("locationFacilityId")]
        public long? LocationFacilityId { get; set; }

        [JsonProperty("caseItemNumber")]
        public long CaseItemNumber { get; set; }

        [JsonProperty("caseContainerStatus")]
        public long CaseContainerStatus { get; set; }

        [JsonProperty("currentLocationName")]
        public string CurrentLocationName { get; set; }

        [JsonProperty("serialNumber")]
        public string SerialNumber { get; set; }

        [JsonProperty("vendorName")]
        public string VendorName { get; set; }
    }
}
