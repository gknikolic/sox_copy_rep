﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack
{
    public class CensiTrackOptions
    {
        public string ClientId { get; set; }
        public string ContainerId { get; set; }
        public string FacilityId { get; set; }
        public string ServiceId { get; set; }
        public CTCasesContainersOption CTCasesContainersOption { get; set; }
    }
}
