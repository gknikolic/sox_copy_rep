USE [Sox]

CREATE TABLE [dbo].[CTAssemblyGraphic]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[ContainerTypeId] BIGINT NOT NULL,
	[CensisSetHistoryId] NVARCHAR(200) NULL,
	[LifecycleStatusTypeId] INT NOT NULL,
	[GraphicId] BIGINT NOT NULL,
	[Description] NVARCHAR(MAX) NULL,
	[GCImageId] INT NULL

CONSTRAINT [PK_CTAssemblyGraphic] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CTAssemblyGraphic]  WITH CHECK ADD  CONSTRAINT [FK_CTAssemblyGraphic_GCTraysLifecycleStatusTypes] FOREIGN KEY([LifecycleStatusTypeId])
REFERENCES [dbo].[GCTraysLifecycleStatusTypes] ([Id])
GO
ALTER TABLE [dbo].[CTAssemblyGraphic] CHECK CONSTRAINT [FK_CTAssemblyGraphic_GCTraysLifecycleStatusTypes]
GO

ALTER TABLE [dbo].[CTAssemblyGraphic]  WITH CHECK ADD  CONSTRAINT [FK_CTAssemblyGraphic_GCImage] FOREIGN KEY([GCImageId])
REFERENCES [dbo].[GCImage] ([Id])
GO
ALTER TABLE [dbo].[CTAssemblyGraphic] CHECK CONSTRAINT [FK_CTAssemblyGraphic_GCImage]
GO