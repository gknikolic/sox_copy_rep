﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Enums
{
    public enum QBBillingType
    {
        Tray = 1,
        Case = 2
    }
}
