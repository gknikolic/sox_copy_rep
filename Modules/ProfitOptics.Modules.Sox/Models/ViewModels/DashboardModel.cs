﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class DashboardModel
	{
		public List<CalendarCaseEvent> Events{ get; set; }
		public IEnumerable<SelectListItem> GCCustomers { get; set; }
		public IEnumerable<SelectListItem> GCVendors { get; set; }
		public string SelectedCustomers { get; set; }
		public string SelectedVendors { get; set; }
        public string CaseReference { get; set; }
        public string Surgeon { get; set; }
		public LatestETLLogsModel LatestETLLogs { get; set; }
		public ETLLogModel ETLLogWWOrder { get; set; }
		public ETLLogModel ETLLogWWRoute { get; set; }

		//public string UserId { get; set; }
		public ClaimsPrincipal User { get; set; }
		public bool GetForSpecificTimeframe { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public List<GoogleMapMarker> MapMarkers { get; set; }

		public DashboardModel()
		{
			Events = new List<CalendarCaseEvent>();
		}
	}


}
