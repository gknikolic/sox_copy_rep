﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Services.BusinessReview
{
    public interface IBusinessReviewDataService
    {
        /// <summary>
        /// Gets customer name, based on the given customerId and reportType value. 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="isBillTo"></param>
        /// <returns></returns>
        string GetCustomerName(int customerId, BusinessReviewReportType reportType);

        /// <summary>
        /// Gets report sales history data for specified customer identifier and report type.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="reportType"></param>
        /// <returns></returns>
        List<ReportSalesHistory> GetReportSalesHistoryData(int customerId, BusinessReviewReportType reportType);

    }
}
