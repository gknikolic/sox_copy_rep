﻿using CommandLine;
using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Etl.Helpers;
using ProfitOptics.Modules.Sox.Enums;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ProfitOptics.Framework.Etl.Modules
{
	[Verb(ProfitOptics.Modules.Sox.Helpers.Constants.ETL_MONNIT_MODULE_NAME, HelpText = "Module for iMonnit Integration.")]

	public class iMonnitModuleOptions : IOptions
	{
		[Option('a', "action", Required = true, HelpText = "Action type Enumerated value of type: IMActionType.")]
		public string Action { get; set; }
	}
	public class iMonnitModule : IModule
	{
		private readonly IEtlHelper _etlHelper;
        private readonly IMonnitClient _monnitClient;
        private readonly IMonnitService _monnitService;

        public iMonnitModule( IEtlHelper etlHelper, IMonnitClient monnitClient, IMonnitService monnitService)
		{
			_etlHelper = etlHelper;
            _monnitClient = monnitClient;
            _monnitService = monnitService;
		}

        public int Execute(IOptions opts)
        {
            //Log.Information("Started rendering template.");
            var options = opts as iMonnitModuleOptions;
            IMActionType actionType = (IMActionType)(int.Parse(options.Action));
            ETLLogs etlLog = new ETLLogs();
            etlLog.Source = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_MONNIT_MODULE_NAME;
            etlLog.FileName = "no file";
            etlLog.Username = "no username";
            etlLog.Status = ETLStatusType.OK.ToString();
            etlLog.Message = ProfitOptics.Modules.Sox.Helpers.Constants.ETL_SUCCESS_MESSAGE_TITLE;
            etlLog.RowCount = 0;
            etlLog.ActionType = (int)actionType;
            etlLog.Options = $@" Action: {options.Action};";
            etlLog.Description = string.Empty;

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                switch (actionType)
                {
                    case IMActionType.SyncSensors:
                        //_workWaveService.SyncOrdersWithWorkWaveAsync().GetAwaiter().GetResult();
                        _monnitService.SyncMonnitSensorsAsync().GetAwaiter().GetResult();
                        break;
                    //case WWActionType.SyncRoutes:
                    //    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                    //    DateTime cstTime = TimeZoneInfo.ConvertTime(DateTime.Now, cstZone);
                    //    _workWaveClient.SyncRoutesWithWorkWaveAsync(cstTime).GetAwaiter().GetResult();
                    //    _workWaveClient.SyncImagesWithWorkWave().GetAwaiter().GetResult();
                    //    break;
                    //case WWActionType.DeleteOldETLLogs:
                    //    _workWaveClient.DeleteOldEtlLogs();
                    //    break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                etlLog.Status = ETLStatusType.Error.ToString();
                etlLog.Message = ex.Message;
                etlLog.Description = ex.StackTrace;
            }
            finally
            {
                stopwatch.Stop();
                etlLog.Description += $" | Time elapsed: { stopwatch.Elapsed }";
                etlLog.ProcessingTime = DateTime.UtcNow;
                _etlHelper.LogProcessedAction(etlLog.Username, etlLog.Source, etlLog.FileName, etlLog.ProcessingTime.ToString(), etlLog.Status, etlLog.RowCount, etlLog.Message, etlLog.ActionType, etlLog.Options, etlLog.Description);
            }

            return 0;
        }
    }
}
