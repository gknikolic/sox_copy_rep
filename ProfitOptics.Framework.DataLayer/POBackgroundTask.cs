﻿using System;

namespace ProfitOptics.Framework.DataLayer
{
    public partial class POBackgroundTask
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
        public decimal Progress { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime? DateEnd { get; set; }

        public virtual AspNetUsers User { get; set; }
    }
}
