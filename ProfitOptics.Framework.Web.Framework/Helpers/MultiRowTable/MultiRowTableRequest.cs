﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Helpers.MultiRowTable
{
    public class MultiRowTableRequest
    {
        public string curSort1 { get; set; }
        public string curSort1Order { get; set; }
        public string curSort2 { get; set; }
        public string curSort2Order { get; set; }
        public int pageIndex { get; set; }
        public int length { get; set; }
        public string url { get; set; }
        public string Search { get; set; }
    }
}
