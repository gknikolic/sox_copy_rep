using System;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportUserFilter
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        
        public string Name { get; set; }

        public bool IsDefault { get; set; }

        public string Value { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
    }
}
