﻿using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ProfitOptics.Framework.Core.Data;
using ProfitOptics.Modules.Quotes.Data.Config;
using ProfitOptics.Modules.Quotes.Data.Domain;

namespace ProfitOptics.Modules.Quotes.Data
{
    public class Entities : DbContext
    {
        public Entities(DbContextOptions<Entities> options) : base(options)
        {
            var listener = this.GetService<DiagnosticSource>();
            (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor());
        }

        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<AspNetRole> AspNetRole { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<QTBillTo> QTBillToes { get; set; }
        public virtual DbSet<QTFedExRate> QTFedExRates { get; set; }
        public virtual DbSet<QTHierarchyCorporate> QTHierarchyCorporates { get; set; }
        public virtual DbSet<QTHierarchyRegion> QTHierarchyRegions { get; set; }
        public virtual DbSet<QTHierarchySalesRep> QTHierarchySalesReps { get; set; }
        public virtual DbSet<QTHierarchyZone> QTHierarchyZones { get; set; }
        public virtual DbSet<QTItem> QTItems { get; set; }
        public virtual DbSet<QTQuote> QTQuotes { get; set; }
        public virtual DbSet<QTQuoteItem> QTQuoteItems { get; set; }
        public virtual DbSet<QTShipTo> QTShipToes { get; set; }
        public virtual DbSet<QTUpsAdditionalSurcharge> QTUpsAdditionalSurcharges { get; set; }
        public virtual DbSet<QTUpsRate> QTUpsRates { get; set; }
        public virtual DbSet<QTUserMapping> QTUserMappings { get; set; }
        public virtual DbSet<QTUserProfile> QTUserProfiles { get; set; }
        public virtual DbSet<QTBillToPriceList> QTBillToPriceLists { get; set; }
        public virtual DbSet<QTCustomerPricing> QTCustomerPricings { get; set; }
        public virtual DbSet<QTInventory> QTInventories { get; set; }
        public virtual DbSet<QTSalesHistory> QTSalesHistories { get; set; }
        public virtual DbSet<QTShipToItemSummary> QTShipToItemSummaries { get; set; }
        public virtual DbSet<QTStandingOrdersSchedule> QTStandingOrdersSchedules { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new AspNetUserConfig());
            builder.ApplyConfiguration(new AspNetRoleConfig());
            builder.ApplyConfiguration(new AspNetUserRolesConfig());
            builder.ApplyConfiguration(new QTBillToConfig());
            builder.ApplyConfiguration(new QtFedexRateConfig());
            builder.ApplyConfiguration(new QTHierarchyCorporateConfig());
            builder.ApplyConfiguration(new QTHierarchyRegionConfig());
            builder.ApplyConfiguration(new QTHierarchySalesRepConfig());
            builder.ApplyConfiguration(new QTHierarchyZoneConfig());
            builder.ApplyConfiguration(new QTItemConfig());
            builder.ApplyConfiguration(new QTQuoteConfig());
            builder.ApplyConfiguration(new QTShipToConfig());
            builder.ApplyConfiguration(new QTUpsAdditionalSurchargeConfig());
            builder.ApplyConfiguration(new QTUpsRateConfig());
            builder.ApplyConfiguration(new QTUserMappingConfig());

            builder.ApplyConfiguration(new QTUserProfileConfig());
            builder.ApplyConfiguration(new QTBillToPriceListConfig());
            builder.ApplyConfiguration(new QTCustomerPricingConfig());
            builder.ApplyConfiguration(new QTInventoryConfig());
            builder.ApplyConfiguration(new QTSalesHistoryConfig());
            builder.ApplyConfiguration(new QTShipToItemSummaryConfig());
            builder.ApplyConfiguration(new QTStandingOrdersScheduleConfig());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}
