﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Modules.Reporting.Factories;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Components
{
    public class FilterWidgetViewComponent : ViewComponent
    {
        private readonly IReportFilterModelFactory _filterModelFactory;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public FilterWidgetViewComponent(IReportFilterModelFactory filterModelFactory, IHttpContextAccessor httpContextAccessor)
        {
            _filterModelFactory = filterModelFactory;
            _httpContextAccessor = httpContextAccessor;
        }

        public IViewComponentResult Invoke()
        {
            int userId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));

            FilterWidgetViewModel vm = _filterModelFactory.CreateFilterWidgetViewModel(userId);

            return View(vm);
        }
    }
}
