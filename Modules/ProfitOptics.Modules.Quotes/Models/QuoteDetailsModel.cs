﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ProfitOptics.Modules.Quotes.Models
{
    public class QuoteDetailsModel
    {
        public int QuoteId { get; set; }
        public string QuoteName { get; set; }
        public string QuoteDescription { get; set; }
        public DateTime? PresentedToCustomer { get; set; }
        public int? CustomerBillToId { get; set; }
        public string Status { get; set; }
        public DateTime? ValidThroughDate { get; set; }
        public string HeaderComments { get; set; }
        public string FooterComments { get; set; }
        public string ContactEmail { get; set; }
        public string CustomerReference { get; set; }
        public bool NewBusiness { get; set; }
        public DateTime? EstimatedFirstOrderDate { get; set; }
        public decimal? Probability { get; set; }
        public decimal? ShippingCost { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? AverageShippingCost { get; set; }
        public int? OriginZipCode { get; set; }
        public string ShipToNum { get; set; }
        //CustomerShipToId = p.CustomerShipToId,
        public string ShipToZipOverride { get; set; }
        public bool IsArchived { get; set; }
        public int? CustomerNum { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        //Notes = quote.Description,
        public int? ShipToId { get; set; }
        public decimal? TotAdjMarginPerc { get; set; }
        public decimal? TotFloorMarginPerc { get; set; }
        public string OrderFrequency { get; set; }
        public decimal? FreightCapAmount { get; set; }
        public string FreightPaymentType { get; set; }
        public string ShippingMethod { get; set; }

        public List<SelectListItem> ShipTos { get; set; }
        public PriceListCodeModel PriceListModel { get; set; }
        public StandingOrderListModel StandingOrders { get; set; }


        public bool ShipToIsOverridden => ShipToZipOverride != null;
    }
}