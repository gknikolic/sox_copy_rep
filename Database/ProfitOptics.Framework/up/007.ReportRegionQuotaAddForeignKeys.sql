USE [ProfitOptics.Framework]


DELETE FROM [ProfitOptics.Framework].[dbo].[ReportRegionQuota] 
WHERE RegionId NOT IN (SELECT Id FROM ReportHierarchyRegion)

DELETE FROM [ProfitOptics.Framework].[dbo].[ReportRegionQuota] 
WHERE SalesRepId NOT IN (SELECT Id FROM ReportHierarchySalesRep)

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ReportHierarchyRegion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ReportHierarchySalesRep SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO


IF (OBJECT_ID('dbo.FK_ReportRegionQuota_ReportHierarchySalesRep', 'F') IS NULL)
BEGIN
ALTER TABLE dbo.ReportRegionQuota ADD CONSTRAINT
	FK_ReportRegionQuota_ReportHierarchySalesRep FOREIGN KEY
	(
	SalesRepId
	) REFERENCES dbo.ReportHierarchySalesRep
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
END
IF (OBJECT_ID('dbo.FK_ReportRegionQuota_ReportHierarchyRegion', 'F') IS NULL)
BEGIN
ALTER TABLE dbo.ReportRegionQuota ADD CONSTRAINT
	FK_ReportRegionQuota_ReportHierarchyRegion FOREIGN KEY
	(
	RegionId
	) REFERENCES dbo.ReportHierarchyRegion
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
END
ALTER TABLE dbo.ReportRegionQuota SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
