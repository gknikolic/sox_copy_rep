﻿namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class BreadcrumbItem
    {
        public int Level { get; set; }
        public string Value { get; set; }
        public bool IsSelected { get; set; }

        public BreadcrumbItem(int level, string value, bool isSelected)
        {
            Level = level;
            Value = value;
            IsSelected = isSelected;
        }
    }
}