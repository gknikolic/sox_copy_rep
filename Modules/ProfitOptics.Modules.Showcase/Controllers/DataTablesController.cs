﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.Showcase.Factories;

namespace ProfitOptics.Modules.Showcase.Controllers
{
    [Authorize]
    [Area("Showcase")]
    public class DataTablesController : BaseController
    {
        private readonly IShowcaseFactory _showcaseFactory;

        public DataTablesController(IShowcaseFactory showcaseFactory)
        {
            _showcaseFactory = showcaseFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetDataTableExample([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request)
        {
            var data = _showcaseFactory.GetDataTablesExampleModels(request);

            return new LargeJsonResult(data.ToDataTablesResponse(request));
        }

        // TODO: add download action
    }
}