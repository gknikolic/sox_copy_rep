﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCCasesLifecycleStatusTypes
    {
        public int Id { get; set; }
        public int LifecycleStatusTypeId { get; set; }
        public int GCCaseId { get; set; }
        public DateTime Timestamp { get; set; }
        public bool IsActiveStatus { get; set; }
        public GCLifecycleStatusType LifecycleStatusType { get; set; }
        public GCCase GCCase { get; set; }
    }
}
