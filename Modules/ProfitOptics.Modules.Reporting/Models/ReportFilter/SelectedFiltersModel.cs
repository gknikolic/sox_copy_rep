﻿using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Models.Select2;

namespace ProfitOptics.Modules.Reporting.Models.ReportFilter
{
    public class SelectedFiltersModel
    {
        public IList<Select2Model> FilterZone { get; set; }
        public IList<Select2Model> FilterRegion { get; set; }
        public IList<Select2Model> FilterBuyGroup { get; set; }
        public IList<Select2Model> FilterVendor { get; set; }
        public IList<Select2Model> FilterCustomerClass { get; set; }
        public IList<Select2Model> FilterItemCategory { get; set; }
        public IList<Select2Model> FilterSalesRep { get; set; }
        public IList<Select2Model> FilterBillTo { get; set; }
        public IList<Select2Model> FilterShipTo { get; set; }
    }
}