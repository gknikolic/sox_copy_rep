﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTProductsResponse
    {
        public List<ETLCTProduct> Items { get; set; }

        public string NextPageLink { get; set; }
        public int? Count { get; set; }
    }
}
