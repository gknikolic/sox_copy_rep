﻿using Newtonsoft.Json;
using System;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTLocation
    {
        [JsonProperty("locationId")]
        public long LocationId { get; set; }

        [JsonProperty("areaId")]
        public long? AreaId { get; set; }

        [JsonProperty("buildingId")]
        public long? BuildingId { get; set; }

        [JsonProperty("facilityId")]
        public long FacilityId { get; set; }

        [JsonProperty("locationInventoryDate")]
        public DateTime? LocationInventoryDate { get; set; }

        [JsonProperty("locationInventoryUser")]
        public string LocationInventoryUser { get; set; }

        [JsonProperty("locationName")]
        public string LocationName { get; set; }

        [JsonProperty("locationType")]
        public string LocationType { get; set; }

        [JsonProperty("sterilizationMethod")]
        public string SterilizationMethod { get; set; }
    }
}
