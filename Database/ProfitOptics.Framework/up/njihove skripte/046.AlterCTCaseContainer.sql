USE [Sox]

BEGIN

	ALTER TABLE [Sox].[dbo].[CTCase] ADD Location NVARCHAR(200) NULL;
	ALTER TABLE [Sox].[dbo].[CTCase] ADD ProcessTypeId BIGINT NULL;
	ALTER TABLE [Sox].[dbo].[CTCase] ADD Status NVARCHAR(100) NULL;
	ALTER TABLE [Sox].[dbo].[CTCase] ADD EndTimestamp DATETIME NULL;
	ALTER TABLE [Sox].[dbo].[CTCase] ADD MultiCount BIT NULL;

	ALTER TABLE [Sox].[dbo].[CTCaseContainer] ADD CTContainerTypeId INT NULL;
END

ALTER TABLE [dbo].[CTCaseContainer]  WITH CHECK ADD  CONSTRAINT [FK_CTCaseContainer_CTContainerType] FOREIGN KEY([CTContainerTypeId])
REFERENCES [dbo].[CTContainerType] ([Id])
GO

ALTER TABLE [dbo].[CTCaseContainer] CHECK CONSTRAINT [FK_CTCaseContainer_CTContainerType]
GO