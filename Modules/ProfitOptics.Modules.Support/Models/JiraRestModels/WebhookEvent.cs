﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class WebhookEvent
    {
        public string timestamp { get; set; }

        public string webhookEvent { get; set; }

        public IssueAssignee user { get; set; }

        public IssuesResult issue { get; set; }

        public WebhookChangeLog changelog { get; set; }

        public WebhookComment comment { get; set; }
    }
}