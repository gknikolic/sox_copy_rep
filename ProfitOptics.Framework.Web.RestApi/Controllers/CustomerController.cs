using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.RestApi.Models;
using ProfitOptics.Framework.Web.RestApi.Models.Customer;
using ProfitOptics.Framework.Web.RestApi.Resources;
using ProfitOptics.Framework.Web.RestApi.Resources.Customer;
using ProfitOptics.Framework.Web.RestApi.Services.Customer;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApiControllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public CustomerController(ICustomerService customerService, IMapper mapper)
        {
            _customerService = customerService;
            _mapper = mapper;
        }

        /// <summary>
        /// Lists all categories.
        /// </summary>
        /// <returns>List os categories.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(QueryResultResource<CustomerResource>), 200)]
        public async Task<QueryResultResource<CustomerResource>> ListAsync([FromQuery] CustomerQueryResource query)
        {
            var customersQuery = _mapper.Map<CustomerQueryResource, CustomerQuery>(query);
            var queryResult = await _customerService.ListAsync(customersQuery);

            var resource = _mapper.Map<QueryResult<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer>, QueryResultResource<CustomerResource>>(queryResult);
            return resource;
        }

        /// <summary>
        /// Saves a new Customer.
        /// </summary>
        /// <param name="resource">Customer data.</param>
        /// <returns>Response for the request.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(CustomerResource), 201)]
        [ProducesResponseType(typeof(ErrorResource), 400)]
        public async Task<IActionResult> PostAsync([FromBody] SaveCustomerResource resource)
        {
            var customer = _mapper.Map<SaveCustomerResource, ProfitOptics.Framework.Web.RestApi.DataLayer.Customer>(resource);
            var result = await _customerService.SaveAsync(customer);

            if (!result.Success)
            {
                return BadRequest(new ErrorResource(result.Message));
            }

            var customerResource = _mapper.Map<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer, CustomerResource>(result.Resource);
            return Ok(customerResource);
        }

        /// <summary>
        /// Updates an existing customer according to an identifier.
        /// </summary>
        /// <param name="id">Customer identifier.</param>
        /// <param name="resource">Updated Customer data.</param>
        /// <returns>Response for the request.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(CustomerResource), 200)]
        [ProducesResponseType(typeof(ErrorResource), 400)]
        public async Task<IActionResult> PutAsync(int id, [FromBody] SaveCustomerResource resource)
        {
            var customer = _mapper.Map<SaveCustomerResource, ProfitOptics.Framework.Web.RestApi.DataLayer.Customer>(resource);
            var result = await _customerService.UpdateAsync(id, customer);

            if (!result.Success)
            {
                return BadRequest(new ErrorResource(result.Message));
            }

            var customerResource = _mapper.Map<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer, CustomerResource>(result.Resource);
            return Ok(customerResource);
        }

        /// <summary>
        /// Deletes a given customer according to an identifier.
        /// </summary>
        /// <param name="id">Customer identifier.</param>
        /// <returns>Response for the request.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(CustomerResource), 200)]
        [ProducesResponseType(typeof(ErrorResource), 400)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _customerService.DeleteAsync(id);

            if (!result.Success)
            {
                return BadRequest(new ErrorResource(result.Message));
            }

            var customerResource = _mapper.Map<ProfitOptics.Framework.Web.RestApi.DataLayer.Customer, CustomerResource>(result.Resource);
            return Ok(customerResource);
        }
    }
}