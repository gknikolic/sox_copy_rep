﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models
{
	public class OrderStep
	{
		[JsonProperty("location")]
		public Location Location { get; set; }

		[JsonProperty("timeWindows")]
		public List<TimeWindow> TimeWindows { get; set; }

		[JsonProperty("notes")]
		public string Notes { get; set; }

		[JsonProperty("serviceTimeSec")]
		public int ServiceTimeSec { get; set; }

		[JsonProperty("tagsIn")]
		public List<string> TagsIn { get; set; }


		[JsonProperty("tagsOut")]
		public List<string> TagsOut { get; set; }

		[JsonProperty("barcodes")]
		public List<string> Barcodes { get; set; }
		[JsonProperty("customFields")]
		public Dictionary<string,string> CustomFields { get; set; }

	}
}
