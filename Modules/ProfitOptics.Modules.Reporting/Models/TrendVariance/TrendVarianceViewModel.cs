﻿namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVarianceViewModel
    {
        public bool IsCorporate { get; set; }

        public string StartDateDefault { get; set; }

        public string EndDateDefault { get; set; }
    }
}
