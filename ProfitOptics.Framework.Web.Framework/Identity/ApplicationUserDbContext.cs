﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;

namespace ProfitOptics.Framework.Web.Framework.Identity
{
    public class ApplicationUserDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, IdentityUserClaim<int>, ApplicationUserRole, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        public virtual DbSet<POUserProfilePicture> POUserProfilePictures { get; set; }
        public virtual DbSet<POUserProfile> POUserProfiles { get; set; }
        public virtual DbSet<POUserHierarchy> POUserHierarchies { get; set; }

        public virtual DbSet<GCParentCompany> GCParentCompany { get; set; }
        public virtual DbSet<GCChildCompany> GCChildCompany { get; set; }
        public virtual DbSet<GCChildCompanyUsers> GCChildCompanyUsers { get; set; }
        public virtual DbSet<GCParentCompanyUsers> GCParentCompanyUsers { get; set; }

        public ApplicationUserDbContext()
        {

        }

        public ApplicationUserDbContext(DbContextOptions<ApplicationUserDbContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        { 
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>(user =>
            {
                user.Property(x => x.StartTime).IsRequired();

                user.Ignore(x => x.FullName);

                user.HasOne(u => u.ProfilePicture).WithOne(pp => pp.User).HasForeignKey<POUserProfilePicture>(s => s.UserId).OnDelete(DeleteBehavior.ClientSetNull);
            });

            builder.Entity<POUserProfilePicture>(profilePicture =>
            {
                profilePicture.HasKey(e => e.UserId);

                profilePicture.ToTable("POUserProfilePicture");

                profilePicture.Property(e => e.UserId).ValueGeneratedNever();

                profilePicture.Property(e => e.Data).IsRequired();
            });

            builder.Entity<POUserProfilePicture>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("POUserProfilePicture");

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.Data).IsRequired();
            });

            builder.Entity<POUserHierarchy>(entity =>
            {
                entity.ToTable("POUserHierarchy");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.ParentId);
                entity.Property(e => e.ChildId);

                entity.HasOne(e => e.ChildUser).WithMany(p => p.POChildUserHierarchies)
                    .HasForeignKey(e => e.ChildId);

                entity.HasOne(e => e.ParentUser).WithMany(p => p.POParentUserHierarchies)
                    .HasForeignKey(e => e.ParentId);
            });

            builder.Entity<POUserProfile>(entity =>
            {
                entity.ToTable("POUserProfile");
                entity.HasKey(user => user.Id);

                entity.Property(user => user.Title).HasMaxLength(50);
                entity.Property(user => user.Id).ValueGeneratedNever();

                entity.HasOne(user => user.AspNetUser).WithOne(p => p.POUserProfile)
                    .HasForeignKey<POUserProfile>(user => user.Id);
            });

            builder.Entity<ApplicationUserRole>(entity =>
            {
                entity.ToTable("AspNetUserRoles");
                //entity.HasKey(ur => new {ur.UserId, ur.RoleId});

                entity.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                entity.HasOne(ur => ur.User)
                    .WithMany(u => u.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });
        }
    }
}
