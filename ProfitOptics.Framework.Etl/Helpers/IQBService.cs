﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public interface IQBService
    {
        string PostGCTraysToQBEstimates(string userName);
    }
}
