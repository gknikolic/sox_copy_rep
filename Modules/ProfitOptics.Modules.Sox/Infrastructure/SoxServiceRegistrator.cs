﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Sox.Data;
using ProfitOptics.Modules.Sox.Factories;
using ProfitOptics.Modules.Sox.Infrastructure.Clients;
using ProfitOptics.Modules.Sox.Services;

namespace ProfitOptics.Modules.Sox.Infrastructure
{
	public sealed class SoxServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            //services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));
            services.AddDbContext<Framework.DataLayer.Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<ISoxService, SoxService>();
            services.AddScoped<IWorkWaveService, WorkWaveService>(); 
            services.AddScoped<ISoxFactory, SoxFactory>();
            services.AddScoped<IHelperService, HelperService>();
            services.AddScoped<IWorkWaveClient, WorkWaveClient>();
            services.AddScoped<IQuickBooksClient, QuickBooksClient>();
            services.AddScoped<IGroundControlService, GroundControlService>();
            services.AddScoped<ICaseManagementClient, CaseManagementClient>();
            services.AddSingleton<ISoxLogger, SoxLogger>();
            services.AddScoped<IReportingService, ReportingService>();
            //services.AddScoped<IUserService, UserService>();
        }
    }
}
