﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class PhotoCaptureModel
    {
        public bool IsYes { get; set; }
        public string Code { get; set; }
        public int TrayId { get; set; }
        public List<TrayImage> TrayImages { get; set; }
    }
}
