using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTBillTo")]
    public partial class QTBillTo
    {
        public int Id { get; set; }

        public string Company { get; set; }

        public int? BillToNum { get; set; }

        public string BillToName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string CustomerId { get; set; }
    }
}
