﻿using System;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVarianceShipToCustomerDrillThroughViewModel : TrendVarianceCustomerDrillThroughViewModel
    {
        private readonly IUrlHelper _urlHelper;

        public TrendVarianceShipToCustomerDrillThroughViewModel(int customerId,
            string customerName,
            string customerNum,
            ComparisonType comparisonType,
            DateTime? comparisonStartDate,
            DateTime? comparisonEndDate,
            bool isCorporate,
            IUrlHelper urlHelper)
            : base(customerId,
                customerName,
                customerNum,
                comparisonType,
                comparisonStartDate,
                comparisonEndDate,
                isCorporate)
        {
            _urlHelper = urlHelper;
        }

        public override bool IsBillTo => false;

        public override string DataUrl => _urlHelper.Action("GetItemCategoryVariancesForShipTo", "TrendVariance",
            new { area = ReportingSettings.AreaName });
    }
}