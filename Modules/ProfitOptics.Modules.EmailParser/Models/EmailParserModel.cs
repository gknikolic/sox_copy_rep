﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace ProfitOptics.Modules.EmailParser.Models
{
    public class InboundEmailInputModel
    {
        public string Headers { get; set; }
        public string Text { get; set; }
        public string Html { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public List<string> Cc { get; set; }
        public string Subject { get; set; }
        public string Dkim { get; set; }
        public string Spf { get; set; }
        public InboundEmailEnvelopeInputModel Envelope { get; set; }
        public InboundEmailCharsetsInputModel Charsets { get; set; }
        public float SpamScore { get; set; }
        public string SpamReport { get; set; }
        public int Attachments { get; set; }
        public dynamic AttachmentInfo { get; set; }
        public IFormFileCollection AttachmentFiles { get; set; }
        public string SenderIp { get; set; }
    }

    public class InboundEmailEnvelopeInputModel
    {
        public string From { get; set; }
        public List<string> To { get; set; }
    }

    public class InboundEmailCharsetsInputModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Html { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
    }

    public static class InboundEmailExtractor
    {
        public static InboundEmailInputModel ExtractInboundEmail(HttpRequest request)
        {
            var inboundEmail = new InboundEmailInputModel
            {
                Headers = request.Form["headers"],
                Text = request.Form["text"],
                Html = request.Form["html"],
                From = request.Form["from"],
                To = request.Form["to"],
                Cc = request.Form["to"].ToString().Split(',').ToList(),
                Subject = request.Form["subject"],
                Dkim = request.Form["dkim"],
                Spf = request.Form["spf"]
            };

            string envelopeRaw = request.Form["envelope"];
            if (!string.IsNullOrEmpty(envelopeRaw))
            {
                inboundEmail.Envelope = JsonConvert.DeserializeObject<InboundEmailEnvelopeInputModel>(envelopeRaw);
            }

            string charsetsRaw = request.Form["charsets"];
            if (!string.IsNullOrEmpty(charsetsRaw))
            {
                inboundEmail.Charsets = JsonConvert.DeserializeObject<InboundEmailCharsetsInputModel>(charsetsRaw);
            }

            string spamScoreRaw = request.Form["spam_score"];
            if (!string.IsNullOrEmpty(spamScoreRaw))
            {
                inboundEmail.SpamScore = float.Parse(spamScoreRaw, CultureInfo.InvariantCulture.NumberFormat);
            }

            inboundEmail.SpamReport = request.Form["spam_report"];

            string attachmentsRaw = request.Form["attachments"];
            if (!string.IsNullOrEmpty(attachmentsRaw))
            {
                inboundEmail.Attachments = Convert.ToInt32(attachmentsRaw);
            }

            string attachmentInfoRaw = request.Form["attachment-info"];
            if (!string.IsNullOrEmpty(attachmentInfoRaw))
            {
                inboundEmail.AttachmentInfo = JsonConvert.DeserializeObject(attachmentInfoRaw);
            }

            inboundEmail.SenderIp = request.Form["sender_ip"];
            inboundEmail.AttachmentFiles = request.Form.Files;

            return inboundEmail;
        }
    }
}
