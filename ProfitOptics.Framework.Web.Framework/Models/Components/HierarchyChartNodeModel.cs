﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Framework.Models.Components
{
    public class HierarchyChartNodeModel
    {
        public string EntityId { get; set; }
        public string ParentId { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        //public string About { get; set; }
        public List<HierarchyChartNodeModel> children { get; set; }

        public string Image { get; set; }
        public DateTime InsertDate { get; set; }

        public HierarchyChartNodeModel()
        {
            this.children = new List<HierarchyChartNodeModel>();
        }
    }
}