﻿using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Framework.Infrastructure
{
    /// <summary>
    /// Represents the menu fragment factory interface.
    /// </summary>
    public interface IMenuFragmentRendererFactory
    {
        /// <summary>
        /// Creates the menu fragment factory instances.
        /// </summary>
        /// <returns>The enumerable collection of menu fragment renderers.</returns>
        IEnumerable<IMenuFragmentRenderer> CreateMenuFragmentRenderers();
    }
}
