﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTQualityFeedItemGraphic
    {
        public int Id { get; set; }
        public int CTQualityFeedItemId { get; set; }
        public int? GCImageId { get; set; }
        public string Description { get; set; }
        public long ReferenceId { get; set; }
        public long GraphicId { get; set; }
        public bool IsGlobal { get; set; }
        public long LoadId { get; set; }
        public DateTime? UpdateTimestamp { get; set; }
        public long UpdateUserId { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }

        public virtual GCImage GCImage { get; set; }
    }
}
