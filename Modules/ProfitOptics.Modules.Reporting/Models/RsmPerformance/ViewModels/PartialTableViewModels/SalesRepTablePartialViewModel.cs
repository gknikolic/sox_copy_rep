﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.PartialTableViewModels
{
    public class SalesRepTablePartialViewModel<T>
    {
        public SalesRepTablePartialViewModel()
        {
            Items = new List<T>();
        }

        public int SalesRepId { get; set; }
        public string SalesRepName { get; set; }
        public List<T> Items { get; set; }
    }
}