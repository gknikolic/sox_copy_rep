﻿using System.Collections.Generic;
using System.Linq;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Models
{
    public class ApplicationUserListModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsEnabled { get; set; }

        public IList<string> Roles { get; set; }
        public string RolesString
        {
            get
            {
                return this.Roles != null && this.Roles.Any() ? this.Roles.Select(q => q).Aggregate((pr, nx) => pr + ", " + nx) : "N/A";
            }
        }
        public bool IsLocked { get; set; }
    }
}
