USE [Sox]

BEGIN

	ALTER TABLE [Sox].[dbo].[GCTraysLifecycleStatusTypes] DROP COLUMN IsLocked;
	ALTER TABLE [Sox].[dbo].[GCTrayColorStatus] ADD IsLocked BIT NULL;
	
END
