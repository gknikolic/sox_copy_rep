USE [Sox]

BEGIN

	ALTER TABLE [Sox].[dbo].[CTCaseContainer] ALTER COLUMN ContainerId BIGINT NULL;
	
END
