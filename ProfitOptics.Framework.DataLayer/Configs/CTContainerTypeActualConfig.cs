﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTContainerTypeActualConfig : IEntityTypeConfiguration<CTContainerTypeActual>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTContainerTypeActual> builder)
        {
            builder.ToTable("CTContainerTypeActual");
            builder.HasKey(e => e.Id);

            //builder.HasOne(d => d.CTContainerType)
            //    .WithMany(p => p.CTContainerTypea)
            //    .HasForeignKey(d => d.CTConainerServiceId)
            //    .HasConstraintName("FK_CTContainerType_CTContainerService");
        }
    }
}
