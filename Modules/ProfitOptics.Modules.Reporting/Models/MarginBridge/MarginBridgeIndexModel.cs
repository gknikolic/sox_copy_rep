﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{

    // TODO [Aca]: Place this on appropriate location, and create separate models for each step.
    public class PricingLevelsModel
    {
        public bool IncludeOverrideSales { get; set; }
        public bool IncludeCustomerContractSales { get; set; }
        public bool IncludeCustomerExceptionSales { get; set; }
        public bool IncludePromotionSales { get; set; }
        public bool IncludeMatrixSales { get; set; }
        public bool IncludeOtherSales { get; set; }
    }

    public class MarginBridgeIndexModel
    {
        public List<ProductHierarchyFilterModel> Products { get; set; }

        public List<LocationHierarchyFilterModel> Locations { get; set; }

        public string PeriodId { get; set; }

        public MarginBridgePeriod FirstPeriod { get; set; }

        public MarginBridgePeriod SecondPeriod { get; set; }

        public IEnumerable<MarginBridgePeriodRange> PeriodRanges { get; set; }

        public LocationLevelType LocationLevel { get; set; }

        public ProductLevelType ProductLevel { get; set; }

        public List<List<string>> CheckedNodes { get; set; }

        public bool IncludeNLItems { get; set; }

        public bool IncludeSDItems { get; set; }

        public bool IncludeListedItems { get; set; }

        public PricingLevelsModel PricingLevels { get; set; }

        public MarginBridgeType MarginBridgeType { get; set; }

        public List<HierarchySelectionLevel> LocationSelection { get; set; }

        public List<HierarchySelectionLevel> ProductSelection { get; set; }

        public MarginBridgeStatisticsModel StatisticsModel { get; set; }
    }
}