﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class InvoiceResponse
    {
        [JsonProperty("Invoice")]
        public List<Invoice> Invoices { get; set; }
    }
}
