﻿using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.TokenizationSearch;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Modules.CrossReference.Data.Domain;
using ProfitOptics.Modules.CrossReference.Models;
using ProfitOptics.Modules.CrossReference.Services;

namespace ProfitOptics.Modules.CrossReference.Factories
{
    public class CrossReferenceFactory : ICrossReferenceFactory
    {
        private readonly ICrossReferenceService _crossReferenceService;

        public CrossReferenceFactory(ICrossReferenceService crossReferenceService)
        {
            _crossReferenceService = crossReferenceService;
        }

        public CrossReferenceIndexModel PrepareIndexModel()
        {
            var result = new CrossReferenceIndexModel
            {
                Competitors = GetCompetitorModels(true),
                CompatibilityOptions = GetCompatibilityOptionModels(true)
            };

            return result;
        }

        public List<CompatibilityOptionModel> GetCompatibilityOptionModels(bool getDeleted = false)
        {
            List<CompatibilityOptionModel> models = _crossReferenceService.GetCompatibilityOptions(getDeleted)
                .Select(CreateCompatibilityOptionModelPredicate).ToList();

            return models;
        }

        public List<CompetitorModel> GetCompetitorModels(bool getDeleted = false)
        {
            List<CompetitorModel> models = _crossReferenceService.GetCompetitors(getDeleted)
                .Select(CreateCompetitorModelPredicate).ToList();

            return models;
        }

        public List<ItemLookupSelect2DataModel> GetItemAutocompleteData(string term)
        {
            List<ItemLookupSelect2DataModel> record = _crossReferenceService.GetQtItemsForAutoComplete(term)
                .Select(CreateSelect2DataModelPredicate).ToList();

            return record;
        }

        public PagedList<CrossReferenceRecordModel> GetCrossReferenceRecordModelsForApproval(IDataTablesRequest request, int userId)
        {
            IQueryable<CrossReferenceRecordModel> records = _crossReferenceService.GetCrossReferenceRecordsForApproval(userId)
                .Select(CreateCrossReferenceRecordModelPredicate).AsQueryable();

            PagedList<CrossReferenceRecordModel> pagedList = ProcessRequest(request, records);

            return pagedList;
        }

        public PagedList<CrossReferenceRecordModel> GetCrossReferenceRecordModels(IDataTablesRequest request, int? competitorId,
                int? compatibilityId, string competitorItem, int userId)
        {
            IQueryable<CrossReferenceRecordModel> records = _crossReferenceService.GetCrossReferenceRecords(competitorId, compatibilityId, competitorItem, userId)
                .Select(CreateCrossReferenceRecordModelPredicate).AsQueryable();

            PagedList<CrossReferenceRecordModel> pagedList = ProcessRequest(request, records);

            return pagedList;
        }

        private static CompatibilityOptionModel CreateCompatibilityOptionModelPredicate(
            CrtCompatibilityOption crtOption)
        {
            return new CompatibilityOptionModel
            {
                Id = crtOption.Id,
                Name = crtOption.CompatibilityOptionName,
                Deleted = crtOption.EndDate != null
            };
        }

        private static CompetitorModel CreateCompetitorModelPredicate(CrtCompetitor crtCompetitor)
        {
            return new CompetitorModel
            {
                Id = crtCompetitor.Id,
                Name = crtCompetitor.CompetitorName,
                Deleted = crtCompetitor.EndDate != null
            };
        }

        public List<CrossReferenceExportModel> GetCrossReferenceExportModelList(List<int?> crossReferenceIds)
        {
            List<CrossReferenceExportModel> records = _crossReferenceService.GetCrossReferenceExportModelList(crossReferenceIds)
                .Select(CreateExportModelPredicate).ToList();

            return records;
        }

        private static CrossReferenceExportModel CreateExportModelPredicate(CrtCrossReferenceRecord crtRecord)
        {
            return new CrossReferenceExportModel
            {
                ClientItemNumber = crtRecord.QTItem?.ItemNum ?? string.Empty,
                ClientItemDescription = crtRecord.QTItem?.ItemDescription ?? string.Empty,
                CompetitorName = crtRecord.CrtCompetitor?.CompetitorName ?? string.Empty,
                CompetitorItemNumber = crtRecord.CompetitorItemNumber,
                CompetitorItemDescription = crtRecord.CompetitorItemDescription,
                Compatibility = crtRecord.CrtCompatibilityOption?.CompatibilityOptionName ?? string.Empty
            };
        }

        private CrossReferenceRecordModel CreateCrossReferenceRecordModelPredicate(CrtCrossReferenceRecord crtRecord)
        {
            return new CrossReferenceRecordModel
            {
                Id = crtRecord.Id,
                SubmittedById = crtRecord.SubmittedBy,
                SubmittedByName = crtRecord.AspNetUser.FirstName + " " + crtRecord.AspNetUser.LastName,
                ItemId = crtRecord.QTItem.Id,
                ItemNumber = crtRecord.QTItem.ItemNum,
                ItemDescription = crtRecord.QTItem.ItemDescription,
                ItemUom = crtRecord.QTItem.SellingUOM,
                CompetitorId = crtRecord.CrtCompetitor.Id,
                CompetitorName = crtRecord.CrtCompetitor.CompetitorName,
                CompatibilityOptionId = crtRecord.CrtCompatibilityOption.Id,
                CompetitorItemNumber = crtRecord.CompetitorItemNumber,
                CompetitorItemDescription = crtRecord.CompetitorItemDescription,
                CompetitorWebsiteUrl = crtRecord.CompetitorWebsiteUrl,
                CompatibilityOptionName = crtRecord.CrtCompatibilityOption.CompatibilityOptionName,
                AlternativeItemNumber = "",
                ThumbsDownCount = _crossReferenceService.GetThumbsDownCountForCrRecord(crtRecord.Id),
                ThumbsUpCount = _crossReferenceService.GetThumbsUpCountForCrRecord(crtRecord.Id),
                StartDate = crtRecord.StartDate,
                Notes = crtRecord.Notes
            };
        }

        private static ItemLookupSelect2DataModel CreateSelect2DataModelPredicate(QTItem qtItem)
        {
            return new ItemLookupSelect2DataModel
            {
                id = qtItem.Id + "",
                itemDescription = qtItem.ItemDescription,
                alternateItemNumberMedco = "",
                mpn = "",
                slug = qtItem.ItemNum,
                text = qtItem.ItemNum
            };
        }

        private PagedList<CrossReferenceRecordModel> ProcessRequest(IDataTablesRequest request, IQueryable<CrossReferenceRecordModel> records)
        {
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                string[] strings = Tokenizer.TokenizeToWords(request.Search.Value);

                records = strings.Where(str => !string.IsNullOrEmpty(str))
                    .Aggregate(records, (current, str) =>
                        current.Where(x => x.CompatibilityOptionName.Contains(str) ||
                                           x.CompetitorItemDescription.Contains(str) ||
                                           x.CompetitorItemNumber.Contains(str) ||
                                           x.CompetitorName.Contains(str) ||
                                           x.ItemDescription.Contains(str) ||
                                           x.ItemUom.Contains(str)));
            }

            records = records.ApplySort(request);

            List<CrossReferenceRecordModel> paged = records.ApplyPagination(request).ToList();

            int totalCount = records.Count();

            return new PagedList<CrossReferenceRecordModel>(paged, totalCount);
        }
    }
}