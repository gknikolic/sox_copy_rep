﻿namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public enum RouteStepStatusType
	{
		undefined = -1,
		reschedule,
		done
	}
}
