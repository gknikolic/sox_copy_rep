﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

let HierarchyChart = (function () {
    //globals
    var self = this;
    var root;
    var hierarchyChartModel;
    var preventNodeClick = false;

    var i = 0;
    let foundNodes = [] //used in chart search

    let settings = {
        nodeCount: 0,
        css: {
            box: {
                width: 250,
                height: 150,
                round: 3
            },
            margin: {
                top: 20,
                right: 120,
                bottom: 20,
                left: 120
            },
            button: {
                height: 30,
                maskHeight: 35
            }
        },
        userSearchInputIdSelector: "#hierarchy-user-search",
        timeoutDuration: 750
    };

    let dimensions = {
        width: 1920 - settings.css.margin.right - settings.css.margin.left,
        height: 1240 - settings.css.margin.top - settings.css.margin.bottom
    };

    let node = (function() {
        // Public functions
        let getNewNode = function () {
            settings.nodeCount++;
            return {
                "EntityId": ++i,
                "Title": "newNode" + settings.nodeCount,
                "Subtitle": "Son of A",
                "Image": "/AdminLTE/plugins/hierarchyChart/img/user.png",
                "children": []
            };
        };

        let addNode = function (parentEntityId, newNode) {
            insertNode(root, parentEntityId, newNode);
        };

        let removeNode = function (nodeEntityId) {
            deleteNode(root, nodeEntityId);
        };

        let editNode = function (nodeEntityId, newTitle, newSubtitle, newImageUrl) {
            updateNode(root, nodeEntityId, newTitle, newSubtitle, newImageUrl);
        };
        // End Public functions

        let insertNode = function (currentNode, parentEntityId, newNode) {
            if (currentNode.EntityId == parentEntityId) {
                // Check if this node is collapsed. If it is, expand it and then add new node.
                if (currentNode.children == null && currentNode._children != null) {
                    userInterface.Toggle(currentNode);
                }

                if (!currentNode.children) {
                    // create empty array if it is undefined
                    currentNode.children = [];
                }

                // add newNode as last child of parent
                currentNode.children.splice(currentNode.children.length, 0, newNode);
                userInterface.Update(currentNode);
                return;
            }

            if (currentNode.children) {
                for (var i = 0, length = currentNode.children.length; i < length; i++) {
                    insertNode(currentNode.children[i], parentEntityId, newNode);
                }
            }
        };

        let deleteNode = function (parent, nodeEntityId) {
            if (!parent.children)
                return;

            for (var i = parent.children.length - 1; i >= 0; i--) {
                if (parent.children[i].EntityId == nodeEntityId) {
                    parent.children.splice(i, 1);
                    userInterface.Update(parent);
                }
                else {
                    deleteNode(parent.children[i], nodeEntityId);
                }
            }
        };

        let updateNode = function (parent, nodeEntityId, newTitle, newSubtitle, newImageUrl) {
            if (!parent.children)
                return;

            if (parent.EntityId == nodeEntityId) {
                parent.Title = newTitle;
                parent.Subtitle = newSubtitle;
                parent.Image = newImageUrl;
                userInterface.Update(parent);
            }

            for (var i = parent.children.length - 1; i >= 0; i--) {
                if (parent.children[i].EntityId == nodeEntityId) {
                    parent.children[i].Title = newTitle;
                    parent.children[i].Subtitle = newSubtitle;
                    parent.children[i].Image = newImageUrl;
                    userInterface.Update(parent);
                }
                else {
                    deleteNode(parent.children[i], nodeEntityId);
                }
            }
        }

        return {
            AddNode: addNode,
            RemoveNode: removeNode,
            NewNode: getNewNode,
            EditNode: editNode
        };
    })();

    let nodeEventHandlers = (function() {
        let leftNodeButtonClick = function (d) {
            console.log("hierarchyChartModel", hierarchyChartModel)
            preventNodeClick = true;
            window[hierarchyChartModel.OnNodeButton1Callback](d);
        };

        let middleNodeButtonClick = function (d) {
            console.log("hierarchyChartModel", hierarchyChartModel)
            preventNodeClick = true;
            window[hierarchyChartModel.OnNodeButton2Callback](d);
        };

        let rightNodeButtonClick = function (d) {
            console.log("hierarchyChartModel", hierarchyChartModel)
            preventNodeClick = true;
            window[hierarchyChartModel.OnNodeButton3Callback](d);
        };
        return {
            LeftNodeButtonClick: leftNodeButtonClick,
            MiddleNodeButtonClick: middleNodeButtonClick,
            RightNodeButtonClick: rightNodeButtonClick
        };
    })();

    let userInterface = (function() {
        // search control
        let setupSearchControl = function () {
            $(settings.userSearchInputIdSelector).select2({
                placeholder: "Search Users"
            });
            $(settings.userSearchInputIdSelector).on('select2:open', function () {
                $(settings.userSearchInputIdSelector).val('').trigger("change");
            });
            $(settings.userSearchInputIdSelector).on('select2:select', function () {
                var currentValue = $(settings.userSearchInputIdSelector).val();

                var nodes = d3Config.Tree.nodes(root).reverse();
                var entity = $.grep(nodes, function (n, i) {
                    return n.EntityId == 0; //0 is EntityId of hardcoded first element
                })[0];

                foundNodes = [];
                searchNodes(entity, currentValue);
                
                if (foundNodes.length > 0) {
                    $.each(foundNodes, function (index) {
                        if (typeof foundNodes[index] != "undefined") {
                            foundNodes[index].selected = true;
                            expandToNode(foundNodes[index]);
                        }
                    });

                    update(nodes);

                    $.each(foundNodes, function (index) {
                        if (typeof foundNodes[index] != "undefined") {
                            foundNodes[index].selected = false;
                        }
                    });
                };

            });
        }

        function expandToNode(foundNode) {
            var parent = foundNode.parent;
            if (parent) {
                var shownChild = false;
                var hiddenChild = false;

                if (parent.children) {
                    shownChild = typeof $.grep(parent.children, function (n, i) {
                        return n.EntityId == foundNode.EntityId;
                    })[0] != "undefined";
                }
                if (parent._children) {
                    hiddenChild = typeof $.grep(parent._children, function (n, i) {
                        return n.EntityId == foundNode.EntityId;
                    })[0] != "undefined";
                }

                if (hiddenChild) {
                    if (!parent.children) {
                        parent.children = [];
                    }
                    parent.children.push(foundNode);
                    parent._children = parent._children.filter(function (obj) {
                        return obj.EntityId !== foundNode.EntityId;
                    });
                    expandToNode(parent);
                }

                if (shownChild) {
                    expandToNode(parent);
                }
            }
        }

        function searchNodes(node, currentValue) {
            currentValue = currentValue + "";
            if (node.children) {
                $.each(node.children, function (index) {
                    if (node.children[index].EntityId == currentValue) {
                        foundNodes.push(node.children[index]);
                    } else {
                        foundNode = searchNodes(node.children[index], currentValue);
                        if (typeof foundNode != "undefined" && foundNode != null && foundNode.length > 0) {
                            foundNodes.push(foundNode);
                        }
                    }
                });
            }
            if (node._children) {
                $.each(node._children, function (index) {
                    if (node._children[index].EntityId == currentValue) {
                        foundNodes.push(node._children[index]);
                    } else {
                        foundNode = searchNodes(node._children[index], currentValue);
                        if (typeof foundNode != "undefined" && foundNode != null && foundNode.length > 0) {
                            foundNodes.push(foundNode);
                        }
                    }
                });
            }
            return;
        }

        // control buttons
        var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        let btnHeight = (settings.css.box.height - 20 + settings.css.box.height) / 2;
        if (isChrome)
            btnHeight = (settings.css.box.height - 30 + settings.css.box.height) / 2;


        let drawNodeElements = function (node, redraw) {
            if (redraw) {
                $("text[data-title]").remove();
                $("text[data-subtitle]").remove();
                $("text[data-childrentag]").remove();
                $("text[data-nodeimage]").remove();
            }

            //node.createTitle(15, 30, "black", function (d) {
            //    return d.Title;
            //}).attr("class", "styleTextclass");

            //node.createName(80, 65, "#7a7a7a", function (d) {
            //    return d.Name;
            //}).attr("class", "styleSmallTextclass");

            node.createTitle(80, 65, "black", function (d) {
                return d.Title;
            }).attr("class", "styleSmallTextclass");

            node.createName(15, 30, "black", function (d) {
                return d.Name;
            }).attr("class", "styleTextclass");

            node.createNodeImage(15, 50, 50, 50, function (d) {
                return d.Image;
            });

            node.createCustomRectangle(80, 80, 35, 20, 4, "userTag");

            node.createImage(82, 83, 15, 15, "/AdminLTE/plugins/hierarchyChart/img/userTag.svg");

            node.createChildrenTag(100, 95, "white", function (d) {
                if (d.children) {
                    if (d._children) {
                        return d.children.length + d._children.length
                    };
                    return d.children.length;
                } else if (d._children) {
                    if (d.children) {
                        return d.children.length + d._children.length
                    };
                    return d._children.length;
                }
                return 0;
            });
        }

        let update = function (source) {
            initPrototypes();

            // Compute the new tree layout.
            let nodes = d3Config.Tree.nodes(root).reverse(),
                links = d3Config.Tree.links(nodes);

            // Normalize for fixed-depth.
            nodes.forEach(function (d) {
                d.y = d.depth * 180;
            });
            // Update the nodes…
            var node = d3Config.Svg.selectAll("g.node")
                .data(nodes, function (d) {
                    return d.id || (d.id = ++i);
                });
            // Update current node
            drawNodeElements(node, true);

            // Enter any new nodes at the parent's previous position.
            // Starting position for animation
            var nodeEnter = node.enter().append("g")
                .attr("class", function (d) {
                    return "node " + (d.selected ? "selectedNode" : "")
                })
                .attr("transform", function (d) {
                    return "translate(" + source.x0 + "," + (source.y0 + 30) + ")";
                })
                .on('click', click);

            // top level rect
            var topRect = nodeEnter.createCustomRectangle(0, 0, settings.css.box.width, settings.css.box.height, settings.css.box.round, "nodeBox");

            drawNodeElements(nodeEnter, false);

            var mask = nodeEnter.append("mask")
                .attr("id", "buttons");
            mask.createCustomRectangle(0, settings.css.box.height - settings.css.button.maskHeight, settings.css.box.width, settings.css.button.maskHeight, 10, "rectMask");


            // Left button
            nodeEnter.createControlButton(0, 1);
            nodeEnter.createText(settings.css.box.width / 6, btnHeight, "#505050", hierarchyChartModel.Button1Text).attr("class", "controlBtnText styleclass");

            // Middle button
            nodeEnter.createControlButton(1, 1);
            nodeEnter.createText(settings.css.box.width * 4 / 6, btnHeight, "#505050", hierarchyChartModel.Button2Text).attr("class", "controlBtnText styleclass");

            // Right button
            //nodeEnter.createControlButton(2, 1);
            //nodeEnter.createText(settings.css.box.width * 5 / 6, btnHeight, "#505050", hierarchyChartModel.Button3Text).attr("class", "controlBtnText styleclass");

            //These handlers will be triggered on the onclick events of the node buttons
            //They will perform some action, whatever defined, and then perform a callback of the user defined methods for these buttons
            var button1 = nodeEnter.createControlButton(0, 0);
            button1.on("click", nodeEventHandlers.LeftNodeButtonClick);

            var button2 = nodeEnter.createControlButton(1, 0);
            button2.on("click", nodeEventHandlers.MiddleNodeButtonClick);

            //var button3 = nodeEnter.createControlButton(2, 0);
            //button3.on("click", nodeEventHandlers.RightNodeButtonClick);

            // Transition nodes to their new position.
            node.transition()
                .duration(settings.timeoutDuration)
                .attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });

            // Transition exiting nodes to the parent's new position.
            // Ending position for animation
            node.exit().transition()
                .duration(settings.timeoutDuration)
                .attr("transform", function (d) {
                    return "translate(" + source.x + "," + source.y + ")";
                })
                .remove();

            // Update the links…
            var link = d3Config.Svg.selectAll("path.link")
                .data(links, function (d) {
                    return d.target.id;
                });
            // Enter any new links at the parent's previous position.
            link.enter().insert("path", "g")
                .attr("class", "link")
                .attr("d", function (d) {
                    var o = { x: source.x0, y: source.y0 };
                    return d3Config.Diagonal({ source: o, target: o });
                });
            // Transition links to their new position.
            link.transition()
                .duration(settings.timeoutDuration)
                .attr("d", d3Config.Diagonal);
            // Transition exiting nodes to the parent's new position.
            link.exit().transition()
                .duration(settings.timeoutDuration)
                .attr("d", function (d) {
                    var o = { x: source.x, y: source.y };
                    return d3Config.Diagonal({ source: o, target: o });
                })
                .remove();
            // Stash the old positions for transition.
            nodes.forEach(function (d) {
                d.x0 = d.x;
                d.y0 = d.y;
            });
        };

        let initPrototypes = function () {
            Array.prototype.createTitle = function (x, y, fill, text) {
                return this.append("text")
                    .attr("data-title", text)
                    .attr("x", x)
                    .attr("y", y)
                    .attr("fill", fill)
                    .text(text);
            };

            Array.prototype.createName = function (x, y, fill, text) {
                return this.append("text")
                    .attr("data-subtitle", text)
                    .attr("x", x)
                    .attr("y", y)
                    .attr("fill", fill)
                    .text(text);
            };

            Array.prototype.createChildrenTag = function (x, y, fill, text) {
                return this.append("text")
                    .attr("data-childrentag", text)
                    .attr("x", x)
                    .attr("y", y)
                    .attr("fill", fill)
                    .text(text);
            };

            Array.prototype.createText = function (x, y, fill, text) {
                return this.append("text")
                    .attr("x", x)
                    .attr("y", y)
                    .attr("fill", fill)
                    .text(text);
            };

            Array.prototype.createNodeImage = function (x, y, width, height, source) {
                return this.append("image")
                    .attr("data-nodeimage", 'image')
                    .attr("x", x)
                    .attr("y", y)
                    .attr("width", width)
                    .attr("height", height)
                    .attr("xlink:href", source);
            };

            Array.prototype.createImage = function (x, y, width, height, source) {
                return this.append("image")
                    .attr("x", x)
                    .attr("y", y)
                    .attr("width", width)
                    .attr("height", height)
                    .attr("xlink:href", source);
            };

            Array.prototype.createControlButton = function (position, opacity) {
                var rectangle = this.append("rect")
                    .attr("mask", "url(#buttons)")
                    .attr("width", settings.css.box.width / 2)
                    .attr("height", settings.css.button.height)
                    .attr("x", settings.css.box.width * position / 2)
                    .attr("y", settings.css.box.height - 30)
                    .attr("class", "btn")
                    .attr("fill-opacity", opacity);

                return rectangle;
            };

            Array.prototype.createCustomRectangle = function (x, y, width, height, rx, style) {
                return this.append("rect")
                    .attr("x", x)
                    .attr("y", y)
                    .attr("width", width)
                    .attr("height", height)
                    .attr("rx", rx)
                    .attr("class", style);
            };
        };

        // Toggle children function
        function toggle(d) {
            if (d._children) {
                if (d.children) {
                    d.children = $.merge(d._children, d.children);
                } else {
                    d.children = d._children;
                }
                d._children = null;
            } else if (d.children) {
                if (d._children) {
                    d._children = $.merge(d._children, d.children);
                } else {
                    d._children = d.children;
                }
                d.children = null;
            }
            return d;
        };

        //hides children top-down, used in chart init
        function hideAllChildred(node) {
            if (node.children) {
                node.children.forEach(function (child) {
                    if (child.children) {
                        child.children.forEach(function (child) { hideAllChildred(child); });
                        child._children = child.children;
                        child.children = null;
                    }
                });
                node._children = node.children;
                node.children = null;
            }
        };

        // Toggle children on click.
        function click(d) {
            if (preventNodeClick) {
                preventNodeClick = false;
                return;
            };
            d = toggle(d);
            update(d);
        };

        return {
            SetupSearch: setupSearchControl,
            Update: update,
            Toggle: toggle,
            HideAllChildred: hideAllChildred
        };
    })();

    let d3Config = (function() {
        let responsivefy = function (svg) {
            // get width of container and resize svg to fit it
            var resize = function (svg) {
                let targetWidth = parseInt(container.style("width"));
                svg.attr("width", targetWidth);
                svg.attr("height", Math.round(targetWidth / aspect));
            };

            // get container + svg aspect ratio
            let container = d3.select(svg.node().parentNode),
                width = parseInt(svg.style("width")),
                height = parseInt(svg.style("height")),
                aspect = width / height;

            // add viewBox and preserveAspectRatio properties,
            // and call resize so that svg resizes on inital page load
            svg.attr("viewBox", "0 0 " + width + " " + height)
                .attr("perserveAspectRatio", "xMinYMid")
                .call(resize);

            // to register multiple listeners for same event type,
            // you need to add namespace, i.e., 'click.foo'
            // necessary if you call invoke this function for multiple svgs
            // api docs: https://github.com/mbostock/d3/wiki/Selections#on
            d3.select(window).on("resize." + container.attr("id"), resize(svg));
        };

        //Redraw for zoom
        let redraw = function () {
            svg.attr("transform",
                "translate(" + d3.event.translate + ")"
                + " scale(" + d3.event.scale + ")");
        }

        let tree = d3.layout.tree()
            .nodeSize([settings.css.box.width + 10, settings.css.box.height + 10]);

        let diagonal = d3.svg.diagonal()
            .source(function (d) {
                return {
                    // offset related to size of the box
                    "x": d.source.x + settings.css.box.width / 2,
                    "y": d.source.y + settings.css.box.height
                };
            })
            .target(function (d) {
                return {
                    "x": d.target.x + settings.css.box.width / 2,
                    "y": d.target.y
                };
            })
            .projection(function (d) {
                return [d.x, d.y];
            });


        let svg = d3.select(".orgchart-container").append("svg")
            .attr("width", dimensions.width + settings.css.margin.right + settings.css.margin.left)
            .attr("height", dimensions.height + settings.css.margin.top + settings.css.margin.bottom)
            .attr("id", "chart")
            .call(responsivefy)
            .call(zm = d3.behavior.zoom().scaleExtent([-1, 1]).on("zoom", redraw)).append("g")
            .append("g")
            .attr("transform", "translate(" + (dimensions.width / 2) + "," + settings.css.margin.top + ")");

        //necessary so that zoom knows where to zoom and unzoom from
        zm.translate([(dimensions.width / 2), settings.css.margin.top]);
        d3.select(self.frameElement).style("height", "500px");

        return {
            //Init: init,
            Tree: tree,
            Diagonal: diagonal,
            Svg: svg
        };
    })();

    //we pass in the view model
    let initTree = function (tree) {
        root = tree.Data;
        hierarchyChartModel = tree;
        root.x0 = dimensions.height / 2;
        root.y0 = 0;
        d3Config.Tree.nodes(root);

        // Collapse all nodes that are children of the top node (show only 2 top levels initially)
        //root.children.forEach(function (n) { userInterface.Toggle(n); });
        if (root.childre !== undefined) {
            root.children.forEach(function (node) {
                userInterface.HideAllChildred(node);
            });
        }

        userInterface.SetupSearch();
        userInterface.Update(root);
    };

    return {
        Init: initTree,
        Node: node
    };
})();