using System;
using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportHierarchyZone : IFilterableEntity
    {
        public ReportHierarchyZone()
        {
            ReportHierarchyRegions = new HashSet<ReportHierarchyRegion>();
        }

        [Select2Id]
        public int Id { get; set; }
        
        [Select2Text]
        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        
        public virtual ICollection<ReportHierarchyRegion> ReportHierarchyRegions { get; set; }
    }
}
