﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTQualityFeedResponse
    {
        [JsonProperty("data")]
        public List<ETLCTQualityFeedItem> Data { get; set; }

        public string Message { get; set; }
    }

    public class ETLCTQualityFeedItem
    {
        [JsonProperty("metadata")]
        public ETLCTQualityFeedItemMetadata Metadata { get; set; }

        [JsonProperty("fields")]
        public List<ETLCTQualityFeedItemField> Fields { get; set; }
    }

    public class ETLCTQualityFeedItemMetadata
    {
        [JsonProperty("isSubtotalRecord")]
        public bool IsSubtotalRecord { get; set; }

        [JsonProperty("isGrandTotalRecord")]
        public bool IsGrandTotalRecord { get; set; }
    }

    public class ETLCTQualityFeedItemField
    {
        [JsonProperty("data")]
        public string Data { get; set; }

    }
    }
