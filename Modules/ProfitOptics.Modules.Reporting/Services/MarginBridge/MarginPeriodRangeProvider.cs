﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProfitOptics.Modules.Reporting.Models.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Services.MarginBridge
{
    public class MarginPeriodRangeProvider : IMarginPeriodRangeProvider
    {
        private static readonly string DefaultPeriodRange = "Prior3MonthsLastComplete3Months";

        private readonly DateTime _endDate;
        private readonly (DateTime start, DateTime end) _lastCompleteMonth;
        private readonly IEnumerable<MarginBridgePeriodRange> _periods;

        public MarginPeriodRangeProvider(DateTime? now = null)
        {
            var nowCoalesced = now?.Date ?? DateTime.UtcNow.Date;
            _endDate = nowCoalesced;
            _lastCompleteMonth = GetLastCompleteMonth(_endDate);
            _periods = InitializePeriods(_endDate);
        }

        private IEnumerable<MarginBridgePeriodRange> InitializePeriods(DateTime now)
        {
            var priorMonthLastCompleteMonth = GetPriorMonthLastCompleteMonth();
            var prior3MonthsLastComplete3Months = GetPrior3MonthsLastComplete3Months();
            var prior12MonthsLastComplete12Months = GetPrior12MonthsLastComplete12Months();
            var equivalent3MonthPriorYearLastComplete3Months = GetEquivalent3MonthPriorYearLastComplete3Months();

            return new[]
            {
                new MarginBridgePeriodRange("PriorMonthLastCompleteMonth", "Prior Month vs. Last Complete Month",
                    priorMonthLastCompleteMonth.firstPeriod, priorMonthLastCompleteMonth.secondPeriod),
                new MarginBridgePeriodRange("Prior3MonthsLastComplete3Months", "Prior 3 Months vs. Last Complete 3 Months",
                    prior3MonthsLastComplete3Months.firstPeriod, prior3MonthsLastComplete3Months.secondPeriod),
                new MarginBridgePeriodRange("Prior12MonthsLastComplete12Months", "Prior 12 Months vs. Last Complete 12 Months",
                    prior12MonthsLastComplete12Months.firstPeriod, prior12MonthsLastComplete12Months.secondPeriod),
                new MarginBridgePeriodRange("Equivalent3MonthPriorYearLastComplete3Months", "Equivalent 3 Months Prior Year vs. Last Complete 3 Months Current Year",
                    equivalent3MonthPriorYearLastComplete3Months.firstPeriod, equivalent3MonthPriorYearLastComplete3Months.secondPeriod),
            };
        }

        public MarginBridgePeriodRange Get(string id)
        {
            return _periods.First(x => x.Id == id);
        }

        public MarginBridgePeriodRange GetDefault()
        {
            return Get(DefaultPeriodRange);
        }

        public IEnumerable<MarginBridgePeriodRange> GetAll()
        {
            return new List<MarginBridgePeriodRange>(_periods).AsReadOnly();
        }

        private (MarginBridgePeriod firstPeriod, MarginBridgePeriod secondPeriod) GetPriorMonthLastCompleteMonth()
        {
            var secondPeriod = new MarginBridgePeriod()
            {
                StartDate = _lastCompleteMonth.start,
                EndDate = _lastCompleteMonth.end
            };

            var firstPeriod = new MarginBridgePeriod()
            {
                StartDate = secondPeriod.StartDate.AddMonths(-1),
                EndDate = secondPeriod.EndDate.AddMonths(-1)
            };

            return (firstPeriod, secondPeriod);
        }

        private (MarginBridgePeriod firstPeriod, MarginBridgePeriod secondPeriod) GetPrior3MonthsLastComplete3Months()
        {
            var secondPeriod = new MarginBridgePeriod()
            {
                StartDate = _lastCompleteMonth.start.AddMonths(-2),
                EndDate = _lastCompleteMonth.end
            };

            var firstPeriod = new MarginBridgePeriod()
            {
                StartDate = secondPeriod.StartDate.AddMonths(-3),
                EndDate = secondPeriod.StartDate.AddDays(-1)
            };

            return (firstPeriod, secondPeriod);
        }

        private (MarginBridgePeriod firstPeriod, MarginBridgePeriod secondPeriod) GetPrior12MonthsLastComplete12Months()
        {
            var secondPeriod = new MarginBridgePeriod()
            {
                StartDate = _lastCompleteMonth.start.AddMonths(-11),
                EndDate = _lastCompleteMonth.end
            };

            var firstPeriod = new MarginBridgePeriod()
            {
                StartDate = secondPeriod.StartDate.AddMonths(-12),
                EndDate = secondPeriod.StartDate.AddDays(-1)
            };

            return (firstPeriod, secondPeriod);
        }

        private (MarginBridgePeriod firstPeriod, MarginBridgePeriod secondPeriod) GetEquivalent3MonthPriorYearLastComplete3Months()
        {
            var secondPeriod = new MarginBridgePeriod()
            {
                StartDate = _lastCompleteMonth.start.AddMonths(-2),
                EndDate = _lastCompleteMonth.end
            };

            var firstPeriod = new MarginBridgePeriod()
            {
                StartDate = secondPeriod.StartDate.AddYears(-1),
                EndDate = secondPeriod.EndDate.AddYears(-1)
            };

            return (firstPeriod, secondPeriod);
        }

        private (DateTime start, DateTime end) GetLastCompleteMonth(DateTime now)
        {
            DateTime endDate;
            if (now.AddDays(1).Month == now.Month)
            {
                var previousMonth = now.AddMonths(-1);

                endDate = new DateTime(previousMonth.Year, previousMonth.Month, DateTime.DaysInMonth(previousMonth.Year, previousMonth.Month));
            }
            else
            {
                endDate = now;
            }

            DateTime startDate = new DateTime(endDate.Year, endDate.Month, 1);

            return (startDate, endDate);
        }
    }
}