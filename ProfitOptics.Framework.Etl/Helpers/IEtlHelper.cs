﻿using System.Collections.Generic;
using System.Data;
using System.IO;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public interface IEtlHelper
    {
        /// <summary>
        /// Downloads files from the specified folder on the IMAP server.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="destinationFolder"></param>
        /// <param name="folder"></param>
        /// <param name="moveProcessedToFolder"></param>
        void DownloadFileImap(string destinationFolder, string folder = "INBOX", string moveProcessedToFolder = "ARCHIVE");

        /// <summary>
        /// Executes SQL scripts from the specified folder, or just a single script if the filename is provided.
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="fileName"></param>
        void ExecuteSql(string folder, string fileName = "");

        /// <summary>
        /// Checks if the source <see cref="DataTable"/> fits the destination <see cref="DataTable"/>.
        /// </summary>
        /// <param name="dtSource"></param>
        /// <param name="dtDestination"></param>
        void CheckIfFits(DataTable dtSource, DataTable dtDestination);

        /// <summary>
        /// Gets the table structure for the spcified database table name.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        DataTable GetTableStructure(string tableName);

        /// <summary>
        /// Loadas the specified <see cref="DataTable"/> data to the specified database table.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="data"></param>
        /// <param name="truncate"></param>
        /// <param name="skipColumns"></param>
        void LoadTable(string tableName, DataTable data, bool truncate, string[] skipColumns = null);

        /// <summary>
        /// Logs the processed file to the database.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="source"></param>
        /// <param name="fileName"></param>
        /// <param name="processingTime"></param>
        /// <param name="status"></param>
        /// <param name="rowCount"></param>
        /// <param name="message"></param>
        /// <param name="actionType"></param>
        /// <param name="options"></param>
        /// <param name="description"></param>
        void LogProcessedAction(string username, string source, string fileName, string processingTime, string status, int rowCount, string message = null, int? actionType = null, string options = null, string description = null);

        /// <summary>
        /// Downloads file from SFTP and retruns a list of downloaded files
        /// </summary>
        /// <param name="hostname"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="sshhostkeyfingerprint"></param>
        /// <param name="sourceFolder"></param>
        /// <param name="destinationFolder"></param>
        /// <param name="deleteFile"></param>
        /// <returns></returns>
        List<string> DownloadFileSftp(string hostname, string username, string password, string sshhostkeyfingerprint, string sourceFolder, string destinationFolder,
            bool deleteFile);

        /// <summary>
        /// Uploads a file to SFTP
        /// </summary>
        /// <param name="hostname">Hostname</param>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="sshhostkeyfingerprint">SSH host key finger print, starts with something like ssh-dss 1024 ErL+, ends with =</param>
        /// <param name="localPath">Local file path, e.g. c:\\tmp\\file.ext</param>
        /// <param name="remotePath">Remote file path, e.g. /folder/file.ext</param>
        void UploadFileSftp(string hostname, string username, string password, string sshhostkeyfingerprint, string localPath, string remotePath);


        /// <summary>
        /// Deletets a file to specific file on the sftp server.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="ftpFileUrl"></param>
        void DeleteFileSFtp(string hostname, string username, string password, string sshhostkeyfingerprint, string fileName);

        /// <summary>
        /// Download file from the specified FTP URL. 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="sourceFileUrl"></param>
        /// <param name="destinationFolder"></param>
        /// <param name="fileName"></param>
        /// <param name="deleteFile">If set to true, downloaded file is deleted after downloading.</param>
        void DownloadFileFtp(string username, string password, string sourceFileUrl, string destinationFolder, string fileName, bool deleteFile);

        /// <summary>
        /// Downloads all files from the specified FTP directory.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="ftpDirectoryUrl"></param>
        /// <param name="destinationFolder"></param>
        /// <param name="deleteFiles">If set to true, downloaded files are deleted after downloading.</param>
        List<string> DownloadAllFilesFromDirectoryFtp(string username, string password, string ftpDirectoryUrl, string destinationFolder, bool deleteFiles);

        /// <summary>
        /// Gets the file name from the specified URL.
        /// </summary>
        /// <param name="fileUrl"></param>
        /// <returns></returns>
        string GetFileNameFromUrl(string fileUrl);

        /// <summary>
        /// Deletes a file on the local file system.
        /// </summary>
        /// <param name="filePath"></param>
        void DeleteFile(string filePath);

        /// <summary>
        /// Creates a file and writes text to it.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="textToWrite"></param>
        void WriteToFileLocal(string fileName, string textToWrite);

        /// <summary>
        /// Checks if files are locked and moves all of them from one location to another and returns the final file paths
        /// </summary>
        /// <param name="sourceDirectory"></param>
        /// <param name="destinationDirectory"></param>
        /// <returns></returns>
        List<string> CopyAllFilesLocalWithLockCheck(string sourceDirectory, string destinationDirectory);

        /// <summary>
        /// Delete all files form a Directory.
        /// </summary>
        /// <param name="DirectoryPath"></param>
        void CleanDirectory(string DirectoryPath);

        /// <summary>
        /// Unpackes a zip file and returns the file name
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        string UpackZipFile(string directoryPath, string fileName);
    }
}
