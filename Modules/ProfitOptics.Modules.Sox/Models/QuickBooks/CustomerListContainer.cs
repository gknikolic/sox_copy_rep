﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class CustomerListContainer
    {
        [JsonProperty("Customer")]
        public List<Customer> Customers { get; set; }
    }
}
