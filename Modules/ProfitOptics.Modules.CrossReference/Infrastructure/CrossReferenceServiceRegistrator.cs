﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.CrossReference.Data;
using ProfitOptics.Modules.CrossReference.Factories;
using ProfitOptics.Modules.CrossReference.Services;

namespace ProfitOptics.Modules.CrossReference.Infrastructure
{
    public sealed class CrossReferenceServiceRegistrator :IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<ICrossReferenceService, CrossReferenceService>();
            services.AddScoped<ICrossReferenceFactory, CrossReferenceFactory>();
        }
    }
}
