﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels;

namespace ProfitOptics.Modules.Reporting.Services.RsmPerformance
{
    public class DaysOffService : IDaysOffService
    {
        private readonly Entities _context;
        private readonly ReportingSettings _reportingSettings;
        
        public DaysOffService(Entities context, ReportingSettings settings)
        {
            _context = context;
            _reportingSettings = settings;
        }

        public List<DateTime> GetHolidayDatesForCurrentYear()
        {
            return GetHolidayDatesForYear(DateTime.Now.Year);
        }

        public List<DateTime> GetHolidayDatesForYear(int year)
        {
            return (from holiday in _context.ReportHolidays
                where holiday.Date.Year == year
                select holiday.Date).ToList();
        }

        public List<DateTime> GetAllNonWorkDatesForCurrentYear()
        {
            return GetAllNonWorkDatesForYear(DateTime.Now.Year);
        }

        public List<DateTime> GetAllNonWorkDatesForYear(int year)
        {
            IEnumerable<DateTime> weekends = GetAllWeekendDatesForYear(year);

            List<DateTime> holidays = GetHolidayDatesForCurrentYear();

            List<DateTime> result = weekends.Union(holidays).ToList();

            return result;
        }

        private static IEnumerable<DateTime> GetAllWeekendDatesForYear(int year)
        {
            var weekends = new List<DateTime>();
            var startOfYear = new DateTime(year, 1, 1);
            var endOfYear = new DateTime(year, 12, 31);
            var loopIncrement = 1;
            var dayIncrement = 1;
            for (var date = startOfYear; date <= endOfYear; date = date.AddDays(loopIncrement))
            {
                if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                {
                    if (loopIncrement == 1)
                    {
                        loopIncrement = 7;
                        dayIncrement = date.DayOfWeek == DayOfWeek.Saturday ? 1 : -1;
                    }

                    weekends.Add(date);
                    weekends.Add(date.AddDays(dayIncrement));
                }
            }

            return weekends;
        }

        public IEnumerable<DateTime> GetAllNonWorkDatesForCurrentYearToDate()
        {
            return GetAllNonWorkDatesForYearToDate(DateTime.Now.Year, DateTime.Now);
        }

        public IEnumerable<DateTime> GetAllNonWorkDatesForYearToDate(int year, DateTime date)
        {
            return year != date.Year ? new List<DateTime>() : GetAllNonWorkDatesForYear(year).Where(x => x < date);
        }

        public IEnumerable<DateTime> GetAllWorkDaysForMonth(int year, int month)
        {
            var startOfMonth = new DateTime(year, month, 1);

            var workDays = new List<DateTime>();

            for (DateTime date = startOfMonth; date < startOfMonth.AddMonths(1); date = date.AddDays(1))
            {
                workDays.Add(date);
            }

            IEnumerable<DateTime> nonWorkDays = GetAllNonWorkDatesForYear(year).Where(day => day.Month == month);

            IEnumerable<DateTime> result = workDays.Where(day => !nonWorkDays.Contains(day));

            return result;
        }

        public IEnumerable<DateTime> GetAllWorkDaysForMonthToDate(int year, int month)
        {
            IEnumerable<DateTime> workDays = GetAllWorkDaysForMonth(year, month);

            return workDays.Where(day => day.DayOfYear < DateTime.Now.DayOfYear);
        }

        public IEnumerable<DateTime> GetRemainingWorkDays(int year, int month)
        {
            IEnumerable<DateTime> workDays = GetAllWorkDaysForMonth(year, month);

            return workDays.Where(day => day.DayOfYear >= DateTime.Now.DayOfYear);
        }

        public string GetMonthName(int month)
        {
            return new DateTime(DateTime.Now.Year, month, 1).ToString("MMM", CultureInfo.InvariantCulture);
        }

        public IEnumerable<string> GetPreviousMonthNames(int month)
        {
            var prevMonths = new List<string>();

            for (var i = 1; i < month; i++)
            {
                prevMonths.Add(GetMonthName(i));
            }

            return prevMonths;
        }

        public MonthDetails GetMonthDetails(int year, int month)
        {
            var result = new MonthDetails
            {
                CurrentMonthName = GetMonthName(month),
                PreviousMonthsNames = GetPreviousMonthNames(month).ToList(),
                WorkDaysCount = GetAllWorkDaysForMonth(year, month).Count(),
                WorkDaysToDateCount = GetAllWorkDaysForMonthToDate(year, month).Count(),
                RemainingWorkDaysCount = GetRemainingWorkDays(year, month).Count()
            };

            return result;
        }

        /// <summary>
        /// Gets the quarter number for the given month
        /// </summary>
        /// <param name="monthNumber">Month number from 1 to 12</param>
        /// <returns></returns>
        public int GetQuarterNumber(int monthNumber)
        {
            DateTimeFormatInfo dtFormatInfo = CultureInfo.InvariantCulture.DateTimeFormat;

            string monthName = dtFormatInfo.GetAbbreviatedMonthName(monthNumber);

            int result = _reportingSettings.MonthMappingDict[monthName.ToLower()];

            return result;
        }

        /// <summary>
        /// Gets the quarter number for the given month
        /// </summary>
        /// <param name="monthName">Month name in abbreviated format ("MMM")</param>
        /// <returns></returns>
        public int GetQuarterNumber(string monthName)
        {
            return _reportingSettings.MonthMappingDict[monthName];
        }

        /// <summary>
        /// Gets the quarter start date, quarter end date and the current quarter number for the given month and year
        /// </summary>
        /// <param name="year">Year</param>
        /// <param name="month">Month</param>
        /// <param name="quarterStart">The quarter start date</param>
        /// <param name="quarterEnd">The quarter end date</param>
        /// <param name="currentQuarter">The current quarter number</param>
        public void GetCurrentQuarterDetails(int year, int month, out DateTime quarterStart, out DateTime quarterEnd, out int currentQuarter)
        {
            int quarterStartMonth = (month - 1) / 3 * 3 + 1;

            quarterStart = new DateTime(year, quarterStartMonth, 1).Date;

            quarterEnd = quarterStart.AddMonths(3).AddDays(-1).Date;

            currentQuarter = _reportingSettings.MonthMappingDict[quarterStart.ToString("MMM", CultureInfo.InvariantCulture).ToLower()];
        }
    }
}