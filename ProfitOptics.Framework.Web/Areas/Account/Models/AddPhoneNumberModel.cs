﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ProfitOptics.Framework.Web.Helpers;

namespace ProfitOptics.Framework.Web.Areas.Account.Models
{
    public class AddPhoneNumberModel
    {
        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Add a phone number")]
        public string Number { get; set; }

        public string ErrorMessage { get; set; }
    }
}

