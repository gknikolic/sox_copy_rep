﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class Barcode
	{
		[JsonProperty("sec")]
		public int Sec { get; set; }
		[JsonProperty("barcode")]
		public string BarcodeValue { get; set; }
		[JsonProperty("barcodeStatus")]
		public string BarcodeStatus { get; set; }

	}
}
