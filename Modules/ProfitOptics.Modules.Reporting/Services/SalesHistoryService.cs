﻿using System;
using System.Linq;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Data.Domain;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Services
{
    public class SalesHistoryService : ISalesHistoryService
    {
        private readonly Entities _context;

        public SalesHistoryService(Entities context)
        {
            _context = context;
        }

        public IQueryable<ReportSalesHistory> GetFilteredSalesHistories(ReportFilterModel reportFilter)
        {
            IQueryable<ReportSalesHistory> query = _context.ReportSalesHistory;

            if (reportFilter == null)
            {
                return query;
            }

            if (reportFilter.ZoneId != null && reportFilter.ZoneId != default(int))
            {
                query = query.Where(p => p.ZoneId == reportFilter.ZoneId);
            }
            else if (reportFilter.RegionId != null && reportFilter.RegionId != default(int))
            {
                query = query.Where(p => p.RegionId == reportFilter.RegionId);
            }
            else if (reportFilter.SalesRepId != null && reportFilter.SalesRepId != default(int))
            {
                query = query.Where(p => p.SalesRepId == reportFilter.SalesRepId);
            }
            
            if (reportFilter.FilterShipTo.Count > 0)
            {
                query = query.Where(p => reportFilter.FilterShipTo.Contains(p.ShipToId));
            }

            if (reportFilter.FilterItemCategory.Count > 0)
            {
                query = query.Where(p => reportFilter.FilterItemCategory.Contains(p.ItemCategoryId ?? 0));
            }

            if (reportFilter.FilterCustomerClass.Count > 0)
            {
                query = query.Where(p => reportFilter.FilterCustomerClass.Contains(p.CustomerClassId ?? 0));
            }

            if (reportFilter.FilterVendor.Count > 0)
            {
                query = query.Where(p => reportFilter.FilterVendor.Contains(p.VendorId ?? 0));
            }

            if (reportFilter.FilterSalesRep.Count > 0)
            {
                query = query.Where(p => reportFilter.FilterSalesRep.Contains(p.SalesRepId ?? 0));
            }

            if (reportFilter.FilterBillTo.Count > 0)
            {
                query = query.Where(p => reportFilter.FilterBillTo.Contains(p.BillToId));
            }

            if (reportFilter.FilterZone.Count > 0)
            {
                query = query.Where(p => reportFilter.FilterZone.Contains(p.ZoneId ?? 0));
            }
            
            if (reportFilter.FilterRegion.Count > 0)
            {
                query = query.Where(p => reportFilter.FilterRegion.Contains(p.RegionId ?? 0));
            }

            return query;
        }

        /// <inheritdoc />
        public IQueryable<ReportSalesHistory> GetFilteredSalesHistoriesForDateRange(ReportFilterModel reportFilter, DateTime startDate, DateTime endDate)
        {
            return GetFilteredSalesHistories(reportFilter).Where(s => s.InvoiceDate >= startDate && s.InvoiceDate <= endDate);
        }
    }
}