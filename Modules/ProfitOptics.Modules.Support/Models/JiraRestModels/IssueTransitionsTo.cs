﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueTransitionsTo
    {
        public string self { get; set; }

        public string description { get; set; }

        public string name { get; set; }

        public string id { get; set; }

        public IssueTransitionsStatusCategory statusCategory { get; set; }
    }
}