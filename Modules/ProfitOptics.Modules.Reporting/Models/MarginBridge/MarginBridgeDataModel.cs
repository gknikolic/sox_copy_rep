﻿using System;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class MarginBridgeDataModel
    {
        public string PeriodId { get; set; }

        public MarginBridgePeriod FirstPeriod { get; set; }

        public MarginBridgePeriod SecondPeriod { get; set; }

        public ProductLevelType ProductSelectionLevel { get; set; }

        public LocationLevelType LocationSelectionLevel { get; set; }

        public MarginBridgeType MarginBridgeType { get; set; }
    }

    public class MarginBridgePeriod
    {
        private readonly string marginBridgePeriodFormat = "yyyy-MM";

        public DateTime StartDate { get; set; }

        public string StartDateFormatted => StartDate.ToString(marginBridgePeriodFormat);

        public DateTime EndDate { get; set; }

        public string EndDateFormatted => EndDate.ToString(marginBridgePeriodFormat);
    }

    public class MarginBridgePeriodRange
    {
        public string Id { get; }
        public string Title { get; }
        public MarginBridgePeriod FirstPeriod { get; }
        public MarginBridgePeriod SecondPeriod { get; }

        public MarginBridgePeriodRange(string id, string title, MarginBridgePeriod firstPeriod, MarginBridgePeriod secondPeriod)
        {
            Id = id;
            Title = title;
            FirstPeriod = firstPeriod;
            SecondPeriod = secondPeriod;
        }
    }
}