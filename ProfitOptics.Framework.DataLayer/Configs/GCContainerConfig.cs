﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCContainerConfig : IEntityTypeConfiguration<GCContainer>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCContainer> builder)
        {
            builder.ToTable("GCContainer");
            builder.HasKey(entity => entity.Id);
            builder.HasOne(entity => entity.ContainerType)
               .WithMany(p => p.Containers)
               .HasForeignKey(entity => entity.ContainerTypeId);
            builder.HasOne(entity => entity.Set)
              .WithMany(p => p.Containers)
              .HasForeignKey(entity => entity.SetId);
        }
    }
}
