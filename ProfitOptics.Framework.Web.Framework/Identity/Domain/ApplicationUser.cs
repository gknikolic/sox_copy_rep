﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Framework.Identity.Domain
{
    public class ApplicationUser : IdentityUser<int>
    {
        public ApplicationUser()
        {
        }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool Approved { get; set; }
        public bool IsGoogleAuthenticatorEnabled { get; set; }
        public string GoogleAuthenticatorSecretKey { get; set; }
        public bool IsEnabled { get; set; }
        public string FullName => (FirstName + " " + LastName).Trim();

        public virtual POUserProfilePicture ProfilePicture { get; set; }
        public virtual POUserProfile POUserProfile { get; set; }

        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
        public virtual ICollection<POUserHierarchy> POChildUserHierarchies { get; set; }
        public virtual ICollection<POUserHierarchy> POParentUserHierarchies { get; set; }
        public virtual ICollection<GCParentCompanyUsers> GCParentCompanyUsers { get; set; }
        public virtual ICollection<GCChildCompanyUsers> GCChildCompanyUsers { get; set; }
    }
}