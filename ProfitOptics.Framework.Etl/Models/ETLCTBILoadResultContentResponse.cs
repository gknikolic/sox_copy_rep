﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Models
{
    public class ETLCTBILoadResultContentResponse
    {
        [JsonProperty("loadHeader")]
        public ETLCTBILoadResult LoadHeader { get; set; }

        [JsonProperty("loadItems")]
        public List<ETLCTBILoadResultContent> LoadItems { get; set; }
    }
}
