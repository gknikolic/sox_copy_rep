﻿using ProfitOptics.Framework.Web.RestApi.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.RestApi.Services.Auth
{
    public interface ITokenHandler
    {
        Task<AccessToken> CreateAccessToken(string username);
        RefreshToken TakeRefreshToken(string token);
        void RevokeRefreshToken(string token);
    }
}
