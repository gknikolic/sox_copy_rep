﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProfitOptics.Modules.ManualEtl.Data.Domain;
using ProfitOptics.Modules.ManualEtl.Models;

namespace ProfitOptics.Modules.ManualEtl.Services
{
    public interface IManualEtlService
    {
        List<string> GetAdminEmails();

        Task<bool> SendPortalQaCheckReport(List<string> emails, string body);

        IQueryable<ETLLog> GetEtlLogsForSource(string source);

        IQueryable<ETLLog> GetEtlLogs();

        ManualUploadFileTypeModel GetFileDetails(ManualUploadFileConfigModel configModel);
    }
}