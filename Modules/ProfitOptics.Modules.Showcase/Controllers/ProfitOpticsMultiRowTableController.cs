﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using ProfitOptics.Framework.Core.Helper;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers.MultiRowTable;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.Showcase.Factories;
using System;

namespace ProfitOptics.Modules.Showcase.Controllers
{
    [Authorize]
    [Area("Showcase")]
    public class ProfitOpticsMultiRowTableController : BaseController
    {
        private readonly IShowcaseFactory _showcaseFactory;
        private readonly ICompositeViewEngine _viewEngine;

        public ProfitOpticsMultiRowTableController(IShowcaseFactory showcaseFactory, ICompositeViewEngine viewEngine)
        {
            _showcaseFactory = showcaseFactory;
            _viewEngine = viewEngine;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetDataExampleGroupedRows(MultiRowTableRequest request)
        {
            var model = _showcaseFactory.GetProfitOpticsMultiRowTableExampleModel(request);

            int rem;
            var pageCount = Math.DivRem(model.TotalCount, request.length, out rem);

            return new LargeJsonResult(new
            {
                Html = Render.RenderPartialViewToString(this, _viewEngine, "_TableBodyGroupedRows", model),
                Totals = Render.RenderPartialViewToString(this, _viewEngine, "_TotalsGroupedRows", model),
                PageCount = rem == 0 ? pageCount - 1: pageCount 
            });
        }

        [HttpPost]
        public IActionResult GetDataExampleScrolling(MultiRowTableRequest request)
        {
            var model = _showcaseFactory.GetProfitOpticsMultiRowTableExampleModel(request);

            int rem;
            var pageCount = Math.DivRem(model.TotalCount, request.length, out rem);

            return new LargeJsonResult(new
            {
                Html = Render.RenderPartialViewToString(this, _viewEngine, "_TableBodyScrolling", model),
                Totals = Render.RenderPartialViewToString(this, _viewEngine, "_TotalsScrolling", model),
                PageCount = rem == 0 ? pageCount - 1 : pageCount
            });
        }

        [HttpPost]
        public IActionResult GetDataExampleLevels(MultiRowTableRequest request)
        {
            var model = _showcaseFactory.GetProfitOpticsMultiRowTableLevelsExampleModel(request);

            int rem;
            var pageCount = Math.DivRem(model.TotalCount, request.length, out rem);

            return new LargeJsonResult(new
            {
                Html = Render.RenderPartialViewToString(this, _viewEngine, "_TableBodyLevels", model),
                Totals = Render.RenderPartialViewToString(this, _viewEngine, "_TotalsLevels", model),
                PageCount = rem == 0 ? pageCount - 1 : pageCount
            });
        }

        [HttpPost]
        public IActionResult GetDataExampleAjaxLevels(MultiRowTableRequest request, string level, string masterId)
        {
            if (masterId != null)
            {
                if (level == "0")
                {
                    // LOAD EMPLOYEES BY MANAGER
                    return new LargeJsonResult(new
                    {
                        Html = Render.RenderPartialViewToString(
                            this, 
                            _viewEngine, 
                            "_TableBodyLevelsAjax", 
                            _showcaseFactory.GetProfitOpticsMultiRowTableLevelsExampleModelEmployees(request, masterId))
                    });
                }
                else if (level == "1")
                {
                    // LOAD OUTSOURCED BY MANAGER
                    return new LargeJsonResult(new
                    {
                        Html = Render.RenderPartialViewToString(
                            this,
                            _viewEngine,
                            "_TableBodyLevelsAjax",
                            _showcaseFactory.GetProfitOpticsMultiRowTableLevelsExampleModelOutsourced(request, masterId))
                    });
                }
            }

            var model = _showcaseFactory.GetProfitOpticsMultiRowTableLevelsWithAjaxExampleModel(request);

            int rem;
            var pageCount = Math.DivRem(model.TotalCount, request.length, out rem);

            return new LargeJsonResult(new
            {
                Html = Render.RenderPartialViewToString(this, _viewEngine, "_TableBodyLevelsAjax", model),
                Totals = Render.RenderPartialViewToString(this, _viewEngine, "_TotalsLevelsAjax", model),
                PageCount = rem == 0 ? pageCount - 1 : pageCount
            });
        }
    }
}