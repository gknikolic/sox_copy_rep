﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportShipToConfig : IEntityTypeConfiguration<ReportShipTo>
    {
        public void Configure(EntityTypeBuilder<ReportShipTo> builder)
        {
            builder.ToTable(nameof(ReportShipTo));
            builder.HasKey(s => s.Id);

            builder.Property(s => s.ShipToNum).HasMaxLength(50).IsRequired();
            builder.Property(s => s.ShipToName).HasMaxLength(255).IsRequired();
            builder.Property(s => s.Address).HasMaxLength(255).IsRequired();
            builder.Property(s => s.City).HasMaxLength(50).IsRequired();
            builder.Property(s => s.State).HasMaxLength(50).IsRequired();
            builder.Property(s => s.Zip).HasMaxLength(50).IsRequired();
            builder.Property(s => s.StartDate).IsRequired();

            builder.HasMany(s => s.ReportSalesHistories)
                .WithOne(sh => sh.ReportShipTo)
                .HasForeignKey(sh => sh.ShipToId);
        }
    }
}
