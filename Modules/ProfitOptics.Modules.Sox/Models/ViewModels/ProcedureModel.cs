﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class ProcedureModel
    {
        public int Id { get; set; }

        public int? GCProcedureTypeId { get; set; }

        [Required]
        [Display(Name = "Procedure")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "ProcedureTypeName")]
        public string ProcedureTypeName { get; set; }

        [Display(Name = "Procedure code (ICD9)")]
        public string ICD9 { get; set; }

        [Display(Name = "Procedure code (ICD10)")]
        public string ICD10 { get; set; }

        public int SelectedProcedureType { get; set; }
        public IEnumerable<SelectListItem> ProcdureTypes { get; set; }
    }
}
