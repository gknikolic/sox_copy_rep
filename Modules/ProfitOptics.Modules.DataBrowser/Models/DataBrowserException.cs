﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;

namespace ProfitOptics.Modules.DataBrowser.Models
{
    public class DataBrowserException : Exception
    {
        public DataBrowserException()
        {
        }

        public DataBrowserException(string message)
            : base(message)
        {
        }

        public DataBrowserException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}