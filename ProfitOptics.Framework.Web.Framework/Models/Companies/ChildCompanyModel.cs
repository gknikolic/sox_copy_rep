﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Models
{
    public class ChildCompanyModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? ParentCompanyId { get; set; }
        public bool PermissionTypeViewSelected { get; set; }
        public bool PermissionTypeEditSelected { get; set; }
        public bool PermissionTypeAddSelected { get; set; }
        public bool PermissionTypeDeleteSelected { get; set; }
        public int? GCVendorId { get; set; }
        public int? GCCustomerId { get; set; }
        public GCVendor Vendor { get; set; }
        public GCCustomer GCCustomer { get; set; }
        public string SelectedCustomer { get; set; }
        public string SelectedVendor { get; set; }

        public IEnumerable<SelectListItem> Vendors { get; set; }
        public IEnumerable<SelectListItem> Customers { get; set; }
    }
}
