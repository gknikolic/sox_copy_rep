﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class TimeWindow
	{
		[JsonProperty("startSec")]
		public int StartSec { get; set; }
		[JsonProperty("endSec")]
		public int EndSec { get; set; }
	}
}
