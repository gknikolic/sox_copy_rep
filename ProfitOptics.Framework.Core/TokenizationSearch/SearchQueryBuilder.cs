﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Core.TokenizationSearch
{
    public class SearchQueryBuilder
    {
        public static string BuildString(List<string> propList, TokenCollection tokenCollection)
        {
            if (tokenCollection != null && tokenCollection.mTokensAND.Count > 0 && propList != null && propList.Count > 0)
            {
                List<string> queries = new List<string>();
                foreach (string prop in propList)
                {
                    List<string> queriesAND = new List<string>();
                    foreach (TokenAND tokenAND in tokenCollection.mTokensAND)
                    {
                        List<string> queriesOR = new List<string>();
                        foreach (string tokenOR in tokenAND.mTokensOR)
                        {
                            queriesOR.Add(string.Format("{0}.Contains(\"{1}\")", prop, tokenOR));
                        }
                        queriesAND.Add(string.Join(" OR ", queriesOR));
                    }
                    queries.Add("(" + string.Join(") AND (", queriesAND) + ")");
                }

                if (queries.Count == 0)
                    return string.Empty;
                else
                    return "(" + string.Join(") OR (", queries) + ")";
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
