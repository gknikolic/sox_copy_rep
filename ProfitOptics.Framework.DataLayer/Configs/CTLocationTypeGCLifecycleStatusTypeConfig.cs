﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTLocationTypeGCLifecycleStatusTypeConfig : IEntityTypeConfiguration<CTLocationTypeGCLifecycleStatusType>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CTLocationTypeGCLifecycleStatusType> builder)
        {
            builder.ToTable("CTLocationTypesGCLifecycleStatusTypes");
            builder.HasKey(entity => entity.Id);
            builder.Property(e => e.GCLifecycleStatusTypeId).IsRequired();
            builder.Property(e => e.CTLocationTypeId).IsRequired();
        }
    }
}
