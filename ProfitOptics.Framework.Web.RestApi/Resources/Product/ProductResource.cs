using ProfitOptics.Framework.Web.RestApi.DataLayer;
using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.RestApi.Resources.Product
{
    public class ProductResource
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public int QuantityInPackage { get; set; }
        
        public string UnitOfMeasurement { get; set; }

        public List<DistributorProduct> DistributorProducts { get; set; }

        public List<CustomerProduct> CustomerProducts { get; set; }
    }
}