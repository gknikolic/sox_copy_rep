﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Infrastructure.Exceptions;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.Quotes.Factories;
using ProfitOptics.Modules.Quotes.Services;

namespace ProfitOptics.Modules.Quotes.Controllers
{
    [Area("Quotes")]
    [Authorize(Roles = "quoting,admin,superadmin")]
    public class QuoteItemsController : BaseController
    {
        private readonly IQuoteService _quoteService;
        private readonly IQuoteFactory _quoteFactory;

        public QuoteItemsController(IQuoteService quoteService,
            IQuoteFactory quoteFactory)
        {
            _quoteService = quoteService;
            _quoteFactory = quoteFactory;
        }

        [HttpPost]
        public IActionResult GetItemsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request, int quoteId, string filter)
        {
            var data = _quoteFactory.GetItemsData(request, quoteId, filter);
            return new LargeJsonResult(data.ToDataTablesResponse(request));
        }
        
        [HttpPost]
        public IActionResult GetQuoteItemsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request, int quoteId)
        {
            var data = _quoteFactory.GetQuoteItemsData(request, quoteId, GetCurrentUserId());
            return new LargeJsonResult(data.ToDataTablesResponse(request));
        }

        [HttpPost]
        public IActionResult GetItemsFromPriceListData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request, int quoteId, string plcCode)
        {
            var data = _quoteFactory.GetItemModelsFromPriceListData(request, quoteId, plcCode);

            return new LargeJsonResult(data.ToDataTablesResponse(request));
        }

        [HttpPost]
        public IActionResult GetItemsFromStandingOrderData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request, int quoteId, int orderNum)
        {
            var data = _quoteFactory.GetItemsFromStandingOrderData(request, quoteId, GetCurrentUserId(), orderNum);

            return new LargeJsonResult(data.ToDataTablesResponse(request));
        }

        [HttpPost]
        public IActionResult AddItemsToQuote(int quoteId, List<int> items)
        {
            try
            {
                _quoteService.AddItemsToQuote(quoteId, items);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult AddItemToQuote(int quoteId, string itemNum)
        {
            try
            {
                _quoteService.AddItemToQuote(quoteId, itemNum);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        public IActionResult ValidateItemsFromList(int quoteId, List<string> itemsList)
        {
            try
            {
                var notValid = _quoteService.ValidateItemsFromList(quoteId, itemsList);
                if (notValid.Any())
                {
                    return Json(new { success = false, message = "Error", notValid = notValid });
                }
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult AddItemsFromList(int quoteId, List<string> itemsList)
        {
            try
            {
                _quoteService.AddItemsFromList(quoteId, itemsList);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult RemoveItemFromQuote(int quoteId, int itemId)
        {
            try
            {
                _quoteService.RemoveItemFromQuote(quoteId, itemId);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult RemoveItemListFromQuote(int quoteId, List<int> items)
        {
            try
            {
                _quoteService.RemoveItemListFromQuote(quoteId, items);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult UpdateQuoteItemQty(int quoteId, int itemId, int units)
        {
            try
            {
                _quoteService.UpdateItemQty(quoteId, itemId, units);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult UpdateQuoteItemGmPerc(int quoteId, int itemId, decimal newValue)
        {
            try
            {
                _quoteService.UpdateQuoteItemGmPerc(quoteId, itemId, newValue);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult UpdateQuoteItemPrice(int quoteId, int itemId, decimal newValue)
        {
            try
            {
                _quoteService.UpdateQuoteItemPrice(quoteId, itemId, newValue);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                var e = new JavaScriptErrorException(ex.Message);
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult UpdateCompetitorPrice(int quoteId, int itemId, decimal newValue)
        {
            try
            {
                _quoteService.UpdateCompetitorPrice(quoteId, itemId, newValue);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult UpdateCompetitorUoM(int quoteId, int itemId, string newValue)
        {
            try
            {
                _quoteService.UpdateCompetitorUoM(quoteId, itemId, newValue);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult UpdateCompetitorCf(int quoteId, int itemId, decimal newValue)
        {
            try
            {
                _quoteService.UpdateCompetitorCf(quoteId, itemId, newValue);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        [HttpPost]
        public IActionResult RevertPriceToPlcPrice(int quoteId, int itemId)
        {
            try
            {
                _quoteService.RevertPriceToPlcPrice(quoteId, itemId);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        public IActionResult SessionLookupAutocomplete(int quoteId, string term)
        {
            return Json(new
            {
                items = _quoteService.GetItemAutocompleteData(quoteId, term)
            });
        }

        public IActionResult ReplaceItemInQuote(int quoteId, string oldItemNum, string newItemNum)
        {
            try
            {
                _quoteService.ReplaceItemInQuote(quoteId, oldItemNum, newItemNum);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        public IActionResult UpdateLineNumbers(int quoteItemId, int currentLineNumber, int newLineNumber)
        {
            try
            {
                _quoteService.UpdateLineNumbers(quoteItemId, currentLineNumber, newLineNumber);
                return Json(new { success = true, message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }
    }
}