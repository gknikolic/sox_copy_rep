﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer.GroundControl
{
    public class GCLocation
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string MappingTitle { get; set; }
        public int Code { get; set; }
        public virtual ICollection<GCLocationInfo> GCLocationInfos { get; set; }
    }
}
