﻿var passwordValidator = (function () {
    var settings = {
        passwordSelector: "",
        confirmPasswordSelector: "",
        passwordValidationsListSelector: "#password-validations",
        passwordLenghtListItemSelector: "#password-length-error",
        passwordMatchListItemSelector: "#password-match-error",
        passwordRegexListItemSelectorNumber: "#password-regex-error-number",
        passwordRegexListItemSelectorUppercaseLowercase: "#password-regex-error-uppercaselowercase",
        passwordRegexListItemSelectorSpecial: "#password-regex-error-special",
        passwordLengthIconSpanSelector: "#password-length-error > span",
        passwordMatchIconSpanSelector: "#password-match-error > span",
        passwordRegexIconSpanSelectorSpecial: "#password-regex-error-special > span",
        passwordRegexIconSpanSelectorNumber: "#password-regex-error-number > span",
        passwordRegexIconSpanSelectorUppercaseLowercase: "#password-regex-error-uppercaselowercase > span",
        passwordRegexNumber: new RegExp("(?=.*[0-9])"),
        passwordRegexUppercaseLowercase: new RegExp("(?=.*[a-z])(?=.*[A-Z])"),
        passwordRegexSpecial: new RegExp("(?=.*[!@@#\$%\^&\*\~])(?=.{6,})")
    };

    var passwordLengthCheck = function () {
        var len = $(settings.passwordSelector).val().length;
        if (len < 6) {
            $(settings.passwordLenghtListItemSelector).show();
            $(settings.passwordLenghtListItemSelector).removeClass("text-success");
            $(settings.passwordLengthIconSpanSelector).addClass("glyphicon-remove").removeClass("glyphicon-ok");
        } else {
            $(settings.passwordLenghtListItemSelector).addClass("text-success");
            $(settings.passwordLengthIconSpanSelector).removeClass("glyphicon-remove").addClass("glyphicon-ok");
        }
    };

    var passwordMatchCheck = function () {
        var password = $(settings.passwordSelector).val();
        var confirm = $(settings.confirmPasswordSelector).val();

        if (password === "" || confirm === "" || password !== confirm) {
            $(settings.passwordMatchListItemSelector).show();
            $(settings.passwordMatchListItemSelector).removeClass("text-success");
            $(settings.passwordMatchIconSpanSelector).addClass("glyphicon-remove").removeClass("glyphicon-ok");
        } else {
            $(settings.passwordMatchListItemSelector).addClass("text-success");
            $(settings.passwordMatchIconSpanSelector).removeClass("glyphicon-remove").addClass("glyphicon-ok");
        }
    };

    var passwordRegexCheck = function () {
        var password = $(settings.passwordSelector).val();

        var result = settings.passwordRegexNumber.test(password);
        if (result) {
            $(settings.passwordRegexListItemSelectorNumber).addClass("text-success");
            $(settings.passwordRegexIconSpanSelectorNumber).removeClass("glyphicon-remove").addClass("glyphicon-ok");
        } else {
            $(settings.passwordRegexListItemSelectorNumber).show();
            $(settings.passwordRegexListItemSelectorNumber).removeClass("text-success");
            $(settings.passwordRegexIconSpanSelectorNumber).addClass("glyphicon-remove").removeClass("glyphicon-ok");
        }

        result = settings.passwordRegexUppercaseLowercase.test(password);
        if (result) {
            $(settings.passwordRegexListItemSelectorUppercaseLowercase).addClass("text-success");
            $(settings.passwordRegexIconSpanSelectorUppercaseLowercase).removeClass("glyphicon-remove").addClass("glyphicon-ok");
        } else {
            $(settings.passwordRegexListItemSelectorUppercaseLowercase).show();
            $(settings.passwordRegexListItemSelectorUppercaseLowercase).removeClass("text-success");
            $(settings.passwordRegexIconSpanSelectorUppercaseLowercase).addClass("glyphicon-remove").removeClass("glyphicon-ok");
        }

        result = settings.passwordRegexSpecial.test(password);
        if (result) {
            $(settings.passwordRegexListItemSelectorSpecial).addClass("text-success");
            $(settings.passwordRegexIconSpanSelectorSpecial).removeClass("glyphicon-remove").addClass("glyphicon-ok");
        } else {
            $(settings.passwordRegexListItemSelectorSpecial).show();
            $(settings.passwordRegexListItemSelectorSpecial).removeClass("text-success");
            $(settings.passwordRegexIconSpanSelectorSpecial).addClass("glyphicon-remove").removeClass("glyphicon-ok");
        }
    };

    var passwordFocus = function () {
        $(settings.passwordSelector).focus(function () {
            $(settings.passwordValidationsListSelector).show();

            $(settings.passwordSelector).keyup(function () {
                passwordLengthCheck();
                passwordMatchCheck();
                passwordRegexCheck();
            });

            $(settings.confirmPasswordSelector).keyup(function () {
                passwordMatchCheck();
            });
        });
    };

    var init = function (passwordInputName, confirmPasswordInputName) {
        settings.passwordSelector = "#" + passwordInputName;
        settings.confirmPasswordSelector = "#" + confirmPasswordInputName;
        passwordFocus();
    };

    return {
        create: init
    };
})();