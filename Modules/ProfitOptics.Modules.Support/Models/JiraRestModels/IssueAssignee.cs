﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class IssueAssignee
    {
        public string key { get; set; }

        public string name { get; set; }

        public string emailAddress { get; set; }

        public string displayName { get; set; }
    }
}