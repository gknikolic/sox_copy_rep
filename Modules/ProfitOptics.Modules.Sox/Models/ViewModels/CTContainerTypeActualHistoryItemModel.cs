﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class CTContainerTypeActualHistoryItemModel
    {
        public int Id { get; set; }
        public int CTContainerTypeActualId { get; set; }
        public string LocationElapsedCase { get; set; }
        public DateTime UpdateTimestamp { get; set; }
        public DateTime UpdateTimestampOriginal { get; set; }
        public int? ActualLapCount { get; set; }
    }
}
