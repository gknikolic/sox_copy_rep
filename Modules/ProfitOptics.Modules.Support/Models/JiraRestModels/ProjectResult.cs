﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class ProjectResult
    {
        public string expand { get; set; }

        public int? id { get; set; }

        public string self { get; set; }

        public string key { get; set; }

        public string name { get; set; }
    }
}