﻿using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class Customer
    {
        public int? Id { get; set; }
        public bool? Taxable { get; set; }
        public Address BillAddr { get; set; }
        public string Notes { get; set; }
        public bool? Job { get; set; }
        public bool BillWithParent { get; set; }
        public decimal? Balance { get; set; }
        public decimal? BalanceWithJobs { get; set; }
        public CurrencyRef CurrencyRef { get; set; }
        public string PreferredDeliveryMethod { get; set; }
        public string Domain { get; set; }
        public bool? Sparse { get; set; }
        public string SyncToken { get; set; }
        public MetaData MetaData { get; set; }
        public string FullyQualifiedName { get; set; }
        public string DisplayName { get; set; }
        public string PrintOnCheckName { get; set; }
        public bool Active { get; set; }
        public Phone PrimaryPhone { get; set; }
        public Phone AlternatePhone { get; set; }
        public Email PrimaryEmailAddr { get; set; }
        public DefaultTaxCodeRef DefaultTaxCodeRef { get; set; }
        public string GivenName { get; set; }
        public string MiddleName { get; set; }
        public string FamilyName { get; set; }
    }
}
