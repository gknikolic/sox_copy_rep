﻿(function (showcase, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    // Public properties
    showcase.somePublicProperty = {};

    // Public Methods
    showcase.somePublicMethod = function () {
    };

    // Private Methods
    function somePrivateMethod(item) {
    }
}(window.showcase = window.showcase || {}, jQuery));