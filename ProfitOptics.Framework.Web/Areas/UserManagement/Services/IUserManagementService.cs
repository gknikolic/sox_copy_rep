﻿using ProfitOptics.Framework.Web.Areas.UserManagement.Models;
using ProfitOptics.Framework.Web.Models.Common;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Services
{
    public interface IUserManagementService
    {
        /// <summary>
        /// Updates user by provided model
        /// </summary>
        /// <param name="model">User update model</param>
        /// <returns></returns>
        Task<UserManagementServiceResult> SaveUser(ApplicationUserModel model);

        /// <summary>
        /// Enables/Disables user
        /// </summary>
        /// <param name="userId">Id of a user</param>
        Task<UserManagementServiceResult> ToggleUserEnabledAsync(int userId);

        /// <summary>
        /// Locks/Unlocks user
        /// </summary>
        /// <param name="userId">Id of a user</param>
        /// <returns></returns>
        Task<UserManagementServiceResult> ToggleUserLockAsync(int userId);

        /// <summary>
        /// Generates a URL for the password reset email
        /// </summary>
        /// <param name="user">Application user</param>
        /// <param name="scheme">Request scheme</param>
        /// <returns></returns>

        Task<PasswordResetServiceResult> AjaxResetPassword(int userId, string requestScheme);
    }
}
