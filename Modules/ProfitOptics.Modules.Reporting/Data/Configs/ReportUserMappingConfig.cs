﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.Reporting.Data.Domain;

namespace ProfitOptics.Modules.Reporting.Data.Configs
{
    public class ReportUserMappingConfig : IEntityTypeConfiguration<ReportUserMapping>
    {
        public void Configure(EntityTypeBuilder<ReportUserMapping> builder)
        {
            builder.ToTable(nameof(ReportUserMapping));
            builder.HasKey(m => m.UserId);

            builder.HasOne(m => m.AspNetUser)
                .WithOne()
                .HasForeignKey<ReportUserMapping>(m => m.UserId)
                .IsRequired();

            builder.HasOne(m => m.ReportHierarchyCorporate)
                .WithOne()
                .HasForeignKey<ReportUserMapping>(m => m.CorporateId)
                .IsRequired(false);

            builder.HasOne(m => m.ReportHierarchyRegion)
                .WithOne()
                .HasForeignKey<ReportUserMapping>(m => m.RegionId)
                .IsRequired(false);

            builder.HasOne(m => m.ReportHierarchyZone)
                .WithOne()
                .HasForeignKey<ReportUserMapping>(m => m.ZoneId)
                .IsRequired(false);

            builder.HasOne(m => m.ReportHierarchySalesRep)
                .WithOne()
                .HasForeignKey<ReportUserMapping>(m => m.SalesRepId)
                .IsRequired(false);
        }
    }
}
