﻿using Microsoft.EntityFrameworkCore;

namespace ProfitOptics.Framework.DataLayer
{
    public class GCTrayActualHistoryConfig : IEntityTypeConfiguration<GCTrayActualHistory>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCTrayActualHistory> builder)
        {
            builder.ToTable("GCTrayActualHistory");
            builder.HasKey(entity => entity.Id);
        }
    }
}
