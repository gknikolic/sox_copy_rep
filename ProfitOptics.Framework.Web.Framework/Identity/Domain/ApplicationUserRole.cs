﻿using Microsoft.AspNetCore.Identity;

namespace ProfitOptics.Framework.Web.Framework.Identity.Domain
{
    public class ApplicationUserRole : IdentityUserRole<int>
    {
        public virtual ApplicationUser User { get; set; }
        public virtual ApplicationRole Role { get; set; }
    }
}