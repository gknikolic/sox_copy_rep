﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ProfitOptics.Framework.Web.Framework.Models.Select2
{
    public static class Select2Extensions
    {
        /// <summary>
        /// Maps an object to a new <see cref="Select2Model"/> with the specified accessors.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        /// <param name="idAccessor">A delegate to map a property of the model to the id.</param>
        /// <param name="textAccessor">A delegate to map a property of the model to the text.</param>
        /// <returns></returns>
        public static Select2Model GetSelect2Model<TModel>(this TModel model,
            Expression<Func<TModel, string>> idAccessor,
            Expression<Func<TModel, string>> textAccessor)
        {
            var result = new Select2Model
            {
                id = idAccessor.Compile().Invoke(model),
                text = textAccessor.Compile().Invoke(model),
            };

            return result;
        }

        /// <summary>
        /// Maps an <see cref="IEnumerable{T}"/> collection to an enumerable of <see cref="Select2Model"/> objects.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="idAccessor">A delegate to map a property of the model to the id.</param>
        /// <param name="textAccessor">A delegate to map a property of the model to the text.</param>
        /// <returns></returns>
        public static IEnumerable<Select2Model> ToSelect2Enumerable<TModel>(this IEnumerable<TModel> enumerable,
            Expression<Func<TModel, string>> idAccessor,
            Expression<Func<TModel, string>> textAccessor)
        {
            var result = enumerable.ToList()
                .Select(x => x.GetSelect2Model(idAccessor, textAccessor))
                .OrderBy(x => x.text);

            return result;
        }

        /// <summary>
        /// Maps an <see cref="IEnumerable{T}"/> collection to a list of <see cref="Select2Model"/> objects.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="idAccessor">A delegate to map a property of the model to the id.</param>
        /// <param name="textAccessor">A delegate to map a property of the model to the text.</param>
        /// <returns></returns>
        public static List<Select2Model> ToSelect2List<TModel>(this IEnumerable<TModel> enumerable,
            Expression<Func<TModel, string>> idAccessor,
            Expression<Func<TModel, string>> textAccessor)
        {
            return enumerable.ToList()
                .ToSelect2Enumerable(idAccessor, textAccessor)
                .OrderBy(x => x.text)
                .ToList();
        }

        /// <summary>
        /// Converts the specified object to a <see cref="Select2Model"/>.
        /// <para>
        /// The class must have properties decorated with <see cref="Select2IdAttribute"/> and <see cref="Select2TextAttribute"/>. 
        /// </para>
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="selected"></param>
        /// <exception cref="Select2AttributeNotPresentException"></exception>
        /// <returns></returns>
        public static Select2Model ToSelect2Model(this object obj, bool selected = false)
        {
            var idIsSet = false;
            var id = string.Empty;
            var textIsSet = false;
            var text = string.Empty;
            foreach (var prop in obj.GetType().GetProperties())
            {
                if (prop.CustomAttributes.Any(x => x.AttributeType == typeof(Select2IdAttribute)) && !idIsSet)
                {
                    id = prop.GetValue(obj).ToString();
                    idIsSet = true;
                }

                if (prop.CustomAttributes.Any(x => x.AttributeType == typeof(Select2TextAttribute)) && !textIsSet)
                {
                    text = prop.GetValue(obj).ToString();
                    textIsSet = true;
                }
            }

            if (!idIsSet)
            {
                throw new Select2AttributeNotPresentException(@"The class does not have the necessary attribute specified", nameof(Select2IdAttribute));
            }

            if (!textIsSet)
            {
                throw new Select2AttributeNotPresentException(@"The class does not have the necessary attribute specified", nameof(Select2TextAttribute));
            }

            var result = new Select2Model
            {
                id = id,
                text = text,
                selected = selected
            };

            return result;
        }

        /// <summary>
        /// Maps an <see cref="IEnumerable{T}"/> collection to an enumerable of <see cref="Select2Model"/> objects.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static IEnumerable<Select2Model> ToSelect2EnumerableFromModel<TModel>(this IEnumerable<TModel> enumerable)
        {
            var result = enumerable.ToList()
                .Select(x => x.ToSelect2Model())
                .OrderBy(x => x.text);

            return result;
        }

        /// <summary>
        /// Maps an <see cref="IEnumerable{T}"/> collection to an enumerable of <see cref="Select2Model"/> objects.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static IEnumerable<Select2Model> ToSelect2Enumerable(this IEnumerable<object> enumerable, bool selected = false)
        {
            return enumerable.ToList()
                .Select(x => x.ToSelect2Model(selected))
                .OrderBy(x => x.text);
        }

        /// <summary>
        /// Maps an <see cref="IEnumerable{T}"/> collection to a list of <see cref="Select2Model"/> objects.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static List<Select2Model> ToSelect2ListFromModel<TModel>(this IEnumerable<TModel> enumerable)
        {
            return enumerable.ToList()
                .ToSelect2EnumerableFromModel()
                .OrderBy(x => x.text)
                .ToList();
        }

        /// <summary>
        /// Maps an <see cref="IEnumerable{T}"/> collection to a list of <see cref="Select2Model"/> objects.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static List<Select2Model> ToSelect2List(this IEnumerable<object> enumerable, bool selected = false)
        {
            return enumerable.ToList()
                .ToSelect2Enumerable(selected)
                .OrderBy(x=> x.text)
                .ToList();
        }

        /// <summary>
        /// Maps an <see cref="IEnumerable{T}"/> collection to a <see cref="Select2PagedResult"/> object.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static Select2PagedResult ToSelect2PagedResultFromModel<TModel>(this IEnumerable<TModel> enumerable, int count)
        {
            var result = new Select2PagedResult(enumerable.ToSelect2ListFromModel(), count);

            return result;
        }

        /// <summary>
        /// Maps an <see cref="IEnumerable{T}"/> collection to a <see cref="Select2PagedResult"/> object.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static Select2PagedResult ToSelect2PagedResult(this IEnumerable<object> enumerable, int count)
        {
            var result = new Select2PagedResult(enumerable.ToSelect2List(), count);

            return result;
        }

        /// <summary>
        /// Maps an object to a new <see cref="Select2PagedResult"/> with the specified accessors.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        /// <param name="idAccessor">A delegate to map a property of the model to the id.</param>
        /// <param name="textAccessor">A delegate to map a property of the model to the text.</param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static Select2PagedResult ToSelect2PagedResultFromModel<TModel>(this TModel model,
            Expression<Func<TModel, string>> idAccessor,
            Expression<Func<TModel, string>> textAccessor,
            int count)
        {
            var result = new Select2PagedResult
            {
                results = new List<Select2Model> { model.GetSelect2Model(idAccessor, textAccessor) },
                count_filtered = count
            };

            return result;
        }

        /// <summary>
        /// Maps an <see cref="IEnumerable{T}"/> collection to a <see cref="Select2PagedResult"/> object.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="idAccessor">A delegate to map a property of the model to the id.</param>
        /// <param name="textAccessor">A delegate to map a property of the model to the text.</param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static Select2PagedResult ToSelect2PagedResultFromModel<TModel>(this IEnumerable<TModel> enumerable,
            Expression<Func<TModel, string>> idAccessor,
            Expression<Func<TModel, string>> textAccessor,
            int count)
        {
            var result = new Select2PagedResult(enumerable.ToSelect2List(idAccessor, textAccessor), count);

            return result;
        }
    }
}
