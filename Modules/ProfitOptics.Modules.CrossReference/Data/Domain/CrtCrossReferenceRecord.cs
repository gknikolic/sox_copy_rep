using System;

namespace ProfitOptics.Modules.CrossReference.Data.Domain
{
    public partial class CrtCrossReferenceRecord
    {
        public int Id { get; set; }

        public int SubmittedBy { get; set; }

        public int CompetitorId { get; set; }

        public string CompetitorItemNumber { get; set; }

        public string CompetitorItemDescription { get; set; }

        public string CompetitorWebsiteUrl { get; set; }

        public int ItemId { get; set; }

        public int? CompatibilityOptionId { get; set; }

        public string Notes { get; set; }

        public bool Approved { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        public virtual CrtCompatibilityOption CrtCompatibilityOption { get; set; }

        public virtual CrtCompetitor CrtCompetitor { get; set; }

        public virtual QTItem QTItem { get; set; }
    }
}
