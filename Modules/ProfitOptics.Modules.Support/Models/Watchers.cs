﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Support.Models
{
    public class Watchers
    {
        public Watchers()
        {
            this.GlobalWatchers = new List<string>();
            this.Watcher = new List<Modules.Support.Models.IssueWatcher>();
        }

        public List<string> GlobalWatchers { get; set; }

        public List<Modules.Support.Models.IssueWatcher> Watcher { get; set; }

        #region Json storage inside the proj entity prop, db will be used, here for future usage
        //public static string GetJson(Watchers obj)
        //{
        //    var toReturn = string.Empty;

        //    var js = new JavaScriptSerializer();

        //    try
        //    {
        //        toReturn = js.Serialize(obj);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
        //    }

        //    return toReturn;
        //}

        //public static Watchers FromJson(string val)
        //{
        //    var toReturn = new Watchers();

        //    var js = new JavaScriptSerializer();

        //    try
        //    {
        //        toReturn = js.Deserialize<Watchers>(val);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
        //    }

        //    return toReturn;
        //}

        //public void AddWatch(string email, string issueId)
        //{
        //    if (this.GlobalWatchers.Any(e => e == email))
        //    {
        //        return;
        //    }

        //    var found = this.Watcher.Find(w => w.Email == email);
        //    if (found != null)
        //    {
        //        if (found.IssueIds != null)
        //        {
        //            if (!found.IssueIds.Any(i => i == issueId))
        //            {
        //                found.IssueIds.Add(issueId);
        //            }
        //        }
        //        else
        //        {
        //            found.IssueIds = new List<string>() { issueId };
        //        }
        //    }
        //    else
        //    {
        //        this.Watcher.Add(new IssueWatcher
        //        {
        //            Email = email,
        //            IssueIds = new List<string>() { issueId }
        //        });
        //    }
        //}

        //public void RemoveWatch(string email, string issueId)
        //{
        //    if (this.GlobalWatchers.Any(e => e == email))
        //    {
        //        return;
        //    }

        //    var found = this.Watcher.Find(w => w.Email == email);
        //    if (found != null)
        //    {
        //        if (found.IssueIds != null)
        //        {
        //            found.IssueIds.Remove(issueId);

        //            if (found.IssueIds.Count < 1)
        //            {
        //                found.IssueIds = null;
        //            }
        //        }
        //    }
        //}

        //public List<string> GetWatchedIssues(string email)
        //{
        //    var toReturn = new List<string>();

        //    var found = this.Watcher.Find(w => w.Email == email);
        //    if (found != null)
        //    {
        //        if (found.IssueIds != null)
        //        {
        //            toReturn = new List<string>(found.IssueIds);
        //        }
        //    }

        //    return toReturn;
        //}

        //public bool IsGlobalWatcher(string email)
        //{
        //    if (this.GlobalWatchers != null && this.GlobalWatchers.Count > 0)
        //    {
        //        if (this.GlobalWatchers.Any(e => e == email))
        //        {
        //            return true;
        //        }
        //    }

        //    return false;
        //}

        //public void AddGlobalWatcher(string email)
        //{
        //    if (this.GlobalWatchers != null && this.GlobalWatchers.Count > 0)
        //    {
        //        if (!this.GlobalWatchers.Any(e => e == email))
        //        {
        //            this.GlobalWatchers.Add(email);
        //        }
        //    }
        //    else
        //    {
        //        this.GlobalWatchers = new List<string>() { email };
        //    }
        //}

        //public void RemoveGlobalWatcher(string email)
        //{
        //    if (this.GlobalWatchers != null && this.GlobalWatchers.Count > 0)
        //    {
        //        this.GlobalWatchers.Remove(email);
        //    }
        //}

#endregion
    }
}