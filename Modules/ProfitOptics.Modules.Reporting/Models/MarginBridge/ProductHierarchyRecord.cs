﻿namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class ProductHierarchyRecord
    {
        public string ProductFocusCore { get; set; }

        public string ProductSuperBusinessUnit { get; set; }

        public string ProductSuperGroup { get; set; }

        public string ProductGroup { get; set; }

        public string ProductSubgroup { get; set; }
    }
}