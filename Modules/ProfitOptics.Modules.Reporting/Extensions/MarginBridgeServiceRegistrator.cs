﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProfitOptics.Framework.Core.Settings;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Modules.Reporting.Data;
using ProfitOptics.Modules.Reporting.Factories.MarginBridge;
using ProfitOptics.Modules.Reporting.Services.MarginBridge;

namespace ProfitOptics.Modules.Reporting.Extensions
{
    public sealed class MarginBridgeServiceRegistrator : IServiceRegistrator
    {
        public void RegisterServices(IServiceCollection services, IConfiguration configuration, ConnectionStrings connectionStrings, Auth auth = null)
        {
            services.AddDbContext<Entities>(options => options.UseSqlServer(connectionStrings.AreaEntities));

            services.AddScoped<IMarginBridgeFactory, MarginBridgeFactory>();
            services.AddScoped<IMarginBridgeService, MarginBridgeService>();
            services.AddScoped<IMarginBridgeSessionService, MarginBridgeSessionService>();
            services.AddScoped<IMarginBridgeAlgorithmService, MarginBridgeAlgorithmService>();
            services.AddScoped<IEPPlusExtensionService, EPPlusExtensionService>();
            services.AddTransient<IMarginPeriodRangeProvider, MarginPeriodRangeProvider>();
        }
    }
}