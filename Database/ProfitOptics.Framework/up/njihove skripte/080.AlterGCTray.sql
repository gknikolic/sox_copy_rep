USE [Sox]

ALTER TABLE [Sox].[dbo].[GCTray] ADD [CTContainerTypeActualId] INT NOT NULL;

GO

ALTER TABLE [dbo].[GCTray]  WITH CHECK ADD  CONSTRAINT [FK_GCTray_CTContainerTypeActual] FOREIGN KEY([CTContainerTypeActualId])
REFERENCES [dbo].[CTContainerTypeActual] ([Id])
GO
ALTER TABLE [dbo].[GCTray] CHECK CONSTRAINT [FK_GCTray_CTContainerTypeActual]
GO