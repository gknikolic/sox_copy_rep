USE [Sox]
ALTER TABLE [dbo].[GCCase] ADD CTCaseId INT NOT NULL;
ALTER TABLE [dbo].[GCCase] ADD OutgoingPickupOrderId NVARCHAR(50) NULL;
ALTER TABLE [dbo].[GCCase] ADD OutgoingDeliveryOrderId NVARCHAR(50) NULL;
ALTER TABLE [dbo].[GCCase] ADD IncomingPickupOrderId NVARCHAR(50) NULL;
ALTER TABLE [dbo].[GCCase] ADD IncomingDeliveryOrderId NVARCHAR(50) NULL;
ALTER TABLE [dbo].[GCCase] ADD RequestId NVARCHAR(50) NULL;

ALTER TABLE [dbo].[GCCase]  WITH CHECK ADD  CONSTRAINT [FK_GCCase_CTCase] FOREIGN KEY([CTCaseId])
REFERENCES [dbo].[CTCase] ([Id])
GO
ALTER TABLE [dbo].[GCCase] CHECK CONSTRAINT [FK_GCCase_CTCase]
GO