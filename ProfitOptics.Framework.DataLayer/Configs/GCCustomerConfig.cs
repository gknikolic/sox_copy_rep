﻿using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.DataLayer;

namespace ProfitOptics.Framework.DataLayer
{
	public class GCCustomerConfig : IEntityTypeConfiguration<GCCustomer>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<GCCustomer> builder)
        {
            builder.ToTable("GCCustomer");
            builder.HasKey(entity => entity.Id);
        }
    }


}
