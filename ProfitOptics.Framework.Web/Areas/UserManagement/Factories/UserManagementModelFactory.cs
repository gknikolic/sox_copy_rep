﻿using ProfitOptics.Framework.Core.Collections;
using ProfitOptics.Framework.Core.TokenizationSearch;
using ProfitOptics.Framework.Web.Areas.UserManagement.Models;
using ProfitOptics.Framework.Web.Models.Common;
using ProfitOptics.Framework.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Models.UserAssignments;
using IUserService = ProfitOptics.Framework.Web.Framework.Services.IUserService;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.Sox.Enums;
using OfficeOpenXml.FormulaParsing.Utilities;
using Microsoft.EntityFrameworkCore.Internal;

namespace ProfitOptics.Framework.Web.Areas.UserManagement.Factories
{
    public class UserManagementModelFactory : IUserManagementModelFactory
    {
        #region Fields

        private readonly IUserService _userService;
        private readonly IUserAssignmentService _userAssignmentService;

        #endregion

        #region Ctor

        public UserManagementModelFactory(IUserService userService,
            IUserAssignmentService userAssignmentService)
        {
            this._userService = userService;
            this._userAssignmentService = userAssignmentService;
        }

        #endregion

        #region Methods

        public PagedList<ApplicationUserListModel> CreateUserModels(IDataTablesRequest dataTablesRequest)
        {
            var users = _userService.GetUsersAsQueryable(true).Select(CreateUserManagementModelPredicate).AsQueryable();

            var pagedUser = ProcessRequest(dataTablesRequest, users);

            return pagedUser;
        }

        private PagedList<ApplicationUserListModel> ProcessRequest(IDataTablesRequest request, IQueryable<ApplicationUserListModel> users)
        {
            if (!string.IsNullOrWhiteSpace(request?.Search?.Value))
            {
                string[] tokens = Tokenizer.TokenizeToWords(request.Search.Value);

                foreach (var token in tokens)
                {
                    users = users.Where(user =>
                        user.FirstName != null &&
                        user.FirstName.Contains(token, StringComparison.InvariantCultureIgnoreCase) ||
                        user.LastName != null &&
                        user.LastName.Contains(token, StringComparison.InvariantCultureIgnoreCase) ||
                        user.Email != null && user.Email.Contains(token, StringComparison.InvariantCultureIgnoreCase));
                }
            }
            // sort
            users = users.ApplySort(request);

            // page
            var paged = users.ApplyPagination(request).ToList();

            // return data table response
            var totalCount = users.Count();


            return new PagedList<ApplicationUserListModel>(paged, totalCount);
        }

        private ApplicationUserListModel CreateUserManagementModelPredicate(ApplicationUser user)
        {
            return new ApplicationUserListModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Id = user.Id,
                IsEnabled = user.IsEnabled,
                IsLocked = _userService.IsLockedOutAsync(user).GetAwaiter().GetResult(),
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Roles = user.UserRoles?.Select(x => x.Role.Name).ToList(),
                Username = user.UserName
            };
        }

        public async Task<ApplicationUserModel> PrepareApplicationUserModelAsync(int userId)
        {
            var user = await _userService.FindUserByIdAsync(userId);
            if (user == null)
            {
                return null;
            }

            var soxRoles = Enum.GetNames(typeof(Modules.Sox.Enums.SoxUserRoleEnum)).ToList().Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
            var selectedRoleName = user.UserRoles.Select(x => x.Role.Name).ToList();

            if (user.UserRoles != null && user.UserRoles.Count > 0)
            {
                foreach (var userRole in user.UserRoles)
                {
                    var role = soxRoles.Where(x => x.Text == userRole.Role.Name).FirstOrDefault();
                    if(role != null)
                    {
                        role.Selected = true;
                    }
                }
            }

            var allParentCompanies = _userService.GetGCParentCompanies();
            //var userParentCompanies = _userService.GetParentCompaniesByUserId(user.Id).Select(x => x.Id).ToList();
            var userParentCompanies = user.GCParentCompanyUsers.Select(x => x.GCParentCompany.Id).ToList();
            if (userParentCompanies != null && userParentCompanies.Count > 0)
            {
                foreach (var userCompany in userParentCompanies)
                {
                    var company = allParentCompanies.Where(x => x.Value == x.ToString()).FirstOrDefault();
                    if(company != null)
                    {
                        company.Selected = true;
                    }
                }
            }

            var childCompanyPermissions = new List<string>();
            var userClaims = _userService.GetUserClaims(user.Id, "CompanyPermissionType").ToList();
            if (userClaims != null && userClaims.Count > 0)
            {
                userClaims.ForEach(x =>
                {
                    var claimValues = x.ClaimValue.Split(",").ToList();

                    claimValues.ForEach(claimValue =>
                    {
                        childCompanyPermissions.Add(string.Format("{0}_{1}", x.ClaimType, claimValue));
                    });
                });
            }

            return new ApplicationUserModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Phone = string.IsNullOrWhiteSpace(user.PhoneNumber) ? null : user.PhoneNumber,
                UserName = user.UserName,
                Title = user.POUserProfile?.Title,
                UserRoles = soxRoles,
                SelectedRoles = selectedRoleName,
                //ParentCompanies = userParentCompanies,
                SelectedParentCompanies = userParentCompanies,
                AllParentCompanies = allParentCompanies,
                ChildCompanyPermissions = childCompanyPermissions
            };
        }

        public IEnumerable<SelectListItem> PrepareUserRolesSelectListItems()
        {
            return Enum.GetValues(typeof(SoxUserRoleEnum)).Cast<SoxUserRoleEnum>()
                .Select(role => new SelectListItem
                {
                    Text = role.ToString(),
                    Value = role.ToString()
                });
        }

        public ParentCompanyModel PrepareParentCompanyModel(int parentCompanyId)
        {
            List<int> parentIds = new List<int>();
            parentIds.Add(parentCompanyId);
            var companies = _userService.GetChildrenForGCParentCompany(parentIds);

            return new ParentCompanyModel
            {
                Id = companies.ParentCompanies[0].Id,
                Title = companies.ParentCompanies[0].Title
            };
        }

        public ChildCompanyModel PrepareChildCompanyModel(int childCompanyId)
        {
            var company = _userService.GetChildCompanyById(childCompanyId);

            return new ChildCompanyModel
            {
                Id = company.Id,
                Title = company.Title
            };
        }
        #endregion
    }
}
