﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.DataLayer
{
    public class CTBILoadResultGraphic
    {
        public int Id { get; set; }
        public int CTBILoadResultId { get; set; }
        public int? GCImageId { get; set; }
        public long GraphicId { get; set; }
        public long LoadId { get; set; }
        public bool IsGlobal { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public string UserName { get; set; }
        public DateTime UpdateTimestamp { get; set; }
        public bool UseLocalImage { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime? ModifiedAtUtc { get; set; }
        public DateTime UpdateTimestampOriginal { get; set; }
        public virtual GCImage GCImage { get; set; }
    }
}
