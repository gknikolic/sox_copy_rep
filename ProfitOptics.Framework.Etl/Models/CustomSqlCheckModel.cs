﻿namespace ProfitOptics.Framework.Etl.Models
{
    public class CustomSqlCheckModel
    {
        public string TableName { get; set; }
        public string Constraint { get; set; }
        public string Sql { get; set; }
        public CustomCheckModelReturnType Type { get; set; }
    }
}
