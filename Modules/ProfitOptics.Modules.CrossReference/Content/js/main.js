﻿(function (crossReference, $, undefined) {
    // Private Properties
    var somePrivateProperty = true;

    // Public properties
    crossReference.somePublicProperty = {};

    // Public Methods
    crossReference.somePublicMethod = function () {
    };

    crossReference.removeCompetitor = function (competitorId, callback) {
        bootbox.confirm("Are you sure you want to remove this competitor?", function (result) {
            if (result == true) {
                app.blockUI("Please wait...");
                $.ajax({
                    type: 'post',
                    url: '/CrossReference/CrossReference/RemoveCompetitor',
                    data: {
                        competitorId: competitorId
                    },
                    dataType: 'html',
                    cache: false,
                    timeout: 1000000,
                    success: callback,
                    error: function (xr, data) {
                        app.unblockUI();
                        app.showAlertMessage("Error", "Error occurred while removing competitor!", "error");
                    }
                });
            }


        });
    };

    crossReference.updateCompetitor = function (competitorId, competitorName, callback) {
        app.blockUI("Please wait...");
        $.ajax({
            type: 'post',
            url: '/CrossReference/CrossReference/UpdateCompetitor',
            data: {
                competitorId: competitorId,
                competitorName: competitorName
            },
            dataType: 'html',
            cache: false,
            timeout: 1000000,
            success: callback,
            error: function (xr, data) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while trying to update competitor!", "error");
            }
        });
    };

    crossReference.addCompetitor = function (competitorName, callback) {
        app.blockUI("Please wait...");
        $.ajax({
            type: 'post',
            url: '/CrossReference/CrossReference/CreateCompetitor',
            data: {
                competitorName: competitorName
            },
            dataType: 'html',
            cache: false,
            timeout: 1000000,
            success: callback,
            error: function (xr, data) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while trying to create new competitor", "error");
            }
        });
    };

    crossReference.addCompatibilityOption = function (compatibilityName, callback) {
        app.blockUI("Please wait...");
        $.ajax({
            type: 'post',
            url: '/CrossReference/CrossReference/CreateCompatibilityOption',
            data: {
                compatibilityName: compatibilityName
            },
            dataType: 'html',
            cache: false,
            timeout: 1000000,
            success: callback,
            error: function (xr, data) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while trying to create compatibility option!", "error");
            }
        });
    };

    crossReference.removeCompatibilityOption = function (compatibilityId, callback) {
        bootbox.confirm("Are you sure you want to remove this competitor?", function (result) {
            if (result == true) {
                app.blockUI("Please wait...");
                $.ajax({
                    type: 'post',
                    url: '/CrossReference/CrossReference/RemoveCompatibilityOption',
                    data: {
                        compatibilityId: compatibilityId
                    },
                    dataType: 'html',
                    cache: false,
                    timeout: 1000000,
                    success: callback,
                    error: function (xr, data) {
                        app.unblockUI();
                        app.showAlertMessage("Error", "Error occurred while removing compatibility option!", "error");
                    }
                });
            }
        });
    };



    crossReference.updateCompatibilityOption = function (compatibilityOptionId, compatibilityOptionName, callback) {
        app.blockUI("Please wait...");
        $.ajax({
            type: 'post',
            url: '/CrossReference/CrossReference/UpdateCompatibilityOption',
            data: {
                compatibilityOptionId: compatibilityOptionId,
                compatibilityOptionName: compatibilityOptionName
            },
            dataType: 'html',
            cache: false,
            timeout: 1000000,
            success: callback,
            error: function (xr, data) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while trying to update compatibility option!", "error");
            }
        });

    };

    crossReference.removeCrossReferenceRecordForApproval = function (rowId, callback) {
        app.blockUI("Please wait...");
        $.ajax({
            type: 'post',
            url: '/CrossReference/CrossReference/RemoveCrossReferenceRecordForApproval',
            data: {
                id: rowId
            },
            dataType: 'html',
            cache: false,
            timeout: 1000000,
            success: callback,
            error: function (xr, data) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while removing items from the cross reference submitted", "error");
            }
        });
    };

    crossReference.approveCrossReferenceRecord = function (rowId, callback) {
        bootbox.confirm("Are you sure you want to approve this item?", function (result) {
            if (result == true) {
                app.blockUI("Please wait...");
                $.ajax({
                    type: 'post',
                    url: '/CrossReference/CrossReference/ApproveCrossReferenceRecord',
                    data: {
                        id: rowId
                    },
                    dataType: 'html',
                    cache: false,
                    timeout: 1000000,
                    success: callback,
                    error: function (xr, data) {
                        app.unblockUI();
                        app.showAlertMessage("Error", "Error occurred while removing items from the cross reference submitted", "error");
                    }
                });
            }

        });
    };

    crossReference.editCrossReferenceRecordForApproval = function (crossReferenceRecord, callback) {
        app.blockUI("Please wait...");
        $.ajax({
            type: 'post',
            url: '/CrossReference/CrossReference/EditCrossReferenceRecordForApproval',
            data: {
                crossReferenceRecord: crossReferenceRecord
            },
            dataType: 'html',
            cache: false,
            timeout: 1000000,
            success: callback,
            error: function (xr, data) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while editing items from the cross reference submitted", "error");
            }
        });
    };

    crossReference.addCrossReferenceRecord = function (crossReferenceRecord, callback) {
        app.blockUI("Please wait...");
        $.ajax({
            type: 'post',
            url: '/CrossReference/CrossReference/AddCrossReferenceRecord',
            data: {
                crossReferenceRecord: crossReferenceRecord
            },
            dataType: 'html',
            cache: false,
            timeout: 1000000,
            success: callback,
            error: function (xr, data) {
                app.unblockUI();
                app.showAlertMessage("Error", "Error occurred while adding cross reference item!", "error");
            }
        });

    };

    // Private Methods
    function somePrivateMethod(item) {

    }
}(window.crossReference = window.crossReference || {}, jQuery));