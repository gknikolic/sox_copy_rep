﻿using ProfitOptics.Framework.Web.Framework.Helpers.Notification;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Web.Framework.Services
{
    /// <summary>
    /// Service responsible for transfer TempData from one action method to another
    /// </summary>
    public interface INotificationService
    {
        void SetNextRequestNotification(NotificationType notificationType, string message);
        void SetNextRequestNotifications(NotificationType notificationType, IEnumerable<string> messages);
        NotificationType? GetNotificationType();
        IList<string> GetNotificationMessages();
        bool HasNotification();
    }
}