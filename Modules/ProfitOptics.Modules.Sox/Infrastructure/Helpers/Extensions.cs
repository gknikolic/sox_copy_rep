﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ProfitOptics.Modules.Sox.Infrastructure
{
	public static class Extensions
	{
        /// <summary>
        ///     A generic extension method that aids in reflecting 
        ///     and retrieving any attribute that is applied to an `Enum`.
        /// </summary>
        public static TAttribute GetAttribute<TAttribute>(this Enum enumValue)
                where TAttribute : Attribute
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<TAttribute>();
        }

        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
        {
            return items.GroupBy(property).Select(x => x.First());
        }

        public static string MapActionType(string source, int actionType)
        {
            if(source == "censi_track_module")
            {
                switch (actionType)
                {
                    case 1:
                        return "Get CT Cases";
                    case 2:
                        return "Get CT Facilities";
                    case 3:
                        return "Get CT Container Services";
                    case 4:
                        return "Get CT Containers By Service";
                    case 5:
                        return "Get CT Products";
                    case 6:
                        return "Get CT Vendors";
                    case 7:
                        return "Get CT Container Items";
                    case 8:
                        return "Get CT Case Containers";
                    case 9:
                        return "Get CT Cases Containers And Submit To WW";
                    case 10:
                        return "Get CT Container Actuals By Container Type";
                    case 11:
                        return "Get CT Location Types";
                    case 12:
                        return "Get CT Locations";
                    case 13:
                        return "Get Container Type Actual History";
                    case 14:
                        return "Get CT Container Type Graphic";
                    case 15:
                        return "Get CT Containe Type Graphic Media";
                    case 16:
                        return "Get CT BI Load Results";
                    case 17:
                        return "Sync Timestamps";
                    case 18:
                        return "Get Container Assembly Media For Actuals";
                    case 19:
                        return "Sync LC For Past Cases";
                    case 20:
                        return "Get Server Time";
                    case 21:
                        return "Create GC Image Thumbnails";
                }
                return "Not Found";
            }
            else if (source == "workwave_module")
            {
                switch (actionType)
                {
                    case 1:
                        return "Sync Order";
                    case 2:
                        return "Sync Routes";
                    case 3:
                        return "Delete Old ETL Logs";
                    case 4:
                        return "Get Orders From WorkWave";
                }
                return "Not Found";
            }
            else
            {
                switch (actionType)
                {
                    case 1:
                        return "Sync Sensors";
                }
                return "Not Found";
            }
        }

        public static string MapSource(string source)
        {
            if (source == "censi_track_module")
            {
                return "Censi Track Module";
            }
            else if (source == "workwave_module")
            {
                return "Workwave Module";
            }
            else
            {
                return "Imonnit Module";
            }
        }
    }
}
