﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.MarginBridge
{
    public class MarginBridgeStatisticsModel
    {
        public List<ProductHierarchyFilterModel> ProductNodes { get; set; }

        public List<LocationHierarchyFilterModel> LocationNodes { get; set; }

        public MarginBridgePeriod FirstPeriod { get; set; }

        public MarginBridgePeriod SecondPeriod { get; set; }

        public List<StatisticsResponse> Result { get; set; }

        public StatisticsResponse Totals { get; set; }

        public StatisticsResponse TotalsPermanent { get; set; }

        public string TableHeaderCaption { get; set; }

        public LocationLevelType LocationLevel { get; set; }

        public ProductLevelType ProductLevel { get; set; }

        public GroupByType GroupedBy { get; set; }

        public List<BreadcrumbItem> ProductsBreadcrumbs { get; set; }

        public List<BreadcrumbItem> LocationsBreadcrumbs { get; set; }

        public bool CanDrillThrough { get; set; }

        public string LocationFilterMaxLevelString { get; set; }

        public string ProductFilterMaxLevelString { get; set; }

        public int NumberOfRows { get; set; }

        public bool GroupedByMax { get; set; }

        public List<ProductHierarchyFilterModel> ProductsInitial { get; set; }

        public List<LocationHierarchyFilterModel> LocationsInitial { get; set; }

        public LocationLevelType MaxLocationLevel { get; set; }

        public ProductLevelType MaxProductLevel { get; set; }

        public bool IncludeNLItems { get; set; }

        public bool IncludeSDItems { get; set; }

        public bool IncludeListedItems { get; set; }

        public PricingLevelsModel PricingLevels { get; set; }

        public MarginBridgeType MarginBridgeType { get; set; }

        public MarginBridgeStatisticsChartModel Chart { get; set; }

        public string SelectedLocation =>
            GroupedBy == GroupByType.Location && !GroupedByMax ? "selected" : string.Empty;
        public string SelectedProduct =>
            GroupedBy == GroupByType.Product && !GroupedByMax ? "selected" : string.Empty;
        public string SelectedMaxProduct =>
            GroupedBy == GroupByType.Product && GroupedByMax ? "selected" : string.Empty;
        public string SelectedMaxLocation =>
            GroupedBy == GroupByType.Location && GroupedByMax ? "selected" : string.Empty;
    }

    public class MarginBridgeStatisticsChartModel
    {
            public decimal ChartStartMargin { get; set; }
            public decimal ChartPrice { get; set; }
            public decimal ChartCost { get; set; }
            public decimal ChartVolume { get; set; }
            public decimal ChartProductMix { get; set; }
            public decimal ChartRebateRate { get; set; }
            public decimal ChartRebateVol { get; set; }
            public decimal ChartRebateProdMix { get; set; }
            public decimal ChartEndMargin { get; set; }
    }
}