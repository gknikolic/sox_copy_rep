﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models.WorkWave
{
	public class Pod
	{
		[JsonProperty("signatures")]
		public int Sec { get; set; }
		[JsonProperty("text")]
		public string Text { get; set; }
		[JsonProperty("token")]
		public string Token { get; set; }
	}
}
