﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ProfitOptics.Framework.Etl.Models;

namespace ProfitOptics.Framework.Etl.DataLayer
{
    public partial class Entities : DbContext
    {
        public Entities()
        {
        }

        public Entities(DbContextOptions<Entities> options)
            : base(options)
        {
        }

        public virtual DbSet<ETLCTCase> CTCases { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("data source=s303.profitoptics.com; initial catalog=sox; user id=sox; password=sox;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);

            modelBuilder.Entity<ETLCTCase>(entity =>
            {
                entity.ToTable("CTCase");

                entity.Property(e => e.CaseReference).IsRequired();
                //entity.Property(e => e.DueTimestamp).IsRequired();
                //entity.Property(e => e.DueInMinutes).IsRequired();
                //entity.Property(e => e.DueInHoursAndMinutes).IsRequired();
                //entity.Property(e => e.DateTimeNow).IsRequired();
            });
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
