﻿using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Reporting.Enums;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Factories
{
    public interface IReportFilterModelFactory
    {
        /// <summary>
        /// Creates the report filter model for the specified user identifier.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ReportFilterModel CreateDefaultReportFilterModelForUserId(int userId);
        
        /// <summary>
        /// Creates the filter widget view model for the specified user identifier.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        FilterWidgetViewModel CreateFilterWidgetViewModel(int userId);

        /// <summary>
        /// Creates the selected filters model.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        SelectedFiltersModel CreateDefaultSelectedFiltersModel(int userId);

        /// <summary>
        /// Creates the selected filters for the specified user identifier and user filter identifier.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userFilterId"></param>
        /// <returns></returns>
        SelectedFiltersModel CreateSelectedFiltersModelForUserFilterId(int userId, int userFilterId);

        /// <summary>
        /// Creates the filter options for the specified parameters.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="filterType"></param>
        /// <param name="search"></param>
        /// <param name="page"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        Select2PagedResult CreateFilterOptions(int userId, FilterType filterType, string search, int page, int take);
    }
}
