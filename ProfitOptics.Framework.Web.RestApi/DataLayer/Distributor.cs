﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.Web.RestApi.DataLayer
{
    public class Distributor
    {
        public Distributor()
        {
            this.Customers = new HashSet<Customer>();

            this.DistributorProducts = new HashSet<DistributorProduct>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(150, ErrorMessage = "Length must be less then 150 characters")]
        public string Name { get; set; }
        
        public ICollection<Customer> Customers { get; set; }

        public ICollection<DistributorProduct> DistributorProducts { get; set; }
    }
}
