﻿namespace ProfitOptics.Modules.Reporting.Models.DailyTrend
{
    public class DailyTrendGroupedModel
    {
        public string GroupCode { get; set; }

        public string GroupName { get; set; }

        public decimal Sales { get; set; }

        public decimal GrossProfit { get; set; }

        public decimal GrossMarginPercent { get; set; }
    }
}
