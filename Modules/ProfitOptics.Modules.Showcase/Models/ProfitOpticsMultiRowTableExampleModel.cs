﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace ProfitOptics.Modules.Showcase.Models
{
    public class ProfitOpticsMultiRowTableExampleModel
    {
        public int Id { get; set; }
        public string TextValue { get; set; }
        public DateTime? DateValue { get; set; }
        public string DateString => DateValue.HasValue ? DateValue.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) : "";
        public double? PercentValue { get; set; }
        public decimal? CurrencyValue { get; set; }
        public string Notes { get; set; }
        public decimal? Price { get; set; }
    }

    public class ProfitOpticsMultiRowTableLevelsExampleModel
    {
        public string Id { get; set; }
        public string TextValue { get; set; }
        public DateTime? DateValue { get; set; }
        public string DateString => DateValue.HasValue ? DateValue.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) : "";
        public double? PercentValue { get; set; }

        public List<ProfitOpticsMultiRowTableLevelsExampleModel> Children { get; set; }
        public string ParentId { get; set; }
    }

    public class ProfitOpticsMultiRowTableLevelsAjaxExampleModel
    {
        public string Id { get; set; }
        public string TextValue { get; set; }
        public DateTime? DateValue { get; set; }
        public string DateString => DateValue.HasValue ? DateValue.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) : "";
        public double? PercentValue { get; set; }

        public bool HasChildren { get; set; }
        public string Level { get; set; }
        public string ParentId { get; set; }
    }
}