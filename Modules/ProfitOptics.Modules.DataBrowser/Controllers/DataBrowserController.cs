﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Controllers;
using ProfitOptics.Framework.Web.Framework.Helpers;
using ProfitOptics.Framework.Web.Framework.Helpers.Anotations;
using ProfitOptics.Framework.Web.Framework.Helpers.DataTables;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;
using ProfitOptics.Framework.Web.Framework.Models;
using ProfitOptics.Modules.DataBrowser.Factories;
using ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder;
using ProfitOptics.Modules.DataBrowser.Models;
using ProfitOptics.Modules.DataBrowser.Services;
using DatabaseTableResult = ProfitOptics.Modules.DataBrowser.Helpers.DatabaseTableResult;

namespace ProfitOptics.Modules.DataBrowser.Controllers
{
    [Area("DataBrowser")]
    [Authorize(Roles = "admin,superadmin")]
    public class DataBrowserController : BaseController
    {
        #region Fields

        private readonly IDataBrowserConfigurationService _dataBrowserConfigurationService;
        private readonly IDataBrowserValidatorService _dataBrowserValidator;
        private readonly IDataBrowserModelFactory _dataBrowserModelFactory;

        private string _inputInvalidMessage =
           "Input data is not valid. Query execution is prevented because of possible SQL injection. " +
           "Please try to filter the query using filters, as it doesn't imposes any limits to input values.";

        #endregion

        #region Ctor

        public DataBrowserController(IDataBrowserConfigurationService dataBrowserConfigurationService,
                                     IDataBrowserValidatorService dataBrowserValidator,
                                     IDataBrowserModelFactory dataBrowserModelFactory)
        {
            this._dataBrowserConfigurationService = dataBrowserConfigurationService;
            this._dataBrowserValidator = dataBrowserValidator;
            this._dataBrowserModelFactory = dataBrowserModelFactory;
        }

        #endregion

        #region Methods

        public IActionResult Index()
        {
            var model = _dataBrowserModelFactory.PrepareDataBrowserViewModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Tables(string databaseName, string searchQuery)
        {
            var result = _dataBrowserModelFactory.PrepareTables(databaseName, searchQuery);

            return Json(result);
        }

        public IActionResult Filter(string databaseName, string tableName)
        {
            var model = _dataBrowserModelFactory.PrepareFilterViewModel(databaseName, tableName);

            return PartialView("_Filter", model);

        }

        public IActionResult GroupBy(string databaseName, string tableName)
        {

            var columns = _dataBrowserModelFactory.PrepareTableDataColumns(databaseName, tableName);
            var model = new GroupByViewModel
            {
                Columns = columns,
                SelectedColumns = { }
            };

            return PartialView("_GroupBy", model);
        }

        [HttpPost]
        [AjaxCall]
        public IActionResult TableDataColumns(string databaseName, string tableName)
        {
            var columnModels = _dataBrowserModelFactory.PrepareTableDataColumns(databaseName, tableName);

            return Json(columnModels);
        }

        [HttpPost]
        public IActionResult TableData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest request, TableQueryModel tableQueryModel)
        {
            var dataBrowserResult = new DataBrowserResult();

            try
            {
                dataBrowserResult.Result = _dataBrowserModelFactory.PrepareTableData(request, tableQueryModel);

                if (dataBrowserResult.Result == null)
                {
                    dataBrowserResult.Status = ResultStatus.ERROR.ToString();
                    dataBrowserResult.Message =
                        "Input data is not valid. Query execution is prevented because of possible SQL injection. " +
                        "Please try to filter the query in 'Conditions' tab, as it does not imposes any limits to the input values.";
                }
            }
            catch(Exception ex)
            {
                dataBrowserResult.Status = ResultStatus.ERROR.ToString();
                dataBrowserResult.Message = "Error occurred while executing query: " + Environment.NewLine +
                                            Environment.NewLine + ex.Message;
            }

            return Json(new
            {
                MaxJsonLength = int.MaxValue,
                data = dataBrowserResult.Result.Data ?? Enumerable.Empty<object>(),
                recordsTotal = dataBrowserResult.Result.TotalCount,
                recordsFiltered = dataBrowserResult.Result.TotalFilteredCount,
                Status = dataBrowserResult.Status,
                Message = dataBrowserResult.Message
            });
           
        }

        [HttpPost]
        public IActionResult WhereStatement(TableQueryModel tableQueryModel)
        {
            var dataBrowserResult = _dataBrowserModelFactory.PrepareWhereStatement(tableQueryModel);

            return new LargeJsonResult(dataBrowserResult)
            {
                MaxJsonLength = int.MaxValue,
                Value = dataBrowserResult
            };
        }

        [HttpPost]
        public IActionResult PrepareDownload(TableQueryModel tableQueryModel)
        {
            var result = new DataBrowserResult();
            DataBrowserDownloadModel model = new DataBrowserDownloadModel();
            try
            {
                model = _dataBrowserModelFactory.PrepareDataBrowserDownloadModel(tableQueryModel);
                if (model == null)
                {
                    result.Status = ResultStatus.ERROR.ToString();
                    result.Message = _inputInvalidMessage;
                }

                if (!model.TooManyRecords)
                {
                    HttpContext.Session.SetObject(SessionKeys.DataBrowserModel, tableQueryModel);
                }
            }
            catch (Exception ex)
            {
                result.Status = ResultStatus.ERROR.ToString();
                result.Message = "Error occurred while executing query: " + Environment.NewLine + Environment.NewLine +
                                 ex.Message;
            }

            return Json(new
            {
                result.Status,
                result.Message,
                Result = new
                {
                    id = model.Id,
                    downloadWarning = model.DownloadWarning,
                    tooManyRecords = model.TooManyRecords,
                    count = model.Count
                }
               
            });
        }

        public IActionResult Download(string id)
        {
            var model = HttpContext.Session.GetObject<TableQueryModel>(SessionKeys.DataBrowserModel);
            if (model == null)
            {
                throw new Exception("TableQueryModel not found in session");
            }

            HttpContext.Session.Remove(SessionKeys.DataBrowserModel.ToString());
            var connectionString = _dataBrowserConfigurationService.GetConnectionString().ConnectionString;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                conn.ChangeDatabase(FormatDatabaseName(model.DatabaseName));

                var queryBuilder = new DataBrowserQueryBuilder(model, true);
                string tableName = model.TableName;
                SqlCommand dataCommand = queryBuilder.BuildDataQueryCommand(false);
                dataCommand.Connection = conn;

                return new DatabaseTableResult(tableName, string.Format("{0}.xls", tableName), "\t", dataCommand, connectionString);
            }
        }

        #endregion

        #region Util

        private string FormatDatabaseName(string databaseName)
        {
            if (databaseName.Contains('[') && databaseName.Contains(']'))
            {
                databaseName = databaseName.Substring(1, databaseName.Length - 2);
            }

            return databaseName;
        }

        #endregion
        
    }
}