﻿using Newtonsoft.Json;

namespace ProfitOptics.Modules.Sox.Models
{
	public class WorkWaveResponse
	{
		[JsonProperty("requestId")]
		public string RequestId { get; set; }
		public string Message { get; set; }
	}
}
