﻿namespace ProfitOptics.Modules.Support
{
    public static class SupportModuleKeys
    {
        public static class Session
        {
            public static readonly string SelectedProjectKey = "Jira_SelectedProjectKey";
            public static readonly string SelectedAssignee = "Jira_SelectedAssignee";
            public static readonly string SelectedReporter = "Jira_SelectedReporter";
            public static readonly string SelectedComponents = "Jira_SelectedComponents";
            public static readonly string SelectedComponent = "Jira_SelectedComponent";
        }

        public static class Cache
        {
            public static string ProjectBoardConfiguration(string projectKey) =>
                $"Jira_ProjectBoardConfiguration_{projectKey}";

            public static string ComponentsPerProject(string projectKey) =>
                $"Jira_ComponentsPerProject_{projectKey}";

            public static string IssueTypesPerProject(string projectKey) =>
                $"Jira_IssueTypesPerProject_{projectKey}";

            public static string PrioritiesPerProject =>
                $"Jira_PrioritiesPerProject";

            public static string JiraUsers => "JiraUsers";

            public static string Projects => "Jira_Projects";

            public static string Issue(string id) => $"Jira_Issue_{id}";
        }
    }
}