﻿using ProfitOptics.Framework.DataLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class GCCustomersModel
    {
        public List<GCCustomerModel> Customers { get; set; }
        public string QBCusomerExternalUrl { get; set; }
    }
}
