using System;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportHoliday
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public DateTime Date { get; set; }
        
        public string Country { get; set; }
    }
}
