﻿using System;

namespace ProfitOptics.Framework.Web.Framework.Models.Select2
{
    /// <summary>
    /// Represents a flag that indicates that the decorated property should be used as the id value for the <see cref="Select2Model"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class Select2IdAttribute : Attribute
    {
    }

    /// <summary>
    /// Represents a flag that indicates that the decorated property should be used as the text value for the <see cref="Select2Model"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class Select2TextAttribute : Attribute
    {
    }

    public class Select2AttributeNotPresentException : ArgumentException
    {
        public Select2AttributeNotPresentException(string message, string nameof) : base(message, nameof)
        {
        }
    }
}
