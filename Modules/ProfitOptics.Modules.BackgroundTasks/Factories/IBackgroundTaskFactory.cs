﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProfitOptics.Modules.BackgroundTasks.Models;

namespace ProfitOptics.Modules.BackgroundTasks.Factories
{
    public interface IBackgroundTaskFactory
    {
        /// <summary>
        /// Gets the background task view models for the specified user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>The background task view models for the specified user identifier.</returns>
        Task<List<BackgroundTaskViewModel>> GetActiveBackgroundTasksViewModelsAsync(int userId);
    }
}