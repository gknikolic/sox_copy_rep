﻿namespace ProfitOptics.Modules.Support
{
    public static class SupportModuleConstants
    {
        public static class Cache
        {
            public static readonly int CacheTime = 30;
        }

        public static class Jira
        {
            public static class ResourceNames
            {
                public static readonly string Board = "board";
                public static readonly string Configuration = "configuration";
                public static readonly string UsersPerProject = "user/assignable/search";
                public static readonly string Project = "project";
                public static readonly string Backlog = "backlog";
                public static readonly string Search = "search";
                public static readonly string Components = "components";
            }

            public static class ParameterNames
            {
                public static readonly string MaxResults = "maxResults";
                public static readonly string ProjectKeyOrId = "projectKeyOrId";
                public static readonly string Project = "project";
            }

            public static class BoardTypes
            {
                public static readonly string Kanban = "kanban";
                public static readonly string Scrum = "scrum";
            }

            public static class BoardColumnNames
            {
                public static readonly string Backlog = "Backlog";
                public static readonly string Closed = "Closed";
            }
        }
    }
}