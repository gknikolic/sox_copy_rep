﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.QuickBooks
{
    public class EditEstimateResponse : EditBase
    {
        public Estimate Estimate { get; set; }
    }
}
