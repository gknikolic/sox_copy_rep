USE [Sox]

BEGIN

CREATE TABLE CTContainerService
(
	[Id] [INT] PRIMARY KEY IDENTITY(1,1),
	[LegacyId] [INT] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[CreatedAtUtc] DateTime NOT NULL,
	[ModifiedAtUtc] DateTime NULL
)

END

