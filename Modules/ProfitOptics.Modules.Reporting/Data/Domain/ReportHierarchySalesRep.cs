using System;
using System.Collections.Generic;
using ProfitOptics.Framework.Web.Framework.Models.Select2;
using ProfitOptics.Modules.Reporting.Models.ReportFilter;

namespace ProfitOptics.Modules.Reporting.Data.Domain
{
    public class ReportHierarchySalesRep : IFilterableEntity
    {
        public ReportHierarchySalesRep()
        {
            ReportBillTos = new HashSet<ReportBillTo>();
            ReportSalesHistories = new HashSet<ReportSalesHistory>();
            ReportShipTos = new HashSet<ReportShipTo>();
        }

        [Select2Id]
        public int Id { get; set; }
        
        [Select2Text]
        public string Name { get; set; }

        public int? RegionId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        
        public virtual ICollection<ReportBillTo> ReportBillTos { get; set; }

        public virtual ReportHierarchyRegion ReportHierarchyRegion { get; set; }
        
        public virtual ICollection<ReportSalesHistory> ReportSalesHistories { get; set; }
        
        public virtual ICollection<ReportShipTo> ReportShipTos { get; set; }

        public virtual ICollection<ReportQuota> ReportQuotas { get; set; }
    }
}
