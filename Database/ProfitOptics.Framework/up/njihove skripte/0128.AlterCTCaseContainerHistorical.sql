USE [Sox]

ALTER TABLE [Sox].[dbo].[CTCaseContainerHistorical] DROP COLUMN [CurrentLocationName];
ALTER TABLE [Sox].[dbo].[CTCaseContainerHistorical] ADD [CurrentLocationName] NVARCHAR(200) NULL;

GO