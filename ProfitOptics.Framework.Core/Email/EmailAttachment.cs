﻿using System;
using System.IO;
using ProfitOptics.Framework.Core.System;

namespace ProfitOptics.Framework.Core.Email
{
    public class EmailAttachment
    {
        private readonly IIO _inputOutput;

        public EmailAttachment(string fileName, byte[] content, IIO inputOutput)
        {
            FileName = fileName;
            _inputOutput = inputOutput;
            SourceFileName = Path.Combine(_inputOutput.GetAttachmentsFolder(), Guid.NewGuid().ToString());
            Disposition = "inline";
        }

        public EmailAttachment(string fileName, string sourceFileName, IIO inputOutput)
        {
            FileName = fileName;
            SourceFileName = sourceFileName;
            _inputOutput = inputOutput;
            Disposition = "inline";
        }

        public string FileName { get; set; }
        public string SourceFileName { get; set; }
        public string Disposition { get; set; }
        public string Type { get; set; }
    }
}