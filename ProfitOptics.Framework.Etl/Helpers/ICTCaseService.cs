﻿using ProfitOptics.Framework.DataLayer;
using ProfitOptics.Framework.Etl.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Etl.Helpers
{
    public interface ICTCaseService
    {
        void AddCTCase(CTCase ctCase);

        void EditCTCase(CTCase ctCase);

        void AddOrEditCTCases(List<CTCase> ctCases);

        void AddOrEditCTCaseContainers(List<CTCaseContainer> ctCaseContainers, int serverOffsetInMinsFromUTC, bool? isStopLifecycleHistory = false);

        void CreateWorkWaveOrdersForCTCases();
        void SettingGCTrayActiveStatus(List<long?> ctContainerTypeActualLegacyIds);
        CTContainerType GetCTContainerByContainerTypeId(long containerTypeId);
        List<CTCaseContainer> CaseTesting();
        ProfitOptics.Framework.DataLayer.CTContainerService GetCTContainerServiceByLegacyId(int serviceId);
        CTContainerTypeActual GetCTContainerTypeActualByLegacyId(long value);
    }
}
