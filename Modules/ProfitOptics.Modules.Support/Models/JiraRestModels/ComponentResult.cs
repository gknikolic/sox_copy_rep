﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class ComponentResult
    {
        public string Id { get; set; }

        public string Self { get; set; }

        public string Name { get; set; }

        public string AssigneeType { get; set; }

        public ComponentAssignee Assignee { get; set; }

        public string RealAssigneeType { get; set; }

        public ComponentAssignee RealAssignee { get; set; }

        public bool IsAssigneeTypeValid { get; set; }

        public string Project { get; set; }

        public int ProjectId { get; set; }

        public bool Archived { get; set; }
    }

    public class ComponentAssignee
    {
        public string Self { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public bool Active { get; set; }
    }
}