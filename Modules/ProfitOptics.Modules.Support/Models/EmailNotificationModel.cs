﻿using ProfitOptics.Modules.Support.Attributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProfitOptics.Modules.Support.Models
{
    public class EmailNotificationModel
    {
        [Display(Name = "Email")]
        [EmailCollection]
        public List<string> Emails { get; set; }
    }
}