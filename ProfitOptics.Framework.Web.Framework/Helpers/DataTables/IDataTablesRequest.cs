﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
namespace ProfitOptics.Framework.Web.Framework.Helpers.DataTables
{
    public interface IDataTablesRequest
    {
        /// <summary>
        /// Gets and sets the draw counter from client-side to give back on the server's response.
        /// </summary>
        int Draw { get; set; }

        /// <summary>
        /// Gets and sets the start record number (count) for paging.
        /// </summary>
        int Start { get; set; }

        /// <summary>
        /// Gets and sets the length of the page (max records per page).
        /// </summary>
        int Length { get; set; }

        /// <summary>
        /// Gets and sets the global search pagameters.
        /// </summary>
        Search Search { get; set; }

        /// <summary>
        /// Gets and sets the read-only collection of client-side columns with their options and configs.
        /// </summary>
        ColumnCollection Columns { get; set; }

        /// <summary>
        /// Gets or sets the value indicating if the returned data should by viewed by sales.
        /// </summary>
        bool ViewBySales { get; set; }
    }
}
