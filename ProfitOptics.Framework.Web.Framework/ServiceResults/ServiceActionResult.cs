﻿namespace ProfitOptics.Framework.Web.Framework.ServiceResults
{
    public abstract class ServiceActionResult<TEnum> where TEnum : struct
    {
        protected ServiceActionResult()
        {
        }

        public string Error { get; set; }
        public TEnum Status { get; set; }

        public abstract bool Succeeded { get; }
    }
}
