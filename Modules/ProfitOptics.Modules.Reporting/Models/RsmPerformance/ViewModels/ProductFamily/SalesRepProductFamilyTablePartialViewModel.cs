﻿using System.Collections.Generic;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels.ProductFamily;
using ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.PartialTableViewModels;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.ViewModels.ProductFamily
{
    public class SalesRepProductFamilyTablePartialViewModel : TablePartialViewModelBase
    {
        public List<SalesRepTablePartialViewModel<ProductFamilyQuotaPerformanceDataModel>> ProductFamilies { get; set; }
    }
}