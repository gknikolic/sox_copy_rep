﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProfitOptics.Modules.CrossReference.Data.Domain;

namespace ProfitOptics.Modules.CrossReference.Data.Config
{
    public class QTItemConfig : IEntityTypeConfiguration<QTItem>
    {
        public void Configure(EntityTypeBuilder<QTItem> builder)
        {
            builder.ToTable("QTItem");
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.Company).HasMaxLength(50);
            builder.Property(entity => entity.ItemNum).HasMaxLength(50);
            builder.Property(entity => entity.ItemDescription).HasMaxLength(2000);
            builder.Property(entity => entity.SellingUOM).HasMaxLength(20);
            builder.Property(entity => entity.VendorId).HasMaxLength(50);
            builder.Property(entity => entity.VendorName).HasMaxLength(50);
            builder.Property(entity => entity.ClassId).HasMaxLength(50);
            builder.Property(entity => entity.CustomCustID).HasMaxLength(50);
        }
    }
}
