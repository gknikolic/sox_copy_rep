﻿using System;
using System.Collections.Generic;

namespace ProfitOptics.Framework.DataLayer
{
    public partial class POEmail
    {
        public POEmail()
        {
            POEmailAttachments = new HashSet<POEmailAttachment>();
        }

        public long Id { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public bool IsHtml { get; set; }
        public string Body { get; set; }
        public DateTime? DontSendBefore { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? SentTime { get; set; }

        public virtual ICollection<POEmailAttachment> POEmailAttachments { get; set; }
    }
}
