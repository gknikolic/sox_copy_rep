USE [Sox]

ALTER TABLE [dbo].[GCTraysLifecycleStatusTypes] ALTER COLUMN TimestampOriginal DATETIME2 NOT NULL;
ALTER TABLE [dbo].[CTContainerTypeActualHistoryItem] ALTER COLUMN UpdateTimestampOriginal DATETIME2 NOT NULL;
ALTER TABLE [dbo].[CTCaseContainer] ALTER COLUMN DueTimestampOriginal DATETIME2 NOT NULL;
ALTER TABLE [dbo].[CTCaseContainer] ALTER COLUMN EndTimestampOriginal DATETIME2 NULL;
ALTER TABLE [dbo].[CTCaseContainer] ALTER COLUMN UpdateTimestampOriginal DATETIME2 NULL;
ALTER TABLE [dbo].[CTCaseContainer] ALTER COLUMN MultipleBasisOriginal DATETIME2 NULL;
ALTER TABLE [dbo].[CTBILoadResult] ALTER COLUMN LoadDateOriginal DATETIME2 NULL;
ALTER TABLE [dbo].[CTBILoadResult] ALTER COLUMN InsertTimestampOriginal DATETIME2 NULL;
ALTER TABLE [dbo].[CTBILoadResult] ALTER COLUMN UpdateTimestampOriginal DATETIME2 NULL;
ALTER TABLE [dbo].[CTBILoadResult] ALTER COLUMN LoadTimestampOriginal DATETIME2 NULL;
ALTER TABLE [dbo].[CTBILoadResultContent] ALTER COLUMN LastUpdateOriginal DATETIME2 NOT NULL;
ALTER TABLE [dbo].[CTBILoadResultIndicator] ALTER COLUMN InsertTimestampOriginal DATETIME2 NULL;
ALTER TABLE [dbo].[CTBILoadResultIndicator] ALTER COLUMN UpdateTimestampOriginal DATETIME2 NULL;
ALTER TABLE [dbo].[CTBILoadResultGraphic] ALTER COLUMN UpdateTimestampOriginal DATETIME2 NOT NULL;
