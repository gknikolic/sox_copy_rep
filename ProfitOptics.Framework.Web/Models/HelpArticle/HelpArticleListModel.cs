﻿using System.Collections.Generic;

namespace ProfitOptics.Framework.Web.Models.HelpArticle
{
    public class HelpArticleListModel
    {
        public List<HelpArticleModel> Data { get; set; }
        public int TotalArticlesCount { get; set; }
    }
}