﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class TrayHistorySterilizeBIResult
    {
        public string LoadNumber { get; set; }
        public string ResultFlag { get; set; }
        public string Result { get; set; }
        public int CTContainerTypeActualId { get; set; }
        public string CTContainerTypeActualName { get; set; }
        public List<TrayHistorySterilizeBIIndicator> Indicators { get; set; }
        public List<string> ImagesData { get; set; }
    }
}
