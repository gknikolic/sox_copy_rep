USE [Sox]

CREATE TABLE GCTrayActualHistory
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[GCTrayId] INT NOT NULL,
	[CTContainerTypeActualId] INT NOT NULL,
	[CreatedAtUtc] DateTime NOT NULL
CONSTRAINT [PK_GCTrayActualHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[GCTrayActualHistory]  WITH CHECK ADD  CONSTRAINT [FK_GCTrayActualHistory_GCTray] FOREIGN KEY([GCTrayId])
REFERENCES [dbo].[GCTray] ([Id])
GO
ALTER TABLE [dbo].[GCTrayActualHistory] CHECK CONSTRAINT [FK_GCTrayActualHistory_GCTray]
GO

ALTER TABLE [dbo].[GCTrayActualHistory]  WITH CHECK ADD  CONSTRAINT [FK_GCTrayActualHistory_CTContainerTypeActual] FOREIGN KEY([CTContainerTypeActualId])
REFERENCES [dbo].[CTContainerTypeActual] ([Id])
GO
ALTER TABLE [dbo].[GCTrayActualHistory] CHECK CONSTRAINT [FK_GCTrayActualHistory_CTContainerTypeActual]
GO