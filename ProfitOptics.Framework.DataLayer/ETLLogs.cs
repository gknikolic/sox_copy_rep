﻿using System;

namespace ProfitOptics.Framework.DataLayer
{
    public partial class ETLLogs
    {
        public int Id { get; set; }
        public string Source { get; set; }
        public string FileName { get; set; }
        public string Username { get; set; }
        public DateTime ProcessingTime { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public int RowCount { get; set; }
        public int? ActionType { get; set; }
        public string Options { get; set; }
        public string Description { get; set; }
    }
}
