USE [Sox]

BEGIN
 
CREATE TABLE GCSettings
(
	[Id] INT PRIMARY KEY IDENTITY(1,1),
	[Environment] [nvarchar](50) NOT NULL,
	[VMAddress] [nvarchar](200) NOT NULL,
	[VMPickupStart] [nvarchar](50) NULL,
	[VMPickupEnd] [nvarchar](50) NOT NULL,
)

 END
