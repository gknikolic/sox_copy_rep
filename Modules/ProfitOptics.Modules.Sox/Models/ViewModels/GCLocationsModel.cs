﻿using ProfitOptics.Framework.DataLayer.GroundControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
    public class GCLocationsModel
    {
        public List<LocationModel> Locations { get; set; }
    }
}
