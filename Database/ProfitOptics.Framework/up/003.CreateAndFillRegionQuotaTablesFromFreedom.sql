USE [ProfitOptics.Framework]
GO
/****** Object:  Table [dbo].[ReportRegionQuota]    Script Date: 20.8.2019. 14:08:08 ******/


 if exists(SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ReportRegionQuota' AND TABLE_CATALOG = 'ProfitOptics.Framework')
 BEGIN
	Drop Table ReportRegionQuota
 END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportRegionQuota](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesRepId] [int] NOT NULL,
	[RegionId] [int] NOT NULL,
	[Month] [nvarchar](20) NOT NULL,
	[Year] [int] NOT NULL,
	[Revenue] [decimal](19, 4) NOT NULL,
 CONSTRAINT [PK_Report_Region_Quota] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ReportRegionQuota] ON 
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (1, 8, 6, N'Apr', 2019, CAST(228712.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (2, 8, 6, N'Aug', 2019, CAST(242152.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (3, 8, 6, N'Dec', 2019, CAST(205129.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (4, 8, 6, N'Feb', 2019, CAST(157867.0300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (5, 8, 6, N'Jan', 2019, CAST(143179.6500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (6, 8, 6, N'Jul', 2019, CAST(177989.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (7, 8, 6, N'Jun', 2019, CAST(171299.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (8, 8, 6, N'Mar', 2019, CAST(243118.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (9, 8, 6, N'May', 2019, CAST(206869.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (10, 8, 6, N'Nov', 2019, CAST(162651.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (11, 8, 6, N'Oct', 2019, CAST(118524.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (12, 8, 6, N'Sep', 2019, CAST(124303.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (13, 13, 7, N'Apr', 2019, CAST(231601.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (14, 13, 7, N'Aug', 2019, CAST(358852.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (15, 13, 7, N'Dec', 2019, CAST(322363.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (16, 13, 7, N'Feb', 2019, CAST(219664.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (17, 13, 7, N'Jan', 2019, CAST(284764.6000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (18, 13, 7, N'Jul', 2019, CAST(201089.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (19, 13, 7, N'Jun', 2019, CAST(295783.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (20, 13, 7, N'Mar', 2019, CAST(275557.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (21, 13, 7, N'May', 2019, CAST(329363.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (22, 13, 7, N'Nov', 2019, CAST(327434.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (23, 13, 7, N'Oct', 2019, CAST(319842.9900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (24, 13, 7, N'Sep', 2019, CAST(201993.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (25, 14, 6, N'Apr', 2019, CAST(246765.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (26, 14, 6, N'Aug', 2019, CAST(395886.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (27, 14, 6, N'Dec', 2019, CAST(369644.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (28, 14, 6, N'Feb', 2019, CAST(275322.6400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (29, 14, 6, N'Jan', 2019, CAST(182921.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (30, 14, 6, N'Jul', 2019, CAST(245345.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (31, 14, 6, N'Jun', 2019, CAST(306761.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (32, 14, 6, N'Mar', 2019, CAST(164294.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (33, 14, 6, N'May', 2019, CAST(288131.6100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (34, 14, 6, N'Nov', 2019, CAST(350899.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (35, 14, 6, N'Oct', 2019, CAST(379938.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (36, 14, 6, N'Sep', 2019, CAST(302551.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (37, 15, 7, N'Apr', 2019, CAST(253285.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (38, 15, 7, N'Aug', 2019, CAST(198555.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (39, 15, 7, N'Dec', 2019, CAST(171764.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (40, 15, 7, N'Feb', 2019, CAST(126496.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (41, 15, 7, N'Jan', 2019, CAST(128265.1200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (42, 15, 7, N'Jul', 2019, CAST(210962.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (43, 15, 7, N'Jun', 2019, CAST(174765.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (44, 15, 7, N'Mar', 2019, CAST(139965.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (45, 15, 7, N'May', 2019, CAST(178764.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (46, 15, 7, N'Nov', 2019, CAST(196314.0200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (47, 15, 7, N'Oct', 2019, CAST(213648.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (48, 15, 7, N'Sep', 2019, CAST(201232.7300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (49, 16, 7, N'Apr', 2019, CAST(221893.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (50, 16, 7, N'Aug', 2019, CAST(330353.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (51, 16, 7, N'Dec', 2019, CAST(359811.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (52, 16, 7, N'Feb', 2019, CAST(157764.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (53, 16, 7, N'Jan', 2019, CAST(151161.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (54, 16, 7, N'Jul', 2019, CAST(287455.4200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (55, 16, 7, N'Jun', 2019, CAST(290023.9700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (56, 16, 7, N'Mar', 2019, CAST(208176.4500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (57, 16, 7, N'May', 2019, CAST(362159.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (58, 16, 7, N'Nov', 2019, CAST(325040.9400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (59, 16, 7, N'Oct', 2019, CAST(210121.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (60, 16, 7, N'Sep', 2019, CAST(244444.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (61, 17, 6, N'Apr', 2019, CAST(240933.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (62, 17, 6, N'Aug', 2019, CAST(185659.4400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (63, 17, 6, N'Dec', 2019, CAST(186667.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (64, 17, 6, N'Feb', 2019, CAST(197686.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (65, 17, 6, N'Jan', 2019, CAST(191549.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (66, 17, 6, N'Jul', 2019, CAST(283698.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (67, 17, 6, N'Jun', 2019, CAST(292817.2700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (68, 17, 6, N'Mar', 2019, CAST(223417.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (69, 17, 6, N'May', 2019, CAST(205253.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (70, 17, 6, N'Nov', 2019, CAST(219158.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (71, 17, 6, N'Oct', 2019, CAST(279389.5000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (72, 17, 6, N'Sep', 2019, CAST(246352.2400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (73, 18, 5, N'Apr', 2019, CAST(175892.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (74, 18, 5, N'Aug', 2019, CAST(212379.1800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (75, 18, 5, N'Dec', 2019, CAST(193056.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (76, 18, 5, N'Feb', 2019, CAST(234507.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (77, 18, 5, N'Jan', 2019, CAST(189075.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (78, 18, 5, N'Jul', 2019, CAST(259543.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (79, 18, 5, N'Jun', 2019, CAST(158927.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (80, 18, 5, N'Mar', 2019, CAST(164613.8800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (81, 18, 5, N'May', 2019, CAST(200056.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (82, 18, 5, N'Nov', 2019, CAST(250888.8000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (83, 18, 5, N'Oct', 2019, CAST(227299.3400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (84, 18, 5, N'Sep', 2019, CAST(237992.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (85, 20, 6, N'Apr', 2019, CAST(86039.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (86, 20, 6, N'Aug', 2019, CAST(262906.9800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (87, 20, 6, N'Dec', 2019, CAST(257148.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (88, 20, 6, N'Feb', 2019, CAST(280815.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (89, 20, 6, N'Jan', 2019, CAST(190277.2500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (90, 20, 6, N'Jul', 2019, CAST(271059.3000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (91, 20, 6, N'Jun', 2019, CAST(177963.0500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (92, 20, 6, N'Mar', 2019, CAST(206812.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (93, 20, 6, N'May', 2019, CAST(264148.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (94, 20, 6, N'Nov', 2019, CAST(305947.6300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (95, 20, 6, N'Oct', 2019, CAST(226467.3700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (96, 20, 6, N'Sep', 2019, CAST(232761.1500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (97, 21, 5, N'Apr', 2019, CAST(239153.3000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (98, 21, 5, N'Aug', 2019, CAST(314357.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (99, 21, 5, N'Dec', 2019, CAST(317963.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (100, 21, 5, N'Feb', 2019, CAST(205008.5300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (101, 21, 5, N'Jan', 2019, CAST(247069.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (102, 21, 5, N'Jul', 2019, CAST(256063.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (103, 21, 5, N'Jun', 2019, CAST(287918.7000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (104, 21, 5, N'Mar', 2019, CAST(265564.8200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (105, 21, 5, N'May', 2019, CAST(324963.4700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (106, 21, 5, N'Nov', 2019, CAST(325241.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (107, 21, 5, N'Oct', 2019, CAST(264436.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (108, 21, 5, N'Sep', 2019, CAST(310607.3800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (109, 25, 5, N'Apr', 2019, CAST(129293.8600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (110, 25, 5, N'Aug', 2019, CAST(209147.4300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (111, 25, 5, N'Dec', 2019, CAST(158550.8700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (112, 25, 5, N'Feb', 2019, CAST(165861.4800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (113, 25, 5, N'Jan', 2019, CAST(106060.9300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (114, 25, 5, N'Jul', 2019, CAST(156233.2600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (115, 25, 5, N'Jun', 2019, CAST(198669.1900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (116, 25, 5, N'Mar', 2019, CAST(169354.3200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (117, 25, 5, N'May', 2019, CAST(193848.8100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (118, 25, 5, N'Nov', 2019, CAST(211300.2800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (119, 25, 5, N'Oct', 2019, CAST(243763.5600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (120, 25, 5, N'Sep', 2019, CAST(262003.5200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (121, 27, 6, N'Apr', 2019, CAST(164901.6800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (122, 27, 6, N'Aug', 2019, CAST(237024.2100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (123, 27, 6, N'Dec', 2019, CAST(254474.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (124, 27, 6, N'Feb', 2019, CAST(231730.3300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (125, 27, 6, N'Jan', 2019, CAST(254131.6700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (126, 27, 6, N'Jul', 2019, CAST(200087.0900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (127, 27, 6, N'Jun', 2019, CAST(260509.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (128, 27, 6, N'Mar', 2019, CAST(233974.6900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (129, 27, 6, N'May', 2019, CAST(261474.7900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (130, 27, 6, N'Nov', 2019, CAST(170632.8300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (131, 27, 6, N'Oct', 2019, CAST(213360.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (132, 27, 6, N'Sep', 2019, CAST(167263.8500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (133, 33, 5, N'Apr', 2019, CAST(194449.6200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (134, 33, 5, N'Aug', 2019, CAST(300320.5800 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (135, 33, 5, N'Dec', 2019, CAST(308149.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (136, 33, 5, N'Feb', 2019, CAST(207392.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (137, 33, 5, N'Jan', 2019, CAST(154898.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (138, 33, 5, N'Jul', 2019, CAST(212081.0400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (139, 33, 5, N'Jun', 2019, CAST(255378.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (140, 33, 5, N'Mar', 2019, CAST(277537.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (141, 33, 5, N'May', 2019, CAST(315149.7600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (142, 33, 5, N'Nov', 2019, CAST(253746.9600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (143, 33, 5, N'Oct', 2019, CAST(309827.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (144, 33, 5, N'Sep', 2019, CAST(277004.8900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (145, 35, 7, N'Apr', 2019, CAST(188925.5500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (146, 35, 7, N'Aug', 2019, CAST(260675.7300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (147, 35, 7, N'Dec', 2019, CAST(182711.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (148, 35, 7, N'Feb', 2019, CAST(246399.5700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (149, 35, 7, N'Jan', 2019, CAST(144756.2000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (150, 35, 7, N'Jul', 2019, CAST(306696.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (151, 35, 7, N'Jun', 2019, CAST(258056.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (152, 35, 7, N'Mar', 2019, CAST(277963.2900 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (153, 35, 7, N'May', 2019, CAST(189711.9000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (154, 35, 7, N'Nov', 2019, CAST(288971.1100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (155, 35, 7, N'Oct', 2019, CAST(331203.4600 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (156, 35, 7, N'Sep', 2019, CAST(237737.2200 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (157, 37, 5, N'Apr', 2019, CAST(216394.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (158, 37, 5, N'Aug', 2019, CAST(189681.7400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (159, 37, 5, N'Dec', 2019, CAST(160350.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (160, 37, 5, N'Feb', 2019, CAST(178629.4000 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (161, 37, 5, N'Jan', 2019, CAST(120394.5400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (162, 37, 5, N'Jul', 2019, CAST(233244.4100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (163, 37, 5, N'Jun', 2019, CAST(215868.8400 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (164, 37, 5, N'Mar', 2019, CAST(224980.9500 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (165, 37, 5, N'May', 2019, CAST(167350.1300 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (166, 37, 5, N'Nov', 2019, CAST(170233.7700 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (167, 37, 5, N'Oct', 2019, CAST(211260.5100 AS Decimal(19, 4)))
GO
INSERT [dbo].[ReportRegionQuota] ([Id], [SalesRepId], [RegionId], [Month], [Year], [Revenue]) VALUES (168, 37, 5, N'Sep', 2019, CAST(202101.4900 AS Decimal(19, 4)))
GO
SET IDENTITY_INSERT [dbo].[ReportRegionQuota] OFF
GO

