﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
namespace ProfitOptics.Modules.DataBrowser.Helpers.QueryBuilder
{
    public class SqlLiteral
    {
        public static string StatementRowsAffected = "SELECT @@ROWCOUNT";

        public SqlLiteral(string value)
        {
            Value = value;
        }

        public string Value { get; set; }
    }
}