﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Framework.DataLayer
{
    public partial class GCProduct
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int ProductTypeId { get; set; }
        public int ContainerId { get; set; }
        public virtual GCProductType ProductType { get; set; }
        public virtual GCContainer Container { get; set; }
    }
}
