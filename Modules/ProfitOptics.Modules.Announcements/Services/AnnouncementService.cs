﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ProfitOptics.Framework.Web.Framework;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using ProfitOptics.Framework.Web.Framework.Infrastructure;
using ProfitOptics.Framework.Web.Framework.Services;
using ProfitOptics.Modules.Announcements.Data;
using ProfitOptics.Modules.Announcements.Data.Domain;
using ProfitOptics.Modules.Announcements.Models;

namespace ProfitOptics.Modules.Announcements.Services
{
    public class AnnouncementService : IAnnouncementService
    {
        private readonly Entities _context;
        private readonly IMemoryCacheHelper _memoryCacheHelper;
        private readonly IUserService _userService;

        public AnnouncementService(Entities context,
            IMemoryCacheHelper memoryCacheHelper,
            IUserService userService)
        {
            _context = context;
            _memoryCacheHelper = memoryCacheHelper;
            _userService = userService;
        }

        /// <inheritdoc />
        public void Save(int userId, IFormCollection form)
        {
            int? id = !string.IsNullOrEmpty(form["Id"]) && form["Id"] != "" ? (int?)Convert.ToInt32(form["Id"]) : null;

            var announcement = new POAnnouncement();

            if (id.HasValue)
            {
                announcement = _context.POAnnouncements.Single(a => a.Id == id);
            }

            UpdateAnnouncement(userId, form, announcement);

            UpdateAnnouncementRoles(form, announcement);

            if (!id.HasValue)
            {
                announcement.CreateTime = DateTime.UtcNow;

                _context.POAnnouncements.Add(announcement);
            }

            _memoryCacheHelper.Set(SessionKeys.AnnouncementUpdateTime.ToString(), DateTime.UtcNow, 30);

            _context.SaveChanges();
        }

        /// <inheritdoc />
        public void Delete(int id)
        {
            if (id <= 0)
            {
                return;
            }

            POAnnouncement announcement = _context.POAnnouncements.Single(a => a.Id == id);

            announcement.IsDeleted = true;

            _context.SaveChanges();
        }

        /// <inheritdoc />
        public void MarkAnnouncementAsRead(int userId, int id)
        {
            AspNetUser user = _context.AspNetUsers.Single(u => u.Id == userId);

            if (user is null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            POAnnouncement announcement = _context.POAnnouncements.Single(a => a.Id == id);

            if (announcement is null)
            {
                throw new ArgumentNullException(nameof(announcement));
            }

            _context.POAnnouncementReads.Add(new POAnnouncementRead
            {
                UserId = user.Id,
                AnnouncementId = announcement.Id,
                ReadDate = DateTime.UtcNow
            });

            _context.SaveChanges();
        }

        /// <inheritdoc />
        public POAnnouncement GetAnnouncementById(int id)
        {
            POAnnouncement announcement = _context.POAnnouncements
                .Single(a => a.Id == id && (a.IsDeleted == null || a.IsDeleted == false));
            
            return announcement;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<POAnnouncement>> GetAnnouncementsForUserIdAsync(int userId, bool getActive = false)
        {
            DateTime utcNow = DateTime.UtcNow;
            
            ApplicationUser user = await _userService.FindUserByIdAsync(userId);
            
            if (user == null)
            {
                return null;
            }

            IList<ApplicationRole> userRoles = await _userService.GetUserRolesAsync(user);

            //IEnumerable<POAnnouncement> results = (from announcement in _context.POAnnouncements
            //    where utcNow < announcement.ValidUntil &&
            //          (announcement.IsDeleted == null || announcement.IsDeleted == false) &&
            //          (!getActive || announcement.POAnnouncementReads.All(q => q.UserId != userId)) &&
            //          announcement.POAnnouncementRoles.Any(q => userRoles.Any(x => x.Id == q.RoleId))
            //    select announcement).ToList();

            IQueryable<POAnnouncement> queryable;

            if (!getActive)
            {
                queryable = _context.POAnnouncements
                    .Include(announcement => announcement.POAnnouncementReads)
                    .AsNoTracking();
            }
            else
            {
                queryable = _context.POAnnouncements
                       .AsNoTracking();
            }
            queryable = queryable
                .Where(announcement => utcNow < announcement.ValidUntil 
                    && (announcement.IsDeleted == null || announcement.IsDeleted == false)
                    && (!getActive || announcement.POAnnouncementReads.All(q => q.UserId != userId)));

            var announcements = await queryable.ToListAsync();

            List<POAnnouncement> results = new List<POAnnouncement>();

            foreach (var announcement in results)
            {
                var annRoles = await _context.POAnnouncementRoles.Where(ar => ar.AnnouncementId == announcement.Id && userRoles.Any(ur => ur.Id == ar.RoleId)).ToListAsync();
                if (annRoles.Count > 0)
                {
                    results.Add(announcement);
                }
            }

            return results;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<POAnnouncementWithProfilePicture>> GetAnnouncementsWithProfilePicturesForUserIdAsync(int userId,
            bool getActive = false)
        {
            IEnumerable<POAnnouncement> announcements = await GetAnnouncementsForUserIdAsync(userId, getActive);

            IEnumerable<POAnnouncementWithProfilePicture> result = from announcement in announcements
                         from profilePicture in _userService.GetProfilePicturesQueryable().Where(x => x.UserId == announcement.UserId).DefaultIfEmpty()
                         select new POAnnouncementWithProfilePicture(announcement, profilePicture?.Data);

            return result;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<POAnnouncement>> SearchAnnouncementsAsync(int? userId = null, bool validOnly = false, bool activeOnly = false)
        {
            DateTime nowUtc = DateTime.UtcNow;

            IEnumerable<POAnnouncement> announcementsQuery;

            if (userId.HasValue)
            {
                announcementsQuery = await GetAnnouncementsForUserIdAsync(userId.Value, activeOnly);
            }
            else
            {
                announcementsQuery = from announcement in _context.POAnnouncements
                                     where announcement.IsDeleted == null || announcement.IsDeleted == false
                                     select announcement;
            }

            return announcementsQuery.Where(x => !validOnly || x.ValidUntil > nowUtc);
        }

        private static void UpdateAnnouncement(int userId, IFormCollection form, POAnnouncement announcement)
        {
            announcement.UserId = userId;
            announcement.Text = form["content"];
            announcement.Title = form["Title"];
            announcement.ValidUntil = Convert.ToDateTime(form["ValidUntil"]);
        }

        private void UpdateAnnouncementRoles(IFormCollection form, POAnnouncement announcement)
        {
            List<int> currentAnnouncementRoleIds = announcement
                .POAnnouncementRoles
                .Select(announcementRole => announcementRole.RoleId)
                .ToList();

            List<int> newAnnouncementRoleIds = form.Keys.Where(key => key.StartsWith("Role_"))
                .Where(key => IsChecked(form, key))
                .Select(key => Convert.ToInt32(key.Substring(5)))
                .ToList();

            List<int> roleIdsToRemove = currentAnnouncementRoleIds
                .Where(roleId => !newAnnouncementRoleIds.Contains(roleId))
                .ToList();

            RemoveAnnouncementRoles(announcement, roleIdsToRemove);

            AddAnnouncementRoles(announcement, newAnnouncementRoleIds, currentAnnouncementRoleIds);
        }

        private static void AddAnnouncementRoles(POAnnouncement announcement, IEnumerable<int> newAnnouncementRoleIds,
            ICollection<int> currentAnnouncementRoleIds)
        {
            List<int> roleIdsToAdd = newAnnouncementRoleIds
                .Where(roleId => !currentAnnouncementRoleIds.Contains(roleId))
                .ToList();

            foreach (int roleId in roleIdsToAdd)
            {
                announcement.POAnnouncementRoles.Add(new POAnnouncementRole
                {
                    RoleId = roleId
                });
            }
        }

        private void RemoveAnnouncementRoles(POAnnouncement announcement, List<int> roleIdsToRemove)
        {
            if (roleIdsToRemove == null)
            {
                throw new ArgumentNullException(nameof(roleIdsToRemove));
            }

            List<POAnnouncementRole> rolesToRemove = announcement
                .POAnnouncementRoles
                .Where(announcementRole => roleIdsToRemove.Contains(announcementRole.RoleId))
                .ToList();

            if (!rolesToRemove.Any())
            {
                return;
            }

            foreach (POAnnouncementRole role in rolesToRemove)
            {
                announcement.POAnnouncementRoles.Remove(role);
            }
        }

        private static bool IsChecked(IFormCollection form, string key)
        {
            return form[key].Contains("true");
        }
    }
}