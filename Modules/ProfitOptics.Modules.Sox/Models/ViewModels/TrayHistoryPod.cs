﻿namespace ProfitOptics.Modules.Sox.Models.ViewModels
{
	public class TrayHistoryPod
	{
		public string Url { get; set; }
		public string Data { get; set; }
		public string Token { get; set; }
		public string Note { get; set; }
		public int? ImageId { get; set; }
	}
}
