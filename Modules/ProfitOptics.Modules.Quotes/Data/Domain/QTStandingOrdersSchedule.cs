using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProfitOptics.Modules.Quotes.Data.Domain
{
    [Table("QTStandingOrdersSchedule")]
    public partial class QTStandingOrdersSchedule
    {
        public int Id { get; set; }

        public string Company { get; set; }

        public int? CustNum { get; set; }

        public string CustID { get; set; }

        public string CustName { get; set; }

        public int? OrderNum { get; set; }

        public int? OrderLineNum { get; set; }

        public string PartNum { get; set; }

        public decimal? OrderQty { get; set; }

        public string StandingOrderCode { get; set; }

        public string OrderPattern { get; set; }

        public string OrderWeek { get; set; }

        public DateTime? OrderStartDate { get; set; }

        public DateTime? OrderEndDate { get; set; }

        public string ShipToNum { get; set; }

        public string ShipToName { get; set; }

        public bool? OpenOrder { get; set; }

        public bool? OrderHeld { get; set; }

        public string PONumber { get; set; }

        public DateTime? RequestDate { get; set; }
    }
}
