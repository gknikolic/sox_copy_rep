﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProfitOptics.Modules.Sox.Models.CensiTrack.ETL
{
    public class ETLCTContainerTypeActualHistoryResponse
    {
        [JsonProperty("data")]
        public List<ETLCTContainerTypeActualHistoryItem> ETLCTContainerTypeActualHistoryItems { get; set; }
    }
}
