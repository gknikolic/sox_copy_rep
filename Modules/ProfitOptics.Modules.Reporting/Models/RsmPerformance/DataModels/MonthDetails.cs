﻿using System.Collections.Generic;

namespace ProfitOptics.Modules.Reporting.Models.RsmPerformance.DataModels
{
    public class MonthDetails
    {
        public string CurrentMonthName { get; set; }
        public List<string> PreviousMonthsNames { get; set; }
        public int WorkDaysCount { get; set; }
        public int WorkDaysToDateCount { get; set; }
        public int RemainingWorkDaysCount { get; set; }
    }
}