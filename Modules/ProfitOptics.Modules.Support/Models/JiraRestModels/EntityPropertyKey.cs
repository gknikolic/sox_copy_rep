﻿namespace ProfitOptics.Modules.Support.Models.JiraRestModels
{
    public class EntityPropertyKey
    {
        public string key { get; set; }
        public string self { get; set; }
    }
}