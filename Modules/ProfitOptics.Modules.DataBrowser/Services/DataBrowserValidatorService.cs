﻿// Copyright (c) 2018, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.

using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using ProfitOptics.Modules.DataBrowser.Helpers;
using ProfitOptics.Modules.DataBrowser.Models;

namespace ProfitOptics.Modules.DataBrowser.Services
{
    public class DataBrowserValidatorService : IDataBrowserValidatorService
    {
        private static readonly string[] disabledKeywords =
        {
            "ADD", "EXTERNAL", "PROCEDURE", "ALL", "FETCH", "PUBLIC", "ALTER", "FILE", "RAISERROR", "FILLFACTOR", "READ",
            "ANY", "FOR", "READTEXT", "FOREIGN",
            "RECONFIGURE", "FREETEXT", "REFERENCES", "AUTHORIZATION", "FREETEXTTABLE", "REPLICATION", "BACKUP", "FROM",
            "RESTORE", "BEGIN", "FULL", "RESTRICT", "FUNCTION", "RETURN", "BREAK", "GOTO",
            "REVERT", "BROWSE", "GRANT", "REVOKE", "BULK", "GROUP", "BY", "HAVING", "ROLLBACK", "CAST", "CASCADE",
            "HOLDLOCK", "ROWCOUNT", "IDENTITY", "ROWGUIDCOL", "CHECK", "IDENTITY_INSERT", "RULE",
            "CHECKPOINT", "IDENTITYCOL", "SAVE", "CLOSE", "IF", "SCHEMA", "CLUSTERED", "SECURITYAUDIT", "INDEX",
            "SELECT", "COLLATE", "INNER", "SEMANTICKEYPHRASETABLE", "COLUMN", "INSERT",
            "SEMANTICSIMILARITYDETAILSTABLE", "COMMIT", "INTERSECT", "SEMANTICSIMILARITYTABLE", "COMPUTE", "INTO",
            "SESSION_USER", "CONSTRAINT", "SET", "CONTAINS", "JOIN", "SETUSER",
            "CONTAINSTABLE", "SHUTDOWN", "CONTINUE", "KILL", "SOME", "CONVERT", "STATISTICS", "CREATE", "SYSTEM_USER",
            "CROSS", "LINENO", "TABLE", "CURRENT", "LOAD", "TABLESAMPLE", "MERGE",
            "TEXTSIZE", "NATIONAL", "CURRENT_TIMESTAMP", "NOCHECK", "CURRENT_USER", "NONCLUSTERED", "TOP", "CURSOR",
            "DATABASE", "TRANSACTION", "DBCC", "NULLIF", "TRIGGER", "DEALLOCATE",
            "OF", "TRUNCATE", "DECLARE", "OFF", "TRY_CONVERT", "DEFAULT", "OFFSETS", "TSEQUAL", "DELETE", "UNION",
            "DENY", "OPEN", "UNIQUE", "DESC", "OPENDATASOURCE", "UNPIVOT", "DISK",
            "OPENQUERY", "UPDATE", "DISTINCT", "OPENROWSET", "UPDATETEXT", "DISTRIBUTED", "OPENXML", "USE", "DOUBLE",
            "OPTION", "USER", "DROP", "VALUES", "DUMP", "ORDER", "VARYING", "ELSE", "OUTER",
            "VIEW", "OVER", "WAITFOR", "ERRLVL", "PERCENT", "ESCAPE", "PIVOT", "WHERE", "PLAN", "WHILE", "EXEC",
            "PRECISION", "WITH", "EXECUTE", "PRIMARY", "WITHIN GROUP", "EXISTS", "PRINT",
            "WRITETEXT", "EXIT", "PROC", ";", "--", "/*", "*/", "xp_", "sp_executesql", "`", "\\"
        };

        private static readonly List<string> allowedDatabases = new List<string>();
        private static readonly IDictionary<string, IEnumerable<string>> allowedTables = new Dictionary<string, IEnumerable<string>>();
        private static readonly IDictionary<string, IEnumerable<string>> validColumns = new Dictionary<string, IEnumerable<string>>();

        private readonly SqlConnection sqlConnection;
        private readonly IDataBrowserConfigurationService _dataBrowserConfigurationService;

        public DataBrowserValidatorService(IDataBrowserConfigurationService dataBrowserConfigurationService)
        {
            this._dataBrowserConfigurationService = dataBrowserConfigurationService;
            this.sqlConnection = new SqlConnection(_dataBrowserConfigurationService.GetConnectionString().ConnectionString);

            allowedDatabases.AddRange(_dataBrowserConfigurationService.GetAllowedDatabases().Select(x => x.ToLower()));
        }

        public bool ValidateDatabaseName(string databaseName)
        {
            if (string.IsNullOrEmpty(databaseName))
            {
                return false;
            }
            return allowedDatabases.Contains(databaseName.ToLower());
        }

        public bool ValidateTableName(string databaseName, string tableName)
        {
            if (!allowedTables.ContainsKey(databaseName))
            {
                IEnumerable<string> validTableNames = _dataBrowserConfigurationService.GetTablesFromDatabase(databaseName)
                                                                                      .Select(x => x.Name.ToLower());
                allowedTables[databaseName] = validTableNames;
            }
            return allowedTables[databaseName].Contains(tableName.ToLower());
        }

        public bool ValidateTableQuery(TableQueryModel tableQueryModel)
        {
            return ValidateDatabaseName(tableQueryModel.DatabaseName)
                   && ValidateTableName(tableQueryModel.DatabaseName, tableQueryModel.TableName)
                   && ValidateColumn(tableQueryModel.DatabaseName, tableQueryModel.TableName, tableQueryModel.SortColumnName)
                   && ValidateColumns(tableQueryModel)
                   && ValidateDynamicWhereStatement(tableQueryModel);
        }

        private bool ValidateColumn(string databaseName, string tableName, string columnName)
        {
            if (string.IsNullOrEmpty(columnName)) // can be null or empty if table is not sortable
            {
                return true;
            }

            if (!validColumns.ContainsKey(tableName))
            {
                var dataBrowserColumns = new DataBrowserColumns(sqlConnection, databaseName);
                validColumns[tableName] = dataBrowserColumns.GetColumns(tableName).Select(x => x.ColumnName.ToLower());
            }
            return validColumns[tableName].Contains(columnName.ToLower());
        }

        private bool ValidateColumns(TableQueryModel queryModel)
        {
            if (queryModel.Conditions != null)
            {
                return queryModel.Conditions.Where(x => x.ColumnName != null)
                                            .Select(x => x.ColumnName.ToLower())
                                            .All(x => ValidateColumn(queryModel.DatabaseName, queryModel.TableName, x));
            }
            return true;
        }

        private bool ValidateDynamicWhereStatement(TableQueryModel tableQueryModel)
        {
            if (tableQueryModel.WhereStatement != null)
            {
                string whereStatement = tableQueryModel.WhereStatement.ToLower();

                foreach (string keyword in disabledKeywords.Select(x => x.ToLower()))
                {
                    if (whereStatement.Contains(keyword))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}