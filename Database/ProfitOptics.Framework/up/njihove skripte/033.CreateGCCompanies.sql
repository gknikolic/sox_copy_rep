USE [Sox]

BEGIN

CREATE TABLE [dbo].[GCParentCompany]
(
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Title] [varchar](200) NOT NULL
)

CREATE TABLE [dbo].[GCChildCompany]
(
	[Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Title] [varchar](200) NOT NULL,
	[GCParentCompanyId] [int],
	FOREIGN KEY ([GCParentCompanyId]) REFERENCES [dbo].[GCParentCompany] ([Id])
)

END