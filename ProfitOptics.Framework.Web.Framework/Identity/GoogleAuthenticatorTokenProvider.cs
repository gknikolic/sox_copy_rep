﻿using Microsoft.AspNetCore.Identity;
using OtpSharp;
using ProfitOptics.Framework.Web.Framework.Identity.Domain;
using System.Threading.Tasks;
using Wiry.Base32;

namespace ProfitOptics.Framework.Web.Framework.Identity
{
    public class GoogleAuthenticatorTokenProvider : IUserTwoFactorTokenProvider<ApplicationUser>
    {
        public Task<bool> CanGenerateTwoFactorTokenAsync(UserManager<ApplicationUser> manager, ApplicationUser user)
        {
            return Task.FromResult(user.IsGoogleAuthenticatorEnabled);
        }

        public Task<string> GenerateAsync(string purpose, UserManager<ApplicationUser> manager, ApplicationUser user)
        {
            return Task.FromResult((string)null);
        }

        public Task<bool> ValidateAsync(string purpose, string token, UserManager<ApplicationUser> manager, ApplicationUser user)
        {
            var otp = new Totp(Base32Encoding.Standard.ToBytes(user.GoogleAuthenticatorSecretKey));
            var valid = otp.VerifyTotp(token, out _, new VerificationWindow(2, 2));
            return Task.FromResult(valid);
        }
    }
}