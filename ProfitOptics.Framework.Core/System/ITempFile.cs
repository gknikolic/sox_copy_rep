﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProfitOptics.Framework.Core.System
{
    public interface ITempFile
    {
        string GetFileName();

        string GetTempFileForUser(int userId, string fileName);

        MemoryStream ToMemoryStream();
    }
}
