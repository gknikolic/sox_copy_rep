﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using ProfitOptics.Framework.Web.Framework.Helpers.Extensions;

namespace ProfitOptics.Modules.Support.Services
{
    public class PersistedDataService : IPersistedDataService
    {
        #region Fields

        private readonly ISession _session;

        #endregion Fields

        #region Ctor

        public PersistedDataService(IHttpContextAccessor httpContextAccessor)
        {
            _session = httpContextAccessor.HttpContext.Session;
        }

        #endregion Ctor

        #region Get Methods

        public string GetSelectedProject()
        {
            var selectedProject = _session.Get<string>(SupportModuleKeys.Session.SelectedProjectKey);

            return selectedProject;
        }

        public string GetSelectedReporter()
        {
            var selectedReporter = _session.Get<string>(SupportModuleKeys.Session.SelectedReporter);

            return selectedReporter;
        }

        public string GetSelectedAssignee()
        {
            var selectedAssignee = _session.Get<string>(SupportModuleKeys.Session.SelectedAssignee);

            return selectedAssignee;
        }

        public string GetSelectedComponent()
        {
            var selectedComponent = _session.Get<string>(SupportModuleKeys.Session.SelectedComponent);

            return selectedComponent;
        }

        public List<string> GetSelectedMultipleComponents()
        {
            var components = _session.Get<List<string>>(SupportModuleKeys.Session.SelectedComponents);

            return components;
        }

        #endregion Get Methods

        #region Set Methods

        public void SetSelectedReporter(string reporter)
        {
            _session.Set<string>(SupportModuleKeys.Session.SelectedReporter, reporter);
        }

        public void SetSelectedAssignee(string assignee)
        {
            _session.Set<string>(SupportModuleKeys.Session.SelectedAssignee, assignee);
        }

        public void SetSelectedComponent(string component)
        {
            _session.Set<string>(SupportModuleKeys.Session.SelectedComponent, component);
        }

        public void SetSelectedProject(string project)
        {
            _session.Set<string>(SupportModuleKeys.Session.SelectedProjectKey, project);
        }

        public void SetSelectedMultipleComponents(List<string> components)
        {
            _session.Set<List<string>>(SupportModuleKeys.Session.SelectedComponents, components);
        }

        #endregion Set Methods
    }
}