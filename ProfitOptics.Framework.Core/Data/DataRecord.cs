﻿// Copyright (c) 2017, ProfitOptics Inc. Richmond, VA
// All rights reserved.	
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which is part of this source code package.
using ProfitOptics.Framework.Core.System;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfitOptics.Framework.Core.Data
{
    /// <summary>
    /// All data records in ProfitOptics.Framework implement IDataRecord
    /// </summary>
    public interface IDataRecord { }

    /// <summary>
    /// Helper methods for IDataRecord
    /// </summary>
    public class DataRecordHelpers
    {
        public static IDataReader ToDataReader<T>(List<T> data) where T : IDataRecord
        {
            var toReturn = new DataTable();
            var dataFieldProperties = GetPropertiesMetadata<T>();
            foreach (var property in dataFieldProperties)
                toReturn.Columns.Add(property.Attribute.Title, Nullable.GetUnderlyingType(property.Property.PropertyType) ?? property.Property.PropertyType);

            // Get data
            var values = new object[dataFieldProperties.Count];
            for (var i = 0; i < data.Count; i++)
            {
                for (var j = 0; j < values.Length; j++)
                    values[j] = dataFieldProperties[j].Property.GetValue(data[i]);

                toReturn.Rows.Add(values);
            }

            return new DataTableReader(toReturn);
        }

        /// <summary>
        /// Gets DataRecord properties with it's metadata in the order as specified in DataFieldAttribute.Position
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<PropertyMetadata> GetPropertiesMetadata<T>() where T : IDataRecord
        {
            var dataFieldProperties = new List<PropertyMetadata>();
            var properties = TypeDescriptor.GetProperties(typeof(T));

            // Get header
            foreach (PropertyDescriptor property in properties)
            {
                // Check if this property has any DataFieldAttribute. 
                foreach (var attribute in property.Attributes)
                {
                    var dataFieldAttribute = attribute as DataFieldAttribute;
                    if (dataFieldAttribute != null)
                    {
                        dataFieldProperties.Add(new PropertyMetadata
                        {
                            Property = property,
                            Attribute = dataFieldAttribute
                        });
                    }
                }
            }

            // Check if there are some duplicates
            if ((from s in dataFieldProperties
                 group s by s.Attribute.Position into g
                 select g.Count()).Where(p => p > 1).Count() > 0)
                throw new ProfitOpticsException(string.Format("Type {0} has properties with duplicated Position values in DataFieldAttribute.", typeof(T).ToString()));
            if((from s in dataFieldProperties
               group s by s.Attribute.Title into g
               select g.Count()).Where(p => p > 1).Count() > 0)
                throw new ProfitOpticsException(string.Format("Type {0} has properties with duplicated Title values in DataFieldAttribute.", typeof(T).ToString()));

            dataFieldProperties = dataFieldProperties.OrderBy(p => p.Attribute.Position).ToList();
            return dataFieldProperties;
        }
    }

    /// <summary>
    /// Metadata for DataRecord properties
    /// </summary>
    public class PropertyMetadata
    {
        public PropertyDescriptor Property { get; set; }
        public DataFieldAttribute Attribute { get; set; }
    }

    /// <summary>
    /// Use to annotate properties of IDataRecord which should be used during export/import operations, showing results in datatables etc.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class DataFieldAttribute : Attribute
    {
        public DataFieldAttribute(string title, string formatString, int position, bool applySubtotal = false)
        {
            Title = title;
            FormatString = formatString;
            Position = position;
            ApplySubtotal = applySubtotal;
        }

        /// <summary>
        /// Data containers will use this as title. If it is an empty string, data containers will use PropertyName
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Data containers will use this as FormatString in ToString() methods.
        /// <example>
        /// Excel formats (for ExcelFile exports/imports):
        /// "#,##0.00_);(#,##0.00)"
        /// "0.00%",
        /// "MM/dd/yyyy"
        /// 
        /// For delimited exportrs/imports, .NET format strings work good.
        /// </example>
        /// </summary>
        public string FormatString { get; private set; }

        /// <summary>
        /// Used to specify at what position property can be found or is displayed.
        /// </summary>
        public int Position { get; set; }

        public bool ApplySubtotal { get; set; }
    }
}
