﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitOptics.Framework.Core.Settings
{
    public class CensiTracOptions
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int ClientId { get; set; }
    }
}
