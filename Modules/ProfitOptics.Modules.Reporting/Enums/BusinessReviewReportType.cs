﻿namespace ProfitOptics.Modules.Reporting.Enums
{
    public enum BusinessReviewReportType
    {
        BillTo = 1,
        ShipTo = 2
    }
}