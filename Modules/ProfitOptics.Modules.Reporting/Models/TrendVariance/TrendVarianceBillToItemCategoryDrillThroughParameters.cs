﻿using System;
using ProfitOptics.Modules.Reporting.Enums;

namespace ProfitOptics.Modules.Reporting.Models.TrendVariance
{
    public class TrendVarianceBillToItemCategoryDrillThroughParameters
    {
        public int BillToId { get; set; }

        public int ItemCategoryId { get; set; }

        public ComparisonType ComparisonType { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
